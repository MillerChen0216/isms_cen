﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_31170.aspx.vb" Inherits="ISMS_31170" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <div>
                <div>
                    <br />
                            <asp:HiddenField ID="Hidden_DP" runat="server" />

                    <table id="Show_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 750px"
                        visible="true">
                        <tr>
                            <td class="tb_title_w_2" colspan="5" rowspan="1" >
                                <img src="Images/exe.GIF" />
                                威脅弱點評估表|列表
                                <asp:DropDownList ID="ddl_Asset" runat="server" AutoPostBack="True" >
                                    <asp:ListItem Value="0">請選擇大分類</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddl_assgroup" runat="server" AutoPostBack="True" >
                                    <asp:ListItem Value="0" Selected="True">請選擇資產群組</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Button ID="Show_Insert" runat="server" CssClass="button-small" Text="新增資料" />&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" >
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
                                    CellPadding="3" Width="750px" HorizontalAlign="Left">
                                    <Columns>
                                        <asp:BoundField DataField="risk_type" HeaderText="風險類別">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="threat" HeaderText="威脅">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="weakness" HeaderText="弱點">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="riskp_score" HeaderText="可能性">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="impact_score" HeaderText="衝擊性">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="tw_score" HeaderText="威脅弱點值">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>                                        
                                        <asp:ButtonField ButtonType="Button" CommandName="Edit_Btn" Text="編輯">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1"  />
                                        </asp:ButtonField>
                                        <asp:ButtonField ButtonType="Button" CommandName="Del_Btn" Text="刪除">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1"  />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="weakness_id" HeaderText="weakness_id">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="threat_id" HeaderText="threat_id">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                       <asp:BoundField DataField="ID" HeaderText="ID">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="assgroup_id" HeaderText="assgroup_id">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="risktype_id" HeaderText="risktype_id">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                         
                                        
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table id="Edit_TB" runat="server" class="tb_1" style="width:720px" visible="false" cellpadding="3" cellspacing="0">
                        <tr>
                            <td class="tb_title_w_2" colspan="9" ><img src="Images/exe.GIF" />
                                威脅脆弱評估表 | 更新資料 &nbsp;<asp:Button ID="Update_Btn" runat="server" CssClass="button-small"
                                    Text="更新資料" /></td>
                        </tr>
                        <tr>
                            <td class="tb_w_1" colspan="4">
                                資產大分類：<asp:Label ID="asset_ed_lb" runat="server"></asp:Label>｜群組化資產類別：<asp:Label ID="assgroup_ed_lb"
                                    runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                        <td class="tb_title_w_1" >
                                風險類別</td>
                            <td class="tb_title_w_1" >
                                威脅</td>
                            <td class="tb_title_w_1" >
                                弱點</td>
                            <td class="tb_title_w_1" >
                                可能性</td>
                            <td class="tb_title_w_1" >
                                衝擊性</td>
                        </tr>
                        <tr >
                                                    <td class="tb_w_1" >
                                &nbsp;<asp:DropDownList ID="ddl_risktype_upd" runat="server" >
                                </asp:DropDownList>&nbsp;
                            </td>
                            <td class="tb_w_1" >
                                &nbsp;<asp:DropDownList ID="ddl_threat_upd" runat="server" >
                                </asp:DropDownList>&nbsp;
                            </td>
                            <td class="tb_w_1" >
                                &nbsp;<asp:DropDownList ID="ddl_weakness_upd" runat="server">
                                </asp:DropDownList>&nbsp;</td>
                            <td class="tb_w_1" >
                                <asp:DropDownList ID="ddl_riskp_upd" runat="server" >
                                </asp:DropDownList></td>
                            <td class="tb_w_1" >
                                <asp:DropDownList ID="ddl_impact_upd" runat="server">
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                </div>
            </div>
            <table id="Ins_TB" runat="server" class="tb_1" style="width:720px" visible="false" cellpadding="3" cellspacing="0">
                <tr>
                    <td class="tb_title_w_2" colspan="9" ><img src="Images/exe.GIF" />
                        威脅脆弱評估表<span style="color: #656b76"> </span>| 新增資料 &nbsp;&nbsp;&nbsp;
                     <asp:Button ID="Insert_Btn" runat="server" CssClass="button-small"
                            Text="確定新增" /></td>
                </tr>
                <tr>
                    <td class="tb_w_1" colspan="4" >
                        資產大分類：<asp:Label ID="asset_ins_lb"
                                runat="server"></asp:Label>｜群組化資產類別：<asp:Label ID="assgroup_ins_lb" runat="server"></asp:Label></td>
                </tr>
                <tr>
                  <td class="tb_title_w_1" >
                        風險類別</td> 
                    <td class="tb_title_w_1" >
                        威脅</td>
                    <td class="tb_title_w_1" >
                        弱點</td>
                    <td class="tb_title_w_1" >
                        可能性</td>
                    <td class="tb_title_w_1" >
                        衝擊性</td>
                </tr>
                <tr >
                                   <td class="tb_w_1">
                        <asp:DropDownList ID="ddl_risktype_ins" runat="server" AutoPostBack="True" >
                        </asp:DropDownList></td> 
                    <td class="tb_w_1">
                        <asp:DropDownList ID="ddl_Cthreat_ins" runat="server" >
                        </asp:DropDownList></td>
                    <td class="tb_w_1">
                        <asp:DropDownList ID="ddl_Cweakness_ins" runat="server">
                        </asp:DropDownList>
                    </td>                        
                    <td class="tb_w_1">
                        <asp:DropDownList ID="ddl_Criskp_ins" runat="server">
                        </asp:DropDownList></td>
                    <td class="tb_w_1">
                        <asp:DropDownList ID="ddl_Cimpact_ins" runat="server">
                        </asp:DropDownList></td>
                </tr>
            </table>
        </div>
    
    </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <asp:HiddenField ID="Hidden_ID" runat="server" Visible="False" />
    </form>
</body>
</html>
