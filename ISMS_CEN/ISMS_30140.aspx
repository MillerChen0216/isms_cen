﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_30140.aspx.vb" Inherits="ISMS_30140" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <div>
                <div>
                    <div>
                        <br />
                                <asp:HiddenField ID="Hidden_DP" runat="server" />

                        <table id="Show_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 500px"
                            visible="true">
                            <tr>
                                <td class="tb_title_w_2" colspan="5">
                                    <img src="Images/exe.GIF" />
                                    資產價值等級 | 列表
                                    <asp:Button ID="Show_Insert" runat="server" CssClass="button-small" Text="新增資料" /></td>
                            </tr>
                            <tr>
                                <td >
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="tb_1"
                                        HorizontalAlign="Left" Width="500px">
                                        <Columns>
                                            <asp:BoundField DataField="secure_lv" HeaderText="等級">
                                                <HeaderStyle CssClass="tb_title_w_1" />
                                                <ItemStyle CssClass="tb_w_1" Width="10%" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="資產價值">
                                                <ItemTemplate>
                                                    <asp:Label ID="lv_scoreS" runat="server" Text='<%# Bind ("lv_scoreS") %>'></asp:Label>
                                                    ~
                                                    <asp:Label ID="lv_scoreE" runat="server" Text='<%# Bind ("lv_scoreE") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="tb_title_w_1" />
                                                <ItemStyle CssClass="tb_w_1" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="serure_desc" HeaderText="價值等級">
                                                <HeaderStyle CssClass="tb_title_w_1" />
                                                <ItemStyle CssClass="tb_w_1" />
                                            </asp:BoundField>
                                            <asp:ButtonField ButtonType="Button" CommandName="Edit_Btn" Text="編輯">
                                                <HeaderStyle CssClass="tb_title_w_1" />
                                                <ItemStyle CssClass="tb_w_1" Width="10%" />
                                            </asp:ButtonField>
                                            <asp:ButtonField ButtonType="Button" CommandName="Del_Btn" Text="刪除">
                                                <HeaderStyle CssClass="tb_title_w_1" />
                                                <ItemStyle CssClass="tb_w_1" Width="10%" />
                                            </asp:ButtonField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <table id="Edit_TB" runat="server" class="tb_1" style="width: 400px;"
                            visible="false" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="tb_title_w_2" colspan="7"><img src="Images/exe.GIF" />
                                    資產價值等級 | 更新資料 &nbsp;<asp:Button ID="Update_Btn" runat="server" CssClass="button-small"
                                        Text="更新資料" /></td>
                            </tr>
                            <tr>
                                <td class="tb_title_w_1">
                                    等級*</td>
                                <td class="tb_title_w_1" >
                                    資產價值</td>
                                <td class="tb_title_w_1" >
                                    價值等級</td>
                            </tr>
                            <tr>
                                <td class="tb_w_1">
                                    
                                        <asp:TextBox ID="secure_lv_upd" runat="server" Width="50px" ReadOnly="True" MaxLength="2" BackColor="WhiteSmoke"></asp:TextBox></td>
                                <td class="tb_w_1">
                                    <asp:TextBox ID="lv_scoreS_upd" runat="server" Width="60px" MaxLength="3"></asp:TextBox>
                                    ~
                                    <asp:TextBox ID="lv_scoreE_upd" runat="server" Width="60px" MaxLength="3"></asp:TextBox></td>
                                <td class="tb_w_1">
                                    
                                        <asp:TextBox ID="serure_desc_upd" runat="server" Width="70px" MaxLength="50"></asp:TextBox></td>
                            </tr>
                        </table>
                        </div>
                </div>
                <table id="ins_TB" runat="server" class="tb_1" style="width: 400px" visible="false" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="tb_title_w_2" colspan="7" style="height: 27px; width: 665px;"><img src="Images/exe.GIF" />
                            資產價值等級 | 新增資料 &nbsp;<asp:Button ID="Insert_Btn" runat="server" CssClass="button-small"
                                Text="新增資料" /></td>
                    </tr>
                    <tr>
                        <td class="tb_title_w_1">
                            等級*</td>
                        <td class="tb_title_w_1">
                            資產價值</td>
                        <td class="tb_title_w_1">
                            價值等級</td>
                    </tr>
                    <tr>
                        <td class="tb_w_1" >
                            
                                <asp:TextBox ID="secure_lv_ins" runat="server" Width="60px" MaxLength="2"></asp:TextBox></td>
                        <td class="tb_w_1">
                            <asp:TextBox ID="lv_scoreS_ins" runat="server" Width="60px" MaxLength="3"></asp:TextBox>
                            ~ &nbsp;<asp:TextBox ID="lv_scoreE_ins" runat="server" Width="60px" MaxLength="3"></asp:TextBox></td>
                        <td class="tb_w_1">
                             <asp:TextBox ID="serure_desc_ins" runat="server" Width="70px" MaxLength="50"></asp:TextBox></td>
                    </tr>
                </table>
                        <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="secure_lv_upd"
                            ErrorMessage="安全等級請輸入數值1到99" MaximumValue="99" MinimumValue="1" Display="None"></asp:RangeValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="secure_lv_ins"
                    ErrorMessage="安全等級請輸入數值1到99" MaximumValue="99" MinimumValue="1" Display="None"></asp:RangeValidator>
                        <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="lv_scoreS_upd"
                            ErrorMessage="資產價值請輸入數值0到999" MaximumValue="999" MinimumValue="0" Display="None"></asp:RangeValidator>
                        <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="lv_scoreE_upd"
                            ErrorMessage="資產價值請輸入數值0到999" MaximumValue="999" MinimumValue="0" Display="None"></asp:RangeValidator><br />
                <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="lv_scoreS_ins"
                    ErrorMessage="資產價值請輸入數值0到999" MaximumValue="999" MinimumValue="0" Display="None"></asp:RangeValidator><br />
                <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="lv_scoreE_ins"
                    ErrorMessage="資產價值請輸入數值0到999" MaximumValue="999" MinimumValue="0" Display="None"></asp:RangeValidator><br />
                <span style="font-size: 12px; color: #656b76; font-family: Arial; background-color: #b8e1f1">
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RangeValidator1">
                    </cc1:ValidatorCalloutExtender>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RangeValidator2">
                    </cc1:ValidatorCalloutExtender>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RangeValidator3">
                    </cc1:ValidatorCalloutExtender>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="RangeValidator4">
                    </cc1:ValidatorCalloutExtender>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="RangeValidator5">
                    </cc1:ValidatorCalloutExtender>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" TargetControlID="RangeValidator6">
                    </cc1:ValidatorCalloutExtender>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </span>
            </div>
        </div>
    
    </div>
    </form>
</body>
</html>
