﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_30230
    Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            'ddl_Asset.DataSource = Common.Get_Asset(Hidden_DP.Value)
            ddl_Asset.DataSource = Common.Get_Asset()
            ddl_Asset.DataValueField = "asset_id"
            ddl_Asset.DataTextField = "asset_name"
            ddl_Asset.DataBind()

        End If
        SqlDataSource()

    End Sub

    Protected Sub SqlDataSource()
        SqlDataSource1.ConnectionString = Conn.ConnectionString
        SqlDataSource1.SelectCommand = "SELECT a.assitem_id,b.assitem_name,a.info_type,c.info_type+'('+c.info_Cname+')' as info_Fname,a.weakness_id,d.weakness,e.threat,a.threat_id,a.riskp_score,a.impact_score FROM riskEvaluate as a,assetItem as b,infoSecurityType as c,weakness as d,threat as e WHERE b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id AND a.info_type = c.info_type AND b.asset_id=" + ddl_Asset.SelectedValue + " ORDER BY a.assitem_id,c.info_order"
        'Response.Write("SELECT a.assitem_id,b.assitem_name,a.info_type,c.info_type+'('+c.info_Cname+')' as info_Fname,a.weakness_id,d.weakness,e.threat,a.threat_id,a.riskp_score,a.impact_score FROM riskEvaluate as a,assetItem as b,infoSecurityType as c,weakness as d,threat as e WHERE b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id AND a.info_type = c.info_type AND b.asset_id=" + ddl_Asset.SelectedValue + " ORDER BY a.assitem_id,c.info_order")
        GridView1.DataSourceID = SqlDataSource1.ID
        Dim KeyNames() As String = {"assitem_id"}
        GridView1.DataKeyNames = KeyNames
    End Sub
    Protected Sub ShowInsddl()
        ddl_assitem_id_ins.DataSource = Common.Get_AssetItem(Hidden_DP.Value)
        ddl_assitem_id_ins.DataValueField = "assitem_id"
        ddl_assitem_id_ins.DataTextField = "assitem_name"
        ddl_assitem_id_ins.DataBind()


        ddl_Cweakness_ins.DataSource = Common.Get_weakness(ddl_Asset.SelectedValue)
        ddl_Cweakness_ins.DataValueField = "weakness_id"
        ddl_Cweakness_ins.DataTextField = "weakness"
        ddl_Cweakness_ins.DataBind()

        ddl_Iweakness_ins.DataSource = Common.Get_weakness(ddl_Asset.SelectedValue)
        ddl_Iweakness_ins.DataValueField = "weakness_id"
        ddl_Iweakness_ins.DataTextField = "weakness"
        ddl_Iweakness_ins.DataBind()

        ddl_Aweakness_ins.DataSource = Common.Get_weakness(ddl_Asset.SelectedValue)
        ddl_Aweakness_ins.DataValueField = "weakness_id"
        ddl_Aweakness_ins.DataTextField = "weakness"
        ddl_Aweakness_ins.DataBind()

        ddl_Cthreat_ins.DataSource = Common.Get_threat(ddl_Asset.SelectedValue)
        ddl_Cthreat_ins.DataValueField = "threat_id"
        ddl_Cthreat_ins.DataTextField = "threat"
        ddl_Cthreat_ins.DataBind()

        ddl_Ithreat_ins.DataSource = Common.Get_threat(ddl_Asset.SelectedValue)
        ddl_Ithreat_ins.DataValueField = "threat_id"
        ddl_Ithreat_ins.DataTextField = "threat"
        ddl_Ithreat_ins.DataBind()

        ddl_Athreat_ins.DataSource = Common.Get_threat(ddl_Asset.SelectedValue)
        ddl_Athreat_ins.DataValueField = "threat_id"
        ddl_Athreat_ins.DataTextField = "threat"
        ddl_Athreat_ins.DataBind()

        ddl_Criskp_ins.DataSource = Common.Get_riskP()
        ddl_Criskp_ins.DataValueField = "riskP_score"
        ddl_Criskp_ins.DataTextField = "riskP_desc"
        ddl_Criskp_ins.DataBind()

        ddl_Iriskp_ins.DataSource = Common.Get_riskP()
        ddl_Iriskp_ins.DataValueField = "riskP_score"
        ddl_Iriskp_ins.DataTextField = "riskP_desc"
        ddl_Iriskp_ins.DataBind()

        ddl_Ariskp_ins.DataSource = Common.Get_riskP()
        ddl_Ariskp_ins.DataValueField = "riskP_score"
        ddl_Ariskp_ins.DataTextField = "riskP_desc"
        ddl_Ariskp_ins.DataBind()

        ddl_Cimpact_ins.DataSource = Common.Get_riskImpact()
        ddl_Cimpact_ins.DataValueField = "impact_score"
        ddl_Cimpact_ins.DataTextField = "impact_desc"
        ddl_Cimpact_ins.DataBind()

        ddl_Iimpact_ins.DataSource = Common.Get_riskImpact()
        ddl_Iimpact_ins.DataValueField = "impact_score"
        ddl_Iimpact_ins.DataTextField = "impact_desc"
        ddl_Iimpact_ins.DataBind()

        ddl_Aimpact_ins.DataSource = Common.Get_riskImpact()
        ddl_Aimpact_ins.DataValueField = "impact_score"
        ddl_Aimpact_ins.DataTextField = "impact_desc"
        ddl_Aimpact_ins.DataBind()

    End Sub
    Protected Sub ShowUpdddl()
        ddl_weakness_upd.DataSource = Common.Get_weakness(ddl_Asset.SelectedValue)
        ddl_weakness_upd.DataValueField = "weakness_id"
        ddl_weakness_upd.DataTextField = "weakness"
        ddl_weakness_upd.DataBind()

        ddl_threat_upd.DataSource = Common.Get_threat(ddl_Asset.SelectedValue)
        ddl_threat_upd.DataValueField = "threat_id"
        ddl_threat_upd.DataTextField = "threat"
        ddl_threat_upd.DataBind()

        ddl_riskp_upd.DataSource = Common.Get_riskP()
        ddl_riskp_upd.DataValueField = "riskP_score"
        ddl_riskp_upd.DataTextField = "riskP_desc"
        ddl_riskp_upd.DataBind()

        ddl_impact_upd.DataSource = Common.Get_riskImpact()
        ddl_impact_upd.DataValueField = "impact_score"
        ddl_impact_upd.DataTextField = "impact_desc"
        ddl_impact_upd.DataBind()

    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)

        Select Case e.CommandName

            Case "Edit_Btn"

                Edit_TB.Visible = True
                Show_TB.Visible = False

                assitem_name_upd.Text = SelectedRow.Cells(1).Text
                info_type_upd.Text = SelectedRow.Cells(9).Text
                ddl_weakness_upd.SelectedValue = SelectedRow.Cells(10).Text
                ddl_threat_upd.SelectedValue = SelectedRow.Cells(11).Text
                ddl_riskp_upd.SelectedValue = SelectedRow.Cells(5).Text
                ddl_impact_upd.SelectedValue = SelectedRow.Cells(6).Text

                Hidden_assitem_id.Value = SelectedRow.Cells(0).Text

                ShowUpdddl()

            Case "Del_Btn"
                SqlTxt = "DELETE FROM [riskEvaluate] WHERE assitem_id ='" + SelectedRow.Cells(0).Text + "'"

                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                GridView1.DataBind()

        End Select

    End Sub

    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click

        SqlTxt = "UPDATE [riskEvaluate] SET [weakness_id] =" + ddl_weakness_upd.SelectedValue + ", [threat_id] =" + ddl_threat_upd.SelectedValue + ",riskp_score=" + ddl_riskp_upd.SelectedValue + ",impact_score=" + ddl_impact_upd.SelectedValue + " WHERE assitem_id ='" + Hidden_assitem_id.Value + "' AND info_type='" + info_type_upd.Text + "'"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()

        GridView1.DataBind()
        Show_TB.Visible = True
        Edit_TB.Visible = False


    End Sub

    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        Dim SInfotype As String

        Conn.Open()
        SInfotype = "C"
        SqlTxt = "INSERT INTO riskEvaluate ([assitem_id],[info_type],[weakness_id],[threat_id],[riskp_score],[impact_score]) VALUES ('" + ddl_assitem_id_ins.SelectedValue + "','" + SInfotype + "'," + ddl_Cweakness_ins.SelectedValue + "," + ddl_Cthreat_ins.SelectedValue + "," + ddl_Criskp_ins.SelectedValue + "," + ddl_Cimpact_ins.SelectedValue + ")"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        SqlCmd.ExecuteNonQuery()

        SInfotype = "I"
        SqlTxt = "INSERT INTO riskEvaluate ([assitem_id],[info_type],[weakness_id],[threat_id],[riskp_score],[impact_score]) VALUES ('" + ddl_assitem_id_ins.SelectedValue + "','" + SInfotype + "'," + ddl_Iweakness_ins.SelectedValue + "," + ddl_Ithreat_ins.SelectedValue + "," + ddl_Iriskp_ins.SelectedValue + "," + ddl_Iimpact_ins.SelectedValue + ")"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        SqlCmd.ExecuteNonQuery()

        SInfotype = "A"
        SqlTxt = "INSERT INTO riskEvaluate ([assitem_id],[info_type],[weakness_id],[threat_id],[riskp_score],[impact_score]) VALUES ('" + ddl_assitem_id_ins.SelectedValue + "','" + SInfotype + "'," + ddl_Aweakness_ins.SelectedValue + "," + ddl_Athreat_ins.SelectedValue + "," + ddl_Ariskp_ins.SelectedValue + "," + ddl_Aimpact_ins.SelectedValue + ")"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        SqlCmd.ExecuteNonQuery()

        Conn.Close()

        GridView1.DataBind()

        Show_TB.Visible = True
        Ins_TB.Visible = False

    End Sub

    Protected Sub Show_Insert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert.Click
        Show_TB.Visible = False
        Ins_TB.Visible = True
        ShowInsddl()
    End Sub
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow OrElse e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(0).Visible = False
            e.Row.Cells(9).Visible = False
            e.Row.Cells(10).Visible = False
            e.Row.Cells(11).Visible = False

        End If

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        Dim oDataRow As Data.DataRowView
        Dim sShowField As String
        Dim sMessage As String
        If e.Row.RowIndex <> -1 Then

            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = oDataRow.Item("assitem_name").ToString()
            sMessage = String.Format("您確定要刪除 [{0}] 嗎?", sShowField)
            CType(e.Row.Cells(8).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"
        End If
    End Sub
End Class
