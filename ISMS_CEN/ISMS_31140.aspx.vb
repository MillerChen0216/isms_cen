﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports System.Collections.Generic
Partial Class ISMS_31140
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer
    Dim oRow As GridViewRow
    Dim Index As Integer


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        If Not Page.IsPostBack Then
            ddl_Asset.DataSource = Common.Get_Asset()
            ddl_Asset.DataValueField = "asset_id"
            ddl_Asset.DataTextField = "asset_name"
            ddl_Asset.DataBind()
            SetFields()
        End If

        Dim ds1 As New DataSet
        ds1 = Common.Get_Asset
        If ds1.Tables(0).Rows.Count = 0 Then
            Server.Transfer("ISMS_20110.aspx")
        End If


        Dim Ds As New DataSet
        Ds = Common.Get_RiskEvCount(Hidden_DP.Value, ddl_Asset.SelectedValue)

        If Ds.Tables(0).Rows.Count >= 1 Then
            SqlDataSource()
            WF_SqlDS()
            tb2.Visible = True
            GridView1.Visible = True
            Output_XLS.Visible = True
        Else
            tb2.Visible = False
            GridView1.Visible = False
            Output_XLS.Visible = False
        End If


    End Sub

    Protected Sub SqlDataSource()
        RiskForm_DS.ConnectionString = Conn.ConnectionString
        'KM LAND
        RiskForm_DS.SelectCommand = "SELECT a.risk_type,a.assitem_id ,b.assitem_name ,b.user_id ,b.holder ,b.dept_id,b.item_amount ,b.location ,b.Confidentiality ,b.Integrity ,b.Availability ,(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability) as assitScore,(Select secure_lv from secureLevel where lv_scoreS <=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)  and  lv_scoreE >=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)) as assitScore_lv ,(Select serure_desc from secureLevel where lv_scoreS <=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)  and  lv_scoreE >=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)) as assitSecure_lv,d.weakness,e.threat,a.riskp_score,a.impact_score ,(a.riskp_score " + Common.Sys_Config("RISK_operation") + " a.impact_score )  as totalrisk_score,(Select totalrisk_desc from totalRisklv where lv_scoreS <=(a.riskp_score " + Common.Sys_Config("Risk_operation") + " a.impact_score" + Common.Sys_Config("Risk_operation") + " (Select secure_lv from secureLevel where lv_scoreS <=(b.Confidentiality + b.Integrity + b.Availability) and lv_scoreE >=(b.Confidentiality + b.Integrity + b.Availability)) )  and  lv_scoreE >=(a.riskp_score " + Common.Sys_Config("Risk_operation") + " a.impact_score " + Common.Sys_Config("Risk_operation") + " (Select secure_lv from secureLevel where lv_scoreS <=(b.Confidentiality + b.Integrity + b.Availability) and lv_scoreE >=(b.Confidentiality + b.Integrity + b.Availability)) )) as totalrisk_lv FROM riskEvaluate as a,assetItem as b,weakness as d,threat as e WHERE b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id and a.dp=b.dp AND b.dp='" + Hidden_DP.Value + "' AND b.asset_id=" + ddl_Asset.SelectedValue + " ORDER BY a.assitem_id"
        'KM Tax
        'RiskForm_DS.SelectCommand = "SELECT a.risk_type,a.assitem_id ,b.assitem_name ,b.user_id ,b.holder ,b.dept_id,b.item_amount ,b.location ,b.Confidentiality ,b.Integrity ,b.Availability ,(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability) as assitScore,(Select secure_lv from secureLevel where lv_scoreS <=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)  and  lv_scoreE >=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)) as assitScore_lv ,(Select serure_desc from secureLevel where lv_scoreS <=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)  and  lv_scoreE >=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)) as assitSecure_lv,d.weakness,e.threat,a.riskp_score,a.impact_score ,(a.riskp_score " + Common.Sys_Config("RISK_operation") + " a.impact_score " + Common.Sys_Config("RISK_operation") + " (b.Confidentiality + b.Integrity + b.Availability))  as totalrisk_score,(Select totalrisk_desc from totalRisklv where lv_scoreS <=(a.riskp_score " + Common.Sys_Config("Risk_operation") + " a.impact_score" + Common.Sys_Config("Risk_operation") + " (Select secure_lv from secureLevel where lv_scoreS <=(b.Confidentiality + b.Integrity + b.Availability) and lv_scoreE >=(b.Confidentiality + b.Integrity + b.Availability)) )  and  lv_scoreE >=(a.riskp_score " + Common.Sys_Config("Risk_operation") + " a.impact_score " + Common.Sys_Config("Risk_operation") + " (Select secure_lv from secureLevel where lv_scoreS <=(b.Confidentiality + b.Integrity + b.Availability) and lv_scoreE >=(b.Confidentiality + b.Integrity + b.Availability)) )) as totalrisk_lv FROM riskEvaluate as a,assetItem as b,weakness as d,threat as e WHERE b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id AND b.asset_id=" + ddl_Asset.SelectedValue + " ORDER BY a.assitem_id"
        'Response.Write("SELECT a.risk_type,a.assitem_id ,b.assitem_name ,b.user_id ,b.holder ,b.dept_id,b.item_amount ,b.location ,b.Confidentiality ,b.Integrity ,b.Availability ,(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability) as assitScore,(Select secure_lv from secureLevel where lv_scoreS <=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)  and  lv_scoreE >=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)) as assitScore_lv ,(Select serure_desc from secureLevel where lv_scoreS <=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)  and  lv_scoreE >=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)) as assitSecure_lv,d.weakness,e.threat,a.riskp_score,a.impact_score ,(a.riskp_score " + Common.Sys_Config("RISK_operation") + " a.impact_score " + Common.Sys_Config("RISK_operation") + " (b.Confidentiality + b.Integrity + b.Availability))  as totalrisk_score,(Select totalrisk_desc from totalRisklv where lv_scoreS <=(a.riskp_score " + Common.Sys_Config("Risk_operation") + " a.impact_score" + Common.Sys_Config("Risk_operation") + " (Select secure_lv from secureLevel where lv_scoreS <=(b.Confidentiality + b.Integrity + b.Availability) and lv_scoreE >=(b.Confidentiality + b.Integrity + b.Availability)) )  and  lv_scoreE >=(a.riskp_score " + Common.Sys_Config("Risk_operation") + " a.impact_score " + Common.Sys_Config("Risk_operation") + " (Select secure_lv from secureLevel where lv_scoreS <=(b.Confidentiality + b.Integrity + b.Availability) and lv_scoreE >=(b.Confidentiality + b.Integrity + b.Availability)) )) as totalrisk_lv FROM riskEvaluate as a,assetItem as b,weakness as d,threat as e WHERE b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id AND b.asset_id=" + ddl_Asset.SelectedValue + " ORDER BY a.assitem_id")
        GridView1.DataSourceID = RiskForm_DS.ID
    End Sub

    Protected Sub GridView1_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.PreRender
        Dim i As Integer = 1
        Dim j As Integer = 0
        Dim k As Integer
        Dim AlternatingRowStyle_i As Integer = 1
        Dim mySingleRow As GridViewRow

        For Each mySingleRow In GridView1.Rows
            For j = 0 To 13
                GridView1.Rows(CInt(mySingleRow.RowIndex)).Cells(j).CssClass = "tb_w_1"
            Next
        Next

        For Each mySingleRow In GridView1.Rows

            If CInt(mySingleRow.RowIndex) = 0 Then

                For k = 0 To 8
                    mySingleRow.Cells(k).RowSpan = 1
                Next


            Else
                If mySingleRow.Cells(0).Text.Trim() = GridView1.Rows(CInt(mySingleRow.RowIndex) - i).Cells(0).Text.Trim() And mySingleRow.Cells(1).Text.Trim() = GridView1.Rows(CInt(mySingleRow.RowIndex) - i).Cells(1).Text.Trim() Then

                    For k = 0 To 8
                        GridView1.Rows(CInt(mySingleRow.RowIndex) - i).Cells(k).RowSpan += 1
                    Next

                    i = i + 1
                    For k = 0 To 8
                        mySingleRow.Cells(k).Visible = False
                    Next


                Else
                    For k = 0 To 8
                        GridView1.Rows(CInt(mySingleRow.RowIndex)).Cells(k).RowSpan = 1
                    Next
                    i = 1
                End If
            End If




        Next
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim tc As TableCellCollection = e.Row.Cells
            tc.Clear()

            tc.Add(New TableHeaderCell)
            tc(0).Text = "風險類別"
            tc.Add(New TableHeaderCell)
            tc(1).Text = "資產編號"
            tc.Add(New TableHeaderCell)
            tc(2).Text = "資產名稱"
            tc.Add(New TableHeaderCell)
            tc(3).Text = "使用者"
            tc.Add(New TableHeaderCell)
            tc(4).Text = "擁有者"
            tc.Add(New TableHeaderCell)
            tc(5).Text = "權責單位"
            tc.Add(New TableHeaderCell)
            tc(6).Text = "數量"
            tc.Add(New TableHeaderCell)
            tc(7).Text = "放置地點"
            tc.Add(New TableHeaderCell)
            tc(8).Text = "資產價值"
            tc.Add(New TableHeaderCell)
            'tc(9).Text = "資產安全等級"
            'tc.Add(New TableHeaderCell)
            tc(9).Text = "威脅"
            tc.Add(New TableHeaderCell)
            tc(10).Text = "弱點"
            tc.Add(New TableHeaderCell)
            tc(11).Text = "可能性"
            tc.Add(New TableHeaderCell)
            tc(12).Text = "衝擊性"
            tc.Add(New TableHeaderCell)
            'KM LAND
            tc(13).Text = "威脅弱點值"
            'KM TAX
            'tc(13).Text = "綜合風險值"  


            'tc.Add(New TableHeaderCell)
            'tc(15).Text = "風險等級"


            For i = 0 To 13
                tc(i).CssClass = "tb_title_w_1"
            Next


        End If
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound


        If e.Row.RowIndex <> -1 Then
            'Dim serure_desc As String
            Dim Ds, Ds1 As New DataSet
            e.Row.Cells(3).Text = Common.Get_User_Name(e.Row.Cells(3).Text)
            e.Row.Cells(4).Text = Common.Get_User_Name(e.Row.Cells(4).Text)
            e.Row.Cells(5).Text = Common.Get_dept_Name(e.Row.Cells(5).Text)

            Ds = Common.Get_AssetName(e.Row.Cells(0).Text)
            e.Row.Cells(0).Text = Ds.Tables(0).Rows(0).Item("asset_name")

            'Ds = Common.Get_Secure_lv(e.Row.Cells(8).Text)
            'serure_desc = Ds.Tables(0).Rows(0).Item("serure_desc")
            'e.Row.Cells(9).Text = serure_desc
        End If
        'Response.Write(e.Row.RowIndex)

        If e.Row.Cells(1).Text = "" Then
            tb2.Visible = False
        Else
            tb2.Visible = True
        End If




    End Sub


    Protected Sub SetFields()

        Dim risk_type, dept_id, assitem_id, assitem_name, location, item_amount, user_id, holder As New BoundField
        Dim assitSecure_lv, assitScore, weakness, threat, riskp_score, impact_score, totalrisk_score, totalrisk_lv As New BoundField

        dept_id.DataField = "dept_id"
        assitem_id.DataField = "assitem_id"
        assitem_name.DataField = "assitem_name"
        user_id.DataField = "user_id"
        holder.DataField = "holder"
        item_amount.DataField = "item_amount"
        location.DataField = "location"
        assitScore.DataField = "assitScore"
        assitSecure_lv.DataField = "assitSecure_lv"
        weakness.DataField = "weakness"
        threat.DataField = "threat"
        riskp_score.DataField = "riskp_score"
        impact_score.DataField = "impact_score"
        totalrisk_score.DataField = "totalrisk_score"
        totalrisk_lv.DataField = "totalrisk_lv"
        risk_type.DataField = "risk_type"




        GridView1.Columns.Add(risk_type)
        GridView1.Columns.Add(assitem_id)
        GridView1.Columns.Add(assitem_name)
        GridView1.Columns.Add(user_id)
        GridView1.Columns.Add(holder)
        GridView1.Columns.Add(dept_id)
        GridView1.Columns.Add(item_amount)
        GridView1.Columns.Add(location)
        GridView1.Columns.Add(assitScore)
        'GridView1.Columns.Add(assitSecure_lv)
        GridView1.Columns.Add(threat)
        GridView1.Columns.Add(weakness)
        GridView1.Columns.Add(riskp_score)
        GridView1.Columns.Add(impact_score)
        GridView1.Columns.Add(totalrisk_score)
        'GridView1.Columns.Add(totalrisk_lv)

    End Sub

    Protected Sub Output_XLS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Output_XLS.Click
        Dim ads As New System.Web.UI.WebControls.SqlDataSource
        Dim ExcelSqlTxt As String
        Dim sw As New System.IO.StringWriter()
        Dim htw As New System.Web.UI.HtmlTextWriter(sw)
        Dim dg As New GridView()
        Dim i As Integer = 1
        Dim j As Integer = 0
        Dim k As Integer
        Dim AlternatingRowStyle_i As Integer = 1
        Dim mySingleRow As GridViewRow
        Dim Ds, Ds1 As New DataSet

        Response.Clear()
        '檔名支援中文
        Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5")
        '避免儲存中文內容有亂碼
        Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8")
        'excel檔名()
        Response.AddHeader("content-disposition", "attachment;filename=" + DateTime.Now.ToString("yyyyMMdd HH:mm") + "_ISMS" + ddl_Asset.SelectedItem.Text + "風險評鑑表.xls")
        Response.ContentType = "application/vnd.ms-excel"
        Response.Charset = ""

        ads.ConnectionString = Conn.ConnectionString
        'KM TAX
        'ExcelSqlTxt = "SELECT a.risk_type as 風險類別,a.assitem_id as 資產編號,b.assitem_name as 資產名稱,b.user_id as 使用者,b.holder as 擁有者,b.dept_id as 權責單位,b.item_amount as 數量,b.location as 放置地點,(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability) as 資產價值,e.threat as 威脅,d.weakness as 弱點,a.riskp_score as 可能性,a.impact_score as 衝擊性,(a.riskp_score " + Common.Sys_Config("Risk_operation") + " a.impact_score " + Common.Sys_Config("Risk_operation") + " (b.Confidentiality + b.Integrity + b.Availability))  as 綜合風險 FROM riskEvaluate as a,assetItem as b,weakness as d,threat as e WHERE b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id  AND b.asset_id=" + ddl_Asset.SelectedValue + " ORDER BY a.assitem_id"
        'KM LAND
        ExcelSqlTxt = "SELECT a.risk_type as 風險類別,a.assitem_id as 資產編號,b.assitem_name as 資產名稱,b.user_id as 使用者,b.holder as 擁有者,b.dept_id as 權責單位,b.item_amount as 數量,b.location as 放置地點,(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability) as 資產價值,e.threat as 威脅,d.weakness as 弱點,a.riskp_score as 可能性,a.impact_score as 衝擊性,(a.riskp_score " + Common.Sys_Config("Risk_operation") + " a.impact_score )  as 威脅弱點值 FROM riskEvaluate as a,assetItem as b,weakness as d,threat as e WHERE b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id  AND b.asset_id=" + ddl_Asset.SelectedValue + " AND a.dp=b.dp AND b.dp='" + Hidden_DP.Value + "' ORDER BY a.assitem_id"
        'Response.Write(ExcelSqlTxt)
        'Response.End()
        ads.SelectCommand = ExcelSqlTxt
        dg.DataSource = ads.Select(DataSourceSelectArguments.Empty)
        dg.DataBind()


        For k = 0 To dg.Rows.Count - 1

            dg.Rows(k).Cells(3).Text = Common.Get_User_Name(dg.Rows(k).Cells(3).Text)
            dg.Rows(k).Cells(4).Text = Common.Get_User_Name(dg.Rows(k).Cells(4).Text)
            dg.Rows(k).Cells(5).Text = Common.Get_dept_Name(dg.Rows(k).Cells(5).Text)

            Dim dsx As New DataSet
            Ds = Common.Get_AssetName(dg.Rows(k).Cells(0).Text)
            dg.Rows(k).Cells(0).Text = Ds.Tables(0).Rows(0).Item("asset_name")


        Next


        '合併相同儲存格()
        For Each mySingleRow In dg.Rows

            If CInt(mySingleRow.RowIndex) = 0 Then

                For k = 0 To 8
                    mySingleRow.Cells(k).RowSpan = 1
                Next

            Else
                If mySingleRow.Cells(0).Text.Trim() = GridView1.Rows(CInt(mySingleRow.RowIndex) - i).Cells(0).Text.Trim() And mySingleRow.Cells(1).Text.Trim() = GridView1.Rows(CInt(mySingleRow.RowIndex) - i).Cells(1).Text.Trim() Then

                    For k = 0 To 8
                        dg.Rows(CInt(mySingleRow.RowIndex) - i).Cells(k).RowSpan += 1

                    Next

                    i = i + 1

                    For k = 0 To 8
                        mySingleRow.Cells(k).Visible = False

                    Next

                Else

                    For k = 0 To 8
                        dg.Rows(CInt(mySingleRow.RowIndex)).Cells(k).RowSpan = 1
                    Next

                    i = 1
                End If
            End If
        Next


        dg.RenderControl(htw)
        Response.Write(sw.ToString())
        Response.End()

    End Sub


    Protected Sub WF_SqlDS()

        WF_DS.ConnectionString = Common.ConnDBS.ConnectionString
        WF_DS.SelectCommand = "SELECT * FROM workflowName where dp='" + Hidden_DP.Value + "' AND apprv_id='5' order by wf_order, wf_inside_order"
        GridView2.DataSourceID = WF_DS.ID

    End Sub

    Protected Sub GridView2_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowCreated
        e.Row.Cells(5).Visible = False

    End Sub

    Protected Sub Send_WF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Send_WF.Click

        Dim Ds, Ds1, Ds3 As New DataSet
        Ds1 = Common.Get_CheckRiskName(Hidden_DP.Value, Risk_name.Text)

        If Ds1.Tables(0).Rows.Count >= 1 Then

            Common.showMsg(Me.Page, "風險填表名稱重複，請重新輸入新的填表名稱!")
            Risk_name.Focus()

        ElseIf Risk_name.Text <> "" Then

            Dim DateTime_Str As String = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            SqlTxt = " INSERT INTO riskEvaluateMaster" + _
            "(riskEvtab_name,asset_type,form_status,add_user,add_date,dp)" + _
            "VALUES('" + Risk_name.Text + "','" + ddl_Asset.SelectedItem.Text + "','RISK_VERIFY','" + User.Identity.Name.ToString + "','" + DateTime_Str + "','" + Hidden_DP.Value + "') "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

            Ds = Common.Get_MaxFormid
            Dim Max_id As String = Ds.Tables(0).Rows(0).Item(0)

            SqlTxt = "INSERT INTO riskEvaluateForm (PID,risk_type,assitem_id,assitem_name,dept_id,item_amount,user_id,holder,location,assitScore,threat,weakness,riskp_score,impact_score,totalrisk_score)" + _
            "Select '" + Max_id + "' as PID,a.risk_type,a.assitem_id,b.assitem_name,b.dept_id,b.item_amount,b.user_id,b.holder,b.location,(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + "  b.Availability) as assitScore,e.threat,d.weakness,a.riskp_score,a.impact_score ,(a.riskp_score " + Common.Sys_Config("risk_operation") + "  a.impact_score " + Common.Sys_Config("risk_operation") + " (b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + "  b.Availability))  as totalrisk_score FROM riskEvaluate as a,assetItem as b,weakness as d,threat as e WHERE a.dp=b.dp AND b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id AND b.dp='" + Hidden_DP.Value + "' AND b.asset_id= " + ddl_Asset.SelectedValue + " ORDER BY a.assitem_id "
            ' Response.Write(SqlTxt)
            'Response.End()
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

            SqlTxt = "INSERT INTO wfFormRecord " + _
                 "(apprv_id,doc_id,wf_order,wf_userid,wf_status_id,wf_comment,signdate,record_id,wf_inside_order,doc_status) VALUES" + _
                "('5','" + Max_id + "','" + GridView2.Rows(0).Cells(0).Text + "','" + CType(GridView2.Rows(0).FindControl("label_userid"), Label).Text + "','" + CType(GridView2.Rows(0).FindControl("ddl_status"), DropDownList).SelectedValue + "'," + _
                " '" + CType(GridView2.Rows(0).FindControl("comment_txt"), TextBox).Text + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','1','" + GridView2.Rows(0).Cells(5).Text + "','RISK')"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Ds3 = Common.Get_Wf_ApprvUser(hidden_dp.Value, 5, GridView2.Rows(0).Cells(0).Text + 1)
            For i = 0 To Ds3.Tables(0).Rows.Count - 1
                Common.SendMail(Ds3.Tables(0).Rows(i).Item("wf_userid"), "WorkFlow_RISK", ddl_Asset.SelectedItem.Text, Risk_name.Text, "")
            Next

            Common.showMsg(Me.Page, "風險評鑑表已經送出審核，謝謝!")
        Else
            Common.showMsg(Me.Page, "風險評鑑表填表請勿空白，謝謝!")
            Risk_name.Focus()

        End If
    End Sub

    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound

        Dim Ds As New DataSet

        If e.Row.RowIndex <> -1 Then

            If e.Row.RowIndex = 0 Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(User.Identity.Name.ToString)
                CType(e.Row.FindControl("label_userid"), Label).Text = User.Identity.Name.ToString
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(0)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
            ElseIf e.Row.RowIndex >= 1 Then
                Ds = Common.Get_Wf_User(Hidden_DP.Value, 5, e.Row.Cells(0).Text, e.Row.Cells(5).Text)
                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(Ds.Tables(0).Rows(0).Item("wf_userid"))
                CType(e.Row.FindControl("label_userid"), Label).Text = Ds.Tables(0).Rows(0).Item("wf_userid")

            End If

        End If
    End Sub

    Protected Sub Risk_name_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Risk_name.TextChanged
        Dim ds As New DataSet
        ds = Common.Get_CheckRiskName(Hidden_DP.Value, Risk_name.Text)
        If ds.Tables(0).Rows.Count >= 1 Then
            Common.showMsg(Me.Page, "風險填表名稱重複，請重新輸入新的填表名稱!")
            Risk_name.Focus()
        Else
            Send_WF.Focus()
        End If
    End Sub



   
    Protected Sub ddl_Asset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Asset.SelectedIndexChanged
        Dim Ds As New DataSet
        Ds = Common.Get_RiskEvCount(Hidden_DP.Value, ddl_Asset.SelectedValue)

        If Ds.Tables(0).Rows.Count >= 1 Then

            SqlDataSource()
            WF_SqlDS()
            tb2.Visible = True
            GridView1.Visible = True
            Output_XLS.Visible = True
        Else
            tb2.Visible = False
            GridView1.Visible = False
            Output_XLS.Visible = False
        End If
    End Sub
End Class
