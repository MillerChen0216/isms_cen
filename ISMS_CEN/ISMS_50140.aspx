﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_50140.aspx.vb" Inherits="ISMS_50140" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
                                <asp:HiddenField ID="HiddenField1" runat="server" />

    <div>
        <asp:HiddenField ID="Hidden_DP" runat="server" />
        <br />
        <table id="Show_TB_degree" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 500px" visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    職稱管理
                    <asp:Button ID="Show_Insert_degree" runat="server" CssClass="button-small" Text="新增資料" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        CellPadding="3" CssClass="tb_1" Width="500px">
                        <Columns>
                            <asp:BoundField DataField="degree_name" HeaderText="職稱" >
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:ButtonField ButtonType="Button" CommandName="Edit_Btn" Text="編輯">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:ButtonField>
                            <asp:ButtonField ButtonType="Button" CommandName="Del_Btn" Text="刪除" >
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="degree_id" HeaderText="編號" >
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <table id="Edit_TB_degree" runat="server" cellpadding="3" cellspacing="0" class="tb_1"
            style="width: 500px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.GIF" />
                    職稱管理
                    <asp:Button ID="Update_Btn" runat="server" CssClass="button-small" Text="確定編輯" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    職稱</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="degree_Name_Edit" runat="server" Width="100px"></asp:TextBox></td>
            </tr>
        </table>
        <table id="Ins_TB_degree" runat="server" cellpadding="3" cellspacing="0" class="tb_1"
            style="width: 500px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.GIF" />
                    職稱管理
                    <asp:Button ID="Insert_Btn" runat="server" CssClass="button-small" Text="確定新增" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    職稱</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="degree_Name_Ins" runat="server" Width="100px"></asp:TextBox></td>
            </tr>
        </table>
        <br />
        <table id="Show_TB_dept" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 500px" visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    單位管理
                    <asp:Button ID="Show_Ins_dep" runat="server" CssClass="button-small" Text="新增資料" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView2" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        CellPadding="3" CssClass="tb_1" Width="500px">
                        <Columns>
                            <asp:BoundField DataField="dept_name" HeaderText="單位名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="below" HeaderText="上層部門">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="chief" HeaderText="部門主管">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:ButtonField ButtonType="Button" CommandName="Edit_Btn" Text="編輯">
                                <HeaderStyle CssClass="tb_title_w_1" />
                            </asp:ButtonField>
                            <asp:ButtonField ButtonType="Button" CommandName="Del_Btn" Text="刪除" >
                                <HeaderStyle CssClass="tb_title_w_1" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="dept_id" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server"></asp:SqlDataSource>
        <table id="Edit_TB_dept" runat="server" cellpadding="3" cellspacing="0" class="tb_1"
            style="width: 500px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="5">
                    <img src="Images/exe.GIF" />
                    單位管理
                    <asp:Button ID="Edit_BtnDep" runat="server" CssClass="button-small" Text="確定編輯" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    單位名稱</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="dept_name_Edit" runat="server" Width="100px"></asp:TextBox></td>
            </tr>
        </table>
        <table id="Ins_TB_dept" runat="server" cellpadding="3" cellspacing="0" class="tb_1"
            style="width: 500px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="5">
                    <img src="Images/exe.GIF" />
                    單位管理
                    <asp:Button ID="Ins_BtnDep" runat="server" CssClass="button-small" Text="確定新增" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    單位名稱</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="dept_name_ins" runat="server" Width="100px"></asp:TextBox></td>
            </tr>
        </table>
        <td class="tb_w_1">
        </td>
        <td class="tb_w_1">
        </td>
        <td class="tb_w_1">
        </td>
        <td class="tb_w_1">
        </td>
        <td class="tb_w_1">
        </td>
        <td class="tb_w_1">
        </td>
        <asp:HiddenField ID="hidden_degreeid" runat="server" Visible="False" /><asp:HiddenField ID="hidden_deptid" runat="server" Visible="False" />
        <br />
         <table id="Edit_TB_dp" runat="server" cellpadding="3" cellspacing="0" class="tb_1"
            style="width: 500px" visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="5">
                    <img src="Images/exe.GIF" />
                    文件群發mail設定
                    <asp:Button ID="Edit_BtnDP" runat="server" CssClass="button-small" Text="確定編輯" />
                </td>
            </tr>
            <tr>
            <td class="tb_title_w_1">機關名稱</td>
                <td class="tb_title_w_1">
                    發佈通知mail</td>
            </tr>
            <tr>
            <td class="tb_w_1">
                    <asp:TextBox ID="dp_name_Edit" runat="server" Width="100px" ReadOnly="True"></asp:TextBox></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="dp_mail_Edit" runat="server" Width="300px" MaxLength="50" ></asp:TextBox></td>
            </tr>
        </table>
        <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        &nbsp;
        <asp:RegularExpressionValidator ID="RVL_EmailFormat" runat="server" ControlToValidate="dp_mail_Edit"
            Display="None" ErrorMessage="不正確的E-Mail格式!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        <br />
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RVL_EmailFormat">
        </cc1:ValidatorCalloutExtender>
        <br />
        <br />
        <td class="tb_w_1">
        </td>
        <td class="tb_w_1">
        </td>
        <td class="tb_w_1">
        </td>
        <td class="tb_w_1">
        </td>
        <td class="tb_w_1">
        </td>
        <td class="tb_w_1">
        </td>
     </div>
    </form>
</body>
</html>