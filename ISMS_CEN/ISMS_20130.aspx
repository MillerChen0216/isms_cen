﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_20130.aspx.vb" Inherits="ISMS_20130" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>資產項清冊</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <asp:HiddenField ID="Hidden_DP" runat="server" />

        <br />
        <table id="Show_TB" class="tb_1" style="width: 750px" runat="server" visible="true" cellpadding="0" cellspacing="0">
            <tr>
                <td class="tb_title_w_2" colspan="5" rowspan="1">
                    <img src="Images/exe.gif" />
                    資產項清冊 | 列表
                    <asp:DropDownList ID="ddl_Asset" runat="server" Width="82px" AppendDataBoundItems="True">
                        <asp:ListItem Value="0">全部</asp:ListItem>
                    </asp:DropDownList>&nbsp;<asp:DropDownList ID="ddl_AssetQryColumn" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="All">請選擇</asp:ListItem>
                        <asp:ListItem Value="assgroup_name">業務流程別</asp:ListItem>
                        <asp:ListItem Value="assitem_id">資產編號</asp:ListItem>
                        <asp:ListItem Value="assitem_name">資產名稱</asp:ListItem>
                        <asp:ListItem Value="holder">擁有人</asp:ListItem>
                        <asp:ListItem Value="location">放置地點</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="t_querystring" runat="server" ToolTip="請輸入查詢字串" Width="167px" MaxLength="50"></asp:TextBox>
                    &nbsp;<asp:Button ID="btn_query" runat="server" CssClass="button-small" Text="查詢" />
                    <asp:Button ID="btn_Excel" runat="server" Text="另存Excel檔" Width="84px" CssClass="button-small" />
                    </td>
            </tr>
            <tr>
                <td rowspan="1">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
                        CellPadding="3" Width="750px">
                        <Columns>
                            <asp:BoundField DataField="assitem_id" HeaderText="資產編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="asset_Ename" HeaderText="資產類別">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="assitem_name" HeaderText="資產名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="assitem_desc" HeaderText="資產說明">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="location" HeaderText="地點" Visible="False">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="item_amount" HeaderText="數量">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="dept_name" HeaderText="權責單位">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="use_name" HeaderText="使用者" Visible="False">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="holder_name" HeaderText="擁有者" Visible="False">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Confidentiality" HeaderText="C">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Integrity" HeaderText="I">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Availability" HeaderText="A">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="資產價值">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="assgroup_name" HeaderText="資產群組">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="t_querystring"
                        ErrorMessage="輸入查詢內容不允許包含不合法字元" ValidationExpression="^[^\/<>&quot;'%;()&\-\=]{1,50}$" Display="None"></asp:RegularExpressionValidator><br />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RegularExpressionValidator1">
        </cc1:ValidatorCalloutExtender>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </form>
</body>
</html>