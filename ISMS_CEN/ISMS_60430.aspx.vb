﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO
Partial Class ISMS_60430
    Inherits PageBase
    'Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim uid, uname As String
    'Dim apprvid As String = ConfigurationManager.AppSettings("wf14")
    Dim apprvid As String
    Dim approvalid As String
    Dim approval_name As String = "地所系統使用者帳號申請表"
    Dim dp As String = Get_DP()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblOffice.Text = System.Web.Configuration.WebConfigurationManager.AppSettings("OfficeName")
        uid = User.Identity.Name
        uname = Common.Get_User_Name(uid)
        Dim nowpage() As String = Request.CurrentExecutionFilePath.Split("/")
        approvalid = Me.Get_approvalid(nowpage(nowpage.Length - 1), dp)
        apprvid = Me.Get_apprvid(nowpage(nowpage.Length - 1), dp)
        SqlDataSource()
        If Not IsPostBack Then
            bindNote()
            bindRepeater()
            ddlbind_year(ddl_year)
        End If

    End Sub
    Protected Sub bindRepeater()

        Repeater1.DataSource = Common.Get_auth_sys(approvalid, dp)
        Repeater1.DataBind()

    End Sub
    Protected Sub bindNote()
        Dim Ds As New DataSet
        Ds = Common.Get_auth_sys_note(approvalid, dp)
        If Ds.Tables(0).Rows.Count > 0 Then
            note_Edit.Text = Ds.Tables(0).Rows(0).Item("note")
        End If
    End Sub
    Protected Sub but_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_search.Click
        GridView1.DataBind()
    End Sub
    Protected Sub Repeater1_OnItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
            'Reference the Repeater Item.
            Dim item As RepeaterItem = e.Item
            'Reference the Controls.
            Dim idtRepeater As Repeater = (TryCast(item.FindControl("ChildRepeater"), Repeater))
            Dim sysid As HiddenField = (TryCast(item.FindControl("hidden_sysid"), HiddenField))
            idtRepeater.DataSource = Common.Get_auth_sys_idt(sysid.Value)
            idtRepeater.DataBind()
        End If
    End Sub
    Protected Sub SqlDataSource()
        '「歷年」申請原因為「新進人員(03)」、「職務異動(02)」之所有表單
        'Dim sql As String = "SELECT user_id,user_name,office_apply_id  FROM office_apply where apply_reason in('02','03') and dp = '" + dp + "'"
        '排除已撤銷之申請單
        'sql += " and using='Y' "
        '排除停用帳號
        'sql += " and user_id in (select user_id from ismsUser where using='Y') "
        'If txb_userid.Text <> "" Then
        'Sql += " and user_id = '" + txb_userid.Text + "'"
        'End If
        'If ddl_year.SelectedValue <> "" Then
        'sql += " AND SUBSTRING(apply_date,1,3) ='" + ddl_year.SelectedValue + "' "
        'Sql += " and apply_date + user_id in (select max(apply_date)+user_id from office_apply  where SUBSTRING(apply_date,1,3) ='" + ddl_year.SelectedValue + "' group by user_id ) "
        'ElseIf ddl_year.SelectedValue = "" Then
        'Sql += " and apply_date + user_id in(select MAX(apply_date)+user_id  from office_apply group by user_id) "
        'End If
        'If txb_year.Text <> "" Then
        '    sql += " and SUBSTRING(apply_date, 1, 3) = '" + txb_year.Text + "'"
        'End If
        'If txb_username.Text <> "" Then
        'Sql += " and user_name = '" + txb_username.Text + "'"
        'End If

        Dim sql As String = " select user_id,user_name,apply_date,office_apply_id from( "
        sql += " select user_id,user_name,apply_date,office_apply_id, "
        sql += " ROW_NUMBER()OVER (PARTITION BY user_id order by apply_date desc ) as sort from office_apply  "
        sql += " where apply_reason in('02','03') and dp ='" + dp + "' and using='Y' and user_id in (select user_id from ismsUser where using='Y') "
        If txb_userid.Text <> "" Then
            sql += " and user_id = '" + txb_userid.Text + "'"
        End If

        If ddl_year.SelectedValue <> "" Then
            sql += " AND SUBSTRING(apply_date,1,3) ='" + ddl_year.SelectedValue + "' "
        End If
        If txb_username.Text <> "" Then
            sql += " and user_name = '" + txb_username.Text + "'"
        End If
        sql += "  )office_apply_sort where office_apply_sort.sort = 1 "


        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = Sql

        GridView1.DataSourceID = SqlDataSource1.ID

    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        '今日(民國年)
        Dim yyymmdd As String = (Now.Year - 1911).ToString.PadLeft(3, "0") + DateTime.Now.ToString("MMdd")
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
        Dim office_apply_id As HiddenField = (TryCast(SelectedRow.FindControl("office_apply_id"), HiddenField))
        'Dim office_apply_id As HiddenField = (TryCast(SelectedRow.FindControl("hidden_office_apply_id"), HiddenField))
        Dim user_id As Label = (TryCast(SelectedRow.FindControl("lb_userId"), Label))
        Select Case e.CommandName

            Case "audit_btn"
                bindRepeater()
                Dim Ds, Ds1 As New DataSet
                'Ds = Common.Get_office_apply_byUser("'" + user_id.Text + "'", dp)
                Ds = Common.Get_office_apply_byUser("'" + office_apply_id.Value + "'", dp)
                If Ds.Tables(0).Rows.Count > 0 Then
                    lbl_dept_name.Text = Ds.Tables(0).Rows(0).Item("dept_name")
                    lbl_user_name.Text = Ds.Tables(0).Rows(0).Item("user_name")
                    lbl_ext.Text = Ds.Tables(0).Rows(0).Item("ext")
                    lbl_degree_name.Text = Ds.Tables(0).Rows(0).Item("degree_name")
                    lbl_item.Text = Ds.Tables(0).Rows(0).Item("item")
                  

                    'Dim start_date As String = (Now.Year - 1911 + 1).ToString.PadLeft(3, "0") + "0101"
                    'lbl_start_date.Text = start_date.Substring(0, 3) + "年" + start_date.Substring(3, 2) + "月" + start_date.Substring(5) + "日"
                    'Dim end_date As String = (Now.Year - 1911 + 1).ToString.PadLeft(3, "0") + "1231"
                    'lbl_end_date.Text = end_date.Substring(0, 3) + "年" + end_date.Substring(3, 2) + "月" + end_date.Substring(5) + "日"
                    'Dim apply_date As String = Ds.Tables(0).Rows(0).Item("apply_date").ToString
                    'If apply_date.Length = 7 Then
                    'lbl_apply_date.Text = apply_date.Substring(0, 3) + "年" + apply_date.Substring(3, 2) + "月" + apply_date.Substring(5, 2) + "日"
                    'Else
                    'lbl_apply_date.Text = apply_date
                    'End If
                    lbl_apply_date.Text = yyymmdd.Substring(0, 3) + "年" + yyymmdd.Substring(3, 2) + "月" + yyymmdd.Substring(5) + "日"
                    'Dim enable_date As String = Ds.Tables(0).Rows(0).Item("enable_date").ToString
                    'If enable_date.Length = 7 Then
                    'lbl_enable_date.Text = enable_date.Substring(0, 3) + "年" + enable_date.Substring(3, 2) + "月" + enable_date.Substring(5, 2) + "日"
                    'Else
                    'lbl_enable_date.Text = enable_date
                    'End If
                    lbl_enable_date.Text = (Now.Year - 1910).ToString.PadLeft(3, "0") + "年01月01日"
                    'Dim start_date As String = Ds.Tables(0).Rows(0).Item("start_date").ToString
                    'If start_date.Length = 7 Then
                    'lbl_start_date.Text = start_date.Substring(0, 3) + "年" + start_date.Substring(3, 2) + "月" + start_date.Substring(5, 2) + "日"
                    'Else
                    'lbl_start_date.Text = start_date
                    'End If
                    lbl_start_date.Text = (Now.Year - 1910).ToString.PadLeft(3, "0") + "年01月01日"
                    'Dim end_date As String = Ds.Tables(0).Rows(0).Item("end_date").ToString
                    'If end_date.Length = 7 Then
                    'lbl_end_date.Text = end_date.Substring(0, 3) + "年" + end_date.Substring(3, 2) + "月" + end_date.Substring(5, 2) + "日"
                    'Else
                    'lbl_end_date.Text = end_date
                    'End If
                    lbl_end_date.Text = (Now.Year - 1910).ToString.PadLeft(3, "0") + "年12月31日"
                    Dim disable_date As String = Ds.Tables(0).Rows(0).Item("disable_date").ToString
                    If disable_date.Length = 7 Then
                        lbl_disable_date.Text = disable_date.Substring(0, 3) + "年" + disable_date.Substring(3, 2) + "月" + disable_date.Substring(5, 2) + "日"
                    Else
                        lbl_disable_date.Text = disable_date
                    End If
                    lbl_user_id.Text = Ds.Tables(0).Rows(0).Item("user_id").ToString
                    lbl_disable_reason.Text = Ds.Tables(0).Rows(0).Item("disable_reason").ToString

                    For Each item As Control In Repeater1.Controls
                        Dim idt_repeater As Repeater = DirectCast(item.FindControl("ChildRepeater"), Repeater)
                        Dim hid_sys_id As HiddenField = DirectCast(item.FindControl("hidden_sysid"), HiddenField)
                        Dim hidden_idtname As HiddenField = DirectCast(item.FindControl("hidden_idtname"), HiddenField)
                        Dim sys_rmk As Label = DirectCast(item.FindControl("lbl_sys_rmk"), Label)
                        Dim list_idt As List(Of String) = New List(Of String)
                        Ds1 = Common.Get_office_apply_dtl_byUser(office_apply_id.Value, hid_sys_id.Value)
                        If Ds1.Tables(0).Rows.Count > 0 Then
                            sys_rmk.Text = Ds1.Tables(0).Rows(0).Item("rmk").ToString
                            For Each item2 As Control In idt_repeater.Controls
                                Dim chk_idt_name As CheckBox = DirectCast(item2.FindControl("chk_idt_name"), CheckBox)
                                Dim find_rows As DataRow() = Ds1.Tables(0).Select("idt_name like '%" + chk_idt_name.Text.Trim + "%'")
                                If find_rows.Length > 0 Then
                                    chk_idt_name.Checked = True
                                    list_idt.Add("," + chk_idt_name.Text)
                                End If
                            Next

                        End If
                        hidden_idtname.Value = String.Join("", list_idt.ToArray())
                    Next

                End If
                tb_result.Visible = True
                tb_query.Visible = False
                tr_gridview.Visible = False


        End Select

    End Sub

    Protected Sub but_print_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_print.Click
        Dim officeName As String = System.Web.Configuration.WebConfigurationManager.AppSettings("OfficeName")
        Dim doc As Document = New Document(PageSize.A4)
        Try
            HttpContext.Current.Response.ContentType = "application/pdf"
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.pdf")
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)

            Dim pdfDoc As New Document()
            Dim writer As PdfWriter = PdfWriter.GetInstance(pdfDoc, HttpContext.Current.Response.OutputStream)
            Dim baseFont As BaseFont = baseFont.CreateFont("C:\\Windows\\Fonts\\kaiu.ttf", baseFont.IDENTITY_H, baseFont.NOT_EMBEDDED)
            Dim font_14 As Font = New Font(baseFont, 14, Font.BOLD)
            Dim font_12 As Font = New Font(baseFont, 12)
            pdfDoc.Open()
            'WRITE PDF <<<<<<

            Dim table As PdfPTable = New PdfPTable(4)
            table.WidthPercentage = 97
            table.DefaultCell.Padding = 5

            Dim title As PdfPCell = New PdfPCell(New Paragraph("桃園市" + officeName + "地政事務所資訊系統使用者帳號申請表 ", font_14))
            title.Colspan = 4
            title.Padding = 5
            title.Border = 0
            title.HorizontalAlignment = Element.ALIGN_CENTER
            table.AddCell(title)
            table.AddCell(New Phrase("單位", font_12))
            table.AddCell(New Phrase(lbl_dept_name.Text, font_12))
            table.AddCell(New Phrase("申請原因", font_12))
            table.AddCell(New Phrase("年度檢討 ", font_12))
            table.AddCell(New Phrase("使用者姓名", font_12))
            table.AddCell(New Phrase(lbl_user_name.Text, font_12))
            table.AddCell(New Phrase("分機", font_12))
            table.AddCell(New Phrase(lbl_ext.Text, font_12))
            table.AddCell(New Phrase("職稱", font_12))
            Dim degreeCell As PdfPCell = New PdfPCell(New Paragraph(lbl_degree_name.Text, font_12))
            degreeCell.Colspan = 3
            degreeCell.Padding = 5
            table.AddCell(degreeCell)
            table.AddCell(New Phrase("業務項目", font_12))
            Dim itemCell As PdfPCell = New PdfPCell(New Paragraph(lbl_item.Text, font_12))
            itemCell.Colspan = 3
            table.AddCell(itemCell)
            table.AddCell(New Phrase("申請日期", font_12))
            table.AddCell(New Phrase(lbl_apply_date.Text, font_12))
            table.AddCell(New Phrase("啟用日期", font_12))
            table.AddCell(New Phrase(lbl_enable_date.Text, font_12))
            table.AddCell(New Phrase("使用期間", font_12))
            Dim startEndCell As PdfPCell = New PdfPCell(New Paragraph(lbl_start_date.Text + "至" + lbl_end_date.Text, font_12))
            startEndCell.Colspan = 3
            startEndCell.Padding = 5
            table.AddCell(startEndCell)
            table.AddCell(New Phrase("使用者代碼", font_12))
            table.AddCell(New Phrase(lbl_user_id.Text, font_12))
            table.AddCell(New Phrase("使用者密碼", font_12))
            table.AddCell(New Phrase("(預設值)", font_12))
            table.AddCell(New Phrase("保密切結", font_12))
            Dim ConCell As PdfPCell = New PdfPCell(New Paragraph("本人_____________於業務權限範圍內，將確實依本系統使用作業規定，查詢或列印相關資料，並遵守個人資料保護法、智慧財產權、著作權等相關規定，如有不當使用、資料外洩或違法者，應依法負民、刑事責任及行政責任。", font_12))
            ConCell.Padding = 5
            ConCell.Colspan = 3
            table.AddCell(ConCell)
            table.AddCell(New Phrase("註銷(停用)日期", font_12))
            table.AddCell(New Phrase(lbl_disable_date.Text, font_12))
            table.AddCell(New Phrase("註銷(停用)原因", font_12))
            table.AddCell(New Phrase(lbl_disable_reason.Text, font_12))
            pdfDoc.Add(table)

            'Dim widths() As Single = {15.0F, 15.0F, 15.0F, 15.0F, 5.0F, 15.0F, 5.0F, 15.0F}

            'Dim table3 As PdfPTable = New PdfPTable(widths)
            Dim table3 As PdfPTable = New PdfPTable(3)
            table3.WidthPercentage = 97
            table3.DefaultCell.Padding = 5

            table3.AddCell(New Phrase("申請人", font_12))
            table3.AddCell(New Phrase("系統管理者", font_12))
            table3.AddCell(New Phrase("業務課課長", font_12))
            Dim cell As PdfPCell = New PdfPCell()
            cell.FixedHeight = 60
            table3.AddCell(cell)
            table3.AddCell(cell)
            table3.AddCell(cell)
            table3.AddCell(New Phrase("資訊課課長", font_12))
            table3.AddCell(New Phrase("秘書", font_12))
            table3.AddCell(New Phrase("主任", font_12))
            table3.AddCell(cell)
            table3.AddCell(cell)
            table3.AddCell(cell)
            'table3.AddCell(New Phrase("申請人", font_12
            'table3.AddCell(New Phrase(" ", font_12))
            'table3.AddCell(New Phrase("業務課課長", font_12))
            'table3.AddCell(New Phrase(" ", font_12))
            'Dim scCell As PdfPCell = New PdfPCell(New Paragraph("秘書", font_12))
            'scCell.Padding = 5
            'scCell.Rowspan = 2
            'table3.AddCell(scCell)
            'Dim scECell As PdfPCell = New PdfPCell(New Paragraph(" ", font_12))
            'scECell.Padding = 5
            'scECell.Rowspan = 2
            'table3.AddCell(scECell)
            'Dim mCell As PdfPCell = New PdfPCell(New Paragraph("主任", font_12))
            'mCell.Padding = 5
            'mCell.Rowspan = 2
            'table3.AddCell(mCell)
            'Dim mECell As PdfPCell = New PdfPCell(New Paragraph(" ", font_12))
            'mECell.Padding = 5
            'mECell.Rowspan = 2
            'table3.AddCell(scECell)
            'table3.AddCell(New Phrase("系統管理者", font_12))
            'table3.AddCell(New Phrase(" ", font_12))
            'table3.AddCell(New Phrase("資訊課課長", font_12))
            'table3.AddCell(New Phrase(" ", font_12))
            pdfDoc.Add(table3)

            Dim table2 As PdfPTable = New PdfPTable(3)
            table2.WidthPercentage = 97
            table2.DefaultCell.Padding = 4
            Dim title2 As PdfPCell = New PdfPCell(New Paragraph("附表：系統授權範圍", font_14))
            title2.Colspan = 3
            title2.HorizontalAlignment = Element.ALIGN_CENTER
            title2.Padding = 5
            title2.Border = 0
            table2.AddCell(title2)
            Dim header1 As PdfPCell = New PdfPCell(New Phrase("授權範圍", font_12))
            header1.BackgroundColor = BaseColor.LIGHT_GRAY
            header1.Padding = 5
            header1.HorizontalAlignment = Element.ALIGN_CENTER
            Dim header2 As PdfPCell = New PdfPCell(New Phrase("權限", font_12))
            header2.BackgroundColor = BaseColor.LIGHT_GRAY
            header2.Padding = 5
            header2.HorizontalAlignment = Element.ALIGN_CENTER
            Dim header3 As PdfPCell = New PdfPCell(New Phrase("備註", font_12))
            header3.BackgroundColor = BaseColor.LIGHT_GRAY
            header3.Padding = 5
            header3.HorizontalAlignment = Element.ALIGN_CENTER
            table2.AddCell(header1)
            table2.AddCell(header2)
            table2.AddCell(header3)

            For Each item As Control In Repeater1.Controls
                Dim idt_repeater As Repeater = DirectCast(item.FindControl("ChildRepeater"), Repeater)
                Dim hidden_idtname As HiddenField = DirectCast(item.FindControl("hidden_idtname"), HiddenField)
                Dim sys_rmk As Label = DirectCast(item.FindControl("lbl_sys_rmk"), Label)
                Dim sys_name As Label = DirectCast(item.FindControl("lbl_sys_name"), Label)
                Dim list_idt As List(Of String) = New List(Of String)


                Dim selectGroup As PdfFormField = PdfFormField.CreateEmpty(writer)


                For Each item2 As Control In idt_repeater.Controls
                    Dim chk_idt_name As CheckBox = DirectCast(item2.FindControl("chk_idt_name"), CheckBox)
                    If hidden_idtname.Value.IndexOf(chk_idt_name.Text) <> -1 Then
                        list_idt.Add(chk_idt_name.Text)
                    End If
                Next
                table2.AddCell(New Phrase(sys_name.Text, font_12))
                table2.AddCell(New Phrase(String.Join(",", list_idt.ToArray()), font_12))
                table2.AddCell(New Phrase(sys_rmk.Text, font_12))
            Next
            pdfDoc.Add(table2)

            'END WRITE PDF >>>>>
            pdfDoc.Close()

            HttpContext.Current.Response.Write(pdfDoc)
            HttpContext.Current.Response.End()


        Catch ex As Exception

        Finally
            doc.Close()
        End Try

    End Sub

    Protected Sub but_esc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_esc.Click
        tb_result.Visible = False
        tb_query.Visible = True
        tr_gridview.Visible = True
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'Dim applydate As Label = DirectCast(e.Row.FindControl("lb_applydate"), Label)
            'If applydate.Text.Length = 7 Then
            '    applydate.Text = applydate.Text.Substring(0, 3) + "/" + applydate.Text.Substring(3, 2) + "/" + applydate.Text.Substring(5, 2)
            'End If
        End If
    End Sub
    Private Sub ddlbind_year(ByVal ddl As DropDownList)
        Dim startyear As Integer = Now.Year - 1911 + 3
        For i As Integer = 6 To 1 Step -1
            ddl.Items.Insert(0, startyear)
            startyear = startyear - 1
        Next
        ddl.SelectedValue = Now.Year - 1911
    End Sub

    Protected Sub btn_sel_print_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_sel_print.Click
        Dim officeName As String = System.Web.Configuration.WebConfigurationManager.AppSettings("OfficeName")
        '今日(民國年)
        Dim yyymmdd As String = (Now.Year - 1911).ToString.PadLeft(3, "0") + DateTime.Now.ToString("MMdd")
        Dim list_office_apply_id As List(Of String) = New List(Of String)
        For Each row As GridViewRow In GridView1.Rows
            Dim result As Boolean = DirectCast(row.FindControl("CheckBox1"), CheckBox).Checked
            If result = True Then
                Dim office_apply_id As HiddenField = (TryCast(row.FindControl("office_apply_id"), HiddenField))
                Dim userid As Label = DirectCast(row.FindControl("lb_userId"), Label)
                list_office_apply_id.Add("'" + office_apply_id.Value + "'")
            End If
        Next
        If list_office_apply_id.Count = 0 Then
            Common.showMsg(Me.Page, "尚未勾選使用者")
            Return
        End If
        Dim Ds, Ds1 As New DataSet
        Ds = Common.Get_office_apply_byUser(String.Join(",", list_office_apply_id.ToArray), dp)
        If Ds.Tables(0).Rows.Count > 0 Then

            Dim pdfDoc As New Document()
            Try
                HttpContext.Current.Response.ContentType = "application/pdf"
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.pdf")
                HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)

                Dim writer As PdfWriter = PdfWriter.GetInstance(pdfDoc, HttpContext.Current.Response.OutputStream)
                Dim baseFont As BaseFont = baseFont.CreateFont("C:\\Windows\\Fonts\\kaiu.ttf", baseFont.IDENTITY_H, baseFont.NOT_EMBEDDED)
                Dim font_14 As Font = New Font(baseFont, 14, Font.BOLD)
                Dim font_12 As Font = New Font(baseFont, 12)
                pdfDoc.Open()
                'WRITE PDF <<<<<<
                Dim arr As ArrayList = New System.Collections.ArrayList() '過濾重複資料
                For Each Dr As DataRow In Ds.Tables(0).Rows
                    If Not arr.Contains(Dr.Item("user_id")) Then
                        arr.Add(Dr.Item("user_id"))
                        Dim start_date As String = (Now.Year - 1911 + 1).ToString.PadLeft(3, "0") + "0101"
                        Dim end_date As String = (Now.Year - 1911 + 1).ToString.PadLeft(3, "0") + "1231"

                        Dim table As PdfPTable = New PdfPTable(4)
                        table.WidthPercentage = 97
                        table.DefaultCell.Padding = 5

                        Dim title As PdfPCell = New PdfPCell(New Paragraph("桃園市" + officeName + "地政事務所資訊系統使用者帳號申請表 ", font_14))
                        title.Colspan = 4
                        title.Padding = 5
                        title.Border = 0
                        title.HorizontalAlignment = Element.ALIGN_CENTER
                        table.AddCell(title)
                        table.AddCell(New Phrase("單位", font_12))
                        table.AddCell(New Phrase(Dr.Item("dept_name"), font_12))
                        table.AddCell(New Phrase("申請原因", font_12))
                        table.AddCell(New Phrase("年度檢討 ", font_12))
                        table.AddCell(New Phrase("使用者姓名", font_12))
                        table.AddCell(New Phrase(Dr.Item("user_name"), font_12))
                        table.AddCell(New Phrase("分機", font_12))
                        table.AddCell(New Phrase(Dr.Item("ext"), font_12))
                        table.AddCell(New Phrase("職稱", font_12))
                        Dim degreeCell As PdfPCell = New PdfPCell(New Paragraph(Dr.Item("degree_name"), font_12))
                        degreeCell.Colspan = 3
                        degreeCell.Padding = 5
                        table.AddCell(degreeCell)
                        table.AddCell(New Phrase("業務項目", font_12))
                        Dim itemCell As PdfPCell = New PdfPCell(New Paragraph(" ", font_12))
                        itemCell.Colspan = 3
                        table.AddCell(itemCell)
                        table.AddCell(New Phrase("申請日期", font_12))
                        table.AddCell(New Phrase(yyymmdd.Substring(0, 3) + "年" + yyymmdd.Substring(3, 2) + "月" + yyymmdd.Substring(5) + "日", font_12))
                        table.AddCell(New Phrase("啟用日期", font_12))
                        table.AddCell(New Phrase(" ", font_12))
                        table.AddCell(New Phrase("使用期間", font_12))
                        Dim startEndCell As PdfPCell = New PdfPCell(New Paragraph(start_date.Substring(0, 3) + "年" + start_date.Substring(3, 2) + "月" + start_date.Substring(5) + "日至" + end_date.Substring(0, 3) + "年" + end_date.Substring(3, 2) + "月" + end_date.Substring(5) + "日", font_12))
                        startEndCell.Colspan = 3
                        startEndCell.Padding = 5
                        table.AddCell(startEndCell)
                        table.AddCell(New Phrase("使用者代碼", font_12))
                        table.AddCell(New Phrase(Dr.Item("user_id"), font_12))
                        table.AddCell(New Phrase("使用者密碼", font_12))
                        table.AddCell(New Phrase("(預設值)", font_12))
                        table.AddCell(New Phrase("保密切結", font_12))
                        Dim ConCell As PdfPCell = New PdfPCell(New Paragraph("本人_____________於業務權限範圍內，將確實依本系統使用作業規定，查詢或列印相關資料，並遵守個人資料保護法、智慧財產權、著作權等相關規定，如有不當使用、資料外洩或違法者，應依法負民、刑事責任及行政責任。", font_12))
                        ConCell.Padding = 5
                        ConCell.Colspan = 3
                        table.AddCell(ConCell)
                        table.AddCell(New Phrase("註銷(停用)日期", font_12))
                        table.AddCell(New Phrase(" ", font_12))
                        table.AddCell(New Phrase("註銷(停用)原因", font_12))
                        table.AddCell(New Phrase(" ", font_12))
                        pdfDoc.Add(table)

                        'Dim widths() As Single = {15.0F, 15.0F, 15.0F, 15.0F, 5.0F, 15.0F, 5.0F, 15.0F}


                        'Dim table3 As PdfPTable = New PdfPTable(widths)
                        Dim table3 As PdfPTable = New PdfPTable(3)
                        table3.WidthPercentage = 97
                        table3.DefaultCell.Padding = 5

                        table3.AddCell(New Phrase("申請人", font_12))
                        table3.AddCell(New Phrase("系統管理者", font_12))
                        table3.AddCell(New Phrase("業務課課長", font_12))
                        Dim cell As PdfPCell = New PdfPCell()
                        cell.FixedHeight = 60
                        table3.AddCell(cell)
                        table3.AddCell(cell)
                        table3.AddCell(cell)
                        table3.AddCell(New Phrase("資訊課課長", font_12))
                        table3.AddCell(New Phrase("秘書", font_12))
                        table3.AddCell(New Phrase("主任", font_12))
                        table3.AddCell(cell)
                        table3.AddCell(cell)
                        table3.AddCell(cell)
                        'Dim scCell As PdfPCell = New PdfPCell(New Paragraph("秘書", font_12))
                        'scCell.Padding = 5
                        'scCell.Rowspan = 2
                        'table3.AddCell(scCell)
                        'Dim scECell As PdfPCell = New PdfPCell(New Paragraph(" ", font_12))
                        'scECell.Padding = 5
                        'scECell.Rowspan = 2
                        'table3.AddCell(scECell)
                        'table3.AddCell(cell)
                        'Dim mCell As PdfPCell = New PdfPCell(New Paragraph("主任", font_12))
                        'mCell.Padding = 5
                        'mCell.Rowspan = 2
                        'table3.AddCell(mCell)
                        'Dim mECell As PdfPCell = New PdfPCell(New Paragraph(" ", font_12))
                        'mECell.Padding = 5
                        'mECell.Rowspan = 2
                        'table3.AddCell(scECell)


                        'table3.AddCell(New Phrase("申請人", font_12))
                        'table3.AddCell(New Phrase(" ", font_12))
                        'table3.AddCell(cell)
                        'table3.AddCell(New Phrase("業務課課長", font_12))
                        'table3.AddCell(New Phrase(" ", font_12))
                        'table3.AddCell(cell)
                        'Dim scCell As PdfPCell = New PdfPCell(New Paragraph("秘書", font_12))
                        'scCell.Padding = 5
                        'scCell.Rowspan = 2
                        'table3.AddCell(scCell)
                        'Dim scECell As PdfPCell = New PdfPCell(New Paragraph(" ", font_12))
                        'scECell.Padding = 5
                        'scECell.Rowspan = 2
                        'table3.AddCell(scECell)
                        'table3.AddCell(cell)
                        'Dim mCell As PdfPCell = New PdfPCell(New Paragraph("主任", font_12))
                        'mCell.Padding = 5
                        'mCell.Rowspan = 2
                        'table3.AddCell(mCell)
                        'Dim mECell As PdfPCell = New PdfPCell(New Paragraph(" ", font_12))
                        'mECell.Padding = 5
                        'mECell.Rowspan = 2
                        'table3.AddCell(scECell)
                        'table3.AddCell(cell)
                        'table3.AddCell(New Phrase("系統管理者", font_12))
                        'table3.AddCell(New Phrase(" ", font_12))
                        'table3.AddCell(cell)
                        'table3.AddCell(New Phrase("資訊課課長", font_12))
                        'table3.AddCell(New Phrase(" ", font_12))
                        'table3.AddCell(cell)
                        pdfDoc.Add(table3)

                        Dim table2 As PdfPTable = New PdfPTable(3)
                        table2.WidthPercentage = 97
                        table2.DefaultCell.Padding = 4
                        Dim title2 As PdfPCell = New PdfPCell(New Paragraph("附表：系統授權範圍", font_14))
                        title2.Colspan = 3
                        title2.HorizontalAlignment = Element.ALIGN_CENTER
                        title2.Padding = 5
                        title2.Border = 0
                        table2.AddCell(title2)
                        Dim header1 As PdfPCell = New PdfPCell(New Phrase("授權範圍", font_12))
                        header1.BackgroundColor = BaseColor.LIGHT_GRAY
                        header1.Padding = 5
                        header1.HorizontalAlignment = Element.ALIGN_CENTER
                        Dim header2 As PdfPCell = New PdfPCell(New Phrase("權限", font_12))
                        header2.BackgroundColor = BaseColor.LIGHT_GRAY
                        header2.Padding = 5
                        header2.HorizontalAlignment = Element.ALIGN_CENTER
                        Dim header3 As PdfPCell = New PdfPCell(New Phrase("備註", font_12))
                        header3.BackgroundColor = BaseColor.LIGHT_GRAY
                        header3.Padding = 5
                        header3.HorizontalAlignment = Element.ALIGN_CENTER
                        table2.AddCell(header1)
                        table2.AddCell(header2)
                        table2.AddCell(header3)
                        For Each item As Control In Repeater1.Controls
                            Dim idt_repeater As Repeater = DirectCast(item.FindControl("ChildRepeater"), Repeater)
                            Dim hid_sys_id As HiddenField = DirectCast(item.FindControl("hidden_sysid"), HiddenField)
                            Dim lbl_sys_name As Label = DirectCast(item.FindControl("lbl_sys_name"), Label)
                            Dim sys_rmk = ""
                            Dim list_idt As List(Of String) = New List(Of String)
                            Ds1 = Common.Get_office_apply_dtl_byUser(Dr.Item("office_apply_id"), hid_sys_id.Value)
                            If Ds1.Tables(0).Rows.Count > 0 Then
                                sys_rmk = Ds1.Tables(0).Rows(0).Item("rmk").ToString
                                For Each item2 As Control In idt_repeater.Controls
                                    Dim chk_idt_name As CheckBox = DirectCast(item2.FindControl("chk_idt_name"), CheckBox)
                                    Dim find_rows As DataRow() = Ds1.Tables(0).Select("idt_name like '%" + chk_idt_name.Text + "%'")
                                    If find_rows.Length > 0 Then
                                        list_idt.Add(chk_idt_name.Text)
                                    End If
                                Next
                            End If
                            table2.AddCell(New Phrase(lbl_sys_name.Text, font_12))
                            table2.AddCell(New Phrase(String.Join(",", list_idt.ToArray()), font_12))
                            table2.AddCell(New Phrase(sys_rmk, font_12))
                        Next
                        pdfDoc.Add(table2)
                        pdfDoc.NewPage()
                    End If
                Next
                'END WRITE PDF >>>>>
                pdfDoc.Close()

                HttpContext.Current.Response.Write(pdfDoc)
                HttpContext.Current.Response.End()

            Catch ex As Exception
                Throw ex
            Finally
                pdfDoc.Close()
            End Try

        End If
    End Sub

    Protected Sub btn_all_print_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_all_print.Click
        Dim Ds, Ds1, Ds2 As New DataSet
        Ds = Common.Get_office_apply_byUser("", dp)
        Dim tmpTable As DataTable = Ds.Tables(0).DefaultView.ToTable(True, "dept_name")
        Try
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.Buffer = True
            HttpContext.Current.Response.ContentType = "application/ms-excel"
            HttpContext.Current.Response.Write("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls")

            HttpContext.Current.Response.Charset = "utf-8"
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("big5")
            'sets font
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>")
            HttpContext.Current.Response.Write("<BR><BR><BR>")
            'sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " + "borderColor='#000000' cellSpacing='0' cellPadding='0' " + "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>")
            'am getting my grid's column headers
            'int columnscount = GridView_Result.Columns.Count;

            HttpContext.Current.Response.Write("<Td>")
            'Get column headers  and make it as bold in excel columns
            HttpContext.Current.Response.Write("<B>")
            HttpContext.Current.Response.Write("姓名")
            HttpContext.Current.Response.Write("</B>")
            HttpContext.Current.Response.Write("</Td>")

            HttpContext.Current.Response.Write("<Td>")
            'Get column headers  and make it as bold in excel columns
            HttpContext.Current.Response.Write("<B>")
            HttpContext.Current.Response.Write("帳號")
            HttpContext.Current.Response.Write("</B>")
            HttpContext.Current.Response.Write("</Td>")

            Ds1 = Common.Get_auth_sys(approvalid, dp)
            For Each Dr1 As DataRow In Ds1.Tables(0).Rows()
                'write in new column
                HttpContext.Current.Response.Write("<Td>")
                'Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>")
                HttpContext.Current.Response.Write(Dr1.Item("sys_name"))
                HttpContext.Current.Response.Write("</B>")
                HttpContext.Current.Response.Write("</Td>")

            Next

            HttpContext.Current.Response.Write("</TR>")

            For Each Dr As DataRow In tmpTable.Rows
                'write in new column
                HttpContext.Current.Response.Write("<TR>")
                HttpContext.Current.Response.Write("<Td bgcolor='#00DDAA'>")
                'Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>")
                HttpContext.Current.Response.Write(Dr.Item("dept_name"))
                HttpContext.Current.Response.Write("</B>")
                HttpContext.Current.Response.Write("</Td>")
                HttpContext.Current.Response.Write("</TR>")

                Dim find_rows As DataRow() = Ds.Tables(0).Select("dept_name='" + Dr.Item("dept_name") + "'")
                Dim arr As ArrayList = New System.Collections.ArrayList() '過濾重複資料

                For Each row As DataRow In find_rows
                    If Not arr.Contains(row.Item("user_id")) Then
                        arr.Add(row.Item("user_id"))
                        HttpContext.Current.Response.Write("<TR>")
                        HttpContext.Current.Response.Write("<Td>")
                        'Get column headers  and make it as bold in excel columns
                        'HttpContext.Current.Response.Write("<B>")
                        HttpContext.Current.Response.Write(row.Item("user_name"))
                        'HttpContext.Current.Response.Write("</B>")
                        HttpContext.Current.Response.Write("</Td>")

                        HttpContext.Current.Response.Write("<Td>")
                        'Get column headers  and make it as bold in excel columns
                        'HttpContext.Current.Response.Write("<B>")
                        HttpContext.Current.Response.Write(row.Item("user_id"))
                        'HttpContext.Current.Response.Write("</B>")
                        HttpContext.Current.Response.Write("</Td>")

                        For Each Dr1 As DataRow In Ds1.Tables(0).Rows()
                            Ds2 = Common.Get_office_apply_dtl_byUser(row.Item("office_apply_id"), Dr1.Item("sys_id"))
                            Dim sel As String = ""
                            If Ds2.Tables(0).Rows.Count > 0 Then
                                sel = "V"
                            End If
                            'write in new column
                            HttpContext.Current.Response.Write("<Td>")
                            'Get column headers  and make it as bold in excel columns
                            HttpContext.Current.Response.Write("<B>")
                            HttpContext.Current.Response.Write(sel)
                            HttpContext.Current.Response.Write("</B>")
                            HttpContext.Current.Response.Write("</Td>")

                        Next
                        HttpContext.Current.Response.Write("</TR>")
                    End If
                Next
            Next


            HttpContext.Current.Response.Write("</Table>")
            HttpContext.Current.Response.Write("</font>")
            HttpContext.Current.Response.Flush()
            HttpContext.Current.Response.[End]()            'WRITE EXCEL <<<<<<


            'HttpContext.Current.Response.Write("\n")
            'HttpContext.Current.Response.End()


        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub
   
End Class
