﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI

Partial Class SYSIO_rec_flow
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlCmd As SqlCommand
    Dim uid As String = User.Identity.Name '使用者名稱
    Dim uname As String = Common.Get_User_Name(User.Identity.Name.ToString) '使用者代號
    Dim SqlTxt As String
    Dim approvalid As String
    Dim apprvid As String
    Dim dp As String = Me.Get_DP
    Dim File_Path As String = ConfigurationManager.AppSettings("SYSIO_Path")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        approvalid = ConfigurationManager.AppSettings("flow_SYSIO_rec") '11
        apprvid = ConfigurationManager.AppSettings("flow_SYSIO_rec") '11
        Gdv1Show() '待審核表
        If Not IsPostBack Then
            Time_DataSource() '進入時間的時間下拉資料
        End If
    End Sub

    '待審核表來源
    Protected Sub Gdv1Show()
        Try
            Dim sql = "select g.applyDate,k.recordId,k.SYSIO_recordId  from SYSIO g , ( select b.SYSIO_recordId, b.recordId from  form_record a join SYSIO_rec b on  a.recordid=b.recordid where b.using = 'Y' and approvalid='" + approvalid + "' and a.status=1 and userid='" + uid + "' ) k where g.recordId = k.SYSIO_recordId order by applyDate"
            Dim data As DataTable = Common.Con(sql)
            GridView1.DataSource = data
            GridView1.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '簽核過程及民國年轉換
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '簽核過程
                Dim recordid As String = CType(e.Row.FindControl("lb_rowid"), Label).Text
                CType(e.Row.FindControl("lb_flow"), Label).Text = Common.Str_form_record_isms(approvalid, recordid)
                '民國年轉換
                Dim lb_applydate As String = CType(e.Row.FindControl("lb_applydate"), Label).Text
                CType(e.Row.FindControl("lb_applydate"), Label).Text = Common.Year(Date.Parse(lb_applydate))
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '審核表畫面至審核畫面_1
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Try
            Dim Index As Integer = CType(e.CommandArgument, Int32)
            Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
            Dim lb_rowid As String = (TryCast(SelectedRow.FindControl("lb_rowid"), Label)).Text

            Select Case e.CommandName
                Case "audit_btn"
                    Dim sql = "  SELECT * FROM SYSIO_rec WHERE recordId = '" + lb_rowid + "'"
                    Dim data As DataTable = Common.Con(sql)
                    If data.Rows.Count > 0 Then
                        '紀錄該表單編號
                        lb_recordid.Text = lb_rowid
                        '載入關卡表
                        GdvShow()

                        '載入資料
                        sql = "select * from SYSIO where recordId = '" + data.Rows(0)("SYSIO_recordId") + "'"
                        Dim data2 = Common.Con(sql)
                        If Not data2.Rows.Count = 0 Then
                            applyDate.Text = Common.Year(data2.Rows(0)("applyDate")) '申請日期
                            SYSIO_recordId.Text = data2.Rows(0)("recordId") '表單編號
                            chk_target_1.Checked = If(data2.Rows(0)("target_1") = "1", True, False) '維護標的_資訊系統
                            chk_target_2.Checked = If(data2.Rows(0)("target_2") = "1", True, False) '維護標的_資訊設備
                            targetName.Text = data2.Rows(0)("targetName") '標的名稱
                            applyOrg.Text = data2.Rows(0)("applyOrg") '申請單位
                            applyName.Text = data2.Rows(0)("applyName") '申請人員
                            chk_method_1.Checked = If(data2.Rows(0)("method_1") = "1", True, False) '維護方式_親至機房
                            chk_method_2.Checked = If(data2.Rows(0)("method_2") = "1", True, False) '維護方式_親至資訊(電腦)室
                            chk_method_3.Checked = If(data2.Rows(0)("method_3") = "1", True, False) '維護方式_Internet連線
                            chk_method_4.Checked = If(data2.Rows(0)("method_4") = "1", True, False) '維護方式_其他
                            methodOther.Text = data2.Rows(0)("methodOther") '維護方式_其他內容
                            chk_location_1.Checked = If(data2.Rows(0)("location_1") = "1", True, False) '存放位置_機房
                            chk_location_2.Checked = If(data2.Rows(0)("location_2") = "1", True, False) '存放位置_辦公(控制)室
                            chk_location_3.Checked = If(data2.Rows(0)("location_3") = "1", True, False) '存放位置_其他
                            locationOther.Text = data2.Rows(0)("locationOther") '存放位置_其他內容
                            InDate.Text = Common.Year(data2.Rows(0)("InDate")) '進出日期
                            InTime.Text = data2.Rows(0)("InTimeHr") + "：" + data2.Rows(0)("InTimeMin") '進出時間_進時'進出時間_進分
                            OutTime.Text = data2.Rows(0)("OutTimeHr") + "：" + data2.Rows(0)("OutTimeMin") '離開時間_出時'離開時間_出分
                            applyComment.Text = data2.Rows(0)("applyComment") '辦理事項
                            chk_IfChangeSoftware.Checked = If(data2.Rows(0)("IfChangeSoftware") = "1", True, False) '變更程式
                            chk_IfSYSPG.Checked = If(data2.Rows(0)("IfSYSPG") = "1", True, False) '軟體程式變更申請單
                            chk_IfFirewall.Checked = If(data2.Rows(0)("IfFirewall") = "1", True, False) '防火牆維護申請表
                            chk_IfTest.Checked = If(data2.Rows(0)("IfTest") = "1", True, False) '測試作業之程式
                            chk_IfItemIn.Checked = If(data2.Rows(0)("IfItemIn") = "1", True, False) '攜入
                            chk_IfItemOut.Checked = If(data2.Rows(0)("IfItemOut") = "1", True, False) '攜出
                            chk_IfItemLocation.Checked = If(data2.Rows(0)("IfItemLocation") = "1", True, False) '變更位置

                            '載入選擇
                            If chk_IfSYSPG.Checked = "1" Then
                                Dim sql_related_syspgrecordId = "select * from SYSIO_related where SYSIO_recordId = '" + lb_rowid + "' and kind = 'PG'"
                                Dim data_related = Common.Con(sql_related_syspgrecordId)
                                If Not data_related.Rows.Count = 0 Then
                                    SYSPG_recordId2.Text = data_related.Rows(0)("related_recordId") 'SYSPG_recordId
                                End If
                            Else
                                SYSPG_recordId2.Text = ""
                            End If
                            If chk_IfFirewall.Checked = "1" Then
                                Dim sql_related_firewallrecordId = "select * from SYSIO_related where SYSIO_recordId = '" + lb_rowid + "' and kind = 'FW'"
                                Dim data_related = Common.Con(sql_related_firewallrecordId)
                                If Not data_related.Rows.Count = 0 Then
                                    Firewall_recordId2.Text = data_related.Rows(0)("related_recordId") 'Firewall_recordId
                                End If
                            Else
                                Firewall_recordId2.Text = ""
                            End If

                            '檢測結果
                            Dim sql_exam = "  select * from SYSIO_exam where SYSIO_recordId = '" + lb_rowid + "' "
                            Dim data_exam = Common.Con(sql_exam)
                            If data_exam.Rows.Count = 0 Then
                                GdvShow1()
                            Else
                                GridView2.DataSource = data_exam
                                GridView2.DataBind()
                                Session("newTable") = data_exam '紀錄
                            End If

                            '查看裡面是否有資料，有才秀出來
                            If IsDBNull(data2.Rows(0)("applyFile")) Then
                                applyFile.Visible = False
                            Else
                                applyFile.Visible = True
                                applyFile_link.Text = data2.Rows(0)("applyFile")
                            End If
                        End If




                        '主資料
                        action.Text = data.Rows(0)("action")
                        actionTime.Text = Common.Year(data.Rows(0)("actionTime"))
                        Ddl_InHour.SelectedValue = data.Rows(0)("InTimeHr")
                        Ddl_InMin.SelectedValue = data.Rows(0)("InTimeMin")
                        Ddl_OutHour.SelectedValue = data.Rows(0)("OutTimeHr")
                        Ddl_OutMin.SelectedValue = data.Rows(0)("OutTimeMin")

                        '配對編號
                        Dim sql_related_syspgrecordId2 = "select * from SYSIO_related where SYSIO_recordId = '" + lb_rowid + "' and kind = 'PG'"
                        Dim data_related_syspg = Common.Con(sql_related_syspgrecordId2)
                        If Not data_related_syspg.Rows.Count = 0 Then
                            SYSPG_recordId2.Text = data_related_syspg.Rows(0)("related_recordId") 'SYSPG_recordId
                        End If

                        Dim sql_related_firewallrecordId2 = "select * from SYSIO_related where SYSIO_recordId = '" + lb_rowid + "' and kind = 'FW'"
                        Dim data_related_firewall = Common.Con(sql_related_firewallrecordId2)
                        If Not data_related_firewall.Rows.Count = 0 Then
                            Firewall_recordId2.Text = data_related_firewall.Rows(0)("related_recordId") 'Firewall_recordId
                        Else
                            Firewall_recordId2.Text = ""
                        End If

                        '查看裡面是否有資料，有才秀出來
                        If IsDBNull(data.Rows(0)("actionFile")) Then
                            actionFile.Visible = False
                        Else
                            actionFile.Visible = True
                            actionFile_link.Text = data.Rows(0)("actionFile")
                        End If

                        Dim workflowId As Integer = Common.Get_workflowId(approvalid, lb_rowid) '取得關卡

                        '如果是第0關
                        If workflowId = 0 Then
                            SheetEnabled(True)
                            File_actionFile.Visible = True
                        Else
                            SheetEnabled(False)
                            File_actionFile.Visible = False
                        End If


                        '將審核畫面弄出來
                        tb_result.Visible = True
                        tr_gridview.Visible = False
                        Insert_Btn.Visible = True
                        but_esc.Visible = True
                    Else
                        Response.Redirect("SYSIO_rec_flow.aspx", True) '重新載入附件表單簽核_審核 因為此表單編號已經不存在
                    End If


            End Select
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '小工具1
    Private Sub SheetEnabled(ByVal use As Boolean)
        action.Enabled = use
        btn_SYSPG.Visible = use
        btn_Firewall.Visible = use
        actionTime.Enabled = use
        Ddl_InHour.Enabled = use
        Ddl_InMin.Enabled = use
        Ddl_OutHour.Enabled = use
        Ddl_OutMin.Enabled = use
        File_actionFile.Visible = use
    End Sub

    '載入SYSIO的檢測結果
    Private Sub GdvShow1()
        Try
            Dim newTable As New DataTable

            newTable.Columns.Add("equipment")
            newTable.Columns.Add("quantity")
            newTable.Columns.Add("result")

            Session("newTable") = newTable '紀錄

            GridView2.DataSource = newTable
            GridView2.DataBind()

        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub


    '時間資料來源
    Private Sub Time_DataSource()
        Try
            '進入時間
            Ddl_InHour.Items.Clear()
            Ddl_InMin.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                Ddl_InHour.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                Ddl_InMin.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            '離開時間
            Ddl_OutHour.Items.Clear()
            Ddl_OutMin.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                Ddl_OutHour.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                Ddl_OutMin.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '按鈕_主頁至軟體程式變更申請單的"選擇"(1)
    Protected Sub btn_SYSPG_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_SYSPG.Click
        Try
            GdvTabel3(DateTime.Now.AddMonths(-1), DateTime.Now) '載入選擇表格的資料(SYSPG)
            Table1.Visible = False
            Table2_1.Visible = True
            Table3_1.Visible = True
            Table3_2.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_載入SYSPG
    Private Sub GdvTabel3(ByVal Startdate As Date, ByRef Enddate As Date)
        Try
            Dim sql As String = "select * from SYSPG where using = 'Y' and applyDate between '" + Startdate.ToString("yyyy/MM/dd") + "' and '" + Enddate.ToString("yyyy/MM/dd") + "'"
            sdate.Text = Common.Year(Startdate.ToString("yyyy/MM/dd"))
            edate.Text = Common.Year(Enddate.ToString("yyyy/MM/dd"))
            Dim data As DataTable = Common.Con(sql)
            For Each item In data.Select
                item("empName") = Common.Year(DateTime.Parse(item("applyDate")).ToString("yyyy/MM/dd"))
            Next
            GridView3.DataSource = data
            GridView3.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '按鈕_主頁至房活強維護申請表的"選擇"(2)
    Protected Sub btn_Firewall_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Firewall.Click
        Try
            GdvTabel4(DateTime.Now.AddMonths(-1), DateTime.Now) '載入選擇表格的資料(SYSPG)
            Table1.Visible = False
            Table2_1.Visible = True
            Table3_1.Visible = False
            Table3_2.Visible = True
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '2_載入Firewall
    Private Sub GdvTabel4(ByVal Startdate As Date, ByRef Enddate As Date)
        Try
            Dim sql As String = "select * from Firewall where using = 'Y' and applyDate between '" + Startdate.ToString("yyyy/MM/dd") + "' and '" + Enddate.ToString("yyyy/MM/dd") + "'"
            sdate.Text = Common.Year(Startdate.ToString("yyyy/MM/dd"))
            edate.Text = Common.Year(Enddate.ToString("yyyy/MM/dd"))
            Dim data As DataTable = Common.Con(sql)
            For Each item In data.Select
                item("empName") = Common.Year(DateTime.Parse(item("applyDate")).ToString("yyyy/MM/dd"))
            Next
            GridView4.DataSource = data
            GridView4.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '按鈕_搜尋表單編號日期區間
    Protected Sub Btn_applyDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_applyDate.Click
        Try
            If Table3_1.Visible Then
                GdvTabel3(DateTime.Parse(Common.YearYYYY(sdate.Text)), DateTime.Parse(Common.YearYYYY(edate.Text)))
            End If
            If Table3_2.Visible Then
                GdvTabel4(DateTime.Parse(Common.YearYYYY(sdate.Text)), DateTime.Parse(Common.YearYYYY(edate.Text)))
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_選取某表單編號_至主頁(SYSPG)
    Protected Sub GridView3_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView3.RowCommand
        Try
            Dim GETrecordId As String = e.CommandArgument
            SYSPG_recordId2.Text = GETrecordId '下面的軟體程式變更申請表編號

            Table1.Visible = True
            Table2_1.Visible = False
            Table3_1.Visible = False
            Table3_2.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '2_選取某表單編號_至主頁(Firewall)
    Protected Sub GridView4_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView4.RowCommand
        Try
            Dim GETrecordId As String = e.CommandArgument
            Firewall_recordId2.Text = GETrecordId '下面的防火牆維護申請表編號

            Table1.Visible = True
            Table2_1.Visible = False
            Table3_1.Visible = False
            Table3_2.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '關卡表
    Private Sub GdvShow()
        Try
            Dim dt As DataTable = Common.Qry_workflowName_isms_Req(approvalid, lb_recordid.Text, apprvid, dp)
            GridView.DataSource = dt
            GridView.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub



    '關卡表_RowDataBound
    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '***已簽核資料

                '***人員
                Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
                Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
                Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
                Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
                '***狀態
                Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
                Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
                Dim hid_status As HiddenField = CType(e.Row.FindControl("hid_status"), HiddenField)
                '***意見
                Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
                Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
                '***關卡***
                Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text
                '***審核人員***
                'Dim lb_wf_inside_order As String = CType(e.Row.FindControl("lb_wf_inside_order"), Label).Text

                Dim ds As DataSet


                '***Start
                If o_wf_order = "0" And hid_status.Value <> "1" Then
                    pan_user.Visible = True
                    '將審核人員設為自己
                    Dim sql = "    SELECT user_id, user_name FROM ismsUser WHERE user_id = '" + uid + "'"
                    Dim data As DataTable = Common.Con(sql)
                    CType(e.Row.FindControl("lb_username"), Label).Text = data.Rows(0)("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = data.Rows(0)("user_id")
                ElseIf hid_status.Value = "1" Then '待審核
                    ds = Common.Qry_form_record_wait(lb_recordid.Text)
                    pan_user.Visible = True
                    CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
                    lb_count.Text = e.Row.RowIndex
                ElseIf hid_status.Value = "2" Or hid_status.Value = "9" Then '審核已通過
                    ds = Common.Qry_form_record_name(lb_recordid.Text, o_wf_order)
                    pan_user.Visible = True
                    CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
                ElseIf e.Row.RowIndex = CType(lb_count.Text, Integer) + 1 Then '待審核下一關卡
                    ddl_user.Visible = True
                    ds = Common.Get_Wf_User(dp, apprvid, o_wf_order, "")
                    ddl_user.DataSource = ds
                    ddl_user.DataTextField = "name_id"
                    ddl_user.DataValueField = "wf_userid"
                    ddl_user.DataBind()
                End If

                If hid_status.Value = "1" Then '待審核
                    ddl_status.Visible = True
                    lb_status.Visible = False
                    txt_comment.Visible = True
                    lb_comment.Visible = False
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(1)
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()

                ElseIf hid_status.Value = "" Then
                    ddl_status.Visible = False
                    lb_status.Visible = True
                    txt_comment.Visible = False
                    lb_comment.Visible = True
                Else
                    ddl_status.Visible = False
                    lb_status.Visible = True
                    txt_comment.Visible = False
                    lb_comment.Visible = True
                    CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(hid_status.Value))
                End If
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '檔案下載_按鈕
    Protected Sub applyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + applyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '檔案下載_按鈕
    Protected Sub actionFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles actionFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + actionFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub


    '返回列表_按鈕
    Protected Sub but_esc_click()
        tr_gridview.Visible = True
        tb_result.Visible = False
        Insert_Btn.Visible = False
        but_esc.Visible = False
        Gdv1Show()
    End Sub

    '送出審核_按鈕
    Protected Sub Insert_Btn_click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim index As Integer = CType(lb_count.Text, Integer)
        Dim max_index As Integer = GridView.Rows.Count

        Dim wf_order As String = CType(GridView.Rows(index).FindControl("wf_order"), Label).Text '待審核的關卡
        Dim status As String = CType(GridView.Rows(index).FindControl("ddl_status"), DropDownList).SelectedValue '狀態
        Dim comment As String = CType(GridView.Rows(index).FindControl("txb_comment"), TextBox).Text
        Dim id As String = CType(GridView.Rows(index).FindControl("lb_id2"), Label).Text
        Dim stage As Integer = Common.Get_workflowId(approvalid, lb_recordid.Text) '取得關卡

        If status = 2 Then '通過
            If index + 1 < GridView.Rows.Count Then '流程尚未到最後關卡

                If stage = 0 Then

                    'String.IsNullOrEmpty()
                    If Not String.IsNullOrEmpty(actionTime.Text) Then
                        Dim inTime = DateTime.Parse(Common.YearYYYY(actionTime.Text + " " + Ddl_InHour.SelectedValue + ":" + Ddl_InMin.SelectedValue))
                        Dim outTime = DateTime.Parse(Common.YearYYYY(actionTime.Text + " " + Ddl_OutHour.SelectedValue + ":" + Ddl_OutMin.SelectedValue))
                        If inTime > outTime Then
                            Common.showMsg(Me.Page, "離開時間需大於進入時間")
                            Return
                        End If
                    End If

                    '回到第0關
                    If String.IsNullOrEmpty(action.Text) Or String.IsNullOrEmpty(actionTime.Text) Or String.IsNullOrEmpty(SYSIO_recordId.Text) Then
                        Common.showMsg(Me.Page, "請確實填寫資訊")
                        Return
                    End If

                    '處理上傳檔案
                    If CType(Me.FindControl("File_actionFile"), FileUpload).HasFile Then
                        '有檔案
                        Dim oriFileName = CType(Me.FindControl("File_actionFile"), FileUpload)
                        Dim UpFileName As String = Common.UpFile(File_Path, lb_recordid.Text, CType(Me.FindControl("File_actionFile"), FileUpload))
                        '塞入主表資料
                        SqlTxt += "  update SYSIO_rec set action = '" + action.Text + "', actionTime = '" + Common.YearYYYY(actionTime.Text) + "', InTimeHr = '" + Ddl_InHour.SelectedValue + "', InTimeMin = '" + Ddl_InMin.SelectedValue + "'," _
                            + "OutTimeHr = '" + Ddl_OutHour.SelectedValue + "', OutTimeMin = '" + Ddl_OutMin.SelectedValue + "', actionFile_ori_name = '" + oriFileName.FileName + "', actionFile = '" + UpFileName + "', op_time = getdate() where recordId = '" + lb_recordid.Text + "'"

                    Else
                        '沒有檔案
                        '塞入主表資料
                        SqlTxt += "  update SYSIO_rec set action = '" + action.Text + "', actionTime = '" + Common.YearYYYY(actionTime.Text) + "', InTimeHr = '" + Ddl_InHour.SelectedValue + "', InTimeMin = '" + Ddl_InMin.SelectedValue + "'," _
                            + "OutTimeHr = '" + Ddl_OutHour.SelectedValue + "', OutTimeMin = '" + Ddl_OutMin.SelectedValue + "', op_time = getdate() where recordId = '" + lb_recordid.Text + "'"

                    End If

                    '先刪掉原本的
                    SqlTxt += " delete SYSIO_related where SYSIO_recordId = '" + lb_recordid.Text + "'"
                    '處理軟體程式變更申請班編號、防火牆維護申請表編號
                    If Not String.IsNullOrEmpty(SYSPG_recordId2.Text) Then
                        SqlTxt += "insert into SYSIO_related (SYSIO_recordId,kind,related_recordId,original_SYSIO_recordId)values('" + lb_recordid.Text + "','PG','" + SYSPG_recordId2.Text + "','" + SYSIO_recordId.Text + "')"
                    End If

                    If Not String.IsNullOrEmpty(Firewall_recordId2.Text) Then
                        SqlTxt += "insert into SYSIO_related (SYSIO_recordId,kind,related_recordId,original_SYSIO_recordId)values('" + lb_recordid.Text + "','FW','" + Firewall_recordId2.Text + "','" + SYSIO_recordId.Text + "')"
                    End If

                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                End If


                Dim workflowid As String = CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) + 1
                Dim user_id = CType(GridView.Rows(index + 1).FindControl("ddl_user"), DropDownList).SelectedValue

                SqlTxt = " INSERT INTO form_record (approvalId,workflowId,userId,userType,status,comment,recordId,signDate,role,dp) VALUES "
                SqlTxt += " ('" + approvalid + "','" + workflowid + "','" + user_id + "','land','1','','" + lb_recordid.Text + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + workflowid + "','" + dp + "') "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            End If
            SqlTxt = " UPDATE form_record SET status='2' , signDate ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' , comment = '" + comment + "' WHERE status != 2 and status !=0 and approvalId='" + approvalid + "'and recordId ='" + lb_recordid.Text + "' and id ='" + id + "' "
        ElseIf status = 9 Then '退回
            If CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) > 0 Then '目前流程不是第一關卡
                Dim workflowid As String = CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) - 1
                'Dim user_id = CType(GridView.Rows(index - 1).FindControl("label_userid"), Label).Text
                Dim dt As DataTable = Common.Qry_form_record1(approvalid, lb_recordid.Text, workflowid)
                Dim user_id As String = dt.Rows(0).Item("userId").ToString

                SqlTxt = " INSERT INTO form_record (approvalId,workflowId,userId,userType,status,comment,recordId,signDate,role,dp) VALUES "
                SqlTxt += " ('" + approvalid + "','" + workflowid + "','" + user_id + "','land','1','','" + lb_recordid.Text + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + workflowid + "','" + dp + "') "

                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            Else
                Common.showMsg(Me.Page, "流程已到填表人，無法再退回！")
                Return
            End If

            SqlTxt = " UPDATE form_record SET status='9' , signDate ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' , comment = '" + comment + "' WHERE approvalId='" + approvalid + "'and recordId ='" + lb_recordid.Text + "' and id ='" + id + "' "

        End If
        Dim cmd As New SqlCommand(SqlTxt.ToString)

        Try
            Conn = Common.ConnDBS : Conn.Open() : cmd.Connection = Conn
            If cmd.ExecuteNonQuery() Then Common.showMsg(Me.Page, "審核完成！") : but_esc_click()
        Catch ex As Exception
            Common.showMsg(Me.Page, "審核失敗！")
        Finally
            Conn.Close() : Conn.Close() : Conn.Dispose() : cmd.Dispose()
        End Try
    End Sub

End Class
