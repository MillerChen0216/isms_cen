﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports System.Collections.Generic

Partial Class SYSIO
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim File_Path As String = ConfigurationManager.AppSettings("SYSIO_Path")
    Dim dp As String = Me.Get_DP
    Dim apprvid As String = ConfigurationManager.AppSettings("flow_SYSIO") '10
    Dim uid As String = User.Identity.Name
    Dim uname As String = Common.Get_User_Name(User.Identity.Name.ToString)


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        If Not IsPostBack Then
            If Session("success") = "Y" Then
                Common.showMsg(Me.Page, "儲存成功！")
                Session("success") = ""
            End If
            GdvShow() '載入流程表
            GdvShow1() '載入SYSIO的檢測結果
            Time_DataSource() '進入時間的時間下拉資料
        End If
    End Sub

    '載入流程表
    Private Sub GdvShow()
        Try
            Dim dt As DataTable = Common.Qry_workflowName_isms(apprvid, dp)
            GridView.DataSource = dt
            GridView.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料

            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            '***意見
            Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text

            '***Start
            Select Case o_wf_order
                Case "0"
                    '人員
                    pan_user.Visible = True : ddl_user.Visible = False : lb_userid.Text = uid
                    '狀態
                    ddl_status.Visible = True : lb_status.Visible = False
                    ddl_status.DataSource = Common.Get_WFStatus(0)
                    ddl_status.DataValueField = "wfstatus_id" : ddl_status.DataTextField = "wfstatus_name" : ddl_status.DataBind()
                    '意見
                    txt_comment.Visible = True : lb_comment.Visible = False
                Case "1"
                    Dim dt_WorkFlow As DataTable = Common.Get_Wf_User1_isms(apprvid, o_wf_order, "")
                    ddl_user.Visible = True : pan_user.Visible = False
                    ddl_user.Items.Clear()
                    ddl_user.DataValueField = "wf_userid" : ddl_user.DataTextField = "user_name"
                    ddl_user.DataSource = dt_WorkFlow : ddl_user.DataBind()
            End Select


            If pan_user.Visible = True And lb_userid.Text <> "" Then
                lb_username.Text = ISMS.Common.notNull(Common.Get_User_Name(lb_userid.Text))
                If lb_username.Text = "" Then lb_username.Text = Common.Get_venderemp_Name(lb_userid.Text) '表示非員工，可能為廠商
            End If
        End If
    End Sub


    '載入SYSIO的檢測結果(初始只有三筆空資料)
    Private Sub GdvShow1()
        Try
            Dim newTable As New DataTable

            newTable.Columns.Add("equipment")
            newTable.Columns.Add("quantity")
            newTable.Columns.Add("result")

            newTable.Rows.Add("", "", "")
            newTable.Rows.Add("", "", "")
            newTable.Rows.Add("", "", "")

            GridView1.DataSource = newTable
            GridView1.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '按鈕_增加檢測結果
    Protected Sub Btn_Addexam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Addexam.Click
        Try

            Dim newTable As New DataTable
            newTable.Columns.Add("equipment")
            newTable.Columns.Add("quantity")
            newTable.Columns.Add("result")

            Dim count = GridView1.Rows.Count
            For Item As Integer = 0 To count - 1 Step 1
                Dim equipment = CType(GridView1.Rows(Item).Cells(0).FindControl("equipment"), TextBox).Text
                Dim quantity = CType(GridView1.Rows(Item).Cells(0).FindControl("quantity"), TextBox).Text
                Dim result = CType(GridView1.Rows(Item).Cells(0).FindControl("result"), TextBox).Text
                newTable.Rows.Add(equipment, quantity, result)
            Next

            newTable.Rows.Add("", "", "")
            GridView1.DataSource = newTable
            GridView1.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub


    '時間資料來源
    Private Sub Time_DataSource()
        Try
            '進入時間
            Ddl_InHour.Items.Clear()
            Ddl_InMin.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                Ddl_InHour.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                Ddl_InMin.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            '離開時間
            Ddl_OutHour.Items.Clear()
            Ddl_OutMin.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                Ddl_OutHour.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                Ddl_OutMin.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '按鈕_主頁至軟體程式變更申請單的"選擇"(1)
    Protected Sub btn_SYSPG_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_SYSPG.Click
        Try
            GdvTabel2_2(DateTime.Now.AddMonths(-1), DateTime.Now) '載入選擇表格的資料(SYSPG)
            Table1.Visible = False
            Table2_1.Visible = True
            Table2_2.Visible = True
            Table2_3.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_載入SYSPG
    Private Sub GdvTabel2_2(ByVal Startdate As Date, ByRef Enddate As Date)
        Try
            Dim sql As String = "select * from SYSPG where using = 'Y' and applyDate between '" + Startdate.ToString("yyyy/MM/dd") + "' and '" + Enddate.ToString("yyyy/MM/dd") + "'"
            sdate.Text = Common.Year(Startdate.ToString("yyyy/MM/dd"))
            edate.Text = Common.Year(Enddate.ToString("yyyy/MM/dd"))
            Dim data As DataTable = Common.Con(sql)
            For Each item In data.Select
                item("empName") = Common.Year(DateTime.Parse(item("applyDate")).ToString("yyyy/MM/dd"))
            Next
            GridView2.DataSource = data
            GridView2.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '按鈕_主頁至房活強維護申請表的"選擇"(2)
    Protected Sub btn_Firewall_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Firewall.Click
        Try
            GdvTabel2_3(DateTime.Now.AddMonths(-1), DateTime.Now) '載入選擇表格的資料(SYSPG)
            Table1.Visible = False
            Table2_1.Visible = True
            Table2_2.Visible = False
            Table2_3.Visible = True
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '2_載入Firewall
    Private Sub GdvTabel2_3(ByVal Startdate As Date, ByRef Enddate As Date)
        Try
            Dim sql As String = "select * from Firewall where using = 'Y' and applyDate between '" + Startdate.ToString("yyyy/MM/dd") + "' and '" + Enddate.ToString("yyyy/MM/dd") + "'"
            sdate.Text = Common.Year(Startdate.ToString("yyyy/MM/dd"))
            edate.Text = Common.Year(Enddate.ToString("yyyy/MM/dd"))
            Dim data As DataTable = Common.Con(sql)
            For Each item In data.Select
                item("empName") = Common.Year(DateTime.Parse(item("applyDate")).ToString("yyyy/MM/dd"))
            Next
            GridView3.DataSource = data
            GridView3.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '按鈕_搜尋表單編號日期區間
    Protected Sub Btn_applyDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_applyDate.Click
        Try
            If Table2_2.Visible Then
                GdvTabel2_2(DateTime.Parse(Common.YearYYYY(sdate.Text)), DateTime.Parse(Common.YearYYYY(edate.Text)))
            Else
                GdvTabel2_3(DateTime.Parse(Common.YearYYYY(sdate.Text)), DateTime.Parse(Common.YearYYYY(edate.Text)))
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_選取某表單編號_至主頁(SYSPG)
    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView2.RowCommand
        Try
            Dim GETrecordId As String = e.CommandArgument
            SYSPG_recordId.Text = GETrecordId
            chk_IfSYSPG.Checked = True
            chk_IfChangeSoftware.Checked = True

            Table1.Visible = True
            Table2_1.Visible = False
            Table2_2.Visible = False
            Table2_3.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '2_選取某表單編號_至主頁(Firewall)
    Protected Sub GridView3_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView3.RowCommand
        Try
            Dim GETrecordId As String = e.CommandArgument
            Firewall_recordId.Text = GETrecordId
            chk_IfFirewall.Checked = True
            chk_IfChangeSoftware.Checked = True

            Table1.Visible = True
            Table2_1.Visible = False
            Table2_2.Visible = False
            Table2_3.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    

    '送出審核
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            'String.IsNullOrEmpty()
            If Not String.IsNullOrEmpty(InDate.Text) Then
                Dim inTime = DateTime.Parse(Common.YearYYYY(InDate.Text + " " + Ddl_InHour.SelectedValue + ":" + Ddl_InMin.SelectedValue))
                Dim outTime = DateTime.Parse(Common.YearYYYY(InDate.Text + " " + Ddl_OutHour.SelectedValue + ":" + Ddl_OutMin.SelectedValue))
                If inTime > outTime Then
                    Common.showMsg(Me.Page, "離開時間需大於進入時間")
                    Return
                End If
            End If


            If String.IsNullOrEmpty(applyDate.Text) Or (Not chk_target_1.Checked And Not chk_target_2.Checked) Or String.IsNullOrEmpty(targetName.Text) _
                Or String.IsNullOrEmpty(applyOrg.Text) Or String.IsNullOrEmpty(applyName.Text) _
                Or (Not chk_method_1.Checked And Not chk_method_2.Checked And Not chk_method_3.Checked And (Not chk_method_4.Checked Or String.IsNullOrEmpty(methodOther.Text))) _
                Or (Not chk_location_1.Checked And Not chk_location_2.Checked And (Not chk_location_3.Checked Or String.IsNullOrEmpty(locationOther.Text))) _
                Or String.IsNullOrEmpty(InDate.Text) Or String.IsNullOrEmpty(applyComment.Text) Then
                Common.showMsg(Me.Page, "請確實填寫資訊")
                Return
            End If


            If GridView.Rows.Count < 1 Then
                Common.showMsg(Me.Page, "請先至(系統管理->審核流程管理)設定關卡流程")
                Return
            End If

            Dim Now As String = Common.YearYYYY(applyDate.Text) '日期
            Dim applyDate_Chn As String = Common.Year(Now).Replace("/", "") '民國YYYMMDD
            Dim sql = "SELECT * FROM SYSIO WHERE applyDate_Chn = '" + applyDate_Chn + "' ORDER BY recordId DESC"
            Dim data As DataTable = Common.Con(sql)
            Dim applyDate_Chn_SN = 1
            If Not (data.Rows.Count = 0) Then
                applyDate_Chn_SN = Integer.Parse(data.Rows(0)("applyDate_Chn_SN")) + 1 'SN
            End If


            Dim recordid As String = "A1-" + applyDate_Chn + "-" + applyDate_Chn_SN.ToString.PadLeft(3, "0") '表單編號

            '處理勾取資料
            Dim target_1 = If(chk_target_1.Checked, "1", "0")
            Dim target_2 = If(chk_target_2.Checked, "1", "0")

            Dim method_1 = If(chk_method_1.Checked, "1", "0")
            Dim method_2 = If(chk_method_2.Checked, "1", "0")
            Dim method_3 = If(chk_method_3.Checked, "1", "0")
            Dim method_4 = If(chk_method_4.Checked, "1", "0")
            Dim method_other = If(chk_method_4.Checked, methodOther.Text, "")

            Dim location_1 = If(chk_location_1.Checked, "1", "0")
            Dim location_2 = If(chk_location_2.Checked, "1", "0")
            Dim location_3 = If(chk_location_3.Checked, "1", "0")
            Dim location_other = If(chk_location_3.Checked, locationOther.Text, "")

            Dim IfChangeSoftware = If(chk_IfChangeSoftware.Checked, "1", "0")
            Dim IfSYSPG = If(Not String.IsNullOrEmpty(SYSPG_recordId.Text) And chk_IfSYSPG.Checked, "1", "0")
            Dim IfFirewall = If(Not String.IsNullOrEmpty(Firewall_recordId.Text) And chk_IfFirewall.Checked, "1", "0")
            Dim IfTest = If(chk_IfTest.Checked, "1", "0")

            Dim IfItemIn = If(chk_IfItemIn.Checked, "1", "0")
            Dim IfItemOut = If(chk_IfItemOut.Checked, "1", "0")
            Dim IfItemLocation = If(chk_IfItemLocation.Checked, "1", "0")
            'insert into SYSIO_exam (recordId,SYSIO_recordId,equipment,quantity,result,create_time,op_time)values('','','','','','','')

            Dim count = GridView1.Rows.Count
            Dim sql_Forexam = " "
            Dim time = DateTime.Now
            For Item As Integer = 0 To count - 1 Step 1
                Dim equipment = CType(GridView1.Rows(Item).Cells(0).FindControl("equipment"), TextBox).Text
                Dim quantity = CType(GridView1.Rows(Item).Cells(0).FindControl("quantity"), TextBox).Text
                Dim result = CType(GridView1.Rows(Item).Cells(0).FindControl("result"), TextBox).Text
                If Not String.IsNullOrEmpty(equipment) And Not String.IsNullOrEmpty(quantity) And Not String.IsNullOrEmpty(result) Then
                    sql_Forexam += " insert into SYSIO_exam (recordId,SYSIO_recordId,equipment,quantity,result,create_time,op_time)values('Sioexam" + time.AddMilliseconds(Item).ToString("yyyymmddHHmmssfff") + "','" + recordid + "','" + equipment + "','" + quantity + "','" + result + "',getdate(),getdate())"
                End If
            Next
            Dim SqlCmd = New SqlCommand(sql_Forexam, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

            '如果有選取軟體程式變更申請表
            If IfSYSPG = "1" Then
                SqlTxt += "insert into SYSIO_related (SYSIO_recordId,kind,related_recordId)values('" + recordid + "','PG','" + SYSPG_recordId.Text + "')"
            End If
            '如果有選取防火牆維護申請表
            If IfFirewall = "1" Then
                SqlTxt += " insert into SYSIO_related (SYSIO_recordId,kind,related_recordId)values('" + recordid + "','FW','" + Firewall_recordId.Text + "')"
            End If

            '處理上傳檔案
            If CType(Me.FindControl("File_applyFile"), FileUpload).HasFile Then
                '有檔案
                Dim oriFileName = CType(Me.FindControl("File_applyFile"), FileUpload)
                Dim UpFileName As String = Common.UpFile(File_Path, recordid, CType(Me.FindControl("File_applyFile"), FileUpload))
                '塞入主表資料
                SqlTxt += " insert into SYSIO (recordId,applyDate,applyDate_Chn,applyDate_Chn_SN,empUid,empName," _
                    + "target_1,target_2,targetName,applyOrg,applyName,method_1,method_2,method_3,method_4,methodOther," _
                    + "location_1,location_2,location_3,locationOther,InDate,InTimeHr,InTimeMin,OutTimeHr,OutTimeMin," _
                    + "applyFile_ori_name,applyFile,applyComment,IfChangeSoftware,IfSYSPG,IfFirewall,IfTest,IfItemIn,IfItemOut,IfItemLocation,using,create_time,op_time)values(" _
                    + "'" + recordid + "','" + Now + "','" + applyDate_Chn + "','" + applyDate_Chn_SN.ToString() + "','" + uid + "','" + uname + "'," _
                    + "'" + target_1 + "','" + target_2 + "','" + targetName.Text + "','" + applyOrg.Text + "','" + applyName.Text + "'," _
                    + "'" + method_1 + "','" + method_2 + "','" + method_3 + "','" + method_4 + "','" + method_other + "'," _
                    + "'" + location_1 + "','" + location_2 + "','" + location_3 + "','" + location_other + "'," _
                    + "'" + Common.YearYYYY(InDate.Text) + "','" + Ddl_InHour.SelectedValue + "','" + Ddl_InMin.SelectedValue + "','" + Ddl_OutHour.SelectedValue + "','" + Ddl_OutMin.SelectedValue + "'," _
                    + "'" + oriFileName.FileName + "','" + UpFileName + "','" + applyComment.Text + "','" + IfChangeSoftware + "','" + IfSYSPG + "','" + IfFirewall + "','" + IfTest + "'," _
                    + "'" + IfItemIn + "','" + IfItemOut + "','" + IfItemLocation + "','Y',getdate(),getdate())"

            Else
                '沒有檔案
                '塞入主表資料
                SqlTxt += "  insert into SYSIO (recordId,applyDate,applyDate_Chn,applyDate_Chn_SN,empUid,empName," _
                    + "target_1,target_2,targetName,applyOrg,applyName,method_1,method_2,method_3,method_4,methodOther," _
                    + "location_1,location_2,location_3,locationOther,InDate,InTimeHr,InTimeMin,OutTimeHr,OutTimeMin," _
                    + "applyComment,IfChangeSoftware,IfSYSPG,IfFirewall,IfTest,IfItemIn,IfItemOut,IfItemLocation,using,create_time,op_time)values(" _
                    + "'" + recordid + "','" + Now + "','" + applyDate_Chn + "','" + applyDate_Chn_SN.ToString() + "','" + uid + "','" + uname + "'," _
                    + "'" + target_1 + "','" + target_2 + "','" + targetName.Text + "','" + applyOrg.Text + "','" + applyName.Text + "'," _
                    + "'" + method_1 + "','" + method_2 + "','" + method_3 + "','" + method_4 + "','" + method_other + "'," _
                    + "'" + location_1 + "','" + location_2 + "','" + location_3 + "','" + location_other + "'," _
                    + "'" + Common.YearYYYY(InDate.Text) + "','" + Ddl_InHour.SelectedValue + "','" + Ddl_InMin.SelectedValue + "','" + Ddl_OutHour.SelectedValue + "','" + Ddl_OutMin.SelectedValue + "'," _
                    + "'" + applyComment.Text + "','" + IfChangeSoftware + "','" + IfSYSPG + "','" + IfFirewall + "','" + IfTest + "'," _
                    + "'" + IfItemIn + "','" + IfItemOut + "','" + IfItemLocation + "','Y',getdate(),getdate())"

            End If


            '申請人
            SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype ,dp) values "
            SqlTxt += " ('" + apprvid + "','" + recordid + "','" + CType(GridView.Rows(0).FindControl("wf_order"), Label).Text + "','" + uid + "',"
            SqlTxt += "'" + CType(GridView.Rows(0).FindControl("ddl_status"), DropDownList).SelectedValue + "','" + CType(GridView.Rows(0).FindControl("txb_comment"), TextBox).Text + "',getdate() ,'0', 'vender','" + dp + "') "


            '下一位審核人
            If GridView.Rows.Count >= 2 Then
                SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype ,dp) values "
                SqlTxt += " ('" + apprvid + "','" + recordid + "','" + CType(GridView.Rows(1).FindControl("wf_order"), Label).Text + "','" + CType(GridView.Rows(1).FindControl("ddl_user"), DropDownList).SelectedValue + "',"
                SqlTxt += " '1', '',getdate() ,'1', 'land','" + dp + "') "
            End If

            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Session("success") = "Y"
            Response.Redirect("SYSIO.aspx", True)
            Common.showMsg(Me.Page, "儲存成功！")
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

End Class
