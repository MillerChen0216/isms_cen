﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports System.Web.Security
Partial Class ISMS_12110
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim i As Integer



    Protected Sub Flow_SqlDS()

        Flow_DS.ConnectionString = Common.ConnDBS.ConnectionString
        ' Flow_DS.SelectCommand = "SELECT * FROM docResoure where ( doc_status='REV_NEW' or doc_status='REV_VERIFY' or doc_status='REV_REJECT' or doc_status ='NEW' or doc_status='VERIFY' or doc_status='REJECT' or doc_status='DEL_NEW' or doc_status='DEL_VERIFY' or doc_status='DEL_REJECT')   and dp='" + Hidden_DP.Value + "' order by create_date"
        Flow_DS.SelectCommand = " SELECT * FROM docResoure a , ismsUser b,ctlGroup c where ( doc_status='REV_NEW' or doc_status='REV_VERIFY' or doc_status='REV_REJECT'  "
        Flow_DS.SelectCommand += " or doc_status ='NEW' or doc_status='VERIFY' or doc_status='REJECT' or doc_status='DEL_NEW'  "
        Flow_DS.SelectCommand += " or doc_status='DEL_VERIFY' or doc_status='DEL_REJECT') and a.next_check_id=b.user_id and b.group_id = c.group_id and c.dp='" + Hidden_DP.Value + "' order by create_date "

        GridView1.DataSourceID = Flow_DS.ID

    End Sub

    Protected Sub WF_SqlDS()

        WF_DS.ConnectionString = Common.ConnDBS.ConnectionString

        'WF_DS.SelectCommand = "SELECT wf_order,chk_name, 1 wf_inside_order FROM workflowName where apprv_id='" + Hidden_apprv_id.Value + "'   and dp='" + Hidden_DP.Value + "' group by wf_order,chk_name order by wf_order"
        WF_DS.SelectCommand = "SELECT distinct a.wf_order,a.chk_name,isnull(b.wf_userid,'') wf_userid,isnull(b.wf_status_id ,'') wf_status_id,isnull(b.wf_comment,'') " +
                                "wf_comment,b.sn ,b.wf_inside_order , case when wf_status_id='1' then null else signdate end  signdate  FROM workflowName a join wfFormRecord     " +
                                "b on  doc_id='" + Hidden_Docid.Value + "' and a.wf_order=b.wf_order where a.apprv_id='" + Hidden_apprv_id.Value + "' and a.dp='" + Hidden_doc_dp.Value + "'  and b.record_id = (select record_id from wfFormRecord where wf_status_id ='1' and doc_id='" + Hidden_Docid.Value + "')  " +
                                "union all SELECT distinct wf_order ,chk_name,'' wf_userid,''  wf_status_id,'' wf_comment ,9999999999 as sn ,1,null   " +
                                "FROM workflowName a where apprv_id='" + Hidden_apprv_id.Value + "' and dp='" + Hidden_doc_dp.Value + "' and wf_order >" +
                                "(select  next_order from docResoure where doc_id='" + Hidden_Docid.Value + "') order by sn, a.wf_order "

        GridView2.DataSourceID = WF_DS.ID

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        Flow_SqlDS()
    End Sub

    Protected Sub GridView1_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.PreRender

    End Sub


    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(5).Visible = False
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound




        Dim oDataRow As Data.DataRowView
        If e.Row.RowIndex <> -1 Then

            oDataRow = CType(e.Row.DataItem, Data.DataRowView)

            Dim Ds, Ds1, Ds2, Ds3 As New DataSet
            Ds = Common.Get_wfFormRecord(e.Row.Cells(0).Text, e.Row.Cells(5).Text, "")
            Ds1 = Common.Get_Wf_ApprvUser(hidden_dp.Value, e.Row.Cells(0).Text, "")
            Ds2 = Common.Get_Doc_info(e.Row.Cells(5).Text)
            Ds3 = Common.Get_docResoure_waitingId(e.Row.Cells(5).Text, User.Identity.Name.ToString)

            If Ds2.Tables(0).Rows(0).Item("doc_status") = "NEW" Or Ds2.Tables(0).Rows(0).Item("doc_status") = "VERIFY" Or Ds2.Tables(0).Rows(0).Item("doc_status") = "REJECT" Then
                CType(e.Row.FindControl("audit_btn"), Button).Text = "新版審核"
                CType(e.Row.FindControl("del_btn"), Button).Visible = True
            ElseIf Left(Ds2.Tables(0).Rows(0).Item("doc_status"), 3) = "DEL" Then
                CType(e.Row.FindControl("audit_btn"), Button).Text = "廢止審核"
            ElseIf Left(Ds2.Tables(0).Rows(0).Item("doc_status"), 3) = "REV" Then
                CType(e.Row.FindControl("audit_btn"), Button).Text = "改版審核"
            End If

            'If Ds.Tables(0).Rows.Count = 1 Then
            'If Ds.Tables(0).Rows.Count = 0 And Ds2.Tables(0).Rows(0).Item("creator") = User.Identity.Name.ToString Then
            If Ds.Tables(0).Rows.Count = 1 And Ds2.Tables(0).Rows(0).Item("creator") = User.Identity.Name.ToString Then

                CType(e.Row.FindControl("audit_btn"), Button).Visible = True
                'CType(e.Row.FindControl("Edit_Btn"), Button).Visible = True
                'ElseIf Ds2.Tables(0).Rows(0).Item("doc_status") = "NEW" Then
                'CType(e.Row.FindControl("del_btn"), Button).Visible = True
            ElseIf Ds2.Tables(0).Rows(0).Item("Del_creator").ToString = User.Identity.Name.ToString And Ds2.Tables(0).Rows(0).Item("doc_status").ToString = "DEl_NEW" Then
                CType(e.Row.FindControl("audit_btn"), Button).Visible = True
            Else
                CType(e.Row.FindControl("audit_btn"), Button).Visible = False
                'CType(e.Row.FindControl("Edit_Btn"), Button).Visible = False
                CType(e.Row.FindControl("del_btn"), Button).Visible = False

            End If

            If Ds3.Tables(0).Rows.Count > 0 Then
                CType(e.Row.FindControl("audit_btn"), Button).Visible = True
            End If

            'If Ds2.Tables(0).Rows(0).Item("Del_creator").ToString = User.Identity.Name.ToString Then
            'CType(e.Row.FindControl("audit_btn"), Button).Visible = True
            'End If

            'If Ds.Tables(0).Rows.Count >= 1 And (Ds2.Tables(0).Rows(0).Item("doc_status") = "REJECT" Or Ds2.Tables(0).Rows(0).Item("doc_status") = "DEL_REJECT" Or Ds2.Tables(0).Rows(0).Item("doc_status") = "REV_REJECT") Then

            'Dim Login_id, Sign_id As String
            'Dim wf_order As String = "0"
            'Login_id = User.Identity.Name.ToString
            'For n = 0 To Ds.Tables(0).Rows.Count - 1
            'If Ds.Tables(0).Rows(n).Item("wf_status_id") <> "9" Then
            'wf_order = Ds.Tables(0).Rows(n).Item("wf_order").ToString
            'Exit For
            'End If
            'Next
            'If wf_order = "0" Then
            'Sign_id = Ds2.Tables(0).Rows(0).Item("creator").ToString
            'Else
            ''Ds3 = Common.Get_Wf_ApprvUser(Hidden_DP.Value, e.Row.Cells(0).Text, wf_order)
            'Sign_id = Ds2.Tables(0).Rows(0).Item("next_check_id").ToString.ToUpper
            'End If

            'If Login_id = Sign_id Then
            'CType(e.Row.FindControl("audit_btn"), Button).Visible = True
            'End If
            'End If
            'If Ds.Tables(0).Rows.Count >= 1 And (Ds2.Tables(0).Rows(0).Item("doc_status") = "VERIFY" Or Ds2.Tables(0).Rows(0).Item("doc_status") = "DEL_VERIFY" Or Ds2.Tables(0).Rows(0).Item("doc_status") = "REV_VERIFY" Or Ds2.Tables(0).Rows(0).Item("doc_status") = "REJECT" Or Ds2.Tables(0).Rows(0).Item("doc_status") = "REV_REJECT" Or Ds2.Tables(0).Rows(0).Item("doc_status") = "DEL_REJECT") Then
            'Dim Login_id, Sign_id As String
            ''Login_id = User.Identity.Name.ToString
            'Login_id = User.Identity.Name.ToString
            'Ds3 = Common.Get_Wf_ApprvUser(Hidden_DP.Value, e.Row.Cells(0).Text, Ds.Tables(0).Rows(0).Item("wf_order"))
            'For i = 0 To Ds3.Tables(0).Rows.Count - 1
            'Sign_id = Ds2.Tables(0).Rows(0).Item("next_check_id").ToString.ToUpper
            'If Login_id = Sign_id Then
            'CType(e.Row.FindControl("audit_btn"), Button).Visible = True

            'End If
            'Next


            ''ElseIf Ds.Tables(0).Rows.Count >= 1 And (Ds2.Tables(0).Rows(0).Item("doc_status") = "REV_REJECT" Or Ds2.Tables(0).Rows(0).Item("doc_status") = "REJECT") Then

            ''    If Ds2.Tables(0).Rows(0).Item("creator") = User.Identity.Name.ToString Then
            ''        CType(e.Row.FindControl("audit_btn"), Button).Visible = True
            ''    End If

            'ElseIf Ds.Tables(0).Rows.Count >= 1 And Ds2.Tables(0).Rows(0).Item("doc_status") = "DEL_NEW" Then
            ''If Ds2.Tables(0).Rows(0).Item("Del_creator") = User.Identity.Name.ToString Then
            'If Ds2.Tables(0).Rows(0).Item("Del_creator") = User.Identity.Name.ToString Then
            'CType(e.Row.FindControl("audit_btn"), Button).Visible = True
            ''CType(e.Row.FindControl("del_btn"), Button).Visible = True
            'End If


            'End If

            e.Row.Cells(0).Text = Common.ISMS_DocNameChange(e.Row.Cells(0).Text)


            Dim sMessage, sShowField As String
            sShowField = e.Row.Cells(1).Text
            sMessage = String.Format("您確定要刪除此份 [{0}] 文件嗎?", sShowField)
            CType(e.Row.FindControl("Del_Btn"), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"


        End If
    End Sub

    Protected Sub audit_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Main_TB.Visible = False
        info_tb.Visible = True
        tb2.Visible = True
        Dim Ds, Ds1, ds2 As New DataSet
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        Hidden_Docid.Value = GridView1.Rows(Index).Cells(5).Text
        Ds = Common.Get_Doc_info(Hidden_Docid.Value)

        docCat_LB.Text = Common.ISMS_DocNameChange(Ds.Tables(0).Rows(0).Item("doc_type"))
        docNum_LB.Text = Ds.Tables(0).Rows(0).Item("doc_num")
        docVer_LB.Text = Ds.Tables(0).Rows(0).Item("doc_version")
        docName_LB.Text = Ds.Tables(0).Rows(0).Item("doc_name")
        docCreator.Text = Ds.Tables(0).Rows(0).Item("creator")
        docCreatorName.Text = Common.Get_User_Name(Ds.Tables(0).Rows(0).Item("creator"))

        Hidden_apprv_id.Value = Ds.Tables(0).Rows(0).Item("apprv_id")

        Ds1 = Common.Get_wfFormRecord(Hidden_apprv_id.Value, Hidden_Docid.Value, "")

        If Ds.Tables(0).Rows(0).Item("doc_status") = "NEW" Or Ds.Tables(0).Rows(0).Item("doc_status") = "VERIFY" Or Ds.Tables(0).Rows(0).Item("doc_status") = "REJECT" Then
            lb_status.Text = "新版審核"
            docDsc_LB.Text = Ds.Tables(0).Rows(0).Item("doc_capsule")
        ElseIf Left(Ds.Tables(0).Rows(0).Item("doc_status"), 3) = "DEL" Then
            lb_status.Text = "廢止審核"
            docDsc_LB.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("doc_capsule"))
        ElseIf Left(Ds.Tables(0).Rows(0).Item("doc_status"), 3) = "REV" Then
            lb_status.Text = "改版審核"
            docDsc_LB.Text = Ds.Tables(0).Rows(0).Item("doc_upddesc")
        End If
        Hidden_doc_dp.Value = Ds.Tables(0).Rows(0).Item("dp")
        WF_SqlDS()
        'lb_wfData.Visible = True

        'ds2 = Common.Get_wfRecord(Hidden_apprv_id.Value, Hidden_Docid.Value, "")

        'For i = 0 To ds2.Tables(0).Rows.Count - 1
        '    If i = ds2.Tables(0).Rows.Count - 1 Then
        '        lb_wfData.Text = lb_wfData.Text + Common.Get_User_Name(ds2.Tables(0).Rows(i).Item("wf_userid")) + "(" + Common.Get_WFStatusName(ds2.Tables(0).Rows(i).Item("wf_status_id")) + ")"
        '    Else
        '        lb_wfData.Text = lb_wfData.Text + Common.Get_User_Name(ds2.Tables(0).Rows(i).Item("wf_userid")) + "(" + Common.Get_WFStatusName(ds2.Tables(0).Rows(i).Item("wf_status_id")) + ") =>"
        '    End If
        'Next



    End Sub


    Protected Sub Filedown_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Filedown_Btn.Click
        'Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", Hidden_Docid.Value, "APPLICATION", "FILEVIEW", docName_LB.Text)
        Dim Ds As New DataSet
        Ds = Common.Get_Doc_info(Hidden_Docid.Value)
        Response.ContentType = "application/octet-stream"
        Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5")
        Response.AddHeader("content-disposition", "attachment;filename=" + Common.Decode((Ds.Tables(0).Rows(0).Item("doc_filename"))) + "")
        Response.WriteFile(Common.Sys_Config("File_Path") + "/" + ("" + (Ds.Tables(0).Rows(0).Item("doc_filename")) + ""))
        Response.End()


    End Sub

    Protected Sub GridView2_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowCreated
        e.Row.Cells(5).Visible = False
    End Sub

    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound

        Dim ds, ds0, ds1, ds2, ds3, ds4, ds5, ds6, ds7 As New DataSet
        Dim doc_status, wf_status, wf_user, wf_count, wf_order, wf_user_order, creator, del_creator As String


        If e.Row.RowIndex <> -1 Then

            If e.Row.RowIndex = 0 Then
                Dim ds9 As DataSet = Common.Get_wfFormRecord_id(CType(e.Row.FindControl("lab_sn"), Label).Text)
                HiddneRecord_id.Value = ds9.Tables(0).Rows(0).Item("record_id")
            End If

            ds = Common.Get_wfFormRecord(Hidden_apprv_id.Value, Hidden_Docid.Value, "")
            ds0 = Common.Get_Wf_User_ALL(Hidden_apprv_id.Value, e.Row.Cells(0).Text, "", Hidden_doc_dp.Value)
            ds1 = Common.Get_Doc_info(Hidden_Docid.Value)
            ds2 = Common.Get_Wf_ApprvUser(Hidden_doc_dp.Value, Hidden_apprv_id.Value, e.Row.Cells(0).Text)
            ds3 = Common.Get_wfFormRecord(Hidden_apprv_id.Value, Hidden_Docid.Value, "ASC")
            ds5 = Common.Get_wfRecord(Hidden_apprv_id.Value, Hidden_Docid.Value, HiddneRecord_id.Value)
            ds6 = Common.Get_wfInside_Record_sn(Hidden_apprv_id.Value, Hidden_Docid.Value, HiddneRecord_id.Value, CType(e.Row.FindControl("lab_sn"), Label).Text)
            ds7 = Common.Get_docResoure_waiting(Hidden_apprv_id.Value, Hidden_Docid.Value)
            'ds8 = Common.Get_docResoure_order(Hidden_Docid.Value, e.Row.RowIndex - 1)

        

            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim label_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lab_status_id As Label = CType(e.Row.FindControl("lab_status_id"), Label)

            If e.Row.Cells(0).Text = "0" And lab_status_id.Text <> "1" Then
                pan_user.Visible = True : ddl_user.Visible = False
                lb_username.Text = ds7.Tables(0).Rows(0).Item("user_name").ToString
                label_userid.Text = ds7.Tables(0).Rows(0).Item("wf_userid").ToString
            ElseIf lab_status_id.Text = "1" Then
                pan_user.Visible = True : ddl_user.Visible = False
                lb_username.Text = ds7.Tables(0).Rows(0).Item("user_name").ToString
                label_userid.Text = ds7.Tables(0).Rows(0).Item("wf_userid").ToString
                lb_count.Text = e.Row.RowIndex
            ElseIf e.Row.RowIndex = CType(lb_count.Text, Integer) + 1 Then '待審核下一關卡
                pan_user.Visible = False : ddl_user.Visible = True

                ddl_user.DataSource = ds0
                ddl_user.DataTextField = "user_name"
                ddl_user.DataValueField = "wf_userid"
                ddl_user.DataBind()
            Else
                pan_user.Visible = False : ddl_user.Visible = False
            End If

            'If ds7.Tables(0).Rows.Count >= 1 Then '
            'pan_user.Visible = True : ddl_user.Visible = False
            'lb_username.Text = ds0.Tables(0).Rows(0).Item("user_name").ToString
            'label_userid.Text = ds0.Tables(0).Rows(0).Item("wf_userid").ToString
            'lb_count.Text = e.Row.RowIndex
            'Else
            'pan_user.Visible = False : ddl_user.Visible = False
            'End If

            'If ds8.Tables(0).Rows.Count >= 1 Then
            'pan_user.Visible = False : ddl_user.Visible = True

            'ddl_user.DataSource = ds0
            'ddl_user.DataTextField = "name_id"
            'ddl_user.DataValueField = "wf_userid"
            'ddl_user.DataBind()
            'End If

            If e.Row.RowIndex >= 1 Then

                'CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("next_check_id")) '取得審核人員名稱
                'CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("next_check_id")
                'ElseIf e.Row.RowIndex = 0 And (Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "NEW" Or Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "VERIFY" Or Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "REJECT" Or Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "REV_NEW" Or Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "REV_VERIFY" Or Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "REV_REJECT") Then

            ElseIf e.Row.RowIndex = 0 And (Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "NEW" Or Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "VERIFY" Or Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "REV_NEW" Or Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "REV_VERIFY") Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("creator"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("creator")


                'ElseIf e.Row.RowIndex = 0 And Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "DEL_NEW" Or Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "DEL_VERIFY" Or Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "DEL_REJECT" Then
            ElseIf e.Row.RowIndex = 0 And Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "DEL_NEW" Or Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "DEL_VERIFY" Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("del_creator"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("del_creator")

            End If


            Try
                wf_status = Common.FixNull(ds.Tables(0).Rows(0).Item("wf_status_id"))
                wf_order = Common.FixNull(ds.Tables(0).Rows(0).Item("wf_order"))

            Catch ex As Exception


            End Try
            doc_status = Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status"))
            wf_user_order = Common.FixNull(ds2.Tables(0).Rows(0).Item("wf_order"))
            wf_user = Common.FixNull(CType(e.Row.FindControl("label_userid"), Label).Text.ToUpper)
            wf_count = Common.FixNull(ds.Tables(0).Rows.Count)
            creator = Common.FixNull(ds1.Tables(0).Rows(0).Item("creator"))
            del_creator = Common.FixNull(ds1.Tables(0).Rows(0).Item("del_creator"))
            Hidden_Status.Value = doc_status

            If wf_count = 1 And wf_user_order = 0 And (doc_status = "NEW" Or doc_status = "REV_NEW") Then

                CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("verify_btn"), Button).Visible = True



            ElseIf wf_count > 1 Then

                'If (doc_status = "REJECT" Or doc_status = "REV_REJECT" Or doc_status = "DEL_REJECT") And ds.Tables(0).Rows(0).Item("wf_status_id") = "9" And wf_user_order = 0 And wf_user = LCase(User.Identity.Name.ToString) Then

                '    CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                '    CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                '    CType(e.Row.Cells(e.Row.RowIndex).FindControl("verify_btn"), Button).Visible = True

                If wf_user_order = 0 And doc_status = "DEL_NEW" Then

                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("verify_btn"), Button).Visible = True

                    'ElseIf (doc_status = "VERIFY" Or doc_status = "REV_VERIFY" Or doc_status = "DEL_VERIFY") And (ds.Tables(0).Rows(0).Item("wf_status_id") = "2" Or ds.Tables(0).Rows(0).Item("wf_status_id") = "0") And wf_count >= 1 And e.Row.RowIndex >= 1 And wf_user_order = (ds.Tables(0).Rows(0).Item("wf_order") + 1) And wf_user = User.Identity.Name.ToString Then
                ElseIf (doc_status = "VERIFY" Or doc_status = "REV_VERIFY" Or doc_status = "DEL_VERIFY") And (ds.Tables(0).Rows(0).Item("wf_status_id") = "1" Or ds.Tables(0).Rows(0).Item("wf_status_id") = "0") And wf_count >= 1 And e.Row.RowIndex >= 1 And wf_user_order = ds.Tables(0).Rows(0).Item("wf_order") And wf_user = User.Identity.Name.ToString Then
                    CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = True
                    CType(e.Row.FindControl("comment_txt"), TextBox).Visible = True
                    CType(e.Row.FindControl("verify_btn"), Button).Visible = True

                ElseIf (doc_status = "REJECT" Or doc_status = "REV_REJECT" Or doc_status = "DEL_REJECT") And (ds.Tables(0).Rows(0).Item("wf_status_id") = "1") And wf_count >= 1 And wf_user_order = (ds.Tables(0).Rows(0).Item("wf_order")) And wf_user = User.Identity.Name.ToString And ds.Tables(0).Rows(0).Item("sn") = CType(e.Row.FindControl("lab_sn"), Label).Text Then
                    CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = True
                    CType(e.Row.FindControl("comment_txt"), TextBox).Visible = True
                    CType(e.Row.FindControl("verify_btn"), Button).Visible = True
                    pan_user.Visible = True : ddl_user.Visible = False

                Else
                    CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = False
                    CType(e.Row.FindControl("comment_txt"), TextBox).Visible = False
                    CType(e.Row.FindControl("verify_btn"), Button).Visible = False

                End If

            Else
                CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = False
                CType(e.Row.FindControl("comment_txt"), TextBox).Visible = False
                CType(e.Row.FindControl("verify_btn"), Button).Visible = False


            End If

            'Response.Write(Hidden_Status.Value)

            '填上已簽核過的狀態與意見
            If ds6.Tables(0).Rows.Count >= 1 Then
                'If ds6.Tables(0).Rows(0).Item("wf_inside_order") = e.Row.Cells(5).Text And e.Row.RowIndex >= 0 And e.Row.RowIndex <= ds5.Tables(0).Rows.Count And ds6.Tables(0).Rows(0).Item("wf_status_id") <> "1" Then
                If CType(e.Row.FindControl("lab_status_id"), Label).Text <> "" Then
                    'If ds.Tables(0).Rows(0).Item("sn") = CType(e.Row.FindControl("lab_sn"), Label).Text And doc_status = "REJECT" Or doc_status = "REV_REJECT" Or doc_status = "DEL_REJECT" Then

                    'CType(e.Row.FindControl("lb_status"), Label).Visible = False
                    'CType(e.Row.FindControl("comment_lb"), Label).Visible = False
                    'ElseIf e.Row.RowIndex = 0 And CType(e.Row.FindControl("lab_status_id"), Label).Text = "1" Then
                    If CType(e.Row.FindControl("lab_status_id"), Label).Text = "1" Then
                        CType(e.Row.FindControl("lb_status"), Label).Visible = False
                        CType(e.Row.FindControl("comment_lb"), Label).Visible = False
                    Else
                        CType(e.Row.FindControl("lb_status"), Label).Visible = True
                        CType(e.Row.FindControl("comment_lb"), Label).Visible = True
                        CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(ds6.Tables(0).Rows(0).Item("wf_status_id")))
                        CType(e.Row.FindControl("comment_lb"), Label).Text = Common.FixNull(ds6.Tables(0).Rows(0).Item("wf_comment"))
                    End If

                    pan_user.Visible = True : ddl_user.Visible = False
                    lb_username.Text = ds6.Tables(0).Rows(0).Item("user_name").ToString
                    label_userid.Text = ds6.Tables(0).Rows(0).Item("wf_userid").ToString
                    'ElseIf e.Row.RowIndex = CType(lb_count.Text, Integer) + 1 Then '待審核下一關卡
                    'pan_user.Visible = False : ddl_user.Visible = True
                    'Else
                    'CType(e.Row.FindControl("lb_status"), Label).Visible = False
                    'CType(e.Row.FindControl("comment_lb"), Label).Visible = False
                End If
            End If


            '狀態選單
            If e.Row.RowIndex >= 1 Then
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(1)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
            Else '填表人審核
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(0)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
            End If



        End If



    End Sub





    Protected Sub verify_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        Dim now_index As Integer = GridView2.Rows(lb_count.Text).Cells(0).Text '關卡 lb_count 是存第幾行 重新新增一筆資料來試

        Dim ddl_status As DropDownList = CType(Me.GridView2.Rows(Index).FindControl("ddl_status"), DropDownList)
        Dim DateTime_Str As String = DateTime.Now.ToString("yyyy-MM-dd HH:mm")

        Dim ds, ds1, ds2, ds3 As New DataSet
        Dim doc_status As String

        ds = Common.Get_Wf_ApprvUser(Hidden_doc_dp.Value, Hidden_apprv_id.Value)

        'Response.Write(Hidden_Status.Value)

        Select Case Hidden_Status.Value
            Case "NEW" '新增
                Hidden_Status.Value = "NEW"
            Case "VERIFY"
                Hidden_Status.Value = "NEW"
            Case "REJECT"
                Hidden_Status.Value = "NEW"

            Case "REV_NEW" '改版
                Hidden_Status.Value = "REV"
            Case "REV_VERIFY"
                Hidden_Status.Value = "REV"
            Case "REV_REJECT"
                Hidden_Status.Value = "REV"

            Case "DEL_NEW" '廢止
                Hidden_Status.Value = "DEL"
            Case "DEL_VERIFY "
                Hidden_Status.Value = "DEL"
            Case "DEL_REJECT"
                Hidden_Status.Value = "DEL"
        End Select

        'Response.Write(Hidden_Status.Value)

        'SqlTxt = "INSERT INTO wfFormRecord " + _
        '"(apprv_id,doc_id,wf_order,wf_userid,wf_status_id,wf_comment,signdate,record_id,wf_inside_order,doc_status) VALUES" + _
        '"('" + Hidden_apprv_id.Value + "','" + Hidden_Docid.Value + "','" + oRow.Cells(0).Text + "','" + CType(GridView2.Rows(Index).FindControl("label_userid"), Label).Text + "','" + CType(GridView2.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue + "'," + _
        '" '" + CType(GridView2.Rows(Index).FindControl("comment_txt"), TextBox).Text + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + HiddneRecord_id.Value + "','" + oRow.Cells(5).Text + "','" + Hidden_Status.Value + "') "

        'SqlCmd = New SqlCommand(SqlTxt, Conn)
        'Conn.Open()
        'SqlCmd.ExecuteNonQuery()
        'Conn.Close()

        ds1 = Common.Get_Doc_info(Hidden_Docid.Value)
        ds2 = Common.Get_WorkFlow_MaxOrderId(Hidden_apprv_id.Value, Hidden_doc_dp.Value)
        doc_status = ds1.Tables(0).Rows(0).Item("doc_status")


        If doc_status = "NEW" Or doc_status = "VERIFY" Or doc_status = "REJECT" Then
            'If doc_status = "NEW" Or doc_status = "VERIFY" Then

            If ddl_status.SelectedValue = "2" And oRow.Cells(0).Text = ds2.Tables(0).Rows(0).Item("wf_order") Then '流程已到最後關卡
                SqlTxt = "UPDATE docResoure  SET doc_status = 'PUBLISH' WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                SqlTxt += " UPDATE wfFormRecord SET wf_status_id ='2' , wf_comment = '" + CType(GridView2.Rows(Index).FindControl("comment_txt"), TextBox).Text + "' WHERE apprv_id='" + Hidden_apprv_id.Value + "' and doc_id='" + Hidden_Docid.Value + "' and wf_status_id ='1' and wf_order ='" + now_index.ToString + "'"
                'Mail通知
                'ds3 = Common.Get_Wf_ApprvUser(Hidden_apprv_id.Value, oRow.Cells(0).Text + 1)
                'ds3 = Common.Get_Wf_ApprvUser(Hidden_doc_dp.Value, Hidden_apprv_id.Value, oRow.Cells(0).Text + 1)
                ds3 = Common.Check_PublishPower(Hidden_doc_dp.Value, "12120")
                ' Common.SendMail(ds1.Tables(0).Rows(i).Item("creator"), "Workflow_OK", docNum_LB.Text, docName_LB.Text, "")
                For i = 0 To ds3.Tables(0).Rows.Count - 1
                    'Common.SendMail(ds3.Tables(0).Rows(i).Item("user_id"), "PublishDoc", docNum_LB.Text, docName_LB.Text, "")
                Next

            ElseIf ddl_status.SelectedValue = "0" Or ddl_status.SelectedValue = "2" Then
                If GridView2.Rows.Count > Index + 1 Then
                    SqlTxt = " UPDATE docResoure  SET doc_status = 'VERIFY' ,next_check_id='" + (CType(GridView2.Rows(Index + 1).FindControl("ddl_user"), DropDownList).SelectedValue).ToString + "' , next_order = '" + (now_index + 1).ToString + "' WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                    SqlTxt += " UPDATE wfFormRecord SET wf_status_id ='" + ddl_status.SelectedValue + "', wf_comment = '" + CType(GridView2.Rows(Index).FindControl("comment_txt"), TextBox).Text + "'  WHERE apprv_id='" + Hidden_apprv_id.Value + "' and doc_id='" + Hidden_Docid.Value + "' and ( wf_status_id  = '1'  or wf_status_id  = '0' ) and wf_order ='" + now_index.ToString + "'"
                    SqlTxt += " INSERT INTO wfFormRecord (apprv_id,doc_id,wf_order,wf_inside_order,wf_userid,wf_status_id,record_id,doc_status,signdate) VALUES('" + Hidden_apprv_id.Value + "','" + Hidden_Docid.Value + "','" + (now_index + 1).ToString + "','" + GridView2.Rows(Index + 1).Cells(5).Text + "','" + CType(GridView2.Rows(Index + 1).FindControl("ddl_user"), DropDownList).SelectedValue.ToString + "','1','" + HiddneRecord_id.Value + "','" + Hidden_Status.Value + "','" + DateTime_Str + "') "
                    'Mail通知
                    ds3 = Common.Get_Wf_ApprvUser(Hidden_doc_dp.Value, Hidden_apprv_id.Value, oRow.Cells(0).Text + 1)
                    For i = 0 To ds3.Tables(0).Rows.Count - 1
                        'Common.SendMail(ds3.Tables(0).Rows(i).Item("wf_userid"), "Workflow", docNum_LB.Text, docName_LB.Text, "")
                    Next
                Else
                    SqlTxt = "UPDATE docResoure  SET doc_status = 'PUBLISH' WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                    SqlTxt += " UPDATE wfFormRecord SET wf_status_id ='2' , wf_comment = '" + CType(GridView2.Rows(Index).FindControl("comment_txt"), TextBox).Text + "' WHERE apprv_id='" + Hidden_apprv_id.Value + "' and doc_id='" + Hidden_Docid.Value + "' and wf_status_id ='1' and wf_order ='" + now_index.ToString + "'"
                    'Mail通知
                    'ds3 = Common.Get_Wf_ApprvUser(Hidden_apprv_id.Value, oRow.Cells(0).Text + 1)
                    'ds3 = Common.Get_Wf_ApprvUser(Hidden_doc_dp.Value, Hidden_apprv_id.Value, oRow.Cells(0).Text + 1)
                    ds3 = Common.Check_PublishPower(Hidden_doc_dp.Value, "12120")
                    ' Common.SendMail(ds1.Tables(0).Rows(i).Item("creator"), "Workflow_OK", docNum_LB.Text, docName_LB.Text, "")
                    'For i = 0 To ds3.Tables(0).Rows.Count - 1
                    'Common.SendMail(ds3.Tables(0).Rows(i).Item("user_id"), "PublishDoc", docNum_LB.Text, docName_LB.Text, "")
                    'Next
                End If

               
              

            ElseIf ddl_status.SelectedValue = "9" Then

                If now_index = 0 Then
                    Common.showMsg(Me.Page, "流程已到填表人，無法再退回！")
                    Return
                End If
                Dim ds5 As DataSet = Common.Get_wfFormRecord_order(Hidden_apprv_id.Value, Hidden_Docid.Value, (now_index - 1).ToString)
                SqlTxt = "UPDATE docResoure  SET doc_status = 'ISSUE',locked='0' WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                SqlTxt += " UPDATE docResoure  SET doc_status = 'REJECT',next_check_id='" + ds5.Tables(0).Rows(0).Item("wf_userid").ToString + "' , next_order = '" + (now_index - 1).ToString + "'  WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                SqlTxt += " UPDATE wfFormRecord SET wf_status_id ='" + ddl_status.SelectedValue + "' , wf_comment = '" + CType(GridView2.Rows(Index).FindControl("comment_txt"), TextBox).Text + "'  WHERE apprv_id='" + Hidden_apprv_id.Value + "' and doc_id='" + Hidden_Docid.Value + "' and ( wf_status_id  = '1'   )and wf_order ='" + now_index.ToString + "'"
                SqlTxt += " INSERT INTO wfFormRecord (apprv_id,doc_id,wf_order,wf_inside_order,wf_userid,wf_status_id,record_id,doc_status,signdate) VALUES('" + Hidden_apprv_id.Value + "','" + Hidden_Docid.Value + "','" + (now_index - 1).ToString + "','" + GridView2.Rows(Index - 1).Cells(5).Text + "','" + ds5.Tables(0).Rows(0).Item("wf_userid").ToString + "','1','" + HiddneRecord_id.Value + "','" + Hidden_Status.Value + "','" + DateTime_Str + "') "
                'Mail通知
                'Common.SendMail(docCreator.Text, "Workflow_KO", docNum_LB.Text, docName_LB.Text, User.Identity.Name.ToString)

            End If

        ElseIf doc_status = "REV_NEW" Or doc_status = "REV_VERIFY" Or doc_status = "REV_REJECT" Then
            'ElseIf doc_status = "REV_NEW" Or doc_status = "REV_VERIFY" Then



            If ddl_status.SelectedValue = "2" And oRow.Cells(0).Text = ds2.Tables(0).Rows(0).Item("wf_order") Then

                SqlTxt = "UPDATE docResoure  SET doc_status = 'REV_PUBLISH' WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                SqlTxt += " UPDATE wfFormRecord SET wf_status_id ='2' , wf_comment = '" + CType(GridView2.Rows(Index).FindControl("comment_txt"), TextBox).Text + "' WHERE apprv_id='" + Hidden_apprv_id.Value + "' and doc_id='" + Hidden_Docid.Value + "' and wf_status_id ='1' and wf_order ='" + now_index.ToString + "'"
                'Mail通知
                ds3 = Common.Check_PublishPower(Hidden_doc_dp.Value, "12120")
                'Common.SendMail(ds1.Tables(0).Rows(i).Item("creator"), "Workflow_OK", docNum_LB.Text, docName_LB.Text, "")
                For i = 0 To ds3.Tables(0).Rows.Count - 1
                    ' Common.SendMail(ds3.Tables(0).Rows(i).Item("user_id"), "PublishDoc", docNum_LB.Text, docName_LB.Text, "")
                Next

                'ds3 = Common.Get_Wf_ApprvUser(Hidden_apprv_id.Value, oRow.Cells(0).Text + 1)
                'For i = 0 To ds3.Tables(0).Rows.Count - 1
                ''''' '    Common.SendMail(ds3.Tables(0).Rows(i).Item("wf_userid"), "Workflow", docNum_LB.Text, docName_LB.Text)
                'Next

            ElseIf ddl_status.SelectedValue = "0" Or ddl_status.SelectedValue = "2" Then
                If GridView2.Rows.Count > Index + 1 Then
                    SqlTxt = "UPDATE docResoure  SET doc_status = 'REV_VERIFY' ,next_check_id='" + CType(GridView2.Rows(Index + 1).FindControl("ddl_user"), DropDownList).SelectedValue.ToString + "' , next_order = '" + (now_index + 1).ToString + "' WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "' "
                    SqlTxt += " UPDATE wfFormRecord SET wf_status_id ='" + ddl_status.SelectedValue + "', wf_comment = '" + CType(GridView2.Rows(Index).FindControl("comment_txt"), TextBox).Text + "' WHERE apprv_id='" + Hidden_apprv_id.Value + "' and doc_id='" + Hidden_Docid.Value + "' and ( wf_status_id  = '1'  or wf_status_id  = '0' ) and wf_order ='" + now_index.ToString + "'"
                    SqlTxt += " INSERT INTO wfFormRecord (apprv_id,doc_id,wf_order,wf_inside_order,wf_userid,wf_status_id,record_id,doc_status,signdate) VALUES('" + Hidden_apprv_id.Value + "','" + Hidden_Docid.Value + "','" + (now_index + 1).ToString + "','" + GridView2.Rows(Index + 1).Cells(5).Text + "','" + CType(GridView2.Rows(Index + 1).FindControl("ddl_user"), DropDownList).SelectedValue.ToString + "','1','" + HiddneRecord_id.Value + "','" + Hidden_Status.Value + "','" + DateTime_Str + "') "
                    'Mail通知
                    ds3 = Common.Get_Wf_ApprvUser(Hidden_doc_dp.Value, Hidden_apprv_id.Value, oRow.Cells(0).Text + 1)
                    For i = 0 To ds3.Tables(0).Rows.Count - 1
                        ' Common.SendMail(ds3.Tables(0).Rows(i).Item("wf_userid"), "Workflow", docNum_LB.Text, docName_LB.Text, "")
                    Next
                Else
                    SqlTxt = "UPDATE docResoure  SET doc_status = 'PUBLISH' WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                    SqlTxt += " UPDATE wfFormRecord SET wf_status_id ='2' , wf_comment = '" + CType(GridView2.Rows(Index).FindControl("comment_txt"), TextBox).Text + "' WHERE apprv_id='" + Hidden_apprv_id.Value + "' and doc_id='" + Hidden_Docid.Value + "' and wf_status_id ='1' and wf_order ='" + now_index.ToString + "'"
                    ds3 = Common.Check_PublishPower(Hidden_doc_dp.Value, "12120")
                End If

                ElseIf ddl_status.SelectedValue = "9" Then
                    If now_index = 0 Then
                        Common.showMsg(Me.Page, "流程已到填表人，無法再退回！")
                        Return
                    End If
                    Dim ds4 As New DataSet
                    ds4 = Common.Get_OldDoc_Rev(docNum_LB.Text, docVer_LB.Text)

                    Dim ds5 As DataSet = Common.Get_wfFormRecord_order(Hidden_apprv_id.Value, Hidden_Docid.Value, (now_index - 1).ToString)
                    Dim SqlTxt1 As String

                    If ds4.Tables(0).Rows.Count >= 1 Then
                        SqlTxt1 = "UPDATE docResoure  SET locked='0', next_check_id='' , next_order = '' WHERE doc_id='" + ds4.Tables(0).Rows(0).Item("doc_id").ToString + "'"
                        SqlTxt1 += " UPDATE wfFormRecord SET wf_status_id ='" + ddl_status.SelectedValue + "' , wf_comment = '" + CType(GridView2.Rows(Index).FindControl("comment_txt"), TextBox).Text + "' WHERE apprv_id='" + Hidden_apprv_id.Value + "' and doc_id='" + Hidden_Docid.Value + "' and ( wf_status_id  = '1'  )and wf_order ='" + now_index.ToString + "'"
                        SqlCmd = New SqlCommand(SqlTxt1, Conn)
                        Conn.Open()
                        SqlCmd.ExecuteNonQuery()
                        Conn.Close()

                    End If
                    SqlTxt = "UPDATE docResoure  SET doc_status = 'REV_REJECT',locked='1',next_check_id='" + ds5.Tables(0).Rows(0).Item("wf_userid").ToString + "' , next_order = '" + (now_index - 1).ToString + "'  WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                    SqlTxt += " UPDATE wfFormRecord SET wf_status_id ='" + ddl_status.SelectedValue + "' , wf_comment = '" + CType(GridView2.Rows(Index).FindControl("comment_txt"), TextBox).Text + "' WHERE apprv_id='" + Hidden_apprv_id.Value + "' and doc_id='" + Hidden_Docid.Value + "' and ( wf_status_id  = '1'  )and wf_order ='" + now_index.ToString + "'"
                    SqlTxt += " INSERT INTO wfFormRecord (apprv_id,doc_id,wf_order,wf_inside_order,wf_userid,wf_status_id,record_id,doc_status,signdate) VALUES('" + Hidden_apprv_id.Value + "','" + Hidden_Docid.Value + "','" + (now_index - 1).ToString + "','" + GridView2.Rows(Index - 1).Cells(5).Text + "','" + ds5.Tables(0).Rows(0).Item("wf_userid").ToString + "','1','" + HiddneRecord_id.Value + "','" + Hidden_Status.Value + "','" + DateTime_Str + "') "

                    'SqlTxt = "UPDATE docResoure  SET doc_status = 'REV_REJECT' WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                    'Mail通知
                    'Common.SendMail(docCreator.Text, "Workflow_KO", docNum_LB.Text, docName_LB.Text, User.Identity.Name.ToString)


                End If

            ElseIf doc_status = "DEL_NEW" Or doc_status = "DEL_VERIFY" Or doc_status = "DEL_REJECT" Then
                'ElseIf doc_status = "DEL_NEW" Or doc_status = "DEL_VERIFY" Then

                If ddl_status.SelectedValue = "2" And oRow.Cells(0).Text = ds2.Tables(0).Rows(0).Item("wf_order") Then

                    SqlTxt = "UPDATE docResoure  SET doc_status = 'DEL_PUBLISH' WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                    SqlTxt += " UPDATE wfFormRecord SET wf_status_id ='2' , wf_comment = '" + CType(GridView2.Rows(Index).FindControl("comment_txt"), TextBox).Text + "' WHERE apprv_id='" + Hidden_apprv_id.Value + "' and doc_id='" + Hidden_Docid.Value + "' and wf_status_id ='1' and wf_order ='" + now_index.ToString + "'"
                    'Mail通知
                    ds3 = Common.Check_PublishPower(Hidden_doc_dp.Value, "12130")
                    ' Common.SendMail(ds1.Tables(0).Rows(i).Item("creator"), "Workflow_OK", docNum_LB.Text, docName_LB.Text, "")
                    For i = 0 To ds3.Tables(0).Rows.Count - 1
                        ' Common.SendMail(ds3.Tables(0).Rows(i).Item("user_id"), "DisableDoc", docNum_LB.Text, docName_LB.Text, "")
                    Next

                ElseIf ddl_status.SelectedValue = "0" Or ddl_status.SelectedValue = "2" Then
                If GridView2.Rows.Count > Index + 1 Then
                    SqlTxt = "UPDATE docResoure  SET doc_status = 'DEL_VERIFY',next_check_id='" + CType(GridView2.Rows(Index + 1).FindControl("ddl_user"), DropDownList).SelectedValue.ToString + "' , next_order = '" + (now_index + 1).ToString + "'  WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                    SqlTxt += " UPDATE wfFormRecord SET wf_status_id ='" + ddl_status.SelectedValue + "' , wf_comment = '" + CType(GridView2.Rows(Index).FindControl("comment_txt"), TextBox).Text + "' WHERE apprv_id='" + Hidden_apprv_id.Value + "' and doc_id='" + Hidden_Docid.Value + "' and ( wf_status_id  = '1'  or wf_status_id  = '0' )and wf_order ='" + now_index.ToString + "'"
                    SqlTxt += " INSERT INTO wfFormRecord (apprv_id,doc_id,wf_order,wf_inside_order,wf_userid,wf_status_id,record_id,doc_status,signdate) VALUES('" + Hidden_apprv_id.Value + "','" + Hidden_Docid.Value + "','" + (now_index + 1).ToString + "','" + GridView2.Rows(Index + 1).Cells(5).Text + "','" + CType(GridView2.Rows(Index + 1).FindControl("ddl_user"), DropDownList).SelectedValue.ToString + "','1','" + HiddneRecord_id.Value + "','" + Hidden_Status.Value + "','" + DateTime_Str + "') "
                    'Mail通知
                    ds3 = Common.Get_Wf_ApprvUser(Hidden_doc_dp.Value, Hidden_apprv_id.Value, oRow.Cells(0).Text + 1)
                    For i = 0 To ds3.Tables(0).Rows.Count - 1
                        'Common.SendMail(ds3.Tables(0).Rows(i).Item("wf_userid"), "Workflow", docNum_LB.Text, docName_LB.Text, "")
                    Next
                Else
                    SqlTxt = "UPDATE docResoure  SET doc_status = 'PUBLISH' WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                    SqlTxt += " UPDATE wfFormRecord SET wf_status_id ='2' , wf_comment = '" + CType(GridView2.Rows(Index).FindControl("comment_txt"), TextBox).Text + "' WHERE apprv_id='" + Hidden_apprv_id.Value + "' and doc_id='" + Hidden_Docid.Value + "' and wf_status_id ='1' and wf_order ='" + now_index.ToString + "'"
                    ds3 = Common.Check_PublishPower(Hidden_doc_dp.Value, "12120")
                End If
                ElseIf ddl_status.SelectedValue = "9" Then
                    If now_index = 0 Then
                        Common.showMsg(Me.Page, "流程已到填表人，無法再退回！")
                        Return
                    End If
                    SqlTxt = "UPDATE docResoure  SET doc_status = 'ISSUE',locked='0',Del_creator='',next_check_id='' , next_order = ''  WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                    SqlTxt += " UPDATE wfFormRecord SET wf_status_id ='" + ddl_status.SelectedValue + "' , wf_comment = '" + CType(GridView2.Rows(Index).FindControl("comment_txt"), TextBox).Text + "' WHERE apprv_id='" + Hidden_apprv_id.Value + "' and doc_id='" + Hidden_Docid.Value + "' and ( wf_status_id  = '1'  )and wf_order ='" + now_index.ToString + "'"
                    'SqlTxt = "UPDATE docResoure  SET doc_status = 'DEL_REJECT' WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                    'Mail通知
                    'Common.SendMail(docCreator.Text, "Workflow_KO", docNum_LB.Text, docName_LB.Text, User.Identity.Name.ToString)

                End If





            End If


            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

            info_tb.Visible = False
            tb2.Visible = False
            Main_TB.Visible = True
            lb_wfData.Visible = False
    End Sub

    Protected Sub Del_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        Dim Ds As New DataSet

        If CType(Me.GridView1.Rows(Index).FindControl("audit_btn"), Button).Text = "新版審核" Then

            SqlTxt = "UPDATE docResoure  SET doc_status='DELETEDOC' WHERE doc_id='" + GridView1.Rows(Index).Cells(5).Text + "'" 'and next_check_id='' and next_order = '
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", GridView1.Rows(Index).Cells(5).Text, System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DELETEDOC", GridView1.Rows(Index).Cells(2).Text)
            Common.SendMail(User.Identity.Name.ToString, "DelDoc", "", "", "")


        ElseIf CType(Me.GridView1.Rows(Index).FindControl("audit_btn"), Button).Text = "改版審核" Then
            Ds = Common.Get_OldDoc_Rev(GridView1.Rows(Index).Cells(1).Text, GridView1.Rows(Index).Cells(3).Text)
            SqlTxt = "UPDATE docResoure SET doc_status='DELETEDOC', next_check_id='' , next_order = ''  WHERE doc_id='" + GridView1.Rows(Index).Cells(5).Text + "'"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

            If Ds.Tables(0).Rows.Count >= 1 Then
                '恢復文件原來狀態
                SqlTxt = "UPDATE docResoure SET  latest='YES',doc_status = 'ISSUE',locked='0', next_check_id='' , next_order = '' WHERE doc_id='" + Ds.Tables(0).Rows(0).Item("doc_id").ToString + "'"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()

            End If

        ElseIf CType(Me.GridView1.Rows(Index).FindControl("audit_btn"), Button).Text = "廢止審核" Then

            SqlTxt = "UPDATE docResoure SET doc_status = 'ISSUE',locked='0', next_check_id='' , next_order = ''  WHERE doc_id='" + GridView1.Rows(Index).Cells(5).Text + "'"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()


        End If

    End Sub


    Protected Sub Edit_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
End Class
