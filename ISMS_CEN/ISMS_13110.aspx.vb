﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_13110
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer
    Dim uid As String
    Dim uname As String
    Dim douser As String = ConfigurationManager.AppSettings("douser")
    Dim wf15 As String = ConfigurationManager.AppSettings("wf15")
    Dim wf16 As String = ConfigurationManager.AppSettings("wf16")



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        uid = User.Identity.Name.ToString
        hidden_apprvid.Value = wf16
        If Not Page.IsPostBack Then

            Common.ddl_DataRead("SELECT left(user_id,5) as dp  FROM ismsUser group by left(user_id,5) order by dp desc", ddl_dp, 0, 0)
            ddl_douser.Enabled = False
            ddl_eventmgr1.DataSource = Common.Get_User(Hidden_DP.Value)
            ddl_eventmgr1.DataTextField = "user_name"
            ddl_eventmgr1.DataValueField = "user_id"
            ddl_eventmgr1.DataBind()

        ElseIf ddl_dp.SelectedIndex = 0 Then
            ddl_douser.Enabled = False
        End If

        eventUser_lb.Text = "(" + Common.Get_User_Name(uid) + ")"
        eventid_lb.Text = uid
        event_email.Text = Common.Get_UserMail(uid)
    End Sub

    Protected Function Get_eventnum()

        Dim Ds As New DataSet
        Dim evennum As String
        Ds = Common.Get_eventNum(Common.Get_Date(0))
        If Common.FixNull(Ds.Tables(0).Rows(0).Item(0)) = "" Then
            evennum = Common.Get_Date(0).ToString + "01"
        Else
            evennum = Common.Get_Date(0).ToString + Ds.Tables(0).Rows(0).Item(0).ToString + 1
        End If

        Return evennum

    End Function

 




    Protected Sub WF_SqlDS()
        WF_DS.ConnectionString = Common.ConnDBS.ConnectionString
        WF_DS.SelectCommand = "SELECT * FROM workflowName where apprv_id='" + hidden_apprvid.Value + "' order by wf_order, wf_inside_order"

        GridView2.DataSourceID = WF_DS.ID

    End Sub

    Protected Sub GridView2_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowCreated
        e.Row.Cells(5).Visible = False

    End Sub

    Protected Sub Send_WF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Send_WF.Click

        Dim Ds, Ds1 As New DataSet
        Dim DateTime_Str As String = DateTime.Now.ToString("yyyyMMddHHmmss")
        Dim DateTime_Str1 As String = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")

        Ds1 = Common.Get_WfUser(hidden_apprvid.Value, "")

        If ddl_douser.SelectedIndex = 0 Then
            Common.showMsg(Me.Page, "請選擇處理人員")
        Else

            For i = 0 To Ds1.Tables(0).Rows.Count - 1

                If Ds1.Tables(0).Rows(i).Item("chk_name") = douser Then
                    SqlTxt = "UPDATE workflow  SET wf_userid='" + ddl_douser.SelectedValue + "'   where  apprv_id=" + hidden_apprvid.Value + "  and wf_order ='" + Common.FixNull(Ds1.Tables(0).Rows(i).Item("wf_order")).ToString + "' and wf_inside_order='" + Common.FixNull(Ds1.Tables(0).Rows(i).Item("wf_inside_order")).ToString + "'"
                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                End If

            Next


            SqlTxt = " INSERT INTO EventFormMaster" + _
            "(event_number,apprv_id,event_user,eventmgr1,event_tel,event_fax,event_email,event_y,event_m,event_d,event_hh,event_mm,event_y1,event_m1,event_d1,event_hh1,event_mm1,event_desc,douser,dousermgr2,douser_tel,douser_email,status)" + _
            " VALUES ('" + Get_eventnum() + "','" + hidden_apprvid.Value + "','" + LCase(User.Identity.Name) + "','" + LCase(ddl_eventmgr1.SelectedValue) + "','" + event_tel.Text + "','" + event_fax.Text + "','" + event_email.Text + "','" + event_y.Text + "'" + _
             ",'" + event_m.Text + "','" + event_d.Text + "','" + event_hh.Text + "','" + event_mm.Text + "','" + event_y1.Text + "','" + event_m1.Text + "','" + event_d1.Text + "','" + event_hh1.Text + "','" + event_mm1.Text + "','" + event_desc.Text + "','" + LCase(ddl_douser.SelectedValue) + "','" + LCase(ddl_dousermgr2.SelectedValue) + "','" + douser_tel.Text + "','" + douser_email.Text + "','FLOW')"

            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

            Ds = Common.Get_eventFormid()
            Dim Max_id As String = Ds.Tables(0).Rows(0).Item(0)

            SqlTxt = "INSERT INTO wfFormRecord " + _
                 "(apprv_id,doc_id,wf_order,wf_userid,wf_status_id,wf_comment,signdate,record_id,wf_inside_order,doc_status) VALUES" + _
                "('" + hidden_apprvid.Value + "','" + Max_id + "','" + GridView2.Rows(0).Cells(0).Text + "','" + CType(GridView2.Rows(0).FindControl("label_userid"), Label).Text + "','" + CType(GridView2.Rows(0).FindControl("ddl_status"), DropDownList).SelectedValue + "'," + _
                " '" + CType(GridView2.Rows(0).FindControl("comment_txt"), TextBox).Text + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','1','" + GridView2.Rows(0).Cells(5).Text + "','EVENTFORM')"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            'Common.showMsg(Me.Page, "已經送出審核，謝謝!")
            Server.Transfer(Request.Url.PathAndQuery)

        End If


    End Sub

    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound

        Dim Ds As New DataSet

        If e.Row.RowIndex <> -1 Then

            If e.Row.RowIndex = 0 Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(LCase(uid))
                CType(e.Row.FindControl("label_userid"), Label).Text = LCase(uid)
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(0)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
                ' ElseIf e.Row.RowIndex >= 1 Then
                'Ds = Common.Get_Wf_User_ALL(hidden_apprvid.Value, e.Row.Cells(0).Text, e.Row.Cells(5).Text)
                'If e.Row.Cells(1).Text = douser Then
                'CType(e.Row.FindControl("lb_username"), Label).Text = ddl_douser.SelectedItem.Text
                'CType(e.Row.FindControl("label_userid"), Label).Text = ddl_douser.SelectedValue
                'Else
                ' CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(Ds.Tables(0).Rows(0).Item("wf_userid"))
                'CType(e.Row.FindControl("label_userid"), Label).Text = Ds.Tables(0).Rows(0).Item("wf_userid")
                ' End If
            End If

        End If
    End Sub


 

    Protected Sub ddl_dp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_dp.SelectedIndexChanged
        ddl_douser.Items.Clear()
        ddl_dousermgr2.Items.Clear()
        ddl_douser.Items.Add(New ListItem("請選擇處理人員", 0))

        If ddl_dp.SelectedIndex = 0 Then
            ddl_douser.Enabled = False
        Else
            ddl_douser.Enabled = True
            Common.ddl_DataRead("SELECT  *  FROM   ismsUser  where left(user_id,5)='" + ddl_dp.SelectedValue + "'", ddl_douser, 1, 0)
            Common.ddl_DataRead("SELECT  *  FROM   ismsUser  where left(user_id,5)='" + ddl_dp.SelectedValue + "'", ddl_dousermgr2, 1, 0)
        End If
    End Sub

    Protected Sub ddl_douser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_douser.SelectedIndexChanged
        tb2.Visible = True
        WF_SqlDS()
        douser_email.Text = Common.Get_UserMail(ddl_douser.SelectedValue)
    End Sub


  
End Class
