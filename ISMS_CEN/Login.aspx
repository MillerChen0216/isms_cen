﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ISMS_系統登入</title>
</head>

<body  style="margin:0">
    <form id="form1" runat="server" onsubmit="return validateInjection()">
    <div>
        <br />
        <br />
        <br />
        <br />
        <div style="text-align: center">
            <table style="width: 600px">
                <tr>
                    <td align="left" style="font-size: 12px; color: black; height: 18px;">
                        | 版本 202003 |</td>
                    <td align="right" style="font-size: 12px; color: black; height: 18px;">
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/ISMS_doclist.aspx" Target="_blank">ISMS文件列表</asp:HyperLink>&nbsp;</td>
                </tr>
            </table>
        </div>
        <div style="text-align: center">
            <table width=100% height="253px">

                <tr>
                    <td style="width: 100%;  background:#BCD1F6";>  
                      <asp:Panel BackColor="White" runat="server" Height="224px" Width="600px" >
                            <table style="width: 600px; background-color: white; padding-top:35px ;" >
                                <tr>
                                    <td colspan="2" >
                                        <asp:Label ID="Label2" runat="server" Font-Size="20px" Text="資訊安全管理系統" Font-Names="微軟正黑體" Font-Bold="True" ForeColor="DimGray"></asp:Label><br />
                                        <asp:Label ID="Label1" runat="server" Font-Size="16px" Text="Information Security Management System " Font-Names="微軟正黑體" Font-Bold="False" ForeColor="Gray"></asp:Label></td>
                                </tr>
                            </table>
                          <asp:Panel ID="pnl_login" runat="server">
                                <table border="0" cellpadding="4" cellspacing="0" style="border-collapse: collapse; width: 100%;">
                                    <tr>
                                        <td >
                                            &nbsp;<table border="0" cellpadding="0" style="width: 500px">
                                                <tr>
                                                    <td align="right" style="width: 395px">
                                                        <asp:Label ID="UserNameLabel" runat="server" Font-Size="8pt" >使用者帳號：</asp:Label></td>
                                                    <td align="left" style="width: 622px">
                                                        <asp:TextBox ID="UserName" runat="server" Font-Size="9pt" Width="120px" style="TEXT-TRANSFORM: uppercase" ></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                            ErrorMessage="必須提供使用者名稱。" ToolTip="必須提供使用者名稱。">*</asp:RequiredFieldValidator>
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 395px; height: 23px;">
                                                        <asp:Label ID="PasswordLabel" runat="server" Font-Size="8pt" >使用者密碼：</asp:Label></td>
                                                    <td align="left" style="width: 622px; height: 23px;">
                                                        <asp:TextBox ID="Password" runat="server" Font-Size="9pt" Width="120px" TextMode="Password"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                                            ErrorMessage="必須提供密碼。" ToolTip="必須提供密碼。">*</asp:RequiredFieldValidator>
                                                        <asp:Button ID="LoginButton" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                                                            BorderStyle="Solid" BorderWidth="1px" CommandName="Login" Font-Names="Verdana"
                                                            Font-Size="10pt" ForeColor="#284775" Text="登入系統" OnClick="LoginButton_Click" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style="width: 395px; height: 23px;" colspan="4">
                                                        <asp:Label ID="lab_error" runat="server" ForeColor="Red" Font-Size="8pt" Text="您的登入嘗試失敗。請再試一次。" Visible ="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" >
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="3" style="color: red; height: 16px;">
                                                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                    </tr>
                                </table>
                              </asp:Panel>

                            <asp:Panel ID="pnl_update" runat="server" Visible="false">
                                <table border="0" cellpadding="4" cellspacing="0" style="border-collapse: collapse; width: 100%;">
                                    <tr>
                                        <td >
                                            &nbsp;<table border="0" cellpadding="0" style="width: 500px">
                                                <tr>
                                                    <td align="right" style="width: 395px">
                                                        <asp:Label ID="Label3" runat="server" Font-Size="8pt" >請輸入新密碼：</asp:Label></td>
                                                    <td align="left" style="width: 622px">
                                                        <asp:TextBox ID="txt_update_pwd1" runat="server" Font-Size="9pt" Width="120px" TextMode="Password" ></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_update_pwd1"
                                                            ErrorMessage="必須輸入新密碼。" ToolTip="必須輸入新密碼。" >*</asp:RequiredFieldValidator>
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 395px; height: 23px;">
                                                        <asp:Label ID="Label4" runat="server" Font-Size="8pt" >請再重新輸入一次密碼：</asp:Label></td>
                                                    <td align="left" style="width: 622px; height: 23px;">
                                                        <asp:TextBox ID="txt_update_pwd2" runat="server" Font-Size="9pt" Width="120px" TextMode="Password"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_update_pwd2"
                                                            ErrorMessage="必須再輸入新密碼一次。" ToolTip="必須再輸入新密碼一次。" >*</asp:RequiredFieldValidator>
                                                        <asp:Button ID="btn_update" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                                                            BorderStyle="Solid" BorderWidth="1px" CommandName="Login" Font-Names="Verdana"
                                                            Font-Size="10pt" ForeColor="#284775" Text="確認" OnClick="UpdateButton_Click" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style="width: 395px; height: 23px;" colspan="4">
                                                        <asp:Label ID="lab_pwderror" runat="server" ForeColor="Red" Font-Size="8pt" Text="您輸入的密碼不同，請再重新輸入。" Visible ="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" >
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="3" style="color: red; height: 16px;">
                                                        <asp:Literal ID="Literal1" runat="server" EnableViewState="False"></asp:Literal>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                    </tr>
                                </table>
                              </asp:Panel>
                                <!--<a href="#" onClick="window.open('../ISMS_List/ISMS_0000.aspx','ISMS_發行文件列表',config='height=50,width=850,scrollbars=yes')">發行文件列表</a> -->
                         </asp:Panel>

                    </td>
                </tr>
            </table>
        </div>    
    </div>
        <div style="text-align: center">
            <table style="width: 600px">
                <tr>
                    <td style="font-size: 9px; color: darkgray" align="center">
                        <table style="width: 600px">
                            <tr>
                                <td align="center" style="font-size: 9px; color: darkgray">
                                    <asp:Image ID="Image1" runat="server" Height="36px" ImageAlign="Right" ImageUrl="~/Images/logo_01.png"
                                        Width="177px" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
     
    </form>
</body>
</html>
