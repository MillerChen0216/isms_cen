﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports System.Collections.Generic

Partial Class ISMS_40150
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim Ds As New DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Hidden_DP.Value = Me.Get_DP
       
        FG_SqlDS()

    End Sub
    Protected Sub FG_SqlDS()

        sds_Form_Group.ConnectionString = Common.ConnDBS.ConnectionString
        sds_Form_Group.SelectCommand = "SELECT distinct approvalId,Form_Name FROM Form_Group GROUP BY approvalId,Form_Name "
        GridView1.DataSourceID = sds_Form_Group.ID

    End Sub
    Protected Sub FF_SqlDS()

        sds_Form_Fun.ConnectionString = Common.ConnDBS.ConnectionString
        sds_Form_Fun.SelectCommand = "SELECT a.sText,a.sValue,a.sURL,b.approvalId,b.Form_Name,CASE WHEN sText = sValue THEN sValue ELSE sValue + '(' +sText +')' END AS Value FROM Menu_TreeView a,Form_Group b WHERE b.approvalId='" + hid_approvalid_glo.Value + "' and a.sURL = b.sURL group by a.sText,a.sValue,a.sURL,b.approvalId,b.Form_Name  "
        GridView2.DataSourceID = sds_Form_Fun.ID

    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        If e.Row.RowIndex <> -1 Then
            Dim Form_Fun As String = ""
            Dim Ds As DataSet
            Dim lab_Form_Fun As Label = CType(e.Row.FindControl("lab_Form_Fun"), Label)
            Dim hid_approvalid As HiddenField = CType(e.Row.FindControl("hid_approvalid"), HiddenField)

            Ds = Common.Get_All_Form_GroupFun(hid_approvalid.Value)

            For i = 0 To Ds.Tables(0).Rows.Count - 1
                If Form_Fun <> "" Then
                    Form_Fun = Form_Fun + Ds.Tables(0).Rows(i).Item("Value").ToString + "<br/>"
                Else
                    Form_Fun = Ds.Tables(0).Rows(i).Item("Value").ToString + "<br/>"
                End If
            Next
            lab_Form_Fun.Text = Form_Fun
           
        End If
        
    End Sub
    Protected Sub btn_Set_FormFun_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        hid_approvalid_glo.Value = CType(oRow.Cells(2).FindControl("hid_approvalid"), HiddenField).Value
        FF_SqlDS()
        TB_Set_FormGroup.Visible = False : TB_Show_FormGroup.Visible = False : TB_Show_FormFun.Visible = True

    End Sub

    Protected Sub btn_Del_FormFun_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex

        SqlTxt = "DELETE FROM Form_Group WHERE sURL='" + CType(oRow.Cells(1).FindControl("hid_sURL"), HiddenField).Value + "'"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        FF_SqlDS()

    End Sub
    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click

        TB_Set_FormGroup.Visible = True : TB_Show_FormGroup.Visible = False : TB_Show_FormFun.Visible = False
        lab_FormName.Visible = False : txt_Ins_Form_Name.Visible = True
        txt_Ins_Form_Name.Text = ""
        lab_InsForm.Text = "文件/表單管理_新增"
        lb_Form.DataSource = Common.Get_All_FormNotInGroup()
        lb_Form.DataTextField = "Value"
        lb_Form.DataValueField = "sURL"
        lb_Form.DataBind()
        FG_SqlDS()
    End Sub
    Protected Sub btn_Ins_FormGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Ins_FormGroup.Click

        If txt_Ins_Form_Name.Visible = True Then '新增文件/表單
            Dim list As New List(Of String)
            Dim max_approvalid As String

            For Each item As ListItem In lb_Form.Items
                If item.Selected Then
                    list.Add(item.Value)
                End If
            Next
            If txt_Ins_Form_Name.Text = "" Then
                Common.showMsg(Me.Page, "請選擇文件/表單")
                Return
            End If
            If list.Count < 1 Then
                Common.showMsg(Me.Page, "請選擇文件/表單")
                Return
            End If

            Ds = Common.Get_Max_Approvalid()
            Dim Ds_AllDp As DataSet = Common.ISMS_dpChange("")
            max_approvalid = Common.FixNull1(Ds.Tables(0).Rows(0).Item("max_approvalid")) + 1
            For n As Integer = 0 To Ds_AllDp.Tables(0).Rows.Count - 1
                For i As Integer = 0 To list.Count - 1 '新增至所有所別
                    SqlTxt = "INSERT INTO Form_Group (approvalId,sURL,Form_Name,dp) VALUES ('" + max_approvalid + "','" + list(i).ToString + "','" + txt_Ins_Form_Name.Text + "','" + Ds_AllDp.Tables(0).Rows(n).Item("dp_id").ToString + "')"
                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                Next
            Next
           
            Common.showMsg(Me.Page, "新增成功!")
            FG_SqlDS()
            TB_Set_FormGroup.Visible = False : TB_Show_FormGroup.Visible = True : TB_Show_FormFun.Visible = False
        Else '新增文件表單功能

            Dim list As New List(Of String)
            Dim Ds As DataSet
            For Each item As ListItem In lb_Form.Items
                If item.Selected Then
                    list.Add(item.Value)
                End If
            Next
            If list.Count < 1 Then
                Common.showMsg(Me.Page, "請選擇文件/表單")
                Return
            End If
            Dim Ds_AllDp As DataSet = Common.ISMS_dpChange("")
            Ds = Common.Get_Form_Data(lab_FormName.Text)
            For n As Integer = 0 To Ds_AllDp.Tables(0).Rows.Count - 1
                For i As Integer = 0 To list.Count - 1 '新增至所有所別

                    SqlTxt = "INSERT INTO Form_Group (approvalId,sURL,Form_Name,dp) VALUES ('" + Ds.Tables(0).Rows(0).Item("approvalId") + "','" + list(i).ToString + "','" + lab_FormName.Text + "','" + Ds_AllDp.Tables(0).Rows(n).Item("dp_id").ToString + "')"
                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                Next
            Next
            
            Common.showMsg(Me.Page, "新增成功!")
            FF_SqlDS()
            TB_Set_FormGroup.Visible = False : TB_Show_FormGroup.Visible = True : TB_Show_FormFun.Visible = False

        End If
       
    End Sub
    Protected Sub btn_Ins_FormFun_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Ins_FormFun.Click

        Dim Ds As DataSet

        TB_Set_FormGroup.Visible = True : TB_Show_FormGroup.Visible = False : TB_Show_FormFun.Visible = False
        lab_FormName.Visible = True : txt_Ins_Form_Name.Visible = False

        Ds = Common.Get_Form_Name(CType(GridView2.Rows(0).FindControl("hid_sURL"), HiddenField).Value)
        lab_FormName.Text = Ds.Tables(0).Rows(0).Item("Form_Name")
        lab_InsForm.Text = "文件/表單功能_新增"
        lb_Form.DataSource = Common.Get_All_FormNotInGroup()
        lb_Form.DataTextField = "Value"
        lb_Form.DataValueField = "sURL"
        lb_Form.DataBind()
    End Sub
    Protected Sub btn_GoBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_GoBack.Click

        If txt_Ins_Form_Name.Visible = True Then '新增文件/表單
            TB_Set_FormGroup.Visible = False : TB_Show_FormGroup.Visible = True : TB_Show_FormFun.Visible = False
        Else
            TB_Set_FormGroup.Visible = False : TB_Show_FormGroup.Visible = False : TB_Show_FormFun.Visible = True
        End If
        
    End Sub
    Protected Sub btn_GoBack2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_GoBack2.Click

        TB_Set_FormGroup.Visible = False : TB_Show_FormGroup.Visible = True : TB_Show_FormFun.Visible = False

    End Sub

End Class
