﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_13140
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer
    Dim uid As String
    Dim uname As String
    Dim wf17 As String = ConfigurationManager.AppSettings("wf17")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        uid = User.Identity.Name.ToString
        hidden_apprvid.Value = wf17


        If Not Page.IsPostBack Then
            Common.ddl_DataRead("SELECT  *  FROM   ismsUser  where left(user_id,5)='" + Hidden_DP.Value + "'", ddl_dousermgr0, 1, 0)
            Common.ddl_DataRead("SELECT left(user_id,5) as dp  FROM ismsUser group by left(user_id,5) order by dp desc", ddl_dp, 0, 0)
            ddl_douser1.Enabled = False
            Common.ddl_DataRead("SELECT left(user_id,5) as dp  FROM ismsUser group by left(user_id,5) order by dp desc", ddl_dp1, 0, 0)
            ddl_douser2.Enabled = False
        ElseIf ddl_dp.SelectedIndex = 0 Then
            ddl_douser1.Enabled = False
        ElseIf ddl_dp1.SelectedIndex = 0 Then
            ddl_douser2.Enabled = False

        End If

        douser.Text = "(" + Common.Get_User_Name(uid) + ")"
        douser_id.Text = uid
        WF_SqlDS()
        tb2.Visible = True

    End Sub

    Protected Sub WF_SqlDS()
        WF_DS.ConnectionString = Common.ConnDBS.ConnectionString
        WF_DS.SelectCommand = "SELECT * FROM workflowName where apprv_id='" + hidden_apprvid.Value + "'  order by wf_order, wf_inside_order"
        GridView2.DataSourceID = WF_DS.ID
    End Sub
    Protected Sub GridView2_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowCreated
        e.Row.Cells(5).Visible = False

    End Sub
    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound

        Dim Ds As New DataSet

        If e.Row.RowIndex <> -1 Then

            If e.Row.RowIndex = 0 Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(User.Identity.Name.ToString)
                CType(e.Row.FindControl("label_userid"), Label).Text = User.Identity.Name.ToString
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(0)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
            ElseIf e.Row.RowIndex >= 1 Then
                Ds = Common.Get_Wf_User(Hidden_DP.Value, hidden_apprvid.Value, e.Row.Cells(0).Text, e.Row.Cells(5).Text)
                'CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(Ds.Tables(0).Rows(0).Item("wf_userid"))
                'CType(e.Row.FindControl("label_userid"), Label).Text = Ds.Tables(0).Rows(0).Item("wf_userid")

            End If

        End If


    End Sub

    Protected Sub Send_WF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Send_WF.Click


        If ddl_dp.SelectedIndex = 0 Or ddl_dp1.SelectedIndex = 0 Then
            Common.showMsg(Me.Page, "請選擇業管人員與承辦人員，謝謝!")

        Else



            Dim Ds, Ds1 As New DataSet
            Dim DateTime_Str As String = DateTime.Now.ToString("yyyyMMddHHmmss")
            Dim DateTime_Str1 As String = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")

            Ds1 = Common.Get_WfUser(hidden_apprvid.Value, "")



            SqlTxt = " INSERT INTO CurrentFormMaster (apprv_id,event_num,event_source,current_desc,douser,dousermgr,douser1,dousermgr1,douser2,dousermgr2,status)  VALUES" + _
            "('" + hidden_apprvid.Value + "','" + event_num.Text + "','" + event_source.Text + "','" + current_desc.Text + "','" + LCase(douser_id.Text) + "','" + LCase(ddl_dousermgr0.SelectedValue) + "','" + LCase(ddl_douser1.SelectedValue) + "','" + LCase(ddl_dousermgr1.SelectedValue) + "','" + LCase(ddl_douser2.SelectedValue) + "','" + LCase(ddl_dousermgr2.SelectedValue) + "','FLOW')"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

            Ds = Common.Get_CurrentFormid()
            Dim Max_id As String = Ds.Tables(0).Rows(0).Item(0)

            SqlTxt = "INSERT INTO wfFormRecord " + _
                 "(apprv_id,doc_id,wf_order,wf_userid,wf_status_id,wf_comment,signdate,record_id,wf_inside_order,doc_status) VALUES" + _
                "('" + hidden_apprvid.Value + "','" + Max_id + "','" + GridView2.Rows(0).Cells(0).Text + "','" + CType(GridView2.Rows(0).FindControl("label_userid"), Label).Text + "','" + CType(GridView2.Rows(0).FindControl("ddl_status"), DropDownList).SelectedValue + "'," + _
                " '" + CType(GridView2.Rows(0).FindControl("comment_txt"), TextBox).Text + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','1','" + GridView2.Rows(0).Cells(5).Text + "','CURRENTFORM')"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            'Common.showMsg(Me.Page, "已經送出審核，謝謝!")
            Server.Transfer(Request.Url.PathAndQuery)

        End If

    End Sub


    Protected Sub ddl_dp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_dp.SelectedIndexChanged
        ddl_douser1.Items.Clear()
        ddl_dousermgr1.Items.Clear()
        ddl_douser1.Items.Add(New ListItem("請選擇業管人員", 0))

        If ddl_dp.SelectedIndex = 0 Then
            ddl_douser1.Enabled = False
        Else
            ddl_douser1.Enabled = True
            Common.ddl_DataRead("SELECT  *  FROM   ismsUser  where left(user_id,5)='" + ddl_dp.SelectedValue + "'", ddl_douser1, 1, 0)
            Common.ddl_DataRead("SELECT  *  FROM   ismsUser  where left(user_id,5)='" + ddl_dp.SelectedValue + "'", ddl_dousermgr1, 1, 0)
        End If
    End Sub


    Protected Sub ddl_douser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_douser1.SelectedIndexChanged
        tb2.Visible = True
        'WF_SqlDS()
        'douser_email.Text = Common.Get_UserMail(ddl_douser.SelectedValue)
    End Sub

    Protected Sub ddl_dp1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_dp1.SelectedIndexChanged
        ddl_douser2.Items.Clear()
        ddl_dousermgr2.Items.Clear()
        ddl_douser2.Items.Add(New ListItem("請選擇承辦人員", 0))

        If ddl_dp1.SelectedIndex = 0 Then
            ddl_douser2.Enabled = False
        Else
            ddl_douser2.Enabled = True
            Common.ddl_DataRead("SELECT  *  FROM   ismsUser  where left(user_id,5)='" + ddl_dp1.SelectedValue + "'", ddl_douser2, 1, 0)
            Common.ddl_DataRead("SELECT  *  FROM   ismsUser  where left(user_id,5)='" + ddl_dp1.SelectedValue + "'", ddl_dousermgr2, 1, 0)
        End If

    End Sub
End Class
