﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_10163
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim uid As String
    Dim uname As String

    Protected Sub SqlDataSource()
        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = "select cid,typeValue from docType2 where  pid='" + ddl_type.SelectedValue + "'  ORDER BY cid"
        GridView1.DataSourceID = SqlDataSource1.ID
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP

        If Not Page.IsPostBack Then
            ddl_type.DataSource = Common.Get_docType1(Hidden_DP.Value)
            ddl_type.DataTextField = "typeValue"
            ddl_type.DataValueField = "id"
            ddl_type.DataBind()
        End If

        uid = LCase(Request.QueryString("uid"))
        uname = Request.QueryString("uname")
        SqlDataSource()
    End Sub



    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click

        SqlTxt = "INSERT INTO docType2  (pid,typeValue) VALUES ('" + ddl_type.SelectedValue + "' , '" + type_value_ins.Text + "' ) "
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        'Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", asset_name_Ins.Text)

        GridView1.DataBind()
        type_value_ins.Text = ""
        Show_TB.Visible = True
        Ins_TB.Visible = False

    End Sub

    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click


        SqlTxt = "UPDATE docType2  SET  typeValue ='" + type_value.Text + "' where cid ='" + hidden_id.Value + "'"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()

        GridView1.DataBind()
        Show_TB.Visible = True
        Edit_TB.Visible = False
        'Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", asset_name_Edit.Text)


    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)

        hidden_id.Value = SelectedRow.Cells(1).Text


        Select Case e.CommandName

            Case "Del_Btn"
                SqlTxt = "DELETE FROM docType2 where cid ='" + hidden_id.Value + "' "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                'Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DEL", SelectedRow.Cells(1).Text)
                GridView1.DataBind()


            Case "Edit_Btn"

                Edit_TB.Visible = True
                Show_TB.Visible = False

                type_value.Text = SelectedRow.Cells(0).Text

        End Select
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(1).Visible = False
        e.Row.Cells(3).Visible = False
    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim oDataRow As Data.DataRowView
        Dim sShowField As String
        Dim sMessage As String
        If e.Row.RowIndex <> -1 Then
            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = oDataRow.Item("typeValue").ToString()
            sMessage = String.Format("您確定要刪除 [{0}] 嗎?", sShowField)
            CType(e.Row.Cells(3).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"
        End If
    End Sub


    Protected Sub ddl_type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_type.SelectedIndexChanged

    End Sub

    Protected Sub asset_groupCn_Ins_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles type_value_ins.TextChanged

    End Sub

    Protected Sub Show_Insert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert.Click
        Show_TB.Visible = False
        Ins_TB.Visible = True
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Show_TB.Visible = False
        Ins_TB.Visible = False
        Edit_TB.Visible = False
        Table1.Visible = True
        Table2.Visible = False

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Show_TB.Visible = False
        Ins_TB.Visible = False
        Edit_TB.Visible = False
        Table1.Visible = False
        Table2.Visible = True
        typevalue.Text = ddl_type.SelectedItem.Text
    End Sub

    Protected Sub Button5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button5.Click
        SqlTxt = "DELETE FROM docType1 where id ='" + ddl_type.SelectedValue + "' "
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        Server.Transfer(Request.Url.PathAndQuery)

    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        SqlTxt = "INSERT INTO docType1  (dp,typeValue) VALUES ('" + Hidden_DP.Value + "', '" + typevalue_ins.Text + "' ) "
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        Server.Transfer(Request.Url.PathAndQuery)
    End Sub

    Protected Sub Button4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button4.Click
        SqlTxt = "UPDATE docType1  SET  typeValue ='" + typevalue.Text + "' where id ='" + ddl_type.SelectedValue + "'"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        Server.Transfer(Request.Url.PathAndQuery)
    End Sub
End Class
