﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_20120
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP

        If Not Page.IsPostBack Then
            ddl_Asset.DataSource = Common.Get_Asset()
            ddl_Asset.DataValueField = "asset_id"
            ddl_Asset.DataTextField = "asset_name"
            ddl_Asset.DataBind()
        End If


        SqlDataSource()
        Dim ds As New DataSet
        ds = Common.Get_Asset()
        If ds.Tables(0).Rows.Count = 0 Then
            Server.Transfer("ISMS_20110.aspx")
        End If


    End Sub

    Protected Sub SqlDataSource()
        SqlDataSource1.ConnectionString = Conn.ConnectionString
        SqlDataSource1.SelectCommand = "SELECT assitem_id,assitem_name FROM assetItem WHERE dp='" + Hidden_DP.Value + "' and  asset_id=" + ddl_Asset.SelectedValue + " ORDER BY assitem_id"
        GridView1.DataSourceID = SqlDataSource1.ID
        Dim KeyNames() As String = {"assitem_id"}
        GridView1.DataKeyNames = KeyNames
    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)

        Select Case e.CommandName

            Case "Edit_Btn"

                Edit_TB.Visible = True
                Show_TB.Visible = False
                Dim sAssetId As String

                Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM [assetItem] where  dp='" + Hidden_DP.Value + "' and  assitem_id ='" + SelectedRow.Cells(0).Text + "'", Conn)
                Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
                mycmd.SelectCommand = selectCMD
                Dim myDs As DataSet = New DataSet()

                mycmd.Fill(myDs, "assetItem")


                ddl_Asset_upd.DataSource = Common.Get_Asset()
                ddl_Asset_upd.DataValueField = "asset_id"
                ddl_Asset_upd.DataTextField = "asset_name"
                ddl_Asset_upd.DataBind()

                ddl_Dept.DataSource = Common.Get_dept(Hidden_DP.Value)
                ddl_Dept.DataValueField = "dept_id"
                ddl_Dept.DataTextField = "dept_name"
                ddl_Dept.DataBind()

                'ddl_user_id.DataSource = Common.Get_User(Hidden_DP.Value)
                'ddl_user_id.DataTextField = "user_name"
                'ddl_user_id.DataValueField = "user_id"
                'ddl_user_id.DataBind()

                'ddl_holder.DataSource = Common.Get_User(Hidden_DP.Value)
                'ddl_holder.DataTextField = "user_name"
                'ddl_holder.DataValueField = "user_id"
                'ddl_holder.DataBind()

                sAssetId = myDs.Tables("assetItem").Rows(0).Item("asset_id")

                ddl_assgroup_upd.DataSource = Common.Get_assgroup_id(sAssetId)
                ddl_assgroup_upd.DataValueField = "assgroup_id"
                ddl_assgroup_upd.DataTextField = "assgroup_name"
                ddl_assgroup_upd.DataBind()
                If myDs.Tables("assetItem").Rows(0).Item("assgroup_id").ToString <> "" Then
                    ddl_assgroup_upd.SelectedValue = myDs.Tables("assetItem").Rows(0).Item("assgroup_id").ToString
                End If

                ddl_Confidentiality.DataSource = Common.Get_assetValueList("C", sAssetId)
                ddl_Confidentiality.DataValueField = "asset_score"
                ddl_Confidentiality.DataTextField = "asset_score"
                ddl_Confidentiality.DataBind()


                ddl_Integrity.DataSource = Common.Get_assetValueList("I", sAssetId)
                ddl_Integrity.DataValueField = "asset_score"
                ddl_Integrity.DataTextField = "asset_score"
                ddl_Integrity.DataBind()

                ddl_Availability.DataSource = Common.Get_assetValueList("A", sAssetId)
                ddl_Availability.DataValueField = "asset_score"
                ddl_Availability.DataTextField = "asset_score"
                ddl_Availability.DataBind()

                ddl_Asset_upd.SelectedValue = myDs.Tables("assetItem").Rows(0).Item("asset_id")
                ddl_Dept.SelectedValue = myDs.Tables("assetItem").Rows(0).Item("dept_id")

                assitem_id.Text = myDs.Tables("assetItem").Rows(0).Item("assitem_id")
                assitem_name.Text = myDs.Tables("assetItem").Rows(0).Item("assitem_name")
                item_amount.Text = Common.FixNull(myDs.Tables("assetItem").Rows(0).Item("item_amount"))
                use_amount.Text = Common.FixNull(myDs.Tables("assetItem").Rows(0).Item("use_amount"))

                If Common.FixNull(myDs.Tables("assetItem").Rows(0).Item("user_id")) = "" Then
                    'ddl_user_id.Items.Insert(0, "")
                    user_id.Text = ""
                Else
                    'ddl_user_id.SelectedValue = myDs.Tables("assetItem").Rows(0).Item("user_id")
                    user_id.Text = myDs.Tables("assetItem").Rows(0).Item("user_id")
                End If


                If Common.FixNull(myDs.Tables("assetItem").Rows(0).Item("holder")) = "" Then
                    'ddl_holder.Items.Insert(0, "")
                    holder.Text = ""
                Else
                    '    ddl_holder.SelectedValue = myDs.Tables("assetItem").Rows(0).Item("holder")
                    holder.Text = myDs.Tables("assetItem").Rows(0).Item("holder")
                End If
                location.Text = myDs.Tables("assetItem").Rows(0).Item("location")

                ddl_Confidentiality.SelectedValue = myDs.Tables("assetItem").Rows(0).Item("Confidentiality")
                ddl_Integrity.SelectedValue = myDs.Tables("assetItem").Rows(0).Item("Integrity")
                ddl_Availability.SelectedValue = myDs.Tables("assetItem").Rows(0).Item("Availability")

                ddl_Evaluate.SelectedValue = myDs.Tables("assetItem").Rows(0).Item("Evaluate")
                ddl_expire.SelectedValue = myDs.Tables("assetItem").Rows(0).Item("expire")

                If myDs.Tables("assetItem").Rows(0).Item("expire_date") = "1900-01-01 00:00:00.000" Or Common.FixNull(myDs.Tables("assetItem").Rows(0).Item("expire_date")) = "" Then
                    expire_date.Text = ""
                Else
                    expire_date.Text = myDs.Tables("assetItem").Rows(0).Item("expire_date")
                End If

                assitem_desc.Text = myDs.Tables("assetItem").Rows(0).Item("assitem_desc")



                Dim Ds, Ds1, Ds3 As New DataSet
                Ds = Common.Get_assetValueListname("C", ddl_Asset_upd.SelectedValue, ddl_Confidentiality.SelectedValue)
                Cedit_lb.Text = Ds.Tables(0).Rows(0).Item("asset_desc")

                Ds = Common.Get_assetValueListname("I", ddl_Asset_upd.SelectedValue, ddl_Integrity.SelectedValue)
                Iedit_lb.Text = Ds.Tables(0).Rows(0).Item("asset_desc")

                Ds = Common.Get_assetValueListname("A", ddl_Asset_upd.SelectedValue, ddl_Availability.SelectedValue)
                Aedit_lb.Text = Ds.Tables(0).Rows(0).Item("asset_desc")




            Case "Del_Btn"
                SqlTxt = "DELETE FROM [assetItem] WHERE assitem_id ='" + SelectedRow.Cells(0).Text + "' and dp='" + Hidden_DP.Value + "' "

                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DEL", SelectedRow.Cells(0).Text)

                GridView1.DataBind()

        End Select

    End Sub

    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click
        'If assitem_id.Text = "" Or assitem_name.Text = "" Then
        '    Common.showMsg(Me.Page, "代碼、名稱不允許空白")
        'Else
        SqlTxt = "UPDATE [assetItem] SET [assitem_name] ='" + assitem_name.Text + "', [dept_id] =" + ddl_Dept.SelectedValue + ", [item_amount]=" + Common.SetNull(item_amount.Text) + ", [use_amount]=" + Common.SetNull(use_amount.Text) + ", [user_id]='" + user_id.Text + "',[holder]='" + holder.Text + "',[location]='" + location.Text + "',[Confidentiality]=" + ddl_Confidentiality.SelectedValue + ", [Integrity]=" + ddl_Integrity.SelectedValue + ",[Availability]=" + ddl_Availability.SelectedValue + ",[evaluate]='" + ddl_Evaluate.SelectedValue + "',[expire]='" + ddl_expire.SelectedValue + "',[expire_date]=CAST('" + expire_date.Text + "'AS datetime),[assitem_desc]='" + assitem_desc.Text + "',[assgroup_id]='" + ddl_assgroup_upd.SelectedValue.ToString + "' WHERE assitem_id = '" + assitem_id.Text + "' and dp='" + Hidden_DP.Value + "'"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", assitem_id.Text)

        GridView1.DataBind()
        Show_TB.Visible = True
        Edit_TB.Visible = False
        'End If

    End Sub

    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        If ddl_Confidentiality_ins.SelectedIndex = 0 Or ddl_Integrity_ins.SelectedIndex = 0 Or ddl_Availability_ins.SelectedIndex = 0 Then
            Common.showMsg(Me.Page, "請選擇機密性、完整性與可用性的值")
        Else
            SqlTxt = "INSERT INTO [assetItem] ([dp],[asset_id],[assitem_id],[assitem_name],[dept_id],[item_amount],[use_amount],[user_id],[holder],[location],[Confidentiality],[Integrity],[Availability],[evaluate],[expire],[expire_date],[assitem_desc],[assgroup_id]) VALUES ('" + Hidden_DP.Value + "'," + ddl_Asset_ins.SelectedValue + ",'" + assitem_id_ins.Text + "','" + assitem_name_ins.Text + "'," + ddl_Dept_Ins.SelectedValue + "," + Common.SetNull(item_amount_ins.Text) + "," + Common.SetNull(use_amount_ins.Text) + ",'" + user_id_ins.Text + "','" + holder_ins.Text + "','" + location_ins.Text + "'," + ddl_Confidentiality_ins.SelectedValue + "," + ddl_Integrity_ins.SelectedValue + "," + ddl_Availability_ins.SelectedValue + ",'" + ddl_Evaluate_ins.SelectedValue + "','" + ddl_expire_ins.SelectedValue + "', '" + expire_date_ins.Text + "','" + assitem_desc_ins.Text + "','" + ddl_assgroup_Ins.SelectedValue + "')"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", assitem_id_ins.Text)
            Server.Transfer(Request.Url.PathAndQuery)
        End If

    End Sub

    Protected Sub Show_Insert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert.Click
        Show_TB.Visible = False
        ins_TB.Visible = True
        Show_Insertddl()
    End Sub
    Protected Sub Show_Insertddl()

        Dim sAssetId As String
        ddl_Asset_ins.DataSource = Common.Get_Asset()
        ddl_Asset_ins.DataValueField = "asset_id"
        ddl_Asset_ins.DataTextField = "asset_name"
        ddl_Asset_ins.DataBind()

        ddl_Dept_Ins.DataSource = Common.Get_dept(Hidden_DP.Value)
        ddl_Dept_Ins.DataValueField = "dept_id"
        ddl_Dept_Ins.DataTextField = "dept_name"
        ddl_Dept_Ins.DataBind()

        'ddl_user_id_ins.DataSource = Common.Get_User(Hidden_DP.Value)
        'ddl_user_id_ins.DataTextField = "user_name"
        'ddl_user_id_ins.DataValueField = "user_id"
        'ddl_user_id_ins.DataBind()
        'ddl_user_id_ins.Items.Insert(0, "")

        'ddl_holder_ins.DataSource = Common.Get_User(Hidden_DP.Value)
        'ddl_holder_ins.DataTextField = "user_name"
        'ddl_holder_ins.DataValueField = "user_id"
        'ddl_holder_ins.DataBind()
        'ddl_holder_ins.Items.Insert(0, "")

        sAssetId = ddl_Asset_ins.SelectedValue

        ddl_assgroup_Ins.DataSource = Common.Get_assgroup_id(sAssetId)
        ddl_assgroup_Ins.DataValueField = "assgroup_id"
        ddl_assgroup_Ins.DataTextField = "assgroup_name"
        ddl_assgroup_Ins.DataBind()

        ddl_Confidentiality_ins.DataSource = Common.Get_assetValueList("C", sAssetId)
        ddl_Confidentiality_ins.DataValueField = "asset_score"
        ddl_Confidentiality_ins.DataTextField = "asset_score"
        ddl_Confidentiality_ins.DataBind()
        ddl_Confidentiality_ins.Items.Insert(0, "請選擇")

        ddl_Integrity_ins.DataSource = Common.Get_assetValueList("I", sAssetId)
        ddl_Integrity_ins.DataValueField = "asset_score"
        ddl_Integrity_ins.DataTextField = "asset_score"
        ddl_Integrity_ins.DataBind()
        ddl_Integrity_ins.Items.Insert(0, "請選擇")

        ddl_Availability_ins.DataSource = Common.Get_assetValueList("A", sAssetId)
        ddl_Availability_ins.DataValueField = "asset_score"
        ddl_Availability_ins.DataTextField = "asset_score"
        ddl_Availability_ins.DataBind()
        ddl_Availability_ins.Items.Insert(0, "請選擇")

        ddl_Asset_ins.SelectedValue = ddl_Asset.SelectedValue


    End Sub

    Protected Sub ddl_Confidentiality_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Confidentiality.SelectedIndexChanged
        Dim Ds As New DataSet
        Ds = Common.Get_assetValueListname("C", ddl_Asset_upd.SelectedValue, ddl_Confidentiality.SelectedValue)
        Cedit_lb.Text = Ds.Tables(0).Rows(0).Item("asset_desc")

    End Sub

    Protected Sub ddl_Integrity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Integrity.SelectedIndexChanged
        Dim Ds As New DataSet
        Ds = Common.Get_assetValueListname("I", ddl_Asset_upd.SelectedValue, ddl_Integrity.SelectedValue)
        Iedit_lb.Text = Ds.Tables(0).Rows(0).Item("asset_desc")

    End Sub

    Protected Sub ddl_Availability_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Availability.SelectedIndexChanged
        Dim Ds As New DataSet
        Ds = Common.Get_assetValueListname("A", ddl_Asset_upd.SelectedValue, ddl_Availability.SelectedValue)
        Aedit_lb.Text = Ds.Tables(0).Rows(0).Item("asset_desc")

    End Sub
  
   
    Protected Sub expire_date_ins_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles expire_date_ins.TextChanged
        If ddl_expire_ins.SelectedValue = "N" Then
            Common.showMsg(Me.Page, "是否報廢選項請選""是""")
            expire_date_ins.Text = ""
        End If

    End Sub

    Protected Sub ddl_expire_ins_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_expire_ins.SelectedIndexChanged
        expire_date_ins.Text = ""
    End Sub

    Protected Sub ddl_expire_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_expire.SelectedIndexChanged
        expire_date.Text = ""

    End Sub

    Protected Sub expire_date_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles expire_date.TextChanged

        If ddl_expire.SelectedValue = "N" Then
            Common.showMsg(Me.Page, "是否報廢選項請選""是""")
            expire_date.Text = ""
        End If

    End Sub




    Protected Sub ddl_Confidentiality_ins_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Confidentiality_ins.SelectedIndexChanged
        Dim Ds As New DataSet
        If ddl_Confidentiality_ins.SelectedIndex >= 1 Then
            Ds = Common.Get_assetValueListname("C", ddl_Asset_ins.SelectedValue, ddl_Confidentiality_ins.SelectedValue)
            CIns_lb.Text = Ds.Tables(0).Rows(0).Item("asset_desc")
        Else
            CIns_lb.Text = ""
        End If
    End Sub

    Protected Sub ddl_Integrity_ins_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Integrity_ins.SelectedIndexChanged
        Dim Ds As New DataSet
        If ddl_Integrity_ins.SelectedIndex >= 1 Then
            Ds = Common.Get_assetValueListname("I", ddl_Asset_ins.SelectedValue, ddl_Integrity_ins.SelectedValue)
            IIns_lb.Text = Ds.Tables(0).Rows(0).Item("asset_desc")
        Else
            IIns_lb.Text = ""
        End If

    End Sub

    Protected Sub ddl_Availability_ins_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Availability_ins.SelectedIndexChanged
        Dim Ds As New DataSet
        If ddl_Availability_ins.SelectedIndex >= 1 Then
            Ds = Common.Get_assetValueListname("A", ddl_Asset_ins.SelectedValue, ddl_Availability_ins.SelectedValue)
            AIns_lb.Text = Ds.Tables(0).Rows(0).Item("asset_desc")
        Else
            AIns_lb.Text = ""
        End If

    End Sub

    Protected Sub ddl_Asset_ins_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Asset_ins.SelectedIndexChanged
        If ddl_Asset_ins.SelectedIndex >= 0 Then
            ddl_assgroup_Ins.DataSource = Common.Get_assgroup_id(ddl_Asset_ins.SelectedValue)
            ddl_assgroup_Ins.DataValueField = "assgroup_id"
            ddl_assgroup_Ins.DataTextField = "assgroup_name"
            ddl_assgroup_Ins.DataBind()
        End If
    End Sub
End Class
