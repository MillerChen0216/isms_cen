﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_30120
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Protected Sub SqlDataSource()
        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = "SELECT sn,riskP_score,riskP_desc,riskP_std FROM [riskP] ORDER BY riskP_score"
        GridView1.DataSourceID = SqlDataSource1.ID
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        SqlDataSource()
    End Sub

    Protected Sub Show_Insert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert.Click
        Show_TB.Visible = False
        Ins_TB.Visible = True
    End Sub
    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        If riskP_score_ins.Text = "" Then
            Common.showMsg(Me.Page, "等級不允許空白")
        Else
            SqlTxt = "INSERT INTO [riskP]  ([riskP_score],[riskP_desc],[riskP_std]) VALUES (" + riskP_score_ins.Text + " , '" + riskP_desc_ins.Text + "','" + riskP_std_ins.Text + "') "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", riskP_score_ins.Text + "-" + riskP_desc_ins.Text + "-" + riskP_std_ins.Text)

            GridView1.DataBind()
            Show_TB.Visible = True
            Ins_TB.Visible = False
        End If

    End Sub
    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click
        If riskP_score_upd.Text = "" Then
            Common.showMsg(Me.Page, "分數不允許空白")
        Else

            SqlTxt = "UPDATE [riskP]  SET  riskP_std='" + riskP_std_upd.Text + "',riskP_score =" + riskP_score_upd.Text + ", riskP_desc='" + riskP_desc_upd.Text + "' WHERE sn= " + Hidden_sn.Value
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", riskP_score_upd.Text + "-" + riskP_desc_upd.Text + "-" + riskP_std_upd.Text)

            GridView1.DataBind()
            Show_TB.Visible = True
            Edit_TB.Visible = False
        End If
    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)

        Hidden_sn.Value = SelectedRow.Cells(5).Text


        Select Case e.CommandName

            Case "Del_Btn"
                SqlTxt = "DELETE FROM [riskP] where sn =" + Hidden_sn.Value
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DEL", SelectedRow.Cells(1).Text + "-" + SelectedRow.Cells(0).Text)

                GridView1.DataBind()

            Case "Edit_Btn"

                Edit_TB.Visible = True
                Show_TB.Visible = False

                riskP_score_upd.Text = SelectedRow.Cells(1).Text
                riskP_desc_upd.Text = SelectedRow.Cells(0).Text
                riskP_std_upd.Text = SelectedRow.Cells(2).Text



        End Select
    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim oDataRow As Data.DataRowView
        Dim sShowField As String
        Dim sMessage As String
        If e.Row.RowIndex <> -1 Then
            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = oDataRow.Item("riskP_desc").ToString()
            sMessage = String.Format("您確定要刪除 [{0}] 嗎?", sShowField)
            CType(e.Row.Cells(4).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"
        End If
    End Sub
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated

        e.Row.Cells(5).Visible = False

    End Sub
End Class
