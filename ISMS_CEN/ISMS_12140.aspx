﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_12140.aspx.vb" Inherits="ISMS_12140" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:HiddenField ID="Hidden_DP" runat="server" />

        <div>
            <br />
            <table id="TB1" cellpadding="0" cellspacing="1" class="tb_1" style="width: 650px" runat="server">
                <tr>
                    <td class="tb_title_w_2" colspan="6">
                        <img src="Images/exe.gif" alt="" />
                        發行記錄查詢</td>
                </tr>
                <tr>
                    <td class="tb_title_w_1">
                        文件類別</td>
                    <td class="tb_title_w_1">
                        開始日期</td>
                    <td class="tb_title_w_1">
                        結束日期</td>
                </tr>
                <tr >
                    <td class="tb_w_1">
                        <asp:DropDownList ID="DDL_DocType" runat="server">
                        </asp:DropDownList></td>
                    <td class="tb_w_1">
                        <asp:TextBox ID="St_Date" runat="server" Width="80px"></asp:TextBox></td>
                    <td class="tb_w_1">
                        <asp:TextBox ID="Ed_Date" runat="server" Width="80px"></asp:TextBox></td>
                </tr>
                <tr >
                    <td class="tb_w_1" colspan="4">
                        <asp:Button ID="Query_Btn" runat="server" CssClass="button-small" Text="發行記錄查詢" /></td>
                </tr>
            </table>
            <br />
        </div>
    
    </div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
            CssClass="tb_1" Visible="False" Width="650px">
            <Columns>
                <asp:BoundField DataField="doc_type" HeaderText="文件類別">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="doc_num" HeaderText="文件編號">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="doc_name" HeaderText="文件名稱">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="doc_version" HeaderText="版次">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="creator" HeaderText="增修人">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="Publisher" HeaderText="發行人">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
            </Columns>
        </asp:GridView>
            <asp:SqlDataSource ID="Doc_DS" runat="server"></asp:SqlDataSource>
        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="St_Date">
        </cc1:CalendarExtender>
        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="Ed_Date">
        </cc1:CalendarExtender>
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
            EnableScriptLocalization="True">
        </asp:ScriptManager>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="VLD_ED_Date">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="VLD_ST_Date">
        </cc1:ValidatorCalloutExtender>
        <asp:CompareValidator ID="VLD_ED_Date" runat="server" ControlToValidate="Ed_Date"
            Display="None" ErrorMessage="開始日期格式錯誤!" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator><asp:CompareValidator
                ID="VLD_ST_Date" runat="server" ControlToValidate="St_Date" Display="None" ErrorMessage="結束日期格式錯誤!"
                Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
    </form>
</body>
</html>
