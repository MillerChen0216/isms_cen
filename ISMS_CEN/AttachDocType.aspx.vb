﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class AttachDocType
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        If Not IsPostBack Then
            EditType.SelectedValue = "0" : EditType_selectedchange(sender, e) '預設為新增
            FileName_DataSource() '附件名稱的下拉Orderby創建時間desc
        End If
    End Sub

    '1_RadioButton_SlectedChange
    Protected Sub EditType_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            EditText_FileName.Text = ""
            Select Case EditType.SelectedValue
                Case "0"
                    '新增
                    otr.Visible = False
                    ntr.Visible = True
                    EditText_FileName.Visible = True
                    EditBtn.Visible = False
                    EditBtn_Save.Visible = True
                Case "1"
                    '修改
                    ntr.Visible = True
                    otr.Visible = True
                    EditDdl_FileName.Visible = True
                    EditText_FileName.Visible = True
                    EditBtn.Visible = False
                    EditBtn_Save.Visible = True
                Case "2"
                    '刪除
                    ntr.Visible = False
                    otr.Visible = True
                    EditDdl_FileName.Visible = True
                    EditBtn.Visible = True '確定按鈕為True
                    EditBtn_Save.Visible = False
                Case Else
                    '沒這可能
            End Select
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_儲存(包含新增跟更新附件名稱)
    Protected Sub EditBtn_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles EditBtn_Save.Click
        Try
            Dim sql = "SELECT * FROM AttachDocType WHERE atdt_Enabled = 1 AND atdt_Name = '" + EditText_FileName.Text + "'"
            Dim data As DataTable = Common.Con(sql)
            If data.Rows.Count = 0 And Not (String.IsNullOrEmpty(EditText_FileName.Text)) Then '確認該名稱目前沒有被使用
                If (otr.Visible) Then
                    '更新
                    SqlTxt = "UPDATE AttachDocType set atdt_Name = '" + EditText_FileName.Text + "' where atdt_Id = '" + EditDdl_FileName.SelectedValue + "'"
                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                    Common.showMsg(Me.Page, "更新附件名稱!")
                Else
                    '新增
                    SqlTxt = "INSERT INTO AttachDocType (atdt_Id ,atdt_Name ,atdt_Enabled ,atdt_userId ,atdt_time) values ('atdt" + Date.Now.ToString("yyyyMMddHHmmssfff") + "','" + EditText_FileName.Text + "',1,'" + User.Identity.Name.ToString + "',GETDATE()  )"
                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                    Common.showMsg(Me.Page, "新增附件名稱!")
                End If
                EditText_FileName.Text = "" '清空附件名稱TextBox
                FileName_DataSource() '更新下拉
            Else
                Common.showMsg(Me.Page, "附件名稱不得重複或空白!")
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_確定(包含刪除文件名稱)
    Protected Sub EditBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles EditBtn.Click
        Try
            '刪除(直接更新Enable狀態20200807要先確認他有沒有被用過)
            Dim sql = "  select * from AttachDoc where inp_atdt_Id = '" + EditDdl_FileName.SelectedValue + "'"
            Dim data = Common.Con(sql)
            If data.Rows.Count = 0 Then
                SqlTxt = " UPDATE AttachDocType set atdt_Enabled = 0 where atdt_Id = '" + EditDdl_FileName.SelectedValue + "'"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.showMsg(Me.Page, "刪除附件名稱!")
                EditText_FileName.Text = "" '清空附件名稱TextBox
                FileName_DataSource() '更新下拉
            Else
                Common.showMsg(Me.Page, "該附件名稱已被使用!")
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_取消
    Protected Sub EditBtn_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles EditBtn_Cancel.Click
        Response.Redirect("AttachDocType.aspx", True) '重新載入附件表單維護
    End Sub


    '1、2_文件名稱下拉來源
    Public Sub FileName_DataSource()
        Try
            Dim sql As String = "SELECT * FROM AttachDocType WHERE atdt_Enabled = 1"
            sql += " order by atdt_time desc "
            EditDdl_FileName.Items.Clear()
            EditDdl_FileName.DataTextField = "atdt_Name"
            EditDdl_FileName.DataValueField = "atdt_Id"
            EditDdl_FileName.DataSource = Common.Con(sql)
            EditDdl_FileName.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

End Class
