<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_13110.aspx.vb" Inherits="ISMS_13110" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>事件記錄表與矯正預防計畫表</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        &nbsp;<asp:HiddenField ID="Hidden_DP" runat="server" />
        <table id="tb2" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 550px"
            visible="false">
            <tr>
                <td class="tb_title_w_2">
                    <img src="Images/exe.gif" />
                    資安事件通報審核</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="550px">
                        <Columns>
                            <asp:BoundField DataField="wf_order" HeaderText="關卡">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="chk_name" HeaderText="審核身分">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="審核人員">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lb_username" runat="server"></asp:Label>
                                    <asp:Label ID="label_userid" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="狀態">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddl_status" runat="server" Visible="False">
                                    </asp:DropDownList>
                                    <asp:Label ID="lb_status" runat="server" Visible="False"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="意見">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="comment_lb" runat="server" Visible="False"></asp:Label>
                                    <asp:TextBox ID="comment_txt" runat="server" Visible="False" Width="120px"></asp:TextBox>&nbsp;
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="wf_inside_order" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    <asp:Button ID="Send_WF" runat="server" Text="送出審核" /></td>
            </tr>
        </table>
        <br />
        <table id="Event_TB" runat="server" cellpadding="2" cellspacing="0" class="tb_1" style="width: 800px"   visible="true" border="1">
            <tr>
                <td align="center" class="normal_font" colspan="4">
                    <asp:Label ID="Label21" runat="server">資通安全事件通報單</asp:Label></td>
            </tr>
            <tr>
             <td class="normal_font" colspan="4" align="left">
                 <asp:Label ID="Label8" runat="server">紀錄編號：</asp:Label>
                 <asp:Label ID="eventnum1_lb" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="normal_font" colspan="1">
                    填報人</td>
                <td class="tb_w_1" colspan="1">
                    <asp:Label ID="eventid_lb" runat="server"></asp:Label>
                    <asp:Label ID="eventUser_lb" runat="server"></asp:Label></td>
                <td class="normal_font" colspan="1">
                    直屬主管</td>
                <td class="tb_w_1" colspan="1">
                    <asp:DropDownList ID="ddl_eventmgr1" runat="server">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="normal_font" colspan="1">
                    洽詢電話</td>
                <td class="tb_w_1" colspan="1">
                    <asp:TextBox ID="event_tel" runat="server" Width="81px"></asp:TextBox></td>
                <td class="normal_font" colspan="1">
                    傳真</td>
                <td class="tb_w_1" colspan="1">
                    <asp:TextBox ID="event_fax" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="normal_font" colspan="1">
                    E-Mail</td>
                <td class="tb_w_1" colspan="1">
                    <asp:TextBox ID="event_email" runat="server" ReadOnly="True" Width="180px"></asp:TextBox></td>
                <td class="normal_font" colspan="1">
                    填報時間</td>
                <td class="tb_w_1" colspan="1">
                    <asp:TextBox ID="event_y" runat="server" Width="30px"></asp:TextBox>年<asp:TextBox
                        ID="event_m" runat="server" Width="30px"></asp:TextBox>月<asp:TextBox ID="event_d"
                            runat="server" Width="30px"></asp:TextBox>日<asp:TextBox ID="event_hh" runat="server"
                                Width="30px"></asp:TextBox>時<asp:TextBox ID="event_mm" runat="server" Width="30px"></asp:TextBox>分</td>
            </tr>
            <tr>
                <td class="normal_font" colspan="1">
                    事件發生時間</td>
                <td class="tb_w_1" colspan="3">
                    <asp:TextBox ID="event_y1" runat="server" Width="30px"></asp:TextBox>年<asp:TextBox
                        ID="event_m1" runat="server" Width="30px"></asp:TextBox>月<asp:TextBox ID="event_d1"
                            runat="server" Width="30px"></asp:TextBox>日<asp:TextBox ID="event_hh1" runat="server"
                                Width="30px"></asp:TextBox>時<asp:TextBox ID="event_mm1" runat="server" Width="30px"></asp:TextBox>分</td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="4">
                    <asp:Label ID="Label10" runat="server">通報事件說明</asp:Label></td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="4">
                    <asp:TextBox ID="event_desc" runat="server" Height="100px" TextMode="MultiLine" Width="800px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="normal_font" colspan="1">
                    處理人員</td>
                <td class="tb_w_1" colspan="1">
                    <asp:DropDownList ID="ddl_dp" runat="server" AutoPostBack="True">
                        <asp:ListItem Selected="True" Value="0">請選擇區處</asp:ListItem>
                    </asp:DropDownList><asp:DropDownList ID="ddl_douser" runat="server" AutoPostBack="True">
                        <asp:ListItem Selected="True" Value="0">請選擇處理人員</asp:ListItem>
                    </asp:DropDownList></td>
                <td class="normal_font" colspan="1">
                    直屬主管</td>
                <td class="tb_w_1" colspan="1">
                    <asp:DropDownList ID="ddl_dousermgr2" runat="server">
                    </asp:DropDownList></td>
            </tr>
            
            <tr>
                <td class="normal_font" colspan="1">
                    洽詢電話</td>
                <td class="tb_w_1" colspan="1">
                    <asp:TextBox ID="douser_tel" runat="server"></asp:TextBox></td>
                <td class="normal_font" colspan="1">
                    E-Mail</td>
                <td class="tb_w_1" colspan="1">
                    <asp:TextBox ID="douser_email" runat="server" ReadOnly="True" Width="180px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="normal_font" colspan="1">
                    主機（伺服器）資料</td>
                <td class="tb_w_1" colspan="3">
                    <asp:TextBox ID="event_server" runat="server" Width="400px" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="normal_font" colspan="1">
                    IP位址（IP ADDRESS）</td>
                <td class="tb_w_1" colspan="3">
                    <asp:TextBox ID="event_ip" runat="server" Width="400px" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="normal_font" colspan="1">
                    網域名稱（Domain name)</td>
                <td class="tb_w_1" colspan="3">
                    <asp:TextBox ID="event_domain" runat="server" Width="400px" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="normal_font" colspan="1">
                    網際網路資訊位址（Web URL)</td>
                <td class="tb_w_1" colspan="3">
                    <asp:TextBox ID="event_web" runat="server" Width="400px" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="normal_font" colspan="1">
                    主機（伺服器）廠牌、型號</td>
                <td class="tb_w_1" colspan="3">
                    <asp:TextBox ID="event_host" runat="server" Width="400px" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="normal_font" colspan="1">
                    作業系統（名稱、版本）</td>
                <td class="tb_w_1" colspan="3">
                    <asp:TextBox ID="event_os" runat="server" Width="400px" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="normal_font" colspan="1">
                    已裝置之安全機制</td>
                <td class="tb_w_1" colspan="3">
                    <asp:TextBox ID="event_security" runat="server" Width="400px" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="4">
                    <asp:Label ID="Label5" runat="server">事件安全等級</asp:Label>
                    </td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="4">
                    <asp:RadioButtonList ID="event_type" runat="server" RepeatLayout="Flow" Enabled="False">
                        <asp:ListItem Value="0">0級事件：&lt;br /&gt;無公務資料遭洩漏。&lt;br /&gt;無公務系統或資料遭竄改。&lt;br /&gt;非公務運作遭影響或短暫停頓</asp:ListItem>
                        <asp:ListItem Value="1">1級事件：&lt;br /&gt;非核心業務資料遭洩漏。&lt;br /&gt; 非核心業務系統或資料遭竄改。&lt;br /&gt; 非核心業務運作遭影響或短暫停頓</asp:ListItem>
                        <asp:ListItem Value="2">2級事件：&lt;br /&gt;非屬密級或敏感之核心業務資料遭洩漏。&lt;br /&gt;核心業務系統或資料遭竄改。&lt;br /&gt;核心業務運作遭影響或系統效率降低，於可容忍中斷時間內回復正常運作。</asp:ListItem>
                        <asp:ListItem Value="3">3級事件：&lt;br /&gt;密級或敏感公務資料遭洩漏。&lt;br /&gt;核心業務系統或資料遭嚴重竄改。&lt;br /&gt;核心業務運作遭影響或系統效率降低，無法於可容忍中斷時間內回復正常運作。</asp:ListItem>
                        <asp:ListItem Value="4">4級事件：&lt;br /&gt;國家機密資料遭洩漏。&lt;br /&gt;國家重要資訊基礎建設系統或資料遭竄改。&lt;br /&gt;國家重要資訊基礎建設運作遭影響或系統效率降低，無法於可容忍中斷時間內回復正常運作。</asp:ListItem>
                    </asp:RadioButtonList></td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="4">
                    <asp:Label ID="Label1" runat="server">事件描述</asp:Label>
                    </td>
            </tr>
            <tr>
   <td align="left" class="normal_font" colspan="4">
                    <asp:Label ID="Label9" runat="server">1.事件分類</asp:Label><br />
                    <asp:CheckBox ID="event_cb1" Text="非法入侵" runat="server" Enabled="False" />
                    <asp:CheckBox ID="event_cb4" Text="資料大量上傳" runat="server" Enabled="False" Visible="False" />
                    <asp:CheckBox ID="event_cb5" Text="應用系統使用異常" runat="server" Enabled="False" Visible="False" /><br />
                    <asp:CheckBox ID="event_cb2" Text="感染病毒" runat="server" Enabled="False" /><br />
                    <asp:CheckBox ID="event_cb3" Text="阻斷服務" runat="server" Enabled="False" /><br />
                    <asp:CheckBox ID="event_cb6" Text="其他" runat="server" Enabled="False" />
                    <asp:TextBox ID="event_cb6_other" runat="server" Enabled="False"></asp:TextBox><br />
                    <asp:Label ID="Label11" runat="server">2.破壞程度</asp:Label><br />
                    <asp:CheckBox ID="event_cb7" Text="系統當機" runat="server" Enabled="False" /><br />
                    <asp:CheckBox ID="event_cb8" Text="資料庫毀損" runat="server" Enabled="False" /><br />
                    <asp:CheckBox ID="event_cb9" Text="網頁竄改" runat="server" Enabled="False" /><br />
                    <asp:CheckBox ID="event_cb10" Text="其他" runat="server" Enabled="False" />
                    <asp:TextBox ID="event_cb10_other" runat="server" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="4">
                    <asp:Label ID="Label3" runat="server">事件說明</asp:Label>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="4">
                    <asp:TextBox ID="event_caption" runat="server" Height="100px" TextMode="MultiLine" Width="800px" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="4">
                    <asp:Label ID="Label2" runat="server">可能影響範圍及損失評估</asp:Label>
                    <asp:TextBox ID="event_effect" runat="server" Height="100px" TextMode="MultiLine" Width="800px" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="4">
                    <asp:Label ID="Label4" runat="server">應變措施</asp:Label>
                    <asp:TextBox ID="event_method" runat="server" Height="100px" TextMode="MultiLine" Width="800px" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" class="normal_font" style="height: 26px">
                    <asp:Label ID="Label6" runat="server">資訊處副處長</asp:Label></td>
                            <td align="center" class="normal_font" colspan="1" style="height: 26px">
                                <asp:Label ID="Label7" runat="server">資訊處處長</asp:Label></td>
                            <td align="center" class="normal_font" colspan="2" style="height: 26px">
                                <asp:Label ID="Label12" runat="server">資訊安全長</asp:Label></td>
            </tr>
            <tr>
                <td align="center" class="normal_font">
                    <asp:Label ID="evnet_comgr" runat="server"></asp:Label>&nbsp;</td>
                <td align="center" class="normal_font" colspan="1">
                    <asp:Label ID="event_mgr" runat="server"></asp:Label>&nbsp;</td>
                <td align="center" class="normal_font" colspan="2">
                    <asp:Label ID="event_cso" runat="server"></asp:Label>&nbsp;</td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="4">
                    <asp:Label ID="Label16" runat="server">資安事件通報人員</asp:Label></td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="4">
                    <asp:Label ID="Label22" runat="server">期望資源項目：</asp:Label><br />
                    <asp:Label ID="Label23" runat="server">人員：</asp:Label><br />
                    <asp:TextBox ID="event_pe" runat="server" Height="50px" TextMode="MultiLine" Width="800px" Enabled="False"></asp:TextBox><br />
                    <asp:Label ID="Label24" runat="server">硬體：</asp:Label><br />
                    <asp:TextBox ID="event_hw" runat="server" Height="50px" TextMode="MultiLine" Width="800px" Enabled="False"></asp:TextBox><br />
                    <asp:Label ID="Label25" runat="server">軟體：</asp:Label><br />
                    <asp:TextBox ID="event_sw" runat="server" Height="50px" TextMode="MultiLine" Width="800px" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" class="normal_font" colspan="2">
                    <asp:Label ID="Label17" runat="server">總處及各區處資安通報承辦人</asp:Label></td>
                <td align="center" class="normal_font" colspan="2">
                    <asp:Label ID="Label18" runat="server">直屬主管</asp:Label></td>
            </tr>
            <tr>
                <td align="center" class="normal_font" colspan="2">
                    <asp:Label ID="event_master" runat="server"></asp:Label>&nbsp;</td>
                <td align="center" class="normal_font" colspan="2">
                    <asp:Label ID="event_mastermgr" runat="server"></asp:Label>&nbsp;</td>
            </tr>
        </table>
        <asp:SqlDataSource id="WF_DS" runat="server">
         
            </asp:SqlDataSource>
        <asp:HiddenField ID="hidden_apprvid" runat="server" Visible="False" />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
    
    
    </form>
</body>
</html>
