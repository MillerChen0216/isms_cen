﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_30160
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Protected Sub SqlDataSource()
        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = "SELECT risk_cftype,risk_cfvalue,risk_cfdesc FROM riskConfig "
        GridView1.DataSourceID = SqlDataSource1.ID
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        SqlDataSource()
    End Sub



    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click

        SqlTxt = "UPDATE [riskConfig]  SET  risk_cfvalue =" + risk_cfvalue.Text + ",risk_cfdesc='" + risk_cfdesc.Text + "' where risk_cftype ='" + Hidden_risk_cftype.Value + "' and dp='" + Hidden_DP.Value + "'"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()

        GridView1.DataBind()
        Show_TB.Visible = True
        Edit_TB.Visible = False
        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", "風險參數" + risk_cfvalue.Text)

    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)

        Hidden_risk_cftype.Value = SelectedRow.Cells(2).Text


        Select Case e.CommandName

            Case "Edit_Btn"

                Edit_TB.Visible = True
                Show_TB.Visible = False

                risk_cfvalue.Text = SelectedRow.Cells(0).Text
                risk_cfdesc.Text = SelectedRow.Cells(1).Text

        End Select
    End Sub
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated

        e.Row.Cells(2).Visible = False

        If Hidden_DP.Value = "pubhq" Or Hidden_DP.Value = "PUBHQ" Then
            e.Row.Cells(3).Visible = True
        Else
            e.Row.Cells(3).Visible = False
        End If

    End Sub
End Class
