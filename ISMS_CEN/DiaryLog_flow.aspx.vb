﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI

Partial Class DiaryLog_flow
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlCmd As SqlCommand
    Dim uid As String = User.Identity.Name '使用者名稱
    Dim uname As String = Common.Get_User_Name(User.Identity.Name.ToString) '使用者代號
    Dim SqlTxt As String
    Dim approvalid As String
    Dim apprvid As String
    Dim dp As String = Me.Get_DP
    Dim File_Path As String = ConfigurationManager.AppSettings("DiaryLog_Path")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        approvalid = ConfigurationManager.AppSettings("flow_DiaryLog") '12
        apprvid = ConfigurationManager.AppSettings("flow_DiaryLog") '12
        Gdv1Show() '待審核表
    End Sub

    '待審核表來源
    Protected Sub Gdv1Show()
        Try
            Dim sql = " select * from  form_record a join DiaryLog b on  a.recordid=b.recordid where b.using = 'Y' and approvalid='" + approvalid + "' and a.status=1 and userid='" + uid + "' order by b.applyDate"
            Dim data As DataTable = Common.Con(sql)
            GridView1.DataSource = data
            GridView1.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '簽核過程及民國年轉換
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '簽核過程
                Dim recordid As String = CType(e.Row.FindControl("lb_rowid"), Label).Text
                CType(e.Row.FindControl("lb_flow"), Label).Text = Common.Str_form_record_isms(approvalid, recordid)
                '民國年轉換
                Dim lb_applydate As String = CType(e.Row.FindControl("lb_applydate"), Label).Text
                CType(e.Row.FindControl("lb_applydate"), Label).Text = Common.Year(Date.Parse(lb_applydate))
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '預設一般記事
    Private Sub DefaultNotesSource()
        Try
            '一般記事
            Dim newTable As New DataTable

            newTable.Columns.Add("time_h")
            newTable.Columns.Add("time_m")
            newTable.Columns.Add("ap_comment")
            newTable.Columns.Add("ap_org")
            newTable.Columns.Add("ap_name")

            newTable.Rows.Add("0", "0", "", "", "")
            newTable.Rows.Add("0", "0", "", "", "")

            GridView3.DataSource = newTable
            GridView3.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '預設各項作業檢查表
    Private Sub DefaultExamsSource()
        Try
            '各項作業檢查表
            Dim newTable2 As New DataTable
            newTable2.Columns.Add("Num")
            newTable2.Columns.Add("coment")
            newTable2.Columns.Add("result")

            newTable2.Rows.Add("1", "", "")
            newTable2.Rows.Add("2", "", "")
            newTable2.Rows.Add("3", "", "")

            GridView4.DataSource = newTable2
            GridView4.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '審核表畫面至審核畫面_1
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Try
            Dim Index As Integer = CType(e.CommandArgument, Int32)
            Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
            Dim lb_rowid As String = (TryCast(SelectedRow.FindControl("lb_rowid"), Label)).Text

            Select Case e.CommandName
                Case "audit_btn"
                    Dim sql = "  SELECT * FROM DiaryLog WHERE recordId = '" + lb_rowid + "'"
                    Dim data As DataTable = Common.Con(sql)
                    If data.Rows.Count > 0 Then
                        '紀錄該表單編號
                        lb_recordid.Text = lb_rowid
                        '載入關卡表
                        GdvShow()



                        Dim workflowId As Integer = Common.Get_workflowId(approvalid, lb_rowid) '取得關卡
                        If workflowId = 0 Then
                            SheetEnabled(True)
                            File_applyFile.Visible = True
                        Else
                            SheetEnabled(False)
                            File_applyFile.Visible = False
                        End If


                        '填入該lb_rowid資料
                        applyDate.Text = Common.Year(data.Rows(0)("applyDate")) '日期
                        recordId.Text = data.Rows(0)("recordId") '表單編號
                        Radio_checkStatus.SelectedValue = data.Rows(0)("checkStatus") '各項作業檢查
                        Text_comment.Text = data.Rows(0)("comment") '異常記事(狀況描述與處理情形)
                        workitems.Text = data.Rows(0)("workitems") '交代事項

                        '查看裡面是否有資料，有才秀出來
                        If IsDBNull(data.Rows(0)("applyFile")) Then
                            applyFile.Visible = False
                        Else
                            applyFile.Visible = True
                            applyFile_link.Text = data.Rows(0)("applyFile")
                        End If

                        '匯入的 
                        sql = " select SYSIO_recordId recordId,time_h InTimeHr,time_m InTimeMin,ap_comment applyComment,ap_org applyOrg,ap_name applyName,('2020-12-07 ' + time_h +':' +time_m) as Time from DiaryLog_notes where DiaryLog_recordId = '" + lb_rowid + "' and kind = 1 order by Time"
                        Dim notesData_1 As DataTable = Common.Con(sql)
                        GridView2.DataSource = notesData_1
                        GridView2.DataBind()

                        '自己打的
                        sql = " select *,('2020-12-07 ' + time_h +':' +time_m) as Time from DiaryLog_notes where DiaryLog_recordId = '" + lb_rowid + "' and kind = 2 order by Time"
                        Dim notesData_2 As DataTable = Common.Con(sql)
                        GridView3.DataSource = notesData_2
                        GridView3.DataBind()
                        If notesData_2.Rows.Count = 0 And workflowId = 0 Then
                            DefaultNotesSource()
                        End If

                        '各項作業檢查
                        sql = "  select ROW_NUMBER() OVER(ORDER BY recordId) AS Num,* from DiaryLog_exam where DiaryLog_recordId = '" + lb_rowid + "'"
                        Dim examsData As DataTable = Common.Con(sql)
                        GridView4.DataSource = examsData
                        GridView4.DataBind()
                        If examsData.Rows.Count = 0 And workflowId = 0 Then
                            DefaultExamsSource()
                        End If

                        '將審核畫面弄出來
                        tb_result.Visible = True
                        tr_gridview.Visible = False
                        Insert_Btn.Visible = True
                        but_esc.Visible = True
                    Else
                        Response.Redirect("DiaryLog_flow.aspx", True) '重新載入附件表單簽核_審核 因為此表單編號已經不存在
                    End If


            End Select
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '小工具1
    Private Sub SheetEnabled(ByVal use As Boolean)
        Btn_DiaryLog.Visible = use
        td_Addnote.Visible = use
        If use Then
            td_noteColspan.ColSpan = 4
        Else
            td_noteColspan.ColSpan = 5
        End If
        Radio_checkStatus.Enabled = use
        Text_comment.Enabled = use
        workitems.Enabled = use
        File_applyFile.Visible = use
        Btn_Addexam.Visible = use
    End Sub

    '各項作業檢查的Enabled
    Protected Sub GridView4_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView4.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '跟File_applyFile的顯示一樣
                Dim comment = CType(e.Row.FindControl("comment"), TextBox)
                Dim result = CType(e.Row.FindControl("result"), TextBox)

                Dim workflowId As Integer = Common.Get_workflowId(approvalid, lb_recordid.Text) '取得關卡

                comment.Enabled = (workflowId = 0)
                result.Enabled = (workflowId = 0)
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '關卡表
    Private Sub GdvShow()
        Try
            Dim dt As DataTable = Common.Qry_workflowName_isms_Req(approvalid, lb_recordid.Text, apprvid, dp)
            GridView.DataSource = dt
            GridView.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub



    '關卡表_RowDataBound
    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '***已簽核資料

                '***人員
                Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
                Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
                Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
                Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
                '***狀態
                Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
                Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
                Dim hid_status As HiddenField = CType(e.Row.FindControl("hid_status"), HiddenField)
                '***意見
                Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
                Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
                '***關卡***
                Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text
                '***審核人員***
                'Dim lb_wf_inside_order As String = CType(e.Row.FindControl("lb_wf_inside_order"), Label).Text

                Dim ds As DataSet


                '***Start
                If o_wf_order = "0" And hid_status.Value <> "1" Then
                    pan_user.Visible = True
                    '將審核人員設為自己
                    Dim sql = "    SELECT user_id, user_name FROM ismsUser WHERE user_id = '" + uid + "'"
                    Dim data As DataTable = Common.Con(sql)
                    CType(e.Row.FindControl("lb_username"), Label).Text = data.Rows(0)("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = data.Rows(0)("user_id")
                ElseIf hid_status.Value = "1" Then '待審核
                    ds = Common.Qry_form_record_wait(lb_recordid.Text)
                    pan_user.Visible = True
                    CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
                    lb_count.Text = e.Row.RowIndex
                ElseIf hid_status.Value = "2" Or hid_status.Value = "9" Then '審核已通過
                    ds = Common.Qry_form_record_name(lb_recordid.Text, o_wf_order)
                    pan_user.Visible = True
                    CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
                ElseIf e.Row.RowIndex = CType(lb_count.Text, Integer) + 1 Then '待審核下一關卡
                    ddl_user.Visible = True
                    ds = Common.Get_Wf_User(dp, apprvid, o_wf_order, "")
                    ddl_user.DataSource = ds
                    ddl_user.DataTextField = "name_id"
                    ddl_user.DataValueField = "wf_userid"
                    ddl_user.DataBind()
                End If

                If hid_status.Value = "1" Then '待審核
                    ddl_status.Visible = True
                    lb_status.Visible = False
                    txt_comment.Visible = True
                    lb_comment.Visible = False
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(1)
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()

                ElseIf hid_status.Value = "" Then
                    ddl_status.Visible = False
                    lb_status.Visible = True
                    txt_comment.Visible = False
                    lb_comment.Visible = True
                Else
                    ddl_status.Visible = False
                    lb_status.Visible = True
                    txt_comment.Visible = False
                    lb_comment.Visible = True
                    CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(hid_status.Value))
                End If
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_按鈕_增加一般記事
    Protected Sub Btn_Addnote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Addnote.Click
        Try

            Dim newTable As New DataTable
            newTable.Columns.Add("time_h")
            newTable.Columns.Add("time_m")
            newTable.Columns.Add("ap_comment")
            newTable.Columns.Add("ap_org")
            newTable.Columns.Add("ap_name")

            Dim count = GridView3.Rows.Count
            For Item As Integer = 0 To count - 1 Step 1
                Dim time_h = CType(GridView3.Rows(Item).Cells(0).FindControl("ddl_TimeH"), DropDownList).SelectedValue
                Dim time_m = CType(GridView3.Rows(Item).Cells(0).FindControl("ddl_TimeM"), DropDownList).SelectedValue
                Dim ap_comment = CType(GridView3.Rows(Item).Cells(0).FindControl("ap_comment"), TextBox).Text
                Dim ap_org = CType(GridView3.Rows(Item).Cells(0).FindControl("ap_org"), TextBox).Text
                Dim ap_name = CType(GridView3.Rows(Item).Cells(0).FindControl("ap_name"), TextBox).Text

                newTable.Rows.Add(time_h, time_m, ap_comment, ap_org, ap_name)
            Next

            newTable.Rows.Add("0", "0", "", "", "")
            GridView3.DataSource = newTable
            GridView3.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_將使用者可以自行新增的一般記事新增的"時間來源" 及預設資料
    Protected Sub GridView3_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView3.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim ddl_TimeH As DropDownList = CType(e.Row.FindControl("ddl_TimeH"), DropDownList)
            Dim ddl_TimeM As DropDownList = CType(e.Row.FindControl("ddl_TimeM"), DropDownList)
            Dim ap_comment As TextBox = CType(e.Row.FindControl("ap_comment"), TextBox)
            Dim ap_org As TextBox = CType(e.Row.FindControl("ap_org"), TextBox)
            Dim ap_name As TextBox = CType(e.Row.FindControl("ap_name"), TextBox)

            Dim workflowId As Integer = Common.Get_workflowId(approvalid, lb_recordid.Text) '取得關卡

            ddl_TimeH.Enabled = (workflowId = 0)
            ddl_TimeM.Enabled = (workflowId = 0)
            ap_comment.Enabled = (workflowId = 0)
            ap_org.Enabled = (workflowId = 0)
            ap_name.Enabled = (workflowId = 0)


            Dim time_h = CType(e.Row.FindControl("time_h"), Label).Text
            Dim time_m = CType(e.Row.FindControl("time_m"), Label).Text

            ddl_TimeH.Items.Clear()
            ddl_TimeM.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                ddl_TimeH.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                ddl_TimeM.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            ddl_TimeH.SelectedValue = time_h
            ddl_TimeM.SelectedValue = time_m

        End If
    End Sub

    '2_按鈕_主頁至各項作業檢查表
    Protected Sub Btn_exam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_exam.Click
        Try
            Table2.Visible = True
            Table1.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '2_按鈕_增加各項作業檢查
    Protected Sub Btn_Addexam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Addexam.Click
        Try

            Dim newTable As New DataTable
            newTable.Columns.Add("Num")
            newTable.Columns.Add("comment")
            newTable.Columns.Add("result")

            Dim count = GridView4.Rows.Count
            For Item As Integer = 0 To count - 1 Step 1
                Dim coment = CType(GridView4.Rows(Item).Cells(0).FindControl("comment"), TextBox).Text
                Dim result = CType(GridView4.Rows(Item).Cells(0).FindControl("result"), TextBox).Text

                newTable.Rows.Add(Item + 1, coment, result)
            Next

            newTable.Rows.Add(count + 1, "", "")
            GridView4.DataSource = newTable
            GridView4.DataBind()
        Catch ex As Exception
            lb_msg2.Text = ex.Message
        End Try
    End Sub

    '2_按鈕_各項作業檢查表至主頁
    Protected Sub Btn_examBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_examBack.Click
        Try
            Table2.Visible = False
            Table1.Visible = True
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '按鈕_匯入申請表
    Protected Sub Btn_DiaryLog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_DiaryLog.Click
        Try
            If Not String.IsNullOrEmpty(applyDate.Text) Then
                Dim sql = " select * from SYSIO where using = 'Y' and applyDate = '" + Common.YearYYYY(applyDate.Text) + "'"
                Dim data = Common.Con(sql)
                GridView2.DataSource = data
                GridView2.DataBind()
            Else
                Common.showMsg(Me.Page, "請確實填寫日期")
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '匯入申請表的隱藏欄位
    Protected Sub GridView2_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowCreated
        'Response.Write(e.Row.Cells(2).Visible)
        e.Row.Cells(0).Visible = False
    End Sub

    '一般記事內的連結
    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView2.RowCommand
        Try
            Dim SYSIO_recrodId As String = e.CommandArgument

            Select Case e.CommandName
                Case "Btn_SYSIO"
                    Response.Write("<script>window.open('ViewSYSIO.aspx','_target')</script>")
                    Session("SYSIO_recordId") = SYSIO_recrodId
            End Select
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '檔案下載_按鈕
    Protected Sub applyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + applyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '返回列表_按鈕
    Protected Sub but_esc_click()
        tr_gridview.Visible = True
        tb_result.Visible = False
        Insert_Btn.Visible = False
        but_esc.Visible = False
        Gdv1Show()
    End Sub

    '送出審核_按鈕
    Protected Sub Insert_Btn_click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim index As Integer = CType(lb_count.Text, Integer)
        Dim max_index As Integer = GridView.Rows.Count

        Dim wf_order As String = CType(GridView.Rows(index).FindControl("wf_order"), Label).Text '待審核的關卡
        Dim status As String = CType(GridView.Rows(index).FindControl("ddl_status"), DropDownList).SelectedValue '狀態
        Dim comment As String = CType(GridView.Rows(index).FindControl("txb_comment"), TextBox).Text
        Dim id As String = CType(GridView.Rows(index).FindControl("lb_id2"), Label).Text
        Dim stage As Integer = Common.Get_workflowId(approvalid, recordId.Text) '取得關卡

        If status = 2 Then '通過
            If index + 1 < GridView.Rows.Count Then '流程尚未到最後關卡

                If stage = 0 Then
                    Dim recordId = lb_recordid.Text
                    '回到第0關()
                    '一般記事
                    Dim count = GridView2.Rows.Count
                    Dim noteCheck = False
                    If count = 0 Then
                        count += GridView3.Rows.Count
                        For Item As Integer = 0 To count - 1 Step 1
                            Dim time_h = CType(GridView3.Rows(Item).Cells(0).FindControl("ddl_TimeH"), DropDownList).SelectedValue
                            Dim time_m = CType(GridView3.Rows(Item).Cells(0).FindControl("ddl_TimeM"), DropDownList).SelectedValue
                            Dim ap_comment = CType(GridView3.Rows(Item).Cells(0).FindControl("ap_comment"), TextBox).Text
                            Dim ap_org = CType(GridView3.Rows(Item).Cells(0).FindControl("ap_org"), TextBox).Text
                            Dim ap_name = CType(GridView3.Rows(Item).Cells(0).FindControl("ap_name"), TextBox).Text
                            If Not String.IsNullOrEmpty(ap_comment) And Not String.IsNullOrEmpty(ap_org) And Not String.IsNullOrEmpty(ap_name) Then
                                noteCheck = True
                                Exit For
                            End If
                        Next
                    Else
                        noteCheck = True
                    End If
                    '作業各項檢查
                    count = GridView4.Rows.Count
                    Dim examCheck = False
                    For Item As Integer = 0 To count - 1 Step 1
                        Dim coment = CType(GridView4.Rows(Item).Cells(0).FindControl("comment"), TextBox).Text
                        Dim result = CType(GridView4.Rows(Item).Cells(0).FindControl("result"), TextBox).Text
                        If Not String.IsNullOrEmpty(coment) And Not String.IsNullOrEmpty(result) Then
                            examCheck = True
                            Exit For
                        End If
                    Next

                    If String.IsNullOrEmpty(applyDate.Text) Or Not noteCheck Or Not examCheck Then
                        Common.showMsg(Me.Page, "請確實填寫資訊")
                        Return
                    End If

                    '先刪掉該 recordId 原本的 DiaryLog_notes 及 DiaryLog_exam
                    SqlTxt += " delete DiaryLog_notes where DiaryLog_recordId = '" + recordId + "'    delete DiaryLog_exam where DiaryLog_recordId = '" + recordId + "'"

                    'DiaryLog_notes   
                    count = GridView2.Rows.Count '匯入的
                    For Item As Integer = 0 To count - 1 Step 1
                        Dim Hr = CType(GridView2.Rows(Item).Cells(0).FindControl("Hr"), Label).Text
                        Dim Min = CType(GridView2.Rows(Item).Cells(0).FindControl("Min"), Label).Text
                        Dim ap_comment = CType(GridView2.Rows(Item).Cells(0).FindControl("ap_comment"), LinkButton).Text
                        Dim ap_org = CType(GridView2.Rows(Item).Cells(0).FindControl("ap_org"), Label).Text
                        Dim ap_name = CType(GridView2.Rows(Item).Cells(0).FindControl("ap_name"), Label).Text
                        Dim SYSIO_recordId = CType(GridView2.Rows(Item).Cells(0).FindControl("SYSIO_recordId"), Label).Text
                        If Not String.IsNullOrEmpty(ap_comment) And Not String.IsNullOrEmpty(ap_org) And Not String.IsNullOrEmpty(ap_name) Then
                            Dim notes_Id = "drylognotes" + Date.Now.AddMilliseconds(Item).ToString("yyyymmddHHmmssfff")
                            SqlTxt += " insert into DiaryLog_notes  ( recordId, DiaryLog_recordId, kind,SYSIO_recordId, time_h, time_m, ap_comment, ap_org, ap_name, create_time, op_time )" _
                                + "values ('" + notes_Id + "','" + recordId + "','1','" + SYSIO_recordId + "','" + Hr + "','" + Min + "','" + ap_comment + "','" + ap_org + "','" + ap_name + "',getdate(),getdate())  "
                        End If
                    Next

                    count = GridView3.Rows.Count '自己輸入的
                    For Item As Integer = 0 To count - 1 Step 1
                        Dim time_h = CType(GridView3.Rows(Item).Cells(0).FindControl("ddl_TimeH"), DropDownList).SelectedValue
                        Dim time_m = CType(GridView3.Rows(Item).Cells(0).FindControl("ddl_TimeM"), DropDownList).SelectedValue
                        Dim ap_comment = CType(GridView3.Rows(Item).Cells(0).FindControl("ap_comment"), TextBox).Text
                        Dim ap_org = CType(GridView3.Rows(Item).Cells(0).FindControl("ap_org"), TextBox).Text
                        Dim ap_name = CType(GridView3.Rows(Item).Cells(0).FindControl("ap_name"), TextBox).Text
                        If Not String.IsNullOrEmpty(ap_comment) And Not String.IsNullOrEmpty(ap_org) And Not String.IsNullOrEmpty(ap_name) Then
                            Dim notes_Id = "drylognotes" + Date.Now.AddSeconds(1).AddMilliseconds(Item).ToString("yyyymmddHHmmssfff")
                            SqlTxt += " insert into DiaryLog_notes  ( recordId, DiaryLog_recordId, kind, time_h, time_m, ap_comment, ap_org, ap_name, create_time, op_time )" _
                                + "values ('" + notes_Id + "','" + recordId + "','2','" + time_h + "','" + time_m + "','" + ap_comment + "','" + ap_org + "','" + ap_name + "',getdate(),getdate())  "
                        End If
                    Next

                    'DiaryLog_exam   
                    count = GridView4.Rows.Count
                    For Item As Integer = 0 To count - 1 Step 1
                        Dim coment = CType(GridView4.Rows(Item).Cells(0).FindControl("comment"), TextBox).Text
                        Dim result = CType(GridView4.Rows(Item).Cells(0).FindControl("result"), TextBox).Text
                        If Not String.IsNullOrEmpty(coment) And Not String.IsNullOrEmpty(result) Then
                            Dim exam_Id = "drylogexam" + Date.Now.AddSeconds(1).AddMilliseconds(Item).ToString("yyyymmddHHmmssfff")
                            SqlTxt += " insert into DiaryLog_exam  ( recordId, DiaryLog_recordId, comment, result, create_time, op_time )" _
                                + "values ('" + exam_Id + "','" + recordId + "','" + coment + "','" + result + "',getdate(),getdate())"
                        End If
                    Next



                    '處理上傳檔案
                    If CType(Me.FindControl("File_applyFile"), FileUpload).HasFile Then
                        '有檔案
                        Dim oriFileName = CType(Me.FindControl("File_applyFile"), FileUpload)
                        Dim UpFileName As String = Common.UpFile(File_Path, recordId, CType(Me.FindControl("File_applyFile"), FileUpload))
                        '塞入主表資料
                        SqlTxt += "  update DiaryLog set checkStatus = '" + Radio_checkStatus.SelectedValue + "', comment = '" + Text_comment.Text + "', workitems = '" + workitems.Text + "'," _
                            + "applyFile_ori_name = '" + oriFileName.FileName + "', applyFile = '" + UpFileName + "', op_time = getdate() where recordId = '" + recordId + "'"

                    Else
                        '沒有檔案
                        '塞入主表資料
                        SqlTxt += "  update DiaryLog set checkStatus = '" + Radio_checkStatus.SelectedValue + "', comment = '" + Text_comment.Text + "', workitems = '" + workitems.Text + "'," _
                            + " op_time = getdate() where recordId = '" + recordId + "'"

                    End If



                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                End If


                Dim workflowid As String = CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) + 1
                Dim user_id = CType(GridView.Rows(index + 1).FindControl("ddl_user"), DropDownList).SelectedValue

                SqlTxt = " INSERT INTO form_record (approvalId,workflowId,userId,userType,status,comment,recordId,signDate,role,dp) VALUES "
                SqlTxt += " ('" + approvalid + "','" + workflowid + "','" + user_id + "','land','1','','" + lb_recordid.Text + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + workflowid + "','" + dp + "') "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            End If
            SqlTxt = " UPDATE form_record SET status='2' , signDate ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' , comment = '" + comment + "' WHERE status != 2 and status !=0 and approvalId='" + approvalid + "'and recordId ='" + lb_recordid.Text + "' and id ='" + id + "' "
        ElseIf status = 9 Then '退回
            If CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) > 0 Then '目前流程不是第一關卡
                Dim workflowid As String = CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) - 1
                'Dim user_id = CType(GridView.Rows(index - 1).FindControl("label_userid"), Label).Text
                Dim dt As DataTable = Common.Qry_form_record1(approvalid, lb_recordid.Text, workflowid)
                Dim user_id As String = dt.Rows(0).Item("userId").ToString

                SqlTxt = " INSERT INTO form_record (approvalId,workflowId,userId,userType,status,comment,recordId,signDate,role,dp) VALUES "
                SqlTxt += " ('" + approvalid + "','" + workflowid + "','" + user_id + "','land','1','','" + lb_recordid.Text + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + workflowid + "','" + dp + "') "

                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            Else
                Common.showMsg(Me.Page, "流程已到填表人，無法再退回！")
                Return
            End If

            SqlTxt = " UPDATE form_record SET status='9' , signDate ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' , comment = '" + comment + "' WHERE approvalId='" + approvalid + "'and recordId ='" + lb_recordid.Text + "' and id ='" + id + "' "

        End If
        Dim cmd As New SqlCommand(SqlTxt.ToString)

        Try
            Conn = Common.ConnDBS : Conn.Open() : cmd.Connection = Conn
            If cmd.ExecuteNonQuery() Then Common.showMsg(Me.Page, "審核完成！") : but_esc_click()
        Catch ex As Exception
            Common.showMsg(Me.Page, "審核失敗！")
        Finally
            Conn.Close() : Conn.Close() : Conn.Dispose() : cmd.Dispose()
        End Try
    End Sub

End Class
