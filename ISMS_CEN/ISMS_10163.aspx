<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_10163.aspx.vb" Inherits="ISMS_10163" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HiddenField ID="Hidden_DP" runat="server" />
        <br />
        <table id="Show_TB" class="tb_1" runat="server" style="width: 600px" cellpadding="0" cellspacing="0" visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="5">
                    <img src="Images/exe.gif" />
                    文件保存分類管理&nbsp;
                    <asp:DropDownList ID="ddl_type" runat="server" AppendDataBoundItems="True" AutoPostBack="True">
                    </asp:DropDownList>&nbsp;
                    <asp:Button ID="Show_Insert" runat="server" CssClass="button-small" Text="新增子分類" /></td>
            </tr>
            <tr>
                <td class="tb_title_w_center" colspan="5">
                    <asp:Button ID="Button1" runat="server" CssClass="button-small" Text="新增大分類" />
                    <asp:Button ID="Button2" runat="server" CssClass="button-small" Text="編輯大分類" />
                    <asp:Button ID="Button5" runat="server" CssClass="button-small" Text="刪除大分類"  OnClientClick="if (confirm('您確定要刪除大分類嗎?')==false) {return false;}" Visible="False"/></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="tb_1"
                        Width="600px" HorizontalAlign="Left">
                        <Columns>
                            <asp:BoundField DataField="typeValue" HeaderText="文件保存子分類">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="cid" HeaderText="CID" >
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                           <asp:ButtonField ButtonType="Button" CommandName="Edit_Btn" Text="編輯">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:ButtonField>
                            <asp:ButtonField ButtonType="Button" CommandName="Del_Btn" Text="刪除">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:ButtonField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <table id="Edit_TB" runat="server" cellpadding="3" cellspacing="0" class="tb_1" style="width: 500px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.GIF" />
                    文件保存子分類管理
                    <asp:Button ID="Update_Btn" runat="server" CssClass="button-small" Text="確定編輯" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    子分類名稱</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="type_value" runat="server" MaxLength="10" Width="100px"></asp:TextBox></td>
            </tr>
        </table>
        <table id="Ins_TB" runat="server" cellpadding="3" cellspacing="0" class="tb_1" style="width: 500px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="7" style="height: 27px">
                    <img src="Images/exe.GIF" />
                    文件保存子分類管理
                    <asp:Button ID="Insert_Btn" runat="server" CssClass="button-small" Text="確定新增" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    子分類名稱</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="type_value_ins" runat="server" MaxLength="60" Width="100px"></asp:TextBox></td>
            </tr>
        </table><table id="Table1" runat="server" cellpadding="3" cellspacing="0" class="tb_1" style="width: 500px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.GIF" />
                    文件保存大分類管理
                    <asp:Button ID="Button3" runat="server" CssClass="button-small" Text="確定新增" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    大分類名稱</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="typevalue_ins" runat="server" MaxLength="10" Width="100px"></asp:TextBox></td>
            </tr>
        </table>
        <table id="Table2" runat="server" cellpadding="3" cellspacing="0" class="tb_1" style="width: 500px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.GIF" />
                    文件保存大分類管理
                    <asp:Button ID="Button4" runat="server" CssClass="button-small" Text="確定編輯" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    大分類名稱</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="typevalue" runat="server" MaxLength="10" Width="100px"></asp:TextBox></td>
            </tr>
        </table>
        <br />
        <asp:HiddenField ID="hidden_id" runat="server" />
        &nbsp;&nbsp;
    </form>
</body>
</html>