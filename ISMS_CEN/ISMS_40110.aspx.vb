﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class ISMS_40110
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim Ds As New DataSet


    Protected Sub DataSource1()
        SqlDS1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlTxt = " SELECT a.* FROM appLog a,ismsUser b,ctlGroup c where target_type='GENERAL' "
        SqlTxt += " AND a.user_id = b.user_id and b.group_id = c.group_id and c.dp ='" + Hidden_DP.Value + "' "

        If St_Date.Text <> "" And Ed_Date.Text <> "" Then
            'SqlTxt = SqlTxt + "and issue_date >= '" + St_Date.Text + "' and issue_date <='" + Ed_Date.Text + "' "
            SqlTxt = SqlTxt + "and Convert(varchar, issue_date, 111) >= '" + St_Date.Text + "' and  Convert(varchar, issue_date, 111) <='" + Ed_Date.Text + "' "

        End If

        SqlTxt = SqlTxt + "order by issue_date desc"
        'Response.Write(SqlTxt)

        SqlDS1.SelectCommand = SqlTxt
        GridView1.DataSourceID = SqlDS1.ID
    End Sub


    Protected Sub DataSource2()
        SqlDS2.ConnectionString = Common.ConnDBS.ConnectionString
        'SqlTxt = "SELECT * FROM appLog where target_type='APPLICATION' AND substring(user_id,1,2)='" + Hidden_DP.Value + "' "

        SqlTxt = " SELECT a.* FROM appLog a,ismsUser b,ctlGroup c where target_type='APPLICATION' "
        SqlTxt += " AND a.user_id = b.user_id and b.group_id = c.group_id and c.dp = '" + Hidden_DP.Value + "' "

        If St_Date.Text <> "" And Ed_Date.Text <> "" Then
            'SqlTxt = SqlTxt + "and issue_date >= '" + St_Date.Text + "' and issue_date <='" + Ed_Date.Text + "' "
            SqlTxt = SqlTxt + "and Convert(varchar, issue_date, 111) >= '" + St_Date.Text + "' and  Convert(varchar, issue_date, 111) <='" + Ed_Date.Text + "' "

        End If
        SqlTxt = SqlTxt + "order by issue_date desc"

        SqlDS2.SelectCommand = SqlTxt
        GridView2.DataSourceID = SqlDS2.ID
    End Sub

    Protected Sub Query_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Query_Btn.Click
        If DDL_DocType.SelectedValue = "GENERAL" Then
            DataSource1()
            GridView1.Visible = True
            GridView2.Visible = False
        Else '資料異動查詢
            DataSource2()
            GridView1.Visible = False
            GridView2.Visible = True
        End If
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType <> DataControlRowType.Header Then
            e.Row.Cells(1).Text = e.Row.Cells(1).Text + "/" + Common.Get_User_Name(e.Row.Cells(1).Text)

            Select Case e.Row.Cells(2).Text
                Case "LOGININ"
                    e.Row.Cells(2).Text = "登入成功"
                Case "LOGINOUT"
                    e.Row.Cells(2).Text = "登出系統"
                Case "LOGINFAIL"
                    e.Row.Cells(2).Text = "登入失敗"
            End Select
        End If

    End Sub

    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound
        If e.Row.RowType <> DataControlRowType.Header Then
            e.Row.Cells(1).Text = e.Row.Cells(1).Text + "/" + Common.Get_User_Name(e.Row.Cells(1).Text)

            e.Row.Cells(2).Text = Common.Get_MenuUrl(e.Row.Cells(2).Text)
            Select Case e.Row.Cells(4).Text
                Case "ADDNEWDOC"
                    e.Row.Cells(4).Text = "文件新增"
                Case "DELETEDOC"
                    e.Row.Cells(4).Text = "文件刪除"
                Case "DISABLEDOC"
                    e.Row.Cells(4).Text = "文件廢止"
                Case "DOCDEL"
                    e.Row.Cells(4).Text = "文件廢止完成"
                Case "FILEDOWN"
                    e.Row.Cells(4).Text = "檔案下載"
                Case "NEWDOCPUBLISH"
                    e.Row.Cells(4).Text = "文件新版發行"
                Case "REVDOCPUBLISH"
                    e.Row.Cells(4).Text = "文件改版發行"
                Case "UPDATEVERSION"
                    e.Row.Cells(4).Text = "文件改版"
                Case "ADD"
                    e.Row.Cells(4).Text = "新增"
                Case "EDIT"
                    e.Row.Cells(4).Text = "修改"
                Case "DEL"
                    e.Row.Cells(4).Text = "刪除"
                Case "OPEN"
                    e.Row.Cells(4).Text = "啟用"
                Case "CLOSE"
                    e.Row.Cells(4).Text = "停用"
                Case "RISKPUBLISH"
                    e.Row.Cells(4).Text = "風險評鑑填表完成"
                Case "SETUP"
                    e.Row.Cells(4).Text = "權限管理 "
                Case "ADDTRAINING"
                    e.Row.Cells(4).Text = "新增教育訓練"
                Case "EDITTRAINING"
                    e.Row.Cells(4).Text = "修改教育訓練"
                Case "DELTRAINING"
                    e.Row.Cells(4).Text = "刪除教育訓練"
            End Select
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
    End Sub
End Class
