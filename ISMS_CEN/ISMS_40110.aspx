﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_40110.aspx.vb" Inherits="ISMS_40110" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-color: #ffffff">
    <form id="form1" runat="server">
    <div>
        <br />
                <asp:HiddenField ID="Hidden_DP" runat="server" />

        <table id="TB1" runat="server" cellpadding="0" cellspacing="1" class="tb_1" style="width: 650px">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img alt="" src="Images/exe.gif" />
                    系統Log查詢</td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    查詢類別</td>
                <td class="tb_title_w_1">
                    開始日期</td>
                <td class="tb_title_w_1">
                    結束日期</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:DropDownList ID="DDL_DocType" runat="server">
                        <asp:ListItem Selected="True" Value="GENERAL">登入紀錄</asp:ListItem>
                        <asp:ListItem Value="APPLICATION">資料異動查詢</asp:ListItem>
                    </asp:DropDownList></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="St_Date" runat="server" Width="80px"></asp:TextBox></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="Ed_Date" runat="server" Width="80px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tb_w_1" colspan="4">
                    <asp:Button ID="Query_Btn" runat="server" CssClass="button-small" Text="紀錄查詢" /></td>
            </tr>
        </table>
        <cc1:calendarextender id="CalendarExtender1" runat="server" targetcontrolid="St_Date" format="yyyy/MM/dd"> </cc1:calendarextender>
        <cc1:calendarextender id="CalendarExtender2" runat="server" targetcontrolid="Ed_Date" format="yyyy/MM/dd"></cc1:calendarextender>
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
            EnableScriptLocalization="True">
        </asp:ScriptManager>
        <cc1:validatorcalloutextender id="ValidatorCalloutExtender1" runat="server" targetcontrolid="VLD_ED_Date"> </cc1:validatorcalloutextender>
        <cc1:validatorcalloutextender id="ValidatorCalloutExtender2" runat="server" targetcontrolid="VLD_ST_Date"></cc1:validatorcalloutextender>
        <asp:CompareValidator ID="VLD_ED_Date" runat="server" ControlToValidate="Ed_Date"
            Display="None" ErrorMessage="開始日期格式錯誤!" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator><asp:CompareValidator
                ID="VLD_ST_Date" runat="server" ControlToValidate="St_Date" Display="None" ErrorMessage="結束日期格式錯誤!"
                Operator="DataTypeCheck" Type="Date"></asp:CompareValidator><br />
        <asp:SqlDataSource ID="SqlDS1" runat="server"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDS2" runat="server"></asp:SqlDataSource>
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
            CssClass="tb_1" Visible="False" Width="650px">
            <Columns>
                <asp:BoundField DataField="issue_date" HeaderText="日期時間">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="user_id" HeaderText="使用者">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="action" HeaderText="紀錄行為">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
            </Columns>
        </asp:GridView>
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="3"
            CssClass="tb_1" Visible="False" Width="650px">
            <Columns>
                <asp:BoundField DataField="issue_date" HeaderText="日期時間">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="user_id" HeaderText="使用者">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="target_page" HeaderText="資源類別">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="log_memo" HeaderText="資源" Visible="false">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="action" HeaderText="系統行為">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
            </Columns>
        </asp:GridView>
        <br />
    
    </div>
    </form>
</body>
</html>
