﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_40120.aspx.vb" Inherits="ISMS_40120" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js" type="text/javascript"></script>
      <script type="text/javascript">
          $(function () {
              $('[id*=lb_Ins_User]').multiselect({
                  includeSelectAllOption: true
              });
          });
    </script>
    
</head>
<body style="background-color: #ffffff">
    <form id="form1" runat="server">
    <div>
     <asp:HiddenField ID="Hidden_DP" runat="server" />

        <br />
                <asp:HiddenField ID="HiddenField1" runat="server" />

        <table id="Approval_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 750px"
            visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    審核流程管理
                    <asp:Button ID="Show_Insert_Process" runat="server" CssClass="button-small" Text="新增資料" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server"
                        AutoGenerateColumns="False" CellPadding="3" CssClass="tb_1" Width="750px">
                        <Columns>
                            <asp:BoundField DataField="apprv_id" HeaderText="序號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="form_name" HeaderText="流程名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="form_desc" HeaderText="備註">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="表單">
                               <ItemTemplate>      
                                   <asp:Label ID="lab_FormName" runat="server"></asp:Label>
                                   <asp:DropDownList ID="ddl_FormName" runat="server" Visible="false" ></asp:DropDownList>
                                   <asp:Button ID="btn_SetForm" runat="server" Visible="false" CommandArgument='<%# Container.DataItemIndex %>' Text ="設定" OnClick="btn_SetForm_Click" onclientclick="return confirm('確定要設定此表單嗎？')"  />
                               </ItemTemplate>
                                 <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField> 
                            <asp:ButtonField ButtonType="Button" CommandName="Set_WF" HeaderText="管理" InsertVisible="False"
                                Text="設定流程">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:ButtonField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
           <table id="Ins_TB_Process" runat="server" cellpadding="3" cellspacing="0" class="tb_1"
            style="width: 800px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.GIF" />
                     審核流程新增 
                    <asp:Button ID="Insert_Btn" runat="server" CssClass="button-small" Text="確定新增" />
                </td>

            </tr>
            <tr>
                <td class="tb_title_w_1">
                    流程名稱</td>
                <td class="tb_title_w_1">
                    備註</td>
                <td class="tb_title_w_1">
                    表單項目</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="Process_Name_Ins" runat="server" Width="200px"></asp:TextBox></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="Remarks_Ins" runat="server" Width="200px"></asp:TextBox></td>
                <td class="tb_w_1">
                    <asp:DropDownList ID="ddl_Item" runat="server" Width="180px"></asp:DropDownList></td>
            </tr>
        </table>
       </div>
        <asp:SqlDataSource ID="Approval_DS" runat="server"></asp:SqlDataSource><table id="WF_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 750px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6" style="height: 27px">
                    <img src="Images/exe.gif" />
                    設定流程 | 流程名稱：<asp:Label ID="WF_Lable" runat="server"></asp:Label>
                    |</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView2" runat="server"
                        AutoGenerateColumns="False" CellPadding="3" CssClass="tb_1" Width="750px">
                        <Columns>
                            <asp:BoundField DataField="wf_order" HeaderText="順序">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="角色名稱">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("chk_name") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("chk_name") %>'></asp:Label>
                                    <asp:TextBox ID="role_txt" runat="server" Visible="False" Width="80px"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="關卡人員">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="label_userid" runat="server"></asp:Label>
                                    <asp:DropDownList ID="ddl_user" runat="server" Visible="False" >
                                    </asp:DropDownList>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="管理" ShowHeader="False">
                                <ItemTemplate>
                                    <asp:Button ID="Edit_RoleBtn" runat="server" CausesValidation="False" CommandName="Edit_Role"
                                        Text="修改角色名稱" OnClick="Edit_RoleBtn_Click" Visible="False" />
                                    <asp:Button ID="Del_Flow" runat="server" CausesValidation="false" CommandName="Del_Flow"
                                        Text="移除關卡" OnClick="Del_UserBtn_Click" Visible="False" />
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    &nbsp;<asp:Button ID="Submit" runat="server" CausesValidation="false"
                                        OnClick="Submit_Click" Text="確定" />
                                    <asp:Button ID="Cancel" runat="server" CausesValidation="false"
                                        OnClick="Cancel_Click" Text="取消" />
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="wf_inside_order" HeaderText="wf_inside_order">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    <asp:Button ID="InsWf_Btn" runat="server" Text="新增關卡" />
                    <asp:Button ID="Button1" runat="server" Text="回流程管理" /></td>
            </tr>
        </table>
        <asp:SqlDataSource ID="WF_DS" runat="server"></asp:SqlDataSource>
        <asp:HiddenField ID="hidden_apprvid" runat="server" Visible="False" /><asp:HiddenField ID="hidden_WFindex" runat="server" Visible="False" /><asp:HiddenField ID="Hidden_Mode" runat="server" Visible="False" />
        <asp:HiddenField ID="Hidden_insideid" runat="server" Visible="False" />
        <br />
        <table id="Ins_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 750px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="4" style="height: 27px">
                    <img src="Images/exe.gif" />
                    新增關卡流程</td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    關卡順序</td>
                <td class="tb_title_w_1">
                    角色名稱</td>
                <td class="tb_title_w_1">
                    關卡人員</td>
                <td class="tb_title_w_1">
                    全部使用者</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    &nbsp;<asp:DropDownList ID="ddl_FlowOrder" runat="server">
                    </asp:DropDownList></td>
                <td class="tb_w_1" style="color: #656b76">
                    <asp:TextBox ID="role_name" runat="server"></asp:TextBox>
                </td>
                <td class="tb_w_1">
                    <asp:Label ID="lab_dp" runat="server" Text="所別 :"></asp:Label>
                    <asp:DropDownList ID="ddl_dp" runat="server" OnSelectedIndexChanged="ddl_dp_selectedchange" AutoPostBack="true" ></asp:DropDownList>
                    <asp:Label ID="lab_deptment" runat="server" Text="單位 :"></asp:Label>
                    <asp:DropDownList ID="ddl_deptment" runat="server" OnSelectedIndexChanged="ddl_deptment_selectedchange" AutoPostBack="true" ></asp:DropDownList>
                    &nbsp;<asp:ListBox ID="lb_Ins_User" runat="server" SelectionMode="Multiple"></asp:ListBox></td>
                <td class="tb_w_1" style="color: #656b76">
                    <asp:CheckBox ID="chk_distin" runat="server" AutoPostBack="true"></asp:CheckBox>
                </td>
            </tr>
            <tr>
                <td align="left" class="tb_w_1" colspan="4">
                    <asp:Button ID="Ins_Btn" runat="server" Text="確定新增" />
                    <asp:Button ID="Cancel_Btn" runat="server" Text="取消新增" /></td>
            </tr>
        </table>
    </form>
</body>
</html>