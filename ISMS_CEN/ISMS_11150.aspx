﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_11150.aspx.vb" Inherits="ISMS_11150" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:HiddenField ID="Hidden_DP" runat="server" />
        <div>
            <br />
            <table id="TB1" cellpadding="0" cellspacing="1" class="tb_1" style="width: 650px" runat="server">
                <tr>
                    <td class="tb_title_w_2" colspan="8">
                        <img src="Images/exe.gif" alt="" /> 文件搜尋功能</td>
                </tr>
                <tr>
                    <td class="tb_title_w_1">
                        文件類別</td>
                    <td class="tb_title_w_1">
                        開始日期</td>
                    <td class="tb_title_w_1">
                        結束日期</td>
                    <td class="tb_title_w_1">
                        關鍵字</td>
                </tr>
                <tr >
                    <td class="tb_w_1">
                        <asp:DropDownList ID="DDL_DocType" runat="server">
                        </asp:DropDownList></td>
                    <td class="tb_w_1">
                        <asp:TextBox ID="St_Date" runat="server" Width="80px"></asp:TextBox></td>
                    <td class="tb_w_1">
                        <asp:TextBox ID="Ed_Date" runat="server" Width="80px"></asp:TextBox></td>
                    <td class="tb_w_1">
                        <asp:TextBox ID="Keyword_txt" runat="server"></asp:TextBox></td>
                </tr>
                <tr >
                    <td class="tb_w_1" colspan="6">
                        <asp:Button ID="Query_Btn" runat="server" CssClass="button-small" Text="查詢文件" /></td>
                </tr>
            </table>
            <br />
        </div>
    
    </div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
            CssClass="tb_1" Visible="False" Width="650px">
            <Columns>
                <asp:BoundField DataField="doc_type" HeaderText="文件類別">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="doc_num" HeaderText="文件編號">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="doc_name" HeaderText="文件名稱">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="doc_version" HeaderText="版次">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="doc_capsule" HeaderText="文件簡述">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="下載文件" ShowHeader="False">
                    <ItemTemplate>
                        <asp:Button ID="Button1" runat="server" CausesValidation="false" CommandName="" OnClick="Button1_Click"
                            Text="下載" />
                    </ItemTemplate>
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:TemplateField>
                <asp:BoundField DataField="doc_id" HeaderText="編號">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
            </Columns>
        </asp:GridView>
            <asp:SqlDataSource ID="Doc_DS" runat="server"></asp:SqlDataSource>
        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="St_Date">
        </cc1:CalendarExtender>
        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="Ed_Date">
        </cc1:CalendarExtender>
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
            EnableScriptLocalization="True">
        </asp:ScriptManager>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="VLD_ED_Date">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="VLD_ST_Date">
        </cc1:ValidatorCalloutExtender>
        <asp:CompareValidator ID="VLD_ED_Date" runat="server" ControlToValidate="Ed_Date"
            Display="None" ErrorMessage="日期格式錯誤!" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator><asp:CompareValidator
                ID="VLD_ST_Date" runat="server" ControlToValidate="St_Date" Display="None" ErrorMessage="日期格式錯誤!"
                Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
    </form>
</body>
</html>
