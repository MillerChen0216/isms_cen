﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports System.Collections.Generic

Partial Class DiaryLog
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim File_Path As String = ConfigurationManager.AppSettings("DiaryLog_Path")
    Dim dp As String = Me.Get_DP
    Dim apprvid As String = ConfigurationManager.AppSettings("flow_DiaryLog") '12
    Dim uid As String = User.Identity.Name
    Dim uname As String = Common.Get_User_Name(User.Identity.Name.ToString)

    Private Class SYSExam
        Public equipment As String
        Public quantity As String
        Public result As String
    End Class


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        If Not IsPostBack Then
            If Session("success") = "Y" Then
                Common.showMsg(Me.Page, "儲存成功！")
                Session("success") = ""
            End If
            GdvShow() '載入流程表
            DefaultSource() '一般記事 (預設)
            Session("Btn_examBack") = ""
        End If
    End Sub

    '載入流程表
    Private Sub GdvShow()
        Try
            Dim dt As DataTable = Common.Qry_workflowName_isms(apprvid, dp)
            GridView.DataSource = dt
            GridView.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料

            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            '***意見
            Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text

            '***Start
            Select Case o_wf_order
                Case "0"
                    '人員
                    pan_user.Visible = True : ddl_user.Visible = False : lb_userid.Text = uid
                    '狀態
                    ddl_status.Visible = True : lb_status.Visible = False
                    ddl_status.DataSource = Common.Get_WFStatus(0)
                    ddl_status.DataValueField = "wfstatus_id" : ddl_status.DataTextField = "wfstatus_name" : ddl_status.DataBind()
                    '意見
                    txt_comment.Visible = True : lb_comment.Visible = False
                Case "1"
                    Dim dt_WorkFlow As DataTable = Common.Get_Wf_User1_isms(apprvid, o_wf_order, "")
                    ddl_user.Visible = True : pan_user.Visible = False
                    ddl_user.Items.Clear()
                    ddl_user.DataValueField = "wf_userid" : ddl_user.DataTextField = "user_name"
                    ddl_user.DataSource = dt_WorkFlow : ddl_user.DataBind()
            End Select


            If pan_user.Visible = True And lb_userid.Text <> "" Then
                lb_username.Text = ISMS.Common.notNull(Common.Get_User_Name(lb_userid.Text))
                If lb_username.Text = "" Then lb_username.Text = Common.Get_venderemp_Name(lb_userid.Text) '表示非員工，可能為廠商
            End If
        End If
    End Sub


    '預設資料來源
    Private Sub DefaultSource()
        Try
            '一般記事(1)
            Dim newTable As New DataTable

            GridView1.DataSource = newTable
            GridView1.DataBind()

            newTable.Columns.Add("time_h")
            newTable.Columns.Add("time_m")
            newTable.Columns.Add("ap_comment")
            newTable.Columns.Add("ap_org")
            newTable.Columns.Add("ap_name")

            newTable.Rows.Add("0", "0", "", "", "")
            newTable.Rows.Add("0", "0", "", "", "")

            GridView2.DataSource = newTable
            GridView2.DataBind()


            '各項作業檢查表(2) - 抓最後一次資料
            Dim sql = "  select ROW_NUMBER() OVER(ORDER BY recordId) AS Num,* from DiaryLog_exam where DiaryLog_recordId in (select top 1 DiaryLog_recordId from DiaryLog_exam order by create_time desc)"
            Dim data = Common.Con(sql)

            GridView3.DataSource = data
            GridView3.DataBind()

        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_按鈕_增加一般記事
    Protected Sub Btn_Addnote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Addnote.Click
        Try

            Dim newTable As New DataTable
            newTable.Columns.Add("time_h")
            newTable.Columns.Add("time_m")
            newTable.Columns.Add("ap_comment")
            newTable.Columns.Add("ap_org")
            newTable.Columns.Add("ap_name")

            Dim count = GridView2.Rows.Count
            For Item As Integer = 0 To count - 1 Step 1
                Dim time_h = CType(GridView2.Rows(Item).Cells(0).FindControl("ddl_TimeH"), DropDownList).SelectedValue
                Dim time_m = CType(GridView2.Rows(Item).Cells(0).FindControl("ddl_TimeM"), DropDownList).SelectedValue
                Dim ap_comment = CType(GridView2.Rows(Item).Cells(0).FindControl("ap_comment"), TextBox).Text
                Dim ap_org = CType(GridView2.Rows(Item).Cells(0).FindControl("ap_org"), TextBox).Text
                Dim ap_name = CType(GridView2.Rows(Item).Cells(0).FindControl("ap_name"), TextBox).Text

                newTable.Rows.Add(time_h, time_m, ap_comment, ap_org, ap_name)
            Next

            newTable.Rows.Add("0", "0", "", "", "")
            GridView2.DataSource = newTable
            GridView2.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_將使用者可以自行新增的一般記事新增的"時間來源" 及預設資料
    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ddl_TimeH As DropDownList = CType(e.Row.FindControl("ddl_TimeH"), DropDownList)
            Dim ddl_TimeM As DropDownList = CType(e.Row.FindControl("ddl_TimeM"), DropDownList)

            Dim time_h = CType(e.Row.FindControl("time_h"), Label).Text
            Dim time_m = CType(e.Row.FindControl("time_m"), Label).Text

            ddl_TimeH.Items.Clear()
            ddl_TimeM.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                ddl_TimeH.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                ddl_TimeM.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            ddl_TimeH.SelectedValue = time_h
            ddl_TimeM.SelectedValue = time_m

        End If
    End Sub

    '2_按鈕_主頁至各項作業檢查表
    Protected Sub Btn_exam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_exam.Click
        Try
            Table2.Visible = True
            Table1.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '2_按鈕_增加各項作業檢查
    Protected Sub Btn_Addexam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Addexam.Click
        Try

            Dim newTable As New DataTable
            newTable.Columns.Add("Num")
            newTable.Columns.Add("comment")
            newTable.Columns.Add("result")

            Dim count = GridView3.Rows.Count
            For Item As Integer = 0 To count - 1 Step 1
                Dim coment = CType(GridView3.Rows(Item).Cells(0).FindControl("coment"), TextBox).Text
                Dim result = CType(GridView3.Rows(Item).Cells(0).FindControl("result"), TextBox).Text

                newTable.Rows.Add(Item + 1, coment, result)
            Next

            newTable.Rows.Add(count + 1, "", "")
            GridView3.DataSource = newTable
            GridView3.DataBind()
        Catch ex As Exception
            lb_msg2.Text = ex.Message
        End Try
    End Sub

    '2_按鈕_各項作業檢查表至主頁
    Protected Sub Btn_examBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_examBack.Click
        Try
            Table2.Visible = False
            Table1.Visible = True
            Session("Btn_examBack") = "有點擊過"
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '按鈕_匯入申請表
    Protected Sub Btn_DiaryLog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_DiaryLog.Click
        Try
            If Not String.IsNullOrEmpty(applyDate.Text) Then
                Dim sql = " select * from SYSIO where using = 'Y' and applyDate = '" + Common.YearYYYY(applyDate.Text) + "'"
                Dim data = Common.Con(sql)
                GridView1.DataSource = data
                GridView1.DataBind()
            Else
                Common.showMsg(Me.Page, "請確實填寫日期")
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '匯入申請表的隱藏欄位
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        'Response.Write(e.Row.Cells(2).Visible)
        e.Row.Cells(0).Visible = False
    End Sub

    '一般記事內的連結
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Try
            Dim SYSIO_recrodId As String = e.CommandArgument

            Select Case e.CommandName
                Case "Btn_SYSIO"
                    Response.Write("<script>window.open('ViewSYSIO.aspx','_target')</script>")
                    Session("SYSIO_recordId") = SYSIO_recrodId
            End Select
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '送出審核
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            'String.IsNullOrEmpty()

            '一般記事
            Dim count = GridView1.Rows.Count
            Dim noteCheck = False
            If count = 0 Then
                count += GridView2.Rows.Count
                For Item As Integer = 0 To count - 1 Step 1
                    Dim time_h = CType(GridView2.Rows(Item).Cells(0).FindControl("ddl_TimeH"), DropDownList).SelectedValue
                    Dim time_m = CType(GridView2.Rows(Item).Cells(0).FindControl("ddl_TimeM"), DropDownList).SelectedValue
                    Dim ap_comment = CType(GridView2.Rows(Item).Cells(0).FindControl("ap_comment"), TextBox).Text
                    Dim ap_org = CType(GridView2.Rows(Item).Cells(0).FindControl("ap_org"), TextBox).Text
                    Dim ap_name = CType(GridView2.Rows(Item).Cells(0).FindControl("ap_name"), TextBox).Text
                    If Not String.IsNullOrEmpty(ap_comment) And Not String.IsNullOrEmpty(ap_org) And Not String.IsNullOrEmpty(ap_name) Then
                        noteCheck = True
                        Exit For
                    End If
                Next
            Else
                noteCheck = True
            End If
            '作業各項檢查

            count = GridView3.Rows.Count
            Dim examCheck = False
            For Item As Integer = 0 To count - 1 Step 1
                Dim coment = CType(GridView3.Rows(Item).Cells(0).FindControl("coment"), TextBox).Text
                Dim result = CType(GridView3.Rows(Item).Cells(0).FindControl("result"), TextBox).Text
                If Not String.IsNullOrEmpty(coment) And Not String.IsNullOrEmpty(result) Then
                    examCheck = True
                    Exit For
                End If
            Next

            If String.IsNullOrEmpty(applyDate.Text) Or Not noteCheck Or Not examCheck Or Not Session("Btn_examBack") = "有點擊過" Then
                Dim msg As String = ""
                If String.IsNullOrEmpty(applyDate.Text) Then
                    msg += "日期"
                End If
                If Not noteCheck Then
                    If msg = "" Then
                        msg = "一般記事"
                    Else
                        msg += ", 一般記事"
                    End If
                End If
                If Not examCheck Then
                    If msg = "" Then
                        msg = "作業各項檢查"
                    Else
                        msg += ", 作業各項檢查"
                    End If
                End If
                If Not Session("Btn_examBack") = "有點擊過" Then
                    If msg = "" Then
                        msg = "請執行「各項作業檢查」確定資料"
                    Else
                        msg += ", 請執行「各項作業檢查」確定資料"
                    End If
                End If
                Common.showMsg(Me.Page, "請確實填寫" + msg)
                Return
            End If


            If GridView.Rows.Count < 1 Then
                Common.showMsg(Me.Page, "請先至(系統管理->審核流程管理)設定關卡流程")
                Return
            End If

            Dim Now As String = Common.YearYYYY(applyDate.Text) '日期
            Dim applyDate_Chn As String = Common.Year(Now).Replace("/", "") '民國YYYMMDD
            Dim sql = "SELECT * FROM DiaryLog WHERE applyDate_Chn = '" + applyDate_Chn + "' ORDER BY recordId DESC"
            Dim data As DataTable = Common.Con(sql)
            Dim applyDate_Chn_SN = 1
            If Not (data.Rows.Count = 0) Then
                applyDate_Chn_SN = Integer.Parse(data.Rows(0)("applyDate_Chn_SN")) + 1 'SN
            End If


            Dim recordid As String = "A2-" + applyDate_Chn + "-" + applyDate_Chn_SN.ToString.PadLeft(3, "0") '表單編號


            'DiaryLog_notes   
            count = GridView1.Rows.Count '匯入的
            For Item As Integer = 0 To count - 1 Step 1
                Dim Hr = CType(GridView1.Rows(Item).Cells(0).FindControl("Hr"), Label).Text
                Dim Min = CType(GridView1.Rows(Item).Cells(0).FindControl("Min"), Label).Text
                Dim ap_comment = CType(GridView1.Rows(Item).Cells(0).FindControl("ap_comment"), LinkButton).Text
                Dim ap_org = CType(GridView1.Rows(Item).Cells(0).FindControl("ap_org"), Label).Text
                Dim ap_name = CType(GridView1.Rows(Item).Cells(0).FindControl("ap_name"), Label).Text
                Dim SYSIO_recordId = CType(GridView1.Rows(Item).Cells(0).FindControl("SYSIO_recordId"), Label).Text
                If Not String.IsNullOrEmpty(ap_comment) And Not String.IsNullOrEmpty(ap_org) And Not String.IsNullOrEmpty(ap_name) Then
                    Dim notes_Id = "drylognotes" + Date.Now.AddMilliseconds(Item).ToString("yyyymmddHHmmssfff")
                    SqlTxt += " insert into DiaryLog_notes  ( recordId, DiaryLog_recordId, kind,SYSIO_recordId, time_h, time_m, ap_comment, ap_org, ap_name, create_time, op_time )" _
                        + "values ('" + notes_Id + "','" + recordid + "','1','" + SYSIO_recordId + "','" + Hr + "','" + Min + "','" + ap_comment + "','" + ap_org + "','" + ap_name + "',getdate(),getdate())  "
                End If
            Next

            count = GridView2.Rows.Count '自己輸入的
            For Item As Integer = 0 To count - 1 Step 1
                Dim time_h = CType(GridView2.Rows(Item).Cells(0).FindControl("ddl_TimeH"), DropDownList).SelectedValue
                Dim time_m = CType(GridView2.Rows(Item).Cells(0).FindControl("ddl_TimeM"), DropDownList).SelectedValue
                Dim ap_comment = CType(GridView2.Rows(Item).Cells(0).FindControl("ap_comment"), TextBox).Text
                Dim ap_org = CType(GridView2.Rows(Item).Cells(0).FindControl("ap_org"), TextBox).Text
                Dim ap_name = CType(GridView2.Rows(Item).Cells(0).FindControl("ap_name"), TextBox).Text
                If Not String.IsNullOrEmpty(ap_comment) And Not String.IsNullOrEmpty(ap_org) And Not String.IsNullOrEmpty(ap_name) Then
                    Dim notes_Id = "drylognotes" + Date.Now.AddSeconds(1).AddMilliseconds(Item).ToString("yyyymmddHHmmssfff")
                    SqlTxt += " insert into DiaryLog_notes  ( recordId, DiaryLog_recordId, kind, time_h, time_m, ap_comment, ap_org, ap_name, create_time, op_time )" _
                        + "values ('" + notes_Id + "','" + recordid + "','2','" + time_h + "','" + time_m + "','" + ap_comment + "','" + ap_org + "','" + ap_name + "',getdate(),getdate())  "
                End If
            Next

            'DiaryLog_exam   
            count = GridView3.Rows.Count
            For Item As Integer = 0 To count - 1 Step 1
                Dim coment = CType(GridView3.Rows(Item).Cells(0).FindControl("coment"), TextBox).Text
                Dim result = CType(GridView3.Rows(Item).Cells(0).FindControl("result"), TextBox).Text
                If Not String.IsNullOrEmpty(coment) And Not String.IsNullOrEmpty(result) Then
                    Dim exam_Id = "drylogexam" + Date.Now.AddSeconds(1).AddMilliseconds(Item).ToString("yyyymmddHHmmssfff")
                    SqlTxt += " insert into DiaryLog_exam  ( recordId, DiaryLog_recordId, comment, result, create_time, op_time )" _
                        + "values ('" + exam_Id + "','" + recordid + "','" + coment + "','" + result + "',getdate(),getdate())"
                End If
            Next



            '處理上傳檔案
            If CType(Me.FindControl("File_applyFile"), FileUpload).HasFile Then
                '有檔案
                Dim oriFileName = CType(Me.FindControl("File_applyFile"), FileUpload)
                Dim UpFileName As String = Common.UpFile(File_Path, recordid, CType(Me.FindControl("File_applyFile"), FileUpload))
                '塞入主表資料
                SqlTxt += "  insert into DiaryLog  ( recordId, applyDate, applyDate_Chn, applyDate_Chn_SN, empUid, empName," _
                    + "checkStatus, comment, workitems, applyFile_ori_name, applyFile, using, create_time, op_time ) values (" _
                    + "'" + recordid + "','" + Now + "','" + applyDate_Chn + "','" + applyDate_Chn_SN.ToString() + "','" + uid + "','" + uname + "'," _
                    + "'" + Radio_checkStatus.SelectedValue + "','" + comment.Text + "','" + workitems.Text + "','" + oriFileName.FileName + "','" + UpFileName + "'," _
                    + "'Y',getdate(),getdate())"

            Else
                '沒有檔案
                '塞入主表資料
                SqlTxt += "  insert into DiaryLog  ( recordId, applyDate, applyDate_Chn, applyDate_Chn_SN, empUid, empName," _
                    + "checkStatus, comment, workitems, using, create_time, op_time ) values (" _
                    + "'" + recordid + "','" + Now + "','" + applyDate_Chn + "','" + applyDate_Chn_SN.ToString() + "','" + uid + "','" + uname + "'," _
                    + "'" + Radio_checkStatus.SelectedValue + "','" + comment.Text + "','" + workitems.Text + "'," _
                    + "'Y',getdate(),getdate())"

            End If


            '申請人
            SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype ,dp) values "
            SqlTxt += " ('" + apprvid + "','" + recordid + "','" + CType(GridView.Rows(0).FindControl("wf_order"), Label).Text + "','" + uid + "',"
            SqlTxt += "'" + CType(GridView.Rows(0).FindControl("ddl_status"), DropDownList).SelectedValue + "','" + CType(GridView.Rows(0).FindControl("txb_comment"), TextBox).Text + "',getdate() ,'0', 'vender','" + dp + "') "


            '下一位審核人
            If GridView.Rows.Count >= 2 Then
                SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype ,dp) values "
                SqlTxt += " ('" + apprvid + "','" + recordid + "','" + CType(GridView.Rows(1).FindControl("wf_order"), Label).Text + "','" + CType(GridView.Rows(1).FindControl("ddl_user"), DropDownList).SelectedValue + "',"
                SqlTxt += " '1', '',getdate() ,'1', 'land','" + dp + "') "
            End If

            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Session("success") = "Y"
            Response.Redirect("DiaryLog.aspx", True)
            Common.showMsg(Me.Page, "儲存成功！")
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

End Class
