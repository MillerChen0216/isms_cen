﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_11160.aspx.vb" Inherits="ISMS_11160" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <form id="form1" runat="server">
        <br />
 <asp:HiddenField ID="Hidden_DP" runat="server" />

            <table id="TABLE1" class="tb_1" style="width: 300px" runat="server">
                <tr>
                    <td class="tb_title_w_2" colspan="5" >
                        <img src="Images/exe.gif" />
                        檢視下載記錄</td>
                </tr>
                <tr>
                    <td class="tb_title_w_1">
                        檢視類別</td>
                    <td class="tb_title_w_1">
                        關鍵字</td>
                </tr>
                <tr>
                    <td class="tb_w_1">
                            <asp:DropDownList ID="DDL_Type" runat="server">
                            </asp:DropDownList></td>
                        <td class="tb_w_1">
                            <asp:TextBox ID="Keyword_txt" runat="server"></asp:TextBox></td>
                </tr>
                <tr >
                    <td class="tb_w_1" colspan="5">
                        <asp:Button ID="QueryBtn" runat="server" CssClass="button-small" Text="搜尋紀錄" /></td>
                </tr>
            </table>
        <br />
        <table id="Doc_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 500px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    <asp:Label ID="LB_List" runat="server"></asp:Label>
                    | 依文件查詢結果 |</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
            CssClass="tb_1" Width="500px">
                        <Columns>
                            <asp:BoundField DataField="doc_num" HeaderText="文件編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_name" HeaderText="文件名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_version" HeaderText="文件版別">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="下載記錄">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Button ID="UserDown_Btn" runat="server" OnClick="UserDown_Btn_Click" Text="人員下載記錄" />&nbsp;
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="doc_id" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="Hidden_DocId" runat="server" Visible="False" /><asp:HiddenField ID="Hidden_userId" runat="server" Visible="False" />
        &nbsp;
        <br />
        <table id="Result_TB1" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 300px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    <asp:Label ID="Label_txt" runat="server"></asp:Label>
                    | 下載紀錄結果 |</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="3"
            CssClass="tb_1" Width="300px">
                        <Columns>
                            <asp:BoundField DataField="user_id" HeaderText="完成下載">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CellPadding="3"
            CssClass="tb_1" Width="300px">
                        <Columns>
                            <asp:BoundField DataField="user_id" HeaderText="尚未下載">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
            </Columns>
        </asp:GridView>
</td>
            </tr>
        </table>
        <table id="TB_User" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 500px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    依人員查詢結果</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" CellPadding="3"
            CssClass="tb_1" Width="500px">
                        <Columns>
                            <asp:BoundField DataField="user_id" HeaderText="使用者">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="文件下載記錄">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Button ID="DocDown_Btn" runat="server"  Text="文件下載記錄" OnClick="DocDown_Btn_Click" />&nbsp;
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <table id="Result_TB2" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 500px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    <asp:Label ID="user_lb" runat="server"></asp:Label>
                    | 查詢結果 |</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView5" runat="server" AutoGenerateColumns="False" CellPadding="3"
            CssClass="tb_1" Width="500px">
                        <Columns>
                            <asp:BoundField DataField="doc_type" HeaderText="文件類別">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_num" HeaderText="文件代碼">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_name" HeaderText="文件名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_version" HeaderText="文件版別">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="下載記錄">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="DownRecordLB" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="doc_id" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <asp:SqlDataSource ID="SQLDS1" runat="server"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SQLDS2" runat="server"></asp:SqlDataSource><asp:SqlDataSource ID="SQLDS3" runat="server"></asp:SqlDataSource>
        <asp:SqlDataSource ID="Doc_DS" runat="server"></asp:SqlDataSource>
    </form>
</body>
</html>
