﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_31140.aspx.vb" Inherits="ISMS_31140" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HiddenField ID="Hidden_DP" runat="server" />
        <br />
        <table id="Doc_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 770px"
            visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    風險評鑑填表 &nbsp;
                    <asp:DropDownList ID="ddl_Asset" runat="server" AutoPostBack="True">
                    </asp:DropDownList>&nbsp;
                    <asp:Button ID="Output_XLS" runat="server" Text="另存Excel檔案" Visible="False" CssClass="button-small"/></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3" CssClass="tb_1" Width="770px" Visible="False">
                       
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <asp:SqlDataSource ID="RiskForm_DS" runat="server"></asp:SqlDataSource>
        <asp:SqlDataSource ID="WF_DS" runat="server"></asp:SqlDataSource>
        <br />
        <table id="tb2" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 550px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" >
                    <img src="Images/exe.gif" />
                    風險評鑑審核作業 -
                    <asp:Label ID="lb_status" runat="server">風險評鑑填表：</asp:Label>
                    <asp:TextBox ID="Risk_name" runat="server" AutoPostBack="True" CssClass="input.button-small"></asp:TextBox>&nbsp;
                </td>
             
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="550px">
                        <Columns>
                            <asp:BoundField DataField="wf_order" HeaderText="關卡">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="chk_name" HeaderText="審核身分">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="審核人員">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lb_username" runat="server"></asp:Label>
                                    (<asp:Label ID="label_userid" runat="server"></asp:Label>)
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="狀態">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddl_status" runat="server" Visible="False">
                                    </asp:DropDownList>
                                    <asp:Label ID="lb_status" runat="server" Visible="False"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="意見">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="comment_lb" runat="server" Visible="False"></asp:Label>
                                    <asp:TextBox ID="comment_txt" runat="server" Visible="False" Width="120px"></asp:TextBox>&nbsp;
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="wf_inside_order" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    <asp:Button ID="Send_WF" runat="server" Text="送出審核" /></td>
            </tr>
        </table>
        &nbsp;
    
    </div>
    </form>
</body>
</html>
