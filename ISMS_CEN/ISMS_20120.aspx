﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_20120.aspx.vb" Inherits="ISMS_20120" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
                <asp:HiddenField ID="Hidden_DP" runat="server" />
        <br />
        <table id="Show_TB" class="tb_1" style="width: 500px" runat="server" visible="true" cellpadding="0" cellspacing="0">
            <tr>
                <td class="tb_title_w_2" colspan="5" style="width: 500px" rowspan="1">
                    <img src="Images/exe.gif" />
                    資產項目管理|列表
                    <asp:DropDownList ID="ddl_Asset" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:Button ID="Show_Insert" runat="server" CssClass="button-small" Text="新增" /></td>
            </tr>
            <tr>
                <td rowspan="1">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
                        CellPadding="0" Width="500px">
                        <Columns>
                            <asp:BoundField DataField="assitem_id" HeaderText="資產代碼">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" Width="20%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="assitem_name" HeaderText="資產名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:ButtonField ButtonType="Button" Text="編輯" CommandName="Edit_Btn">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" Width="15%" />
                            </asp:ButtonField>
                            <asp:ButtonField ButtonType="Button" Text="刪除" CommandName="Del_Btn">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" Width="15%" />
                            </asp:ButtonField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
        <br />
        <table id="ins_TB" class="tb_1" style="width: 800px" runat="server" visible="false" cellpadding="1" cellspacing="1">
            <tr>
                <td class="tb_title_w_2" colspan="6" >
                    <img src="Images/exe.gif" />
                    資產項目管理 | 新增 &nbsp;
                    <asp:Button ID="Insert_Btn" runat="server" CssClass="button-small" Text="確定送出" /></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" >
                    大分類</td>
                <td class="tb_w_1" ><asp:DropDownList ID="ddl_Asset_ins" runat="server" AutoPostBack="True" >
                </asp:DropDownList></td>
                <td class="tb_title_w_1" >
                    資產群組</td>
                    <td class="tb_w_1" ><asp:DropDownList ID="ddl_assgroup_Ins" runat="server" AutoPostBack="True" >
                </asp:DropDownList></td>
               </tr> 
               <tr>
                <td class="tb_title_w_1" >
                    權責單位</td>
                <td class="tb_w_1" >
                    <asp:DropDownList ID="ddl_Dept_Ins" runat="server">
                    </asp:DropDownList></td>
                                    <td class="tb_title_w_1">
                    放置地點</td>
                <td class="tb_w_1" >
                    <asp:TextBox ID="location_ins" runat="server" MaxLength="256"></asp:TextBox></td>
            </tr>
            <tr >
                <td class="tb_title_w_1" >
                    資產編號<font color="red">*</font></td>
                <td class="tb_w_1" >
                    <asp:TextBox ID="assitem_id_ins" runat="server" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="assitem_id_ins"
                        Display="None" ErrorMessage="代碼請勿空白！"></asp:RequiredFieldValidator></td>
                <td class="tb_title_w_1">
                    資產名稱<font color="red">*</font></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="assitem_name_ins" runat="server" MaxLength="256"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="assitem_name_ins"
                        Display="None" ErrorMessage="名稱請勿空白！"></asp:RequiredFieldValidator></td>
            </tr>
            <tr >
                <td class="tb_title_w_1">
                    數量</td>
                <td class="tb_w_1" >
                    <asp:TextBox ID="item_amount_ins" runat="server" MaxLength="5" Width="60px"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="item_amount_ins"
                        ErrorMessage="僅能輸入數字" MaximumValue="99999" MinimumValue="0" Display="None"></asp:RangeValidator></td>
                <td class="tb_title_w_1">
                    使用人數</td>
                <td class="tb_w_1">
                    <asp:TextBox ID="use_amount_ins" runat="server" MaxLength="5" Width="60px"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="use_amount_ins"
                        ErrorMessage="僅能輸入數字" MaximumValue="99999" MinimumValue="0" Display="None"></asp:RangeValidator></td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    使用者</td>
                <td class="tb_w_1" >
                    <asp:DropDownList ID="ddl_user_id_ins" runat="server" Visible="False">
                    </asp:DropDownList><asp:TextBox ID="user_id_ins" runat="server" Width="100px"></asp:TextBox></td>
                <td class="tb_title_w_1">
                    擁有者</td>
                <td class="tb_w_1">
                    <asp:DropDownList ID="ddl_holder_ins" runat="server" Visible="False">
                    </asp:DropDownList><asp:TextBox ID="holder_ins" runat="server" Width="100px"></asp:TextBox></td>
            </tr>
            <tr >

                <td class="tb_title_w_1">
                    機密性<font color="red">*</font></td>
                <td class="tb_w_1_left" colspan="3">
                    <asp:DropDownList ID="ddl_Confidentiality_ins" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:Label ID="CIns_lb" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" >
                    完整性<font color="red">*</font></td>
                <td class="tb_w_1_left"  colspan="3">
                    <asp:DropDownList ID="ddl_Integrity_ins" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:Label ID="IIns_lb" runat="server"></asp:Label></td></tr>
                    <tr>
                <td class="tb_title_w_1" >
                    可用性<font color="red">*</font></td>
                <td class="tb_w_1_left" colspan="3" >
                    <asp:DropDownList ID="ddl_Availability_ins" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:Label ID="AIns_lb" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" >
                    是否評鑑</td>
                <td class="tb_w_1" >
                    <asp:DropDownList ID="ddl_Evaluate_ins" runat="server">
                        <asp:ListItem Value="Y">是</asp:ListItem>
                        <asp:ListItem Value="N">否</asp:ListItem>
                    </asp:DropDownList></td>
                <td class="tb_title_w_1">
                    是否報廢</td>
                <td class="tb_w_1">
                    <asp:DropDownList ID="ddl_expire_ins" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="Y">是</asp:ListItem>
                        <asp:ListItem Selected="True" Value="N">否</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr >
                <td class="tb_title_w_1" >
                    報廢日期</td>
                <td class="tb_w_1" >
                    <asp:TextBox ID="expire_date_ins" runat="server" Width="80px" AutoPostBack="True"></asp:TextBox></td>
                <td class="tb_title_w_1">
                    資產說明</td>
                <td class="tb_w_1">
                    <asp:TextBox ID="assitem_desc_ins" runat="server" TextMode="MultiLine" MaxLength="256"></asp:TextBox></td>
            </tr>
        </table>
        <br /><table id="Edit_TB" class="tb_1" style="width: 800px" runat="server" visible="false" cellpadding="1" cellspacing="1">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    資產項目管理 | 修改
                    <asp:Button ID="Update_Btn" runat="server" CssClass="button-small" Text="確定送出" /></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" >
                    大分類</td>
                <td class="tb_w_1"><asp:DropDownList ID="ddl_Asset_upd" runat="server" Enabled="False" >
                </asp:DropDownList></td>
                               <td class="tb_title_w_1" >
                    資產群組</td>
                    <td class="tb_w_1" ><asp:DropDownList ID="ddl_assgroup_upd" runat="server" AutoPostBack="True" >
                </asp:DropDownList></td>
               </tr> 
               <tr> 
                <td class="tb_title_w_1" >
                    權責單位</td>
                <td class="tb_w_1" >
                    <asp:DropDownList ID="ddl_Dept" runat="server">
                    </asp:DropDownList></td>
                                    <td class="tb_title_w_1">
                    放置地點</td>
                <td class="tb_w_1">
                    <asp:TextBox ID="location" runat="server" MaxLength="256"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    資產代碼<font color="red">*</font></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="assitem_id" runat="server" ReadOnly="True" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="assitem_id"
                        Display="None" ErrorMessage="代碼請勿空白"></asp:RequiredFieldValidator></td>
                <td class="tb_title_w_1">
                    資產名稱<font color="red">*</font></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="assitem_name" runat="server" MaxLength="256"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="assitem_name"
                        Display="None" ErrorMessage="名稱請勿空白"></asp:RequiredFieldValidator></td>
            </tr>
            <tr >
                <td class="tb_title_w_1">
                    數量</td>
                <td class="tb_w_1">
                    &nbsp;<asp:TextBox ID="item_amount" runat="server" Width="60px" MaxLength="5"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="item_amount"
                        ErrorMessage="僅能輸入數字" MaximumValue="99999" MinimumValue="0" Display="None"></asp:RangeValidator></td>
                <td class="tb_title_w_1">
                    使用人數</td>
                <td class="tb_w_1">
                    <asp:TextBox ID="use_amount" runat="server" MaxLength="5" Width="60px"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="use_amount"
                        ErrorMessage="僅能輸入數字" MaximumValue="99999" MinimumValue="0" Display="None"></asp:RangeValidator></td>
            </tr>
            <tr >
                <td class="tb_title_w_1">
                    使用者</td>
                <td class="tb_w_1">
                    <asp:DropDownList ID="ddl_user_id" runat="server" AutoPostBack="True" Visible="False">
                    </asp:DropDownList><asp:TextBox ID="user_id" runat="server" Width="100px"></asp:TextBox></td>
                <td class="tb_title_w_1">
                    擁有者</td>
                <td class="tb_w_1">
                    <asp:DropDownList ID="ddl_holder" runat="server" AutoPostBack="True" Visible="False">
                    </asp:DropDownList><asp:TextBox ID="holder" runat="server" Width="100px"></asp:TextBox>
                </td>
            </tr>
            <tr >
                <td class="tb_title_w_1">
                    機密性<font color="red">*</font></td>
                <td class="tb_w_1_left" colspan="3">
                    <asp:DropDownList ID="ddl_Confidentiality" runat="server" AutoPostBack="True">
                    </asp:DropDownList><asp:Label ID="Cedit_lb" runat="server"></asp:Label></td>
            </tr>
            <tr >
                <td class="tb_title_w_1">
                    完整性<font color="red">*</font></td>
                <td class="tb_w_1_left" colspan="3">
                    <asp:DropDownList ID="ddl_Integrity" runat="server" AutoPostBack="True">
                    </asp:DropDownList><asp:Label ID="Iedit_lb" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                <td class="tb_title_w_1">
                    可用性<font color="red">*</font></td>
                <td class="tb_w_1_left" colspan="3">
                    <asp:DropDownList ID="ddl_Availability" runat="server" AutoPostBack="True">
                    </asp:DropDownList><asp:Label ID="Aedit_lb" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    是否評鑑</td>
                <td class="tb_w_1">
                    <asp:DropDownList ID="ddl_Evaluate" runat="server">
                        <asp:ListItem Value="Y">是</asp:ListItem>
                        <asp:ListItem Value="N">否</asp:ListItem>
                    </asp:DropDownList></td>
                <td class="tb_title_w_1">
                    是否報廢</td>
                <td class="tb_w_1">
                    <asp:DropDownList ID="ddl_expire" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="Y">是</asp:ListItem>
                        <asp:ListItem Value="N">否</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr >
                <td class="tb_title_w_1" >
                    報廢日期</td>
                <td class="tb_w_1" >
                    <asp:TextBox ID="expire_date" runat="server" Width="80px" AutoPostBack="True"></asp:TextBox></td>
                <td class="tb_title_w_1" >
                    資產說明</td>
                <td class="tb_w_1" >
                    <asp:TextBox ID="assitem_desc" runat="server" TextMode="MultiLine" MaxLength="256"></asp:TextBox></td>
            </tr>
        </table>
        <br />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        &nbsp;&nbsp;
        <cc1:calendarextender id="CalendarExtender1" runat="server" targetcontrolid="expire_date_ins"></cc1:calendarextender>
        <cc1:calendarextender id="CalendarExtender2" runat="server" targetcontrolid="expire_date"></cc1:calendarextender>
        <cc1:validatorcalloutextender id="ValidatorCalloutExtender1" runat="server" targetcontrolid="VLD_expire_date_ins"></cc1:validatorcalloutextender>
        <cc1:validatorcalloutextender id="ValidatorCalloutExtender2" runat="server" targetcontrolid="VLD_expire_date"></cc1:validatorcalloutextender>
        <asp:CompareValidator ID="VLD_expire_date_ins" runat="server" ControlToValidate="expire_date_ins"
            Display="None" ErrorMessage="日期格式錯誤!" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator><asp:CompareValidator
                ID="VLD_expire_date" runat="server" ControlToValidate="expire_date" Display="None"
                ErrorMessage="日期格式錯誤!" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator><asp:ScriptManager
                    ID="ScriptManager1" runat="server" EnableScriptGlobalization="True" EnableScriptLocalization="True">
                </asp:ScriptManager>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RangeValidator1">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="RangeValidator2">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="RangeValidator3">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" TargetControlID="RangeValidator4">
        </cc1:ValidatorCalloutExtender><cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="server" TargetControlID="RequiredFieldValidator1">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="server" TargetControlID="RequiredFieldValidator2">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="server" TargetControlID="RequiredFieldValidator3">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="server" TargetControlID="RequiredFieldValidator4">
        </cc1:ValidatorCalloutExtender>
        <br />
        <br />
    </form>
</body>
</html>
