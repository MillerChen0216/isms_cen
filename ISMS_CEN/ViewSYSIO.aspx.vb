﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class ViewSYSIO
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim File_Path As String = ConfigurationManager.AppSettings("SYSIO_Path")
    Dim dp As String = Me.Get_DP
    Dim apprvid As String = ConfigurationManager.AppSettings("flow_SYSIO_rec") '11
    Dim uid As String = User.Identity.Name
    Dim uname As String = Common.Get_User_Name(User.Identity.Name.ToString)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Session("SYSIO_recordId")) Then
                SYSIO_recordId.Text = Session("SYSIO_recordId")
                LoadingData()
            Else
                Response.Redirect("DiaryLog.aspx", True)
            End If
        End If
    End Sub

    '載入SYSIO的資料
    Private Sub LoadingData()
        Try
            Dim GETrecordId = SYSIO_recordId.Text
            Dim sql = "select * from SYSIO where recordId = '" + GETrecordId + "'"
            Dim data = Common.Con(sql)

            applyDate.Text = Common.Year(data.Rows(0)("applyDate")) '申請日期
            SYSIO_recordId.Text = data.Rows(0)("recordId") '表單編號
            chk_target_1.Checked = If(data.Rows(0)("target_1") = "1", True, False) '維護標的_資訊系統
            chk_target_2.Checked = If(data.Rows(0)("target_2") = "1", True, False) '維護標的_資訊設備
            targetName.Text = data.Rows(0)("targetName") '標的名稱
            applyOrg.Text = data.Rows(0)("applyOrg") '申請單位
            applyName.Text = data.Rows(0)("applyName") '申請人員
            chk_method_1.Checked = If(data.Rows(0)("method_1") = "1", True, False) '維護方式_親至機房
            chk_method_2.Checked = If(data.Rows(0)("method_2") = "1", True, False) '維護方式_親至資訊(電腦)室
            chk_method_3.Checked = If(data.Rows(0)("method_3") = "1", True, False) '維護方式_Internet連線
            chk_method_4.Checked = If(data.Rows(0)("method_4") = "1", True, False) '維護方式_其他
            methodOther.Text = data.Rows(0)("methodOther") '維護方式_其他內容
            chk_location_1.Checked = If(data.Rows(0)("location_1") = "1", True, False) '存放位置_機房
            chk_location_2.Checked = If(data.Rows(0)("location_2") = "1", True, False) '存放位置_辦公(控制)室
            chk_location_3.Checked = If(data.Rows(0)("location_3") = "1", True, False) '存放位置_其他
            locationOther.Text = data.Rows(0)("locationOther") '存放位置_其他內容
            InDate.Text = Common.Year(data.Rows(0)("InDate")) '進出日期
            InTime.Text = data.Rows(0)("InTimeHr") + "：" + data.Rows(0)("InTimeMin") '進出時間_進時'進出時間_進分
            OutTime.Text = data.Rows(0)("OutTimeHr") + "：" + data.Rows(0)("OutTimeMin") '離開時間_出時'離開時間_出分
            applyComment.Text = data.Rows(0)("applyComment") '辦理事項
            chk_IfChangeSoftware.Checked = If(data.Rows(0)("IfChangeSoftware") = "1", True, False) '變更程式
            chk_IfSYSPG.Checked = If(data.Rows(0)("IfSYSPG") = "1", True, False) '軟體程式變更申請單
            chk_IfFirewall.Checked = If(data.Rows(0)("IfFirewall") = "1", True, False) '防火牆維護申請表
            chk_IfTest.Checked = If(data.Rows(0)("IfTest") = "1", True, False) '測試作業之程式
            chk_IfItemIn.Checked = If(data.Rows(0)("IfItemIn") = "1", True, False) '攜入
            chk_IfItemOut.Checked = If(data.Rows(0)("IfItemOut") = "1", True, False) '攜出
            chk_IfItemLocation.Checked = If(data.Rows(0)("IfItemLocation") = "1", True, False) '變更位置

            '載入選擇
            If chk_IfSYSPG.Checked = "1" Then
                Dim sql_related_syspgrecordId = "select * from SYSIO_related where SYSIO_recordId = '" + GETrecordId + "' and kind = 'PG'"
                Dim data_related = Common.Con(sql_related_syspgrecordId)
                If Not data_related.Rows.Count = 0 Then
                    SYSPG_recordId.Text = data_related.Rows(0)("related_recordId") 'SYSPG_recordId
                End If
            Else
                SYSPG_recordId.Text = ""
            End If
            If chk_IfFirewall.Checked = "1" Then
                Dim sql_related_firewallrecordId = "select * from SYSIO_related where SYSIO_recordId = '" + GETrecordId + "' and kind = 'FW'"
                Dim data_related = Common.Con(sql_related_firewallrecordId)
                If Not data_related.Rows.Count = 0 Then
                    Firewall_recordId.Text = data_related.Rows(0)("related_recordId") 'Firewall_recordId
                End If
            Else
                Firewall_recordId.Text = ""
            End If

            '檢測結果
            Dim sql_exam = "  select * from SYSIO_exam where SYSIO_recordId = '" + GETrecordId + "' "
            Dim data_exam = Common.Con(sql_exam)
            If data_exam.Rows.Count = 0 Then
            Else
                GridView2.DataSource = data_exam
                GridView2.DataBind()
                Session("newTable") = data_exam '紀錄
            End If

            '查看裡面是否有資料，有才秀出來
            If IsDBNull(data.Rows(0)("applyFile")) Then
                applyFile.Visible = False
            Else
                applyFile.Visible = True
                applyFile_link.Text = data.Rows(0)("applyFile")
            End If

        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

End Class
