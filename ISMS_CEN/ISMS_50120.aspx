﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_50120.aspx.vb" Inherits="ISMS_50120" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-color: #ffffff">
    <form id="form1" runat="server">
        <br />
                    <asp:HiddenField ID="Hidden_DP" runat="server" />

        <table id="Show_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 500px"
            visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    群組管理
                    <asp:Button ID="Show_Insert" runat="server" CssClass="button-small" Text="新增資料" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        CellPadding="3" CssClass="tb_1" Width="500px">
                        <Columns>
                            <asp:BoundField DataField="group_name" HeaderText="群組名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="group_desc" HeaderText="群組敘述">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:ButtonField ButtonType="Button" CommandName="Edit_Btn" Text="編輯">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:ButtonField>
                            <asp:ButtonField ButtonType="Button" CommandName="Del_Btn" Text="刪除" Visible="False">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="group_id" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <table id="Edit_TB" runat="server" cellpadding="3" cellspacing="0" class="tb_1" style="width: 500px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.GIF" />
                    群組管理
                    <asp:Button ID="Update_Btn" runat="server" CssClass="button-small" Text="確定編輯" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    群組名稱</td>
                <td class="tb_title_w_1">
                    群組敘述</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="Gp_Name_Edit" runat="server" Width="100px"></asp:TextBox></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="Gp_Dsc_Edit" runat="server" Width="144px"></asp:TextBox></td>
            </tr>
        </table>
        <table id="Ins_TB" runat="server" cellpadding="3" cellspacing="0" class="tb_1" style="width: 500px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.GIF" />
                    群組管理
                    <asp:Button ID="Insert_Btn" runat="server" CssClass="button-small" Text="確定新增" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    群組名稱</td>
                <td class="tb_title_w_1">
                    群組敘述</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="Gp_Name_Ins" runat="server" Width="100px"></asp:TextBox></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="Gp_Dsc_Ins" runat="server" Width="144px"></asp:TextBox></td>
            </tr>
        </table>
        <td class="tb_w_1">
        </td>
        <td class="tb_w_1">
        </td>
        <td class="tb_w_1">
        </td>
        <td class="tb_w_1">
        </td>
        <td class="tb_w_1">
        </td>
        <td class="tb_w_1">
        </td>
        <asp:HiddenField ID="Hidden_Groupid" runat="server" Visible="False" />
    </form>
</body>
</html>
