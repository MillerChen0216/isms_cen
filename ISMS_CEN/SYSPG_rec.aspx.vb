﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class SYSPG_rec
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim File_Path As String = ConfigurationManager.AppSettings("SYSPG_Path")
    Dim dp As String = Me.Get_DP
    Dim apprvid As String = ConfigurationManager.AppSettings("flow_SYSPG_rec") '14
    Dim uid As String = User.Identity.Name
    Dim uname As String = Common.Get_User_Name(User.Identity.Name.ToString)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        If Not IsPostBack Then
            If Session("success") = "Y" Then
                Common.showMsg(Me.Page, "儲存成功！")
                Session("success") = ""
            End If
            sysName_DataSource()
            GdvShow() '載入流程表
        End If
    End Sub

    '按鈕_主頁至選擇葉面
    Protected Sub Btn_Choose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Choose.Click
        Try
            GdvrecordId(DateTime.Now.AddMonths(-1), DateTime.Now)
            Table1.Visible = False
            Table2_1.Visible = True
            Table2_2.Visible = True
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '載入SYSPG
    Private Sub GdvrecordId(ByVal Startdate As Date, ByRef Enddate As Date)
        Try
            Dim sql As String = "select * from SYSPG where using = 'Y' and applyDate between '" + Startdate.ToString("yyyy/MM/dd") + "' and '" + Enddate.ToString("yyyy/MM/dd") + "' and  recordId not in (select distinct SYSPG_recordId from SYSPG_rec where using='Y')"
            sdate.Text = Common.Year(Startdate.ToString("yyyy/MM/dd"))
            edate.Text = Common.Year(Enddate.ToString("yyyy/MM/dd"))
            Dim data As DataTable = Common.Con(sql)
            For Each item In data.Select
                item("empName") = Common.Year(DateTime.Parse(item("applyDate")).ToString("yyyy/MM/dd"))
            Next
            GridView1.DataSource = data
            GridView1.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '按鈕_搜尋表單編號日期區間
    Protected Sub Btn_applyDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_applyDate.Click
        Try
            GdvrecordId(DateTime.Parse(Common.YearYYYY(sdate.Text)), DateTime.Parse(Common.YearYYYY(edate.Text)))
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub


    '選取某表單編號_至主頁
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Try
            Dim GETrecordId As String = e.CommandArgument
            SYSPG_recordId.Text = GETrecordId

            Dim sql = "select * from SYSPG where recordId = '" + GETrecordId + "'"
            Dim data = Common.Con(sql)

            applyDate.Text = Common.Year(data.Rows(0)("applyDate")) '申請日期
            sysName.Text = data.Rows(0)("syscd_Name") '系統名稱
            sysFunction.Text = data.Rows(0)("sysFunction") '功能名稱
            applyOrg.Text = data.Rows(0)("applyOrg") '申請單位
            applyName.Text = data.Rows(0)("applyName") '申請人員
            Radio_applyReason.SelectedValue = data.Rows(0)("applyReason") '變更原因
            applyAction.Text = data.Rows(0)("applyAction") '申請變更事項
            applyPath.Text = data.Rows(0)("applyPath") '程式路徑(詳列變更之程式存放路徑)
            If Not IsDBNull(data.Rows(0)("expectedTime")) Then
                expectedTime.Text = Common.Year(data.Rows(0)("expectedTime")) + " " + DateTime.Parse(data.Rows(0)("expectedTime")).ToString("HH:mm") '預定完成時間
            End If

            '查看裡面是否有資料，有才秀出來
            If IsDBNull(data.Rows(0)("applyFile")) Then
                applyFile.Visible = False
            Else
                applyFile.Visible = True
                applyFile_link.Text = data.Rows(0)("applyFile")
            End If


            '載入資訊維護暨攜出入申請單編號
            SYSIO_recordId.Text = ""
            Dim sql3 = "select * from SYSIO_related where related_recordId = '" + SYSPG_recordId.Text + "'"
            Dim data3 As DataTable = Common.Con(sql3)
            If Not data3.Rows.Count = 0 Then
                For Each item In data3.Select
                    If Not String.IsNullOrEmpty(item("original_SYSIO_recordId").ToString()) Then
                        If Not SYSIO_recordId.Text.Contains(item("original_SYSIO_recordId").ToString()) Then
                            SYSIO_recordId.Text += item("original_SYSIO_recordId") + ", "
                        End If
                    Else
                        SYSIO_recordId.Text += item("SYSIO_recordId") + ","
                    End If
                Next
            End If

            Table1.Visible = True
            Table2_1.Visible = False
            Table2_2.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '檔案下載_按鈕
    Protected Sub applyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + applyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '載入流程表
    Private Sub GdvShow()
        Try
            Dim dt As DataTable = Common.Qry_workflowName_isms(apprvid, dp)
            GridView.DataSource = dt
            GridView.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '時間資料來源
    Private Sub sysName_DataSource()
        Try
            Ddl_Hour.Items.Clear()
            Ddl_Min.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                Ddl_Hour.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                Ddl_Min.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料

            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            '***意見
            Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text

            '***Start
            Select Case o_wf_order
                Case "0"
                    '人員
                    pan_user.Visible = True : ddl_user.Visible = False : lb_userid.Text = uid
                    '狀態
                    ddl_status.Visible = True : lb_status.Visible = False
                    ddl_status.DataSource = Common.Get_WFStatus(0)
                    ddl_status.DataValueField = "wfstatus_id" : ddl_status.DataTextField = "wfstatus_name" : ddl_status.DataBind()
                    '意見
                    txt_comment.Visible = True : lb_comment.Visible = False
                Case "1"
                    Dim dt_WorkFlow As DataTable = Common.Get_Wf_User1_isms(apprvid, o_wf_order, "")
                    ddl_user.Visible = True : pan_user.Visible = False
                    ddl_user.Items.Clear()
                    ddl_user.DataValueField = "wf_userid" : ddl_user.DataTextField = "user_name"
                    ddl_user.DataSource = dt_WorkFlow : ddl_user.DataBind()
            End Select


            If pan_user.Visible = True And lb_userid.Text <> "" Then
                lb_username.Text = ISMS.Common.notNull(Common.Get_User_Name(lb_userid.Text))
                If lb_username.Text = "" Then lb_username.Text = Common.Get_venderemp_Name(lb_userid.Text) '表示非員工，可能為廠商
            End If
        End If
    End Sub

    '送出審核
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            If String.IsNullOrEmpty(SYSPG_recordId.Text) Or String.IsNullOrEmpty(action.Text) Or String.IsNullOrEmpty(actionTime.Text) Then
                Common.showMsg(Me.Page, "請確實填寫資訊")
                Return
            End If


            If GridView.Rows.Count < 1 Then
                Common.showMsg(Me.Page, "請先至(系統管理->審核流程管理)設定關卡流程")
                Return
            End If

            Dim recordId As String = "Spgrc" + DateTime.Now.ToString("yyyymmddHHmmssfff") 'SYSPG_rec表ID

            Dim action_time As String = "null"
            '處理預定完成時間
            If Not String.IsNullOrEmpty(expectedTime.Text) Then
                action_time = "'" + Common.YearYYYY(actionTime.Text) + " " + Ddl_Hour.SelectedValue + ":" + Ddl_Min.SelectedValue + "'"
            End If


            '處理上傳檔案
            If CType(Me.FindControl("File_actionFile"), FileUpload).HasFile Then
                '有檔案
                Dim oriFileName = CType(Me.FindControl("File_actionFile"), FileUpload)
                Dim UpFileName As String = Common.UpFile(File_Path, recordId, CType(Me.FindControl("File_actionFile"), FileUpload))
                '塞入主表資料
                Dim te = recordId + SYSPG_recordId.Text + uid + uname + action.Text + action_time + oriFileName.FileName + UpFileName


                SqlTxt = " insert into SYSPG_rec (recordId,SYSPG_recordId,empUid,empName,action,actionTime,using,create_time" _
                    + ",op_time,actionFile_ori_name,actionFile)values('" + recordId + "','" + SYSPG_recordId.Text + "','" + uid + "','" + uname + "','" + action.Text + "'," + action_time + ",'Y',getdate(),getdate(),'" + oriFileName.FileName + "','" + UpFileName + "')"

            Else
                '沒有檔案
                '塞入主表資料
                SqlTxt = " insert into SYSPG_rec (recordId,SYSPG_recordId,empUid,empName,action,actionTime,using,create_time" _
                    + ",op_time)values('" + recordId + "','" + SYSPG_recordId.Text + "','" + uid + "','" + uname + "','" + action.Text + "'," + action_time + ",'Y',getdate(),getdate())"

            End If


            '申請人
            SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype ,dp) values "
            SqlTxt += " ('" + apprvid + "','" + recordId + "','" + CType(GridView.Rows(0).FindControl("wf_order"), Label).Text + "','" + uid + "',"
            SqlTxt += "'" + CType(GridView.Rows(0).FindControl("ddl_status"), DropDownList).SelectedValue + "','" + CType(GridView.Rows(0).FindControl("txb_comment"), TextBox).Text + "',getdate() ,'0', 'vender','" + dp + "') "


            '下一位審核人
            If GridView.Rows.Count >= 2 Then
                SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype ,dp) values "
                SqlTxt += " ('" + apprvid + "','" + recordId + "','" + CType(GridView.Rows(1).FindControl("wf_order"), Label).Text + "','" + CType(GridView.Rows(1).FindControl("ddl_user"), DropDownList).SelectedValue + "',"
                SqlTxt += " '1', '',getdate() ,'1', 'land','" + dp + "') "
            End If

            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Session("success") = "Y"
            Response.Redirect("SYSPG_rec.aspx", True)
            Common.showMsg(Me.Page, "儲存成功！")
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

End Class
