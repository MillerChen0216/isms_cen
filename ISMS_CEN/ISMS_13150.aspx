<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_13150.aspx.vb" Inherits="ISMS_13150" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>事件記錄表與矯正預防計畫表</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        &nbsp;<asp:HiddenField ID="Hidden_DP" runat="server" />
        <table id="Main_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 750px"
            visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    矯正預防措施單審核作業 | 列表&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="750px">
                        <Columns>
                            <asp:BoundField DataField="event_num" HeaderText="紀錄編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="event_source" HeaderText="事件來源">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="審核">
                                <ItemTemplate>
                                    <asp:Button ID="audit_btn" runat="server" OnClick="audit_btn_Click" Text="審核" Visible="False" />
                                    &nbsp;
                                    <asp:Button ID="Button1" runat="server" OnClick="audit_btn_Click" Text="查看" Visible="True" />
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="form_id" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="審核狀態">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lb_wf" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <table id="tb2" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 550px"
            visible="false">
            <tr>
                <td class="tb_title_w_2">
                    <img src="Images/exe.gif" />
                    審核作業</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="550px">
                        <Columns>
                            <asp:BoundField DataField="wf_order" HeaderText="關卡">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="chk_name" HeaderText="審核身分">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="審核人員">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lb_username" runat="server"></asp:Label>
                                    (<asp:Label ID="label_userid" runat="server"></asp:Label>)
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="狀態">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddl_status" runat="server" Visible="False">
                                    </asp:DropDownList>
                                    <asp:Label ID="lb_status" runat="server" Visible="False"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="意見">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="comment_lb" runat="server" Visible="False"></asp:Label>
                                    <asp:TextBox ID="comment_txt" runat="server" Visible="False" Width="100px"></asp:TextBox>
                                    <asp:Button ID="verify_btn" runat="server" OnClick="verify_btn_Click" Text="審核" Visible="False" />
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="wf_inside_order" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <table id="Event_TB" runat="server" cellpadding="2" cellspacing="0" class="tb_1" style="width: 700px"   visible="false" border="1">
            <tr>
                <td align="center" class="normal_font" colspan="6">
                    <asp:Button ID="save_btn" runat="server" Text="儲存資料" Visible="False" /></td>
            </tr>
            <tr>
                <td align="center" class="normal_font" colspan="6">
                    <asp:Label ID="Label21" runat="server" Font-Bold="True">矯正預防措施單</asp:Label></td>
            </tr>
            <tr>
             <td class="normal_font" colspan="6" align="left">
                 <asp:Label ID="Label8" runat="server">紀錄編號：</asp:Label>
                 <asp:TextBox ID="event_num" runat="server" Width="180px" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="6">
                    <asp:Label ID="Label13" runat="server">事件來源依據：</asp:Label>
                    <asp:TextBox ID="event_source" runat="server" Width="400px" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" class="normal_font" colspan="6">
                    <asp:Label ID="Label10" runat="server" Font-Bold="True">提出單位</asp:Label></td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="6">
                    <asp:Label ID="Label14" runat="server">異常事項說明：</asp:Label><br />
                    <asp:TextBox ID="current_desc" runat="server" Height="100px" TextMode="MultiLine" Width="800px" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="normal_font" colspan="5">
                    <asp:Label ID="Label4" runat="server">填表人：</asp:Label>
                    <asp:Label ID="douser" runat="server"></asp:Label>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <asp:Label ID="Label3" runat="server">直屬主管：</asp:Label>
                    <asp:Label ID="dousermgr" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" class="normal_font" colspan="6">
                    <asp:Label ID="Label5" runat="server" Font-Bold="True">權責單位</asp:Label>
                    </td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="6">
                    <asp:Label ID="Label15" runat="server">1.異常原因調查分析：</asp:Label>
                    <asp:TextBox ID="current_analyze" runat="server" Height="100px" TextMode="MultiLine" Width="800px" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="6">
                    <asp:Label ID="Label1" runat="server">2.處理對策 （預計完成期限</asp:Label>
                    <asp:TextBox ID="current_method_yy" runat="server" ReadOnly="True" Width="40px"></asp:TextBox>
                    <asp:Label ID="Label19" runat="server">年</asp:Label>
                    <asp:TextBox ID="current_method_mm" runat="server" ReadOnly="True" Width="40px"></asp:TextBox>
                    <asp:Label ID="Label26" runat="server">月</asp:Label>
                    <asp:TextBox ID="current_method_dd" runat="server" ReadOnly="True" Width="40px"></asp:TextBox>
                    <asp:Label ID="Label20" runat="server">日）</asp:Label>
                    <asp:TextBox ID="current_method" runat="server" Height="100px" TextMode="MultiLine" Width="800px" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" class="normal_font" colspan="6">
                                <asp:Label ID="Label7" runat="server">業管人員：</asp:Label>
                    <asp:Label ID="douser1" runat="server"></asp:Label>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <asp:Label ID="Label2" runat="server">直屬主管：</asp:Label>
                    <asp:Label ID="dousermgr1" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" class="normal_font" colspan="6">
                    <asp:Label ID="Label16" runat="server" Font-Bold="True">管理單位</asp:Label></td>
            </tr>
            <tr>
                <td align="left" class="normal_font" colspan="6">
                    <asp:CheckBox ID="flow_status" runat="server" Text="未結案" Enabled="False" />
                    ，原因：<asp:TextBox ID="flow_desc" runat="server" ReadOnly="True" Width="180px"></asp:TextBox>
                    展延完成日期至
                    <asp:TextBox ID="flow_yy" runat="server" ReadOnly="True" Width="40px"></asp:TextBox>
                    <asp:Label ID="Label11" runat="server">年</asp:Label>
                    <asp:TextBox ID="flow_mm" runat="server" ReadOnly="True" Width="40px"></asp:TextBox>
                    <asp:Label ID="Label12" runat="server">月</asp:Label>
                    <asp:TextBox ID="flow_dd" runat="server" ReadOnly="True" Width="40px"></asp:TextBox>
                    <asp:Label ID="Label17" runat="server">日</asp:Label><br />
                    <asp:CheckBox ID="close_status" runat="server" Text="已結案" Enabled="False" />，確認：<asp:TextBox ID="close_desc"
                        runat="server" ReadOnly="True" Width="180px"></asp:TextBox>
                    最後完成日期至
                    <asp:TextBox ID="close_yy" runat="server" ReadOnly="True" Width="40px"></asp:TextBox>
                    <asp:Label ID="Label18" runat="server">年</asp:Label>
                    <asp:TextBox ID="close_mm" runat="server" ReadOnly="True" Width="40px"></asp:TextBox>
                    <asp:Label ID="Label22" runat="server">月</asp:Label>
                    <asp:TextBox ID="close_dd" runat="server" ReadOnly="True" Width="40px"></asp:TextBox>
                    <asp:Label ID="Label23" runat="server">日</asp:Label></td>
            </tr>
            <tr>
                <td align="center" class="normal_font" colspan="6">
                    <asp:Label ID="Label6" runat="server">承辦人：</asp:Label>
                    <asp:Label ID="douser2" runat="server"></asp:Label>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <asp:Label ID="Label9" runat="server">直屬主管：</asp:Label>
                    <asp:Label ID="dousermgr2" runat="server"></asp:Label></td>
            </tr>
        </table>
        <br />
        <asp:SqlDataSource id="WF_DS" runat="server">
         
            </asp:SqlDataSource>
        <asp:SqlDataSource ID="Flow_DS" runat="server"></asp:SqlDataSource>
        <asp:HiddenField ID="hidden_apprvid" runat="server" Visible="False" /><asp:HiddenField ID="Hidden_Formid" runat="server" Visible="False" />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
    
    
    </form>
</body>
</html>
