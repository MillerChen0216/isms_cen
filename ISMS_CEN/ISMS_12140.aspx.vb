﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_12140
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim Ds As New DataSet


    Protected Sub Doc_SqlDS()
        Doc_DS.ConnectionString = Common.ConnDBS.ConnectionString
        SqlTxt = "SELECT * FROM docResoure where issue_date is not null and 1=1   and dp='" + Hidden_DP.Value + "'"

        If St_Date.Text <> "" And Ed_Date.Text <> "" Then
            SqlTxt = SqlTxt + " and issue_date >= '" + St_Date.Text + "' and issue_date <='" + Ed_Date.Text + "' "
        End If

        If DDL_DocType.SelectedValue = "0" Then
            SqlTxt = SqlTxt
        Else
            SqlTxt = SqlTxt + "and  doc_type = '" + DDL_DocType.SelectedValue + "'"
        End If
        Doc_DS.SelectCommand = SqlTxt
        GridView1.DataSourceID = Doc_DS.ID
    End Sub

    Protected Sub Show_DDL_DocType()

        DDL_DocType.DataSource = Common.Get_DocType
        DDL_DocType.DataTextField = "sValue"
        DDL_DocType.DataValueField = "doc_type"
        DDL_DocType.DataBind()
        DDL_DocType.Items.Insert(0, New ListItem("全部", "0"))

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP

        If Not Page.IsPostBack Then
            Show_DDL_DocType()
        End If

    End Sub

    Protected Sub Query_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Query_Btn.Click
        GridView1.Visible = True
        Doc_SqlDS()
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType <> DataControlRowType.Header Then

            e.Row.Cells(0).Text = Common.ISMS_DocNameChange(e.Row.Cells(0).Text)
            e.Row.Cells(4).Text = Common.Get_User_Name(e.Row.Cells(4).Text)
            e.Row.Cells(5).Text = Common.Get_User_Name(e.Row.Cells(5).Text)
        End If
    End Sub

End Class
