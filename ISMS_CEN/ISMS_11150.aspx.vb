﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_11150
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim Ds As New DataSet
    Dim i As Integer


    Protected Sub Doc_SqlDS()
        Doc_DS.ConnectionString = Common.ConnDBS.ConnectionString

        'If ck_box.Checked = False Then
        SqlTxt = "SELECT * FROM docResoure where 1=1 and   issue_date is not null and latest='YES'   and dp='" + Hidden_DP.Value + "'"

        If St_Date.Text <> "" And Ed_Date.Text <> "" Then
            SqlTxt = SqlTxt + " and issue_date >= '" + St_Date.Text + "' and issue_date <='" + Ed_Date.Text + "' "
        End If

        If Keyword_txt.Text <> "" Then
            SqlTxt = SqlTxt + " and (doc_capsule Like  '%" + Keyword_txt.Text + "%'  or doc_num  Like '%" + Keyword_txt.Text + "%' or doc_version Like '%" + Keyword_txt.Text + "%')"
        End If

        If DDL_DocType.SelectedValue = "0" Then
            SqlTxt = SqlTxt
        Else
            SqlTxt = SqlTxt + "and  doc_type = '" + DDL_DocType.SelectedValue + "'  order by doc_type,doc_num,doc_version "
        End If
        Doc_DS.SelectCommand = SqlTxt
        GridView1.DataSourceID = Doc_DS.ID
        'ElseIf ck_box.Checked = True Then

        'SqlTxt = "SELECT * FROM docResoure where 1=1 and   issue_date is not null  and dp='" + Hidden_DP.Value + "'"

        'If St_Date.Text <> "" And Ed_Date.Text <> "" Then
        '    SqlTxt = SqlTxt + " and issue_date >= '" + St_Date.Text + "' and issue_date <='" + Ed_Date.Text + "' "
        'End If

        'If Keyword_txt.Text <> "" Then
        '    SqlTxt = SqlTxt + " and (doc_capsule Like  '%" + Keyword_txt.Text + "%'  or doc_num  Like '%" + Keyword_txt.Text + "%' or doc_version Like '%" + Keyword_txt.Text + "%')"
        'End If

        'If DDL_DocType.SelectedValue = "0" Then
        '    SqlTxt = SqlTxt
        'Else
        '    SqlTxt = SqlTxt + "and  doc_type = '" + DDL_DocType.SelectedValue + "'  order by doc_type,doc_num,doc_version "
        'End If
        'Doc_DS.SelectCommand = SqlTxt
        'GridView1.DataSourceID = Doc_DS.ID

        'End If




    End Sub

    Protected Sub Show_DDL_DocType()

        DDL_DocType.DataSource = Common.Get_PowerDocType(User.Identity.Name.ToString)
        DDL_DocType.DataTextField = "sValue"
        DDL_DocType.DataValueField = "doc_type"
        DDL_DocType.DataBind()
        Ds = Common.Get_PowerDocType(User.Identity.Name.ToString)
        If Ds.Tables(0).Rows.Count >= 4 Then
            DDL_DocType.Items.Insert(0, New ListItem("全部", "0"))
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        If Not Page.IsPostBack Then
            Show_DDL_DocType()
        End If

    End Sub

    Protected Sub Query_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Query_Btn.Click
        GridView1.Visible = True
        Doc_SqlDS()
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(6).Visible = False
    End Sub


    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType <> DataControlRowType.Header Then
            e.Row.Cells(0).Text = Common.ISMS_DocNameChange(e.Row.Cells(0).Text)
        End If


    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", GridView1.Rows(Index).Cells(6).Text, System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "FILEDOWN", GridView1.Rows(Index).Cells(2).Text)

        Ds = Common.Get_Doc_info(Me.GridView1.Rows(Index).Cells(6).Text)
        Response.ContentType = "application/octet-stream"
        Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5")
        Response.AddHeader("content-disposition", "attachment;filename=" + Common.Decode((Ds.Tables(0).Rows(0).Item("doc_filename"))) + "")
        Response.WriteFile(Common.Sys_Config("File_Path") + "/" + ("" + (Ds.Tables(0).Rows(0).Item("doc_filename")) + ""))
        Response.End()

    End Sub
End Class
