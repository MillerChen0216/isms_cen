﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_30220
    Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ddl_Asset.DataSource = Common.Get_Asset(Hidden_DP.Value)
            ddl_Asset.DataValueField = "asset_id"
            ddl_Asset.DataTextField = "asset_name"
            ddl_Asset.DataBind()
            ddl_Asset_ins.DataSource = Common.Get_Asset(Hidden_DP.Value)
            ddl_Asset_ins.DataValueField = "asset_id"
            ddl_Asset_ins.DataTextField = "asset_name"
            ddl_Asset_ins.DataBind()
            ddl_Asset_upd.DataSource = Common.Get_Asset(Hidden_DP.Value)
            ddl_Asset_upd.DataValueField = "asset_id"
            ddl_Asset_upd.DataTextField = "asset_name"
            ddl_Asset_upd.DataBind()
        End If
        SqlDataSource()

    End Sub

    Protected Sub SqlDataSource()
        SqlDataSource1.ConnectionString = Conn.ConnectionString
        SqlDataSource1.SelectCommand = "SELECT threat_id,asset_id,threat FROM threat WHERE asset_id=" + ddl_Asset.SelectedValue
        GridView1.DataSourceID = SqlDataSource1.ID
        Dim KeyNames() As String = {"threat_id"}
        GridView1.DataKeyNames = KeyNames
    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)

        Select Case e.CommandName

            Case "Edit_Btn"

                Edit_TB.Visible = True
                Show_TB.Visible = False

                ddl_Asset_upd.SelectedValue = SelectedRow.Cells(0).Text
                threat_upd.Text = SelectedRow.Cells(2).Text

                Hidden_sn.Value = SelectedRow.Cells(1).Text

            Case "Del_Btn"
                SqlTxt = "DELETE FROM [threat] WHERE threat_id =" + SelectedRow.Cells(1).Text

                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                GridView1.DataBind()

        End Select

    End Sub

    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click

        SqlTxt = "UPDATE [threat] SET [threat] ='" + threat_upd.Text + "', [asset_id] =" + ddl_Asset_upd.SelectedValue + " WHERE threat_id =" + Hidden_sn.Value
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()

        GridView1.DataBind()
        Show_TB.Visible = True
        Edit_TB.Visible = False


    End Sub

    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        SqlTxt = "INSERT INTO [threat] ([asset_id],[threat]) VALUES (" + ddl_Asset_ins.SelectedValue + ",'" + threat_ins.Text + "')"

        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()

        GridView1.DataBind()
        Show_TB.Visible = True
        Ins_TB.Visible = False

    End Sub

    Protected Sub Show_Insert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert.Click
        Show_TB.Visible = False
        ins_TB.Visible = True
    End Sub
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated

        e.Row.Cells(0).Visible = False

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        Dim oDataRow As Data.DataRowView
        Dim sShowField As String
        Dim sMessage As String
        If e.Row.RowIndex <> -1 Then

            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = oDataRow.Item("threat").ToString()
            sMessage = String.Format("您確定要刪除 [{0}] 嗎?", sShowField)
            CType(e.Row.Cells(4).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"
        End If
    End Sub
End Class
