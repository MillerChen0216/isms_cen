﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports System.Web.Configuration
Partial Class Login
    Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader

    Protected Sub Auth_Date()

        'Dim sDate As Date
        'sDate = "2008/8/31"
        'Label3.Text = "系統到期日：" + sDate.AddMonths(7)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Auth_Date()     
    End Sub

    Protected Sub LoginButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim pmtUserName As New SqlParameter
        Dim pmtUserPassword As New SqlParameter

        SqlTxt = "Select * from ismsUser where user_id=@UserName and pwd=@Passwd and using='Y'"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()

        pmtUserName.ParameterName = "@UserName"
        pmtUserName.Value = Trim(UserName.Text.ToUpper)
        pmtUserPassword.ParameterName = "@Passwd"
        pmtUserPassword.Value = Trim(Common.Encode(Password.Text))


        'Response.Write(pmtUserPassword.Value)
        SqlCmd.Parameters.Add(pmtUserName)
        SqlCmd.Parameters.Add(pmtUserPassword)

        Dim txtID As String = CType(SqlCmd.ExecuteScalar, String)
        SqlCmd.Dispose()
        Conn.Dispose()
        Conn.Close()

        If Not String.IsNullOrEmpty(txtID) Then
            'e.Authenticated = True
           
            Dim ds As DataSet = Common.Check_Update(pmtUserName.Value.ToString, pmtUserPassword.Value, DateTime.Now.AddDays(-90).ToString("yyyyMMdd"))
            If ds.Tables(0).Rows.Count > 0 Then '超過3個月沒更新密碼
                
                Common.showMsg(Me.Page, "已超過3個月未更新密碼，請先更新密碼!")

                Label2.Text = "資訊安全管理系統-密碼更新"
                Label1.Text = "Information Security Management System-Update Password"

                pnl_login.Visible = False
                pnl_update.Visible = True
            Else
                Common.Ins_Log(pmtUserName.Value.ToString, "EMPLOYEE", "0", System.IO.Path.GetFileName(Request.PhysicalPath), "GENERAL", "LOGININ", "")
                System.Web.Security.FormsAuthentication.RedirectFromLoginPage(Trim(UserName.Text.ToUpper), True)
            End If

        Else
            lab_error.Visible = True
            Common.Ins_Log(pmtUserName.Value.ToString, "EMPLOYEE", "0", System.IO.Path.GetFileName(Request.PhysicalPath), "GENERAL", "LOGINFAIL", "")
            Dim dsfail As DataSet = Common.Get_loginfail(pmtUserName.Value.ToString, DateTime.Now.AddMinutes(-2).ToString("yyyy-MM-dd HH:mm:ss"))
            If dsfail.Tables(0).Rows.Count > 0 Then
                If CType(dsfail.Tables(0).Rows(0).Item("countfail").ToString, Integer) >= 3 Then '2分鐘內輸入帳號密碼失敗3次
                    Common.Update_User_Using(pmtUserName.Value.ToString)
                    Common.showMsg(Me.Page, "您已重複輸入3次帳號密碼失敗，此帳號予以停用。")
                End If
            End If
        End If

        pmtUserName = Nothing
        pmtUserPassword = Nothing

    End Sub
    Protected Sub UpdateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If txt_update_pwd1.Text <> txt_update_pwd2.Text Then '2次輸入密碼不同
            lab_pwderror.Text = "您輸入的密碼不同，請再重新輸入。"
            lab_pwderror.Visible = True
        Else
            Dim ds As DataSet = Common.Get_oldpwd(Trim(UserName.Text.ToUpper))
            Dim old_pwd As String() = ds.Tables(0).Rows(0).Item("old_pwd").ToString.Split(",")

            For Each st As String In old_pwd
                If Trim(Common.Encode(txt_update_pwd1.Text)) = st Then
                    lab_pwderror.Text = "密碼更新前3次不可重複，請再重新輸入。"
                    lab_pwderror.Visible = True
                    Return
                End If
            Next

            Dim old_pwd_db As String = check_oldpwd(ds.Tables(0).Rows(0).Item("old_pwd").ToString, Trim(Common.Encode(txt_update_pwd1.Text)))
         
            Dim pmtUserName As New SqlParameter
            Dim pmtUserPassword As New SqlParameter

            SqlTxt = " UPDATE ismsuser SET pwd=@Passwd ,update_date ='" + DateTime.Now.ToString("yyyyMMdd") + "' ,old_pwd = '" + old_pwd_db + "'Where user_id=@UserName "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()

            pmtUserName.ParameterName = "@UserName"
            pmtUserName.Value = Trim(UserName.Text.ToUpper)
            pmtUserPassword.ParameterName = "@Passwd"
            pmtUserPassword.Value = Trim(Common.Encode(txt_update_pwd1.Text))

            SqlCmd.Parameters.Add(pmtUserName)
            SqlCmd.Parameters.Add(pmtUserPassword)

            SqlCmd.ExecuteScalar()
            SqlCmd.Dispose()
            Conn.Dispose()
            Conn.Close()

            Common.Ins_Log(pmtUserName.Value.ToString, "EMPLOYEE", "0", System.IO.Path.GetFileName(Request.PhysicalPath), "GENERAL", "LOGININ", "")
            System.Web.Security.FormsAuthentication.RedirectFromLoginPage(Trim(UserName.Text.ToUpper), True)
        End If


      
    End Sub

    Protected Function check_oldpwd(ByVal oldpwd As String, ByVal newpwd As String) As String '資料庫只存最近3次更改的密碼
        Dim oldpwd2 As String() = oldpwd.Split(",")
        If oldpwd2.Length >= 3 Then
            Return oldpwd2(1) + "," + oldpwd2(2) + "," + newpwd
        ElseIf oldpwd = "" Then
            Return newpwd
        Else
            Return oldpwd + "," + newpwd
        End If
    End Function
End Class
