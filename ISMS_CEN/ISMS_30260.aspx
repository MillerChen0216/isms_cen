﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_30260.aspx.vb" Inherits="ISMS_30260" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <br />
                    <asp:HiddenField ID="Hidden_DP" runat="server" />

            <table id="Table3" runat="server" class="tb_1" style="width: 720px" visible="true">
                <tr>
                    <td class="tb_title_w_2" colspan="17">
                        <img src="Images/Icon1.GIF" alt="" />風險評鑑結果</td>
                </tr>
                <tr>
                    <td class="tb_title_w_1" style="width: 96px">
                        大分類</td>
                    <td class="tb_title_w_1" style="width: 498px">
                        評鑑表名稱</td>
                    <td class="tb_title_w_1" style="width: 84px">
                        日期</td>
                    <td class="tb_title_w_1">
                        管理</td>
                </tr>
                <tr style="color: #0033cc; background-color: #ffffff">
                    <td class="tb_w_1" style="width: 96px">
                        資訊類</td>
                    <td class="tb_w_1" style="width: 498px">
                        ISMS-風險評鑑表-S134-002</td>
                    <td class="tb_w_1" style="width: 84px">
                        2008/04/18</td>
                    <td class="tb_w_1" style="font-size: 12pt; font-family: arial,verdana,sans-serif">
                        <asp:Button ID="Button3" runat="server" CssClass="button-small" Text="瀏覽" /></td>
                </tr>
                <tr style="color: #0033cc; background-color: #ffffff">
                    <td class="tb_w_1" style="width: 96px">
                        文件類</td>
                    <td class="tb_w_1" style="width: 498px">
                        ISMS-風險評鑑表-B-001</td>
                    <td class="tb_w_1" style="width: 84px">
                        <span style="color: #345b7b">2008/04/10</span></td>
                    <td class="tb_w_1">
                        <asp:Button ID="Button1" runat="server" CssClass="button-small" Text="瀏覽" /></td>
                </tr>
            </table>
        </div>
        <span style="font-size: 12px; color: #2666a6; font-family: Arial; background-color: #b8e1f1">
        </span>
        <table id="Table1" runat="server" class="tb_1" style="width: 768px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="23">
                    <img src="Images/Icon1.GIF" />風險評鑑表： 資訊類-ISMS-風險評鑑表-S134-002 &nbsp; &nbsp;</td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 30px; height: 76px">
                        資產代碼</td>
                <td class="tb_title_w_1" style="height: 76px">
                        資產名稱</td>
                <td class="tb_title_w_1" style="width: 19px; height: 76px">
                        使用人</td>
                <td class="tb_title_w_1" style="width: 20px; height: 76px">
                        保管人</td>
                <td class="tb_title_w_1" style="height: 76px">
                        數量</td>
                <td class="tb_title_w_1" style="height: 76px">
                        放置地點</td>
                <td class="tb_title_w_1" style="width: 24px; height: 76px">
                        機密性</td>
                <td class="tb_title_w_1" style="width: 22px; height: 76px">
                        完整性</td>
                <td class="tb_title_w_1" style="width: 22px; height: 76px">
                        可用性</td>
                <td class="tb_title_w_1" style="width: 30px; height: 76px">
                    資產價值</td>
                <td class="tb_title_w_1" style="width: 29px; height: 76px">
                    資產安全等級</td>
                <td class="tb_title_w_1" style="width: 67px; height: 76px">
                        脆弱點敘述</td>
                <td class="tb_title_w_1" style="height: 76px">
                        威脅敘述</td>
                <td class="tb_title_w_1" style="height: 76px">
                        風險機率值</td>
                <td class="tb_title_w_1" style="height: 76px">
                        風險衝擊值</td>
                <td class="tb_title_w_1" style="height: 76px">
                        風險值</td>
                <td class="tb_title_w_1" style="height: 76px">
                        風險等級</td>
            </tr>
            <tr style="color: #0033cc; background-color: #ffffff">
                <td class="tb_w_1" rowspan="3" style="width: 30px">
                        IT001</td>
                <td class="tb_w_1" rowspan="3">
                        HP SERVER</td>
                <td class="tb_w_1" rowspan="3" style="width: 19px">
                    林芳智</td>
                <td class="tb_w_1" rowspan="3" style="width: 20px">
                    林芳智</td>
                <td class="tb_w_1" rowspan="3">
                    1</td>
                <td class="tb_w_1" rowspan="3">
                    機房</td>
                <td class="tb_w_1" rowspan="3" style="width: 24px">
                    1</td>
                <td class="tb_w_1" rowspan="3" style="width: 22px">
                    1</td>
                <td class="tb_w_1" rowspan="3" style="width: 22px">
                    1</td>
                <td class="tb_w_1" rowspan="3" style="width: 30px">
                    3</td>
                <td class="tb_w_1" rowspan="3" style="width: 29px">
                    1</td>
                <td class="tb_w_1" style="width: 67px">
                        使用者授權機制不足</td>
                <td class="tb_w_1">
                        外部人員未經授權存取</td>
                <td class="tb_w_1">
                    1</td>
                <td class="tb_w_1">
                    1</td>
                <td class="tb_w_1">
                    1</td>
                <td class="tb_w_1">
                    低</td>
            </tr>
            <tr style="color: #0033cc; background-color: #ffffff">
                <td class="tb_w_1" style="width: 67px">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
            </tr>
            <tr style="color: #0033cc; background-color: #ffffff">
                <td class="tb_w_1" style="width: 67px">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
            </tr>
            <tr style="color: #0033cc; background-color: #ffffff">
                <td class="tb_w_1" rowspan="3" style="width: 30px">
                        IT002</td>
                <td class="tb_w_1" rowspan="3">
                        IBM Server x225</td>
                <td class="tb_w_1" rowspan="3" style="width: 19px">
                    林芳智</td>
                <td class="tb_w_1" rowspan="3" style="width: 20px">
                        許慧玲</td>
                <td class="tb_w_1" rowspan="3">
                    1</td>
                <td class="tb_w_1" rowspan="3">
                    機房</td>
                <td class="tb_w_1" rowspan="3" style="width: 24px">
                    1</td>
                <td class="tb_w_1" rowspan="3" style="width: 22px">
                    1</td>
                <td class="tb_w_1" rowspan="3" style="width: 22px">
                    1</td>
                <td class="tb_w_1" rowspan="3" style="width: 30px">
                    3</td>
                <td class="tb_w_1" rowspan="3" style="width: 29px">
                    1</td>
                <td class="tb_w_1" style="width: 67px">
                        含敏感資料</td>
                <td class="tb_w_1">
                        內部人員誤用資料</td>
                <td class="tb_w_1">
                    2</td>
                <td class="tb_w_1">
                    2</td>
                <td class="tb_w_1">
                    4</td>
                <td class="tb_w_1">
                    低</td>
            </tr>
            <tr style="color: #0033cc; background-color: #ffffff">
                <td class="tb_w_1" style="width: 67px">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
            </tr>
            <tr style="color: #0033cc; background-color: #ffffff">
                <td class="tb_w_1" style="width: 67px">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
                <td class="tb_w_1">
                    -</td>
            </tr>
        </table>
        <br />
        <div>
            &nbsp;</div>
    
    </div>
    </form>
</body>
</html>
