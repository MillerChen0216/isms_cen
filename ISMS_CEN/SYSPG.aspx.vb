﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class SYSPG
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim File_Path As String = ConfigurationManager.AppSettings("SYSPG_Path")
    Dim dp As String = Me.Get_DP
    Dim apprvid As String = ConfigurationManager.AppSettings("flow_SYSPG") '13
    Dim uid As String = User.Identity.Name
    Dim uname As String = Common.Get_User_Name(User.Identity.Name.ToString)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        If Not IsPostBack Then
            sysName_DataSource() '系統名稱及時間來源
            If Session("success") = "Y" Then
                Common.showMsg(Me.Page, "儲存成功！")
                Session("success") = ""
            End If
            GdvShow() '載入流程表
        End If
    End Sub

    '載入流程表
    Private Sub GdvShow()
        Try
            Dim dt As DataTable = Common.Qry_workflowName_isms(apprvid, dp)
            GridView.DataSource = dt
            GridView.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '系統名稱資料來源 以及時間來源
    Private Sub sysName_DataSource()
        Try
            Dim sql As String = "SELECT * FROM SysCode WHERE syscd_Enabled = 1"
            sql += " ORDER BY syscd_time DESC "
            Ddl_sysName.Items.Clear()
            Ddl_sysName.DataTextField = "syscd_Name"
            Ddl_sysName.DataValueField = "syscd_Id"
            Ddl_sysName.DataSource = Common.Con(sql)
            Ddl_sysName.DataBind()
            Ddl_sysName.SelectedValue = Ddl_sysName.Items(0).Value '預設選項為第一個值

            Ddl_Hour.Items.Clear()
            Ddl_Min.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                Ddl_Hour.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                Ddl_Min.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料

            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            '***意見
            Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text

            '***Start
            Select Case o_wf_order
                Case "0"
                    '人員
                    pan_user.Visible = True : ddl_user.Visible = False : lb_userid.Text = uid
                    '狀態
                    ddl_status.Visible = True : lb_status.Visible = False
                    ddl_status.DataSource = Common.Get_WFStatus(0)
                    ddl_status.DataValueField = "wfstatus_id" : ddl_status.DataTextField = "wfstatus_name" : ddl_status.DataBind()
                    '意見
                    txt_comment.Visible = True : lb_comment.Visible = False
                Case "1"
                    Dim dt_WorkFlow As DataTable = Common.Get_Wf_User1_isms(apprvid, o_wf_order, "")
                    ddl_user.Visible = True : pan_user.Visible = False
                    ddl_user.Items.Clear()
                    ddl_user.DataValueField = "wf_userid" : ddl_user.DataTextField = "user_name"
                    ddl_user.DataSource = dt_WorkFlow : ddl_user.DataBind()
            End Select


            If pan_user.Visible = True And lb_userid.Text <> "" Then
                lb_username.Text = ISMS.Common.notNull(Common.Get_User_Name(lb_userid.Text))
                If lb_username.Text = "" Then lb_username.Text = Common.Get_venderemp_Name(lb_userid.Text) '表示非員工，可能為廠商
            End If
        End If
    End Sub

    '送出審核
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            If String.IsNullOrEmpty(applyDate.Text) Or String.IsNullOrEmpty(Ddl_sysName.SelectedValue) Or String.IsNullOrEmpty(applyOrg.Text) Or String.IsNullOrEmpty(applyName.Text) _
                Or String.IsNullOrEmpty(sysFunction.Text) Or String.IsNullOrEmpty(Radio_applyReason.SelectedValue) Or String.IsNullOrEmpty(applyAction.Text) _
                Or String.IsNullOrEmpty(applyPath.Text) Or String.IsNullOrEmpty(expectedTime.Text) Then
                Dim msg As String = ""
                If String.IsNullOrEmpty(applyDate.Text) Then
                    msg += "申請日期"
                End If
                If String.IsNullOrEmpty(Ddl_sysName.SelectedValue) Then
                    If msg = "" Then
                        msg = "系統名稱"
                    Else
                        msg += ", 系統名稱"
                    End If
                End If
                If String.IsNullOrEmpty(sysFunction.Text) Then
                    If msg = "" Then
                        msg = "功能名稱"
                    Else
                        msg += ", 功能名稱"
                    End If
                End If
                If String.IsNullOrEmpty(applyOrg.Text) Then
                    If msg = "" Then
                        msg = "申請單位"
                    Else
                        msg += ", 申請單位"
                    End If
                End If
                If String.IsNullOrEmpty(applyName.Text) Then
                    If msg = "" Then
                        msg = "申請人員"
                    Else
                        msg += ", 申請人員"
                    End If
                End If
                If String.IsNullOrEmpty(Radio_applyReason.SelectedValue) Then
                    If msg = "" Then
                        msg = "變更原因"
                    Else
                        msg += ", 變更原因"
                    End If
                End If
                If String.IsNullOrEmpty(applyAction.Text) Then
                    If msg = "" Then
                        msg = "申請變更事項"
                    Else
                        msg += ", 申請變更事項"
                    End If
                End If
                If String.IsNullOrEmpty(applyPath.Text) Then
                    If msg = "" Then
                        msg = "程式路徑"
                    Else
                        msg += ", 程式路徑"
                    End If
                End If
                If String.IsNullOrEmpty(expectedTime.Text) Then
                    If msg = "" Then
                        msg = "預定完成時間"
                    Else
                        msg += ", 預定完成時間"
                    End If
                End If
                Common.showMsg(Me.Page, "請確實填寫" + msg)
                Return
            End If

            If GridView.Rows.Count < 1 Then
                Common.showMsg(Me.Page, "請先至(系統管理->審核流程管理)設定關卡流程")
                Return
            End If

            Dim Now As String = Common.YearYYYY(applyDate.Text) '日期
            Dim applyDate_Chn As String = Common.Year(Now).Replace("/", "") '民國YYYMMDD
            Dim sql = "SELECT * FROM SYSPG WHERE applyDate_Chn like '" + applyDate_Chn.Substring(0, 3) + "%' ORDER BY applyDate_Chn_SN DESC"
            Dim data As DataTable = Common.Con(sql)
            Dim applyDate_Chn_SN = 1
            If Not (data.Rows.Count = 0) Then
                applyDate_Chn_SN = Integer.Parse(Right(data.Rows(0)("recordId"), 3)) + 1 'SN
            End If

            '處理資訊系統軟體代號
            Dim sql2 = "select * from SysCode where syscd_Id = '" + Ddl_sysName.SelectedValue + "'"
            Dim data2 As DataTable = Common.Con(sql2)
            Dim syscd_Code = data2.Rows(0)("syscd_Code")

            Dim recordid As String = applyDate_Chn.Substring(0, 3) + syscd_Code + applyDate_Chn_SN.ToString.PadLeft(3, "0") '表單編號

            Dim expected_time As String = "null"
            '處理預定完成時間
            If Not String.IsNullOrEmpty(expectedTime.Text) Then
                expected_time = "'" + Common.YearYYYY(expectedTime.Text) + " " + Ddl_Hour.SelectedValue + ":" + Ddl_Min.SelectedValue + "'"
            End If


            '處理上傳檔案
            If CType(Me.FindControl("File_applyFile"), FileUpload).HasFile Then
                '有檔案
                Dim oriFileName = CType(Me.FindControl("File_applyFile"), FileUpload)
                Dim UpFileName As String = Common.UpFile(File_Path, recordid, CType(Me.FindControl("File_applyFile"), FileUpload))
                '塞入主表資料
                SqlTxt = "  insert into SYSPG (recordId,applyDate,applyDate_Chn,applyDate_Chn_SN,empUid,empName,syscd_Id,syscd_Code,syscd_Name," _
                + "sysFunction,applyOrg,applyName,applyReason,applyAction,applyPath,expectedTime,applyFile_ori_name,applyFile,using,create_time,op_time) " _
                + "values('" + recordid + "','" + Now + "','" + applyDate_Chn + "','" + applyDate_Chn_SN.ToString() + "','" + uid + "','" + uname + "','" + Ddl_sysName.SelectedValue + "'," _
                + "'" + syscd_Code + "','" + Ddl_sysName.SelectedItem.Text + "','" + sysFunction.Text + "','" + applyOrg.Text + "','" + applyName.Text + "'," _
                + "'" + Radio_applyReason.SelectedValue + "','" + applyAction.Text + "','" + applyPath.Text + "'," + expected_time + ",'" + oriFileName.FileName + "'," _
                + "'" + UpFileName + "','Y',getdate(),getdate())"

            Else
                '沒有檔案
                '塞入主表資料
                SqlTxt = "  insert into SYSPG (recordId,applyDate,applyDate_Chn,applyDate_Chn_SN,empUid,empName,syscd_Id,syscd_Code,syscd_Name," _
                + "sysFunction,applyOrg,applyName,applyReason,applyAction,applyPath,expectedTime,using,create_time,op_time) " _
                + "values('" + recordid + "','" + Now + "','" + applyDate_Chn + "','" + applyDate_Chn_SN.ToString() + "','" + uid + "','" + uname + "','" + Ddl_sysName.SelectedValue + "'," _
                + "'" + syscd_Code + "','" + Ddl_sysName.SelectedItem.Text + "','" + sysFunction.Text + "','" + applyOrg.Text + "','" + applyName.Text + "'," _
                + "'" + Radio_applyReason.SelectedValue + "','" + applyAction.Text + "','" + applyPath.Text + "'," + expected_time + "," _
                + "'Y',getdate(),getdate())"

            End If


            '申請人
            SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype ,dp) values "
            SqlTxt += " ('" + apprvid + "','" + recordid + "','" + CType(GridView.Rows(0).FindControl("wf_order"), Label).Text + "','" + uid + "',"
            SqlTxt += "'" + CType(GridView.Rows(0).FindControl("ddl_status"), DropDownList).SelectedValue + "','" + CType(GridView.Rows(0).FindControl("txb_comment"), TextBox).Text + "',getdate() ,'0', 'vender','" + dp + "') "

            '下一位審核人
            If GridView.Rows.Count >= 2 Then
                SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype ,dp) values "
                SqlTxt += " ('" + apprvid + "','" + recordid + "','" + CType(GridView.Rows(1).FindControl("wf_order"), Label).Text + "','" + CType(GridView.Rows(1).FindControl("ddl_user"), DropDownList).SelectedValue + "',"
                SqlTxt += " '1', '',getdate() ,'1', 'land','" + dp + "') "
            End If
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Session("success") = "Y"
            Response.Redirect("SYSPG.aspx", True)
            Common.showMsg(Me.Page, "儲存成功！")
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub
End Class

