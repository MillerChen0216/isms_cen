<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_doclist.aspx.vb" Inherits="ISMS_doclist" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>資訊安全管理系統文件點閱一覽表</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <asp:HiddenField ID="Hidden_DP" runat="server" />
        <br />
        <img src="Images/doc_1.gif" /><br />

        <br />
        <table id="Doc_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 850px"
            visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    <asp:Label ID="LB_DocType" runat="server"></asp:Label>
                    | ISMS文件點閱列表&nbsp;
                    <asp:DropDownList ID="ddl_dp" runat="server" AutoPostBack="True">
                    </asp:DropDownList>&nbsp;<asp:DropDownList ID="ddl_doctype" runat="server" AutoPostBack="True">
                        <asp:ListItem Selected="True" Value="0">全部</asp:ListItem>
                        <asp:ListItem Value="1">一階政策文件</asp:ListItem>
                        <asp:ListItem Value="2">二階規範文件</asp:ListItem>
                        <asp:ListItem Value="3">三階作業要點</asp:ListItem>
                        <asp:ListItem Value="4">四階表單或紀錄</asp:ListItem>
                    </asp:DropDownList>            
                    </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="850px" AllowSorting="True" DataSourceID="Doc1_DS">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/news_icon.gif" />
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                            </asp:TemplateField>                            
                            <asp:BoundField DataField="doc_type" HeaderText="文件類別">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_num" HeaderText="文件編號" SortExpression="doc_num">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_name" HeaderText="文件名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_version" HeaderText="版次">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="issue_date" HeaderText="發行日期" DataFormatString="{0:d}">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>                                   
                     
                            <asp:BoundField DataField="doc_capsule" HeaderText="文件簡述">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_id" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="下載文件" ShowHeader="False">
                                <ItemTemplate>
                                    <asp:Button ID="Button1" runat="server" CausesValidation="false" CommandName="" OnClick="Button1_Click"
                                        Text="下載" />
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
        <asp:SqlDataSource ID="Doc1_DS" runat="server"></asp:SqlDataSource>
        <br />
    </form>
</body>
</html>
