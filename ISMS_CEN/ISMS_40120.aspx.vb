﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports System.Collections.Generic

Partial Class ISMS_40120
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim SqlDa As SqlDataAdapter
    Dim SqlDs As DataSet
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim i As Integer

    Protected Sub Approval_SqlDS()
        Dim s1 As String
        Approval_DS.ConnectionString = Common.ConnDBS.ConnectionString
        Approval_DS.SelectCommand = "SELECT * FROM Process_Record where dp='" + Hidden_DP.Value + "'  order by apprv_id"
        s1 = Hidden_DP.Value
        GridView1.DataSourceID = Approval_DS.ID
    End Sub



    Protected Sub WF_SqlDS()
        WF_DS.ConnectionString = Common.ConnDBS.ConnectionString
        'WF_DS.SelectCommand = "SELECT workflow.apprv_id, workflow.wf_userid, workflow.wf_order, workflow.wf_inside_order, workflowName.chk_name as chk_name FROM workflow INNER JOIN workflowName ON workflow.apprv_id = workflowName.apprv_id AND workflow.wf_order = workflowName.wf_order where  workflow.apprv_id ='" + hidden_apprvid.Value + "' "

        WF_DS.SelectCommand = "SELECT * FROM workflowname where apprv_id ='" + hidden_apprvid.Value + "'   and dp='" + Hidden_DP.Value + "' order by wf_order ,wf_inside_order"

        Common.showMsg(Me.Page, WF_DS.SelectCommand)

        GridView2.DataSourceID = WF_DS.ID

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP

        Approval_SqlDS()

    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
        Dim ds As DataSet
        ds = GridView1.DataSource

        Select Case e.CommandName

            Case "Set_WF"
                WF_Lable.Text = SelectedRow.Cells(1).Text
                hidden_apprvid.Value = SelectedRow.Cells(0).Text

                WF_SqlDS()
                'Response.Write(hidden_apprvid.Value)
                Approval_TB.Visible = False
                WF_TB.Visible = True
                ddl_Wf()

        End Select

    End Sub


    Protected Sub ddl_Wf()

        chk_distin.Checked = False

        ddl_dp.DataSource = Common.ISMS_dpChange("")
        ddl_dp.DataTextField = "dp_name"
        ddl_dp.DataValueField = "dp_id"
        ddl_dp.DataBind()
        ddl_dp.SelectedValue = Hidden_DP.Value
        ddl_dp.Enabled = False

        ddl_deptment.DataSource = Common.Get_dept(Hidden_DP.Value)
        ddl_deptment.DataTextField = "dept_name"
        ddl_deptment.DataValueField = "dept_id"
        ddl_deptment.DataBind()
        ddl_deptment.SelectedIndex = 0

        lb_Ins_User.DataSource = Common.Get_User_Y(Hidden_DP.Value, ddl_deptment.SelectedValue.ToString)
        lb_Ins_User.DataTextField = "user_name"
        lb_Ins_User.DataValueField = "user_id"
        lb_Ins_User.DataBind()

        ddl_FlowOrder.DataSource = Common.Get_WorkFlow_OrderId(hidden_apprvid.Value, Hidden_DP.Value)
        ddl_FlowOrder.DataTextField = "wf_order"
        ddl_FlowOrder.DataValueField = "wf_order"
        ddl_FlowOrder.DataBind()


    End Sub


    Protected Sub GridView2_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowCreated
        e.Row.Cells(4).Visible = False
        e.Row.Cells(5).Visible = False
    End Sub

    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound
        Dim Ds, Ds1, Ds2 As New DataSet
        Dim User_id, User_name As String

        If e.Row.RowIndex <> -1 Then

            If e.Row.RowIndex >= 1 Then
                Ds1 = Common.Get_WorkFlow_MaxOrderId(hidden_apprvid.Value, Hidden_DP.Value)
                Ds2 = Common.Get_WorkFlow_MaxInsideOrderId1(hidden_apprvid.Value, e.Row.Cells(0).Text, Hidden_DP.Value)

                If e.Row.Cells(5).Text = Ds2.Tables(0).Rows(0).Item("wf_inside_order") Then
                    CType(e.Row.FindControl("Del_Flow"), Button).Visible = True
                    CType(e.Row.FindControl("Edit_RoleBtn"), Button).Visible = True
                End If

                Ds = Common.Get_WorkFlow_UserOrder(hidden_apprvid.Value, e.Row.Cells(0).Text, e.Row.Cells(5).Text, Hidden_DP.Value)
                User_id = Ds.Tables(0).Rows(0).Item("wf_userid")
                User_name = Common.Get_User_Name(User_id)
                CType(e.Row.FindControl("label_userid"), Label).Text = CType(e.Row.FindControl("label_userid"), Label).Text + "&nbsp;" + User_name + "(" + User_id + ")  "
            Else
                e.Row.Cells(0).Text = "起始"
                e.Row.Cells(2).Text = "填表人自己"

            End If
        End If


    End Sub

    Protected Sub Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        Dim Ds, Ds1 As New DataSet

        Select Case Hidden_Mode.Value

            Case "Edit_RoleName"
                Dim Role_name As String
                Role_name = CType(GridView2.Rows(hidden_WFindex.Value).FindControl("role_txt"), TextBox).Text
                If CType(GridView2.Rows(hidden_WFindex.Value).FindControl("role_txt"), TextBox).Text <> "" Then


                    SqlTxt = "UPDATE workflowName SET  chk_name ='" + Role_name + "'  where apprv_id ='" + hidden_apprvid.Value + "' and wf_order ='" + GridView2.Rows(hidden_WFindex.Value).Cells(0).Text + "' and dp='" + Hidden_DP.Value + "'"

                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                    WF_SqlDS()
                    GridView2.DataBind()
                Else
                    Common.showMsg(Me.Page, "角色名稱不允許空白")
                End If

                'Case "Ins_User"

                '    Dim insideid As String

                '    If Hidden_insideid.Value = 0 Then
                '        insideid = Hidden_insideid.Value
                '    Else
                '        insideid = Hidden_insideid.Value + 1
                '    End If


                '    If insideid = "0" Then

                '        insideid = insideid + 1
                '        SqlTxt = "INSERT INTO workflow (apprv_id,wf_userid,wf_order,wf_inside_order) VALUES ('" + hidden_apprvid.Value + "','" + CType(GridView2.Rows(hidden_WFindex.Value).FindControl("ddl_user"), DropDownList).SelectedValue + "','" + oRow.Cells(0).Text + "','" + insideid + "')"
                '        SqlCmd = New SqlCommand(SqlTxt, Conn)
                '        Conn.Open()
                '        SqlCmd.ExecuteNonQuery()
                '        Conn.Close()
                '        WF_SqlDS()
                '        GridView2.DataBind()

                '    Else

                '        Ds = Common.Get_Wf_ApprvUser(hidden_apprvid.Value, oRow.Cells(0).Text)
                '        If Ds.Tables(0).Rows(0).Item("wf_userid") = CType(GridView2.Rows(hidden_WFindex.Value).FindControl("ddl_user"), DropDownList).SelectedValue Then
                '            Common.showMsg(Me.Page, "此人員此關卡已建立了!")
                '        Else


                '            SqlTxt = "INSERT INTO workflow (apprv_id,wf_userid,wf_order,wf_inside_order) VALUES ('" + hidden_apprvid.Value + "','" + CType(GridView2.Rows(hidden_WFindex.Value).FindControl("ddl_user"), DropDownList).SelectedValue + "','" + oRow.Cells(0).Text + "','" + insideid + "')"
                '            SqlCmd = New SqlCommand(SqlTxt, Conn)
                '            Conn.Open()
                '            SqlCmd.ExecuteNonQuery()
                '            Conn.Close()
                '            WF_SqlDS()
                '            GridView2.DataBind()
                '        End If


                '    End If

            Case "Del_Flow"

                SqlTxt = "Delete from workflow where apprv_id ='" + hidden_apprvid.Value + "' and wf_order ='" + oRow.Cells(0).Text + "' and wf_inside_order ='" + oRow.Cells(5).Text + "' and dp='" + Hidden_DP.Value + "'"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()

                SqlTxt = "Delete from workflowName where apprv_id ='" + hidden_apprvid.Value + "' and wf_order ='" + oRow.Cells(0).Text + "' and wf_inside_order ='" + oRow.Cells(5).Text + "' and dp='" + Hidden_DP.Value + "'"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()


                Ds = Common.Get_WorkFlow_MaxOrderId(hidden_apprvid.Value, Hidden_DP.Value)
                Ds1 = Common.Get_WorkFlow_MaxInsideOrderId1(hidden_apprvid.Value, oRow.Cells(0).Text, Hidden_DP.Value)
                Try
                    ' If Ds.Tables(0).Rows.Count >= 1 Then


                    For i = oRow.Cells(0).Text To Ds.Tables(0).Rows(0).Item("wf_order")

                        If i <> Ds.Tables(0).Rows(0).Item("wf_order") Then
                            SqlTxt = "UPDATE workflow SET  wf_order='" + i.ToString + "' where wf_inside_order ='" + oRow.Cells(5).Text + "' and wf_order='" + (i + 1).ToString + "' and  apprv_id ='" + hidden_apprvid.Value + "' and dp='" + Hidden_DP.Value + "'"
                            SqlCmd = New SqlCommand(SqlTxt, Conn)
                            Conn.Open()
                            SqlCmd.ExecuteNonQuery()
                            Conn.Close()

                            SqlTxt = "UPDATE workflowName SET  wf_order='" + i.ToString + "' where wf_inside_order ='" + oRow.Cells(5).Text + "' and wf_order='" + (i + 1).ToString + "' and  apprv_id ='" + hidden_apprvid.Value + "' and dp='" + Hidden_DP.Value + "'"
                            SqlCmd = New SqlCommand(SqlTxt, Conn)
                            Conn.Open()
                            SqlCmd.ExecuteNonQuery()
                            Conn.Close()

                        End If
                    Next
                    '   End If
                Catch ex As Exception

                End Try
                Try
                    ' If Ds1.Tables(0).Rows.Count >= 1 And Ds1.Tables(0).Rows(0).Item("wf_inside_order") > oRow.Cells(5).Text Then

                    For i = oRow.Cells(5).Text To Ds1.Tables(0).Rows(0).Item("wf_inside_order")

                        If i <> Ds1.Tables(0).Rows(0).Item("wf_inside_order") Then
                            SqlTxt = "UPDATE workflow SET  wf_inside_order='" + i.ToString + "' where wf_order='" + oRow.Cells(0).Text + "' and wf_inside_order='" + (i + 1).ToString + "'  and  apprv_id ='" + hidden_apprvid.Value + "' and dp='" + Hidden_DP.Value + "' "
                            SqlCmd = New SqlCommand(SqlTxt, Conn)
                            Conn.Open()
                            SqlCmd.ExecuteNonQuery()
                            Conn.Close()

                            SqlTxt = "UPDATE workflowName SET  wf_inside_order='" + i.ToString + "' where wf_order='" + oRow.Cells(0).Text + "' and wf_inside_order='" + (i + 1).ToString + "' and  apprv_id ='" + hidden_apprvid.Value + "' and dp='" + Hidden_DP.Value + "' "
                            SqlCmd = New SqlCommand(SqlTxt, Conn)
                            Conn.Open()
                            SqlCmd.ExecuteNonQuery()
                            Conn.Close()

                        End If

                    Next
                    ' End If
                Catch ex As Exception

                End Try


                WF_SqlDS()
                GridView2.DataBind()


        End Select
    End Sub

    Protected Sub Show_Insert_Process_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert_Process.Click
        Approval_TB.Visible = False
        WF_TB.Visible = False
        Ins_TB_Process.Visible = True

        ddl_Item.DataSource = Common.Get_Form_Process("", Hidden_DP.Value)
        ddl_Item.DataTextField = "form_name"
        ddl_Item.DataValueField = "approvalid"
        ddl_Item.DataBind()
        ddl_Item.Items.Insert(0, New ListItem("請選擇", "0"))
        ddl_Item.SelectedIndex = 0

        Process_Name_Ins.Text = ""
        Remarks_Ins.Text = ""
        ddl_Item.SelectedIndex = 0

    End Sub
    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        If Process_Name_Ins.Text = "" Then
            Common.showMsg(Me.Page, "流程名稱不允許空白")
        Else
            Dim max_apprv_id As String = CType(CType(Common.Get_MaxApprvId(), Integer) + 1, String)
            'SqlTxt = " INSERT INTO approval (dp,apprv_id,apprv_name,apprv_desc) VALUES ('" + Me.Get_DP + "','" + Common.Get_MaxApprvId() + "','" + Process_Name_Ins.Text + "','" + Remarks_Ins.Text + "') "

            SqlTxt += " INSERT INTO Process_Record (apprv_id,form_name,form_desc,approvalId,dp) VALUES ('" + max_apprv_id + "','" + Process_Name_Ins.Text + "','" + Remarks_Ins.Text + "','" + ddl_Item.SelectedValue + "','" + Me.Get_DP + "') "

            SqlTxt += " INSERT INTO workflow (dp,apprv_id,wf_userid,wf_order ,wf_inside_order) VALUES ('" + Me.Get_DP + "','" + max_apprv_id + "','" + User.Identity.Name.ToString + "','0','1') "

            SqlTxt += " INSERT INTO workflowName (dp,apprv_id,wf_order,chk_name,wf_inside_order) VALUES ('" + Me.Get_DP + "','" + max_apprv_id + "','0','申請人自己','1') "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

            GridView1.DataBind()
            Approval_TB.Visible = True
            WF_TB.Visible = False
            Ins_TB_Process.Visible = False
        End If
    End Sub
    Protected Sub Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        WF_TB.Visible = True
        Approval_TB.Visible = False

        WF_SqlDS()
        GridView2.DataBind()

    End Sub


    Protected Sub Edit_RoleBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        hidden_WFindex.Value = Index
        oRow.Cells(3).Visible = False
        oRow.Cells(4).Visible = True
        CType(oRow.FindControl("Label1"), Label).Visible = False
        CType(oRow.FindControl("role_txt"), TextBox).Visible = True
        CType(oRow.FindControl("role_txt"), TextBox).Text = CType(oRow.FindControl("Label1"), Label).Text
        Hidden_Mode.Value = "Edit_RoleName"

    End Sub

    Protected Sub Ins_UserBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim Ds As New DataSet
        'oRow = CType(sender, Button).NamingContainer
        'Index = oRow.RowIndex
        'hidden_WFindex.Value = Index
        'oRow.Cells(3).Visible = False
        'oRow.Cells(4).Visible = True

        'CType(oRow.FindControl("ddl_user"), DropDownList).Visible = True
        'CType(oRow.FindControl("label_userid"), Label).Visible = False
        'CType(oRow.FindControl("ddl_user"), DropDownList).DataSource = Common.Get_User()
        'CType(oRow.FindControl("ddl_user"), DropDownList).DataValueField = "user_id"
        'CType(oRow.FindControl("ddl_user"), DropDownList).DataTextField = "user_name"
        'CType(oRow.FindControl("ddl_user"), DropDownList).DataBind()

        'Ds = Common.Get_WorkFlow_MaxInsideOrderId(hidden_apprvid.Value, oRow.Cells(0).Text)

        'If IsDBNull(Ds.Tables(0).Rows(0).Item("max_order")) = True Then
        '    Hidden_insideid.Value = 0
        'Else
        '    Hidden_insideid.Value = Ds.Tables(0).Rows(0).Item("max_order")
        'End If
        'Hidden_Mode.Value = "Ins_User"


    End Sub

    Protected Sub Del_UserBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        hidden_WFindex.Value = Index
        oRow.Cells(3).Visible = False
        oRow.Cells(4).Visible = True
        Hidden_Mode.Value = "Del_Flow"



        'CType(oRow.FindControl("ddl_user"), DropDownList).Visible = True
        'CType(oRow.FindControl("label_userid"), Label).Visible = False
        'CType(oRow.FindControl("ddl_user"), DropDownList).DataSource = Common.Get_Wf_User(hidden_apprvid.Value, oRow.Cells(0).Text, "")
        'CType(oRow.FindControl("ddl_user"), DropDownList).DataValueField = "wf_userid"
        'CType(oRow.FindControl("ddl_user"), DropDownList).DataTextField = "user_name"
        'CType(oRow.FindControl("ddl_user"), DropDownList).DataBind()



        'Response.Write(Me.GridView2.Rows(hidden_WFindex.Value).Cells(0).Text)
        'Response.Write("<BR>")
        'Response.Write(Me.GridView2.Rows(hidden_WFindex.Value).Cells(5).Text)


        'SqlTxt = ""

        'SqlCmd = New SqlCommand(SqlTxt, Conn)
        'Conn.Open()
        'SqlCmd.ExecuteNonQuery()
        'Conn.Close()

    End Sub



    Protected Sub InsWf_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles InsWf_Btn.Click
        WF_SqlDS()
        GridView2.DataBind()
        role_name.Text = ""
        ddl_Wf()
        Ins_TB.Visible = True
    End Sub

    Protected Sub Cancel_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cancel_Btn.Click
        WF_SqlDS()
        GridView2.DataBind()

        Ins_TB.Visible = False
    End Sub
    Protected Sub btn_SetForm_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex

        SqlTxt = " UPDATE Process_Record SET approvalId = '" + CType(oRow.Cells(3).FindControl("ddl_FormName"), DropDownList).SelectedValue + "' WHERE dp='" + Hidden_DP.Value + "' and  apprv_id='" + oRow.Cells(0).Text + "' "
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()

        GridView1.DataBind()
        Approval_TB.Visible = True
        WF_TB.Visible = False
        Ins_TB_Process.Visible = False
    End Sub

    Protected Sub Ins_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ins_Btn.Click

        Dim Ds As New DataSet
        Dim wf_inside As String
        Dim list As New List(Of String)

        For Each item As ListItem In lb_Ins_User.Items
            If item.Selected Then
                list.Add(item.Value)
            End If
        Next
        If list.Count < 1 Then
            Common.showMsg(Me.Page, "請選擇關卡人員!")
            Return
        End If

        For i As Integer = 0 To list.Count - 1
            Ds = Common.Get_WorkFlow_MaxInsideOrderId(hidden_apprvid.Value, ddl_FlowOrder.SelectedValue, Hidden_DP.Value)

            wf_inside = Common.FixNull1(Ds.Tables(0).Rows(0).Item("max_order")) + 1
            ' SqlTxt = "INSERT INTO workflow (dp,apprv_id,wf_userid,wf_order,wf_inside_order) VALUES ('" + Hidden_DP.Value + "','" + hidden_apprvid.Value + "','" + lb_Ins_User.SelectedValue + "', '" + ddl_FlowOrder.SelectedValue + "','" + wf_inside + "')"
            SqlTxt = "INSERT INTO workflow (dp,apprv_id,wf_userid,wf_order,wf_inside_order) VALUES ('" + Hidden_DP.Value + "','" + hidden_apprvid.Value + "','" + list(i).ToString + "', '" + ddl_FlowOrder.SelectedValue + "','" + wf_inside + "')"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

            SqlTxt = "INSERT INTO workflowName (dp,apprv_id,wf_order,chk_name,wf_inside_order) VALUES ('" + Hidden_DP.Value + "','" + hidden_apprvid.Value + "','" + ddl_FlowOrder.SelectedValue + "','" + role_name.Text + "','" + wf_inside + "')"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
        Next



        WF_SqlDS()
        GridView2.DataBind()
        ddl_Wf()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Approval_TB.Visible = True
        WF_TB.Visible = False
        Ins_TB.Visible = False
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowIndex <> -1 Then
            Dim Ds1 As New DataSet
            Dim check As String
            Dim lab_FormName As Label = CType(e.Row.FindControl("lab_FormName"), Label)
            Dim btn_SetForm As Button = CType(e.Row.FindControl("btn_SetForm"), Button)
            Dim ddl_FormName As DropDownList = CType(e.Row.FindControl("ddl_FormName"), DropDownList)

            check = Common.Get_wfFormCount(e.Row.Cells(0).Text.ToString, Hidden_DP.Value)
            If check = "Y" Then
                e.Row.Cells(4).Enabled = False
            Else
                e.Row.Cells(4).Enabled = True
            End If
            Ds1 = Common.Get_Form_Process(e.Row.Cells(0).Text, Hidden_DP.Value)
            If Ds1.Tables(0).Rows.Count > 0 Then '已選定申請表
                lab_FormName.Visible = True : btn_SetForm.Visible = False : ddl_FormName.Visible = False
                lab_FormName.Text = Ds1.Tables(0).Rows(0).Item("form_name")
            Else
                lab_FormName.Visible = False : btn_SetForm.Visible = True : ddl_FormName.Visible = True
                ddl_FormName.DataSource = Common.Get_Form_Process("", Hidden_DP.Value)
                ddl_FormName.DataTextField = "form_name"
                ddl_FormName.DataValueField = "approvalid"
                ddl_FormName.DataBind()
            End If
        End If
    End Sub
    Protected Sub ddl_dp_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ds As DataSet = Common.Get_dept(ddl_dp.SelectedValue)
        ddl_deptment.DataSource = ds
        ddl_deptment.DataTextField = "dept_name"
        ddl_deptment.DataValueField = "dept_id"
        ddl_deptment.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            ddl_deptment.SelectedIndex = 0
            lb_Ins_User.DataSource = Common.Get_User_Y(ddl_dp.SelectedValue, ddl_deptment.SelectedValue)
            lb_Ins_User.DataTextField = "user_name"
            lb_Ins_User.DataValueField = "user_id"
            lb_Ins_User.DataBind()
        Else
            lb_Ins_User.Items.Clear()
        End If

    End Sub
    Protected Sub ddl_deptment_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)

        lb_Ins_User.DataSource = Common.Get_User_Y(ddl_dp.SelectedValue, ddl_deptment.SelectedValue)
        lb_Ins_User.DataTextField = "user_name"
        lb_Ins_User.DataValueField = "user_id"
        lb_Ins_User.DataBind()
    End Sub
    Protected Sub chk_distin_checkedchange(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_distin.CheckedChanged

        If chk_distin.Checked Then

            ddl_dp.Enabled = True
            lb_Ins_User.DataSource = Common.Get_User_Y(Hidden_DP.Value, ddl_deptment.SelectedValue.ToString)
            lb_Ins_User.DataTextField = "user_name"
            lb_Ins_User.DataValueField = "user_id"
            lb_Ins_User.DataBind()
        Else

            ddl_dp.Enabled = False
            lb_Ins_User.DataSource = Common.Get_User_Y(Hidden_DP.Value, ddl_deptment.SelectedValue.ToString)
            lb_Ins_User.DataTextField = "user_name"
            lb_Ins_User.DataValueField = "user_id"
            lb_Ins_User.DataBind()

        End If
    End Sub
End Class

