﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class ISMS_doclist
    Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim Ds As New DataSet


    Protected Sub Doc1_SqlDS()
        Doc1_DS.ConnectionString = Common.ConnDBS.ConnectionString
        If ddl_doctype.SelectedIndex <> 0 Then
            Doc1_DS.SelectCommand = "SELECT * FROM docResoure where doc_type in ('1','2','3','4') and latest='YES'  and dp='" + Hidden_DP.Value + "' and doc_type='" + ddl_doctype.SelectedValue + "' order by doc_type"
        Else
            Doc1_DS.SelectCommand = "SELECT * FROM docResoure where doc_type in ('1','2','3','4') and latest='YES'  and dp='" + Hidden_DP.Value + "' order by doc_type"

        End If
        'Response.Write("SELECT * FROM docResoure where doc_type in ('1','2','3','4') and latest='YES'  and dp='" + Hidden_DP.Value + "' order by doc_type")
        GridView1.DataSourceID = Doc1_DS.ID
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim Ds As New DataSet
        If Not Page.IsPostBack Then

            ddl_dp.DataSource = Common.ISMS_dpChange("")
            ddl_dp.DataValueField = "dp_id"
            ddl_dp.DataTextField = "dp_name"
            ddl_dp.DataBind()
        End If

        Hidden_DP.Value = ddl_dp.SelectedValue
        Doc1_SqlDS()

        Ds = Common.ISMS_dpChange(Hidden_DP.Value)
        LB_DocType.Text = Ds.Tables(0).Rows(0).Item("dp_name")
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(7).Visible = False
        e.Row.Cells(6).Visible = False

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex

        Try
            'Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", GridView1.Rows(Index).Cells(6).Text, System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "FILEDOWN", GridView1.Rows(Index).Cells(3).Text)
            Ds = Common.Get_Doc_info(GridView1.Rows(Index).Cells(7).Text)
            Response.ContentType = "application/octet-stream"
            Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5")
            Response.AddHeader("content-disposition", "attachment;filename=" + Common.Decode((Ds.Tables(0).Rows(0).Item("doc_filename"))) + "")
            Response.WriteFile(Common.Sys_Config("File_Path") + "/" + ("" + (Ds.Tables(0).Rows(0).Item("doc_filename")) + ""))
            Response.End()
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try


    End Sub


    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowIndex <> -1 Then
            Dim ds As New DataSet
            ds = Common.Get_issueDoc_info(e.Row.Cells(7).Text)
            Dim wDate As Date
            wDate = ds.Tables(0).Rows(0).Item("issue_date")

            If wDate.AddDays(Common.Sys_Config("News_Day")) >= Now.Date Then
                CType(e.Row.FindControl("Image1"), Image).Visible = True
            Else
                CType(e.Row.FindControl("Image1"), Image).Visible = False
            End If

            Select Case e.Row.Cells(1).Text
                Case "1"
                    e.Row.Cells(1).Text = "一階政策文件"
                Case "2"
                    e.Row.Cells(1).Text = "二階規範文件"
                Case "3"
                    e.Row.Cells(1).Text = "三階作業要點"
                Case "4"
                    e.Row.Cells(1).Text = "四階表單或紀錄"
            End Select


        End If

    End Sub


    Protected Sub ddl_dp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_dp.SelectedIndexChanged
        If ddl_dp.SelectedValue = "TW" Then
            ddl_doctype.Items(1).Enabled = True
            ddl_doctype.Items(2).Enabled = True
        Else
            ddl_doctype.Items(1).Enabled = False
            ddl_doctype.Items(2).Enabled = False

        End If
    End Sub
End Class
