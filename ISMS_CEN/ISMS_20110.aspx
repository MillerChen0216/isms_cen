﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_20110.aspx.vb" Inherits="ISMS_20110" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <br />
        <table id="Show_TB" class="tb_1" runat="server" style="width: 500px" cellpadding="0" cellspacing="0" visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="5">
                    <img src="Images/exe.gif" />
                    資產大分類管理
                    <asp:Button ID="Show_Insert" runat="server" CssClass="button-small" Text="新增資料" /></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="tb_1"
                        Width="500px" HorizontalAlign="Left">
                        <Columns>
                            <asp:BoundField DataField="asset_id" HeaderText="大分類代碼">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="asset_name" HeaderText="大分類名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="asset_Ename" HeaderText="資產類別">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="asset_desc" HeaderText="資產說明">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:ButtonField ButtonType="Button" CommandName="Edit_Btn" Text="編輯">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:ButtonField>
                            <asp:ButtonField ButtonType="Button" CommandName="Del_Btn" Text="刪除">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:ButtonField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <table id="Edit_TB" runat="server" cellpadding="3" cellspacing="0" class="tb_1" style="width: 500px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="8">
                    <img src="Images/exe.GIF" />
                    資產大分類管理
                    <asp:Button ID="Update_Btn" runat="server" CssClass="button-small" Text="確定編輯" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    大分類代碼<font color="red">*</font></td>
                <td class="tb_title_w_1">
                    大分類名稱<font color="red">*</font></td>
                <td class="tb_title_w_1">
                    資產類別</td>
                <td class="tb_title_w_1">
                    資產說明</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="asset_id_Edit" runat="server" Width="30px" MaxLength="2" ReadOnly="True"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="asset_id_Edit"
                        ErrorMessage="請輸入數字1到99" MaximumValue="99" MinimumValue="1" Display="None"></asp:RangeValidator></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="asset_name_Edit" runat="server" Width="80px" MaxLength="50"></asp:TextBox></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="asset_ename_Edit" runat="server" MaxLength="50" Width="100px"></asp:TextBox></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="asset_desc_Edit" runat="server" MaxLength="50" Width="100px"></asp:TextBox></td>
            </tr>
        </table>
        <table id="Ins_TB" runat="server" cellpadding="3" cellspacing="0" class="tb_1" style="width: 500px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="8">
                    <img src="Images/exe.GIF" />
                    資產大分類管理
                    <asp:Button ID="Insert_Btn" runat="server" CssClass="button-small" Text="確定新增" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    大分類代碼*</td>
                <td class="tb_title_w_1">
                    大分類名稱*</td>
                <td class="tb_title_w_1">
                    資產類別</td>
                <td class="tb_title_w_1">
                    資產說明</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="asset_id_Ins" runat="server" Width="30px" MaxLength="2"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="asset_id_Ins"
                        ErrorMessage="請輸入數字1到99" MaximumValue="99" MinimumValue="1" Display="None"></asp:RangeValidator></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="asset_name_Ins" runat="server" Width="80px" MaxLength="50"></asp:TextBox></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="asset_Ename_Ins" runat="server" MaxLength="50" Width="100px"></asp:TextBox></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="asset_desc_Ins" runat="server" MaxLength="50" Width="100px"></asp:TextBox></td>
            </tr>
        </table>
        <asp:HiddenField ID="Hidden_Assetid" runat="server" />
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RangeValidator1"> 
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RangeValidator2">
        </cc1:ValidatorCalloutExtender>
        &nbsp;&nbsp;<asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </form>
</body>
</html>
