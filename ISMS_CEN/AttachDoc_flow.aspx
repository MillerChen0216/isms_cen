﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AttachDoc_flow.aspx.vb"
    Inherits="AttachDoc_flow" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>地政局系統使用者帳號申請表</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="True">
    </asp:ScriptManager>
    <table border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2" width="850px">
        <tr>
            <td>
                <img src="Images/exe.gif" />附件線上簽核 | 待審核
                <asp:Label ID="lb_msg" runat="server" ForeColor="red" EnableViewState="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table id="tb_result" runat="server" border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2"
                    style="width: 100%" visible="false">
                    <tr>
                        <td class="tb_title_w_1" colspan="1">
                            申請日期
                        </td>
                        <td class="tb_title_w_2" align="center" colspan="1">
                        <asp:Label ID="applyDate" runat="server"></asp:Label>
                        </td>
                        <td class="tb_title_w_1" colspan="1">
                            表單編號
                        </td>
                        <td class="tb_title_w_2" colspan="1">
                            <asp:Label ID="recordId" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="otr">
                        <td class="tb_title_w_1" width="150px" colspan="1">
                            申請人
                        </td>
                        <td class="tb_title_w_2" colspan="4">
                            <asp:Label ID="empName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="ntr">
                        <td class="tb_title_w_1" width="150px" colspan="5">
                            表單項目*
                        </td>
                    </tr>
                    <tr>
                        <td class="tb_title_w_2" width="150px" colspan="5">
                            <asp:RadioButtonList ID="atdt_Name" runat="server">
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tb_title_w_1" colspan="1">
                            附件檔案*
                        </td>
                        <td class="tb_w_1" align="left" colspan="4">
                        <asp:FileUpload ID="File_images1" runat="server" Width="70%" />
                        <asp:Label ID="images1_link" runat="server" Visible="false" Text="0"></asp:Label>
                        <asp:LinkButton ID="images1" runat="server"  Text="檔案下載"  ></asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="tb_title_w_1" width="150px" colspan="5">
                            說明
                        </td>
                    </tr>
                    <tr>
                        <td class="tb_title_w_2" colspan="5">
                            <asp:TextBox ID="comment" TextMode="MultiLine" runat="server" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2" style="width: 100%">
                                <tr>
                                    <td colspan="4" align="center">
                                        <%--<uc1:FlowControl ID="FlowControl1" runat="server"  />--%>
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="lb_recordid" runat="server" Visible="false" Text="0"></asp:Label>
                                                <asp:Label ID="lb_count" runat="server" Visible="false" Text="999"></asp:Label>
                                                <asp:Label ID="lb_id" runat="server" Visible="false" Text="99"></asp:Label>
                                                <asp:Label ID="lb_name" runat="server" Visible="false" Text="99"></asp:Label>
                                                <asp:GridView ID="GridView" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                                    CssClass="tb_1" Width="100%">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="關卡" ItemStyle-CssClass="tb_w_1">
                                                            <ItemTemplate>
                                                                <asp:Label ID="wf_order" runat="server" Text='<%# eval("wf_order") %>'></asp:Label>
                                                                <asp:Label ID="lb_id2" runat="server" Visible="false" Text='<%# Eval("id")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="審核身份" ItemStyle-CssClass="tb_w_1">
                                                            <ItemTemplate>
                                                                <asp:Label ID="chk_name" runat="server" Text='<%# eval("chk_name") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="審核人員" ItemStyle-CssClass="tb_w_1">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddl_user" runat="server" Visible="false">
                                                                </asp:DropDownList>
                                                                <asp:Panel ID="pan_user" runat="server" Visible="false">
                                                                    <asp:Label ID="lb_username" runat="server"></asp:Label>
                                                                    (<asp:Label ID="label_userid" runat="server"></asp:Label>)
                                                                </asp:Panel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="狀態" ItemStyle-CssClass="tb_w_1">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddl_status" runat="server" Visible="False">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lb_status" runat="server" Visible="False"></asp:Label>
                                                                <asp:HiddenField ID="hid_status" runat="server" Value='<%#Eval("wf_status_id") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="意見" ItemStyle-CssClass="tb_w_1">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txb_comment" runat="server" Text='<%# Eval("wf_comment")%>' Visible="False"
                                                                    Width="150px"></asp:TextBox>
                                                                <asp:Label ID="lb_comment" runat="server" Text='<%# eval("wf_comment") %>' Visible="False"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="tb_title_w_1" />
                                                </asp:GridView>
                                                <%--  <asp:SqlDataSource ID="WF_DS" runat="server"></asp:SqlDataSource>--%>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:Button ID="Insert_Btn" runat="server" Text="送出審核" OnClick="Insert_Btn_click"
                                            Visible="false" />
                                        <asp:Button ID="but_esc" runat="server" Text="回列表" OnClick="but_esc_click" Visible="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="tr_gridview" runat="server">
            <td>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
                <asp:GridView ID="GridView1" runat="server" CssClass="tb_1" AutoGenerateColumns="false"
                    Width="850px">
                    <Columns>
                        <asp:TemplateField HeaderText="申請日期">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lb_applydate" Text='<%# eval("applyDate") %>'></asp:Label>
                                <asp:Label runat="server" ID="lb_rowid" Text='<%# eval("recordid") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="表單編號">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="recordId" Text='<%# eval("recordId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="簽核過程">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lb_flow"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField ButtonType="Button" CommandName="audit_btn" Text="審核" HeaderText="審核">
                            <ItemStyle CssClass="tb_w_1" />
                        </asp:ButtonField>
                    </Columns>
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <EmptyDataTemplate>
                        …尚無資料…</EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
