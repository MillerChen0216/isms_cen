﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class ISMS_11170
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim Ds As New DataSet


    Protected Sub Doc1_SqlDS()
        Doc1_DS.ConnectionString = Common.ConnDBS.ConnectionString
        Doc1_DS.SelectCommand = "SELECT * FROM docResoure where doc_type='10' and latest='YES'  and dp='" + Hidden_DP.Value + "'"
        GridView1.DataSourceID = Doc1_DS.ID
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        Doc1_SqlDS()
        LB_DocType.Text = Common.ISMS_DocNameChange("10")
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(5).Visible = False
        e.Row.Cells(3).Visible = False
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex

        Try
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", GridView1.Rows(Index).Cells(5).Text, System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "FILEDOWN", GridView1.Rows(Index).Cells(2).Text)
            Ds = Common.Get_Doc_info(GridView1.Rows(Index).Cells(5).Text)
            Response.ContentType = "application/octet-stream"
            Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5")
            Response.AddHeader("content-disposition", "attachment;filename=" + Common.Decode((Ds.Tables(0).Rows(0).Item("doc_filename"))) + "")
            Response.WriteFile(Common.Sys_Config("File_Path") + "/" + ("" + (Ds.Tables(0).Rows(0).Item("doc_filename")) + ""))
            Response.End()
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try


    End Sub


    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowIndex <> -1 Then
            Dim ds As New DataSet
            ds = Common.Get_issueDoc_info(e.Row.Cells(5).Text)
            Dim wDate As Date
            wDate = ds.Tables(0).Rows(0).Item("issue_date")

            If wDate.AddDays(Common.Sys_Config("News_Day")) >= Now.Date Then
                CType(e.Row.FindControl("Image1"), Image).Visible = True
            Else
                CType(e.Row.FindControl("Image1"), Image).Visible = False
            End If
        End If
    End Sub
End Class
