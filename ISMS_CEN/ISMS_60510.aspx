﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_60510.aspx.vb" Inherits="ISMS_60510" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>局端審查地所系統帳號申請表</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <br />
        <table id="Show_auth_sys" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 500px" visible="true">
            <tr>
                <td class="tb_w_1">
                    <img src="Images/exe.gif" />
                    授權範圍
                     <asp:TextBox ID="sys_Name_Qry" runat="server" Width="200px"></asp:TextBox>
                     <asp:Button ID="Query_Btn" runat="server" CssClass="button-small" Text="查詢" />
                     <asp:Button ID="Show_Insert_auth_sys" runat="server" CssClass="button-small" Text="新增資料" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        CellPadding="3" CssClass="tb_1" Width="100%" AllowPaging="True">
                        <Columns>
                            <asp:TemplateField HeaderText="授權範圍">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lb_sys_name" Text='<%# eval("sys_name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="管理">
                                <ItemTemplate>
                                    <asp:Button ID="Button1" runat="server" Text="編輯" CommandName="Edit_Btn" CommandArgument='<%# Container.DataItemIndex%>'/>
                                    <%--<asp:Button ID="Button2" runat="server" Text="刪除" CommandName="Del_Btn" CommandArgument='<%# Container.DataItemIndex%>'/> --%>
                                    <asp:Button ID="Button3" runat="server" Text="檢視權限" CommandName="ViewIdt_Btn" CommandArgument='<%# Container.DataItemIndex%>'/>
                                    <asp:RadioButtonList ID="rdo_able" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" OnSelectedIndexChanged="rdo_ableSelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Value="Y">啟用</asp:ListItem>
                                        <asp:ListItem Value="N">停用</asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:HiddenField ID="hid_sys_id" runat="server" Value='<%# eval("sys_id") %>'/>
                                    <asp:HiddenField ID="hid_sys_rmk" runat="server" Value='<%# eval("sys_rmk") %>'/>
                                    <asp:HiddenField ID="hid_using" runat="server" Value='<%# Eval("using")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="tb_title_w_1" />
                        <RowStyle CssClass="tb_w_1" />
                    </asp:GridView>
                 </td>
            </tr>
            <tr>
               <td>
                    <table id="Edit_auth_sys_note" runat="server" cellpadding="3" cellspacing="0" class="tb_1"
                        style="width: 600px" >
                        <tr>
                            <td class="tb_title_w_2" colspan="6">
                                <img src="Images/exe.GIF" />
                                局端審查地所系統帳號申請表-填表說明
                                <asp:Button ID="EditNote_Btn" runat="server" CssClass="button-small" Text="確定編輯" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">
                                填表說明</td>
                            <td class="tb_w_1">
                                <asp:TextBox ID="note_Edit" runat="server" Width="98%" TextMode="MultiLine" Rows="10"></asp:TextBox></td>
                        </tr>
                    </table>
               </td>
            </tr>
        </table>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <table id="Edit_auth_sys" runat="server" cellpadding="3" cellspacing="0" class="tb_1"
            style="width: 500px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.GIF" />
                    授權範圍管理
                    <asp:Button ID="Update_Btn" runat="server" CssClass="button-small" Text="確定編輯" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    授權範圍</td>
                <td class="tb_w_1">
                    <asp:TextBox ID="sys_Name_Edit" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    備註</td>
                <td class="tb_w_1">
                    <asp:TextBox ID="sys_Rmk_Edit" runat="server" Width="300px"></asp:TextBox></td>
            </tr>
        </table>
        <asp:HiddenField ID="hidden_sysid" runat="server"  />
        <asp:HiddenField ID="hidden_sysname" runat="server"   />
        <table id="Ins_auth_sys" runat="server" cellpadding="3" cellspacing="0" class="tb_1"
            style="width: 500px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.GIF" />
                    授權範圍管理
                    <asp:Button ID="Insert_Btn" runat="server" CssClass="button-small" Text="確定新增" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    授權範圍</td>
                <td class="tb_w_1">
                    <asp:TextBox ID="sys_Name_Ins" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    備註</td>
                <td class="tb_w_1">
                    <asp:TextBox ID="sys_Rmk_Ins" runat="server" Width="300px"></asp:TextBox></td>
            </tr>
        </table>
        <table id="Show_auth_sys_idt" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 500px" visible="false">
            <tr>
                <td class="tb_title_w_1">
                    授權範圍</td>
                <td class="tb_w_1">
                    <asp:Label ID="sys_Name_show" runat="server" Width="200px"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_w_1" colspan="2">
                    <img src="Images/exe.gif" />
                    權限
                    <asp:Button ID="Show_Insert_auth_sys_idt" runat="server" CssClass="button-small"
                        Text="新增資料" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="GridView2" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        CellPadding="3" CssClass="tb_1" Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="權限">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lb_idt_name" Text='<%# eval("idt_name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="管理">
                                <ItemTemplate>
                                    <asp:Button ID="Button1" runat="server" Text="編輯" CommandName="Edit_Btn" CommandArgument='<%# Container.DataItemIndex%>'/>
                                    <asp:Button ID="Button2" runat="server" Text="刪除" CommandName="Del_Btn" CommandArgument='<%# Container.DataItemIndex%>'/>
                                    <asp:HiddenField ID="hid_idt_id" runat="server" Value='<%# eval("idt_id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="tb_title_w_1" />
                        <RowStyle CssClass="tb_w_1" />
                  </asp:GridView>
                </td>
            </tr>
        </table>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server"></asp:SqlDataSource>
        <table id="Edit_auth_sys_idt" runat="server" cellpadding="3" cellspacing="0" class="tb_1"
            style="width: 500px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="2">
                    <img src="Images/exe.GIF" />
                    權限管理
                    <asp:Button ID="UpdateIdt_Btn" runat="server" CssClass="button-small" Text="確定編輯" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    授權範圍</td>
                <td class="tb_w_1">
                    <asp:Label ID="sys_Name_view" runat="server" Width="200px"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" >
                    權限</td>
                <td class="tb_w_1" colspan="2">
                    <asp:TextBox ID="idt_Name_Edit" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
        </table>
        <asp:HiddenField ID="hidden_idtid" runat="server" Visible="False" />
        <table id="Ins_auth_sys_idt" runat="server" cellpadding="3" cellspacing="0" class="tb_1"
            style="width: 500px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="2">
                    <img src="Images/exe.GIF" />
                    權限管理
                    <asp:Button ID="InsertIdt_Btn" runat="server" CssClass="button-small" Text="確定新增" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    授權範圍</td>
                <td class="tb_w_1">
                    <asp:Label ID="sys_Name" runat="server" Width="200px"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    權限</td>
                <td class="tb_w_1" colspan="2">
                    <asp:TextBox ID="idt_Name_Ins" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
