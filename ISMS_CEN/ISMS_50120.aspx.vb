﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_50120
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand

    Protected Sub Show_Insert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert.Click
        Show_TB.Visible = False
        Ins_TB.Visible = True

    End Sub

    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        If Gp_Name_Ins.Text = "" Then
            Common.showMsg(Me.Page, "群組名稱不允許空白")
        Else

            SqlTxt = "INSERT INTO ctlGroup  (dp,[group_name],[group_desc]) VALUES ('" + Hidden_DP.Value + "','" + Gp_Name_Ins.Text.Trim + "' , '" + Gp_Dsc_Ins.Text.Trim + "'  ) "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", Gp_Name_Ins.Text)

            GridView1.DataBind()
            Show_TB.Visible = True
            Ins_TB.Visible = False
        End If
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)

        SelectedRow.Cells(4).Visible = True
        Hidden_Groupid.Value = SelectedRow.Cells(4).Text


        Select Case e.CommandName

            Case "Del_Btn"

                SqlTxt = "DELETE FROM ctlGroup where dp ='" + Hidden_DP.Value + "' and group_id ='" + Hidden_Groupid.Value + "' "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DEL", SelectedRow.Cells(0).Text)

                GridView1.DataBind()

            Case "Edit_Btn"

                Edit_TB.Visible = True
                Show_TB.Visible = False

                Gp_Name_Edit.Text = SelectedRow.Cells(0).Text.Trim
                Gp_Dsc_Edit.Text = SelectedRow.Cells(1).Text.Trim.Replace("&nbsp;", "")


        End Select

    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        'Response.Write(e.Row.Cells(2).Visible)
        e.Row.Cells(4).Visible = False

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim oDataRow As Data.DataRowView
        Dim sShowField As String
        Dim sMessage As String
        If e.Row.RowIndex <> -1 Then
            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = oDataRow.Item("group_name").ToString()
            sMessage = String.Format("您確定要刪除 [{0}] 嗎?", sShowField)
            CType(e.Row.Cells(3).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"
        End If
    End Sub

    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click
        If Gp_Name_Edit.Text = "" Then
            Common.showMsg(Me.Page, "群組名稱不允許空白")
        Else

            SqlTxt = "UPDATE ctlGroup  SET  group_name ='" + Gp_Name_Edit.Text + "' , group_desc ='" + Gp_Dsc_Edit.Text + "' where dp='" + Hidden_DP.Value + "' and group_id ='" + Hidden_Groupid.Value + "'"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", Gp_Name_Edit.Text)

            GridView1.DataBind()
            Show_TB.Visible = True
            Edit_TB.Visible = False
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP

        SqlDataSource()

    End Sub

    Protected Sub SqlDataSource()
        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = "SELECT * FROM [ctlGroup] where dp='" + Hidden_DP.Value + "'"
        GridView1.DataSourceID = SqlDataSource1.ID
    End Sub
End Class
