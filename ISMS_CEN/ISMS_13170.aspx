<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_13170.aspx.vb" Inherits="ISMS_13170" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-color: #ffffff">
    <form id="form1" runat="server">
    <div>
        &nbsp;<asp:HiddenField ID="Hidden_DP" runat="server" />
        <br />
        <table id="Doc_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 750px" visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    <asp:Label ID="LB_List" runat="server"></asp:Label>
                    | 列表&nbsp;
                    <asp:Button ID="Ins_DocBtn" runat="server" Text="新增資料" /></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="750px">
                        <Columns>
                            <asp:BoundField DataField="uname" HeaderText="姓名">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="class_date" HeaderText="上課日期">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="class_num" HeaderText="期別">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="class_name" HeaderText="課程名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="class_hour" HeaderText="時數">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        
                            
                            <asp:TemplateField HeaderText="管理" ShowHeader="False">
                                <ItemTemplate>
                                    &nbsp;<asp:Button ID="Rev_Doc" runat="server" CommandName="Rev_Doc" Text="修改" OnClick="Rev_Doc_Click"  />&nbsp;<asp:Button
                                        ID="Disable_Doc" runat="server" CommandName="Disable_Doc" Text="刪除" OnClick="Disable_Doc_Click" />
                                    <asp:Button ID="View_btn" runat="server" OnClick="View_btn_Click" Text="瀏覽" />
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="id" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <table id="info_tb" runat="server" cellpadding="1" cellspacing="1" class="tb_1" style="width: 600px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="2">
                    <img src="Images/exe.gif" />
                    <asp:Label ID="Label2" runat="server"></asp:Label>
                    | 詳細資料 &nbsp;<asp:Button ID="list_btn" runat="server" Text="回列表" /></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    姓名</td>
                <td class="tb_w_1">
                    <asp:Label ID="lb_uid" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    上課日期</td>
                <td class="tb_w_1">
                    <asp:Label ID="lb_class_date" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    期別</td>
                <td class="tb_w_1">
                    <asp:Label ID="lb_class_num" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    課程名稱</td>
                <td class="tb_w_1">
                    <asp:Label ID="lb_class_name" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    時數</td>
                <td class="tb_w_1">
                    <asp:Label ID="lb_class_hour" runat="server"></asp:Label></td>
            </tr>
            <tr style="color: #345b7b">
                <td class="tb_title_w_1" style="width: 135px">
                    附件下載</td>
                <td class="tb_w_1">
                    <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" Visible="False">[HyperLink1]</asp:HyperLink></td>
            </tr>
        </table>
    
    </div>
        <asp:SqlDataSource ID="Doc1_DS" runat="server"></asp:SqlDataSource>

      <table id="Ins_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 750px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="7" style="height: 27px">
                    <img src="Images/exe.gif" />
                    <asp:Label ID="LB_ADD" runat="server"></asp:Label>
                    | 新增資<span style="background-color: #b8e1f1"></span>料</td>
            </tr>
            <tr>

                <td class="tb_title_w_1">
                    姓名</td>
                <td class="tb_title_w_1">
                    期別</td>
                <td class="tb_title_w_1">
                    時數</td>
                <td class="tb_title_w_1">
                    課程名稱</td>
                <td class="tb_title_w_1">
                    上課日期</td>
            </tr>
            <tr>
                <td class="tb_w_1" >
                    <asp:DropDownList ID="ddl_user_ins" runat="server">
                    </asp:DropDownList>&nbsp;</td>
                <td class="tb_w_1">
                    <asp:TextBox ID="class_num_ins" runat="server" Width="40px"></asp:TextBox></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="class_hour_ins" runat="server" Width="40px"></asp:TextBox></td>

                <td class="tb_w_1" >
                    <asp:TextBox ID="class_name_ins" runat="server" TextMode="MultiLine" Width="280px" Rows="3"></asp:TextBox>
                    </td>
                <td class="tb_w_1" >
                    &nbsp;<asp:TextBox ID="class_date_ins" runat="server" Rows="3" TextMode="MultiLine" Width="200px"></asp:TextBox></td>
            </tr>
          <tr>
              <td class="tb_title_w_1" colspan="7" >
                  附件上傳</td>
          </tr>
          <tr>
              <td  colspan="7">
                  <asp:FileUpload ID="FileUpload1" runat="server" Width="400px" />
                  </td>
          </tr>
          <tr>
              <td align="left" class="tb_w_1" colspan="7">
                  <asp:Button ID="Ins_Btn" runat="server" Text="確定新增" />&nbsp;
              </td>
          </tr>
      </table>

        <table id="Edit_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1"  style="width: 750px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="7" style="height: 27px">
                    <img src="Images/exe.gif" />
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                    | 修改資<span style="background-color: #b8e1f1"></span>料</td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    姓名</td>
                <td class="tb_title_w_1">
                    期別</td>
                <td class="tb_title_w_1">
                    時數</td>
                <td class="tb_title_w_1">
                    課程內容</td>
                <td class="tb_title_w_1">
                    上課日期</td>
            </tr>
            <tr>
                <td class="tb_w_1" >
                    &nbsp;<asp:DropDownList ID="ddl_user_edit" runat="server">
                    </asp:DropDownList></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="class_num_edit" runat="server" Width="40px"></asp:TextBox></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="class_hour_edit" runat="server" Width="40px"></asp:TextBox></td>
                <td class="tb_w_1" >
                    <asp:TextBox ID="class_name_edit" runat="server" TextMode="MultiLine" Width="280px" Rows="3"></asp:TextBox>
                    </td>
                <td class="tb_w_1" >
                    &nbsp;<asp:TextBox ID="class_date_edit" runat="server" Rows="3" TextMode="MultiLine" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" colspan="7" >
                    附件上傳</td>
            </tr>
            <tr>
                <td  colspan="7">
                    <asp:FileUpload ID="FileUpload4" runat="server" Width="400px" />
                    <asp:HyperLink ID="HyperLink2" runat="server" Target="_blank" Visible="False">[HyperLink2]</asp:HyperLink></td>
            </tr>
            <tr>
                <td align="left" class="tb_w_1" colspan="7">
                    <asp:Button ID="EditBtn" runat="server" Text="確定修改" />&nbsp;
                </td>
            </tr>
        </table>
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True" EnableScriptLocalization="True">
        </asp:ScriptManager>

        <asp:HiddenField ID="Hidden_Docid" runat="server" Visible="False" />
        &nbsp; &nbsp;&nbsp;

    </form>
</body>
</html>
