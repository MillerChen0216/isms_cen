﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_31160
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer
    Dim oRow As GridViewRow
    Dim Index As Integer



    Protected Sub SqlDS()

        Flow_DS.ConnectionString = Common.ConnDBS.ConnectionString
        Flow_DS.SelectCommand = "SELECT * FROM riskEvaluateMaster  where form_status='RISK_ISSUE' AND dp='" + Hidden_DP.Value + "' order by issue_date desc"
        GridView1.DataSourceID = Flow_DS.ID
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        SqlDS()
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(4).Visible = False
    End Sub

    Protected Sub audit_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        RiskForm_DS.ConnectionString = Conn.ConnectionString
        RiskForm_DS.SelectCommand = "SELECT *  FROM riskEvaluateForm where PID=" + GridView1.Rows(Index).Cells(4).Text + ""
        GridView2.DataSourceID = RiskForm_DS.ID

        SetFields()
        tb1.Visible = True
        Main_TB.Visible = False
        lb_riskname.Text = GridView1.Rows(Index).Cells(1).Text
        Hidden_Formid.Value = GridView1.Rows(Index).Cells(4).Text
    End Sub
    Protected Sub GridView2_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView2.PreRender
        Dim i As Integer = 1
        Dim j As Integer = 0
        Dim k As Integer
        Dim AlternatingRowStyle_i As Integer = 1
        Dim mySingleRow As GridViewRow

        For Each mySingleRow In GridView2.Rows
            For j = 0 To 13
                GridView2.Rows(CInt(mySingleRow.RowIndex)).Cells(j).CssClass = "tb_w_1"
            Next
        Next

        For Each mySingleRow In GridView2.Rows

            If CInt(mySingleRow.RowIndex) = 0 Then

                For k = 0 To 8
                    mySingleRow.Cells(k).RowSpan = 1
                Next


            Else
                If mySingleRow.Cells(0).Text.Trim() = GridView2.Rows(CInt(mySingleRow.RowIndex) - i).Cells(0).Text.Trim() And mySingleRow.Cells(1).Text.Trim() = GridView2.Rows(CInt(mySingleRow.RowIndex) - i).Cells(1).Text.Trim() Then

                    For k = 0 To 8
                        GridView2.Rows(CInt(mySingleRow.RowIndex) - i).Cells(k).RowSpan += 1
                    Next

                    i = i + 1
                    For k = 0 To 8
                        mySingleRow.Cells(k).Visible = False
                    Next


                Else
                    For k = 0 To 8
                        GridView2.Rows(CInt(mySingleRow.RowIndex)).Cells(k).RowSpan = 1
                    Next
                    i = 1
                End If
            End If
        Next
    End Sub
    Protected Sub GridView2_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim tc As TableCellCollection = e.Row.Cells
            tc.Clear()

            tc.Add(New TableHeaderCell)
            tc(0).Text = "風險類別"
            tc.Add(New TableHeaderCell)
            tc(1).Text = "資產編號"
            tc.Add(New TableHeaderCell)
            tc(2).Text = "資產名稱"
            tc.Add(New TableHeaderCell)
            tc(3).Text = "使用者"
            tc.Add(New TableHeaderCell)
            tc(4).Text = "擁有者"
            tc.Add(New TableHeaderCell)
            tc(5).Text = "權責單位"
            tc.Add(New TableHeaderCell)
            tc(6).Text = "數量"
            tc.Add(New TableHeaderCell)
            tc(7).Text = "放置地點"
            tc.Add(New TableHeaderCell)
            tc(8).Text = "資產價值"
            tc.Add(New TableHeaderCell)
            'tc(9).Text = "資產安全等級"
            'tc.Add(New TableHeaderCell)
            tc(9).Text = "威脅"
            tc.Add(New TableHeaderCell)
            tc(10).Text = "弱點"
            tc.Add(New TableHeaderCell)
            tc(11).Text = "可能性"
            tc.Add(New TableHeaderCell)
            tc(12).Text = "衝擊性"
            tc.Add(New TableHeaderCell)
            'KM LAND
            tc(13).Text = "威脅弱點值"
            'KM TAX
            'tc(13).Text = "綜合風險值"  


            For i = 0 To 13
                tc(i).CssClass = "tb_title_w_1"
            Next


        End If
    End Sub

    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound
        If e.Row.RowIndex <> -1 Then
            Dim Ds, Ds1 As New DataSet
            e.Row.Cells(3).Text = Common.Get_User_Name(e.Row.Cells(3).Text)
            e.Row.Cells(4).Text = Common.Get_User_Name(e.Row.Cells(4).Text)
            e.Row.Cells(5).Text = Common.Get_dept_Name(e.Row.Cells(5).Text)
            Ds = Common.Get_AssetName(e.Row.Cells(0).Text)
            e.Row.Cells(0).Text = Ds.Tables(0).Rows(0).Item("asset_name")

        End If
    End Sub
    Protected Sub SetFields()


        Dim risk_type, dept_id, assitem_id, assitem_name, location, item_amount, user_id, holder As New BoundField
        Dim assitSecure_lv, assitScore, weakness, threat, riskp_score, impact_score, totalrisk_score, totalrisk_lv As New BoundField

        dept_id.DataField = "dept_id"
        assitem_id.DataField = "assitem_id"
        assitem_name.DataField = "assitem_name"
        user_id.DataField = "user_id"
        holder.DataField = "holder"
        item_amount.DataField = "item_amount"
        location.DataField = "location"
        assitScore.DataField = "assitScore"
        assitSecure_lv.DataField = "assitSecure_lv"
        weakness.DataField = "weakness"
        threat.DataField = "threat"
        riskp_score.DataField = "riskp_score"
        impact_score.DataField = "impact_score"
        totalrisk_score.DataField = "totalrisk_score"
        totalrisk_lv.DataField = "totalrisk_lv"
        risk_type.DataField = "risk_type"




        GridView2.Columns.Add(risk_type)
        GridView2.Columns.Add(assitem_id)
        GridView2.Columns.Add(assitem_name)
        GridView2.Columns.Add(user_id)
        GridView2.Columns.Add(holder)
        GridView2.Columns.Add(dept_id)
        GridView2.Columns.Add(item_amount)
        GridView2.Columns.Add(location)
        GridView2.Columns.Add(assitScore)
        'GridView1.Columns.Add(assitSecure_lv)
        GridView2.Columns.Add(threat)
        GridView2.Columns.Add(weakness)
        GridView2.Columns.Add(riskp_score)
        GridView2.Columns.Add(impact_score)
        GridView2.Columns.Add(totalrisk_score)
        'GridView1.Columns.Add(totalrisk_lv)

    End Sub

    Protected Sub Output_XLS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Output_XLS.Click
        Dim ads As New System.Web.UI.WebControls.SqlDataSource
        Dim ExcelSqlTxt As String
        Dim sw As New System.IO.StringWriter()
        Dim htw As New System.Web.UI.HtmlTextWriter(sw)
        Dim dg As New GridView()
        Dim i As Integer = 1
        Dim j As Integer = 0
        Dim k As Integer
        Dim AlternatingRowStyle_i As Integer = 1
        Dim mySingleRow As GridViewRow
        Dim Ds, Ds1 As New DataSet

        Response.Clear()
        '檔名支援中文
        Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5")
        '避免儲存中文內容有亂碼
        Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8")
        'excel檔名()
        Response.AddHeader("content-disposition", "attachment;filename=" + DateTime.Now.ToString("yyyyMMdd HH:mm") + "_ISMS" + lb_riskname.Text + "風險評鑑表.xls")
        Response.ContentType = "application/vnd.ms-excel"
        Response.Charset = ""

        ads.ConnectionString = Conn.ConnectionString
        'ExcelSqlTxt = "SELECT a.assitem_id as 資產編號,b.assitem_name as 資產名稱,b.user_id as 使用人,b.holder as 保管人,b.item_amount as 數量,b.location as 放置地點,b.Confidentiality as 機密性,b.Integrity as 完整性,b.Availability as 可用性,a.info_type as 資產價值,d.weakness as 資產安全等級,d.weakness as 脆弱點敘述,e.threat as 威脅述敘,a.riskp_score as 風險機率值,a.impact_score as 風險衝擊值,a.riskp_score as 總風險值,a.impact_score as 風險等級 FROM riskEvaluate as a,assetItem as b,infoSecurityType as c,weakness as d,threat as e WHERE b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id AND a.info_type = c.info_type AND b.asset_id=" + ddl_Asset.SelectedValue + " ORDER BY a.assitem_id,c.info_order"
        'KM TAX
        'ExcelSqlTxt = "SELECT [risk_type] as 風險類別,[assitem_id] as 資產編號,[assitem_name] as 資產名稱,[user_id] as 使用者,[holder] as 擁有人,[dept_id] as 權責單位,[item_amount] as 數量,[location] as 放置地點,[assitScore] as 資產價值,[threat] as 威脅,[weakness] as 弱點,[riskp_score] as 可能性,[impact_score] as 衝擊性,[totalrisk_score] as 綜合風險 FROM riskEvaluateForm where PID=" + Hidden_Formid.Value + ""
        'KM LAND
        ExcelSqlTxt = "SELECT [risk_type] as 風險類別,[assitem_id] as 資產編號,[assitem_name] as 資產名稱,[user_id] as 使用者,[holder] as 擁有人,[dept_id] as 權責單位,[item_amount] as 數量,[location] as 放置地點,[assitScore] as 資產價值,[threat] as 威脅,[weakness] as 弱點,[riskp_score] as 可能性,[impact_score] as 衝擊性,[totalrisk_score] as 威脅弱點值 FROM riskEvaluateForm where PID=" + Hidden_Formid.Value + ""
        ads.SelectCommand = ExcelSqlTxt
        dg.DataSource = ads.Select(DataSourceSelectArguments.Empty)
        dg.DataBind()

        For k = 0 To dg.Rows.Count - 1
            dg.Rows(k).Cells(3).Text = Common.Get_User_Name(dg.Rows(k).Cells(3).Text)
            dg.Rows(k).Cells(4).Text = Common.Get_User_Name(dg.Rows(k).Cells(4).Text)
            dg.Rows(k).Cells(5).Text = Common.Get_dept_Name(dg.Rows(k).Cells(5).Text)

            Dim dsx As New DataSet
            Ds = Common.Get_AssetName(dg.Rows(k).Cells(0).Text)
            dg.Rows(k).Cells(0).Text = Ds.Tables(0).Rows(0).Item("asset_name")
        Next




        '合併相同儲存格()
        For Each mySingleRow In dg.Rows

            If CInt(mySingleRow.RowIndex) = 0 Then

                For k = 0 To 8
                    mySingleRow.Cells(k).RowSpan = 1
                Next

            Else
                If mySingleRow.Cells(0).Text.Trim() = GridView2.Rows(CInt(mySingleRow.RowIndex) - i).Cells(0).Text.Trim() And mySingleRow.Cells(1).Text.Trim() = GridView2.Rows(CInt(mySingleRow.RowIndex) - i).Cells(1).Text.Trim() Then

                    For k = 0 To 8
                        dg.Rows(CInt(mySingleRow.RowIndex) - i).Cells(k).RowSpan += 1

                    Next

                    i = i + 1

                    For k = 0 To 8
                        mySingleRow.Cells(k).Visible = False

                    Next

                Else

                    For k = 0 To 8
                        dg.Rows(CInt(mySingleRow.RowIndex)).Cells(k).RowSpan = 1
                    Next

                    i = 1
                End If
            End If
        Next


        dg.RenderControl(htw)
        Response.Write(sw.ToString())
        Response.End()

    End Sub
End Class
