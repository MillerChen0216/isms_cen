﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_20140.aspx.vb" Inherits="ISMS_20140" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <br />
        <table id="Show_TB" class="tb_1" runat="server" style="width: 500px" cellpadding="0" cellspacing="0" visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="5">
                    <img src="Images/exe.gif" />
                    資產群組管理<asp:DropDownList ID="ddl_Asset" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:Button ID="Show_Insert" runat="server" CssClass="button-small" Text="新增資料" /></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="tb_1"
                        Width="500px" HorizontalAlign="Left">
                        <Columns>
                            <asp:BoundField DataField="assgroup_id" HeaderText="群組代碼">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="assgroup_name" HeaderText="群組分類名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="asset_id" HeaderText="大分類">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="asset_name" HeaderText="大分類名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="assgroup_desc" HeaderText="群組說明">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:ButtonField ButtonType="Button" CommandName="Edit_Btn" Text="編輯">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:ButtonField>
                            <asp:ButtonField ButtonType="Button" CommandName="Del_Btn" Text="刪除">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:ButtonField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <table id="Edit_TB" runat="server" cellpadding="3" cellspacing="0" class="tb_1" style="width: 500px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="8">
                    <img src="Images/exe.GIF" />
                    資產大分類管理
                    <asp:Button ID="Update_Btn" runat="server" CssClass="button-small" Text="確定編輯" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    群組代碼*</td>
                <td class="tb_title_w_1">
                    群組分類名稱*</td>
                <td class="tb_title_w_1">
                    大分類*</td>
                <td class="tb_title_w_1">
                    群組說明</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="assgroup_id_Edit" runat="server" Width="40px" MaxLength="4" ReadOnly="True"></asp:TextBox>&nbsp;
                </td>
                <td class="tb_w_1">
                    <asp:TextBox ID="assgroup_name_Edit" runat="server" Width="100px" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="assgroup_name_Edit"
                        Display="None" ErrorMessage="代碼請勿空白！"></asp:RequiredFieldValidator></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="asset_id_Edit" runat="server" MaxLength="50" Width="100px" ReadOnly="True"></asp:TextBox></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="assgroup_desc_Edit" runat="server" MaxLength="50" Width="120px"></asp:TextBox></td>
            </tr>
        </table>
        <table id="Ins_TB" runat="server" cellpadding="3" cellspacing="0" class="tb_1" style="width: 500px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="8">
                    <img src="Images/exe.GIF" />
                    資產大分類管理
                    <asp:Button ID="Insert_Btn" runat="server" CssClass="button-small" Text="確定新增" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    群組代碼*</td>
                <td class="tb_title_w_1">
                    群組分類名稱*</td>
                <td class="tb_title_w_1">
                    大分類*</td>
                <td class="tb_title_w_1">
                    群組說明</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="assgroup_id_Ins" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="assgroup_id_Ins"
                        Display="None" ErrorMessage="代碼請勿空白！"></asp:RequiredFieldValidator></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="assgroup_name_Ins" runat="server" Width="100px" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="assgroup_name_Ins"
                        Display="None" ErrorMessage="代碼請勿空白！"></asp:RequiredFieldValidator></td>
                <td class="tb_w_1">
                    &nbsp;<asp:DropDownList ID="ddl_Asset_ins" runat="server" AutoPostBack="True">
                    </asp:DropDownList></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="assgroup_desc_Ins" runat="server" MaxLength="50" Width="120px"></asp:TextBox></td>
            </tr>
        </table>
        <asp:HiddenField ID="Hidden_assgroup_id" runat="server" />
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1"> 
        </cc1:ValidatorCalloutExtender><cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3">
        </cc1:ValidatorCalloutExtender>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </form>
</body>
</html>
