﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class ISMS_10160
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim oRow As GridViewRow
    Dim Index As Integer

    Protected Sub Doc1_SqlDS()
        Doc1_DS.ConnectionString = Common.ConnDBS.ConnectionString
        Doc1_DS.SelectCommand = "SELECT * FROM docResoure where doc_type='10' and doc_status='ISSUE' and locked='0'   and dp='" + Hidden_DP.Value + "'"
        GridView1.DataSourceID = Doc1_DS.ID
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        Doc1_SqlDS()
        LB_List.Text = Common.ISMS_DocNameChange("10")

    End Sub


    Protected Sub Ins_DocBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ins_DocBtn.Click
        Ins_TB.Visible = True
        Doc_TB.Visible = False
        Author_date.Text = Now.Date
        Author.Text = Common.Get_User_Name(User.Identity.Name.ToString)
        LB_ADD.Text = Common.ISMS_DocNameChange("10")
    End Sub

    Protected Sub Ins_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ins_Btn.Click

        Dim DateTime_Str As String = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
        Dim apprv_id As String = "10"
        Dim doc_type As String = "10"
        Dim doc_status As String = "ISSUE"
        Dim latest As String = "YES"
        Dim path As String = HttpContext.Current.Request.MapPath(Common.Sys_Config("File_Path"))
        Dim Str_EnCode As String = Common.Encode(Hidden_DP.Value + "_" + Doc_num.Text + "_" + Doc_Ver.Text + "_" + Doc_Upload.FileName)
        'Dim Str_EnCode As String = Common.Encode(Doc_Upload.FileName)
        Dim max_docid As String = Common.Get_MaxDocid1()
        Doc_Upload.SaveAs(path + Str_EnCode)

        SqlTxt = "INSERT INTO docResoure (dp,doc_id,doc_name,doc_type,doc_capsule,dept_id,doc_num,doc_version,latest,doc_status,apprv_id,creator,create_date,doc_filename,issue_date)  VALUES('" + Hidden_DP.Value + "','" + max_docid + "','" + Me.Doc_name.Text + "','" + doc_type + "','" + Me.Doc_desc.Text + "','" + Session("deptid") + "','" + Me.Doc_num.Text + "','" + Me.Doc_Ver.Text + "','" + latest + "','" + doc_status + "','" + apprv_id + "','" + User.Identity.Name.ToString + "','" + DateTime_Str + "','" + Str_EnCode + "','" + DateTime_Str + "')"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()


        GridView1.DataBind()
        Doc_TB.Visible = True
        Ins_TB.Visible = False

        Dim Ds As New DataSet
        Ds = Common.Get_MaxDocid
        'Common.SendMail(User.Identity.Name, "Workflow", Ds.Tables(0).Rows(0).Item("doc_num"), Ds.Tables(0).Rows(0).Item("doc_name"))
        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", Ds.Tables(0).Rows(0).Item("doc_id"), System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADDNEWDOC", Ds.Tables(0).Rows(0).Item("doc_name"))



    End Sub

    Protected Sub Disable_Doc_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex

        SqlTxt = "UPDATE docResoure  SET Del_creator='" + User.Identity.Name.ToString + "',latest='NO',locked='1',doc_status = 'DELETEDOC'  WHERE doc_id='" + GridView1.Rows(Index).Cells(6).Text + "'"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()

        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", GridView1.Rows(Index).Cells(6).Text, System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DISABLEDOC", GridView1.Rows(Index).Cells(2).Text)
        'Common.SendMail(User.Identity.Name, "Workflow", GridView1.Rows(Index).Cells(1).Text, GridView1.Rows(Index).Cells(2).Text)



    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(6).Visible = False
        e.Row.Cells(3).Visible = False
    End Sub


    Protected Sub Rev_Doc_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex


        Ins_TB.Visible = False
        Doc_TB.Visible = False
        ChRev_TB.Visible = True
        Rev_AuthorDate.Text = Now.Date
        Rec_AuthorTxt.Text = Common.Get_User_Name(User.Identity.Name.ToString)

        Dim ds As New DataSet
        ds = Common.Get_Doc_info(Me.GridView1.Rows(Index).Cells(6).Text)
        Doc_num_rev.Text = ds.Tables(0).Rows(0).Item("doc_num").ToString
        doc_name_rev.Text = ds.Tables(0).Rows(0).Item("doc_name").ToString
        '金門版本1.0~
        Rev_Num.Text = Format(Val(ds.Tables(0).Rows(0).Item("doc_version").ToString + 10 / 100), "0.0")
        '標準版本01~
        'Rev_Num.Text = Format(Val(ds.Tables(0).Rows(0).Item("doc_version").ToString + 1), "00")
        LB_REV.Text = Common.ISMS_DocNameChange("1")

        Hidden_Docid.Value = GridView1.Rows(Index).Cells(6).Text



    End Sub




    'Protected Sub Rev_OKBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rev_OKBtn.Click


    '    SqlTxt = "UPDATE docResoure  SET locked='1' WHERE doc_id='" + Hidden_Docid.Value + "'"
    '    SqlCmd = New SqlCommand(SqlTxt, Conn)
    '    Conn.Open()
    '    SqlCmd.ExecuteNonQuery()
    '    Conn.Close()


    '    Dim DateTime_Str As String = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
    '    Dim apprv_id As String = "6"
    '    Dim doc_type As String = "6"
    '    Dim doc_status As String = "REV_NEW"
    '    Dim latest As String = "NO"
    '    Dim path As String = HttpContext.Current.Request.MapPath(Common.Sys_Config("File_Path"))
    '    Dim Str_EnCode As String = Common.Encode(Doc_num_rev.Text + "_" + Rev_Num.Text + "_" + Doc_Upload_rev.FileName)
    '    Doc_Upload_rev.SaveAs(path + Str_EnCode)


    '    SqlTxt = "INSERT INTO docResoure (doc_name,doc_type,doc_upddesc,dept_id,doc_num,doc_version,latest,doc_status,apprv_id,creator,create_date,doc_filename)  VALUES('" + Me.doc_name_rev.Text + "','" + doc_type + "','" + Me.Doc_desc_rev.Text + "','" + Session("deptid") + "','" + Me.Doc_num_rev.Text + "','" + Me.Rev_Num.Text + "','" + latest + "','" + doc_status + "','" + apprv_id + "','" + User.Identity.Name.ToString + "','" + DateTime_Str + "','" + Str_EnCode + "')"
    '    SqlCmd = New SqlCommand(SqlTxt, Conn)
    '    Conn.Open()
    '    SqlCmd.ExecuteNonQuery()
    '    Conn.Close()



    '    GridView1.DataBind()
    '    Doc_TB.Visible = True
    '    ChRev_TB.Visible = False

    '    Dim Ds As New DataSet
    '    Ds = Common.Get_MaxDocid
    '    Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", Ds.Tables(0).Rows(0).Item("doc_id"), System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "UPDATEVERSION", Ds.Tables(0).Rows(0).Item("doc_name"))
    '    Common.SendMail(User.Identity.Name, "Workflow", Doc_num_rev.Text, doc_name_rev.Text)

    'End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim sMessage, sShowField As String
        Dim oDataRow As Data.DataRowView
        If e.Row.RowIndex <> -1 Then
            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = e.Row.Cells(1).Text
            'sMessage = String.Format("您確定要改版此份 [{0}] 文件嗎?", sShowField)
            'CType(e.Row.FindControl("Rev_Doc"), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"
            sMessage = String.Format("您確定要廢止此份 [{0}] 文件嗎?", sShowField)
            CType(e.Row.FindControl("Disable_Doc"), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"

            Dim ds As New DataSet
            ds = Common.Get_issueDoc_info(e.Row.Cells(6).Text)
            Dim wDate As Date
            wDate = ds.Tables(0).Rows(0).Item("issue_date")

            If wDate.AddDays(Common.Sys_Config("News_Day")) >= Now.Date Then
                CType(e.Row.FindControl("Image1"), Image).Visible = True
            Else
                CType(e.Row.FindControl("Image1"), Image).Visible = False
            End If


        End If
    End Sub
End Class
