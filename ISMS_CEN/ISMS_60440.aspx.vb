﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO
Partial Class ISMS_60440
    Inherits PageBase
    'Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim uid, uname, recordid As String
    'Dim apprvid As String = ConfigurationManager.AppSettings("wf14")
    Dim apprvid As String
    Dim approvalid As String
    Dim approval_name As String = "地所系統使用者帳號申請表"
    Dim dp As String = Get_DP()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblOffice.Text = System.Web.Configuration.WebConfigurationManager.AppSettings("OfficeName")
        uid = User.Identity.Name
        uname = Common.Get_User_Name(uid)
        Dim nowpage() As String = Request.CurrentExecutionFilePath.Split("/")
        approvalid = Me.Get_approvalid(nowpage(nowpage.Length - 1), dp)
        apprvid = Me.Get_apprvid(nowpage(nowpage.Length - 1), dp)
        SqlDataSource()

        If Not IsPostBack Then
            Dim sy As String = Now.AddDays(-30).Year - 1911
            Dim sm As String = Now.AddDays(-30).Month
            Dim sd As String = Now.AddDays(-30).Day
            Dim ey As String = Now.AddDays(1).Year - 1911
            Dim em As String = Now.AddDays(1).Month
            Dim ed As String = Now.AddDays(1).Day
            txb_sdate.Text = sy + "/" + sm.PadLeft(2, "0") + "/" + sd.PadLeft(2, "0")
            txb_edate.Text = ey + "/" + em.PadLeft(2, "0") + "/" + ed.PadLeft(2, "0")
            bindRepeater()
            bindNote()
            Show_ddl()
        End If

    End Sub
    Protected Sub Show_ddl()
        '單位
        ddl_deptment.DataSource = Common.Get_dept(dp)
        ddl_deptment.DataValueField = "dept_id"
        ddl_deptment.DataTextField = "dept_name"
        ddl_deptment.DataBind()

        '職稱
        ddl_degree.DataSource = Common.Get_degree(dp)
        ddl_degree.DataValueField = "degree_id"
        ddl_degree.DataTextField = "degree_name"
        ddl_degree.DataBind()

        ddlbind_year(ddl_apply_year) : ddlbind_year(ddl_enable_year) : ddlbind_year(ddl_s_year) : ddlbind_year(ddl_e_year)
        ddlbind_month(ddl_apply_month) : ddlbind_month(ddl_enable_month) : ddlbind_month(ddl_s_month) : ddlbind_month(ddl_e_month)
        ddlbind_day(ddl_apply_day) : ddlbind_day(ddl_enable_day) : ddlbind_day(ddl_s_day) : ddlbind_day(ddl_e_day)


    End Sub
    Protected Sub bindRepeater()

        Repeater1.DataSource = Common.Get_auth_sys(approvalid, dp)
        Repeater1.DataBind()

    End Sub
    Protected Sub bindNote()
        Dim Ds As New DataSet
        Ds = Common.Get_auth_sys_note(approvalid, dp)
        If Ds.Tables(0).Rows.Count > 0 Then
            note_Edit.Text = Ds.Tables(0).Rows(0).Item("note")
        End If
    End Sub
    Private Sub ddlbind_year(ByVal ddl As DropDownList)
        Dim startyear As Integer = Now.Year - 1911 + 3
        For i As Integer = 6 To 1 Step -1
            ddl.Items.Insert(0, New System.Web.UI.WebControls.ListItem(startyear, startyear))
            startyear = startyear - 1
        Next
        ddl.SelectedValue = Now.Year - 1911
    End Sub
    Private Sub ddlbind_month(ByVal ddl As DropDownList)
        For i As Integer = 12 To 1 Step -1
            Dim mon As String = i.ToString.PadLeft(2, "0")
            ddl.Items.Insert(0, New System.Web.UI.WebControls.ListItem(mon, mon))
        Next
        ddl.SelectedValue = Now.Month.ToString.PadLeft(2, "0")
    End Sub
    Private Sub ddlbind_day(ByVal ddl As DropDownList, Optional ByVal year As String = "", Optional ByVal month As String = "", Optional ByVal default_day As Boolean = True)
        ddl.Items.Clear()
        If month = "" Then month = Now.Month
        If year = "" Then year = Now.Year - 1911
        For i As Integer = Date.DaysInMonth(year, month) To 1 Step -1
            ddl.Items.Insert(0, New System.Web.UI.WebControls.ListItem(i.ToString.PadLeft(2, "0"), i.ToString.PadLeft(2, "0")))
        Next
        If default_day Then ddl.SelectedValue = Now.Day.ToString.PadLeft(2, "0")
    End Sub
    Protected Sub ddl_s_day_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlbind_day(ddl_s_day, ddl_s_year.SelectedValue, ddl_s_month.SelectedValue, False)
    End Sub
    Protected Sub ddl_open_day_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlbind_day(ddl_enable_day, ddl_enable_year.SelectedValue, ddl_enable_month.SelectedValue, False)
    End Sub
    Protected Sub ddl_apply_day_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlbind_day(ddl_apply_day, ddl_apply_year.SelectedValue, ddl_apply_month.SelectedValue, False)
    End Sub
    Protected Sub ddl_e_day_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlbind_day(ddl_e_day, ddl_e_year.SelectedValue, ddl_e_month.SelectedValue, False)
    End Sub

    Protected Sub SqlDataSource()

        Dim sql As String = "SELECT * FROM office_apply where apply_user_id='" + uid + "' and dp = '" + dp + "'"
        '排除已撤銷之申請單
        sql += " and using='Y' "
        '排除停用帳號
        sql += " and user_id in (select user_id from ismsUser where using='Y') "
        If txb_sdate.Text <> "" Then
            sql += " and apply_date >= '" + txb_sdate.Text.Replace("/", "") + "'"
        End If
        If txb_edate.Text <> "" Then
            sql += " and apply_date <= '" + txb_edate.Text.Replace("/", "") + "'"
        End If

        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = sql

        GridView1.DataSourceID = SqlDataSource1.ID

    End Sub


    Protected Sub Repeater1_OnItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
            'Reference the Repeater Item.
            Dim item As RepeaterItem = e.Item
            'Reference the Controls.
            Dim idtRepeater As Repeater = (TryCast(item.FindControl("ChildRepeater"), Repeater))
            Dim sysid As HiddenField = (TryCast(item.FindControl("hidden_sysid"), HiddenField))
            idtRepeater.DataSource = Common.Get_auth_sys_idt(sysid.Value)
            idtRepeater.DataBind()
        End If
    End Sub


    Protected Sub but_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_search.Click
        GridView1.DataBind()

    End Sub

    Protected Sub but_delete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lb_msg.Text = "刪除成功！"
    End Sub




    Protected Sub audit_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        tb_result.Visible = True
        tb_query.Visible = False
        GridView1.Visible = False
    End Sub

    Protected Sub but_esc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_esc.Click
        tb_result.Visible = False
        tb_query.Visible = True
        GridView1.Visible = True
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim office_apply_id As String = e.CommandArgument
        hidden_office_apply_id.Value = office_apply_id
        'Dim Index As Integer = CType(e.CommandArgument, Int32)
        'Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
        'Dim hidden_recordId As HiddenField = (TryCast(SelectedRow.FindControl("hidden_recordId"), HiddenField))
        Select Case e.CommandName

            Case "audit_btn"
                '109modify 修改bug
                Dim index As Integer = (CType((CType(e.CommandSource, Button)).NamingContainer, GridViewRow)).RowIndex
                Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
                Dim hidden_recordId As HiddenField = (TryCast(SelectedRow.FindControl("hidden_recordId"), HiddenField))


                bindRepeater()

                Dim Ds, Ds1 As New DataSet
                Ds = Common.Get_office_apply(office_apply_id)
                If Ds.Tables(0).Rows.Count > 0 Then
                    Dim dscheck As DataSet = Common.Qry_Process(Ds.Tables(0).Rows(0).Item("recordId"))

                    If dscheck.Tables(0).Rows.Count <= 0 Then
                        but_update.Visible = False
                    End If
                    ddl_deptment.SelectedValue = Ds.Tables(0).Rows(0).Item("dept_id")
                    For Each li As System.Web.UI.WebControls.ListItem In rdo_apply_Reason.Items
                        If li.Value = Ds.Tables(0).Rows(0).Item("apply_reason") Then
                            li.Selected = True
                        End If

                    Next
                    txb_user_name.Text = Ds.Tables(0).Rows(0).Item("user_name")
                    txb_ext.Text = Ds.Tables(0).Rows(0).Item("ext")
                    ddl_degree.SelectedValue = Ds.Tables(0).Rows(0).Item("degree_id")
                    txb_item.Text = Ds.Tables(0).Rows(0).Item("item")
                    lb_recordid.Text = Ds.Tables(0).Rows(0).Item("recordid").ToString
                    lb_id.Text = Ds.Tables(0).Rows(0).Item("user_id").ToString
                    lb_name.Text = Ds.Tables(0).Rows(0).Item("user_name").ToString
                    GdvShow()
                    Dim apply_date As String = Ds.Tables(0).Rows(0).Item("apply_date").ToString
                    If apply_date.Length = 7 Then
                        ddl_apply_year.SelectedValue = apply_date.Substring(0, 3)
                        ddl_apply_month.SelectedValue = apply_date.Substring(3, 2)
                        ddl_apply_day.SelectedValue = apply_date.Substring(5, 2)
                    End If
                    Dim enable_date As String = Ds.Tables(0).Rows(0).Item("enable_date").ToString
                    If enable_date.Length = 7 Then
                        ddl_enable_year.SelectedValue = enable_date.Substring(0, 3)
                        ddl_enable_month.SelectedValue = enable_date.Substring(3, 2)
                        ddl_enable_day.SelectedValue = enable_date.Substring(5, 2)
                    End If
                    Dim start_date As String = Ds.Tables(0).Rows(0).Item("start_date").ToString
                    If start_date.Length = 7 Then
                        ddl_s_year.SelectedValue = start_date.Substring(0, 3)
                        ddl_s_month.SelectedValue = start_date.Substring(3, 2)
                        ddl_s_day.SelectedValue = start_date.Substring(5, 2)
                    End If
                    Dim end_date As String = Ds.Tables(0).Rows(0).Item("end_date").ToString
                    If end_date.Length = 7 Then
                        ddl_e_year.SelectedValue = end_date.Substring(0, 3)
                        ddl_e_month.SelectedValue = end_date.Substring(3, 2)
                        ddlbind_day(ddl_e_day, ddl_e_year.SelectedValue, ddl_e_month.SelectedValue, False) '1091119modify修正bug
                        ddl_e_day.SelectedValue = end_date.Substring(5, 2)
                    End If
                    Dim disable_date As String = Ds.Tables(0).Rows(0).Item("disable_date").ToString
                    If disable_date.Length = 7 Then
                        txb_disable_date.Text = disable_date.Substring(0, 3) + "/" + disable_date.Substring(3, 2) + "/" + disable_date.Substring(5, 2)
                    End If
                    txb_user_id.Text = Ds.Tables(0).Rows(0).Item("user_id")
                    txb_disable_reason.Text = Ds.Tables(0).Rows(0).Item("disable_reason")

                    For Each item As Control In Repeater1.Controls
                        Dim idt_repeater As Repeater = DirectCast(item.FindControl("ChildRepeater"), Repeater)
                        Dim hid_sys_id As HiddenField = DirectCast(item.FindControl("hidden_sysid"), HiddenField)
                        Dim hidden_idtname As HiddenField = DirectCast(item.FindControl("hidden_idtname"), HiddenField)
                        Dim sys_rmk As TextBox = DirectCast(item.FindControl("sys_rmk"), TextBox)
                        Dim list_idt As List(Of String) = New List(Of String)
                        Ds1 = Common.Get_office_apply_dtl(office_apply_id, hid_sys_id.Value)
                        If Ds1.Tables(0).Rows.Count > 0 Then
                            sys_rmk.Text = Ds1.Tables(0).Rows(0).Item("rmk").ToString
                            For Each item2 As Control In idt_repeater.Controls
                                Dim chk_idt_name As CheckBox = DirectCast(item2.FindControl("chk_idt_name"), CheckBox)
                                Dim find_rows As DataRow() = Ds1.Tables(0).Select("idt_name like '%" + chk_idt_name.Text + "%'")
                                If find_rows.Length > 0 Then
                                    chk_idt_name.Checked = True
                                    list_idt.Add("," + chk_idt_name.Text)
                                End If
                            Next

                        End If
                        hidden_idtname.Value = String.Join("", list_idt.ToArray())
                    Next

                End If


                tb_result.Visible = True
                tb_query.Visible = False
                GridView1.Visible = False

            Case "using_btn"
                Dim sql As New StringBuilder
                sql.Append(" update office_apply set using='N' where office_apply_id ='" & office_apply_id & "' ")
                Dim Ds As New DataSet
                Ds = Common.Get_office_apply(office_apply_id)
                If Ds.Tables(0).Rows.Count > 0 Then
                    Dim recordId As String = Ds.Tables(0).Rows(0).Item("recordid").ToString
                    sql.Append("  update form_record set status = -1 where recordId = '" + recordId + "' and approvalId = '" + approvalid + "' and status = 1")
                End If

                Dim conn As SqlConnection = Nothing : Dim cmd As New SqlCommand(sql.ToString)
                Try
                    conn = Common.ConnDBS : conn.Open() : cmd.Connection = conn
                    If cmd.ExecuteNonQuery() Then GridView1.DataBind() : lb_msg.Text = "撤銷成功！"
                Catch ex As Exception
                    lb_msg.Text = "撤銷失敗！" + ex.Message
                Finally
                    conn.Close() : conn.Close() : conn.Dispose() : cmd.Dispose()
                End Try

        End Select

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim applydate As Label = DirectCast(e.Row.FindControl("lb_applydate"), Label)
            Dim applyreason As Label = DirectCast(e.Row.FindControl("lb_applyreason"), Label)
            Dim hidden_recordId As HiddenField = DirectCast(e.Row.FindControl("hidden_recordId"), HiddenField)
            Dim Button2 As Button = DirectCast(e.Row.FindControl("Button2"), Button)
            Dim ds As DataSet = Common.Qry_Process(hidden_recordId.Value)

            If applydate.Text.Length = 7 Then
                applydate.Text = applydate.Text.Substring(0, 3) + "/" + applydate.Text.Substring(3, 2) + "/" + applydate.Text.Substring(5, 2)
            End If

            Select Case applyreason.Text
                Case "01"
                    applyreason.Text = "年度檢討"
                Case "02"
                    applyreason.Text = "職務異動"
                Case "03"
                    applyreason.Text = "新進人員"
                Case "04"
                    applyreason.Text = "離職"
                Case "05"
                    applyreason.Text = "其他"
            End Select

            If ds.Tables(0).Rows.Count <= 0 Then
                Button2.Visible = False
            End If
        End If

    End Sub

    Protected Sub but_print_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_print.Click
        Dim officeName As String = System.Web.Configuration.WebConfigurationManager.AppSettings("OfficeName")
        Dim doc As Document = New Document(PageSize.A4)
        Try
            HttpContext.Current.Response.ContentType = "application/pdf"
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.pdf")
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)

            Dim pdfDoc As New Document()
            Dim writer As PdfWriter = PdfWriter.GetInstance(pdfDoc, HttpContext.Current.Response.OutputStream)
            Dim baseFont As BaseFont = baseFont.CreateFont("C:\\Windows\\Fonts\\kaiu.ttf", baseFont.IDENTITY_H, baseFont.NOT_EMBEDDED)
            Dim font_14 As Font = New Font(baseFont, 14, Font.BOLD)
            Dim font_12 As Font = New Font(baseFont, 12)
            pdfDoc.Open()
            'WRITE PDF <<<<<<

            Dim table As PdfPTable = New PdfPTable(4)
            table.WidthPercentage = 97
            table.DefaultCell.Padding = 5

            Dim title As PdfPCell = New PdfPCell(New Paragraph("桃園市" + officeName + "地政事務所資訊系統使用者帳號申請表 ", font_14))
            title.Colspan = 4
            title.Padding = 5
            title.Border = 0
            title.HorizontalAlignment = Element.ALIGN_CENTER
            table.AddCell(title)
            table.AddCell(New Phrase("單位", font_12))
            table.AddCell(New Phrase(ddl_deptment.SelectedItem.Text, font_12))
            Dim apply_reason As String = ""
            For Each li As System.Web.UI.WebControls.ListItem In rdo_apply_Reason.Items
                If li.Selected = True Then
                    Select Case li.Value
                        Case "01"
                            apply_reason = "年度檢討"
                        Case "02"
                            apply_reason = "職務異動"
                        Case "03"
                            apply_reason = "新進人員"
                        Case "04"
                            apply_reason = "離職"
                        Case "05"
                            apply_reason = "其他"
                    End Select

                End If
            Next
            table.AddCell(New Phrase("申請原因", font_12))
            table.AddCell(New Phrase(apply_reason, font_12))
            table.AddCell(New Phrase("使用者姓名", font_12))
            table.AddCell(New Phrase(txb_user_name.Text, font_12))
            table.AddCell(New Phrase("分機", font_12))
            table.AddCell(New Phrase(txb_ext.Text, font_12))
            table.AddCell(New Phrase("職稱", font_12))
            Dim degreeCell As PdfPCell = New PdfPCell(New Paragraph(ddl_degree.SelectedItem.Text, font_12))
            degreeCell.Colspan = 3
            degreeCell.Padding = 5
            table.AddCell(degreeCell)
            table.AddCell(New Phrase("業務項目", font_12))
            Dim itemCell As PdfPCell = New PdfPCell(New Paragraph(txb_item.Text, font_12))
            itemCell.Colspan = 3
            table.AddCell(itemCell)
            table.AddCell(New Phrase("申請日期", font_12))
            Dim apply_date As String = ddl_apply_year.SelectedValue + "年" + ddl_apply_month.SelectedValue + "月" + ddl_apply_day.SelectedValue + "日"
            table.AddCell(New Phrase(apply_date, font_12))
            table.AddCell(New Phrase("啟用日期", font_12))
            Dim enable_date As String = ddl_enable_year.SelectedValue + "年" + ddl_enable_month.SelectedValue + "月" + ddl_enable_day.SelectedValue + "日"
            table.AddCell(New Phrase(enable_date, font_12))
            table.AddCell(New Phrase("使用期間", font_12))
            Dim s_date As String = ddl_s_year.SelectedValue + "年" + ddl_s_month.SelectedValue + "月" + ddl_s_day.SelectedValue + "日"
            Dim e_date As String = ddl_e_year.SelectedValue + "年" + ddl_e_month.SelectedValue + "月" + ddl_e_day.SelectedValue + "日"
            Dim startEndCell As PdfPCell = New PdfPCell(New Paragraph(s_date + "至" + e_date, font_12))
            startEndCell.Colspan = 3
            startEndCell.Padding = 5
            table.AddCell(startEndCell)
            table.AddCell(New Phrase("使用者代碼", font_12))
            table.AddCell(New Phrase(txb_user_id.Text, font_12))
            table.AddCell(New Phrase("使用者密碼", font_12))
            table.AddCell(New Phrase("(預設值)", font_12))
            table.AddCell(New Phrase("保密切結", font_12))
            Dim ConCell As PdfPCell = New PdfPCell(New Paragraph("本人_____________於業務權限範圍內，將確實依本系統使用作業規定，查詢或列印相關資料，並遵守個人資料保護法、智慧財產權、著作權等相關規定，如有不當使用、資料外洩或違法者，應依法負民、刑事責任及行政責任。", font_12))
            ConCell.Padding = 5
            ConCell.Colspan = 3
            table.AddCell(ConCell)
            table.AddCell(New Phrase("註銷(停用)日期", font_12))
            table.AddCell(New Phrase(txb_disable_date.Text, font_12))
            table.AddCell(New Phrase("註銷(停用)原因", font_12))
            table.AddCell(New Phrase(txb_disable_reason.Text, font_12))
            pdfDoc.Add(table)

            'Dim widths() As Single = {15.0F, 15.0F, 15.0F, 15.0F, 5.0F, 15.0F, 5.0F, 15.0F}
            'Dim table3 As PdfPTable = New PdfPTable(widths)
            Dim table3 As PdfPTable = New PdfPTable(3)
            table3.WidthPercentage = 97
            table3.DefaultCell.Padding = 5

            table3.AddCell(New Phrase("申請人", font_12))
            table3.AddCell(New Phrase("系統管理者", font_12))
            table3.AddCell(New Phrase("業務課課長", font_12))
            Dim cell As PdfPCell = New PdfPCell()
            cell.FixedHeight = 60
            table3.AddCell(cell)
            table3.AddCell(cell)
            table3.AddCell(cell)
            table3.AddCell(New Phrase("資訊課課長", font_12))
            table3.AddCell(New Phrase("秘書", font_12))
            table3.AddCell(New Phrase("主任", font_12))
            table3.AddCell(cell)
            table3.AddCell(cell)
            table3.AddCell(cell)
            'table3.AddCell(New Phrase("申請人", font_12))
            'table3.AddCell(New Phrase(" ", font_12))
            'table3.AddCell(New Phrase("業務課課長", font_12))
            'table3.AddCell(New Phrase(" ", font_12))
            'Dim scCell As PdfPCell = New PdfPCell(New Paragraph("秘書", font_12))
            'scCell.Padding = 5
            'scCell.Rowspan = 2
            'table3.AddCell(scCell)
            'Dim scECell As PdfPCell = New PdfPCell(New Paragraph(" ", font_12))
            'scECell.Padding = 5
            'scECell.Rowspan = 2
            'table3.AddCell(scECell)
            'Dim mCell As PdfPCell = New PdfPCell(New Paragraph("主任", font_12))
            'mCell.Padding = 5
            'mCell.Rowspan = 2
            'table3.AddCell(mCell)
            'Dim mECell As PdfPCell = New PdfPCell(New Paragraph(" ", font_12))
            'mECell.Padding = 5
            'mECell.Rowspan = 2
            'table3.AddCell(scECell)
            'table3.AddCell(New Phrase("系統管理者", font_12))
            'table3.AddCell(New Phrase(" ", font_12))
            'table3.AddCell(New Phrase("資訊課課長", font_12))
            'table3.AddCell(New Phrase(" ", font_12))
            pdfDoc.Add(table3)

            Dim table2 As PdfPTable = New PdfPTable(3)
            table2.WidthPercentage = 97
            table2.DefaultCell.Padding = 4
            Dim title2 As PdfPCell = New PdfPCell(New Paragraph("附表：系統授權範圍", font_14))
            title2.Colspan = 3
            title2.HorizontalAlignment = Element.ALIGN_CENTER
            title2.Padding = 5
            title2.Border = 0
            table2.AddCell(title2)
            Dim header1 As PdfPCell = New PdfPCell(New Phrase("授權範圍", font_12))
            header1.BackgroundColor = BaseColor.LIGHT_GRAY
            header1.Padding = 5
            header1.HorizontalAlignment = Element.ALIGN_CENTER
            Dim header2 As PdfPCell = New PdfPCell(New Phrase("權限", font_12))
            header2.BackgroundColor = BaseColor.LIGHT_GRAY
            header2.Padding = 5
            header2.HorizontalAlignment = Element.ALIGN_CENTER
            Dim header3 As PdfPCell = New PdfPCell(New Phrase("備註", font_12))
            header3.BackgroundColor = BaseColor.LIGHT_GRAY
            header3.Padding = 5
            header3.HorizontalAlignment = Element.ALIGN_CENTER
            table2.AddCell(header1)
            table2.AddCell(header2)
            table2.AddCell(header3)

            For Each item As Control In Repeater1.Controls
                Dim idt_repeater As Repeater = DirectCast(item.FindControl("ChildRepeater"), Repeater)
                Dim hidden_idtname As HiddenField = DirectCast(item.FindControl("hidden_idtname"), HiddenField)
                Dim sys_rmk As TextBox = DirectCast(item.FindControl("sys_rmk"), TextBox)
                Dim sys_name As Label = DirectCast(item.FindControl("lbl_sys_name"), Label)
                Dim list_idt As List(Of String) = New List(Of String)


                Dim selectGroup As PdfFormField = PdfFormField.CreateEmpty(writer)


                For Each item2 As Control In idt_repeater.Controls
                    Dim chk_idt_name As CheckBox = DirectCast(item2.FindControl("chk_idt_name"), CheckBox)
                    If hidden_idtname.Value.IndexOf(chk_idt_name.Text) <> -1 Then
                        list_idt.Add(chk_idt_name.Text)
                    End If
                Next
                table2.AddCell(New Phrase(sys_name.Text, font_12))
                table2.AddCell(New Phrase(String.Join(",", list_idt.ToArray()), font_12))
                table2.AddCell(New Phrase(sys_rmk.Text, font_12))
            Next
            pdfDoc.Add(table2)

            'END WRITE PDF >>>>>
            pdfDoc.Close()

            HttpContext.Current.Response.Write(pdfDoc)
            HttpContext.Current.Response.End()


        Catch ex As Exception

        Finally
            doc.Close()
        End Try

    End Sub

    Protected Sub but_update_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_update.Click
        Dim apply_date As String = ddl_apply_year.SelectedValue + ddl_apply_month.SelectedValue + ddl_apply_day.SelectedValue
        Dim enable_date As String = ddl_enable_year.SelectedValue + ddl_enable_month.SelectedValue + ddl_enable_day.SelectedValue
        Dim start_date As String = ddl_s_year.SelectedValue + ddl_s_month.SelectedValue + ddl_s_day.SelectedValue
        Dim end_date As String = ddl_e_year.SelectedValue + ddl_e_month.SelectedValue + ddl_e_day.SelectedValue
        Dim disable_date As String = txb_disable_date.Text.Replace("/", "")
        Dim apply_reason As String = ""
        For Each li As System.Web.UI.WebControls.ListItem In rdo_apply_Reason.Items
            If li.Selected = True Then
                apply_reason = li.Value
            End If
        Next

        SqlTxt = "delete office_apply_dtl where office_apply_id = '" + hidden_office_apply_id.Value + "' "
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()

        For Each item As Control In Repeater1.Controls
            Dim idt_repeater As Repeater = DirectCast(item.FindControl("ChildRepeater"), Repeater)
            Dim hid_sys_id As HiddenField = DirectCast(item.FindControl("hidden_sysid"), HiddenField)
            Dim sys_rmk As TextBox = DirectCast(item.FindControl("sys_rmk"), TextBox)
            Dim list_idt As List(Of String) = New List(Of String)
            For Each item2 As Control In idt_repeater.Controls
                Dim chk_idt_name As CheckBox = DirectCast(item2.FindControl("chk_idt_name"), CheckBox)
                If chk_idt_name.Checked Then
                    list_idt.Add(chk_idt_name.Text)
                End If
            Next
            If list_idt.Count > 0 Then
                SqlTxt = "INSERT INTO office_apply_dtl ([office_apply_id],[sys_id],[idt_name],[rmk]) VALUES ('" + hidden_office_apply_id.Value + "','" + hid_sys_id.Value + "','" + String.Join(",", list_idt.ToArray()) + "','" + sys_rmk.Text + "') "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            End If

        Next

        SqlTxt = "UPDATE office_apply set dept_id ='" + ddl_deptment.SelectedValue + "', apply_reason ='" + apply_reason + "' , apply_reason_other='" + IIf(apply_reason = "03", txb_apply_Reason_other.Text, "") + "'"
        SqlTxt += ", user_name ='" + txb_user_name.Text + "', ext='" + txb_ext.Text + "' , degree_id='" + ddl_degree.SelectedValue + "',item='" + txb_item.Text + "' "
        SqlTxt += " , apply_date='" + apply_date + "' , enable_date='" + enable_date + "' , start_date='" + start_date + "', end_date='" + end_date + "' ,user_id='" + txb_user_id.Text + "'"
        SqlTxt += " , disable_date='" + disable_date + "', disable_reason='" + txb_disable_reason.Text + "' "
        SqlTxt += " WHERE office_apply_id='" + hidden_office_apply_id.Value + "'"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        Common.showMsg(Me.Page, "修改成功")

    End Sub

    Private Sub GdvShow()
        Dim dt As DataTable = Common.Qry_workflowName_isms_Req(approvalid, lb_recordid.Text, apprvid, dp)
        GridView.DataSource = dt
        GridView.DataBind()

    End Sub

    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料
            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            Dim hid_status As HiddenField = CType(e.Row.FindControl("hid_status"), HiddenField)
            '***意見
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text
            '***審核人員***
            'Dim lb_wf_inside_order As String = CType(e.Row.FindControl("lb_wf_inside_order"), Label).Text

            Dim ds As DataSet

            '***Start
            If o_wf_order = "0" And hid_status.Value <> "1" Then
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = lb_name.Text
                CType(e.Row.FindControl("label_userid"), Label).Text = lb_id.Text
            ElseIf hid_status.Value = "1" Then '待審核
                ds = Common.Qry_form_record_wait(lb_recordid.Text)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
            ElseIf hid_status.Value = "2" Or hid_status.Value = "9" Then '審核已通過
                ds = Common.Qry_form_record_name(lb_recordid.Text, o_wf_order)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
            End If

            If hid_status.Value = "" Then
                lb_status.Text = ""
            Else
                CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(hid_status.Value))
            End If
        End If
    End Sub
End Class
