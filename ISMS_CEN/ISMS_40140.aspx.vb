﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI
Partial Class ISMS_40140
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlCmd As SqlCommand
    Dim uid, uname, recordId As String
    Dim SqlTxt As String
    Dim dp As String = Me.Get_DP

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uid = User.Identity.Name '使用者帳號 
        Gdv1Show()

    End Sub

    Protected Sub Gdv1Show()
        Dim dt As DataTable = Common.Qry_FormRecord(dp)
        GridView1.DataSource = dt
        GridView1.DataBind()
    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim recordid As String = CType(e.Row.FindControl("hid_recordid_grd"), HiddenField).Value
            Dim hid_docid_grd As String = CType(e.Row.FindControl("hid_docid_grd"), HiddenField).Value
            Dim dp As HiddenField = CType(e.Row.FindControl("hid_dp"), HiddenField)
            Dim apprvid As HiddenField = CType(e.Row.FindControl("hid_apprvid"), HiddenField)
            Dim wf_order As HiddenField = CType(e.Row.FindControl("hid_wf_order"), HiddenField)

            Dim lb_flow As String = ""
            Dim ds, ds1 As DataSet

            If hid_docid_grd = "" Then
                CType(e.Row.FindControl("lb_flow"), Label).Text = Common.Str_form_record_isms_chkname(recordid, CType(e.Row.FindControl("hid_approvalid"), HiddenField).Value) '表單
            Else '文件
                ds = Common.Str_wfFormRecord_isms_chkname(hid_docid_grd, recordid)
                If ds.Tables(0).Rows.Count < 1 Then
                    CType(e.Row.FindControl("lb_flow"), Label).Text = "填表人尚未審核"
                    e.Row.Cells(3).Enabled = False
                Else
                    If ds.Tables(0).Rows.Count > 0 Then
                        With ds.Tables(0)
                            For i As Integer = 0 To .Rows.Count - 1
                                lb_flow += .Rows(i)("chk_name") + .Rows(i)("type")
                                If i <> .Rows.Count - 1 Then lb_flow += " -> "
                            Next
                        End With
                    End If

                    dp.Value = ds.Tables(0).Rows(0).Item("dp").ToString
                    apprvid.Value = ds.Tables(0).Rows(0).Item("apprv_id")
                    wf_order.Value = ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item("wf_order")
                    If ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item("type") = "(退回)" Then
                        wf_order.Value = ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item("wf_order") - 1
                    Else
                        wf_order.Value = ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item("wf_order") + 1
                    End If
                    ds1 = Common.Get_workflowName(apprvid.Value, dp.Value, wf_order.Value)
                    If ds1.Tables(0).Rows.Count < 1 Then
                        CType(e.Row.FindControl("lb_flow"), Label).Text = lb_flow
                    Else
                        CType(e.Row.FindControl("lb_flow"), Label).Text = lb_flow + " -> " + ds1.Tables(0).Rows(0).Item("chk_name") + "(等待)"
                    End If
                End If
            End If

        End If
    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
        Dim hid_recordid_grd As HiddenField = (TryCast(SelectedRow.FindControl("hid_recordid_grd"), HiddenField))
        Dim lb_FormName As Label = (TryCast(SelectedRow.FindControl("lb_FormName"), Label))
        Dim hid_docid_grd As HiddenField = CType(SelectedRow.FindControl("hid_docid_grd"), HiddenField)
        Dim hid_dp As HiddenField = CType(SelectedRow.FindControl("hid_dp"), HiddenField)
        Dim hid_apprvid As HiddenField = CType(SelectedRow.FindControl("hid_apprvid"), HiddenField)
        Dim hid_wf_order As HiddenField = CType(SelectedRow.FindControl("hid_wf_order"), HiddenField)
        Dim ds As DataSet

        Select Case e.CommandName
            Case "audit_btn"

                If hid_docid_grd.Value <> "" Then '文件
                    ds = Common.Qry_wfFormRecord_wait(hid_docid_grd.Value)
                    hid_type.Value = "D"
                    check_name.Text = ds.Tables(0).Rows(0).Item("user_name")
                Else '申請表
                    ds = Common.Qry_form_record_wait(hid_recordid_grd.Value)
                    hid_type.Value = "F"
                    check_name.Text = ds.Tables(0).Rows(0).Item("user_name")
                End If

                hid_recordid.Value = hid_recordid_grd.Value
                hid_docid.Value = hid_docid_grd.Value

                chk_distin.Checked = False
                ddl_dp.Enabled = False

                ddl_dp.DataSource = Common.ISMS_dpChange("")
                ddl_dp.DataTextField = "dp_name"
                ddl_dp.DataValueField = "dp_id"
                ddl_dp.DataBind()
                ddl_dp.SelectedValue = Get_DP()

                ddl_deptment.DataSource = Common.Get_dept(dp)
                ddl_deptment.DataTextField = "dept_name"
                ddl_deptment.DataValueField = "dept_id"
                ddl_deptment.DataBind()
                ddl_deptment.SelectedIndex = 0

                ddl_Upd_User.DataSource = Common.Get_User_Y(dp, ddl_deptment.SelectedValue)
                ddl_Upd_User.DataTextField = "user_name"
                ddl_Upd_User.DataValueField = "user_id"
                ddl_Upd_User.DataBind()

                tr_gridview.Visible = False
                Ins_TB.Visible = True
        End Select

    End Sub
    Protected Sub Cancel_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cancel_Btn.Click
         Gdv1Show()

        Ins_TB.Visible = False
        tr_gridview.Visible = True
    End Sub
    Protected Sub Update_Btm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click

        Try
            If hid_type.Value = "D" Then '文件
                SqlTxt = " UPDATE docResoure set next_check_id ='" + ddl_Upd_User.SelectedValue + "' WHERE doc_id ='" + hid_docid.Value + "' "
                SqlTxt += " UPDATE wfFormRecord SET wf_userid ='" + ddl_Upd_User.SelectedValue + "' WHERE doc_id='" + hid_docid.Value + "' and wf_status_id ='1' "
            Else
                SqlTxt = "update form_record set userId='" + ddl_Upd_User.SelectedValue + "' where recordId='" + hid_recordid.Value + "' and status='1' "
            End If
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

            Common.showMsg(Me.Page, "變更審查人員成功!")

        Catch ex As Exception

        End Try
        Gdv1Show()

        Ins_TB.Visible = False
        tr_gridview.Visible = True

    End Sub
    Protected Sub ddl_dp_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ds As DataSet = Common.Get_dept(ddl_dp.SelectedValue)
        ddl_deptment.DataSource = ds
        ddl_deptment.DataTextField = "dept_name"
        ddl_deptment.DataValueField = "dept_id"
        ddl_deptment.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            ddl_deptment.SelectedIndex = 0
            ddl_Upd_User.DataSource = Common.Get_User_Y(ddl_dp.SelectedValue, ddl_deptment.SelectedValue)
            ddl_Upd_User.DataTextField = "user_name"
            ddl_Upd_User.DataValueField = "user_id"
            ddl_Upd_User.DataBind()
        Else
            ddl_Upd_User.Items.Clear()
        End If

    End Sub
    Protected Sub ddl_deptment_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)

        ddl_Upd_User.DataSource = Common.Get_User_Y(ddl_dp.SelectedValue, ddl_deptment.SelectedValue)
        ddl_Upd_User.DataTextField = "user_name"
        ddl_Upd_User.DataValueField = "user_id"
        ddl_Upd_User.DataBind()
    End Sub
    Protected Sub chk_distin_checkedchange(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_distin.CheckedChanged

        If chk_distin.Checked Then

            ddl_dp.Enabled = True
            ddl_Upd_User.DataSource = Common.Get_User_Y(dp, ddl_deptment.SelectedValue)
            ddl_Upd_User.DataTextField = "user_name"
            ddl_Upd_User.DataValueField = "user_id"
            ddl_Upd_User.DataBind()
        Else

            ddl_dp.Enabled = False
            ddl_Upd_User.DataSource = Common.Get_User_Y(dp, ddl_deptment.SelectedValue)
            ddl_Upd_User.DataTextField = "user_name"
            ddl_Upd_User.DataValueField = "user_id"
            ddl_Upd_User.DataBind()

        End If
    End Sub
End Class

