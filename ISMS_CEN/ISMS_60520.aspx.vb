﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Partial Class ISMS_60520
    Inherits PageBase
    'Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim uid, uname As String
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim dp As String = Get_DP()

    'Dim apprvid As String = ConfigurationManager.AppSettings("wf14")
    Dim apprvid As String
    Dim approvalid As String
    Dim approval_name As String = "局端審查地所系統帳號申請表"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uid = User.Identity.Name
        uname = Common.Get_User_Name(uid)
        Dim nowpage() As String = Request.CurrentExecutionFilePath.Split("/")
        approvalid = Me.Get_approvalid(nowpage(nowpage.Length - 1), dp)
        apprvid = Me.Get_apprvid(nowpage(nowpage.Length - 1), dp)

        If Not IsPostBack Then
            bindRepeater()
            Show_ddl()
            bindPhone(uid)
            bindNote()
            bindAuthority(uid)
            txb_user_name.Text = uname
            txb_user_id.Text = uid
            ddl_e_year.SelectedValue = Now.Year - 1911
            ddl_e_month.SelectedValue = 12
            ddl_e_day.SelectedValue = 31
            GdvShow()
        End If
    End Sub

    Private Function Chk_Save() As Boolean
        Chk_Save = False
    End Function

    Protected Sub bindRepeater()

        Repeater1.DataSource = Common.Get_auth_sys(approvalid, dp)
        Repeater1.DataBind()

    End Sub
    Protected Sub bindPhone(ByVal user_id As String)
        Dim Ds As New DataSet
        Ds = Common.Get_phone(user_id)
        If Ds.Tables(0).Rows.Count > 0 Then
            txb_ext.Text = Ds.Tables(0).Rows(0).Item("ext").ToString
            txb_phone.Text = Ds.Tables(0).Rows(0).Item("phone").ToString
        End If
    End Sub
    Protected Sub Show_ddl()
        '單位
        ddl_deptment.DataSource = Common.Get_dept(dp)
        ddl_deptment.DataValueField = "dept_id"
        ddl_deptment.DataTextField = "dept_name"
        ddl_deptment.DataBind()

        '職稱
        ddl_degree.DataSource = Common.Get_degree(dp)
        ddl_degree.DataValueField = "degree_id"
        ddl_degree.DataTextField = "degree_name"
        ddl_degree.DataBind()

        ddlbind_year(ddl_apply_year) : ddlbind_year(ddl_open_year) : ddlbind_year(ddl_s_year) : ddlbind_year(ddl_e_year)
        ddlbind_month(ddl_apply_month) : ddlbind_month(ddl_open_month) : ddlbind_month(ddl_s_month) : ddlbind_month(ddl_e_month)
        ddlbind_day(ddl_apply_day) : ddlbind_day(ddl_open_day) : ddlbind_day(ddl_s_day) : ddlbind_day(ddl_e_day)


    End Sub

    Protected Sub bindNote()
        Dim Ds As New DataSet
        Ds = Common.Get_auth_sys_note(approvalid, dp)
        If Ds.Tables(0).Rows.Count > 0 Then
            note_Edit.Text = Ds.Tables(0).Rows(0).Item("note")
        End If
    End Sub
    Protected Sub bindAuthority(ByVal user_id As String)
        For Each item As Control In Repeater1.Controls
            Dim Ds As New DataSet
            Dim idt_repeater As Repeater = DirectCast(item.FindControl("ChildRepeater"), Repeater)
            Dim hid_sys_id As HiddenField = DirectCast(item.FindControl("hidden_sysid"), HiddenField)
            Dim hidden_idtname As HiddenField = DirectCast(item.FindControl("hidden_idtname"), HiddenField)
            Dim sys_rmk As TextBox = DirectCast(item.FindControl("sys_Rmk"), TextBox)
            Dim list_idt As List(Of String) = New List(Of String)
            Ds = Common.Get_b_office_apply_dtl2(user_id, hid_sys_id.Value)
            If Ds.Tables(0).Rows.Count > 0 Then
                sys_rmk.Text = Ds.Tables(0).Rows(0).Item("rmk").ToString
                For Each item2 As Control In idt_repeater.Controls
                    Dim chk_idt_name As CheckBox = DirectCast(item2.FindControl("chk_idt_name"), CheckBox)
                    Dim find_rows As DataRow() = Ds.Tables(0).Select("idt_name like '%" + chk_idt_name.Text + "%'")
                    If find_rows.Length > 0 Then
                        chk_idt_name.Checked = True
                        list_idt.Add("," + chk_idt_name.Text)
                    End If
                Next
            End If

        Next
    End Sub


    Protected Sub Repeater1_OnItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
            'Reference the Repeater Item.
            Dim item As RepeaterItem = e.Item
            'Reference the Controls.
            Dim idtRepeater As Repeater = (TryCast(item.FindControl("ChildRepeater"), Repeater))
            Dim sysid As HiddenField = (TryCast(item.FindControl("hidden_sysid"), HiddenField))
            idtRepeater.DataSource = Common.Get_auth_sys_idt(sysid.Value)
            idtRepeater.DataBind()
        End If
    End Sub
    Protected Sub ddl_s_day_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlbind_day(ddl_s_day, ddl_s_year.SelectedValue, ddl_s_month.SelectedValue, False)
    End Sub
    Protected Sub ddl_open_day_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlbind_day(ddl_open_day, ddl_open_year.SelectedValue, ddl_open_month.SelectedValue, False)
    End Sub
    Protected Sub ddl_apply_day_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlbind_day(ddl_apply_day, ddl_apply_year.SelectedValue, ddl_apply_month.SelectedValue, False)
    End Sub
    Protected Sub ddl_e_day_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlbind_day(ddl_e_day, ddl_e_year.SelectedValue, ddl_e_month.SelectedValue, False)
    End Sub
    Private Sub ddlbind_day(ByVal ddl As DropDownList, Optional ByVal year As String = "", Optional ByVal month As String = "", Optional ByVal default_day As Boolean = True)
        ddl.Items.Clear()
        If month = "" Then month = Now.Month
        If year = "" Then year = Now.Year - 1911
        For i As Integer = Date.DaysInMonth(year, month) To 1 Step -1
            ddl.Items.Insert(0, New ListItem(i.ToString.PadLeft(2, "0"), i.ToString.PadLeft(2, "0")))
        Next
        If default_day Then ddl.SelectedValue = Now.Day.ToString.PadLeft(2, "0")
    End Sub
    Private Sub ddlbind_year(ByVal ddl As DropDownList)
        Dim startyear As Integer = Now.Year - 1911 + 3
        For i As Integer = 6 To 1 Step -1
            ddl.Items.Insert(0, New ListItem(startyear, startyear))
            startyear = startyear - 1
        Next
        ddl.SelectedValue = Now.Year - 1911
    End Sub
    Private Sub ddlbind_month(ByVal ddl As DropDownList)
        For i As Integer = 12 To 1 Step -1
            Dim mon As String = i.ToString.PadLeft(2, "0")
            ddl.Items.Insert(0, New ListItem(mon, mon))
        Next
        ddl.SelectedValue = Now.Month.ToString.PadLeft(2, "0")
    End Sub
    Protected Sub txb_user_nameTextChanged()
        Dim id = Common.Get_User_ID(txb_user_name.Text, dp)
        If id = "" Then
            Common.showMsg(Me.Page, "沒有此使用者姓名之代碼，請重新輸入")
            txb_user_name.Text = ""
        Else
            txb_user_id.Text = id
            bindRepeater()
            bindAuthority(id)
            bindPhone(id)
        End If
    End Sub
    Protected Sub txb_user_idTextChanged()
        Dim name = Common.Get_User_Name_dp(txb_user_id.Text, dp)
        If name = "" Then
            Common.showMsg(Me.Page, "沒有此使用者代碼之使用者，請重新輸入")
            txb_user_id.Text = ""
        Else
            txb_user_name.Text = name
            bindRepeater()
            bindAuthority(txb_user_id.Text)
        End If
    End Sub

    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        Dim selectedValues As List(Of String) = New List(Of String)
        For Each li As ListItem In chk_apply_Reason.Items
            If li.Selected Then
                selectedValues.Add(li.Value)
            End If
        Next
        If selectedValues.Count = 0 Then
            Common.showMsg(Me.Page, "請勾選申請原因")
            Return
        End If
        If txb_user_name.Text = "" Then
            Common.showMsg(Me.Page, "請輸入使用者姓名")
            Return
        End If
        If txb_user_id.Text = "" Then
            Common.showMsg(Me.Page, "請輸入使用者代碼")
            Return
        End If
        If GridView.Rows.Count < 1 Then
            Common.showMsg(Me.Page, "請先至(系統管理->審核流程管理)設定關卡流程")
            Return
        End If
        Dim b_office_apply_id As String = Common.Get_max_b_office_apply_id()
        Dim TNow As DateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
        Dim recordid As String = uid + Format(TNow, "yyyyMMddHHmmss")
        For Each item As Control In Repeater1.Controls
            Dim idt_repeater As Repeater = DirectCast(item.FindControl("ChildRepeater"), Repeater)
            Dim hid_sys_id As HiddenField = DirectCast(item.FindControl("hidden_sysid"), HiddenField)
            Dim sys_rmk As TextBox = DirectCast(item.FindControl("sys_Rmk"), TextBox)
            Dim list_idt As List(Of String) = New List(Of String)
            For Each item2 As Control In idt_repeater.Controls
                Dim chk_idt_name As CheckBox = DirectCast(item2.FindControl("chk_idt_name"), CheckBox)
                If chk_idt_name.Checked Then
                    list_idt.Add(chk_idt_name.Text)
                End If
            Next
            If list_idt.Count > 0 Then
                SqlTxt = "INSERT INTO b_office_apply_dtl ([b_office_apply_id],[sys_id],[idt_name],[rmk]) VALUES ('" + b_office_apply_id + "','" + hid_sys_id.Value + "','" + String.Join(",", list_idt.ToArray()) + "','" + sys_rmk.Text + "') "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            End If

        Next
        Dim apply_date As String = ddl_apply_year.SelectedValue + ddl_apply_month.SelectedValue + ddl_apply_day.SelectedValue
        Dim enable_date As String = ddl_open_year.SelectedValue + ddl_open_month.SelectedValue + ddl_open_day.SelectedValue
        Dim start_date As String = ddl_s_year.SelectedValue + ddl_s_month.SelectedValue + ddl_s_day.SelectedValue
        Dim end_date As String = ddl_e_year.SelectedValue + ddl_e_month.SelectedValue + ddl_e_day.SelectedValue

        SqlTxt = "INSERT INTO b_office_apply ([b_office_apply_id],[dept_id],[apply_reason],[apply_reason_other],[user_name],[ext],[email],[phone]"
        SqlTxt += ",[degree_id],[item],[apply_date],[enable_date],[start_date],[end_date],[user_id],[user_pwd],[disable_date],[disable_reason],[apply_user_id],[using],[dp],[recordid]) "
        SqlTxt += "VALUES ('" + b_office_apply_id + "','" + ddl_deptment.SelectedValue + "','" + String.Join(",", selectedValues.ToArray()) + "','" + IIf(selectedValues.Contains("03"), txb_apply_Reason_other.Text, "") + "',"
        SqlTxt += "'" + txb_user_name.Text + "','" + txb_ext.Text + "','" + txb_email.Text + "','" + txb_phone.Text + "','" + ddl_degree.SelectedValue + "','" + txb_item.Text + "','" + apply_date + "','" + enable_date + "','" + start_date + "','" + end_date + "',"
        SqlTxt += "'" + txb_user_id.Text + "','','" + txb_disable_date.Text.Replace("/", "") + "','" + txb_disable_reason.Text + "','" + uid + "','Y' ,'" + dp + "','" + recordid + "') "

        '申請人
        SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype , dp) values "
        SqlTxt += " ('" + approvalid + "','" + recordid + "','" + CType(GridView.Rows(0).FindControl("wf_order"), Label).Text + "','" + uid + "',"
        SqlTxt += "'" + CType(GridView.Rows(0).FindControl("ddl_status"), DropDownList).SelectedValue + "','" + CType(GridView.Rows(0).FindControl("txb_comment"), TextBox).Text + "',getdate() ,'0', 'vender', '" + dp + "') "

        '下一位審核人
        '1091119modify 修改bug
        If GridView.Rows.Count >= 2 Then
            SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype , dp) values "
            SqlTxt += " ('" + approvalid + "','" + recordid + "','" + CType(GridView.Rows(1).FindControl("wf_order"), Label).Text + "','" + CType(GridView.Rows(1).FindControl("ddl_user"), DropDownList).SelectedValue + "',"
            SqlTxt += " '1', '',getdate() ,'1', 'land', '" + dp + "') "

        End If

        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", "申請地所系統使用者帳號 ")
        Common.showMsg(Me.Page, "儲存成功！")

    End Sub
    Private Sub GdvShow()
        Dim dt As DataTable = Common.Qry_workflowName_isms_Write(apprvid, dp)
        GridView.DataSource = dt
        GridView.DataBind()
        'Dim sql As String = "SELECT * FROM workflowName where apprv_id='" +apprvid +"'   and dp='" + dp + "' order by wf_order, wf_inside_order"

    End Sub
    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料

            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            '***意見
            Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text
            '***審核人員***
            Dim lb_wf_inside_order As String = CType(e.Row.FindControl("lb_wf_inside_order"), Label).Text

            Dim ds As DataSet

            '***Start
            If o_wf_order = "0" Then
                '人員
                pan_user.Visible = True : ddl_user.Visible = False : lb_username.Visible = True : lb_userid.Visible = True
                lb_userid.Text = uid : lb_username.Text = uname
                '狀態
                ddl_status.Visible = True : lb_status.Visible = False
                ddl_status.DataSource = Common.Get_WFStatus(0)
                ddl_status.DataValueField = "wfstatus_id" : ddl_status.DataTextField = "wfstatus_name" : ddl_status.DataBind()
                '意見
                txt_comment.Visible = True : lb_comment.Visible = False
            ElseIf o_wf_order = "1" Then
                ddl_user.Visible = True : pan_user.Visible = False
                ds = Common.Get_Wf_User(dp, apprvid, o_wf_order, "")

                ddl_user.DataSource = ds
                ddl_user.DataTextField = "name_id"
                ddl_user.DataValueField = "wf_userid"
                ddl_user.DataBind()

            End If
        End If
    End Sub
End Class
