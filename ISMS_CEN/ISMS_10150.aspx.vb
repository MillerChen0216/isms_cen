﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_10150
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim oRow As GridViewRow
    Dim Index As Integer

    Protected Sub Doc_SqlDS()
        Doc_DS.ConnectionString = Common.ConnDBS.ConnectionString

        If DDL_DocType.SelectedValue = "0" Then
            Doc_DS.SelectCommand = "SELECT * FROM docResoure where   dp='" + Hidden_DP.Value + "' and  doc_name like '%" + Doc_name_txt.Text + "%' and  issue_date is not null"
        Else
            Doc_DS.SelectCommand = "SELECT * FROM docResoure where  dp='" + Hidden_DP.Value + "' and doc_type='" + DDL_DocType.SelectedValue + "' and doc_name like '%" + Doc_name_txt.Text + "%' and issue_date is not null "
        End If
        GridView1.DataSourceID = Doc_DS.ID
    End Sub

    Protected Sub Show_DDL_DocType()

        DDL_DocType.DataSource = Common.Get_DocType
        DDL_DocType.DataTextField = "sValue"
        DDL_DocType.DataValueField = "doc_type"
        DDL_DocType.DataBind()
        DDL_DocType.Items.Insert(0, New ListItem("全部", "0"))

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        If Not Page.IsPostBack Then
            Show_DDL_DocType()
        End If

    End Sub

    Protected Sub Query_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Query_Btn.Click
        GridView1.Visible = True
        Doc_SqlDS()
    End Sub


    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType <> DataControlRowType.Header Then

            e.Row.Cells(0).Text = Common.ISMS_DocNameChange(e.Row.Cells(0).Text)
            e.Row.Cells(6).Text = Common.Get_User_Name(e.Row.Cells(6).Text)
        End If

    End Sub
End Class
