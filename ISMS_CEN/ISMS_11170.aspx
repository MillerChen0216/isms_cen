﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_11170.aspx.vb" Inherits="ISMS_11170" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:HiddenField ID="Hidden_DP" runat="server" />
        <br />
        <table id="Doc_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 750px"
            visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    <asp:Label ID="LB_DocType" runat="server"></asp:Label>
                    | 列表&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="750px">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/news_icon.gif" />
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                            </asp:TemplateField>                            <asp:BoundField DataField="doc_num" HeaderText="文件代碼">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_name" HeaderText="文件名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_version" HeaderText="版次">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_capsule" HeaderText="文件簡述">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_id" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="下載文件" ShowHeader="False">
                                <ItemTemplate>
                                    <asp:Button ID="Button1" runat="server" CausesValidation="false" CommandName="" OnClick="Button1_Click"
                                        Text="下載" />
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                    
                            
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
        <asp:SqlDataSource ID="Doc1_DS" runat="server"></asp:SqlDataSource>
    </form>
</body>
</html>
