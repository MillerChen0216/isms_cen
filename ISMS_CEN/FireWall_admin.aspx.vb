﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO

Partial Class FireWall_admin
    Inherits PageBase
    'Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim uid As String = User.Identity.Name '使用者名稱
    Dim uname As String = Common.Get_User_Name(uid) '使用者代號
    Dim apprvid As String
    Dim approvalid As String
    Dim dp As String = Get_DP()
    Dim File_Path As String = ConfigurationManager.AppSettings("FireWall_Path")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        approvalid = ConfigurationManager.AppSettings("flow_FireWall") '16
        apprvid = ConfigurationManager.AppSettings("flow_FireWall") '16
        Try
            If Not IsPostBack Then
                txb_sdate.Text = Common.Year(Now.AddDays(-30))
                txb_edate.Text = Common.Year(Now.AddDays(1))
                SqlDataSource() '載入管理者清單
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '載入管理者清單
    Protected Sub SqlDataSource()

        Dim sql As String = "SELECT * FROM FireWall where using='Y'"
        '排除已撤銷之申請單
        '排除停用帳號
        sql += " and empUid in (select user_id from ismsUser where using='Y') "
        If txb_username.Text <> "" Then
            sql += " and empName like'%" + Trim(txb_username.Text) + "%'"
        End If
        If txb_sdate.Text <> "" Then
            sql += " and applyDate >= '" + Common.YearYYYY(txb_sdate.Text) + "'"
        End If
        If txb_edate.Text <> "" Then
            sql += " and applyDate <= '" + Common.YearYYYY(txb_edate.Text) + "'"
        End If

        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = sql

        GridView1.DataSourceID = SqlDataSource1.ID

    End Sub

    '申請清單民國年處理
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '民國年轉換
                Dim lb_applydate As String = CType(e.Row.FindControl("lb_applydate"), Label).Text
                CType(e.Row.FindControl("lb_applydate"), Label).Text = Common.Year(Date.Parse(lb_applydate))
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '查詢
    Protected Sub but_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_search.Click
        Try
            SqlDataSource()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub


    '查看或撤銷
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
        Dim hidden_recordId As HiddenField = (TryCast(SelectedRow.FindControl("hidden_recordId"), HiddenField))
        Select Case e.CommandName

            Case "audit_btn"
                Try
                    lb_recordid.Text = hidden_recordId.Value '紀錄該表單編號
                    Dim sql = "  SELECT * FROM FireWall WHERE recordId = '" + hidden_recordId.Value + "'"
                    Dim data As DataTable = Common.Con(sql)
                    If data.Rows.Count > 0 Then
                        '載入關卡表
                        GdvShow()

                        Dim workflowId = Common.Get_workflowId(approvalid, lb_recordid.Text) '取得關卡

                        ''填入該lb_rowid資料
                        applyDate.Text = Common.Year(data.Rows(0)("applyDate")) '申請日期
                        recordId.Text = data.Rows(0)("recordId") '表單編號
                        empName.Text = data.Rows(0)("empName") '申請代表人
                        applyOrg.Text = data.Rows(0)("applyOrg") '申請單位
                        applyName.Text = data.Rows(0)("applyName") '申請人員
                        applyTel1.Text = data.Rows(0)("applyTel1") '連絡電話1
                        applyTel2.Text = data.Rows(0)("applyTel2") '連絡電話2
                        startTime.Text = Common.Year(data.Rows(0)("startTime")) '預計開放日期(起)
                        endTime.Text = Common.Year(data.Rows(0)("endTime")) '預計開放日期(迄)
                        applyReason.Text = data.Rows(0)("applyReason") '申請事由
                        F_Ip.Text = data.Rows(0)("F_Ip") '來源IP
                        F_Port.Text = data.Rows(0)("F_Port") '來源Port
                        T_Ip.Text = data.Rows(0)("T_Ip") '目的IP
                        T_Port.Text = data.Rows(0)("T_Port") '目的Port
                        Radio_Connected.SelectedValue = data.Rows(0)("Connected") '單/雙向連接

                        '載入資訊維護暨攜出入申請單編號
                        SYSIO_recordId.Text = ""
                        Dim sql2 = "select * from SYSIO_related where related_recordId = '" + lb_recordid.Text + "'"
                        Dim data2 As DataTable = Common.Con(sql2)
                        If Not data2.Rows.Count = 0 Then
                            For Each item In data2.Select
                                If Not String.IsNullOrEmpty(item("original_SYSIO_recordId").ToString()) Then
                                    If Not SYSIO_recordId.Text.Contains(item("original_SYSIO_recordId").ToString()) Then
                                        SYSIO_recordId.Text += item("original_SYSIO_recordId") + ", "
                                    End If
                                Else
                                    SYSIO_recordId.Text += item("SYSIO_recordId") + ","
                                End If
                            Next
                        End If


                        TopSheetEnabled(False) '都不能修改
                        DownSheetEnabled(False) '都不能修改

                        If Not workflowId = 0 Then
                            DownSheetVisible(True)

                            If Not IsDBNull(data.Rows(0)("verifyTime")) Then
                                Radio_verifyRule.SelectedValue = data.Rows(0)("verifyRule") '防火牆規則
                                verifyComment.Text = data.Rows(0)("verifyComment") '審核說明
                                verifyTime.Text = data.Rows(0)("verifyTime") '辦理日期
                            End If
                        End If

                        '查看裡面是否有資料，有才秀出來
                        If IsDBNull(data.Rows(0)("applyFile")) Then
                            applyFile.Visible = False
                        Else
                            applyFile.Visible = True
                            applyFile_link.Text = data.Rows(0)("applyFile")
                        End If

                        '查看裡面是否有資料，有才秀出來
                        If IsDBNull(data.Rows(0)("verifyFile")) Then
                            verifyFile.Visible = False
                        Else
                            verifyFile.Visible = True
                            verifyFile_link.Text = data.Rows(0)("verifyFile") '附件檔案(資訊單位填寫)
                        End If
                    Else
                        Response.Redirect("FireWall_admin.aspx", True) '如果沒有這個recordId就重新導向管理者清單
                    End If
                    tb_result.Visible = True
                    tb_query.Visible = False
                    GridView1.Visible = False
                Catch ex As Exception
                    lb_msg.Text = ex.Message
                End Try
            Case "using_btn"
                Try
                    Dim sql As New StringBuilder
                    sql.Append("update FireWall set using='N' where recordId ='" + hidden_recordId.Value + "' ")
                    sql.Append("  update form_record set status = -1 where recordId = '" + hidden_recordId.Value + "' and approvalId = '" + approvalid + "' and status = 1")

                    Dim conn As SqlConnection = Nothing : Dim cmd As New SqlCommand(sql.ToString)
                    Try
                        conn = Common.ConnDBS : conn.Open() : cmd.Connection = conn
                        If cmd.ExecuteNonQuery() Then GridView1.DataBind() : lb_msg.Text = "撤銷成功！"
                    Catch ex As Exception
                        lb_msg.Text = "撤銷失敗！" + ex.Message
                    Finally
                        conn.Close() : conn.Close() : conn.Dispose() : cmd.Dispose()
                    End Try
                Catch ex As Exception
                    lb_msg.Text = ex.Message
                End Try


        End Select
    End Sub

    '小工具1
    Private Sub TopSheetEnabled(ByVal use As Boolean)
        applyOrg.Enabled = use
        applyName.Enabled = use
        applyTel1.Enabled = use
        applyTel2.Enabled = use
        startTime.Enabled = use
        endTime.Enabled = use
        applyReason.Enabled = use
        F_Ip.Enabled = use
        F_Port.Enabled = use
        T_Ip.Enabled = use
        T_Port.Enabled = use
        Radio_Connected.Enabled = use
    End Sub

    '小工具2
    Private Sub DownSheetVisible(ByVal show As Boolean)
        tr1.Visible = show
        tr2.Visible = show
        tr3.Visible = show
        tr4.Visible = show
        tr5.Visible = show
    End Sub

    '小工具3
    Private Sub DownSheetEnabled(ByVal use As Boolean)
        Radio_verifyRule.Enabled = use
        verifyComment.Enabled = use
        verifyTime.Enabled = use
    End Sub

    '關卡
    Private Sub GdvShow()
        Dim dt As DataTable = Common.Qry_workflowName_isms_Req(approvalid, lb_recordid.Text, apprvid, dp)
        GridView.DataSource = dt
        GridView.DataBind()

    End Sub

    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料

            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            Dim hid_status As HiddenField = CType(e.Row.FindControl("hid_status"), HiddenField)
            '***意見
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text
            '***審核人員***
            ' Dim lb_wf_inside_order As String = CType(e.Row.FindControl("lb_wf_inside_order"), Label).Text

            Dim ds As DataSet

            '***Start
            If o_wf_order = "0" And hid_status.Value <> "1" Then
                pan_user.Visible = True
                '將審核人員設為自己
                Dim sql = "    SELECT user_id, user_name FROM ismsUser WHERE user_id = '" + lb_username.Text + "'"
                Dim data As DataTable = Common.Con(sql)
                CType(e.Row.FindControl("lb_username"), Label).Text = data.Rows(0)("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = data.Rows(0)("user_id")
            ElseIf hid_status.Value = "1" Then '待審核
                ds = Common.Qry_form_record_wait(lb_recordid.Text)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
            ElseIf hid_status.Value = "2" Or hid_status.Value = "9" Then '審核已通過
                ds = Common.Qry_form_record_name(lb_recordid.Text, o_wf_order)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
            End If

            If hid_status.Value = "" Then
                lb_status.Text = ""
            Else
                CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(hid_status.Value))
            End If
        End If
    End Sub
    
    '檔案下載_按鈕(上()
    Protected Sub applyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + applyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '檔案下載_按鈕(下)
    Protected Sub verifyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles verifyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + verifyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_回列表_按鈕
    Protected Sub but_esc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_esc.Click
        Response.Redirect("FireWall_admin.aspx", True)
    End Sub
End Class