<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_31182.aspx.vb" Inherits="ISMS_31182" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <br />
                <asp:HiddenField ID="Hidden_DP" runat="server" />

        <table id="Main_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 750px"
            visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    風險評估彙整表瀏覽作業 | 列表&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="750px">
                        <Columns>
                            <asp:BoundField DataField="asset_id" HeaderText="資產分類">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="riskEvtab_name" HeaderText="風險評鑑表">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="add_date" HeaderText="日期">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="瀏覽">
                                <ItemTemplate>
                                    <asp:Button ID="audit_btn" runat="server" OnClick="audit_btn_Click" Text="瀏覽" />
                                    &nbsp;
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="form_id" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="審核狀態">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lb_wf" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
        <asp:SqlDataSource ID="Flow_DS" runat="server"></asp:SqlDataSource>
        <asp:SqlDataSource ID="RiskForm_DS" runat="server"></asp:SqlDataSource>
        <asp:SqlDataSource ID="WF_DS" runat="server"></asp:SqlDataSource>
        <asp:HiddenField ID="Hidden_Formid" runat="server" Visible="False" /><asp:HiddenField ID="Hidden_FormName" runat="server" Visible="False" />
        <br />
        <table id="Show_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 750px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="5" rowspan="1">
                    <img src="Images/exe.GIF" />
                    風險評估彙整表<asp:Button ID="Output_XLS" runat="server" CssClass="button-small" Text="另存Excel檔案" /></td>
            </tr>
            <tr>
                <td rowspan="1">
                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        HorizontalAlign="Left" Width="750px">
                        <Columns>
                            <asp:BoundField DataField="assitem_id" HeaderText="資產編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="assitem_name" HeaderText="資產名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="twscore" HeaderText="威脅弱點值">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="assitScore" HeaderText="資產價值">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="totalrisk_score" HeaderText="綜合風險值">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="assgroup_name" HeaderText="群組">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        
    </form>
</body>
</html>
