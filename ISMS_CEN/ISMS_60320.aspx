﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_60320.aspx.vb" Inherits="ISMS_60320" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>地政局系統使用者帳號申請表</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function toLocalDate() {
            var year = parseInt(document.getElementById("txb_disable_date").value.substr(0, 4)) - 1911;
            document.getElementById("txb_disable_date").value = year + document.getElementById("txb_disable_date").value.substr(4, 6);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
            EnableScriptLocalization="True">
        </asp:ScriptManager>
        <table id="TB" runat="server" border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2"
            width="850px">
            <%-- <tr><td colspan="4" style="text-align: right"><asp:Button ID="Insert_Btn" runat="server" CssClass="button-small" Text="儲存" /></td></tr>--%>
            <tr>
                <td colspan="4">
                    <img src="Images/exe.gif" />地政局系統使用者帳號申請表 | 填寫
                    <asp:Label ID="lb_msg" runat="server" ForeColor="red" EnableViewState="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 110px;">
                    單位
                </td>
                <td>
                    <asp:DropDownList ID="ddl_deptment" runat="server">
                    </asp:DropDownList>
                </td>
                <td class="tb_title_w_1" style="width: 110px;">
                    申請原因<font color="red">*</font></td>
                <td>
                    <asp:CheckBoxList ID="chk_apply_Reason" runat="server" RepeatColumns="3" RepeatLayout="Flow">
                        <%--<asp:ListItem Value="01">年度檢討</asp:ListItem>--%>
                        <asp:ListItem Value="02">職務異動</asp:ListItem>
                        <asp:ListItem Value="05">其他</asp:ListItem>
                        <asp:ListItem Value="04">離職</asp:ListItem>
                        <asp:ListItem Value="03">新進人員</asp:ListItem>
                    </asp:CheckBoxList>
                    <asp:TextBox ID="txb_apply_Reason_other" Style="text-align: left" Height="19px" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    使用者姓名<font color="red">*</font></td>
                <td>
                    <asp:TextBox ID="txb_user_name" Style="text-align: left" Height="19px" runat="server" OnTextChanged="txb_user_nameTextChanged" AutoPostBack="true"></asp:TextBox>
                </td>
                <td class="tb_title_w_1">
                    分機</td>
                <td>
                    <asp:TextBox ID="txb_ext" Style="text-align: left" Height="19px" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    職稱</td>
                <td colspan="3">
                    <asp:DropDownList ID="ddl_degree" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    業務項目</td>
                <td colspan="3">
                    <asp:TextBox ID="txb_item" Style="text-align: left;width:100%" Height="19px" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    申請日期</td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddl_apply_year" runat="server" Width="60px" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddl_apply_day_selectedchange">
                                        </asp:DropDownList>年
                                        <asp:DropDownList ID="ddl_apply_month" runat="server" Width="50px" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddl_apply_day_selectedchange">
                                        </asp:DropDownList>月
                                        <asp:DropDownList ID="ddl_apply_day" runat="server" Width="50px">
                                        </asp:DropDownList>日
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="tb_title_w_1">
                    啟用日期<font color="red">*</font></td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddl_open_year" runat="server" Width="60px" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddl_open_day_selectedchange">
                                        </asp:DropDownList>年
                                        <asp:DropDownList ID="ddl_open_month" runat="server" Width="50px" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddl_open_day_selectedchange">
                                        </asp:DropDownList>月
                                        <asp:DropDownList ID="ddl_open_day" runat="server" Width="50px">
                                        </asp:DropDownList>日
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    使用期間</td>
                <td colspan="3">
                    <asp:UpdatePanel ID="upd_time" runat="server">
                        <ContentTemplate>
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddl_s_year" runat="server" Width="60px" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddl_s_day_selectedchange">
                                        </asp:DropDownList>年
                                        <asp:DropDownList ID="ddl_s_month" runat="server" Width="50px" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddl_s_day_selectedchange">
                                        </asp:DropDownList>月
                                        <asp:DropDownList ID="ddl_s_day" runat="server" Width="50px">
                                        </asp:DropDownList>日 至<asp:DropDownList ID="ddl_e_year" runat="server" Width="60px"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddl_e_day_selectedchange">
                                        </asp:DropDownList>年
                                        <asp:DropDownList ID="ddl_e_month" runat="server" Width="50px" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddl_e_day_selectedchange">
                                        </asp:DropDownList>月
                                        <asp:DropDownList ID="ddl_e_day" runat="server" Width="50px">
                                        </asp:DropDownList>日
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    使用者代碼<font color="red">*</font></td>
                <td>
                    <asp:TextBox ID="txb_user_id" Style="text-align: left" Height="19px" runat="server" OnTextChanged="txb_user_idTextChanged" AutoPostBack="true" ></asp:TextBox>
                </td>
                <td class="tb_title_w_1">
                    使用者密碼</td>
                <td>
                    (預設值,請勿填寫)
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    保密切結</td>
                <td colspan="3">
                    本人<asp:TextBox ID="TextBox7" Style="text-align: left" Height="19px" runat="server"></asp:TextBox>於業務權限範圍內，將確實依本系統使用作業規定，查詢或列印相關資料，並遵守個人資料保護法、智慧財產權、著作權等相關規定，如有不當使用、資料外洩或違法者，應依法負民、刑事責任及行政責任。
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    註銷(停用)日期</td>
                <td>
                    <asp:TextBox ID="txb_disable_date" Style="text-align: left" Height="19px" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txb_disable_date" OnClientDateSelectionChanged="toLocalDate"
                        Format="yyyy/MM/dd">
                    </cc1:CalendarExtender>
                </td>
                <td class="tb_title_w_1">
                    註銷(停用)原因</td>
                <td>
                    <asp:TextBox ID="txb_disable_reason" Style="text-align: left;width:100%" Height="19px" runat="server" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="tb_title_w_1">
                    填表說明</td>
            </tr>
            <tr>
                <td colspan="4" style="height: 109px">
                    <asp:Label ID="note_Edit" runat="server" Text=""></asp:Label>
<%--                     <ol>
                        <li>本申請表請<font style="font-weight:bold;" >確依業務需求提出申請</font>，標註<font style="font-weight:bold;" >＊</font>處申請時<font style="font-weight:bold;text-decoration:underline;" >免填寫</font>。</li>
                        <li>各系統新申請帳號者，預設密碼為aa+使用者卡號，帳號生效後請修改登入密碼，並善盡帳號密碼保管之責；舊使用者(年度檢討、權限異動…等)密碼不變。</li>
                        <li>注意事項︰系統所提供之資料僅作公務機關內部參考使用，不具任何文書證明效力，且不得對外提供。</li>
                     </ol>
--%>                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2" style="width: 100%">
                        <tr>
                            <td class="tb_title_w_1" style="width: 30%;">
                                授權範圍</td>
                            <td class="tb_title_w_1" style="width: 40%;">
                                權限</td>
                            <td class="tb_title_w_1" style="width: 30%;">
                                備註</td>
                        </tr>
                        <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_OnItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Label ID="sys_Name" Enabled="false" Text='<%#Eval("sys_name") %>' runat="server"></asp:Label>
                                        <asp:HiddenField ID="hidden_sysid" runat="server" Value='<%#Eval("sys_id") %>' />
                                    </td>
                                    <td>
                                        <!-- Repeated data -->
                                        <asp:Repeater ID="ChildRepeater" runat="server" >
                                            <ItemTemplate>
                                                <!-- Nested repeated data -->
                                                <asp:CheckBox ID="chk_idt_name" runat="server" Text='<%#Eval("idt_name") %>'  />
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </td>
                                    <td>
                                                                               <asp:TextBox ID="sys_Rmk" Rows="10" Columns="30" TextMode="MultiLine" Style="text-align: left;word-wrap:break-word;word-break:break-all;" Height="19px" Text='<%#Eval("sys_rmk") %>'
                                            runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <tr>
          <td colspan="4" align="center" ><%--<uc1:FlowControl ID="FlowControl1" runat="server"  />--%>
                    <asp:UpdatePanel ID ="UpdatePanel3" runat="server" >
            <ContentTemplate> 
         <asp:GridView ID="GridView" runat="server" AutoGenerateColumns="False" CellPadding="3"    CssClass="tb_1" Width="100%"  >
                        <Columns>
                           <asp:TemplateField HeaderText="關卡" ItemStyle-CssClass="tb_w_1">
                                 <ItemTemplate>
                                    <asp:Label ID="wf_order" runat="server" Text='<%# eval("wf_order") %>'></asp:Label>
                                 </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField Headertext="審核身份" ItemStyle-CssClass="tb_w_1">
                                  <ItemTemplate>
                                    <asp:Label ID="chk_name" runat="server" Text='<%# eval("chk_name") %>' ></asp:Label>
                                 </ItemTemplate>
                           </asp:TemplateField>                        
                           <asp:TemplateField HeaderText="審核人員" ItemStyle-CssClass="tb_w_1">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddl_user" runat="server" Visible="false"  ></asp:DropDownList>
                               
                                    <asp:Panel ID="pan_user" runat="server" Visible="false">
                                    <asp:Label ID="lb_username" runat="server" Visible="false"></asp:Label>
                                    (<asp:Label ID="label_userid" runat="server" Visible="false"></asp:Label>)
                                    </asp:Panel>                         
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="狀態" ItemStyle-CssClass="tb_w_1">
                               <ItemTemplate>
                                    <asp:DropDownList ID="ddl_status" runat="server" Visible="False"></asp:DropDownList>
                                    <asp:Label ID="lb_status" runat="server" Visible="False"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="意見" ItemStyle-CssClass="tb_w_1">
                                <ItemTemplate>
                                    <asp:TextBox ID="txb_comment" runat="server" Visible="False" Width="150px"></asp:TextBox>
                                    <asp:Label ID="lb_comment" runat="server" Visible="False"></asp:Label>                                 
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                       <HeaderStyle CssClass="tb_title_w_1" />
                    </asp:GridView> 
                  <%--  <asp:SqlDataSource ID="WF_DS" runat="server"></asp:SqlDataSource>--%>
                  </ContentTemplate>
          </asp:UpdatePanel>                   
                     <asp:Button ID="Insert_Btn" runat="server" Text="送出審核" />
          </td>
         </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
