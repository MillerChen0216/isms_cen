﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_60530.aspx.vb" Inherits="ISMS_60530" %>

<%@ Register Src="CONTRLS/FlowControl_isms.ascx" TagName="FlowControl_isms" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>局端審查地所系統帳號申請表</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
            EnableScriptLocalization="True">
        </asp:ScriptManager>
        <table border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2" width="850px">
            <tr>
                <td>
                    <img src="Images/exe.gif" />局端審查地所系統帳號申請表 | 年度檢討
                    <asp:Label ID="lb_msg" runat="server" ForeColor="red" EnableViewState="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="tb_query" runat="server" cellpadding="0" cellspacing="0" class="tb_title_w_2" style="width:100%">
                        <tr>
                            <td class="tb_title_w_1">
                                使用者代碼</td>
                            <td>
                                <asp:TextBox ID="txb_userid" runat="server" Width="80px"></asp:TextBox>
                            </td>
                            <td class="tb_title_w_1">
                                使用者姓名</td>
                            <td>
                                <asp:TextBox ID="txb_username" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td class="tb_title_w_1">
                                年度</td>
                            <td>
                                <asp:DropDownList ID="ddl_year" runat="server" Width="80px"></asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="but_search" runat="server" Text="查詢" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr_gridview" runat="server">
               <td>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
                    <asp:GridView ID="GridView1" runat="server" CssClass="tb_1" AutoGenerateColumns="false"  Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="選取">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
<%--                            <asp:TemplateField HeaderText="申請日期">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lb_applydate" Text='<%# eval("apply_date") %>'></asp:Label>
                                    <asp:HiddenField ID="hidden_b_office_apply_id" runat="server" Value='<%#Eval("b_office_apply_id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="使用者代碼">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lb_userId" Text='<%# eval("user_id") %>'></asp:Label>
                                    <asp:HiddenField ID="b_office_apply_id" runat="server" Value='<%#Eval("b_office_apply_id")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="使用者姓名">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lb_userName" Text='<%# eval("user_name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:ButtonField ButtonType="Button" CommandName="audit_btn" Text="查看" HeaderText="管理">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:ButtonField>
                        </Columns>
                        <HeaderStyle CssClass="tb_title_w_1" />
                        <EmptyDataTemplate>
                            …尚無資料…</EmptyDataTemplate>
                    </asp:GridView>
                    <asp:Button ID="btn_sel_print" runat="server" Text="勾選列印" CausesValidation="False"   />
                    <asp:Button ID="btn_all_print" runat="server" Text="帳號一覽表" CausesValidation="False" />
               </td>
            </tr>
            <tr>
                <td>
                    <table id="tb_result" runat="server" border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2" style="width:100%" visible="false">
                        <tr>
                          <td colspan="4" style="text-align:right">
                              <asp:Button ID="but_print" runat="server" Text="列印" CausesValidation="False"  />
                              <asp:Button ID="but_esc" runat="server" Text="回列表" CausesValidation="False" />
                          </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="tb_title_w_1">桃園市政府地政局資訊系統使用者帳號申請表(地所用)</td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1" style="width: 110px;">單位</td>
                            <td><asp:Label ID="lbl_dept_name" runat="server" Text="桃園市政府地政局"></asp:Label></td>
                            <td class="tb_title_w_1" style="width: 110px;"> 申請原因</td>
                            <td>年度檢討</td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">使用者姓名</td>
                            <td><asp:Label ID="lbl_user_name" runat="server" Text=""></asp:Label></td>
                            <td class="tb_title_w_1"> 分機</td>
                            <td><asp:Label ID="lbl_ext" runat="server" Text="100"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">E-mail信箱</td>
                            <td><asp:Label ID="lbl_email" runat="server" ></asp:Label></td>
                            <td class="tb_title_w_1"> 連絡電話</td>
                            <td><asp:Label ID="lbl_phone" runat="server" ></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">職稱</td>
                            <td colspan="3"><asp:Label ID="lbl_degree_name" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">業務項目</td>
                            <td colspan="3"> <asp:Label ID="lbl_item" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">申請日期</td>
                            <td><asp:Label ID="lbl_apply_date" runat="server" Text=""></asp:Label></td>
                            <td class="tb_title_w_1">啟用日期</td>
                            <td><asp:Label ID="lbl_enable_date" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">使用期間</td>
                            <td colspan="3"><asp:Label ID="lbl_start_date" runat="server" Text=""></asp:Label>至
                            <asp:Label ID="lbl_end_date" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">使用者代碼</td>
                            <td><asp:Label ID="lbl_user_id" runat="server" Text=""></asp:Label></td>
                            <td class="tb_title_w_1">使用者密碼</td>
                            <td>預設值</td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">保密切結</td>
                            <td colspan="3">本人___________於業務權限範圍內，將確實依本系統使用作業規定，查詢或列印相關資料，並遵守個人資料保護法、智慧財產權、著作權等相關規定，如有不當使用、資料外洩或違法者，應依法負民、刑事責任及行政責任。</td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">
                                註銷(停用)日期</td>
                            <td><asp:Label ID="lbl_disable_date" runat="server" Text=""></asp:Label></td>
                            <td class="tb_title_w_1">
                                註銷(停用)原因</td>
                            <td><asp:Label ID="lbl_disable_reason" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="4" class="tb_title_w_1">填表說明</td>
                        </tr>
                        <tr>
                            <td colspan="4" style="height: 109px">
                                <asp:Label ID="note_Edit" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="tb_title_w_1">
                                附表：系統授權範圍</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2" style="width: 100%">
                                    <tr>
                                        <td class="tb_title_w_1" style="width: 30%;">
                                            授權範圍</td>
                                        <td class="tb_title_w_1" style="width: 40%;">
                                            權限</td>
                                        <td class="tb_title_w_1" style="width: 30%;">
                                            備註</td>
                                    </tr>
                                    <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_OnItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbl_sys_name" Enabled="false" Text='<%#Eval("sys_name") %>' runat="server"></asp:Label>
                                                    <asp:HiddenField ID="hidden_sysid" runat="server" Value='<%#Eval("sys_id") %>' />
                                                    <asp:HiddenField ID="hidden_idtname" runat="server" />
                                                </td>
                                                <td>
                                                    <!-- Repeated data -->
                                                    <asp:Repeater ID="ChildRepeater" runat="server">
                                                        <ItemTemplate>
                                                            <!-- Nested repeated data -->
                                                            <asp:CheckBox ID="chk_idt_name" runat="server" Text='<%#Eval("idt_name") %>' Enabled="false" />
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbl_sys_rmk" Enabled="false" Text='<%#Eval("sys_rmk") %>' runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                 
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
