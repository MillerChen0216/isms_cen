﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_30130.aspx.vb" Inherits="ISMS_30130" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <div>
                <div>
                    <br />
                            <asp:HiddenField ID="Hidden_DP" runat="server" />

                    <table id="Show_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 500px"
                        visible="true">
                        <tr>
                            <td class="tb_title_w_2" colspan="5">
                                <img src="Images/exe.GIF" />
                                風險衝擊性 | 列表
                                <asp:Button ID="Show_Insert" runat="server" CssClass="button-small" Text="新增資料" /></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="tb_1"
                                    HorizontalAlign="Left" Width="600px">
                                    <Columns>
                                        <asp:BoundField DataField="impact_desc" HeaderText="衝擊">
                                            <HeaderStyle CssClass="tb_title_w_1" Width="70px" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="impact_score" HeaderText="等級">
                                            <HeaderStyle CssClass="tb_title_w_1" Width="70px" />
                                            <ItemStyle CssClass="tb_w_1"  />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="impact_std" HeaderText="衝擊性評估標準">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:ButtonField ButtonType="Button" CommandName="Edit_Btn" Text="編輯">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1"  />
                                        </asp:ButtonField>
                                        <asp:ButtonField ButtonType="Button" CommandName="Del_Btn" Text="刪除">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1"  />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="sn">
                                            <HeaderStyle CssClass="tb_title_w_1"  />
                                            <ItemStyle CssClass="tb_w_1"  />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    <table id="Edit_TB" runat="server" class="tb_1" style="width: 450px"
                        visible="false" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="tb_title_w_2" colspan="7" ><img src="Images/exe.GIF" />
                                風險衝擊性 | 更新資料 &nbsp;<asp:Button ID="Update_Btn" runat="server" CssClass="button-small"
                                    Text="更新資料" /></td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">
                                <span>等級</span></td>
                            <td class="tb_title_w_1">
                                衝擊</td>
                            <td class="tb_title_w_1">
                                衝擊性評估標準</td>
                        </tr>
                        <tr>
                            <td class="tb_w_1">
                                
                                    <asp:TextBox ID="impact_score_upd" runat="server" Width="30px" MaxLength="2"></asp:TextBox></td>
                            <td class="tb_w_1">
                                   <asp:TextBox ID="impact_desc_upd" runat="server" Width="80px" MaxLength="256"></asp:TextBox></td>
                            <td class="tb_w_1">
                                <asp:TextBox ID="impact_std_upd" runat="server" MaxLength="256" TextMode="MultiLine"
                                    Width="300px" Height="100px"></asp:TextBox></td>
                        </tr>
                    </table>
                    </div>
            </div>
            <table id="Ins_TB" runat="server" class="tb_1" style="width: 450px" visible="false" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="tb_title_w_2" colspan="7" ><img src="Images/exe.GIF" />
                        風險衝擊性 | 新增資料 &nbsp;<asp:Button ID="Insert_Btn" runat="server" CssClass="button-small"
                            Text="新增資料" /></td>
                </tr>
                <tr>
                    <td class="tb_title_w_1">
                        等級</td>
                    <td class="tb_title_w_1">
                        衝擊</td>
                    <td class="tb_title_w_1">
                        衝擊性評估表準</td>
                </tr>
                <tr>
                    <td class="tb_w_1">
                           <asp:TextBox ID="impact_score_ins" runat="server" Width="30px" MaxLength="2"></asp:TextBox></td>
                    <td class="tb_w_1">
                          <asp:TextBox ID="impact_desc_ins" runat="server" Width="80px" MaxLength="256"></asp:TextBox></td>
                    <td class="tb_w_1">
                        <asp:TextBox ID="impact_std_ins" runat="server" MaxLength="256" TextMode="MultiLine"
                            Width="300px" Height="100px"></asp:TextBox></td>
                </tr>
            </table>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="impact_score_ins"
                ErrorMessage="分數請輸入數值0到99" MaximumValue="99" MinimumValue="0" Display="None"></asp:RangeValidator>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="impact_score_upd"
                        ErrorMessage="分數請輸入數值0到99" MaximumValue="99" MinimumValue="0" Display="None"></asp:RangeValidator></div>
    
    </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <asp:HiddenField ID="Hidden_sn" runat="server" />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RangeValidator1">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RangeValidator2">
        </cc1:ValidatorCalloutExtender>
    </form>
</body>
</html>
