﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO
Partial Class AttachDoc_list
    Inherits PageBase
    'Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim uid As String = User.Identity.Name '使用者名稱
    Dim uname As String = Common.Get_User_Name(uid) '使用者代號
    Dim apprvid As String
    Dim approvalid As String
    Dim dp As String = Me.Get_DP
    Dim File_Path As String = ConfigurationManager.AppSettings("AttachDoc_Path")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        approvalid = ConfigurationManager.AppSettings("flow_AttachDoc") '8
        apprvid = ConfigurationManager.AppSettings("flow_AttachDoc") '8
        Try
            If Not IsPostBack Then
                txb_sdate.Text = Common.Year(Now.AddDays(-30))
                txb_edate.Text = Common.Year(Now.AddDays(1))
                SqlDataSource()
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '申請清單
    Protected Sub SqlDataSource()
        Dim sql As String = "  SELECT * FROM AttachDoc where empUid='" + uid + "'  and using='Y'  and empUid in (select user_id from ismsUser where using='Y') " '排除已撤銷之申請單'排除停用帳號
        If txb_sdate.Text <> "" Then
            sql += " and applyDate >= '" + Common.YearYYYY(txb_sdate.Text) + "'"
        End If
        If txb_edate.Text <> "" Then
            sql += " and applyDate <= '" + Common.YearYYYY(txb_edate.Text) + "'"
        End If
        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = sql
        GridView1.DataSourceID = SqlDataSource1.ID
    End Sub

    '申請清單_RowDataBound
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '民國年轉換
                Dim Button2 As Button = DirectCast(e.Row.FindControl("Button2"), Button)
                Dim lb_applydate As String = CType(e.Row.FindControl("lb_applydate"), Label).Text
                Dim hidden_recordId As String = CType(e.Row.FindControl("hidden_recordId"), HiddenField).Value
                CType(e.Row.FindControl("lb_applydate"), Label).Text = Common.Year(Date.Parse(lb_applydate))
                Dim ds As DataSet = Common.Qry_Process(hidden_recordId)
                If ds.Tables(0).Rows.Count <= 0 Then
                    Button2.Visible = False '只有在第0關才可以撤銷
                End If
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '查詢按鈕
    Protected Sub but_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_search.Click
        SqlDataSource()
    End Sub

    '查看_1及撤銷按鈕
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim GETrecordId As String = e.CommandArgument
        hidden_recordId.Value = GETrecordId
        Select Case e.CommandName

            Case "audit_btn"
                '查看
                Try

                    lb_recordid.Text = hidden_recordId.Value

                    '若不是第0關則不開放修改儲存表 20200804修改 都不開訪
                    'Dim dscheck As DataSet = Common.Qry_Process(GETrecordId)
                    'If dscheck.Tables(0).Rows.Count <= 0 Then
                    but_update.Visible = False
                    images1.Visible = False
                    atdt_Name.Enabled = False
                    comment.Enabled = False
                    'Else
                    'but_update.Visible = True
                    'images1.Visible = True
                    'atdt_Name.Enabled = True
                    'comment.Enabled = True
                    'End If

                    atdt_Name_DataSource() 'RadioButtonList資料來源

                    '資料載入
                    Dim sql = "  SELECT * FROM AttachDoc WHERE recordId = '" + GETrecordId + "'"
                    Dim data As DataTable = Common.Con(sql)
                    If data.Rows.Count > 0 Then
                        applyDate.Text = Common.Year(data.Rows(0)("applyDate"))
                        recordId.Text = GETrecordId
                        empName.Text = data.Rows(0)("empName")
                        atdt_Name.SelectedValue = data.Rows(0)("inp_atdt_Id")
                        comment.Text = data.Rows(0)("comment")
                        images1_link.Text = data.Rows(0)("images1")
                    End If
                    '頁面調整
                    tb_result.Visible = True
                    tb_query.Visible = False
                    GridView1.Visible = False
                    GdvShow() '載入關卡表
                Catch ex As Exception
                    lb_msg.Text = ex.Message
                End Try
            Case "using_btn"
                '撤銷
                Dim sql As New StringBuilder
                sql.Append("update AttachDoc set using='N' where recordId ='" + hidden_recordId.Value + "' ")
                sql.Append("  update form_record set status = -1 where recordId = '" + hidden_recordId.Value + "' and approvalId = '" + approvalid + "' and status = 1")

                Dim conn As SqlConnection = Nothing : Dim cmd As New SqlCommand(sql.ToString)
                Try
                    conn = Common.ConnDBS : conn.Open() : cmd.Connection = conn
                    If cmd.ExecuteNonQuery() Then GridView1.DataBind() : lb_msg.Text = "撤銷成功！"
                Catch ex As Exception
                    lb_msg.Text = "撤銷失敗！" + ex.Message
                Finally
                    conn.Close() : conn.Close() : conn.Dispose() : cmd.Dispose()
                    SqlDataSource() '20200804
                End Try

        End Select

    End Sub

    '1_檔案下載_按鈕
    Protected Sub images1_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles images1_Btn.Click
        Try
            Common.DownFile(Me.Page, File_Path + images1_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_修改儲存_按鈕
    'Protected Sub but_update_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_update.Click
    '    Try
    '        Dim sql = "SELECT * FROM AttachDoc WHERE recordId = '" + recordId.Text + "'"
    '        Dim data As DataTable = Common.Con(sql)
    '        If data.Rows.Count > 0 Then
    '            '如果有資料才做更新如果沒有的話就回到上一頁
    '            If Not (CType(Me.FindControl("images1"), FileUpload).HasFile) Then
    '                '沒有更新上傳檔案
    '                SqlTxt = "UPDATE AttachDoc SET inp_atdt_Id = '" + atdt_Name.SelectedValue + "', inp_atdt_Name = '" + atdt_Name.SelectedItem.Text + "', comment = '" + comment.Text + "', op_time = GETDATE() WHERE recordId = '" + recordId.Text + "'"
    '                SqlCmd = New SqlCommand(SqlTxt, Conn)
    '                Conn.Open()
    '                SqlCmd.ExecuteNonQuery()
    '                Conn.Close()
    '                Common.showMsg(Me.Page, "修改成功!")
    '            Else
    '                '處理上傳檔案
    '                Dim oriFileName = CType(Me.FindControl("images1"), FileUpload)
    '                Dim UpFileName As String = Common.UpFile(File_Path, recordId.Text, CType(Me.FindControl("images1"), FileUpload))
    '                SqlTxt = "UPDATE AttachDoc SET inp_atdt_Id = '" + atdt_Name.SelectedValue + "', inp_atdt_Name = '" + atdt_Name.SelectedItem.Text + "', images1_ori_name = '" + oriFileName.FileName + "', images1 = '" + UpFileName + "', comment = '" + comment.Text + "', op_time = GETDATE() WHERE recordId = '" + recordId.Text + "'"
    '                SqlCmd = New SqlCommand(SqlTxt, Conn)
    '                Conn.Open()
    '                SqlCmd.ExecuteNonQuery()
    '                Conn.Close()
    '                Common.showMsg(Me.Page, "修改成功!")
    '            End If

    '        Else
    '            Response.Redirect("AttachDoc_list", True) '如果沒有這個recordId就重新導向個人申請單
    '        End If

    '    Catch ex As Exception
    '        lb_msg.Text = ex.Message
    '    End Try
    'End Sub

    '1_回列表_按鈕
    Protected Sub but_esc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_esc.Click
        Response.Redirect("AttachDoc_list.aspx", True)
    End Sub

    '表單項目資料來源
    Private Sub atdt_Name_DataSource()
        Try
            Dim sql As String = "SELECT * FROM AttachDocType WHERE atdt_Enabled = 1"
            sql += " ORDER BY atdt_time DESC "
            atdt_Name.Items.Clear()
            atdt_Name.DataTextField = "atdt_Name"
            atdt_Name.DataValueField = "atdt_Id"
            atdt_Name.DataSource = Common.Con(sql)
            atdt_Name.DataBind()
            atdt_Name.SelectedValue = atdt_Name.Items(0).Value '預設選項為第一個值
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '載入關卡表
    Private Sub GdvShow()
        Dim dt As DataTable = Common.Qry_workflowName_isms_Req(approvalid, lb_recordid.Text, apprvid, dp)
        GridView.DataSource = dt
        GridView.DataBind()

    End Sub

    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料

            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            Dim hid_status As HiddenField = CType(e.Row.FindControl("hid_status"), HiddenField)
            '***意見
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text
            '***審核人員***
            'Dim lb_wf_inside_order As String = CType(e.Row.FindControl("lb_wf_inside_order"), Label).Text

            Dim ds As DataSet

            '***Start
            If o_wf_order = "0" And hid_status.Value <> "1" Then
                pan_user.Visible = True
                '將審核人員設為自己
                Dim sql = "    SELECT user_id, user_name FROM ismsUser WHERE user_id = '" + uid + "'"
                Dim data As DataTable = Common.Con(sql)
                CType(e.Row.FindControl("lb_username"), Label).Text = data.Rows(0)("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = data.Rows(0)("user_id")
            ElseIf hid_status.Value = "1" Then '待審核
                ds = Common.Qry_form_record_wait(lb_recordid.Text)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
            ElseIf hid_status.Value = "2" Or hid_status.Value = "9" Then '審核已通過
                ds = Common.Qry_form_record_name(lb_recordid.Text, o_wf_order)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
            End If

            If hid_status.Value = "" Then
                lb_status.Text = ""
            Else
                CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(hid_status.Value))
            End If
        End If
    End Sub

End Class
