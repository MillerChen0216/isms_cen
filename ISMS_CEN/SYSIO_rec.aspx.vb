﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class SYSIO_rec
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim File_Path As String = ConfigurationManager.AppSettings("SYSIO_Path")
    Dim dp As String = Me.Get_DP
    Dim apprvid As String = ConfigurationManager.AppSettings("flow_SYSIO_rec") '11
    Dim uid As String = User.Identity.Name
    Dim uname As String = Common.Get_User_Name(User.Identity.Name.ToString)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        If Not IsPostBack Then
            If Session("success") = "Y" Then
                Common.showMsg(Me.Page, "儲存成功！")
                Session("success") = ""
            End If
            Time_DataSource() '預設進入時間以及離開時間的下拉
            GdvShow() '載入流程表
        End If
    End Sub

    '時間資料來源
    Private Sub Time_DataSource()
        Try
            '進入時間
            Ddl_InHour.Items.Clear()
            Ddl_InMin.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                Ddl_InHour.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                Ddl_InMin.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            '離開時間
            Ddl_OutHour.Items.Clear()
            Ddl_OutMin.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                Ddl_OutHour.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                Ddl_OutMin.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '按鈕_搜尋表單編號日期區間
    Protected Sub Btn_applyDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_applyDate.Click
        Try
            Select Case Session("SYSIO_page")
                Case "SYSIO"
                    GdvrecordId(DateTime.Parse(Common.YearYYYY(sdate.Text)), DateTime.Parse(Common.YearYYYY(edate.Text)))
                Case "SYSPG"
                    GdvTabel3(DateTime.Parse(Common.YearYYYY(sdate.Text)), DateTime.Parse(Common.YearYYYY(edate.Text)))
                Case "Firewall"
                    GdvTabel4(DateTime.Parse(Common.YearYYYY(sdate.Text)), DateTime.Parse(Common.YearYYYY(edate.Text)))
            End Select
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub


    '按鈕_主頁至選擇葉面(0)
    Protected Sub Btn_Choose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Choose.Click
        Try
            GdvrecordId(DateTime.Now.AddMonths(-1), DateTime.Now)
            Table1.Visible = False
            Table2_1.Visible = True
            Table2_2.Visible = True
            Session("SYSIO_page") = "SYSIO"
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '0_載入SYSIO
    Private Sub GdvrecordId(ByVal Startdate As Date, ByRef Enddate As Date)
        Try
            Dim sql As String = "select * from SYSIO where using = 'Y' and applyDate between '" + Startdate.ToString("yyyy/MM/dd") + "' and '" + Enddate.ToString("yyyy/MM/dd") + "' and recordId not in (select distinct SYSIO_recordId from SYSIO_rec where using='Y')"
            sdate.Text = Common.Year(Startdate.ToString("yyyy/MM/dd"))
            edate.Text = Common.Year(Enddate.ToString("yyyy/MM/dd"))
            Dim data As DataTable = Common.Con(sql)
            For Each item In data.Select
                item("empName") = Common.Year(DateTime.Parse(item("applyDate")).ToString("yyyy/MM/dd"))
            Next
            GridView1.DataSource = data
            GridView1.DataBind()

            Session("SYSIO_page") = ""
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '0_選取某表單編號_至主頁
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Try
            Dim GETrecordId As String = e.CommandArgument
            SYSPG_recordId.Text = GETrecordId

            Dim sql = "select * from SYSIO where recordId = '" + GETrecordId + "'"
            Dim data = Common.Con(sql)

            applyDate.Text = Common.Year(data.Rows(0)("applyDate")) '申請日期
            SYSIO_recordId.Text = data.Rows(0)("recordId") '表單編號
            chk_target_1.Checked = If(data.Rows(0)("target_1") = "1", True, False) '維護標的_資訊系統
            chk_target_2.Checked = If(data.Rows(0)("target_2") = "1", True, False) '維護標的_資訊設備
            targetName.Text = data.Rows(0)("targetName") '標的名稱
            applyOrg.Text = data.Rows(0)("applyOrg") '申請單位
            applyName.Text = data.Rows(0)("applyName") '申請人員
            chk_method_1.Checked = If(data.Rows(0)("method_1") = "1", True, False) '維護方式_親至機房
            chk_method_2.Checked = If(data.Rows(0)("method_2") = "1", True, False) '維護方式_親至資訊(電腦)室
            chk_method_3.Checked = If(data.Rows(0)("method_3") = "1", True, False) '維護方式_Internet連線
            chk_method_4.Checked = If(data.Rows(0)("method_4") = "1", True, False) '維護方式_其他
            methodOther.Text = data.Rows(0)("methodOther") '維護方式_其他內容
            chk_location_1.Checked = If(data.Rows(0)("location_1") = "1", True, False) '存放位置_機房
            chk_location_2.Checked = If(data.Rows(0)("location_2") = "1", True, False) '存放位置_辦公(控制)室
            chk_location_3.Checked = If(data.Rows(0)("location_3") = "1", True, False) '存放位置_其他
            locationOther.Text = data.Rows(0)("locationOther") '存放位置_其他內容
            InDate.Text = Common.Year(data.Rows(0)("InDate")) '進出日期
            InTime.Text = data.Rows(0)("InTimeHr") + "：" + data.Rows(0)("InTimeMin") '進出時間_進時'進出時間_進分
            OutTime.Text = data.Rows(0)("OutTimeHr") + "：" + data.Rows(0)("OutTimeMin") '離開時間_出時'離開時間_出分
            applyComment.Text = data.Rows(0)("applyComment") '辦理事項
            chk_IfChangeSoftware.Checked = If(data.Rows(0)("IfChangeSoftware") = "1", True, False) '變更程式
            chk_IfSYSPG.Checked = If(data.Rows(0)("IfSYSPG") = "1", True, False) '軟體程式變更申請單
            chk_IfFirewall.Checked = If(data.Rows(0)("IfFirewall") = "1", True, False) '防火牆維護申請表
            chk_IfTest.Checked = If(data.Rows(0)("IfTest") = "1", True, False) '測試作業之程式
            chk_IfItemIn.Checked = If(data.Rows(0)("IfItemIn") = "1", True, False) '攜入
            chk_IfItemOut.Checked = If(data.Rows(0)("IfItemOut") = "1", True, False) '攜出
            chk_IfItemLocation.Checked = If(data.Rows(0)("IfItemLocation") = "1", True, False) '變更位置

            '載入選擇
            If chk_IfSYSPG.Checked = "1" Then
                Dim sql_related_syspgrecordId = "select * from SYSIO_related where SYSIO_recordId = '" + GETrecordId + "' and kind = 'PG'"
                Dim data_related = Common.Con(sql_related_syspgrecordId)
                If Not data_related.Rows.Count = 0 Then
                    SYSPG_recordId.Text = data_related.Rows(0)("related_recordId") 'SYSPG_recordId
                End If
            Else
                SYSPG_recordId.Text = ""
            End If
            If chk_IfFirewall.Checked = "1" Then
                Dim sql_related_firewallrecordId = "select * from SYSIO_related where SYSIO_recordId = '" + GETrecordId + "' and kind = 'FW'"
                Dim data_related = Common.Con(sql_related_firewallrecordId)
                If Not data_related.Rows.Count = 0 Then
                    Firewall_recordId.Text = data_related.Rows(0)("related_recordId") 'Firewall_recordId
                End If
            Else
                Firewall_recordId.Text = ""
            End If

            '檢測結果
            Dim sql_exam = "  select * from SYSIO_exam where SYSIO_recordId = '" + GETrecordId + "' "
            Dim data_exam = Common.Con(sql_exam)
            If data_exam.Rows.Count = 0 Then
                GdvShow1()
            Else
                GridView2.DataSource = data_exam
                GridView2.DataBind()
                Session("newTable") = data_exam '紀錄
            End If

            '查看裡面是否有資料，有才秀出來
            If IsDBNull(data.Rows(0)("applyFile")) Then
                applyFile.Visible = False
            Else
                applyFile.Visible = True
                applyFile_link.Text = data.Rows(0)("applyFile")
            End If

            Table1.Visible = True
            Table2_1.Visible = False
            Table2_2.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '0_載入SYSIO的檢測結果
    Private Sub GdvShow1()
        Try
            Dim newTable As New DataTable

            newTable.Columns.Add("equipment")
            newTable.Columns.Add("quantity")
            newTable.Columns.Add("result")

            GridView2.DataSource = newTable
            GridView2.DataBind()

        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '按鈕_主頁至軟體程式變更申請單的"選擇"(1)
    Protected Sub btn_SYSPG_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_SYSPG.Click
        Try
            GdvTabel3(DateTime.Now.AddMonths(-1), DateTime.Now) '載入選擇表格的資料(SYSPG)
            Table1.Visible = False
            Table2_1.Visible = True
            Table2_2.Visible = False
            Table3_1.Visible = True
            Table3_2.Visible = False
            Session("SYSIO_page") = "SYSPG"
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_載入SYSPG
    Private Sub GdvTabel3(ByVal Startdate As Date, ByRef Enddate As Date)
        Try
            Dim sql As String = "select * from SYSPG where using = 'Y' and applyDate between '" + Startdate.ToString("yyyy/MM/dd") + "' and '" + Enddate.ToString("yyyy/MM/dd") + "'"
            sdate.Text = Common.Year(Startdate.ToString("yyyy/MM/dd"))
            edate.Text = Common.Year(Enddate.ToString("yyyy/MM/dd"))
            Dim data As DataTable = Common.Con(sql)
            For Each item In data.Select
                item("empName") = Common.Year(DateTime.Parse(item("applyDate")).ToString("yyyy/MM/dd"))
            Next
            GridView3.DataSource = data
            GridView3.DataBind()

            Session("SYSIO_page") = ""
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '按鈕_主頁至房活強維護申請表的"選擇"(2)
    Protected Sub btn_Firewall_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Firewall.Click
        Try
            GdvTabel4(DateTime.Now.AddMonths(-1), DateTime.Now) '載入選擇表格的資料(SYSPG)
            Table1.Visible = False
            Table2_1.Visible = True
            Table2_2.Visible = False
            Table3_1.Visible = False
            Table3_2.Visible = True
            Session("SYSIO_page") = "Firewall"
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '2_載入Firewall
    Private Sub GdvTabel4(ByVal Startdate As Date, ByRef Enddate As Date)
        Try
            Dim sql As String = "select * from Firewall where using = 'Y' and applyDate between '" + Startdate.ToString("yyyy/MM/dd") + "' and '" + Enddate.ToString("yyyy/MM/dd") + "'"
            sdate.Text = Common.Year(Startdate.ToString("yyyy/MM/dd"))
            edate.Text = Common.Year(Enddate.ToString("yyyy/MM/dd"))
            Dim data As DataTable = Common.Con(sql)
            For Each item In data.Select
                item("empName") = Common.Year(DateTime.Parse(item("applyDate")).ToString("yyyy/MM/dd"))
            Next
            GridView4.DataSource = data
            GridView4.DataBind()

            Session("SYSIO_page") = ""
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub



    '1_選取某表單編號_至主頁(SYSPG)
    Protected Sub GridView3_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView3.RowCommand
        Try
            Dim GETrecordId As String = e.CommandArgument
            SYSPG_recordId2.Text = GETrecordId '下面的軟體程式變更申請表編號

            Table1.Visible = True
            Table2_1.Visible = False
            Table2_2.Visible = False
            Table3_1.Visible = False
            Table3_2.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '2_選取某表單編號_至主頁(Firewall)
    Protected Sub GridView4_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView4.RowCommand
        Try
            Dim GETrecordId As String = e.CommandArgument
            Firewall_recordId2.Text = GETrecordId '下面的防火牆維護申請表編號

            Table1.Visible = True
            Table2_1.Visible = False
            Table2_2.Visible = False
            Table3_1.Visible = False
            Table3_2.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '載入流程表
    Private Sub GdvShow()
        Try
            Dim dt As DataTable = Common.Qry_workflowName_isms(apprvid, dp)
            GridView.DataSource = dt
            GridView.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub



    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料

            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            '***意見
            Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text

            '***Start
            Select Case o_wf_order
                Case "0"
                    '人員
                    pan_user.Visible = True : ddl_user.Visible = False : lb_userid.Text = uid
                    '狀態
                    ddl_status.Visible = True : lb_status.Visible = False
                    ddl_status.DataSource = Common.Get_WFStatus(0)
                    ddl_status.DataValueField = "wfstatus_id" : ddl_status.DataTextField = "wfstatus_name" : ddl_status.DataBind()
                    '意見
                    txt_comment.Visible = True : lb_comment.Visible = False
                Case "1"
                    Dim dt_WorkFlow As DataTable = Common.Get_Wf_User1_isms(apprvid, o_wf_order, "")
                    ddl_user.Visible = True : pan_user.Visible = False
                    ddl_user.Items.Clear()
                    ddl_user.DataValueField = "wf_userid" : ddl_user.DataTextField = "user_name"
                    ddl_user.DataSource = dt_WorkFlow : ddl_user.DataBind()
            End Select


            If pan_user.Visible = True And lb_userid.Text <> "" Then
                lb_username.Text = ISMS.Common.notNull(Common.Get_User_Name(lb_userid.Text))
                If lb_username.Text = "" Then lb_username.Text = Common.Get_venderemp_Name(lb_userid.Text) '表示非員工，可能為廠商
            End If
        End If
    End Sub

    '送出審核
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try

            'String.IsNullOrEmpty()
            If Not String.IsNullOrEmpty(actionTime.Text) Then
                Dim inTime = DateTime.Parse(Common.YearYYYY(actionTime.Text + " " + Ddl_InHour.SelectedValue + ":" + Ddl_InMin.SelectedValue))
                Dim outTime = DateTime.Parse(Common.YearYYYY(actionTime.Text + " " + Ddl_OutHour.SelectedValue + ":" + Ddl_OutMin.SelectedValue))
                If inTime > outTime Then
                    Common.showMsg(Me.Page, "離開時間需大於進入時間")
                    Return
                End If
            End If

            If String.IsNullOrEmpty(action.Text) Or String.IsNullOrEmpty(actionTime.Text) Or String.IsNullOrEmpty(SYSIO_recordId.Text) Then
                Common.showMsg(Me.Page, "請確實填寫資訊")
                Return
            End If


            If GridView.Rows.Count < 1 Then
                Common.showMsg(Me.Page, "請先至(系統管理->審核流程管理)設定關卡流程")
                Return
            End If

            Dim recordid As String = "Siorc" + DateTime.Now.ToString("yyyymmddHHmmssfff") '表單編號



            '處理上傳檔案
            If CType(Me.FindControl("File_actionFile"), FileUpload).HasFile Then
                '有檔案
                Dim oriFileName = CType(Me.FindControl("File_actionFile"), FileUpload)
                Dim UpFileName As String = Common.UpFile(File_Path, recordid, CType(Me.FindControl("File_actionFile"), FileUpload))
                '塞入主表資料
                SqlTxt += "  insert into SYSIO_rec (recordId,SYSIO_recordId,empUid,empName," _
                    + "action,actionTime,InTimeHr,InTimeMin,OutTimeHr,OutTimeMin,actionFile_ori_name,actionFile,using,create_time,op_time)" _
                    + "values('" + recordid + "','" + SYSIO_recordId.Text + "','" + uid + "','" + uname + "'," _
                    + "'" + action.Text + "','" + Common.YearYYYY(actionTime.Text) + "','" + Ddl_InHour.SelectedValue + "','" + Ddl_InMin.SelectedValue + "','" + Ddl_OutHour.SelectedValue + "','" + Ddl_OutMin.SelectedValue + "'," _
                    + "'" + oriFileName.FileName + "','" + UpFileName + "','Y',getdate(),getdate())"

            Else
                '沒有檔案
                '塞入主表資料
                SqlTxt += "  insert into SYSIO_rec (recordId,SYSIO_recordId,empUid,empName," _
                    + "action,actionTime,InTimeHr,InTimeMin,OutTimeHr,OutTimeMin,using,create_time,op_time)" _
                    + "values('" + recordid + "','" + SYSIO_recordId.Text + "','" + uid + "','" + uname + "'," _
                    + "'" + action.Text + "','" + Common.YearYYYY(actionTime.Text) + "','" + Ddl_InHour.SelectedValue + "','" + Ddl_InMin.SelectedValue + "','" + Ddl_OutHour.SelectedValue + "','" + Ddl_OutMin.SelectedValue + "','Y',getdate(),getdate())"

            End If

            '處理軟體程式變更申請班編號、防火牆維護申請表編號
            If Not String.IsNullOrEmpty(SYSPG_recordId2.Text) Then
                SqlTxt += "insert into SYSIO_related (SYSIO_recordId,kind,related_recordId,original_SYSIO_recordId)values('" + recordid + "','PG','" + SYSPG_recordId2.Text + "','" + SYSIO_recordId.Text + "')"
            End If

            If Not String.IsNullOrEmpty(Firewall_recordId2.Text) Then
                SqlTxt += "insert into SYSIO_related (SYSIO_recordId,kind,related_recordId,original_SYSIO_recordId)values('" + recordid + "','FW','" + Firewall_recordId2.Text + "','" + SYSIO_recordId.Text + "')"
            End If


            '申請人
            SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype ,dp) values "
            SqlTxt += " ('" + apprvid + "','" + recordid + "','" + CType(GridView.Rows(0).FindControl("wf_order"), Label).Text + "','" + uid + "',"
            SqlTxt += "'" + CType(GridView.Rows(0).FindControl("ddl_status"), DropDownList).SelectedValue + "','" + CType(GridView.Rows(0).FindControl("txb_comment"), TextBox).Text + "',getdate() ,'0', 'vender','" + dp + "') "


            '下一位審核人
            If GridView.Rows.Count >= 2 Then
                SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype ,dp) values "
                SqlTxt += " ('" + apprvid + "','" + recordid + "','" + CType(GridView.Rows(1).FindControl("wf_order"), Label).Text + "','" + CType(GridView.Rows(1).FindControl("ddl_user"), DropDownList).SelectedValue + "',"
                SqlTxt += " '1', '',getdate() ,'1', 'land','" + dp + "') "
            End If

            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Session("success") = "Y"
            Response.Redirect("SYSIO_rec.aspx", True)
            Common.showMsg(Me.Page, "儲存成功！")
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

End Class
