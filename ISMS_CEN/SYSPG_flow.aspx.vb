﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI

Partial Class SYSPG_flow
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlCmd As SqlCommand
    Dim uid As String = User.Identity.Name '使用者名稱
    Dim uname As String = Common.Get_User_Name(User.Identity.Name.ToString) '使用者代號
    Dim SqlTxt As String
    Dim approvalid As String
    Dim apprvid As String
    Dim dp As String = Me.Get_DP
    Dim File_Path As String = ConfigurationManager.AppSettings("SYSPG_Path")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        approvalid = ConfigurationManager.AppSettings("flow_SYSPG") '13
        apprvid = ConfigurationManager.AppSettings("flow_SYSPG") '13
        Gdv1Show() '待審核表
    End Sub

    '待審核表來源
    Protected Sub Gdv1Show()
        Try
            Dim sql = " select * from  form_record a join SYSPG b on  a.recordid=b.recordid where b.using = 'Y' and approvalid='" + approvalid + "' and a.status=1 and userid='" + uid + "' order by b.applyDate"
            Dim data As DataTable = Common.Con(sql)
            GridView1.DataSource = data
            GridView1.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '簽核過程及民國年轉換
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '簽核過程
                Dim recordid As String = CType(e.Row.FindControl("lb_rowid"), Label).Text
                CType(e.Row.FindControl("lb_flow"), Label).Text = Common.Str_form_record_isms(approvalid, recordid)
                '民國年轉換
                Dim lb_applydate As String = CType(e.Row.FindControl("lb_applydate"), Label).Text
                CType(e.Row.FindControl("lb_applydate"), Label).Text = Common.Year(Date.Parse(lb_applydate))
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '時間資料來源
    Private Sub Time_DataSource()
        Try
            Ddl_Hour.Items.Clear()
            Ddl_Min.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                Ddl_Hour.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                Ddl_Min.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '審核表畫面至審核畫面_1
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Try
            Dim Index As Integer = CType(e.CommandArgument, Int32)
            Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
            Dim lb_rowid As String = (TryCast(SelectedRow.FindControl("lb_rowid"), Label)).Text

            Select Case e.CommandName
                Case "audit_btn"
                    Dim sql = "  SELECT * FROM SYSPG WHERE recordId = '" + lb_rowid + "'"
                    Dim data As DataTable = Common.Con(sql)
                    If data.Rows.Count > 0 Then
                        '紀錄該表單編號
                        lb_recordid.Text = lb_rowid
                        '載入關卡表
                        GdvShow()
                        Dim stage As Integer = CType(lb_count.Text, Integer) '找關卡

                        ''填入該lb_rowid資料
                        applyDate.Text = Common.Year(data.Rows(0)("applyDate")) '申請日期
                        recordId.Text = data.Rows(0)("recordId") '表單編號
                        syscd_Name.Text = data.Rows(0)("syscd_Name") '系統名稱
                        sysFunction.Text = data.Rows(0)("sysFunction") '功能名稱
                        applyOrg.Text = data.Rows(0)("applyOrg") '申請單位
                        applyName.Text = data.Rows(0)("applyName") '申請人員
                        Radio_applyReason.SelectedValue = data.Rows(0)("applyReason") '變更原因
                        applyAction.Text = data.Rows(0)("applyAction") '申請變更事項
                        applyPath.Text = data.Rows(0)("applyPath") '程式路徑(詳列變更之程式存放路徑)
                        Time_DataSource()
                        If Not IsDBNull(data.Rows(0)("expectedTime")) Then
                            expectedTime.Text = Common.Year(data.Rows(0)("expectedTime")) '預定完成時間
                            Ddl_Hour.SelectedValue = DateTime.Parse(data.Rows(0)("expectedTime")).ToString("HH") '時間(時)
                            Ddl_Min.SelectedValue = DateTime.Parse(data.Rows(0)("expectedTime")).ToString("mm") '時間(分)
                        Else
                            expectedTime.Text = "" '預定完成時間
                            Ddl_Hour.SelectedValue = Ddl_Hour.Items(0).Value '時間(時)
                            Ddl_Min.SelectedValue = Ddl_Min.Items(0).Value '時間(分)
                        End If

                        Dim workflowId As Integer = Common.Get_workflowId(approvalid, recordId.Text) '取得關卡

                        If workflowId = 0 Then
                            SheetEnabled(True)
                            File_applyFile.Visible = True
                        Else
                            SheetEnabled(False)
                            File_applyFile.Visible = False
                        End If


                        '查看裡面是否有資料，有才秀出來
                        If IsDBNull(data.Rows(0)("applyFile")) Then
                            applyFile.Visible = False
                        Else
                            applyFile.Visible = True
                            applyFile_link.Text = data.Rows(0)("applyFile")
                        End If

                        '將審核畫面弄出來
                        tb_result.Visible = True
                        tr_gridview.Visible = False
                        Insert_Btn.Visible = True
                        but_esc.Visible = True
                    Else
                        Response.Redirect("SYSPG_flow.aspx", True) '重新載入附件表單簽核_審核 因為此表單編號已經不存在
                    End If


            End Select
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '小工具1
    Private Sub SheetEnabled(ByVal use As Boolean)
        sysFunction.Enabled = use
        applyOrg.Enabled = use
        applyName.Enabled = use
        Radio_applyReason.Enabled = use
        applyAction.Enabled = use
        applyPath.Enabled = use
        expectedTime.Enabled = use
        Ddl_Hour.Enabled = use
        Ddl_Min.Enabled = use
    End Sub

    '關卡表
    Private Sub GdvShow()
        Try
            Dim dt As DataTable = Common.Qry_workflowName_isms_Req(approvalid, lb_recordid.Text, apprvid, dp)
            GridView.DataSource = dt
            GridView.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    

    '關卡表_RowDataBound
    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '***已簽核資料

                '***人員
                Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
                Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
                Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
                Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
                '***狀態
                Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
                Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
                Dim hid_status As HiddenField = CType(e.Row.FindControl("hid_status"), HiddenField)
                '***意見
                Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
                Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
                '***關卡***
                Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text
                '***審核人員***
                'Dim lb_wf_inside_order As String = CType(e.Row.FindControl("lb_wf_inside_order"), Label).Text

                Dim ds As DataSet


                '***Start
                If o_wf_order = "0" And hid_status.Value <> "1" Then
                    pan_user.Visible = True
                    '將審核人員設為自己
                    Dim sql = "    SELECT user_id, user_name FROM ismsUser WHERE user_id = '" + uid + "'"
                    Dim data As DataTable = Common.Con(sql)
                    CType(e.Row.FindControl("lb_username"), Label).Text = data.Rows(0)("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = data.Rows(0)("user_id")
                ElseIf hid_status.Value = "1" Then '待審核
                    ds = Common.Qry_form_record_wait(lb_recordid.Text)
                    pan_user.Visible = True
                    CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
                    lb_count.Text = e.Row.RowIndex
                ElseIf hid_status.Value = "2" Or hid_status.Value = "9" Then '審核已通過
                    ds = Common.Qry_form_record_name(lb_recordid.Text, o_wf_order)
                    pan_user.Visible = True
                    CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
                ElseIf e.Row.RowIndex = CType(lb_count.Text, Integer) + 1 Then '待審核下一關卡
                    ddl_user.Visible = True
                    ds = Common.Get_Wf_User(dp, apprvid, o_wf_order, "")
                    ddl_user.DataSource = ds
                    ddl_user.DataTextField = "name_id"
                    ddl_user.DataValueField = "wf_userid"
                    ddl_user.DataBind()
                End If

                If hid_status.Value = "1" Then '待審核
                    ddl_status.Visible = True
                    lb_status.Visible = False
                    txt_comment.Visible = True
                    lb_comment.Visible = False
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(1)
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()

                ElseIf hid_status.Value = "" Then
                    ddl_status.Visible = False
                    lb_status.Visible = True
                    txt_comment.Visible = False
                    lb_comment.Visible = True
                Else
                    ddl_status.Visible = False
                    lb_status.Visible = True
                    txt_comment.Visible = False
                    lb_comment.Visible = True
                    CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(hid_status.Value))
                End If
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '檔案下載_按鈕
    Protected Sub applyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + applyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub


    '返回列表_按鈕
    Protected Sub but_esc_click()
        tr_gridview.Visible = True
        tb_result.Visible = False
        Insert_Btn.Visible = False
        but_esc.Visible = False
        Gdv1Show()
    End Sub

    '送出審核_按鈕
    Protected Sub Insert_Btn_click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim index As Integer = CType(lb_count.Text, Integer)
        Dim max_index As Integer = GridView.Rows.Count

        Dim wf_order As String = CType(GridView.Rows(index).FindControl("wf_order"), Label).Text '待審核的關卡
        Dim status As String = CType(GridView.Rows(index).FindControl("ddl_status"), DropDownList).SelectedValue '狀態
        Dim comment As String = CType(GridView.Rows(index).FindControl("txb_comment"), TextBox).Text
        Dim id As String = CType(GridView.Rows(index).FindControl("lb_id2"), Label).Text
        Dim stage As Integer = Common.Get_workflowId(approvalid, recordId.Text) '取得關卡

        If status = 2 Then '通過
            If index + 1 < GridView.Rows.Count Then '流程尚未到最後關卡

                If stage = 0 Then
                    '回到第0關
                    If String.IsNullOrEmpty(applyDate.Text) Or String.IsNullOrEmpty(applyOrg.Text) Or String.IsNullOrEmpty(applyName.Text) _
                    Or String.IsNullOrEmpty(sysFunction.Text) Or String.IsNullOrEmpty(Radio_applyReason.SelectedValue) Or String.IsNullOrEmpty(applyAction.Text) _
                    Or String.IsNullOrEmpty(applyPath.Text) Or String.IsNullOrEmpty(expectedTime.Text) Then
                        Common.showMsg(Me.Page, "請確實填寫資訊")
                        Return
                    End If
                    Dim Now As String = Common.YearYYYY(applyDate.Text) '日期
                    Dim applyDate_Chn As String = Common.Year(Now).Replace("/", "") '民國YYYMMDD
                    Dim sql = "SELECT * FROM SYSPG WHERE applyDate_Chn like '" + applyDate_Chn.Substring(0, 3) + "%' ORDER BY recordId DESC"
                    Dim data As DataTable = Common.Con(sql)
                    Dim applyDate_Chn_SN = 1
                    If Not (data.Rows.Count = 0) Then
                        applyDate_Chn_SN = Integer.Parse(Right(data.Rows(0)("recordId"), 3)) + 1 'SN
                    End If


                    Dim expected_time As String = "null"
                    '處理預定完成時間
                    If Not String.IsNullOrEmpty(expectedTime.Text) Then
                        expected_time = "'" + Common.YearYYYY(expectedTime.Text) + " " + Ddl_Hour.SelectedValue + ":" + Ddl_Min.SelectedValue + "'"
                    End If


                    '處理上傳檔案
                    If CType(Me.FindControl("File_applyFile"), FileUpload).HasFile Then
                        '有檔案
                        Dim oriFileName = CType(Me.FindControl("File_applyFile"), FileUpload)
                        Dim UpFileName As String = Common.UpFile(File_Path, recordId.Text, CType(Me.FindControl("File_applyFile"), FileUpload))
                        '塞入主表資料
                        SqlTxt = "  update SYSPG set sysFunction = '" + sysFunction.Text + "', applyOrg = '" + applyOrg.Text + "', applyName = '" + applyName.Text + "'," _
                            + "applyReason = '" + Radio_applyReason.SelectedValue + "', applyAction = '" + applyAction.Text + "', applyPath = '" + applyPath.Text + "'," _
                            + "expectedTime = " + expected_time + ", applyFile_ori_name = '" + oriFileName.FileName + "', applyFile = '" + UpFileName + "', op_time = getdate() where recordId = '" + recordId.Text + "'"

                    Else
                        '沒有檔案
                        '塞入主表資料
                        SqlTxt = "  update SYSPG set sysFunction = '" + sysFunction.Text + "', applyOrg = '" + applyOrg.Text + "', applyName = '" + applyName.Text + "'," _
                            + "applyReason = '" + Radio_applyReason.SelectedValue + "', applyAction = '" + applyAction.Text + "', applyPath = '" + applyPath.Text + "'," _
                            + "expectedTime = " + expected_time + ", op_time = getdate() where recordId = '" + recordId.Text + "'"

                    End If

                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                End If


                Dim workflowid As String = CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) + 1
                Dim user_id = CType(GridView.Rows(index + 1).FindControl("ddl_user"), DropDownList).SelectedValue

                SqlTxt = " INSERT INTO form_record (approvalId,workflowId,userId,userType,status,comment,recordId,signDate,role,dp) VALUES "
                SqlTxt += " ('" + approvalid + "','" + workflowid + "','" + user_id + "','land','1','','" + lb_recordid.Text + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + workflowid + "','" + dp + "') "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            End If
            SqlTxt = " UPDATE form_record SET status='2' , signDate ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' , comment = '" + comment + "' WHERE status != 2 and status !=0 and approvalId='" + approvalid + "'and recordId ='" + lb_recordid.Text + "' and id ='" + id + "' "
        ElseIf status = 9 Then '退回
            If CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) > 0 Then '目前流程不是第一關卡
                Dim workflowid As String = CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) - 1
                'Dim user_id = CType(GridView.Rows(index - 1).FindControl("label_userid"), Label).Text
                Dim dt As DataTable = Common.Qry_form_record1(approvalid, lb_recordid.Text, workflowid)
                Dim user_id As String = dt.Rows(0).Item("userId").ToString

                SqlTxt = " INSERT INTO form_record (approvalId,workflowId,userId,userType,status,comment,recordId,signDate,role,dp) VALUES "
                SqlTxt += " ('" + approvalid + "','" + workflowid + "','" + user_id + "','land','1','','" + lb_recordid.Text + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + workflowid + "','" + dp + "') "

                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            Else
                Common.showMsg(Me.Page, "流程已到填表人，無法再退回！")
                Return
            End If

            SqlTxt = " UPDATE form_record SET status='9' , signDate ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' , comment = '" + comment + "' WHERE approvalId='" + approvalid + "'and recordId ='" + lb_recordid.Text + "' and id ='" + id + "' "

        End If
        Dim cmd As New SqlCommand(SqlTxt.ToString)

        Try
            Conn = Common.ConnDBS : Conn.Open() : cmd.Connection = Conn
            If cmd.ExecuteNonQuery() Then Common.showMsg(Me.Page, "審核完成！") : but_esc_click()
        Catch ex As Exception
            Common.showMsg(Me.Page, "審核失敗！")
        Finally
            Conn.Close() : Conn.Close() : Conn.Dispose() : cmd.Dispose()
        End Try
    End Sub
End Class
