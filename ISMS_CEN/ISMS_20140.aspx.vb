﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class ISMS_20140
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand

    Protected Sub SqlDataSource()
        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = "SELECT a.asset_id,a.assgroup_id,a.assgroup_name,a.assgroup_desc,b.asset_name FROM [assetGroup] AS a,[asset] AS b WHERE a.asset_id=b.asset_id AND a.asset_id='" + ddl_Asset.SelectedValue + "' ORDER BY a.asset_id,a.assgroup_id"
        GridView1.DataSourceID = SqlDataSource1.ID
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        If Not Page.IsPostBack Then
            ddl_Asset.DataSource = Common.Get_Asset()
            ddl_Asset.DataValueField = "asset_id"
            ddl_Asset.DataTextField = "asset_name"
            ddl_Asset.DataBind()
        End If
        SqlDataSource()
    End Sub

    Protected Sub Show_Insert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert.Click
        Show_TB.Visible = False
        Ins_TB.Visible = True
        ddl_Asset_ins.DataSource = Common.Get_Asset()
        ddl_Asset_ins.DataValueField = "asset_id"
        ddl_Asset_ins.DataTextField = "asset_name"
        ddl_Asset_ins.DataBind()
    End Sub

    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        If assgroup_id_Ins.Text = "" Then
            Common.showMsg(Me.Page, "分類代碼不允許空白")
        Else
            If assgroup_name_Ins.Text = "" Then
                Common.showMsg(Me.Page, "分類名稱不允許空白")
            Else
                SqlTxt = "INSERT INTO assetGroup (assgroup_id,assgroup_name,asset_id,assgroup_desc) VALUES ('" + assgroup_id_Ins.Text + "' , '" + assgroup_name_Ins.Text + "' ,'" + ddl_Asset_ins.SelectedValue + "','" + assgroup_desc_Ins.Text + "' ) "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", assgroup_name_Ins.Text)

                GridView1.DataBind()
                assgroup_id_Ins.Text = ""
                assgroup_name_Ins.Text = ""
                Show_TB.Visible = True
                Ins_TB.Visible = False

            End If

        End If
    End Sub

    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click
        If assgroup_name_Edit.Text = "" Then
            Common.showMsg(Me.Page, "分類名稱不允許空白")
        Else

            SqlTxt = "UPDATE assetGroup SET  assgroup_name ='" + assgroup_name_Edit.Text + "' , assgroup_desc='" + assgroup_desc_Edit.Text + "' where assgroup_id ='" + Hidden_assgroup_id.Value + "' AND asset_id='" + asset_id_Edit.Text + "'"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

            GridView1.DataBind()
            Show_TB.Visible = True
            Edit_TB.Visible = False
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", assgroup_name_Edit.Text)

        End If
    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)

        Hidden_assgroup_id.Value = SelectedRow.Cells(0).Text


        Select Case e.CommandName

            Case "Del_Btn"
                SqlTxt = "DELETE FROM assetGroup where assgroup_id ='" + Hidden_assgroup_id.Value + "' AND asset_id='" + SelectedRow.Cells(2).Text + "'"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DEL", SelectedRow.Cells(1).Text)
                GridView1.DataBind()


            Case "Edit_Btn"

                Edit_TB.Visible = True
                Show_TB.Visible = False

                assgroup_id_Edit.Text = SelectedRow.Cells(0).Text
                assgroup_name_Edit.Text = SelectedRow.Cells(1).Text
                asset_id_Edit.Text = SelectedRow.Cells(2).Text
                assgroup_desc_Edit.Text = SelectedRow.Cells(4).Text


        End Select
    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim oDataRow As Data.DataRowView
        Dim sShowField As String
        Dim sMessage As String
        If e.Row.RowIndex <> -1 Then
            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = oDataRow.Item("assgroup_name").ToString()
            sMessage = String.Format("您確定要刪除 [{0}] 嗎?", sShowField)
            CType(e.Row.Cells(6).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"
        End If
    End Sub
End Class
