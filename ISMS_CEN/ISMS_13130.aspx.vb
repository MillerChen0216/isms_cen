﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_13130
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer
    Dim uid As String
    Dim uname As String
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim douser As String = ConfigurationManager.AppSettings("douser")
    Dim mgr1 As String = ConfigurationManager.AppSettings("evnet_comgr")
    Dim mgr2 As String = ConfigurationManager.AppSettings("event_mgr")
    Dim mgr3 As String = ConfigurationManager.AppSettings("event_cso")
    Dim mgr4 As String = ConfigurationManager.AppSettings("event_master")
    Dim mgr5 As String = ConfigurationManager.AppSettings("event_mastermgr")
    Dim wf15 As String = ConfigurationManager.AppSettings("wf15")
    Dim wf16 As String = ConfigurationManager.AppSettings("wf16")



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = "pubhq"
        uid = User.Identity.Name.ToString


        'hidden_apprvid.Value = wf16

        'If Not Page.IsPostBack Then
        '    Common.ddl_DataRead("SELECT left(user_id,5) as dp  FROM ismsUser group by left(user_id,5) order by dp desc", ddl_dp, 0, 0)
        '    ddl_douser.Enabled = False
        'ElseIf ddl_dp.SelectedIndex = 0 Then
        '    ddl_douser.Enabled = False
        'End If

        'eventUser_lb.Text = "(" + Common.Get_User_Name(uid) + ")"
        'eventid_lb.Text = uid
        'ddl_eventmgr1.DataSource = Common.Get_User(Hidden_DP.Value)
        'ddl_eventmgr1.DataTextField = "user_name"
        'ddl_eventmgr1.DataValueField = "user_id"
        'ddl_eventmgr1.DataBind()
        'event_email.Text = Common.Get_UserMail(uid)

        SqlDS()
    End Sub



    Protected Sub audit_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Event_TB.Visible = True
        Main_TB.Visible = False
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        Dim Ds As New DataSet
        Ds = Common.Get_event_info(GridView1.Rows(Index).Cells(3).Text)
        hidden_apprvid.Value = Ds.Tables(0).Rows(0).Item("apprv_id")

        eventnum_lb.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_number"))
        eventUser_lb.Text = Common.FixNull(Common.Get_User_Name(Ds.Tables(0).Rows(0).Item("event_user")))
        ddl_eventmgr1.Text = Common.FixNull(Common.Get_User_Name(Ds.Tables(0).Rows(0).Item("eventmgr1")))
        event_tel.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_tel"))
        event_fax.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_fax"))
        event_email.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_email"))
        event_y.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_y"))
        event_m.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_m"))
        event_d.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_d"))
        event_hh.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_hh"))
        event_mm.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_mm"))
        event_y1.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_y1"))
        event_m1.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_m1"))
        event_d1.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_d1"))
        event_hh1.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_hh1"))
        event_mm1.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_mm1"))
        event_desc.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_desc"))
        ddl_douser.Text = Common.FixNull(Common.Get_User_Name(Ds.Tables(0).Rows(0).Item("douser")))
        ddl_dousermgr2.Text = Common.FixNull(Common.Get_User_Name(Ds.Tables(0).Rows(0).Item("dousermgr2")))
        douser_tel.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("douser_tel"))
        douser_email.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("douser_email"))


        If Ds.Tables(0).Rows(0).Item("apprv_id") = "16" Then
            ddl_eventflow.SelectedIndex = 1
            'tb2.Visible = True
            'WF_SqlDS()

        ElseIf Ds.Tables(0).Rows(0).Item("apprv_id") = "15" Then
            ddl_eventflow.SelectedIndex = 2
            'tb2.Visible = True
            'WF_SqlDS()

        End If

        event_server.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_server"))
        event_ip.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_ip"))
        event_domain.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_domain"))
        event_web.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_web"))
        event_host.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_host"))
        event_os.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_os"))
        event_security.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_security"))
        event_cb6_other.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb6_other"))
        event_cb10_other.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb10_other"))
        event_caption.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_caption"))
        event_effect.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_effect"))
        event_method.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_method"))
        event_hw.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_hw"))
        event_pe.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_pe"))
        event_sw.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_sw"))

        evnet_comgr.Text = Common.FixNull(Common.Get_User_Name(Ds.Tables(0).Rows(0).Item("evnet_comgr")))
        event_mgr.Text = Common.FixNull(Common.Get_User_Name(Ds.Tables(0).Rows(0).Item("event_mgr")))
        event_cso.Text = Common.FixNull(Common.Get_User_Name(Ds.Tables(0).Rows(0).Item("event_cso")))
        event_master.Text = Common.FixNull(Common.Get_User_Name(Ds.Tables(0).Rows(0).Item("event_master")))
        event_mastermgr.Text = Common.FixNull(Common.Get_User_Name(Ds.Tables(0).Rows(0).Item("event_mastermgr")))

        If Common.FixNull(Ds.Tables(0).Rows(0).Item("event_type")) <> "" Then
            event_type.SelectedValue = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_type"))
        End If



        If Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb1")) = "True" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb1")) <> "" Then
            event_cb1.Checked = True
        ElseIf Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb1")) = "False" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb1")) <> "" Then
            event_cb1.Checked = False
        End If

        If Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb2")) = "True" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb2")) <> "" Then
            event_cb2.Checked = True
        ElseIf Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb2")) = "False" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb2")) <> "" Then
            event_cb2.Checked = False
        End If

        If Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb3")) = "True" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb3")) <> "" Then
            event_cb3.Checked = True
        ElseIf Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb3")) = "False" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb3")) <> "" Then
            event_cb3.Checked = False
        End If

        If Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb1")) = "True" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb1")) <> "" Then
            event_cb1.Checked = True
        ElseIf Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb1")) = "False" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb1")) <> "" Then
            event_cb1.Checked = False
        End If

        If Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb1")) = "True" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb1")) <> "" Then
            event_cb1.Checked = True
        ElseIf Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb1")) = "False" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb1")) <> "" Then
            event_cb1.Checked = False
        End If

        If Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb4")) = "True" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb4")) <> "" Then
            event_cb4.Checked = True
        ElseIf Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb4")) = "False" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb4")) <> "" Then
            event_cb4.Checked = False
        End If

        If Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb5")) = "True" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb5")) <> "" Then
            event_cb5.Checked = True
        ElseIf Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb5")) = "False" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb5")) <> "" Then
            event_cb5.Checked = False
        End If

        If Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb6")) = "True" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb6")) <> "" Then
            event_cb6.Checked = True
        ElseIf Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb6")) = "False" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb6")) <> "" Then
            event_cb6.Checked = False
        End If

        If Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb7")) = "True" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb7")) <> "" Then
            event_cb7.Checked = True
        ElseIf Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb7")) = "False" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb7")) <> "" Then
            event_cb7.Checked = False
        End If

        If Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb8")) = "True" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb8")) <> "" Then
            event_cb8.Checked = True
        ElseIf Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb8")) = "False" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb8")) <> "" Then
            event_cb8.Checked = False
        End If

        If Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb9")) = "True" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb9")) <> "" Then
            event_cb9.Checked = True
        ElseIf Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb9")) = "False" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb9")) <> "" Then
            event_cb9.Checked = False
        End If

        If Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb10")) = "True" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb10")) <> "" Then
            event_cb10.Checked = True
        ElseIf Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb10")) = "False" And Common.FixNull(Ds.Tables(0).Rows(0).Item("event_cb10")) <> "" Then
            event_cb10.Checked = False
        End If

        Hidden_Formid.Value = GridView1.Rows(Index).Cells(3).Text


    End Sub
    Protected Function Get_eventnum()

        Dim Ds As New DataSet
        Dim evennum As String
        Ds = Common.Get_eventNum(Common.Get_Date(0))
        If Common.FixNull(Ds.Tables(0).Rows(0).Item(0)) = "" Then
            evennum = Common.Get_Date(0).ToString + "01"
        Else
            evennum = Common.Get_Date(0).ToString + Ds.Tables(0).Rows(0).Item(0).ToString + 1
        End If

        Return evennum

    End Function



    Protected Sub SqlDS()

        Flow_DS.ConnectionString = Common.ConnDBS.ConnectionString
        Flow_DS.SelectCommand = "SELECT * FROM EventFormMaster where status ='ISSUE' order by form_id desc"
        GridView1.DataSourceID = Flow_DS.ID
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(3).Visible = False
        e.Row.Cells(4).Visible = False
    End Sub

    Protected Sub WF_SqlDS()
        WF_DS.ConnectionString = Common.ConnDBS.ConnectionString
        WF_DS.SelectCommand = "SELECT * FROM workflowName where apprv_id='" + hidden_apprvid.Value + "' order by wf_order, wf_inside_order"
        GridView3.DataSourceID = WF_DS.ID

    End Sub

    Protected Sub GridView3_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView3.RowCreated
        e.Row.Cells(5).Visible = False

    End Sub


    Protected Sub GridView3_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView3.RowDataBound

        Dim ds, ds1, ds2, ds3, ds4, ds5, ds6 As New DataSet
        Dim form_status, wf_status, wf_user, wf_count, wf_order, wf_user_order As String



        If e.Row.RowIndex <> -1 Then

            ds = Common.Get_wfFormRecord(hidden_apprvid.Value, Hidden_Formid.Value, "")
            ds1 = Common.Get_event_info(Hidden_Formid.Value)
            ds2 = Common.Get_Wf_ApprvUser(Hidden_DP.Value, hidden_apprvid.Value, e.Row.Cells(0).Text)
            ds3 = Common.Get_wfFormRecord(hidden_apprvid.Value, Hidden_Formid.Value, "ASC")
            ds5 = Common.Get_wfRecord(hidden_apprvid.Value, Hidden_Formid.Value, 1)
            ds6 = Common.Get_wfInside_Record(hidden_apprvid.Value, Hidden_Formid.Value, 1, e.Row.Cells(0).Text)

            'If e.Row.RowIndex >= 1 Then

            'ds4 = Common.Get_Wf_User(hidden_apprvid.Value, e.Row.Cells(0).Text, e.Row.Cells(5).Text)
            'CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds4.Tables(0).Rows(0).Item("wf_userid"))
            'CType(e.Row.FindControl("label_userid"), Label).Text = ds4.Tables(0).Rows(0).Item("wf_userid")

            'ElseIf e.Row.RowIndex = 1 And Common.FixNull(ds1.Tables(0).Rows(0).Item("status")) = "FLOW Then" Then

            '    ds4 = Common.Get_Wf_User(hidden_apprvid.Value, e.Row.Cells(0).Text, e.Row.Cells(5).Text)
            '    CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("douser"))
            '    CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("douser")

            If e.Row.RowIndex = 0 And Common.FixNull(ds1.Tables(0).Rows(0).Item("status")) = "FLOW" Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("event_user"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("event_user")


            ElseIf e.Row.RowIndex = 1 And Common.FixNull(ds1.Tables(0).Rows(0).Item("status")) = "FLOW" Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("douser"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("douser")

            ElseIf e.Row.RowIndex >= 2 And Common.FixNull(ds1.Tables(0).Rows(0).Item("status")) = "FLOW" Then

                'ds4 = Common.Get_Wf_User_ALL(hidden_apprvid.Value, e.Row.Cells(0).Text, e.Row.Cells(5).Text)
                'CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds4.Tables(0).Rows(0).Item("wf_userid"))
                'CType(e.Row.FindControl("label_userid"), Label).Text = ds4.Tables(0).Rows(0).Item("wf_userid")

                'ElseIf e.Row.RowIndex = 1 And Common.FixNull(ds1.Tables(0).Rows(0).Item("status")) = "FLOW" Then

                '    CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("douser"))
                '    CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("douser")



            End If


            Try
                wf_status = Common.FixNull(ds.Tables(0).Rows(0).Item("wf_status_id"))
                wf_order = Common.FixNull(ds.Tables(0).Rows(0).Item("wf_order"))
            Catch ex As Exception
            End Try


            form_status = Common.FixNull(ds1.Tables(0).Rows(0).Item("status"))
            wf_user_order = Common.FixNull(ds2.Tables(0).Rows(0).Item("wf_order"))
            wf_user = Common.FixNull(CType(e.Row.FindControl("label_userid"), Label).Text)
            wf_count = Common.FixNull(ds.Tables(0).Rows.Count)

            If wf_count >= 1 Then

                If wf_count = 1 And wf_user_order = 1 And form_status = "FLOW" And ds1.Tables(0).Rows(0).Item("douser") = uid Then

                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("verify_btn"), Button).Visible = True

                ElseIf form_status = "FLOW" And (ds.Tables(0).Rows(0).Item("wf_status_id") = "2" Or ds.Tables(0).Rows(0).Item("wf_status_id") = "0") And wf_count >= 1 And e.Row.RowIndex >= 1 And wf_user_order = ds.Tables(0).Rows(0).Item("wf_order") + 1 And wf_user = uid Then

                    CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = True
                    CType(e.Row.FindControl("comment_txt"), TextBox).Visible = True
                    CType(e.Row.FindControl("verify_btn"), Button).Visible = True
                Else
                    CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = False
                    CType(e.Row.FindControl("comment_txt"), TextBox).Visible = False
                    CType(e.Row.FindControl("verify_btn"), Button).Visible = False

                End If

            Else
                CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = False
                CType(e.Row.FindControl("comment_txt"), TextBox).Visible = False
                CType(e.Row.FindControl("verify_btn"), Button).Visible = False


            End If

            '填上已簽核過的狀態與意見
            If ds6.Tables(0).Rows.Count >= 1 Then
                If ds6.Tables(0).Rows(0).Item("wf_inside_order") = e.Row.Cells(5).Text And e.Row.RowIndex >= 0 And e.Row.RowIndex <= ds5.Tables(0).Rows.Count Then
                    CType(e.Row.FindControl("lb_status"), Label).Visible = True
                    CType(e.Row.FindControl("comment_lb"), Label).Visible = True
                    CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(ds6.Tables(0).Rows(0).Item("wf_status_id")))
                    CType(e.Row.FindControl("comment_lb"), Label).Text = Common.FixNull(ds6.Tables(0).Rows(0).Item("wf_comment"))
                Else
                    CType(e.Row.FindControl("lb_status"), Label).Visible = False
                    CType(e.Row.FindControl("comment_lb"), Label).Visible = False
                End If

            End If


            '狀態選單
            If e.Row.RowIndex >= 1 Then
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(1)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
            Else
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(0)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
            End If



        End If
    End Sub






    Protected Sub verify_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex

        If event_type.SelectedValue = "" Or event_caption.Text = "" Then
            Common.showMsg(Me.Page, "資料請填寫")
        Else

            Dim ds, ds1, ds2, ds3, Ds4 As New DataSet
            Dim form_status As String

            ds = Common.Get_Wf_ApprvUser(Hidden_DP.Value, hidden_apprvid.Value, "")
            Ds4 = Common.Get_WfUser(hidden_apprvid.Value, "")

            SqlTxt = "INSERT INTO wfFormRecord " + _
             "(apprv_id,doc_id,wf_order,wf_userid,wf_status_id,wf_comment,signdate,record_id,wf_inside_order,doc_status) VALUES" + _
            "('" + hidden_apprvid.Value + "','" + Hidden_Formid.Value + "','" + oRow.Cells(0).Text + "','" + CType(GridView3.Rows(Index).FindControl("label_userid"), Label).Text + "','" + CType(GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue + "'," + _
            " '" + CType(GridView3.Rows(Index).FindControl("comment_txt"), TextBox).Text + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','1','" + oRow.Cells(5).Text + "','EVENTFORM')"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()


            ds1 = Common.Get_event_info(Hidden_Formid.Value)
            ds2 = Common.Get_WorkFlow_MaxOrderId(hidden_apprvid.Value, Hidden_DP.Value)
            form_status = ds1.Tables(0).Rows(0).Item("status")


            If form_status = "FLOW" Then


                If CType(Me.GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue = "2" And oRow.Cells(0).Text = ds2.Tables(0).Rows(0).Item("wf_order") Then



                    SqlTxt = "UPDATE EventFormMaster  SET status = 'ISSUE' WHERE form_id='" + Hidden_Formid.Value + "' "
                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()

                    ' Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", Hidden_Formid.Value, System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "RISKPUBLISH", lb_riskname.Text)

                    'Mail通知()
                    'ds3 = Common.Get_Wf_ApprvUser(5, oRow.Cells(0).Text + 1)
                    'ds3 = Common.Get_User
                    'Common.SendMail(ds1.Tables(0).Rows(i).Item("creator"), "Workflow_OK", docNum_LB.Text, docName_LB.Text)
                    'For i = 0 To ds3.Tables(0).Rows.Count - 1
                    'Common.SendMail(ds1.Tables(0).Rows(0).Item("add_user"), "Workflow_RISK_OK", "", lb_riskname.Text)
                    'Next


                    'ElseIf CType(Me.GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue = "2" And ds.Tables(0).Rows.Count - 1 > Index Then

                    'SqlTxt = "UPDATE riskEvaluateMaster  SET Issue_date='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',form_status = 'RISK_ISSUE' WHERE form_id='" + Hidden_Formid.Value + "' "

                    'Mail通知()
                    'ds3 = Common.Get_Wf_ApprvUser(5, oRow.Cells(0).Text + 1)
                    'ds3 = Common.Get_User
                    'Common.SendMail(ds1.Tables(0).Rows(i).Item("creator"), "Workflow_OK", docNum_LB.Text, docName_LB.Text)
                    'For i = 0 To ds3.Tables(0).Rows.Count - 1
                    'Common.SendMail(ds3.Tables(0).Rows(0).Item("wf_userid"), "Workflow_RISK1", "", lb_riskname.Text)
                    'Next



                ElseIf CType(Me.GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue = "9" Then

                    SqlTxt = "UPDATE EventFormMaster  SET status = 'REJECT' WHERE form_id='" + Hidden_Formid.Value + "' "
                    'Mail通知
                    'Common.SendMail(ds1.Tables(0).Rows(0).Item("add_user"), "Workflow_RISK_KO", "", lb_riskname.Text)
                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()

                End If


            End If
            tb2.Visible = False
            Main_TB.Visible = True
            Event_TB.Visible = False

        End If


    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound


        'Dim oDataRow As Data.DataRowView
        ''Dim Login_id, Sign_id As String

        'If e.Row.RowIndex <> -1 Then

        '    oDataRow = CType(e.Row.DataItem, Data.DataRowView)

        '    Dim Ds, Ds1, Ds2, Ds3 As New DataSet
        '    Ds2 = Common.Get_event_info(e.Row.Cells(3).Text)

        '    Ds = Common.Get_wfFormRecord(Ds2.Tables(0).Rows(0).Item("apprv_id"), e.Row.Cells(3).Text, "")
        '    Ds1 = Common.Get_Wf_ApprvUser(Ds2.Tables(0).Rows(0).Item("apprv_id"), "")

        '    'If Ds.Tables(0).Rows.Count = 0 And (Common.FixNull(Ds2.Tables(0).Rows(0).Item("douser")) = LCase(uid)) Then
        '    '    CType(e.Row.FindControl("audit_btn"), Button).Visible = True
        '    'End If


        '    If Ds.Tables(0).Rows.Count >= 1 And Ds2.Tables(0).Rows(0).Item("status") = "FLOW" And (Common.FixNull(Ds2.Tables(0).Rows(0).Item("douser")) = LCase(uid)) Then
        '        'Login_id = LCase(uid)
        '        'Ds3 = Common.Get_Wf_ApprvUser(Ds2.Tables(0).Rows(0).Item("apprv_id"), Ds.Tables(0).Rows(0).Item("wf_order") + 1)
        '        'For i = 0 To Ds3.Tables(0).Rows.Count - 1
        '        '    Sign_id = Ds3.Tables(0).Rows(i).Item("wf_userid")
        '        '    If Login_id = Sign_id Then
        '        CType(e.Row.FindControl("audit_btn"), Button).Visible = True
        '        'End If

        '        'Next


        '    End If
        'End If
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_eventflow.SelectedIndexChanged


        'save_btn.Visible = True
        'tb2.Visible = True
        'If ddl_eventflow.SelectedValue = "2" Then
        '    tb2.Visible = True
        '    hidden_apprvid.Value = wf15
        '    SqlTxt = "UPDATE EventFormMaster  SET apprv_id='" + hidden_apprvid.Value + "' WHERE form_id='" + Hidden_Formid.Value + "' "
        '    SqlCmd = New SqlCommand(SqlTxt, Conn)
        '    Conn.Open()
        '    SqlCmd.ExecuteNonQuery()
        '    Conn.Close()
        '    SqlTxt = "UPDATE wfFormRecord  SET apprv_id=" + hidden_apprvid.Value + "  WHERE doc_id='" + Hidden_Formid.Value + "' "
        '    SqlCmd = New SqlCommand(SqlTxt, Conn)
        '    Conn.Open()
        '    SqlCmd.ExecuteNonQuery()
        '    Conn.Close()
        '    WF_SqlDS()
        'ElseIf ddl_eventflow.SelectedValue = "1" Then
        '    tb2.Visible = True
        '    hidden_apprvid.Value = wf16
        '    SqlTxt = "UPDATE EventFormMaster  SET apprv_id='" + hidden_apprvid.Value + "' WHERE form_id='" + Hidden_Formid.Value + "' "
        '    SqlCmd = New SqlCommand(SqlTxt, Conn)
        '    Conn.Open()
        '    SqlCmd.ExecuteNonQuery()
        '    Conn.Close()
        '    SqlTxt = "UPDATE wfFormRecord  SET apprv_id=" + hidden_apprvid.Value + "  WHERE doc_id='" + Hidden_Formid.Value + "' "
        '    SqlCmd = New SqlCommand(SqlTxt, Conn)
        '    Conn.Open()
        '    SqlCmd.ExecuteNonQuery()
        '    Conn.Close()
        '    WF_SqlDS()
        'ElseIf ddl_eventflow.SelectedValue = "0" Then
        '    tb2.Visible = False
        '    save_btn.Visible = False
        'End If
    End Sub

    Protected Sub save_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles save_btn.Click

        'Dim Ds4 As New DataSet
        'Ds4 = Common.Get_WfUser(hidden_apprvid.Value, "")

        'If ddl_eventflow.SelectedValue = "2" Then

        '    For i = 0 To Ds4.Tables(0).Rows.Count - 1

        '        If Ds4.Tables(0).Rows(i).Item("chk_name") = mgr1 Then
        '            SqlTxt = "UPDATE EventFormMaster  SET evnet_comgr= '" + Ds4.Tables(0).Rows(i).Item("wf_userid") + "'   WHERE form_id='" + Hidden_Formid.Value + "' "
        '            SqlCmd = New SqlCommand(SqlTxt, Conn)
        '            Conn.Open()
        '            SqlCmd.ExecuteNonQuery()
        '            Conn.Close()
        '        ElseIf Ds4.Tables(0).Rows(i).Item("chk_name") = mgr2 Then
        '            SqlTxt = "UPDATE EventFormMaster  SET event_mgr= '" + Ds4.Tables(0).Rows(i).Item("wf_userid") + "'    WHERE form_id='" + Hidden_Formid.Value + "' "
        '            SqlCmd = New SqlCommand(SqlTxt, Conn)
        '            Conn.Open()
        '            SqlCmd.ExecuteNonQuery()
        '            Conn.Close()


        '        ElseIf Ds4.Tables(0).Rows(i).Item("chk_name") = mgr3 Then
        '            SqlTxt = "UPDATE EventFormMaster  SET event_cso = '" + Ds4.Tables(0).Rows(i).Item("wf_userid") + "'  WHERE form_id='" + Hidden_Formid.Value + "' "
        '            SqlCmd = New SqlCommand(SqlTxt, Conn)
        '            Conn.Open()
        '            SqlCmd.ExecuteNonQuery()
        '            Conn.Close()

        '        ElseIf Ds4.Tables(0).Rows(i).Item("chk_name") = mgr4 Then
        '            SqlTxt = "UPDATE EventFormMaster  SET event_master = '" + Ds4.Tables(0).Rows(i).Item("wf_userid") + "'  WHERE form_id='" + Hidden_Formid.Value + "' "
        '            SqlCmd = New SqlCommand(SqlTxt, Conn)
        '            Conn.Open()
        '            SqlCmd.ExecuteNonQuery()
        '            Conn.Close()

        '        ElseIf Ds4.Tables(0).Rows(i).Item("chk_name") = mgr5 Then
        '            SqlTxt = "UPDATE EventFormMaster  SET event_mastermgr = '" + Ds4.Tables(0).Rows(i).Item("wf_userid") + "'  WHERE form_id='" + Hidden_Formid.Value + "' "
        '            SqlCmd = New SqlCommand(SqlTxt, Conn)
        '            Conn.Open()
        '            SqlCmd.ExecuteNonQuery()
        '            Conn.Close()
        '        End If

        '    Next

        'End If


        'SqlTxt = "UPDATE EventFormMaster  SET event_server='" + event_server.Text + "',event_ip = '" + event_ip.Text + "', event_domain='" + event_domain.Text + "', event_web='" + event_web.Text + "',event_host='" + event_host.Text + "',event_os='" + event_os.Text + "',event_security='" + event_security.Text + "',event_type='" + event_type.SelectedValue + "',event_cb1='" + event_cb1.Checked.ToString + "',event_cb2='" + event_cb2.Checked.ToString + "',event_cb3='" + event_cb3.Checked.ToString + "',event_cb4='" + event_cb4.Checked.ToString + "',event_cb5='" + event_cb5.Checked.ToString + "',event_cb6='" + event_cb6.Checked.ToString + "',event_cb7='" + event_cb7.Checked.ToString + "',event_cb8='" + event_cb8.Checked.ToString + "',event_cb9='" + event_cb9.Checked.ToString + "',event_cb10='" + event_cb10.Checked.ToString + "',event_cb6_other='" + event_cb6_other.Text + "',event_cb10_other='" + event_cb10_other.Text + "',event_caption='" + event_caption.Text + "',event_effect='" + event_effect.Text + "',event_method='" + event_method.Text + "',event_hw='" + event_hw.Text + "',event_pe='" + event_pe.Text + "',event_sw='" + event_sw.Text + "' WHERE form_id='" + Hidden_Formid.Value + "' "
        ''Response.Write(SqlTxt)
        ''Response.End()
        'SqlCmd = New SqlCommand(SqlTxt, Conn)
        'Conn.Open()
        'SqlCmd.ExecuteNonQuery()
        'Conn.Close()
        'Common.showMsg(Me.Page, "資料儲存成功，但尚未送審!!")
    End Sub
End Class
