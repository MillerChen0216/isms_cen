﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class SYSCode
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim uid As String = User.Identity.Name '使用者名稱
    Dim uname As String = Common.Get_User_Name(uid) '使用者代號

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        If Not IsPostBack Then
            EditType.SelectedValue = "0" : EditType_selectedchange(sender, e) '預設為新增
        End If
    End Sub

    'RadioButton_SlectedChange (2來源)(3按鈕)
    Protected Sub EditType_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Select Case EditType.SelectedValue
                Case "0"
                    '新增
                    otr.Visible = False
                    ntr1.Visible = True
                    ntr2.Visible = True
                    ntr3.Visible = True

                    Btn_Action.Visible = False '確定
                    Btn_Save.Visible = True '儲存

                    TextBox_Code.Text = ""
                    TextBox_Name.Text = ""
                    TextBox_Remarks.Text = ""
                Case "1"
                    '修改
                    otr.Visible = True
                    ntr1.Visible = False
                    ntr2.Visible = True
                    ntr3.Visible = True

                    Btn_Action.Visible = False '確定
                    Btn_Save.Visible = True '儲存

                    Ddl_Name_DataSource() '更新下拉


                    Dim sql2 = "SELECT * FROM SysCode WHERE syscd_Id = '" + Ddl_Name.SelectedValue + "'"
                    Dim data As DataTable = Common.Con(sql2)
                    TextBox_Name.Text = data.Rows(0)("syscd_Name")
                    TextBox_Remarks.Text = data.Rows(0)("syscd_Remarks")
                Case "2"
                    '刪除
                    otr.Visible = True
                    ntr1.Visible = False
                    ntr2.Visible = False
                    ntr3.Visible = False

                    Btn_Action.Visible = True '確定 (只有刪除是確定鈕)
                    Btn_Save.Visible = False '儲存

                    Ddl_Name_DataSource() '更新下拉
                Case Else
                    '沒這可能
            End Select
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    'RadioButton_SlectedChange (2來源)(3按鈕)
    Protected Sub Ddl_Name_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim sql2 = "SELECT * FROM SysCode WHERE syscd_Id = '" + Ddl_Name.SelectedValue + "'"
            Dim data As DataTable = Common.Con(sql2)
            TextBox_Name.Text = data.Rows(0)("syscd_Name")
            TextBox_Remarks.Text = data.Rows(0)("syscd_Remarks")
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '(2)系統名稱下拉來源
    Public Sub Ddl_Name_DataSource()
        Try
            Dim sql As String = "SELECT syscd_Code+'-'+syscd_Name as syscd_Name,syscd_Id,syscd_time FROM SysCode WHERE syscd_Enabled = 1"
            sql += " order by syscd_time desc "
            Ddl_Name.Items.Clear()
            Ddl_Name.DataTextField = "syscd_Name"
            Ddl_Name.DataValueField = "syscd_Id"
            Ddl_Name.DataSource = Common.Con(sql)
            Ddl_Name.DataBind()

        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '(3)儲存(包含新增跟更新系統名稱)
    Protected Sub EditBtn_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Save.Click
        Try
            If (otr.Visible) Then
                '更新
                If Not (String.IsNullOrEmpty(TextBox_Name.Text)) Then '確認 系統代號及名稱不得空白
                    SqlTxt = "UPDATE SysCode set syscd_Name = '" + TextBox_Name.Text + "',syscd_Remarks = '" + TextBox_Remarks.Text + "' where syscd_Id = '" + Ddl_Name.SelectedValue + "'"
                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                    Common.showMsg(Me.Page, "更新系統名稱!")
                Else
                    Common.showMsg(Me.Page, "系統名稱不得空白!")
                End If
            Else
                '新增
                If Not (String.IsNullOrEmpty(TextBox_Code.Text)) And Not (String.IsNullOrEmpty(TextBox_Name.Text)) Then '確認 系統代號及名稱不得空白
                    Dim sql = "SELECT * FROM SysCode WHERE syscd_Enabled = 1 AND syscd_Code = '" + TextBox_Code.Text.ToUpper() + "'"
                    Dim data As DataTable = Common.Con(sql)
                    If data.Rows.Count = 0 Then '確認該名稱目前沒有被使用
                        SqlTxt = "INSERT INTO SysCode (syscd_Id ,syscd_Code ,syscd_Name ,syscd_Remarks ,syscd_Enabled, syscd_userId, syscd_time) values ('syscd" + Date.Now.ToString("yyyyMMddHHmmssfff") + "','" + TextBox_Code.Text.ToUpper() + "','" + TextBox_Name.Text + "','" + TextBox_Remarks.Text + "',1,'" + uname + "',getdate())"
                        SqlCmd = New SqlCommand(SqlTxt, Conn)
                        Conn.Open()
                        SqlCmd.ExecuteNonQuery()
                        Conn.Close()
                        Common.showMsg(Me.Page, "新增系統名稱!")
                        EditType_selectedchange(sender, e) '更新頁面
                    Else
                        Common.showMsg(Me.Page, "系統代號不得重複!")
                    End If
                Else
                    Common.showMsg(Me.Page, "系統代號及名稱不得空白!")
                End If
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '(3)確定(包含刪除系統名稱)
    Protected Sub EditBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Action.Click
        Try
            '刪除(直接更新Enable狀態20200807確認狀態是否有被使用)
            Dim sql = "  select * from SYSPG where syscd_Id = '" + Ddl_Name.SelectedValue + "'"
            Dim data = Common.Con(sql)
            If data.Rows.Count = 0 Then
                SqlTxt = " UPDATE SysCode set syscd_Enabled = 0 where syscd_Id = '" + Ddl_Name.SelectedValue + "'"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.showMsg(Me.Page, "刪除系統名稱!")
                EditType_selectedchange(sender, e) '更新頁面
            Else
                Common.showMsg(Me.Page, "該系統名稱已被使用!")
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '(3)取消
    Protected Sub EditBtn_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Cancel.Click
        Response.Redirect("SYSCode.aspx", True) '重新載入附件表單維護
    End Sub

End Class
