﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class ChPwd
    Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click


        If pwdtxt1.Text = "" Or pwdtxt2.Text = "" Then
            Common.showMsg(Me.Page, "密碼欄位不得為空白，謝謝！")
            pwdtxt1.Focus()
        ElseIf pwdtxt1.Text <> pwdtxt2.Text Then
            Common.showMsg(Me.Page, "兩次密碼輸入不一樣，請檢查，謝謝！")
            pwdtxt1.Focus()
        Else
            SqlTxt = "UPDATE ismsUser  SET  pwd ='" + Common.Encode(pwdtxt2.Text) + "',update_date = '" + DateTime.Now.ToString("yyyyMMdd") + "' where user_id = '" + useridtxt.Text + "' "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", "", "APPLICATION", "EDIT", useridtxt.Text + "使用者密碼")
            Common.showMsg(Me.Page, "密碼修改成功，請下次登入系統使用新設定密碼，謝謝！")

        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        useridtxt.Text = User.Identity.Name.ToString
    End Sub
End Class
