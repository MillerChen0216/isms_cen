Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Public Class PageBase
    Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim uid As String = User.Identity.Name '�ϥΪ̱b��

    Public Function Get_DP() As String

        Dim selectCMD As SqlCommand = New SqlCommand(" Select b.dp From ismsUser a, ctlGroup b Where a.group_id = b.group_id and a.user_id = '" + uid + "'", Conn)
        Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
        mycmd.SelectCommand = selectCMD
        Dim myDs As DataSet = New DataSet()
        Dim dp As String
        mycmd.Fill(myDs, "isms_User")

        If myDs.Tables("isms_User").Rows.Count = 0 Then
            dp = ""
        Else
            dp = myDs.Tables("isms_User").Rows(0).Item("dp")
        End If
        Return dp

    End Function

    Public Function Get_approvalid(ByVal sURL As String, ByVal dp As String) As String

        Dim selectCMD As SqlCommand = New SqlCommand(" select a.apprv_id,a.approvalId from Process_Record a left outer join Form_Group b on a.approvalId = b.approvalId WHERE  b.dp='" + dp + "' and b.sURL = '" + sURL + "'", Conn)
        Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
        mycmd.SelectCommand = selectCMD
        Dim myDs As DataSet = New DataSet()
        Dim approvalId As String
        mycmd.Fill(myDs, "Process_Record")

        If myDs.Tables("Process_Record").Rows.Count = 0 Then
            approvalId = "0"
        Else
            approvalId = myDs.Tables("Process_Record").Rows(0).Item("approvalId")
        End If
        Return approvalId

    End Function

    Public Function Get_apprvid(ByVal sURL As String, ByVal dp As String) As String

        Dim selectCMD As SqlCommand = New SqlCommand(" select a.apprv_id,a.approvalId from Process_Record a left outer join Form_Group b on a.approvalId = b.approvalId WHERE b.dp ='" + dp + "' and b.sURL = '" + sURL + "'", Conn)
        Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
        mycmd.SelectCommand = selectCMD
        Dim myDs As DataSet = New DataSet()
        Dim apprv_id As String
        mycmd.Fill(myDs, "Process_Record")

        If myDs.Tables("Process_Record").Rows.Count = 0 Then
            apprv_id = "0"
        Else
            apprv_id = myDs.Tables("Process_Record").Rows(0).Item("apprv_id")
        End If
        Return apprv_id

    End Function

    Public Sub Check_users()
        Dim Ds As New DataSet
        'Ds = Common.Check_Power(User.Identity.Name, Right(Request.ServerVariables("url").Trim(), 15)) 
        Ds = Common.Check_Power(User.Identity.Name, GetFileName())
        Dim DP As String
        DP = User.Identity.Name.ToString

        If Ds.Tables(0).Rows.Count = 0 Then
            FormsAuthentication.SignOut()
            Response.Redirect("Login.aspx")
        End If

    End Sub

    Protected Function GetFileName() As String
        Dim FileString As String = Request.CurrentExecutionFilePath
        Dim Length As Integer = 0
        Length = FileString.LastIndexOf("/")
        Dim GetFileName_s As String = FileString.Substring(Length + 1, FileString.Length - Length - 1)
        Return GetFileName_s.ToLower()
    End Function
End Class
