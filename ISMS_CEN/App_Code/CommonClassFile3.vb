Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Security.Cryptography
Imports System.IO
Imports System.Web.UI.Page
Imports System.Net.Mail


Namespace ISMS
    Partial Public Class Common
        Inherits System.Web.UI.UserControl
        Function ChkValue(ByVal str As String) As String
            Return HttpContext.Current.Server.HtmlEncode(str)
        End Function

        Public Function Get_max_sysid() '授權範圍最大編號

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT ISNULL(max(sys_id)+1,1) as sys_id FROM auth_sys ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim max_sysid As String
            mycmd.Fill(myDs, "auth_sys")

            If myDs.Tables("auth_sys").Rows.Count = 0 Then
                max_sysid = ""
            Else
                max_sysid = myDs.Tables("auth_sys").Rows(0).Item("sys_id")
            End If
            Return max_sysid

        End Function
        Public Function Get_max_idtid() '權限身份最大編號

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT ISNULL(max(idt_id)+1,1) as idt_id FROM auth_sys_idt ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim max_sysid As String
            mycmd.Fill(myDs, "auth_sys_idt")

            If myDs.Tables("auth_sys_idt").Rows.Count = 0 Then
                max_sysid = ""
            Else
                max_sysid = myDs.Tables("auth_sys_idt").Rows(0).Item("idt_id")
            End If
            Return max_sysid

        End Function

        Public Function Get_max_bureau_apply_id() '地政局系統使用者帳號申請表最大編號

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT ISNULL(max(bureau_apply_id)+1,1) as bureau_apply_id FROM bureau_apply ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim max_sysid As String
            mycmd.Fill(myDs, "bureau_apply")

            If myDs.Tables("bureau_apply").Rows.Count = 0 Then
                max_sysid = ""
            Else
                max_sysid = myDs.Tables("bureau_apply").Rows(0).Item("bureau_apply_id")
            End If
            Return max_sysid

        End Function
        Public Function Get_max_office_apply_id() '地所端系統使用者帳號申請表最大編號

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT ISNULL(max(office_apply_id)+1,1) as office_apply_id FROM office_apply ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim max_sysid As String
            mycmd.Fill(myDs, "office_apply")

            If myDs.Tables("office_apply").Rows.Count = 0 Then
                max_sysid = ""
            Else
                max_sysid = myDs.Tables("office_apply").Rows(0).Item("office_apply_id")
            End If
            Return max_sysid

        End Function

        Public Function Get_max_b_office_apply_id() '局端審查地所使用者帳號申請表最大編號

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT ISNULL(max(b_office_apply_id)+1,1) as b_office_apply_id FROM b_office_apply ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim max_sysid As String
            mycmd.Fill(myDs, "b_office_apply")

            If myDs.Tables("b_office_apply").Rows.Count = 0 Then
                max_sysid = ""
            Else
                max_sysid = myDs.Tables("b_office_apply").Rows(0).Item("b_office_apply_id")
            End If
            Return max_sysid

        End Function

        Public Function Get_bureau_apply(ByVal bureau_apply_id As String) '取得地政局系統使用者帳號申請表
            Dim sql As String = ""
            sql += " SELECT  a.*,c.dept_name,b.degree_name "
            sql += " FROM bureau_apply a "
            sql += " left outer join degree b on a.degree_id=b.degree_id "
            sql += " left outer join deptment c on a.dept_id = c.dept_id "
            sql += " where bureau_apply_id=" + bureau_apply_id

            Dim selectCMD As SqlCommand = New SqlCommand(sql, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "bureau_apply")
            Return myDs
        End Function

        Public Function Get_bureau_apply_byUser(ByVal bureau_apply_id As String, ByVal dp As String)
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" SELECT  a.*,c.dept_name,b.degree_name ")
            sql.Append(" FROM bureau_apply a ")
            sql.Append(" left outer join degree b on a.degree_id=b.degree_id ")
            sql.Append(" left outer join deptment c on a.dept_id = c.dept_id ")
            sql.Append(" where apply_reason in ('02','03') and using='Y' and a.dp = '" + dp + "'")
            If bureau_apply_id.Length <> 0 Then
                sql.Append(" and bureau_apply_id in (" + bureau_apply_id + ") ")
            End If
            '排除停用帳號
            sql.Append(" and user_id in (select user_id from ismsUser where using='Y') ")
            sql.Append(" order by apply_date desc  ")
            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "bureau_apply")
            Return myDs
        End Function
        Public Function Get_bureau_apply_byrecordid(ByVal recordid As String, ByVal dp As String) '增
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" SELECT  a.*,c.dept_name,b.degree_name ")
            sql.Append(" FROM bureau_apply a ")
            sql.Append(" left outer join degree b on a.degree_id=b.degree_id ")
            sql.Append(" left outer join deptment c on a.dept_id = c.dept_id ")
            'sql.Append(" where apply_reason in ('02','03') and using='Y' and a.dp = '" + dp + "'")
            sql.Append(" where  using='Y' and a.dp = '" + dp + "'")
            If recordid.Length <> 0 Then
                sql.Append(" and a.recordid = '" + recordid + "'")
            End If
            '排除停用帳號
            sql.Append(" and user_id in (select user_id from ismsUser where using='Y') ")
            sql.Append(" order by apply_date desc  ")
            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "bureau_apply")
            Return myDs
        End Function
        Public Function Get_bureau_apply_dtl(ByVal bureau_apply_id As String, ByVal sys_id As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM dbo.bureau_apply_dtl where bureau_apply_id='" + bureau_apply_id + "' and sys_id='" + sys_id + "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "auth_sys")
            Return myDs
        End Function
        Public Function Get_bureau_apply_dtl2(ByVal user_id As String, ByVal sys_id As String)
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" SELECT TOP 1 sys_id,idt_name,rmk FROM bureau_apply A,bureau_apply_dtl B ")
            sql.Append(" WHERE A.bureau_apply_id = B.bureau_apply_id AND A.user_id='" + user_id + "' AND B.sys_id='" + sys_id + "' ")
            sql.Append(" AND using ='Y ' and apply_date = ( select max(apply_date) from bureau_apply where using='Y' and user_id='" + user_id + "') ")
            sql.Append(" ORDER BY apply_date DESC ")
            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "auth_sys")
            Return myDs
        End Function

        Public Function Get_bureau_apply_dtl_byUser(ByVal bureau_apply_id As String, ByVal sys_id As String)
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" select TOP 1 * ")
            sql.Append(" from bureau_apply a ")
            sql.Append(" left outer join dbo.bureau_apply_dtl b on a.bureau_apply_id=b.bureau_apply_id ")
            sql.Append(" where  apply_reason in ('02','03') and using='Y' ")
            'sql.Append(" and apply_date = ( select max(apply_date) from bureau_apply where user_id = '" + user_id + "' )  ")
            sql.Append(" and sys_id='" + sys_id + "' ")
            sql.Append(" and a.bureau_apply_id='" + bureau_apply_id + "'")
            '排除停用帳號
            sql.Append(" and user_id in (select user_id from ismsUser where using='Y') ")
            sql.Append(" order by apply_date desc ")

            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "bureau_apply")
            Return myDs
        End Function

        Public Function Get_office_apply(ByVal office_apply_id As String) '取得地所端系統使用者帳號申請表
            Dim sql As String = ""
            sql += " SELECT  a.*,c.dept_name,b.degree_name "
            sql += " FROM office_apply a "
            sql += " left outer join degree b on a.degree_id=b.degree_id "
            sql += " left outer join deptment c on a.dept_id = c.dept_id "
            sql += " where office_apply_id=" + office_apply_id

            Dim selectCMD As SqlCommand = New SqlCommand(sql, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "office_apply")
            Return myDs
        End Function
        Public Function Get_office_apply_byUser(ByVal office_apply_id As String, ByVal dp As String) '取得地所端系統使用者帳號申請表年度檢討相關資料(申請原因為「新進人員」、「職務異動」之所有表單) 
            Dim yyymmdd As String = (Now.Year - 1911).ToString.PadLeft(3, "0") + DateTime.Now.ToString("MMdd")
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" SELECT  a.*,c.dept_name,b.degree_name ")
            sql.Append(" FROM office_apply a ")
            sql.Append(" left outer join degree b on a.degree_id=b.degree_id ")
            sql.Append(" left outer join deptment c on a.dept_id = c.dept_id ")
            sql.Append(" where apply_reason in ('02','03') and using='Y' and a.dp = '" + dp + "' ")
            If office_apply_id.Length <> 0 Then
                sql.Append(" and office_apply_id in (" + office_apply_id + ") ")
            End If
            '排除停用帳號
            sql.Append(" and user_id in (select user_id from ismsUser where using='Y') ")

            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "office_apply")
            Return myDs
        End Function

        Public Function Get_office_apply_byrecordid(ByVal recordid As String, ByVal dp As String) '增
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" SELECT  a.*,c.dept_name,b.degree_name ")
            sql.Append(" FROM office_apply a ")
            sql.Append(" left outer join degree b on a.degree_id=b.degree_id ")
            sql.Append(" left outer join deptment c on a.dept_id = c.dept_id ")
            'sql.Append(" where apply_reason in ('02','03') and using='Y' and a.dp = '" + dp + "'")
            sql.Append(" where  using='Y' and a.dp = '" + dp + "'")
            If recordid.Length <> 0 Then
                sql.Append(" and a.recordid = '" + recordid + "'")
            End If
            '排除停用帳號
            sql.Append(" and user_id in (select user_id from ismsUser where using='Y') ")
            sql.Append(" order by apply_date desc  ")
            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "office_apply")
            Return myDs
        End Function

        Public Function Get_office_apply_dtl(ByVal office_apply_id As String, ByVal sys_id As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM dbo.office_apply_dtl where office_apply_id='" + office_apply_id + "' and sys_id='" + sys_id + "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "auth_sys")
            Return myDs
        End Function
        Public Function Get_office_apply_dtl2(ByVal user_id As String, ByVal sys_id As String)
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" SELECT TOP 1 sys_id,idt_name,rmk FROM office_apply A,office_apply_dtl B ")
            sql.Append(" WHERE A.office_apply_id = B.office_apply_id AND A.user_id='" + user_id + "' AND B.sys_id='" + sys_id + "' ")
            sql.Append(" AND using ='Y ' and apply_date = ( select max(apply_date) from office_apply where using='Y' and  user_id='" + user_id + "') ")
            sql.Append(" ORDER BY apply_date DESC ")
            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "auth_sys")
            Return myDs
        End Function
        Public Function Get_office_apply_dtl_byUser(ByVal office_apply_id As String, ByVal sys_id As String)
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" select TOP 1 * ")
            sql.Append(" from office_apply a ")
            sql.Append(" left outer join office_apply_dtl b on a.office_apply_id=b.office_apply_id ")
            sql.Append(" where  apply_reason in ('02','03')  and using='Y' ")
            sql.Append(" and sys_id='" + sys_id + "' ")
            sql.Append(" and a.office_apply_id='" + office_apply_id + "'")
            '排除停用帳號
            sql.Append(" and user_id in (select user_id from ismsUser where using='Y') ")
            sql.Append(" order by apply_date desc ")

            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "office_apply")
            Return myDs
        End Function
        Public Function Get_b_office_apply(ByVal b_office_apply_id As String) '取得地所端系統使用者帳號申請表
            Dim sql As String = ""
            sql += " SELECT  a.*,c.dept_name,b.degree_name "
            sql += " FROM b_office_apply a "
            sql += " left outer join degree b on a.degree_id=b.degree_id "
            sql += " left outer join deptment c on a.dept_id = c.dept_id "
            sql += " where b_office_apply_id=" + b_office_apply_id

            Dim selectCMD As SqlCommand = New SqlCommand(sql, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "b_office_apply")
            Return myDs
        End Function
        Public Function Get_b_office_apply_byUser(ByVal b_office_apply_id As String, ByVal dp As String) '取得年度檢討相關資料(申請原因為「新進人員」、「職務異動」之所有表單) 
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" SELECT  a.*,c.dept_name,b.degree_name ")
            sql.Append(" FROM b_office_apply a ")
            sql.Append(" left outer join degree b on a.degree_id=b.degree_id ")
            sql.Append(" left outer join deptment c on a.dept_id = c.dept_id ")
            sql.Append(" where apply_reason in ('02','03') and using='Y' and a.dp ='" + dp + "' ")
            If b_office_apply_id.Length <> 0 Then
                sql.Append(" and b_office_apply_id in (" + b_office_apply_id + ") ")
            End If
            '排除停用帳號
            sql.Append(" and user_id in (select user_id from ismsUser where using='Y') ")

            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "b_office_apply")
            Return myDs
        End Function
        Public Function Get_b_office_apply_byrecordid(ByVal recordid As String) '增
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" SELECT  a.*,c.dept_name,b.degree_name ")
            sql.Append(" FROM b_office_apply a ")
            sql.Append(" left outer join degree b on a.degree_id=b.degree_id ")
            sql.Append(" left outer join deptment c on a.dept_id = c.dept_id ")
            'sql.Append(" where apply_reason in ('02','03') and using='Y'")
            sql.Append(" where   using='Y'")
            If recordid.Length <> 0 Then
                sql.Append(" and a.recordid = '" + recordid + "'")
            End If
            '排除停用帳號
            sql.Append(" and user_id in (select user_id from ismsUser where using='Y') ")
            sql.Append(" order by apply_date desc  ")
            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "b_office_apply")
            Return myDs
        End Function
        Public Function Get_b_office_apply_dtl(ByVal b_office_apply_id As String, ByVal sys_id As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM dbo.b_office_apply_dtl where b_office_apply_id='" + b_office_apply_id + "' and sys_id='" + sys_id + "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "b_office_apply_dtl")
            Return myDs
        End Function
        Public Function Get_b_office_apply_dtl2(ByVal user_id As String, ByVal sys_id As String)
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" SELECT TOP 1 sys_id,idt_name,rmk FROM b_office_apply A,b_office_apply_dtl B ")
            sql.Append(" WHERE A.b_office_apply_id = B.b_office_apply_id AND A.user_id='" + user_id + "' AND B.sys_id='" + sys_id + "' ")
            sql.Append(" AND using ='Y ' and apply_date = ( select max(apply_date) from b_office_apply where using='Y' and user_id='" + user_id + "') ")
            sql.Append(" ORDER BY apply_date DESC ")
            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "auth_sys")
            Return myDs
        End Function

        Public Function Get_b_office_apply_dtl_byUser(ByVal b_office_apply_id As String, ByVal sys_id As String)
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" select TOP 1 * ")
            sql.Append(" from b_office_apply a ")
            sql.Append(" left outer join b_office_apply_dtl b on a.b_office_apply_id=b.b_office_apply_id ")
            sql.Append(" where  apply_reason in ('02','03') and using='Y' ")
            sql.Append(" and sys_id='" + sys_id + "' ")
            sql.Append(" and a.b_office_apply_id='" + b_office_apply_id + "'")
            '排除停用帳號
            sql.Append(" and user_id in (select user_id from ismsUser where using='Y') ")
            sql.Append(" order by apply_date desc ")

            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "b_office_apply")
            Return myDs
        End Function

        Public Function Get_auth_sys(ByVal auth_type As String, ByVal dp As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM auth_sys where using ='Y' and auth_type='" + auth_type + "' and dp = '" + dp + "' order by sys_id ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "auth_sys")
            Return myDs
        End Function

        Public Function Get_auth_sys_idt(ByVal sys_id As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM  auth_sys_idt where sys_id='" + sys_id + "' order by idt_id ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "auth_sys_idt")
            Return myDs
        End Function

        Public Function Get_auth_sys_note(ByVal auth_type As String, ByVal dp As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM  auth_sys_note where auth_type='" + auth_type + "'and dp ='" + dp + "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "auth_sys_note")
            Return myDs
        End Function

        Public Function Get_approval(ByVal apprv_id As String) As String
            Dim apprv_name As String = "" : Dim rd As SqlDataReader = Nothing : Dim conn As New SqlConnection
            Dim cmd As New SqlCommand("SELECT * FROM approval where apprv_id=@apprv_id")
            cmd.Parameters.Add("@apprv_id", SqlDbType.VarChar).Value = apprv_id
            Try
                conn = ConnDBS() : cmd.Connection = conn : conn.Open() : rd = cmd.ExecuteReader
                If rd.Read Then apprv_name = notNull(rd.Item("apprv_name"))
                Return apprv_name
            Catch ex As Exception
                Throw ex
            Finally
                conn.Close() : conn.Dispose() : rd.Close() : cmd.Dispose()
            End Try
        End Function
        Public Sub DownFile(ByVal WebForm As System.Web.UI.Page, ByVal File_Name As String, Optional ByVal FileName As String = "")
            Try



                WebForm.Response.ContentType = "application/octet-stream"
                WebForm.Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5")
                Dim fileContents As Byte() = System.IO.File.ReadAllBytes(File_Name)
                WebForm.Response.AddHeader("Content-Disposition", [String].Format("attachment; filename={0}", File_Name))
                WebForm.Response.OutputStream.Write(fileContents, 0, fileContents.Length)
                WebForm.Response.Flush()
                WebForm.Response.End()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function UpFile(ByVal File_Path As String, ByVal UpName As String, ByVal Upload As FileUpload, Optional ByVal R_path As Boolean = False) As String
            Try
                Dim FileExten As String = System.IO.Path.GetExtension(Upload.PostedFile.FileName)
                UpFile = File_Path & UpName & FileExten.ToLower

                '將上傳的檔案儲存
                If (Upload.HasFile) Then
                    '20170708 修正file 判斷邏輯
                    If File.Exists(UpFile) Then File.Delete(UpFile)
                    Upload.PostedFile.SaveAs(UpFile)
                    If R_path = False Then UpFile = UpName & FileExten.ToLower '回傳檔案不加路徑
                Else
                    UpFile = ""
                End If
            Catch ex As Exception
                Dim j As String = ex.Message
                ' Throw ex
            End Try
        End Function

        Public Shared Function notNull(ByRef s As Object) As String
            If IsDBNull(s) Then
                Return ""
            ElseIf s Is Nothing Then
                Return ""
            ElseIf s.ToString.Trim().Length() = 0 Then
                Return ""
            End If

            Return s.ToString.Trim()

        End Function
        Public Function SendMail3(ByVal user_id As String, ByVal action As String, ByVal approval_name As String, Optional ByVal assetMidName As String = "") As Boolean
            Try
                Dim Subject As String = ""
                Dim Body As String = ""
                Dim UserName As String = notNull(Get_User_Name(user_id))
                If UserName = "" Then UserName = notNull(Get_venderemp_Name(user_id)) '外部廠商
                'Dim approval_name As String = Get_approval(apprvid)
                Select Case action

                    Case "Workflow"
                        Subject = "[審核]  " & approval_name & "   審核通知"
                        Body = "" + UserName + "您好，<br><br>請您登入ISMS系統後，點選［ " & approval_name & " \  " & approval_name & "審核］進行審核 。<br>"
                        If assetMidName <> "" Then Body += "<br>專案名稱：" + assetMidName + "<br>"
                    Case "Workflow_KO"
                        Subject = "(不同意)  " & approval_name & "  審核通知"
                        Body = "" + UserName + "您好，<br><br>您所提送的 " & approval_name & "已被退件，請您登入ISMS系統確認 。"

                    Case "Workflow_OK"
                        Subject = "(同意)  " & approval_name & "  審核通知"
                        Body = "" + UserName + "您好，<br><br>您所提送的 " & approval_name & "已經同意，請您登入ISMS系統確認 。"

                End Select
                Body += "<br>謝謝。<br><br>請勿回覆此系統通知郵件!"



                SendMail1(Subject, Body, user_id)

            Catch ex As Exception

                Throw ex

            End Try
        End Function

        Public Function Qry_MID_asset_sub_asset_mid() As DataTable
            Dim sql As String = "select id,name from form_prj where valid='1' order by ordering,id "
            Dim selectCMD As SqlCommand = New SqlCommand(sql, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function

        Public Function SendMail1(ByVal Subject As String, ByVal Body As String, ByVal User_id As String) As Boolean
            Try
                Dim msg As New MailMessage()
                Dim UserMail As String = notNull(Get_UserMail(User_id))
                If UserMail = "" Then UserMail = notNull(Get_venderemp_Mail(User_id)) '外部廠商
                If UserMail = "" Then Exit Function
                'Dim UserMail As String = "oce1_3@seed.net.tw"
                msg.From = New Net.Mail.MailAddress(Sys_Config("Admin_Mail"), "ISMS系統通知")
                msg.To.Add(New Net.Mail.MailAddress(UserMail, "ISMS使用者"))
                msg.IsBodyHtml = True
                msg.Subject = Subject
                msg.Body = Body


                'Dim smtp As New SmtpClient(System.Web.Configuration.WebConfigurationManager.AppSettings("SmtpHost"))
                'smtp.Send(msg)


                If FixNull(Sys_Config("SMTP_Host")) = "" Then

                Else

                    Dim smtp As New System.Net.Mail.SmtpClient(Sys_Config("SMTP_Host"))

                    If FixNull(Sys_Config("SMTP_User")) = "" Then
                        smtp.Send(msg)
                    Else
                        smtp.Credentials = New System.Net.NetworkCredential(Sys_Config("SMTP_User"), Sys_Config("SMTP_PWD"))
                        smtp.Send(msg)

                    End If
                End If


            Catch ex As Exception

                '  Throw ex

            End Try
        End Function

        Public Function Get_venderemp_Mail(ByVal name As String) As String
            Dim selectCMD As SqlCommand = New SqlCommand("select * from vender_emp  where username='" & name & "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Dim dt As DataTable = myDs.Tables(0)
            If dt.Rows.Count = 0 Then
                Get_venderemp_Mail = ""
            Else
                Get_venderemp_Mail = notNull(dt.Rows(0)("mail"))
            End If
        End Function

        Public Function Get_venderemp_Name(ByVal name As String) As String
            Dim selectCMD As SqlCommand = New SqlCommand("select * from vender_emp  where username='" & name & "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Dim dt As DataTable = myDs.Tables(0)
            If dt.Rows.Count = 0 Then
                Get_venderemp_Name = ""
            Else
                Get_venderemp_Name = notNull(dt.Rows(0)("givenname"))
            End If
        End Function
        Public Function Get_Wf_User1_isms(ByVal app_id As String, ByVal wf_order As String, ByVal wf_inside As String) As DataTable '取得WF使用者名稱 order
            Dim selectCMD As SqlCommand
            If wf_inside <> "" Then
                selectCMD = New SqlCommand("SELECT  b.apprv_id, isnull(b.wf_userid,'') as wf_userid, b.wf_order, isnull(c.user_name ,wf_userid) as user_name  , convert(varchar(10),wf_userid)+convert(varchar(1),wf_inside_order) a FROM  workflow b left JOIN  ismsUser  c ON b.wf_userid = c.user_id where b.apprv_id='" + app_id + "' and b.wf_order='" + wf_order + "' and b.wf_inside_order = '" + wf_inside + "' order by wf_inside_order ", ConnDBS)
            Else
                selectCMD = New SqlCommand("SELECT  b.apprv_id, isnull(b.wf_userid,'') as wf_userid, b.wf_order, isnull(c.user_name,wf_userid)  as user_name , convert(varchar(10),wf_userid)+convert(varchar(1),wf_inside_order) a  FROM  workflow b left JOIN    ismsUser   c ON b.wf_userid = c.user_id where b.apprv_id='" + app_id + "' and b.wf_order='" + wf_order + "' order by wf_inside_order ", ConnDBS)
            End If
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "WFUser")
            Return myDs.Tables(0)
        End Function
        Public Function Qry_form_record1(ByVal approvalid As String, ByVal recordid As String, ByVal workflowid As String) As DataTable
            Dim sql As New StringBuilder
            sql.Append("select * from  form_record  where approvalid='" & approvalid & "' and  recordid='" & recordid & "' and workflowid='" & workflowid & "' ")
            Dim cmd As New SqlCommand(sql.ToString, ConnDBS)

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter(cmd)

            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function
        Public Function Qry_form_record(ByVal approvalid As String, ByVal recordid As String, ByVal role As String) As DataTable

            Dim sql As New StringBuilder
            sql.Append("  select * from form_record  where approvalid='" & approvalid & "' and recordid='" & recordid & "' and status<>9 and role<'" & role & "' order by signdate desc ")



            Dim cmd As New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = cmd
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function
        Public Function Str_form_record_isms(ByVal recordid As String) As String
            Dim sql As New StringBuilder : Str_form_record_isms = ""

            sql.Append("  select case when (select top 1 givenname from vender_emp where username=userid) is null  then isnull( (select top 1 user_name from  ismsuser where  userid=user_id), userid)  else isnull((select top 1 givenname from vender_emp where username=userid),userid)  end  +case status when 0 then '(填寫)' when 1 then '(等待)' when 2 then '(通過)' when 9 then '(退回)'  when 3 then '(結束)' else '' end  as flow from form_record  where recordid='" & recordid & "' order by signdate,id asc   ")
            'sql.Append(" select case when isnull(user_name,'')='' then isnull((select givenname from vender_emp where username=userid),'') else user_name end  +case status when 0 then '(填寫)' when 1 then '(等待)' when 2 then '(通過)' when 9 then '(退回)'  when 3 then '(結束)' else '' end  as flow from form_record left join [98ISMS].dbo.ismsuser on  userid=user_id  where approvalid='" & approvalid & "' and recordid='" & recordid & "'  order by signdate ")
            Dim cmd As New SqlCommand(sql.ToString, ConnDBS)

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = cmd
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            If myDs.Tables(0).Rows.Count > 0 Then
                With myDs.Tables(0)
                    For i As Integer = 0 To .Rows.Count - 1
                        Str_form_record_isms += .Rows(i)("flow")
                        If i <> .Rows.Count - 1 Then Str_form_record_isms += " -> "
                    Next
                End With
            End If
        End Function
        Public Function Str_form_record_isms_chkname(ByVal recordid As String, ByVal approvalid As String) As String
            Dim sql As New StringBuilder : Str_form_record_isms_chkname = ""

            sql.Append(" select distinct signdate,id,chk_name,status,case status when 0 then '(填寫)' when 1 then '(等待)' when 2 then '(通過)' when 9 then '(退回)'     ")
            sql.Append(" when 3 then '(結束)' else '' end  as type   from form_record a,workflowName b,Process_Record c  ")
            sql.Append(" where a.dp = b.dp and a.workflowId = b.wf_order and a.approvalId ='" + approvalid + "' and c.approvalId ='" + approvalid + "' and b.apprv_id = c.apprv_id and recordid='" + recordid + "'  order by signdate,id,chk_name asc  ")


            Dim cmd As New SqlCommand(sql.ToString, ConnDBS)

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = cmd
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            If myDs.Tables(0).Rows.Count > 0 Then
                With myDs.Tables(0)
                    For i As Integer = 0 To .Rows.Count - 1
                        Str_form_record_isms_chkname += .Rows(i)("chk_name") + .Rows(i)("type")
                        If i <> .Rows.Count - 1 Then Str_form_record_isms_chkname += " -> "
                    Next
                End With
            End If
        End Function
        Public Function Str_wfFormRecord_isms_chkname(ByVal doc_id As String, ByVal record_id As String) As DataSet
            'Dim sql As New StringBuilder : Str_wfFormRecord_isms_chkname = ""
            Dim sql As New StringBuilder

            sql.Append(" SELECT distinct a.apprv_id ,b.dp,a.wf_order,signdate,sn,chk_name,wf_status_id ,case wf_status_id when 0 then '(填寫)' when 1 then '(等待)' when 2 then '(通過)' when 9 then '(退回)'  ")
            sql.Append(" when 3 then '(結束)' else '' end  as type FROM wfFormRecord a,docResoure b, workflowName c  ")
            sql.Append(" WHERE a.doc_id=b.doc_id and c.dp=b.dp and  a.doc_id='" + doc_id + "' and record_id='" + record_id + "' and c.apprv_id=b.apprv_id and a.wf_order = c.wf_order  order by signdate,sn,chk_name asc  ")


            Dim cmd As New SqlCommand(sql.ToString, ConnDBS)

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = cmd
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs
            
        End Function
        Public Function Str_form_record_isms(ByVal approvalid As String, ByVal recordid As String) As String
            Dim sql As New StringBuilder : Str_form_record_isms = ""

            sql.Append("  select case when (select top 1 givenname from vender_emp where username=userid) is null  then isnull( (select top 1 user_name from  ismsuser where  userid=user_id), userid)  else isnull((select top 1 givenname from vender_emp where username=userid),userid)  end  +case status when 0 then '(填寫)' when 1 then '(等待)' when 2 then '(通過)' when 9 then '(退回)'  when 3 then '(結束)' else '' end  as flow from form_record  where approvalid='" & approvalid & "' and recordid='" & recordid & "' order by signdate,id asc   ")
            'sql.Append(" select case when isnull(user_name,'')='' then isnull((select givenname from vender_emp where username=userid),'') else user_name end  +case status when 0 then '(填寫)' when 1 then '(等待)' when 2 then '(通過)' when 9 then '(退回)'  when 3 then '(結束)' else '' end  as flow from form_record left join [98ISMS].dbo.ismsuser on  userid=user_id  where approvalid='" & approvalid & "' and recordid='" & recordid & "'  order by signdate ")
            Dim cmd As New SqlCommand(sql.ToString, ConnDBS)

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = cmd
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            If myDs.Tables(0).Rows.Count > 0 Then
                With myDs.Tables(0)
                    For i As Integer = 0 To .Rows.Count - 1
                        Str_form_record_isms += .Rows(i)("flow")
                        If i <> .Rows.Count - 1 Then Str_form_record_isms += " -> "
                    Next
                End With
            End If
        End Function
        Public Function Qry_ctrlrm_use(ByVal empid As String, ByVal Sdate As String, ByVal Edate As String) As DataTable
            Dim Sql As String = "select * from ctrlrm_use where empid='" & empid & "'  "
            If Not String.IsNullOrEmpty(Sdate) Then Sql += " and convert(varchar(10),enterDate,111)>= '" & Sdate & "' "
            If Not String.IsNullOrEmpty(Edate) Then Sql += " and convert(varchar(10),enterDate,111) <= '" & Edate & "' "
            Sql += " order by enterDate desc "
            Dim selectCMD As SqlCommand = New SqlCommand(Sql, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function
        Public Function Qry_ctrlrm_use(ByVal recordid As String) As DataTable
            Dim Sql As String = "select * from ctrlrm_use  where recordid='" & recordid & "'  order by enterDate desc "
            Dim selectCMD As SqlCommand = New SqlCommand(Sql, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function
        Public Function Qry_remote_connect(ByVal empUid As String, ByVal Sdate As String, ByVal Edate As String) As DataTable
            Dim Sql As String = "select * from remote_connect where empUid='" & empUid & "'  "
            If Not String.IsNullOrEmpty(Sdate) Then Sql += " and convert(varchar(10),connectStart,111)>= '" & Sdate & "' "
            If Not String.IsNullOrEmpty(Edate) Then Sql += " and convert(varchar(10),connectEnd ,111) <= '" & Edate & "' "
            Sql += " order by applydate desc "
            Dim selectCMD As SqlCommand = New SqlCommand(Sql, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function
        Public Function Qry_remote_connect1(ByVal vender As String, ByVal Sdate As String, ByVal Edate As String) As DataTable
            Dim Sql As String = "select * from remote_connect where empUid=empUid "
            If Not String.IsNullOrEmpty(vender) Then Sql += " and  vender='" & vender & "'  "
            If Not String.IsNullOrEmpty(Sdate) Then Sql += " and convert(varchar(10),connectStart,111)>= '" & Sdate & "' "
            If Not String.IsNullOrEmpty(Edate) Then Sql += " and convert(varchar(10),connectEnd ,111) <= '" & Edate & "' "
            Sql += " order by applydate desc "
            Dim selectCMD As SqlCommand = New SqlCommand(Sql, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function

        Public Function Qry_remote_connect_wfFormRecord(ByVal approvalid As String, ByVal uid As String) As DataTable
            Dim selectCMD As SqlCommand = New SqlCommand("select * from  form_record a join remote_connect b on  a.recordid=b.recordid where approvalid='" & approvalid & "' and a.status=1 and userid='" & uid & "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function


        Public Function Qry_remote_connect(ByVal recordid As String) As DataTable
            Dim selectCMD As SqlCommand = New SqlCommand("select *,isnull(images,'') images1 from remote_connect where recordid='" & recordid & "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function

        Public Function Qry_SUB_asset_sub_asset_mid(ByVal id As String) As DataTable
            Dim cmd As SqlCommand = New SqlCommand()
            cmd.Connection = ConnDBS()
            Dim sql As String = ""
            If String.IsNullOrEmpty(CStr(id)) Then
                sql += "  select subs_id,name from form_sub where valid=1 "
            Else
                sql = "select subs_id,name from form_sub where valid=1 and form_prjid=@id  "
                cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = ChkValue(id)
            End If
            sql += "  order by 1,2 "
            cmd.CommandText = sql
            Dim da As New SqlDataAdapter(cmd)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Function
        Public Function Qry_vender_emp(ByVal name As String) As DataTable
            Dim selectCMD As SqlCommand = New SqlCommand("select * from vender_emp a left join vender b on a.venderid=b.id where a.username='" & name & "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function
        Public Function Qry_holder_asset_sub(ByVal id As Integer) As DataTable
            Dim cmd As SqlCommand = New SqlCommand()
            cmd.Connection = ConnDBS()
            cmd.CommandText = "select holder,* from form_sub where subs_id=@id"
            cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = ChkValue(id)
            Dim da As New SqlDataAdapter(cmd)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Function
        Public Function Qry_vender_contract(ByVal name As String) As DataTable

            Dim cmd As New SqlCommand("select top 1 contractYear, contractNo, contractName from ctrlrm_use where empId='" & name & "' order by recordId desc", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = cmd
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function
        Public Function Qry_workflowName_isms(ByVal apprvid As String, ByVal dp As String) As DataTable
            'Dim sql As New StringBuilder
            'Sql.Append(" SELECT id as chk_id,apprvid as apprv_id,wfOrder as wf_order,name as chk_name, 1 wf_inside_order FROM ISMS.dbo.workflow_Name where apprvid='" + apprvid + "' order by wforder ")
            'Dim cmd As New SqlCommand(sql.ToString, ConnDBS)

            'Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            'mycmd.SelectCommand = cmd
            'Dim myDs As DataSet = New DataSet()
            'mycmd.Fill(myDs, "dt")
            'Return myDs.Tables(0)
            Dim cmd As SqlCommand = New SqlCommand()
            cmd.Connection = ConnDBS()
            ' cmd.CommandText = " SELECT chk_id,apprv_id,wf_order,chk_name, 1 wf_inside_order FROM workflowName where apprv_id=@apprvid  and dp =@dp order by wf_order "
            'cmd.Parameters.Add("@apprvid", SqlDbType.VarChar).Value = ChkValue(apprvid)
            'cmd.Parameters.Add("@dp", SqlDbType.VarChar).Value = dp
            'cmd.CommandText = "SELECT chk_id,apprv_id,wf_order,chk_name, 1 wf_inside_order FROM workflowName where apprv_id='" + apprvid + "' order by wf_order "
            cmd.CommandText = "SELECT chk_id,apprv_id,wf_order,chk_name, 1 wf_inside_order FROM workflowName where apprv_id='" + apprvid + "' and dp = '" + dp + "' order by wf_order "
            Dim da As New SqlDataAdapter(cmd)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Function

        Public Function Qry_workflowName_isms_Write(ByVal apprvid As String, ByVal dp As String) As DataTable '填表時取的流程

            Dim cmd As SqlCommand = New SqlCommand()
            cmd.Connection = ConnDBS()
            'cmd.CommandText = " SELECT chk_id,apprv_id,wf_order,chk_name, 1 wf_inside_order FROM workflowName where apprv_id='" + apprvid + "'  and dp ='" + dp + "' order by wf_order  "
            cmd.CommandText = "  SELECT distinct wf_order,apprv_id,chk_name, 1 wf_inside_order FROM workflowName where apprv_id='" + apprvid + "'  and dp ='" + dp + "' order by wf_order  "

            Dim da As New SqlDataAdapter(cmd)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Function
        Public Function Qry_workflowName_isms_Req(ByVal approvalid As String, ByVal lb_recordid As String, ByVal apprvid As String, ByVal dp As String) As DataTable

            Dim cmd As SqlCommand = New SqlCommand()
            cmd.Connection = ConnDBS()
            ' cmd.CommandText = " SELECT chk_id,apprv_id,wf_order,chk_name, 1 wf_inside_order FROM workflowName where apprv_id=@apprvid  and dp =@dp order by wf_order "
            'cmd.Parameters.Add("@apprvid", SqlDbType.VarChar).Value = ChkValue(apprvid)
            'cmd.Parameters.Add("@dp", SqlDbType.VarChar).Value = dp

            cmd.CommandText = " SELECT distinct a.wf_order,a.chk_name,isnull(b.userid,'') wf_userid,isnull(b.status,'') wf_status_id,isnull(b.comment,'')    "
            cmd.CommandText += " wf_comment,b.id , case when status='1' then null else signdate end  signdate  FROM workflowName a join form_record    "
            cmd.CommandText += " b on  approvalid='" + approvalid + "' and recordid='" + lb_recordid + "' and wf_order=role where apprv_id='" + apprvid + "' and a.dp='" + dp + "'   "
            cmd.CommandText += " union all SELECT distinct wf_order ,chk_name,'' wf_userid,''  wf_status_id,'' wf_comment ,9999999999,null   "
            cmd.CommandText += " FROM workflowName where apprv_id='" + apprvid + "' and dp='" + dp + "' and wf_order > (select  top 1 role from form_record where      "
            cmd.CommandText += "  approvalid='" + approvalid + "' and recordid='" + lb_recordid + "' and status=1 ) order by id, wf_order "





            Dim da As New SqlDataAdapter(cmd)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Function
        Public Function Qry_next_agent(ByVal uid As String) As String
            Dim selectCMD As SqlCommand = New SqlCommand(" select isnull(agentuid, '" & uid & "') agentuid from agent where userid='" & uid & "' and startdate<=getdate() and enddate>=getdate()", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            If myDs.Tables(0).Rows.Count > 0 Then
                Qry_next_agent = myDs.Tables(0).Rows(0)("agentuid")
            Else
                Qry_next_agent = uid
            End If
        End Function
        Public Function Qry_form_record_flow(ByVal approvalid As String, ByVal recordid As String) As DataTable
            Dim sql As New StringBuilder
            sql.Append("select * from  form_record  where approvalid='" & approvalid & "' and status=1 and recordid='" & recordid & "'  ")
            Dim cmd As New SqlCommand(sql.ToString, ConnDBS)

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter(cmd)

            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function
        Public Function Qry_form_record_wait(ByVal recordid As String) As DataSet  '取待審核人員名稱(申請表)
            Dim sql As New StringBuilder
            sql.Append("select user_name,b.user_id   from form_record a inner join ismsUser b on a.userId = b.user_id where a.recordId= '" & recordid & "' and a.status='1' ")
            Dim cmd As New SqlCommand(sql.ToString, ConnDBS)

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter(cmd)

            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs
        End Function
        Public Function Qry_wfFormRecord_wait(ByVal doc_id As String) As DataSet  '取待審核人員名稱(文件)
            Dim sql As New StringBuilder
            sql.Append("SELECT a.*,b.user_name FROM docResoure a,ismsUser b  WHERE a.next_check_id = b.user_id and doc_id='" + doc_id + "'")
            Dim cmd As New SqlCommand(sql.ToString, ConnDBS)

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter(cmd)

            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs
        End Function

        Public Function Qry_form_record_name(ByVal recordid As String, ByVal workflowid As String) As DataSet  '取審核人員角色名稱
            Dim sql As New StringBuilder
            sql.Append("select user_name,b.user_id   from form_record a inner join ismsUser b on a.userId = b.user_id where a.recordId= '" & recordid & "' and a.workflowId ='" & workflowid & "' ")
            Dim cmd As New SqlCommand(sql.ToString, ConnDBS)

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter(cmd)

            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs
        End Function

        Public Function Qry_ctrlrm_use1(ByVal vender As String, ByVal Sdate As String, ByVal Edate As String) As DataTable

            Dim Sql As String = "select * from ctrlrm_use where empid=empid  "
            If Not String.IsNullOrEmpty(vender) Then Sql += " and  vender='" & vender & "'  "
            If Not String.IsNullOrEmpty(Sdate) Then Sql += " and convert(varchar(10),enterDate,111)>= '" & Sdate & "' "
            If Not String.IsNullOrEmpty(Edate) Then Sql += " and convert(varchar(10),enterDate,111) <= '" & Edate & "' "
            Sql += " order by enterDate desc "
            Dim selectCMD As SqlCommand = New SqlCommand(Sql, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function
        Public Function ChkNull(ByVal s As Object, Optional ByVal o_value As String = "1") As String
            If IsDBNull(s) Then
                Return o_value
            ElseIf s Is Nothing Then
                Return o_value
            ElseIf s.ToString.Trim().Length() = 0 Then
                Return o_value
            End If

            Return s.ToString.Trim()
        End Function

        Public Function Get_apprv_id(ByVal approvalId As String, ByVal dp As String) As String
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM Process_Record WHERE approvalId='" + approvalId + "' and dp = '" + dp + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Dim dt As DataTable = myDs.Tables(0)
            If dt.Rows.Count = 0 Then
                Get_apprv_id = ""
            Else
                Get_apprv_id = notNull(dt.Rows(0)("apprv_id"))
            End If
        End Function
        Public Function Get_workflowName(ByVal apprv_id As String, ByVal dp As String, ByVal wf_order As String) As DataSet
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM workflowName WHERE apprv_id='" + apprv_id + "' and dp='" + dp + "' and wf_order='" + wf_order + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs
        End Function
        Public Function Qry_FormRecord(ByVal dp As String) As DataTable
            Dim sql As StringBuilder = New StringBuilder
            'sql.Append(" select a.signDate ,a.recordid,a.workflowid,a.approvalid,b.form_name as type from  form_record a,Process_Record b     ")
            'sql.Append(" where a.approvalId = b.approvalId and status=1 and dp='" + dp + "' order by a.approvalId  ,signDate   ")   
            sql.Append(" SELECT a.record_id as recordid,a.doc_id,max(a.wf_order) as workflowid ,d.apprv_id as approvalid ,d.apprv_id,d.form_name as type,a.signdate  ")
            sql.Append(" FROM  ismsUser  b,ctlGroup c, wfFormRecord  a  ")
            sql.Append(" left outer join Process_Record d on a.apprv_id = d.apprv_id  ")
            sql.Append(" where a.wf_status_id='1' and a.wf_userid=b.user_id and c.group_id=b.group_id and c.dp='" + dp + "' ")
            sql.Append(" group by a.doc_id,d.form_name,a.signdate,d.apprv_id ,a.record_id  ")
            sql.Append(" union  ")
            sql.Append(" select  a.recordid,doc_id = null,a.workflowid,a.approvalid,apprv_id = null,b.form_name as type,a.signDate from  form_record a,Process_Record b,ismsUser c,ctlGroup d  ")
            sql.Append(" where a.approvalId = b.approvalId and status=1 and a.userId = c.user_id and c.group_id = d.group_id and d.dp = '" + dp + "' ")
            sql.Append(" order by approvalId,signdate  ")
            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function
        Public Function Qry_ctrlrm_use_wfFormRecord(ByVal approvalid As String, ByVal uid As String) As DataTable
            Dim selectCMD As SqlCommand = New SqlCommand("select * from  form_record a join ctrlrm_use b on  a.recordid=b.recordid where approvalid='" & approvalid & "' and a.status=1 and userid='" & uid & "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function
        Public Function Qry_bureau_use_wfFormRecord(ByVal approvalid As String, ByVal uid As String) As DataTable
            Dim selectCMD As SqlCommand = New SqlCommand("select * from  form_record a join bureau_apply b on  a.recordid=b.recordid where approvalid='" & approvalid & "' and a.status=1 and  b.using='Y' and  userid='" & uid & "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function
        Public Function Qry_office_use_wfFormRecord(ByVal approvalid As String, ByVal uid As String) As DataTable
            Dim selectCMD As SqlCommand = New SqlCommand("select * from  form_record a join office_apply b on  a.recordid=b.recordid where approvalid='" & approvalid & "' and a.status=1 and  b.using='Y' and userid='" & uid & "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function
        Public Function Qry_b_office_use_wfFormRecord(ByVal approvalid As String, ByVal uid As String) As DataTable
            Dim selectCMD As SqlCommand = New SqlCommand("select * from  form_record a join b_office_apply b on  a.recordid=b.recordid where approvalid='" & approvalid & "' and a.status=1 and  b.using='Y' and userid='" & uid & "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function
        Public Function Qry_workflow_flow(ByVal approvalid As String, ByVal formid As String, ByVal apprvid As String) As DataTable
            Dim sql As New StringBuilder
            sql.Append("SELECT wf_order,chk_name,isnull(userid,'') wf_userid,isnull(status,'') wf_status_id,isnull(comment,'') wf_comment,b.id , case when status='1' then null else signdate end  signdate  FROM workflowName a join form_record b on  approvalid='" & approvalid & "' and recordid='" & formid & "' and wf_order=role where apprv_id='" + apprvid + "'    union all SELECT  wf_order,chk_name,'' wf_userid,''  wf_status_id,'' wf_comment ,9999999999,null  FROM workflowName where apprv_id='" + apprvid + "'   and wf_order > (select  top 1 role from form_record where  approvalid='" & approvalid & "' and recordid='" & formid & "' and status=1) order by id ,wf_order")
            Dim cmd As New SqlCommand(sql.ToString, ConnDBS)

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter(cmd)

            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function

        Public Function Qry_workflow_flow(ByVal approvalid As String, ByVal formid As String, ByVal apprvid As String, ByVal dp As String) As DataTable
            Dim sql As New StringBuilder
            sql.Append("SELECT wf_order,chk_name,isnull(userid,'') wf_userid,isnull(status,'') wf_status_id,isnull(comment,'') wf_comment,b.id , case when status='1' then null else signdate end  signdate  FROM workflowName a join form_record b on  approvalid='" & approvalid & "' and recordid='" & formid & "' and wf_order=role where apprv_id='" + apprvid + "'    union all SELECT  wf_order,chk_name,'' wf_userid,''  wf_status_id,'' wf_comment ,9999999999,null  FROM workflowName where apprv_id='" + apprvid + "' and dp ='" + dp + "'   and wf_order > (select  top 1 role from form_record where  approvalid='" & approvalid & "' and recordid='" & formid & "' and status=1) order by id ,wf_order")
            Dim cmd As New SqlCommand(sql.ToString, ConnDBS)

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter(cmd)

            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function

        Public Function Qry_form_record3(ByVal approvalid As String, ByVal recordid As String, ByVal role As String) As DataTable
            Dim sql As New StringBuilder
            sql.Append("select approvalid,isnull(userid,'') as wf_userid,role wf_order,isnull(c.user_name,userid) as user_name , convert(varchar(10),userid)+'1' a from form_record b left JOIN [98ISMS].dbo.ismsUser c ON b.userid = c.user_id where approvalid='" & approvalid & "' and recordid='" & recordid & "' and role='" & role & "' ")
            Dim cmd As New SqlCommand(sql.ToString, ConnDBS)

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter(cmd)

            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "dt")
            Return myDs.Tables(0)
        End Function

        Public Function Qry_Auth_using3(ByVal dp As String, ByVal sys_id As String)
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" select * from bureau_apply a,bureau_apply_dtl b ")
            sql.Append(" where a.bureau_apply_id  = b.bureau_apply_id  ")
            sql.Append(" and a.apply_date + a.user_id in (select MAX(apply_date) + user_id from bureau_apply group by user_id) ")
            sql.Append(" and b.sys_id='" + sys_id + "' ")
            sql.Append(" and a.dp ='" + dp + "' ")
            sql.Append(" and a.using ='Y' ")
            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "bureau_apply")
            Return myDs
        End Function
        Public Function Qry_Auth_using4(ByVal dp As String, ByVal sys_id As String)
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" select * from office_apply a,office_apply_dtl b ")
            sql.Append(" where a.office_apply_id  = b.office_apply_id  ")
            sql.Append(" and a.apply_date + a.user_id in (select MAX(apply_date) + user_id from office_apply group by user_id) ")
            sql.Append(" and b.sys_id='" + sys_id + "' ")
            sql.Append(" and a.dp ='" + dp + "' ")
            sql.Append(" and a.using = 'Y' ")
            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "office_apply")
            Return myDs
        End Function
        Public Function Qry_Auth_using5(ByVal dp As String, ByVal sys_id As String)
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" select * from b_office_apply a,b_office_apply_dtl b ")
            sql.Append(" where a.b_office_apply_id  = b.b_office_apply_id  ")
            sql.Append(" and a.apply_date + a.user_id in (select MAX(apply_date) + user_id from b_office_apply group by user_id) ")
            sql.Append(" and b.sys_id='" + sys_id + "' ")
            sql.Append(" and a.dp ='" + dp + "' ")
            sql.Append(" and a.using = 'Y' ")
            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "b_office_apply")
            Return myDs
        End Function

        Public Function Qry_Process(ByVal recordid As String) '查詢流程是否已回到填表人身上
            Dim sql As StringBuilder = New StringBuilder
            sql.Append(" select * from form_record where recordId='" + recordid + "' and workflowId='0' and status ='1' ")
            Dim selectCMD As SqlCommand = New SqlCommand(sql.ToString, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "form_record")
            Return myDs
        End Function


    End Class



End Namespace




