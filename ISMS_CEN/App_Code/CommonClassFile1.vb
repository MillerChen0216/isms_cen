Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Security.Cryptography
Imports System.IO
Imports System.Web.UI.Page


Namespace ISMS
    Partial Public Class Common
        Inherits System.Web.UI.UserControl



        Public Function ConnDBS() As SqlConnection

            '嘉義縣連線字串
            'Dim SqlconnStr As String = "Data Source=.\SQLEXPRESS;Persist Security Info=True;User ID=isms;Password=xetsys;Initial Catalog=ISMS_DB"
            'Nate(本機開發)
            'Dim SqlconnStr As String = "Data Source=localhost;Persist Security Info=True;User ID=sa;Password=root$9*1;Initial Catalog=ISMS_DB"
            '金門142(本機開發)
            'Dim SqlconnStr As String = "Data Source=.;Persist Security Info=True;User ID=sa;Password=22018008;Initial Catalog=ISMS_DB"
            '台南市Connection
            'Dim SqlconnStr As String = "Data Source=.;Persist Security Info=True;User ID=isms;Password=pro1181$;Initial Catalog=ISMS_DB"
            '39 Connection
            'Dim SqlconnStr As String = "Data Source=10.40.190.39;Persist Security Info=True;User ID=sa;Password=root$9*1;Initial Catalog=ISMS_DB"
            '39 Express Connection
            'Dim SqlconnStr As String = "Data Source=.\SQLEXPRESS;Persist Security Info=True;User ID=sa;Password=root$9*1;Initial Catalog=ISMS_DB"
            '台水ISMS
            Dim SqlconnStr As String = ConfigurationManager.ConnectionStrings("ISMS_DBConnectionString").ConnectionString
            ' = "Data Source=localhost\sqlexpress;Persist Security Info=True;User ID=sa;Password=1qaz@4esz;Initial Catalog=ISMS_DB"
            Return New SqlConnection(SqlconnStr)


        End Function
        Public Function Check_Update(ByVal user_id As String, ByVal password As String, ByVal updatedate As String) '確認是否超過3個月未更新密碼
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM ismsUser WHERE user_id = '" + user_id + "' and pwd = '" + password + "' and update_date < '" + updatedate + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "update_date")
            Return myDs
        End Function
        Public Function Get_oldpwd(ByVal user_id As String) '取出前3次更新帳號
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT old_pwd FROM ismsUser WHERE user_id = '" + user_id + "'  ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "old_pwd")
            Return myDs
        End Function
        Public Function Get_loginfail(ByVal user_id As String, ByVal two_minute_ago As String) '取3分鐘內輸入密碼錯誤的次數
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT COUNT(*) AS countfail FROM appLog WHERE user_id='" + user_id + "' and action='LOGINFAIL' and issue_date >= '" + two_minute_ago + "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "countfail")
            Return myDs
        End Function
        Public Function Update_User_Using(ByVal user_id As String) '將帳號停用
            Dim selectCMD As SqlCommand = New SqlCommand("UPDATE ismsUser SET using ='N' WHERE user_id='" + user_id + "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "using")
        End Function
       
        Public Function Get_DOC04(ByVal dp As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT doc_num+'-'+doc_secret AS doc_num,doc_name FROM docResoure where dp='" + dp + "' AND doc_type='4' and doc_status='ISSUE' and locked='0' and substring(doc_num,1,2)='IS'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "docResoure")
            Return myDs
        End Function

        Public Function Get_degree(ByVal dp As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM degree where dp='" + dp + "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "degree")
            Return myDs
        End Function

        Public Function Get_dept(ByVal dp As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM deptment where dp='" + dp + "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "deptment")
            Return myDs
        End Function

        Public Function Get_phone(ByVal user_id As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT phone,ext FROM ismsUser where user_id='" + user_id + "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "phone")
            Return myDs
        End Function

        Public Function Get_assgroup_id(ByVal asset_id As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM assetGroup where asset_id=" + asset_id, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "assetGroup")
            Return myDs
        End Function
        Public Function Get_maxdocnum(ByVal dp As String, ByVal authdate As Date)
            Dim sqlstr As String
            Dim credate As String
            Dim docnum As String
            credate = authdate.ToString("yyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            sqlstr = "SELECT '" + credate + "-'+" + " isnull(substring('0'+convert(varchar,right(MAX(doc_num),2)+1),1,2),'01')  as dounum FROM docResoure where len(doc_num)=18 and substring(doc_num,10,6)='" + credate + "' and dp='" + dp + "'"
            'sqlstr = "SELECT convert(varchar(6),authdate,12)+'-'+isnull(substring('0'+convert(varchar,right(MAX(doc_num),2)+1),1,2),'01')  as dounum FROM docResoure where len(doc_num)=18 and substring(doc_num,10,6)=convert(varchar(6),authdate,12) and dp='" + dp + "'"
            Dim selectCMD As SqlCommand = New SqlCommand(sqlstr, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            'Dim Dept_Name As String
            mycmd.Fill(myDs, "docResoure")

            If myDs.Tables("docResoure").Rows.Count = 0 Then
                docnum = ""
            Else
                docnum = myDs.Tables("docResoure").Rows(0).Item("dounum")
            End If
            Return docnum
        End Function

        Public Function Get_Group(ByVal dp As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM ctlGroup where dp='" + dp + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "ctlGroup")
            Return myDs
        End Function


        Public Function Get_Parent()
            Dim selectCMD As SqlCommand = New SqlCommand(" select a.sValue +case when ISNULL(b.sValue,'')='' then '' else  +' \ '+ ISNULL(b.sValue,'') end sText, case when ISNULL(b.sValue,'')='' then a.NoteId  else  b.NoteId end  NoteId from Menu_TreeView a left join Menu_TreeView b on b.ParentId=a.NoteId  and b.sTarget='_parent' where  a.ParentId=0  ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "ctlGroup")
            Return myDs
        End Function

        Public Function Get_auth(ByVal subsys As String, ByVal ddl_group As String)
            Dim str As String = " select case when ISNULL(c.sValue,'')='' then a.ParentId  else b.ParentId end as ParentId  , "
            str += " a.sValue +case when ISNULL(b.sValue,'')='' then '' else  case when b.sTarget='Main' then '' else +' \ '+ b.sValue end end sText "
            str += ",  case when ISNULL(c.sValue,'')='' then b.sValue  else  c.sValue end as  sValue "
            str += ",  case when ISNULL(c.appid,0)=0 then b.appid  else  c.appid end as  appid "
            str += ",  case when ISNULL(c.sValue,'')='' then a.NoteId  else  b.NoteId end as  NoteId  "
            str += "  ,isnull((SELECT top 1  'Y'  FROM  groupApp ga where case when ISNULL(c.appid,0)=0 then b.appid  else  c.appid end = ga.appId and ga.group_id = '" + ddl_group + "' ),'N') cb  "
            str += " from Menu_TreeView a left join Menu_TreeView b on b.ParentId=a.NoteId  "
            str += " left join Menu_TreeView c on c.ParentId=b.NoteId  and c.Visible=1  "
            str += " where a.ParentId = 0  "

            If subsys <> "" Then
                str = "select * from  (" + str + ") aa where  NoteId  = '" + subsys + "'"
            End If

            str += " order by 1"
            Dim selectCMD As SqlCommand = New SqlCommand(str, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "ctlGroup")
            Return myDs
        End Function

        Public Function Get_User(ByVal dp As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT a.user_id,a.user_name,a.email FROM ismsUser a,ctlGroup b WHERE a.group_id = b.group_id and b.dp ='" + dp + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "ismsUser")
            Return myDs
        End Function
        Public Function Get_User_Y(ByVal dp As String) '狀態為啟用
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT a.user_id,a.user_name,a.email FROM ismsUser a,ctlGroup b WHERE  a.using='Y' and a.group_id = b.group_id and b.dp ='" + dp + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "ismsUser")
            Return myDs
        End Function

        Public Function Get_User_Y(ByVal dp As String, ByVal deptment As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT a.user_id,a.user_name,a.email FROM ismsUser a,ctlGroup b WHERE a.using='Y' and a.group_id = b.group_id and b.dp ='" + dp + "' and a.dept_id='" + deptment + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "ismsUser")
            Return myDs
        End Function


        Public Function Get_CheckUser(ByVal userid As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT user_id FROM ismsUser where user_id='" + userid + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "ismsUser")
            Return myDs

        End Function


        Public Function Get_CheckRiskName(ByVal dp As String, ByVal Risk_Str As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT riskEvtab_name FROM riskEvaluateMaster where riskEvtab_name='" + Risk_Str + "' AND dp='" + dp + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs

        End Function


        Public Function Get_dept_Name(ByVal id As String) '取得部門名稱

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT dept_name FROM deptment where dept_id ='" + id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim Dept_Name As String
            mycmd.Fill(myDs, "deptment")

            If myDs.Tables("deptment").Rows.Count = 0 Then
                Dept_Name = ""
            Else
                Dept_Name = myDs.Tables("deptment").Rows(0).Item("dept_name")
            End If
            Return Dept_Name

        End Function


        Public Function Get_degree_Name(ByVal id As String) '取得職稱名稱
            Dim Degree_Name As String
            Degree_Name = ""
            If id <> "" Then
                Dim selectCMD As SqlCommand = New SqlCommand("SELECT degree_name FROM degree where degree_id ='" + id + "'", ConnDBS)
                Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
                mycmd.SelectCommand = selectCMD
                Dim myDs As DataSet = New DataSet()
                mycmd.Fill(myDs, "degree")

                If myDs.Tables("degree").Rows.Count <> 0 Then
                    Degree_Name = myDs.Tables("degree").Rows(0).Item("degree_name")
                End If
            End If
            Return Degree_Name

        End Function



        Public Function Get_group_Name(ByVal id As String) '取得群組名稱

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT group_name FROM ctlGroup where group_id ='" + id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim Group_Name As String
            mycmd.Fill(myDs, "ctlGroup")

            If myDs.Tables("ctlGroup").Rows.Count = 0 Then
                Group_Name = ""
            Else
                Group_Name = myDs.Tables("ctlGroup").Rows(0).Item("group_name")
            End If
            Return Group_Name

        End Function
        Public Function Get_User_ID(ByVal name As String, ByVal dp As String) '取得使用者代碼

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT user_id FROM ismsUser a , ctlGroup b  where a.group_id = b.group_id and user_name ='" + name + "' and  b.dp='" + dp + "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim User_Name As String
            mycmd.Fill(myDs, "ismsUser")

            If myDs.Tables("ismsUser").Rows.Count = 0 Then
                User_Name = ""
            Else
                User_Name = myDs.Tables("ismsUser").Rows(0).Item("user_id")
            End If
            Return User_Name

        End Function

        Public Function Get_User_Name(ByVal id As String) '取得使用者名稱

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT user_name FROM ismsUser where user_id ='" + id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim User_Name As String
            mycmd.Fill(myDs, "ismsUser")

            If myDs.Tables("ismsUser").Rows.Count = 0 Then
                User_Name = ""
            Else
                User_Name = myDs.Tables("ismsUser").Rows(0).Item("user_name")
            End If
            Return User_Name

        End Function
        Public Function Get_User_Name_dp(ByVal id As String, ByVal dp As String) '取得使用者名稱(有分所別)

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT user_name From ismsUser a, ctlGroup b where a.group_id = b.group_id and user_id ='" + id + "' and b.dp='" + dp + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim User_Name As String
            mycmd.Fill(myDs, "ismsUser")

            If myDs.Tables("ismsUser").Rows.Count = 0 Then
                User_Name = ""
            Else
                User_Name = myDs.Tables("ismsUser").Rows(0).Item("user_name")
            End If
            Return User_Name

        End Function
        Public Function Get_User_dept(ByVal id As String) '取得使用者部門

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT dept_id FROM ismsUser where user_id ='" + id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim dept_id As String
            mycmd.Fill(myDs, "ismsUser")

            If myDs.Tables("ismsUser").Rows.Count = 0 Then
                dept_id = ""
            Else
                dept_id = myDs.Tables("ismsUser").Rows(0).Item("dept_id")
            End If
            Return dept_id

        End Function
        Public Function Get_DPMail(ByVal dp As String) '取得群發Email

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT dp_mail FROM DP_Data where dp_id ='" + dp + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim dp_Email As String
            mycmd.Fill(myDs, "DP_Data")

            If myDs.Tables("DP_Data").Rows.Count = 0 Then
                dp_Email = ""
            Else
                dp_Email = myDs.Tables("DP_Data").Rows(0).Item("dp_mail")
            End If
            Return dp_Email

        End Function


        Public Function Get_Userid(ByVal name As String) '取得使用者名稱

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT user_id FROM ismsUser where user_name ='" + name + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim User_id As String
            mycmd.Fill(myDs, "ismsUser")

            If myDs.Tables("ismsUser").Rows.Count = 0 Then
                User_id = ""
            Else
                User_id = myDs.Tables("ismsUser").Rows(0).Item("user_id")
            End If
            Return User_id

        End Function

        Public Function Get_DP_Name(ByVal user_id As String) As String '取得使用者所別

            Dim selectCMD As SqlCommand = New SqlCommand(" Select c.dp_name From ismsUser a, ctlGroup b ,DP_Data c Where  a.group_id = b.group_id and c.dp_id = b.dp and a.user_id = '" + user_id + "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim dp_name As String
            mycmd.Fill(myDs, "isms_User")

            If myDs.Tables("isms_User").Rows.Count = 0 Then
                dp_name = ""
            Else
                dp_name = myDs.Tables("isms_User").Rows(0).Item("dp_name")
            End If
            Return dp_name

        End Function
        Public Function Get_DP2(ByVal user_id As String) As String '取得使用者所別

            Dim selectCMD As SqlCommand = New SqlCommand(" Select b.dp From ismsUser a, ctlGroup b  Where  a.group_id = b.group_id  and a.user_id = '" + user_id + "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim dp_name As String
            mycmd.Fill(myDs, "isms_User")

            If myDs.Tables("isms_User").Rows.Count = 0 Then
                dp_name = ""
            Else
                dp_name = myDs.Tables("isms_User").Rows(0).Item("dp")
            End If
            Return dp_name

        End Function
        Public Function Get_All_Form()

            Dim selectCMD As SqlCommand = New SqlCommand(" SELECT distinct sValue FROM Menu_TreeView WHERE doc ='y' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Menu_TreeView")
            Return myDs
        End Function


        Public Function Get_Wf_User(ByVal dp As String, ByVal app_id As String, ByVal wf_order As String, ByVal wf_inside As String) '取得WF使用者名稱 order
            Dim selectCMD As SqlCommand
            If wf_inside <> "" Then
                selectCMD = New SqlCommand("SELECT  user_name + '(' +workflow.wf_userid +')' as name_id,workflow.apprv_id, workflow.wf_userid, workflow.wf_order, ismsUser.user_name as user_name FROM    workflow INNER JOIN    ismsUser ON workflow.wf_userid = ismsUser.user_id where workflow.apprv_id='" + app_id + "' and workflow.wf_order='" + wf_order + "' and workflow.wf_inside_order = '" + wf_inside + "'  and dp='" + dp + "'", ConnDBS)
            Else
                selectCMD = New SqlCommand("SELECT  user_name + '(' +workflow.wf_userid +')' as name_id,workflow.apprv_id, workflow.wf_userid, workflow.wf_order, ismsUser.user_name as user_name FROM    workflow INNER JOIN    ismsUser ON workflow.wf_userid = ismsUser.user_id where workflow.apprv_id='" + app_id + "' and workflow.wf_order='" + wf_order + "'  and dp='" + dp + "'", ConnDBS)
            End If
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "WFUser")
            Return myDs
        End Function


        Public Function Get_Wf_User_ALL(ByVal app_id As String, ByVal wf_order As String, ByVal wf_inside As String, ByVal dp As String) '取得WF使用者名稱 order

            Dim selectCMD As SqlCommand
            If wf_inside <> "" Then
                selectCMD = New SqlCommand(" SELECT  workflow.apprv_id, workflow.wf_userid, workflow.wf_order, ismsUser.user_name +'(' +wf_userid+')' as user_name FROM       workflow INNER JOIN    ismsUser ON workflow.wf_userid = ismsUser.user_id where workflow.apprv_id='" + app_id + "' and workflow.wf_order='" + wf_order + "' and workflow.wf_inside_order = '" + wf_inside + "'  and dp='" + dp + "'", ConnDBS)
            Else
                selectCMD = New SqlCommand(" SELECT  workflow.apprv_id, workflow.wf_userid, workflow.wf_order, ismsUser.user_name +'(' +wf_userid+')' as user_name FROM        workflow INNER JOIN    ismsUser ON workflow.wf_userid = ismsUser.user_id where workflow.apprv_id='" + app_id + "' and workflow.wf_order='" + wf_order + "'  and dp='" + dp + "'", ConnDBS)
            End If
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "WFUser")
            Return myDs
        End Function

        Public Function Get_Wf_User_detail(ByVal recordid As String, ByVal workflowId As String) '取得WF使用者狀況意見
            Dim selectCMD As SqlCommand
            selectCMD = New SqlCommand("select * from form_record where recordId ='" + recordid + "' and workflowId ='" + workflowId + "' order by signDate desc ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "WFUser")
            Return myDs
        End Function

        'Msg_Alert訊息
        Public Sub showMsg(ByVal thisPage As Page, ByVal AlertMessage As String)
            Dim txtMsg As New Literal()
            txtMsg.Text = "<script>alert('" & AlertMessage & "')</script>" + "<BR/>"
            thisPage.Controls.Add(txtMsg)
        End Sub

        'Msg_Confirm確認視窗
        Public Sub showConfirm(ByVal thisPage As Page, ByVal ConfirmMessage As String)
            Dim txtMsg As New Literal()
            txtMsg.Text = "<script>confirm('" & ConfirmMessage & "')</script>" & "<BR/>"
            thisPage.Controls.Add(txtMsg)
        End Sub

        Public Function Sys_Config(ByVal txt As String) As String

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT " + txt + " FROM system_Config where ID='0' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "System_Config")

            Dim Str_Config As String

            Str_Config = myDs.Tables(0).Rows(0).Item(0)
            Return Str_Config

        End Function


        Public Function Encode(ByVal code As String) As String
            Dim bytes As Byte() = Encoding.UTF7.GetBytes(code)
            Try
                Encode = Convert.ToBase64String(bytes)

            Catch
                Encode = code
            End Try
            Return Encode
        End Function

        Public Function Decode(ByVal code As String) As String
            Dim bytes As Byte() = Convert.FromBase64String(code)
            Try
                Decode = Encoding.UTF7.GetString(bytes)
            Catch
                Decode = code
            End Try
            Return Decode
        End Function


        Public Function Get_Doc_info(ByVal doc_id As String) '取得文件資訊

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT  * FROM docResoure where doc_id='" + doc_id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Doc_info")
            Return myDs
        End Function


        Public Function Get_issueDoc_info(ByVal doc_id As String) '取得文件資訊

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT  * FROM docResoure where doc_id='" + doc_id + "' and issue_date is not null ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Doc_info")
            Return myDs
        End Function


        Public Function Get_RiskForm_info(ByVal form_id As String) '取得文件資訊

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT  * FROM riskEvaluateMaster where form_id='" + form_id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function



        Public Function Get_WFStatus(ByVal WF_Status As String) '取得WF狀態

            Dim selectCMD As SqlCommand

            If WF_Status = "0" Then
                selectCMD = New SqlCommand("SELECT * from workflowStatus where wfstatus_id='0'", ConnDBS)
            ElseIf WF_Status = "1" Then
                selectCMD = New SqlCommand("SELECT * from workflowStatus where wfstatus_id='2' or wfstatus_id='9'", ConnDBS)
            End If

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "workflowStatus")
            Return myDs
        End Function
        Public Function Get_docResoure_waiting(ByVal apprv_id As String, ByVal doc_id As String) '取得等待的文件審核
            Dim selectCMD As SqlCommand

            selectCMD = New SqlCommand("select a.*,b.user_name from wfFormRecord a, ismsUser b where a.wf_userid =b.user_id and  wf_status_id ='1'  and apprv_id='" + apprv_id + "' and doc_id='" + doc_id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "wfFormRecord")
            Return myDs

        End Function
        Public Function Get_docResoure_waitingId(ByVal doc_id As String, ByVal userId As String) '取得等待的文件審核人
            Dim selectCMD As SqlCommand

            selectCMD = New SqlCommand("SELECT * FROM wfFormRecord WHERE doc_id='" + doc_id + "' and wf_status_id ='1' and wf_userid ='" + userId + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "wfFormRecord")
            Return myDs

        End Function
        Public Function Get_docResoure_order(ByVal doc_id As String, ByVal next_order As String) '取得WF狀態
            Dim selectCMD As SqlCommand

            selectCMD = New SqlCommand("SELECT * FROM docResoure WHERE doc_id='" + doc_id + "' and next_order='" + next_order + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "wfFormRecord")
            Return myDs

        End Function

        Public Function Get_wfFormRecord(ByVal apprv_id As String, ByVal doc_id As String, ByVal sort As String) '取得WF狀態
            Dim selectCMD As SqlCommand

            If sort = "ASC" Then
                selectCMD = New SqlCommand("SELECT * from wfFormRecord where apprv_id='" + apprv_id + "' and doc_id='" + doc_id + "' ORDER BY sn ASC", ConnDBS)
            ElseIf sort = "" Then
                selectCMD = New SqlCommand("SELECT * from wfFormRecord where apprv_id='" + apprv_id + "' and doc_id='" + doc_id + "' ORDER BY sn DESC", ConnDBS)
            End If

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "wfFormRecord")
            Return myDs

        End Function
        Public Function Get_wfFormRecord_id(ByVal sn As String) '取得WF狀態
            Dim selectCMD As SqlCommand

            selectCMD = New SqlCommand("  select * from wfFormRecord where sn = '" + sn + "' ", ConnDBS)

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "wfFormRecord")
            Return myDs

        End Function
        Public Function Get_wfFormRecord_order(ByVal apprv_id As String, ByVal doc_id As String, ByVal wf_order As String) '取得WF狀態
            Dim selectCMD As SqlCommand
            selectCMD = New SqlCommand("select * from wfFormRecord where apprv_id='" + apprv_id + "' and doc_id='" + doc_id + "' and wf_order='" + wf_order + "' order by signdate desc ", ConnDBS)

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "wfFormRecord")
            Return myDs

        End Function




        Public Function Get_wfFormCount(ByVal apprv_id As String, ByVal dp As String) '取得WF狀態
            Dim selectCMD As SqlCommand
            Dim selectCMD1 As SqlCommand
            If apprv_id = "5" Then
                selectCMD = New SqlCommand("SELECT * FROM riskEvaluateMaster WHERE form_status='RISK_VERIFY' AND dp='" + dp + "'", ConnDBS)
                Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
                mycmd.SelectCommand = selectCMD
                Dim myDs As DataSet = New DataSet()
                mycmd.Fill(myDs, "Table")
                If myDs.Tables("Table").Rows.Count > 0 Then
                    Return "Y"
                Else
                    Return "N"
                End If
            Else
                selectCMD = New SqlCommand("select a.* from form_record a,Process_Record b where b.apprv_id='" + apprv_id + "' and a.approvalId = b.approvalid and status='1' and a.dp ='" + dp + "'", ConnDBS)
                'selectCMD1 = New SqlCommand("SELECT * FROM wfFormRecord WHERE apprv_id='" + apprv_id + "' and wf_status_id='1'", ConnDBS)
                selectCMD1 = New SqlCommand("SELECT w.*,s.* FROM wfFormRecord w,docResoure s WHERE w.doc_id = s.doc_id and w.apprv_id='" + apprv_id + "' and w.wf_status_id='1' and s.dp = '" + dp + "' and s.doc_status != 'DELETEDOC' and s.doc_status != 'PUBLISH'", ConnDBS)
                Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
                mycmd.SelectCommand = selectCMD
                Dim myDs As DataSet = New DataSet()
                mycmd.Fill(myDs, "Table")
                Dim mycmd1 As SqlDataAdapter = New SqlDataAdapter()
                mycmd1.SelectCommand = selectCMD1
                Dim myDs1 As DataSet = New DataSet()
                mycmd1.Fill(myDs1, "Table")

                If myDs.Tables("Table").Rows.Count > 0 Or myDs1.Tables("Table").Rows.Count > 0 Then
                    Return "Y"
                Else
                    Return "N"
                End If
            End If

        End Function


        Public Function Get_eventNum(ByVal datestr As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT max(right(event_number,2)) as eventnum  FROM EventFormMaster where left(event_number,8)='" + datestr + "' order by eventnum desc", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function

        Public Function Get_WfUser(ByVal app_id As String, ByVal wf_order As String) '取得WF使用者名稱 判斷用
            Dim selectCMD As SqlCommand
            If wf_order = "" Then
                selectCMD = New SqlCommand("SELECT a.apprv_id,a.wf_order,a.chk_name,a.wf_inside_order,b.wf_userid FROM workflowName a,workflow b where a.apprv_id=b.apprv_id and a.apprv_id='" + app_id + "' and a.wf_order=b.wf_order and a.wf_inside_order=b.wf_inside_order  order by a.wf_order,a.wf_inside_order", ConnDBS)
            Else
                selectCMD = New SqlCommand("SELECT a.apprv_id,a.wf_order,a.chk_name,a.wf_inside_order,b.wf_userid FROM workflowName a,workflow b where a.apprv_id=b.apprv_id and a.apprv_id='" + app_id + "' and wf_order='" + wf_order + "' and a.wf_order=b.wf_order and a.wf_inside_order=b.wf_inside_order  order by a.wf_order,a.wf_inside_order", ConnDBS)
            End If
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "WFUser")
            Return myDs
        End Function


        Public Function Get_Wf_ApprvUser(ByVal dp As String, ByVal app_id As String, ByVal wf_order As String) '取得WF使用者名稱 判斷用
            Dim selectCMD As SqlCommand
            If wf_order = "" Then
                selectCMD = New SqlCommand("SELECT  * FROM workflow where apprv_id='" + app_id + "' and dp='" + dp + "' order by wf_order,wf_inside_order", ConnDBS)
            Else
                selectCMD = New SqlCommand("SELECT *  FROM workflow where  apprv_id='" + app_id + "' and wf_order='" + wf_order + "' and dp='" + dp + "' order by wf_order,wf_inside_order", ConnDBS)
            End If
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "WFUser")
            Return myDs
        End Function

        Public Function Get_Wf_ApprvUser(ByVal dp As String, ByVal app_id As String) '取得WF使用者名稱 判斷用
            Dim selectCMD As SqlCommand            
            selectCMD = New SqlCommand("SELECT  distinct wf_order FROM workflow where apprv_id='" + app_id + "' and dp='" + dp + "'", ConnDBS)

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "WFUser")
            Return myDs
        End Function


        Public Function ISMS_dpChange(ByVal dp As String)
            Dim selectCMD As SqlCommand
            If dp = "" Then
                selectCMD = New SqlCommand("SELECT * FROM DP_Data ", ConnDBS)
            Else
                selectCMD = New SqlCommand("SELECT * FROM DP_Data where dp_id='" + dp + "'", ConnDBS)
            End If

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function

        Public Function FixNull(ByVal dbvalue) As String
            If dbvalue Is DBNull.Value Then
                Return ""
            Else
                Return dbvalue.ToString
            End If
        End Function


        Public Function FixNull1(ByVal dbvalue) As String
            If dbvalue Is DBNull.Value Then
                Return "0"
            Else
                Return dbvalue.ToString
            End If
        End Function


        Public Function SetNull(ByVal SValue As String)  '程式判斷用
            If SValue = "" Then
                Return "null"
            Else
                Return SValue
            End If
        End Function

        Public Function Get_eventFormid()
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT top 1 Form_ID FROM EventFormMaster order by Form_ID desc", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function

        Public Function Get_CurrentFormid()
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT top 1 form_id FROM CurrentFormMaster order by form_id desc", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function


        Public Function Get_Date(ByVal s_day As Integer)
            Dim date_str, yy, mm, dd, dat As String

            date_str = Now.Date.AddDays(s_day)

            yy = CDate(date_str).Year

            mm = CDate(date_str).Month
            dd = CDate(date_str).Day
            If Len(mm) = 1 Then
                mm = "0" + mm
            End If

            If Len(dd) = 1 Then
                dd = "0" + dd
            End If

            dat = yy + mm + dd

            Return dat

        End Function

        Public Function ISMS_DocNameChange(ByVal doc_type As String)

            Dim doc_typename As String
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT sValue FROM Menu_TreeView where name_change='Y' and doc_type='" + doc_type + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Menu_TreeView")

            If myDs.Tables("Menu_TreeView").Rows.Count = 0 Then
                doc_typename = ""
            Else
                doc_typename = myDs.Tables("Menu_TreeView").Rows(0).Item("sValue")
            End If
            Return doc_typename

        End Function




        Public Function Get_WFStatusName(ByVal WF_Status_id As String) '取得WF狀態

            Dim selectCMD As SqlCommand

            selectCMD = New SqlCommand("SELECT wfstatus_name from workflowStatus where wfstatus_id='" + WF_Status_id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "workflowStatus")

            Dim wf_statusName As String

            wf_statusName = myDs.Tables(0).Rows(0).Item(0)
            Return wf_statusName

        End Function



        Public Function Get_wfRecord(ByVal apprv_id As String, ByVal doc_id As String, ByVal record_id As String) '取得WF Record
            Dim selectCMD As SqlCommand
            If record_id = "" Then
                selectCMD = New SqlCommand("SELECT  * FROM wfFormRecord where apprv_id='" + apprv_id + "' and doc_id='" + doc_id + "' ", ConnDBS)
            Else
                selectCMD = New SqlCommand("SELECT  * FROM wfFormRecord where apprv_id='" + apprv_id + "' and doc_id='" + doc_id + "' and record_id = '" + record_id + "'", ConnDBS)
            End If
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "wfFormRecord")
            Return myDs
        End Function


        Public Function Get_wfInside_Record(ByVal apprv_id As String, ByVal doc_id As String, ByVal record_id As String, ByVal wf_order As String) '取得WF Record
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT  a.*,b.user_name FROM wfFormRecord a,ismsUser b where a.wf_userid = b.user_id and apprv_id='" + apprv_id + "' and doc_id='" + doc_id + "' and record_id = '" + record_id + "' and wf_order='" + wf_order + "' order by sn", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "wfFormRecord")
            Return myDs
        End Function
        Public Function Get_wfInside_Record_sn(ByVal apprv_id As String, ByVal doc_id As String, ByVal record_id As String, ByVal sn As String) '取得WF Record
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT  a.*,b.user_name FROM wfFormRecord a,ismsUser b where a.wf_userid = b.user_id and apprv_id='" + apprv_id + "' and doc_id='" + doc_id + "' and record_id = '" + record_id + "' and sn='" + sn + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "wfFormRecord")
            Return myDs
        End Function


        Public Function Get_OldDoc_Rev(ByVal doc_num As String, ByVal doc_ver As String) '取得WF Record
            Dim temp_num, OldDoc_Rev As String
            '標準版本01~
            'temp_num = doc_ver - 1
            'OldDoc_Rev = Format(Val(temp_num), "00")
            '金門版本1.0~
            temp_num = doc_ver - (10 / 100)
            OldDoc_Rev = Format(Val(temp_num), "0.0")
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT  * FROM docResoure where  doc_num='" + doc_num + "' and doc_version='" + OldDoc_Rev + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "OldDoc_Rev")
            Return myDs
        End Function



        Public Function Get_WorkFlow_UserOrder(ByVal apprv_id As String, ByVal wf_order As String, ByVal wf_inside_order As String, ByVal dp As String) '取得WorkFlow傳入wf_order與wf_inside_order取得正確使用者
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM workflow where apprv_id='" + apprv_id + "' and wf_order='" + wf_order + "' and wf_inside_order='" + wf_inside_order + "' and dp='" + dp + "' order by wf_inside_order ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "WorkFlow_UserOrder")
            Return myDs
        End Function
        Public Function Get_Max_Approvalid() '取得WorkFlow，Wf_inside_order最大值，程式判斷用
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT max(approvalid) as max_approvalid FROM Form_GROUP", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "WorkFlow_InsideMaxId")
            Return myDs
        End Function
        Public Function Get_WorkFlow_MaxInsideOrderId(ByVal apprv_id As String, ByVal wf_order As String, ByVal dp As String) '取得WorkFlow，Wf_inside_order最大值，程式判斷用
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT max([wf_inside_order]) as max_order  FROM workflow where apprv_id='" + apprv_id + "' and wf_order='" + wf_order + "' and dp='" + dp + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "WorkFlow_InsideMaxId")
            Return myDs
        End Function

        Public Function Get_WorkFlow_MaxInsideOrderId1(ByVal apprv_id As String, ByVal wf_order As String, ByVal dp As String) '取得WorkFlow，Wf_inside_order最大值，程式判斷用
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT top 1 *  FROM workflow where apprv_id='" + apprv_id + "' and wf_order='" + wf_order + "' and dp='" + dp + "' order by wf_order desc,wf_inside_order desc ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "WorkFlow_InsideMaxId")
            Return myDs
        End Function


        Public Function Get_WorkFlow_MaxOrderId(ByVal apprv_id As String, ByVal dp As String) '取得WorkFlow，WF_Order最大值，程式判斷用
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT top 1 * from workflow where apprv_id='" + apprv_id + "' and dp='" + dp + "' order by wf_order desc,wf_inside_order desc ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "WorkFlow_OrderMaxId")
            Return myDs
        End Function

        Public Function Check_WorkFlow_OrderId(ByVal apprv_id As String, ByVal dp As String) '取得WorkFlow，WF_Order最大值，程式判斷用
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT distinct  wf_order from workflow where apprv_id='" + apprv_id + "' and wf_order <>'0' and dp='" + dp + "' order by wf_order  ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function



        Public Function Get_WorkFlow_OrderId(ByVal apprv_id As String, ByVal dp As String) '取得WorkFlow ID程式判斷用
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT distinct (wf_order + 1) as wf_order from workflow where apprv_id='" + apprv_id + "'   and dp='" + dp + "' order by wf_order desc ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "WorkFlow_OrderId")
            Return myDs
        End Function
        Public Function Get_Form_Process(ByVal apprv_id As String, ByVal dp As String) '取得未設定流程的表單
            Dim selectCMD As SqlCommand
            If apprv_id = "" Then
                'selectCMD = New SqlCommand("SELECT * FROM Form_Group b WHERE  NOT EXISTS (SELECT * FROM Process_Record a WHERE a.approvalId = b.approvalId)", ConnDBS)
                selectCMD = New SqlCommand(" SELECT distinct Form_Name,approvalId FROM Form_Group b WHERE NOT EXISTS (SELECT * FROM Process_Record a WHERE a.approvalId = b.approvalId and a.dp = b.dp and b.dp='" + dp + "')and b.dp='" + dp + "'", ConnDBS)
            Else
                selectCMD = New SqlCommand("SELECT * FROM Process_Record WHERE apprv_id ='" + apprv_id + "'  and approvalId != '0'", ConnDBS)
            End If
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Form_Process")
            Return myDs
        End Function
        Public Function Get_Form_Name(ByVal sURL As String)
            Dim selectCMD As SqlCommand
            selectCMD = New SqlCommand("SELECT * FROM Form_Group WHERE sURL= '" + sURL + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Form_Process")
            Return myDs
        End Function

        Public Function Get_Form_Data(ByVal Form_Name As String)
            Dim selectCMD As SqlCommand
            selectCMD = New SqlCommand("SELECT * FROM Form_Group WHERE Form_Name='" + Form_Name + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Form_Process")
            Return myDs
        End Function



        Public Function Get_PowerDocType(ByVal user_id As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT distinct b.*  FROM groupApp a,Menu_TreeView b,ismsUser c where a.appid=b.appid and a.group_id=c.group_id and c.user_id='" + user_id + "' and b.name_change='y' and sText ='文件點閱' order by noteid", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function


        Public Function Get_DocType()
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT  distinct sValue,doc_type  FROM Menu_TreeView where name_change='y'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "DocType_Menu")
            Return myDs
        End Function

        Public Function Ins_Log(ByVal user_id As String, ByVal user_type As String, ByVal target_id As String, ByVal target_page As String, ByVal target_type As String, ByVal action As String, ByVal log_memo As String)
            Dim Sqltxt As String
            Dim SqlCmd As SqlCommand
            Dim Common As New ISMS.Common
            Dim Conn As SqlConnection = Common.ConnDBS
            Sqltxt = "INSERT INTO appLog (user_id,user_type,target_id,target_page,target_type,action,log_memo,issue_date)  VALUES('" + user_id + "','" + user_type + "','" + target_id + "','" + target_page + "','" + target_type + "','" + action + "','" + log_memo + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')"
            SqlCmd = New SqlCommand(Sqltxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
        End Function

        Public Function SendMail(ByVal user_id As String, ByVal action As String, ByVal doc_num As String, ByVal doc_name As String, ByVal send_user As String)
            Dim msg As New System.Net.Mail.MailMessage
            msg.From = New Net.Mail.MailAddress(Sys_Config("Admin_Mail"), "ISMS系統通知")
            If action = "DocPublishGroup" Then
                msg.To.Add(New Net.Mail.MailAddress(Get_DPMail(user_id), "ISMS使用者"))
            Else
                msg.To.Add(New Net.Mail.MailAddress(Get_UserMail(user_id), "ISMS使用者"))
            End If

            msg.IsBodyHtml = True
            If action = "RegisterUser" Then
                msg.Subject = "test"
                msg.Body = "信件內文"

            ElseIf action = "WorkFlow_RISK" Then
                msg.Subject = "[審核] 資產類別：" + doc_num + " 風險評鑑表：" + doc_name + "-風險評鑑審核通知"
                msg.Body = "" + Get_User_Name(user_id) + "您好，<br><br>請您登入<a href=""http://intranet/isms/"">ISMS系統</a>後，點選［風險評鑑 \ 風險評鑑審核］進行審核 。<br>謝謝。<br><br>請勿回覆此系統通知郵件!"

            ElseIf action = "Workflow_RISK1" Then
                msg.Subject = "[審核] 風險評鑑表：" + doc_name + "-風險評鑑審核通知"
                msg.Body = "" + Get_User_Name(user_id) + "您好，<br><br>請您登入<a href=""http://intranet/isms/"">ISMS系統</a>後，點選［風險評鑑 \ 風險評鑑審核］進行文件審核 。<br>謝謝。<br><br>請勿回覆此系統通知郵件!"

            ElseIf action = "Workflow_RISK_KO" Then
                msg.Subject = "(不同意) 風險評鑑表：" + doc_name + "-風險評鑑審核通知"
                msg.Body = "" + Get_User_Name(user_id) + "您好，<br><br>您所提送的風險評鑑表已被退件，請您登入<a href=""http://intranet/isms/"">ISMS系統</a>確認 。<br>謝謝。<br><br>請勿回覆此系統通知郵件!"
            ElseIf action = "Workflow_RISK_OK" Then
                msg.Subject = "(同意) 風險評鑑：" + doc_name + "-風險評鑑審核通知"
                msg.Body = "" + Get_User_Name(user_id) + "您好，<br><br>您所提送的風險評鑑已經同意，請您登入<a href=""http://intranet/isms/"">ISMS系統</a>確認 。<br>謝謝。<br><br>請勿回覆此系統通知郵件!"
            ElseIf action = "Workflow" Then
                msg.Subject = "[審核] 文件代碼：" + doc_num + " 文件名稱：" + doc_name + "-文件審核通知"
                msg.Body = "" + Get_User_Name(user_id) + "您好，<br><br>請您登入<a href=""http://intranet/isms/"">ISMS系統</a>後，點選［文件管理＼發行管理＼文件審核管理］進行文件審核 。<br>謝謝。<br><br>請勿回覆此系統通知郵件!"
            ElseIf action = "Workflow_OK" Then
                msg.Subject = "(同意) 文件代碼：" + doc_num + " 文件名稱：" + doc_name + "-文件審核通知"
                msg.Body = "" + Get_User_Name(user_id) + "您好，<br><br>您所增修的文件已經同意，請您登入<a href=""http://intranet/isms/"">ISMS系統</a>確認 。<br>謝謝。<br><br>請勿回覆此系統通知郵件!"
            ElseIf action = "Workflow_KO" Then
                msg.Subject = "(不同意) 文件代碼：" + doc_num + " 文件名稱：" + doc_name + "-文件審核結果"
                msg.Body = "" + Get_User_Name(user_id) + "您好，<br><br>您所增修的文件已經被" + Get_User_Name(send_user) + "退件，請您登入<a href=""http://intranet/isms/"">ISMS系統</a>確認 。<br>謝謝。<br><br>請勿回覆此系統通知郵件!"
            ElseIf action = "PublishDoc" Then
                msg.Subject = "(核定管理) 文件代碼：" + doc_num + " 文件名稱：" + doc_name + "-發行管理通知"
                msg.Body = "" + Get_User_Name(user_id) + "您好，<br><br>" + doc_num + " 文件名稱：" + doc_name + "文件已經審核通過，請您登入<a href=""http://intranet/isms/"">ISMS系統</a>，點選［文件管理＼發行管理＼文件發行管理］進行核定文件。<br>謝謝。<br><br>請勿回覆此系統通知郵件!"
            ElseIf action = "DisableDoc" Then
                msg.Subject = "(廢止管理) 文件代碼：" + doc_num + " 文件名稱：" + doc_name + "-廢止管理通知"
                msg.Body = "" + Get_User_Name(user_id) + "您好，<br><br>" + doc_num + " 文件名稱：" + doc_name + "文件已經審核通過，請您登入<a href=""http://intranet/isms/"">ISMS系統</a>，點選［文件管理＼發行管理＼文件廢止管理］進行廢止文件。<br>謝謝。<br><br>請勿回覆此系統通知郵件!"


                'ElseIf action = "ResetPwd" Then
                '    msg.Subject = "ISMS系統通知-使用者密碼重新設定通知"
                '    msg.Body = "" + Get_User_Name(user_id) + "您好，<br><br>請您開啟下列連結，進行密碼重新設定動作。<br>謝謝。<br><br>請勿回覆此系統通知郵件!"
            ElseIf action = "DocPublish" Then
                msg.Subject = "(核定通知) 文件代碼：" + doc_num + " 文件名稱：" + doc_name + "-發行完成通知"
                msg.Body = "" + Get_User_Name(user_id) + "您好，<br><br>您所增修的文件已經發行，請您登入<a href=""http://intranet/isms/"">ISMS系統</a>確認。<br>謝謝。<br><br>請勿回覆此系統通知郵件!"
            ElseIf action = "DocPublishGroup" Then
                msg.Subject = "(發行通知) 文件代碼：" + doc_num + " 文件名稱：" + doc_name + "-發行完成通知"
                msg.Body = "敬啟者您好，文件代碼：" + doc_num + " 文件名稱：" + doc_name + "已發行，您可登入ISMS系統進行文件點閱。<br>謝謝。<br><br>請勿回覆此系統通知郵件!"
            ElseIf action = "DocDisable" Then
                msg.Subject = "(廢止通知) 文件代碼：" + doc_num + " 文件名稱：" + doc_name + "-廢止完成通知"
                msg.Body = "" + Get_User_Name(user_id) + "您好，<br><br>您所增修的文件已經廢止，請您登入<a href=""http://intranet/isms/"">ISMS系統</a>確認。<br>謝謝。<br><br>請勿回覆此系統通知郵件!"
            ElseIf action = "DelDoc" Then
                msg.Subject = "(刪除通知) 文件代碼：" + doc_num + " 文件名稱：" + doc_name + "-文件刪除通知"
                msg.Body = "" + Get_User_Name(user_id) + "您好，<br><br>您所新增的文件已經刪除，請您登入<a href=""http://intranet/isms/"">ISMS系統</a>確認。<br>謝謝。<br><br>請勿回覆此系統通知郵件!"

            End If
            Try

                If FixNull(Sys_Config("SMTP_Host")) = "" Then

                Else

                    Dim smtp As New System.Net.Mail.SmtpClient(Sys_Config("SMTP_Host"))

                    If FixNull(Sys_Config("SMTP_User")) = "" Then
                        smtp.Send(msg)
                    Else
                        smtp.Credentials = New System.Net.NetworkCredential(Sys_Config("SMTP_User"), Sys_Config("SMTP_PWD"))
                        smtp.Send(msg)

                    End If
                End If

            Catch ex As Exception

                '    showMsg(Me.Page, ex.Message)

            End Try

        End Function



        Public Function Get_UserMail(ByVal id As String) '取得使用者Email

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT email FROM ismsUser where user_id ='" + id + "' and using='Y'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            Dim User_Email As String
            mycmd.Fill(myDs, "ismsUser")

            If myDs.Tables("ismsUser").Rows.Count = 0 Then
                User_Email = ""
            Else
                User_Email = myDs.Tables("ismsUser").Rows(0).Item("email").ToString
            End If
            Return User_Email

        End Function


        Public Function Check_DocDownLog(ByVal doc_id As String, ByVal user_id As String)


            Dim selectCMD As SqlCommand = New SqlCommand("Select  distinct docResoure.doc_id, docResoure.doc_name, docResoure.doc_num, docResoure.doc_type, docResoure.doc_version from docResoure,applog where docResoure.doc_id='" + doc_id + "'  and docResoure.doc_id=applog.target_id   and applog.user_id='" + user_id + "' and applog.action='FILEDOWN'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "DownLog")

            Return myDs


        End Function

        Public Function Get_MaxDocid1()
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT top 1 doc_id,doc_name,doc_num FROM docResoure order by doc_id desc", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Dim doc_id As String
            If myDs.Tables("table").Rows.Count = 0 Then
                doc_id = "1"
            Else
                doc_id = (CType(myDs.Tables("table").Rows(0).Item("doc_id").ToString, Integer) + 1).ToString
            End If
            Return doc_id
            Return myDs
        End Function
        Public Function Get_MaxDocid()
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT top 1 doc_id,doc_name,doc_num FROM docResoure order by doc_id desc", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function
        Public Function Get_MaxApprvId()
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT Top 1  apprv_id as apprv_id FROM Process_Record order by apprv_id desc ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "table")
            Dim apprv_id As String
            If myDs.Tables("table").Rows.Count = 0 Then
                apprv_id = "1"
            Else
                apprv_id = myDs.Tables("table").Rows(0).Item("apprv_id").ToString
            End If
            Return apprv_id
        End Function


        Public Function Get_MenuTreeView(ByVal group_id As String, ByVal sValue As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT *  FROM groupApp where group_id ='" + group_id + "' and left(appid,1)= '" + sValue + "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function
        Public Function Get_All_FormNotInGroup()
            Dim selectCMD As SqlCommand = New SqlCommand(" SELECT *,CASE WHEN sValue = sText THEN sValue ELSE sValue + '(' + sText +')' END AS Value FROM Menu_TreeView a WHERE doc ='Y' and a.sTarget != '_parent' and NOT  EXISTS (SELECT * FROM Form_Group b  WHERE a.sURL = b.sURL) ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function
        Public Function Get_All_Form_GroupFun(ByVal approvalId As String)
            Dim selectCMD As SqlCommand = New SqlCommand(" SELECT a.sText,a.sValue,a.sURL,b.approvalId,b.Form_Name,CASE WHEN sText = sValue THEN sValue ELSE sValue + '(' +sText +')' END AS Value FROM Menu_TreeView a,Form_Group b WHERE b.approvalId='" + approvalId + "' and a.sURL = b.sURL group by a.sText,a.sValue,a.sURL,b.approvalId,b.Form_Name", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function


        Public Function Check_Power(ByVal user_id As String, ByVal sUrl As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT distinct b.*  FROM groupApp a,Menu_TreeView b,ismsUser c where a.appid=b.appid and a.group_id=c.group_id and c.user_id='" + user_id + "' and UPPER(sURL)='" + sUrl.ToUpper() + "' order by noteid", ConnDBS) '2020/06 更新加Upper
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function

        Public Function Get_MenuUrl(ByVal sUrl As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * from Menu_TreeView where   sURL ='" + sUrl + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Dim Url_String As String

            If myDs.Tables("Table").Rows.Count = 0 Then
                Url_String = ""
            Else
                Url_String = myDs.Tables("Table").Rows(0).Item("sText") + "\" + myDs.Tables("Table").Rows(0).Item("sValue")
            End If
            Return Url_String
        End Function


        Public Function Check_PublishPower(ByVal dp As String, ByVal appid As String)
            'Dim selectCMD As SqlCommand = New SqlCommand("SELECT distinct b.*  FROM groupApp a,Menu_TreeView b,ismsUser c where a.appid=b.appid and a.group_id=c.group_id and c.user_id='" + user_id + "' and a.appid='" + appid + "' order by noteid", ConnDBS)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT distinct b.*,c.*  FROM groupApp a,Menu_TreeView b,ismsUser c where a.appid=b.appid and a.group_id=c.group_id and a.power='VIEW' and a.appid='" + appid + "' and left(c.user_id,5)='" + dp + "'  and c.using='Y' order by noteid", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function

        Public Function Get_MaxFormid()
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT top 1 form_id FROM riskEvaluateMaster order by form_id desc", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function

        Public Function Get_RiskEvCount(ByVal dp As String, ByVal id As String)

            'Dim selectCMD As SqlCommand = New SqlCommand("SELECT a.assitem_id ,b.assitem_name ,b.user_id ,b.holder ,b.item_amount ,b.location ,b.Confidentiality ,b.Integrity ,b.Availability ,(b.Confidentiality " + Sys_Config("Math_operation") + " b.Integrity " + Sys_Config("Math_operation") + " b.Availability) as assitScore,(Select secure_lv from secureLevel where lv_scoreS <=(b.Confidentiality " + Sys_Config("Math_operation") + " b.Integrity " + Sys_Config("Math_operation") + " b.Availability)  and  lv_scoreE >=(b.Confidentiality " + Sys_Config("Math_operation") + " b.Integrity " + Sys_Config("Math_operation") + " b.Availability)) as assitScore_lv ,(Select serure_desc from secureLevel where lv_scoreS <=(b.Confidentiality " + Sys_Config("Math_operation") + " b.Integrity " + Sys_Config("Math_operation") + " b.Availability)  and  lv_scoreE >=(b.Confidentiality " + Sys_Config("Math_operation") + " b.Integrity " + Sys_Config("Math_operation") + " b.Availability)) as assitSecure_lv,d.weakness,e.threat,a.riskp_score,a.impact_score ,(a.riskp_score " + Sys_Config("RISK_operation") + " a.impact_score)  as totalrisk_score,(Select totalrisk_desc from totalRisklv where lv_scoreS <=(a.riskp_score " + Sys_Config("Risk_operation") + " a.impact_score)  and  lv_scoreE >=(a.riskp_score " + Sys_Config("Risk_operation") + " a.impact_score)) as totalrisk_lv FROM riskEvaluate as a,assetItem as b,infoSecurityType as c,weakness as d,threat as e WHERE b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id AND a.info_type = c.info_type AND b.asset_id=" + id + " ORDER BY a.assitem_id,c.info_order", ConnDBS)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT a.assitem_id ,b.assitem_name ,b.user_id ,b.holder ,b.item_amount ,b.location ,b.Confidentiality ,b.Integrity ,b.Availability ,(b.Confidentiality " + Sys_Config("Math_operation") + " b.Integrity " + Sys_Config("Math_operation") + " b.Availability) as assitScore,(Select secure_lv from secureLevel where lv_scoreS <=(b.Confidentiality " + Sys_Config("Math_operation") + " b.Integrity " + Sys_Config("Math_operation") + " b.Availability)  and  lv_scoreE >=(b.Confidentiality " + Sys_Config("Math_operation") + " b.Integrity " + Sys_Config("Math_operation") + " b.Availability)) as assitScore_lv ,(Select serure_desc from secureLevel where lv_scoreS <=(b.Confidentiality " + Sys_Config("Math_operation") + " b.Integrity " + Sys_Config("Math_operation") + " b.Availability)  and  lv_scoreE >=(b.Confidentiality " + Sys_Config("Math_operation") + " b.Integrity " + Sys_Config("Math_operation") + " b.Availability)) as assitSecure_lv,d.weakness,e.threat,a.riskp_score,a.impact_score ,(a.riskp_score " + Sys_Config("RISK_operation") + " a.impact_score)  as totalrisk_score,(Select totalrisk_desc from totalRisklv where lv_scoreS <=(a.riskp_score " + Sys_Config("Risk_operation") + " a.impact_score)  and  lv_scoreE >=(a.riskp_score " + Sys_Config("Risk_operation") + " a.impact_score)) as totalrisk_lv FROM riskEvaluate as a,assetItem as b,weakness as d,threat as e WHERE b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id AND b.dp='" + dp + "' AND b.asset_id=" + id + " ORDER BY a.assitem_id", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs

        End Function



        Public Function Get_MaxThreatWeaknessEvaluateFormid(ByVal id As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT top 1 form_id FROM ThreatWeaknessEvaluateMaster where apprv_id='" + id + "' order by form_id desc", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function


        Public Function Get_MaxRiskimprovePlanFormid()
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT top 1 Form_id FROM RiskimprovePlanMaster order by Form_id desc", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function

        Public Function Get_ThreatWeaknessRiskForm_info(ByVal form_id As String) '取得文件資訊

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT  * FROM ThreatWeaknessEvaluateMaster where form_id='" + form_id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function

        Public Function Get_RiskimprovePlan_info(ByVal form_id As String) '取得文件資訊

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT  * FROM RiskimprovePlanMaster where form_id='" + form_id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function

        Public Function Get_event_info(ByVal form_id As String) '取得EVENTCURRECT資訊

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT  * FROM EventFormMaster where form_id='" + form_id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function

        Public Function Get_Current_info(ByVal form_id As String) '取得EVENTCURRECT資訊

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT  * FROM CurrentFormMaster where form_id='" + form_id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function



        Public Function Get_MaxTrainingid()
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT top 1 *  FROM ClassTraining_list order by id desc", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function



        Public Function Get_Training_info(ByVal id As String) '取得文件資訊

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM ClassTraining_list where id='" + id + "' and class_enable=1 ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Board_info")
            Return myDs
        End Function

        Public Function Get_Training_files(ByVal id As String) '取得文件資訊

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM ClassTraining_files where pid='" + id + "'  ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Board_files")
            Return myDs
        End Function


        Public Function Get_docType1(ByVal dp As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM docType1 where dp='" + dp + "' order by id", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function

        '共用_連結資料庫抓表格_2020/06/05
        Public Function Con(ByVal sql As String) As DataTable
            Dim cmd As SqlCommand = New SqlCommand()
            Dim SqlconnStr As String = ConfigurationManager.ConnectionStrings("ISMS_DBConnectionString").ConnectionString
            cmd.Connection = New SqlConnection(SqlconnStr)
            cmd.CommandText = Sql
            Dim da As New SqlDataAdapter(cmd)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Function

        '西元轉民國
        Public Function Year(ByVal Timer As Date) As String
            Dim dtNow As Date = CDate(Timer)
            Dim twC = New System.Globalization.TaiwanCalendar()
            Dim date1 As String
            date1 = Integer.Parse(twC.GetYear(dtNow)).ToString() + dtNow.ToString("/MM/dd")
            Return date1
        End Function

        '民國轉西元
        Public Function YearYYYY(ByVal Timer As Date) As String
            Dim strDate As String = Timer
            Dim DateArr As Array = Split(strDate, "/")
            Dim date1 = CInt(DateArr(0)) + 1911 & Mid(strDate, Len(DateArr(0)) + 1)
            Return date1
        End Function

        '西元轉民國(只有年)
        Public Function Y(ByVal Timer As Date) As String
            Dim dtNow As Date = CDate(Timer)
            Dim twC = New System.Globalization.TaiwanCalendar()
            Dim date1 As String
            date1 = Integer.Parse(twC.GetYear(dtNow)).ToString()
            Return date1
        End Function

        '查看目前為哪一關-20200804
        Public Function Get_workflowId(ByVal approvalId As String, ByVal recordId As String) As Integer
            Dim sql = "  select * from form_record where approvalId = " + approvalId + " and recordId = '" + recordId + "' and status = 1"
            Dim data As DataTable = Con(sql)
            If Not data.Rows.Count = 0 Then
                Return data.Rows(0)("workflowId")
            End If
            Return 10000
        End Function

        '下載檔案但顯示原檔名-20201207
        Public Sub DownFileForOriFileName(ByVal WebForm As System.Web.UI.Page, ByVal File_Name As String, ByVal File_oriName As String, Optional ByVal FileName As String = "")
            Try



                WebForm.Response.ContentType = "application/octet-stream"
                WebForm.Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5")
                Dim fileContents As Byte() = System.IO.File.ReadAllBytes(File_Name)
                WebForm.Response.AddHeader("Content-Disposition", [String].Format("attachment; filename={0}", File_oriName))
                WebForm.Response.OutputStream.Write(fileContents, 0, fileContents.Length)
                WebForm.Response.Flush()
                WebForm.Response.End()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class

End Namespace




