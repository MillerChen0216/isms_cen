Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient

Namespace ISMS
    Partial Public Class Common

        Public Function Get_Asset()
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM asset  Order by asset_id ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Asset")
            Return myDs
        End Function
        Public Function Get_AssetItem(ByVal dp As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT assitem_id,assitem_name FROM assetItem where dp='" + dp + "' Order by assitem_id ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "assetItem")
            Return myDs
        End Function


        Public Function Get_assetValueListname(ByVal sInfoType As String, ByVal sAssetId As String, ByVal score As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM [assetValueList] WHERE info_type='" + sInfoType + "' AND asset_id =" + sAssetId + " and asset_score=" + score + " ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "assetValueList")
            Return myDs
        End Function
        Public Function Get_AssetID(ByVal asset As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM asset where asset_name='" + asset + "'  ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Asset")
            Return myDs
        End Function



        Public Function Get_AssetName(ByVal asset_id As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM asset where asset_id='" + asset_id + "'  ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Asset")
            Return myDs
        End Function


        Public Function Get_riskEvaluate_AssetItem(ByVal dp As String, ByVal AssetId As String, ByVal assetitem_id As String)
            Dim selectCMD As SqlCommand

            If assetitem_id = "" Then
                selectCMD = New SqlCommand("SELECT assitem_id,assitem_name FROM assetItem where dp='" + dp + "' and asset_id=" + AssetId + " and  (Confidentiality+Integrity+Availability) >= " + Sys_Config("Asset_Risk") + " Order by assitem_id ", ConnDBS)
            Else
                selectCMD = New SqlCommand("SELECT assitem_id,assitem_name, (Confidentiality+Integrity+Availability) as Asset_Value,dept_id FROM assetItem where dp='" + dp + "' and asset_id=" + AssetId + " and assitem_id='" + assetitem_id + "' and  (Confidentiality+Integrity+Availability) >= " + Sys_Config("Asset_Risk") + " Order by assitem_id ", ConnDBS)
            End If

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "assetItem")
            Return myDs
        End Function



        Public Function Get_infoSecurityType()
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT info_type,info_Cname,info_type+'('+info_Cname+')' as info_Fname FROM infoSecurityType Order by info_order ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "infoSecurityType")
            Return myDs
        End Function
        Public Function Get_assetValueList(ByVal sInfoType As String, ByVal sAssetId As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM [assetValueList] WHERE info_type='" + sInfoType + "' AND asset_id =" + sAssetId + "Order by asset_score ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "assetValueList")
            Return myDs
        End Function
        Public Function Get_riskP()
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT riskP_score,riskP_desc FROM riskP Order by riskP_score ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "riskP")
            Return myDs
        End Function
        Public Function Get_riskImpact()
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT impact_score,impact_desc FROM riskImpact Order by impact_score ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "riskImpact")
            Return myDs
        End Function
        Public Function Get_weakness(ByVal sAssetId As String)
            Dim selectCMD As SqlCommand
            If sAssetId = "" Then
                selectCMD = New SqlCommand("SELECT weakness_id,weakness FROM weakness Order by asset_id,weakness", ConnDBS)
            Else
                selectCMD = New SqlCommand("SELECT weakness_id,weakness FROM weakness WHERE asset_id =" + sAssetId + " Order by weakness_id", ConnDBS)
            End If

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "weakness")
            Return myDs
        End Function
        Public Function Get_threat(ByVal sAssetId As String)
            Dim selectCMD As SqlCommand
            If sAssetId = "" Then
                selectCMD = New SqlCommand("SELECT threat_id,threat FROM threat Order by asset_id,threat", ConnDBS)
            Else
                selectCMD = New SqlCommand("SELECT threat_id,threat FROM threat WHERE asset_id =" + sAssetId + " Order by asset_id", ConnDBS)
            End If

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "threat")
            Return myDs
        End Function


        Public Function Get_Secure_lv(ByVal Score As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT secure_lv,lv_scoreS,lv_scoreE,serure_desc FROM secureLevel where lv_scoreS <='" + Score + "' and lv_scoreE >='" + Score + "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "secureLevel")
            Return myDs
        End Function


        Public Function Get_TotalSecure_lv(ByVal Score As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT totalrisk_lv,lv_scoreS,lv_scoreE,totalrisk_desc FROM totalRisklv  where lv_scoreS <='" + Score + "' and lv_scoreE >='" + Score + "' ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "totalRisklv")
            Return myDs
        End Function

        Public Function Get_CheckriskEvaluatePK(ByVal assitem_id As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT assitem_id FROM riskEvaluate where assitem_id='" + assitem_id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs

        End Function



        Public Function Get_assetidWithRiskType(ByVal asset_id As String)
            Dim SqlTxt As String

            If asset_id = "0" Then ' then
                SqlTxt = "SELECT risk_id,risk_type FROM  risk_type ORDER BY risk_id"
            Else
                SqlTxt = "SELECT risk_id,risk_type FROM  risk_type WHERE  owner_asset_id LIKE '%" + asset_id + "%' ORDER BY risk_id"
            End If
            Dim selectCMD As SqlCommand = New SqlCommand(SqlTxt, ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Return myDs
        End Function


        Public Function Get_ControlItem(ByVal id As String)
            Dim selectCMD As SqlCommand

            If id = "" Then
                selectCMD = New SqlCommand("SELECT ControlID,ControlID+'_'+ControlName as ControlName,ControlDesc FROM ControlItem", ConnDBS)
            Else
                selectCMD = New SqlCommand("SELECT ControlID,ControlID+'_'+ControlName as ControlName,ControlDesc FROM ControlItem where ControlID='" + id + "'", ConnDBS)
            End If

            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table01")

            Return myDs
        End Function


        Public Sub ddl_DataRead(ByVal Sqlstr As String, ByVal ddl As DropDownList, ByVal field_num1 As Integer, ByVal field_num2 As Integer)
            Dim Conn As SqlConnection = ConnDBS()
            Conn.Open()
            Dim com As New SqlCommand(Sqlstr, Conn)
            Dim sr As SqlDataReader = Nothing
            sr = com.ExecuteReader()
            While sr.Read()
                ddl.Items.Add(New ListItem(sr(field_num1).ToString(), sr(field_num2).ToString()))
            End While
            Conn.Close()
        End Sub


        Public Function Risk_Config(ByVal txt As String)

            Dim selectCMD As SqlCommand = New SqlCommand("SELECT risk_cfvalue  FROM riskConfig where risk_cftype='" + txt + "'  ", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "riskConfig")

            Dim Str_Config As String

            Str_Config = myDs.Tables(0).Rows(0).Item(0)
            Return Str_Config

        End Function

        Public Function Get_weaknessName(ByVal id As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM weakness where weakness_id='" + id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")
            Dim txt_String As String
            Try
                txt_String = myDs.Tables("Table").Rows(0).Item("weakness")
            Catch ex As Exception
            End Try
            Return txt_String
        End Function


        Public Function Get_threatName(ByVal id As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM threat where threat_id='" + id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")

            Dim txt_String As String

            Try
                txt_String = myDs.Tables("Table").Rows(0).Item("threat")

            Catch ex As Exception

            End Try
            Return txt_String
        End Function


        Public Function Get_RisktypeName(ByVal id As String)
            Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM risk_type where risk_id='" + id + "'", ConnDBS)
            Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
            mycmd.SelectCommand = selectCMD
            Dim myDs As DataSet = New DataSet()
            mycmd.Fill(myDs, "Table")

            Dim txt_String As String

            Try
                txt_String = myDs.Tables("Table").Rows(0).Item("risk_type")

            Catch ex As Exception

            End Try
            Return txt_String
        End Function

    End Class

End Namespace




