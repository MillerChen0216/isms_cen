﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AttachDocType.aspx.vb" Inherits="AttachDocType" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript">

</script>
<body>
    <form id="form1" runat="server">
    <div>
    <br />
    <table id="Table" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 500px" visible="true">
            <tr>
                <th colspan="2" class="tb_title_w_2" align="center">
                    <img src="Images/Icon1.gif" />
                    附件名稱維護
                    <asp:Label ID="lb_msg" runat="server" ForeColor="red" EnableViewState="false"></asp:Label>
                </th>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    操作
                </td>
                <td class="tb_w_1" align="center">
                    <asp:RadioButtonList ID="EditType" runat="server" RepeatDirection="Horizontal"
                        OnSelectedIndexChanged="EditType_selectedchange" AutoPostBack="True">
                        <asp:ListItem Text="新增" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="修改" Value="1"></asp:ListItem>
                        <asp:ListItem Text="刪除" Value="2"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr runat="server" id="otr">
                <td class="tb_title_w_1" width="150px">
                    附件名稱
                </td>
                <td class="tb_w_1" align="left">
                    <asp:DropDownList ID="EditDdl_FileName" runat="server" Width="200px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr runat="server" id="ntr">
                <td class="tb_title_w_1" width="150px">
                    新附件名稱*
                </td>
                <td class="tb_w_1" align="left">
                    <asp:TextBox ID="EditText_FileName" runat="server" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="EditBtn_Save" runat="server" CssClass="button-small" Text="儲存" />
                    <asp:Button ID="EditBtn" runat="server" CssClass="button-small" Text="確定" />
                    <asp:Button ID="EditBtn_Cancel" runat="server" CssClass="button-small" Text="取消" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
