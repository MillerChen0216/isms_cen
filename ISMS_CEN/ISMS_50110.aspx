<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_50110.aspx.vb" Inherits="ISMS_50110" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />

    
</head>
<body >
    <form id="form1" runat="server">

        <br />
                    <asp:HiddenField ID="Hidden_DP" runat="server" />

        <table class="tb_1" id="Show_TB"   style="width: 750px" runat="server" visible="true" cellpadding="0" cellspacing="0">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    使用者管理
                    <asp:Button ID="Show_Insert" runat="server" CssClass="button-small" Text="新增資料" />
                    </td>
            </tr>
            <tr>
                <td >

        <asp:GridView ID="GridView1" runat="server"  CssClass="tb_1"
            AutoGenerateColumns="False"   Width="750px" CellPadding="3" >
            <Columns>
                <asp:BoundField DataField="user_id" HeaderText="帳號" SortExpression="user_id" >
                        <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="user_name" HeaderText="名稱" SortExpression="user_name" >
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="degree_name" HeaderText="職稱" SortExpression="degree_name" >
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                
                <asp:BoundField DataField="dept_name" HeaderText="單位" SortExpression="dept_name" >
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="group_name" HeaderText="群組" SortExpression="group_name" >
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="using" HeaderText="啟用" SortExpression="using" >
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:BoundField DataField="email" HeaderText="電子郵件" SortExpression="email" >
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                 <asp:BoundField DataField="phone" HeaderText="聯絡電話" SortExpression="phone" >
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                 <asp:BoundField DataField="ext" HeaderText="分機" SortExpression="ext" >
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
                <asp:ButtonField ButtonType="Button" CommandName="Disable" Text="啟用/停用" InsertVisible="False" >
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:ButtonField> 
                <asp:ButtonField ButtonType="Button" CommandName="Edit_Btn" InsertVisible="False"
                    Text="編輯" >
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:ButtonField>
                <asp:ButtonField ButtonType="Button" Text="重設密碼" CommandName="Reset_Pwd" InsertVisible="False">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:ButtonField>
                <asp:BoundField DataField="pwd" HeaderText="密碼">
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <ItemStyle CssClass="tb_w_1" />
                </asp:BoundField>
            </Columns>
        </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <table id="pwd_tb" runat="server" cellpadding="3" cellspacing="0" class="tb_1" style="width: 300px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="3">
                    <img src="Images/exe.GIF" />
                    重設密碼
                    <asp:Button ID="Button1" runat="server" CssClass="button-small" Text="確定重設" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    帳號</td>
                <td class="tb_title_w_1">
                    密碼</td>
                <td class="tb_title_w_1">
                    確認密碼</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="useridtxt" runat="server" BackColor="WhiteSmoke" ReadOnly="True"
                        Width="70px"></asp:TextBox></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="pwdtxt1" runat="server" TextMode="Password" Width="100px"></asp:TextBox>
                </td>
                <td class="tb_w_1">
                    <asp:TextBox ID="pwdtxt2" runat="server" TextMode="Password" Width="100px"></asp:TextBox></td>
            </tr>
        </table>
        <br />
        <table id="Edit_TB" class="tb_1" style="width: 750px" runat="server" visible="false" cellpadding="3" cellspacing="0">
            <tr>
                <td class="tb_title_w_2" colspan="8">
                    <img src="Images/exe.GIF" />
                    使用者管理
                    <asp:Button ID="Update_Btn" runat="server" CssClass="button-small" Text="確定編輯" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1" >
                    帳號</td>
                <td class="tb_title_w_1" >
                    名稱</td>
                <td class="tb_title_w_1" >
                    職稱</td>
                <td class="tb_title_w_1" >
                    單位</td>
                <td class="tb_title_w_1">
                    群組</td>
                <td class="tb_title_w_1" >
                    電子郵件</td>
                <td class="tb_title_w_1" >
                    聯絡電話</td>
                <td class="tb_title_w_1" >
                    分機</td>
            </tr>
            <tr>
                <td class="tb_w_1" >
                    <asp:TextBox ID="user_id" runat="server" BackColor="WhiteSmoke" ReadOnly="True" Width="70px" ></asp:TextBox></td>
                <td class="tb_w_1" >
                    <asp:TextBox ID="user_name" runat="server" Width="100px" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="EDIT_LVDName" runat="server" ControlToValidate="user_name"
                        ErrorMessage="名稱不得空白！">*</asp:RequiredFieldValidator></td>
                <td class="tb_w_1" >
                    <asp:DropDownList ID="ddl_degree" runat="server">
                    </asp:DropDownList></td>
                <td class="tb_w_1" >
                    <asp:DropDownList ID="ddl_dept" runat="server">
                    </asp:DropDownList></td>
                <td class="tb_w_1" >
                    <asp:DropDownList ID="ddl_group" runat="server">
                    </asp:DropDownList></td>
                <td class="tb_w_1" >
                    <asp:TextBox ID="email" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="EDIT_LVDMail" runat="server" ControlToValidate="email"
                        ErrorMessage="電子郵件不得空白！">*</asp:RequiredFieldValidator></td>
                <td class="tb_w_1" >
                    <asp:TextBox ID="phone_edit" runat="server" Width="100px">
                    </asp:TextBox></td>
                <td class="tb_w_1" >
                    <asp:TextBox ID="ext_edit" runat="server" Width="50px">
                    </asp:TextBox></td>
            </tr>
        </table>
  
        <table id="Ins_TB" class="tb_1" style="width: 790px" runat="server" visible="false" cellpadding="3" cellspacing="0">
            <tr>
                <td class="tb_title_w_2" colspan="9">
                    <img src="Images/exe.GIF" />
                    使用者管理
                    <asp:Button ID="Insert_Btn" runat="server" CssClass="button-small" Text="確定新增" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1" >
                    帳號</td>
                <td class="tb_title_w_1">
                    密碼</td>
                <td class="tb_title_w_1" >
                    名稱</td>
                <td class="tb_title_w_1" >
                    職稱</td>
                <td class="tb_title_w_1" >
                    單位</td>
                <td class="tb_title_w_1">
                    群組</td>
                <td class="tb_title_w_1" >
                    電子郵件</td>
                <td class="tb_title_w_1" >
                    聯絡電話</td>
                <td class="tb_title_w_1" >
                    分機</td>
            </tr>
            <tr>
                <td class="tb_w_1" >
                    <asp:TextBox ID="user_id_ins" runat="server" Width="70px" style="TEXT-TRANSFORM: uppercase" AutoPostBack="True" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RLV_Userid" runat="server" ControlToValidate="user_id_ins" ErrorMessage="帳號不得空白！">*</asp:RequiredFieldValidator></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="ins_pwdtxt" runat="server" TextMode="Password" Width="70px" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RLV_PWD" runat="server" ControlToValidate="ins_pwdtxt"
                        ErrorMessage="密碼不得空白！">*</asp:RequiredFieldValidator></td>
                 <td class="tb_w_1" >
                    <asp:TextBox ID="user_name_ins" runat="server" Width="100px" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RLV_name" runat="server" ControlToValidate="user_name_ins"
                        ErrorMessage="名稱不得空白！">*</asp:RequiredFieldValidator></td>
                <td class="tb_w_1" >
                    <asp:DropDownList ID="ddl_degree_ins" runat="server">
                    </asp:DropDownList></td>
                <td class="tb_w_1" >
                    <asp:DropDownList ID="ddl_dept_ins" runat="server">
                    </asp:DropDownList></td>
                <td class="tb_w_1" >
                    <asp:DropDownList ID="ddl_group_ins" runat="server">
                    </asp:DropDownList></td>
                <td class="tb_w_1" >
                    <asp:TextBox ID="email_ins" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RVL_Email_txt" runat="server" ControlToValidate="email_ins" ErrorMessage="電子郵件不得空白！">*</asp:RequiredFieldValidator></td>
                <td class="tb_w_1" >
                    <asp:TextBox ID="phone_ins" runat="server" Width="100px" >
                    </asp:TextBox></td>
                <td class="tb_w_1" >
                    <asp:TextBox ID="ext_ins" runat="server" Width="50px" >
                    </asp:TextBox></td>
            </tr>
</table>

                <td class="tb_w_1" >
                    </td>
                <td class="tb_w_1" >
                    </td>
                <td class="tb_w_1" >
                    </td>
                <td class="tb_w_1" >
               </td>
                <td class="tb_w_1" >
                    </td>
                          <td class="tb_w_1" >
                    </td>
    
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="email"
            Display="None" ErrorMessage="不正確的E-Mail格式!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        &nbsp;
        <asp:RegularExpressionValidator ID="RVL_EmailFormat" runat="server" ControlToValidate="email_ins"
            Display="None" ErrorMessage="不正確的E-Mail格式!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>&nbsp;&nbsp;
        <asp:RegularExpressionValidator ID="RVL_PASSWORD" runat="server" ControlToValidate="ins_pwdtxt"
            ErrorMessage="密碼至少8碼含英數字" ValidationExpression="^.*(?=.{8,})(?=.*\d)(?=.*[a-zA-Z]).*$" Display="None"></asp:RegularExpressionValidator>&nbsp;
        <asp:RegularExpressionValidator ID="RVL_PASSWORD1" runat="server" ControlToValidate="pwdtxt1"
            Display="None" ErrorMessage="密碼至少8碼含英數字" ValidationExpression="^.*(?=.{8,})(?=.*\d)(?=.*[a-zA-Z]).*$"></asp:RegularExpressionValidator>
        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="pwdtxt1"
            ControlToValidate="pwdtxt2" Display="None" ErrorMessage="密碼不同請再確認"></asp:CompareValidator>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RVL_EmailFormat" >
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RVL_Email_txt">
        </cc1:ValidatorCalloutExtender><cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="RLV_name">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RLV_Userid">
        </cc1:ValidatorCalloutExtender><cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="RLV_PWD">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="server" TargetControlID="EDIT_LVDName">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="server" TargetControlID="EDIT_LVDMail">
        </cc1:ValidatorCalloutExtender>
                <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="server" TargetControlID="RVL_PASSWORD">
        </cc1:ValidatorCalloutExtender><cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" TargetControlID="RVL_PASSWORD1">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="server" TargetControlID="CompareValidator1">
        </cc1:ValidatorCalloutExtender>
        &nbsp;&nbsp;
    
    </form>
</body>
</html>
