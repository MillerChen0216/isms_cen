﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_50130
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP

        Dim ds As New DataSet
        ds = Common.Get_Group(Hidden_DP.Value)
        If ds.Tables(0).Rows.Count = 0 Then
            Server.Transfer("ISMS_50120.aspx")
        End If

   
        If Not Page.IsPostBack Then
            ddl_group.DataSource = Common.Get_Group(Hidden_DP.Value)
            ddl_group.DataValueField = "group_id"
            ddl_group.DataTextField = "group_name"
            ddl_group.DataBind()

            '109modify加入子系統篩選


            ddl_subsys.DataSource = Common.Get_Parent()
            ddl_subsys.DataValueField = "NoteId"
            ddl_subsys.DataTextField = "sText"
            ddl_subsys.DataBind()
            'Dim newListItem As ListItem
            'newListItem = New ListItem("全部", "")
            'ddl_subsys.Items.Add(newListItem)

            SqlDataSource()
        End If
    End Sub

    Protected Sub SqlDataSource()

        'Power_DS.SelectCommand = "SELECT * FROM Menu_TreeView where Visible=1 order by appid"
        'Power_DS.SelectCommand = " select a.sValue +case when ISNULL(b.sValue,'')='' then '' else  case when b.sTarget='Main' then '' else +' \ '+ b.sValue end end sText, case when ISNULL(c.sValue,'')='' then b.sValue  else  c.sValue end as  sValue, case when ISNULL(c.appid,0)=0 then b.appid  else  c.appid end as  appid,  case when ISNULL(b.sValue,'')='' then a.NoteId  else  b.NoteId end as  NoteId from Menu_TreeView a left join Menu_TreeView b on b.ParentId=a.NoteId  left join Menu_TreeView c on c.ParentId=b.NoteId  and c.Visible=1  where a.ParentId = 0  order by b.ParentId "
        '        Power_DS.ConnectionString = Common.ConnDBS.ConnectionString
        '        GridView1.DataSourceID = Power_DS.ID
        '        Dim KeyNames() As String = {"appId"}
        '        GridView1.DataKeyNames = KeyNames
        'Dim dt As DataTable = Common.Get_auth(ddl_subsys.SelectedValue)
        GridView1.DataSource = Common.Get_auth(ddl_subsys.SelectedValue, ddl_group.SelectedValue)
        GridView1.DataBind()

    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(2).Visible = False
    End Sub

    Protected Sub SetPower_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SetPower_Btn.Click

        Dim group As String = ddl_group.SelectedValue
        For i = 0 To Me.GridView1.Rows.Count - 1
            Dim appId As String = CType(GridView1.Rows(i).FindControl("Hidden_appId"), HiddenField).Value
            Dim appId_Parent As String = Left(GridView1.Rows(i).Cells(2).Text, 3)

            Dim str As String = String.Empty
            Select Case appId_Parent
                Case "101"
                    str = "10100"
                Case "111"
                    str = "11100"
                Case "121"
                    str = "12100"
                Case "201"
                    str = "20100"
                Case "301"
                    str = "30100"
                Case "311"
                    str = "31100"
                Case "401"
                    str = "40100"
                Case "501"
                    str = "50100"
            End Select

            If (String.IsNullOrEmpty(str) = False) Then
                SqlTxt = "DELETE FROM groupApp where group_id = '" + group + "' and  appId = '" + str + "'"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            End If
            ' Response.Write(SqlTxt)

            SqlTxt = "DELETE FROM groupApp where group_id = '" + group + "' and  appId = '" + appId + "' "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

            'Response.Write(SqlTxt)
        Next


        For i = 0 To Me.GridView1.Rows.Count - 1
            Dim appId As String = CType(GridView1.Rows(i).FindControl("Hidden_appId"), HiddenField).Value
            Dim cb As Boolean = CType(Me.GridView1.Rows(i).FindControl("chkSelect2"), CheckBox).Checked
            Dim appId_Parent As String = Left(GridView1.Rows(i).Cells(2).Text, 3)


            'If Left(GridView1.Rows(i).Cells(2).Text, 3) = "101" And CType(Me.GridView1.Rows(i).FindControl("chkSelect2"), CheckBox).Checked = True Then
            If appId_Parent = "101" And cb = True Then
                SqlTxt = "INSERT INTO groupApp ([group_id],[appId],[power]) VALUES ('" + group + "','10100','VIEW')"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()

                'ElseIf Left(GridView1.Rows(i).Cells(2).Text, 3) = "111" And CType(Me.GridView1.Rows(i).FindControl("chkSelect2"), CheckBox).Checked = True Then
            ElseIf appId_Parent = "111" And cb = True Then
                SqlTxt = "INSERT INTO groupApp ([group_id],[appId],[power]) VALUES ('" + group + "','11100','VIEW')"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()

                ' ElseIf Left(GridView1.Rows(i).Cells(2).Text, 3) = "121" And CType(Me.GridView1.Rows(i).FindControl("chkSelect2"), CheckBox).Checked = True Then
            ElseIf appId_Parent = "121" And cb = True Then
                SqlTxt = "INSERT INTO groupApp ([group_id],[appId],[power]) VALUES ('" + group + "','12100','VIEW')"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()

                '1091119modify缺資產管理加入母項目
                'ElseIf Left(GridView1.Rows(i).Cells(2).Text, 3) = "201" And CType(Me.GridView1.Rows(i).FindControl("chkSelect2"), CheckBox).Checked = True Then
            ElseIf appId_Parent = "201" And cb = True Then
                SqlTxt = "INSERT INTO groupApp ([group_id],[appId],[power]) VALUES ('" + group + "','20100','VIEW')"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()

                'ElseIf Left(GridView1.Rows(i).Cells(2).Text, 3) = "301" And CType(Me.GridView1.Rows(i).FindControl("chkSelect2"), CheckBox).Checked = True Then
            ElseIf appId_Parent = "301" And cb = True Then
                SqlTxt = "INSERT INTO groupApp ([group_id],[appId],[power]) VALUES ('" + group + "','30100','VIEW')"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()

                'ElseIf Left(GridView1.Rows(i).Cells(2).Text, 3) = "311" And CType(Me.GridView1.Rows(i).FindControl("chkSelect2"), CheckBox).Checked = True Then
            ElseIf appId_Parent = "311" And cb = True Then
                SqlTxt = "INSERT INTO groupApp ([group_id],[appId],[power]) VALUES ('" + group + "','31100','VIEW')"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()

                'ElseIf Left(GridView1.Rows(i).Cells(2).Text, 3) = "401" And CType(Me.GridView1.Rows(i).FindControl("chkSelect2"), CheckBox).Checked = True Then
            ElseIf appId_Parent = "401" And cb = True Then
                SqlTxt = "INSERT INTO groupApp ([group_id],[appId],[power]) VALUES ('" + group + "','40100','VIEW')"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()

                'ElseIf Left(GridView1.Rows(i).Cells(2).Text, 3) = "501" And CType(Me.GridView1.Rows(i).FindControl("chkSelect2"), CheckBox).Checked = True Then
            ElseIf appId_Parent = "501" And cb = True Then
                SqlTxt = "INSERT INTO groupApp ([group_id],[appId],[power]) VALUES ('" + group + "','50100','VIEW')"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()

            End If
            'Response.Write(SqlTxt)
            'If CType(Me.GridView1.Rows(i).FindControl("chkSelect2"), CheckBox).Checked = True And GridView1.DataKeys(i).Value.ToString <> "" Then
            If cb = True And appId <> "" Then
                'SqlTxt = "INSERT INTO groupApp ([group_id],[appId],[power]) VALUES ('" + group + "','" + GridView1.DataKeys(i).Value.ToString + "','VIEW')"
                SqlTxt = "INSERT INTO groupApp ([group_id],[appId],[power]) VALUES ('" + group + "','" + appId + "','VIEW')"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                'Response.Write(SqlTxt)
            End If
        Next


        Dim Ds, Ds1 As New DataSet
        Ds = Common.Get_MenuTreeView(group, 1)
        Ds1 = Common.Get_MenuTreeView(group, 3)

        If Ds.Tables(0).Rows.Count >= 1 Then

            SqlTxt = "INSERT INTO groupApp ([group_id],[appId],[power]) VALUES ('" + group + "','10000','VIEW')"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

        End If



        If Ds1.Tables(0).Rows.Count >= 1 Then

            SqlTxt = "INSERT INTO groupApp ([group_id],[appId],[power]) VALUES ('" + group + "','30000','VIEW')"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

        End If

        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "SETUP", "設定權限" + ddl_group.SelectedItem.Text)
        SqlDataSource()
        Common.showMsg(Me.Page, "權限設定完成!")

    End Sub



    Protected Sub ddl_group_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_group.SelectedIndexChanged
        SqlDataSource()
        ' CheckBox_Selected() 
    End Sub
    Protected Sub ddl_subsys_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_subsys.SelectedIndexChanged
        '  CheckBox_Selected()
        '  Me.GridView1.DataBind()
        SqlDataSource()
    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        '  CheckBox_Selected()
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cb As String = CType(e.Row.FindControl("Hidden_cb"), HiddenField).Value
            ' Response.Write(cb)
            Dim chkSelect2 As CheckBox = CType(e.Row.FindControl("chkSelect2"), CheckBox)
            If cb = "Y" Then
                chkSelect2.Checked = True
            Else
                chkSelect2.Checked = False
            End If
        End If
    End Sub

    Protected Sub CheckBox_Selected() '查詢Group的權限,將已設定的權限打勾
        For i = 0 To Me.GridView1.Rows.Count - 1
             
            Dim cb As String = CType(Me.GridView1.Rows(i).FindControl("Hidden_cb"), HiddenField).Value
            If cb = "Y" Then
                CType(Me.GridView1.Rows(i).FindControl("chkSelect2"), CheckBox).Checked = True
            Else
                CType(Me.GridView1.Rows(i).FindControl("chkSelect2"), CheckBox).Checked = False 
            End If
        Next

        'SqlTxt = "SELECT Menu_TreeView.sText, Menu_TreeView.sValue, groupApp.appId FROM Menu_TreeView INNER JOIN groupApp ON Menu_TreeView.appId = groupApp.appId WHERE (groupApp.group_id = '" + ddl_group.SelectedValue + "')"
        ''Response.Write(SqlTxt)
        'SqlCmd = New SqlCommand(SqlTxt, Conn)
        'Conn.Open()
        'SqlDr = SqlCmd.ExecuteReader

        'While SqlDr.Read

        '    For i = 0 To Me.GridView1.Rows.Count - 1

        '        If SqlDr.Item("appId") = CType(Me.GridView1.Rows(i).FindControl("Hidden_appId"), HiddenField).Value Then
        '            CType(Me.GridView1.Rows(i).FindControl("chkSelect2"), CheckBox).Checked = True
        '        End If

        '    Next

        'End While

        'SqlDr.Close()
        'Conn.Close()

    End Sub
End Class
