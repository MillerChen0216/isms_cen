﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class FireWall
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim File_Path As String = ConfigurationManager.AppSettings("FireWall_Path")
    Dim dp As String = Me.Get_DP
    Dim apprvid As String = ConfigurationManager.AppSettings("flow_FireWall") '8
    Dim uid As String = User.Identity.Name
    Dim uname As String = Common.Get_User_Name(User.Identity.Name.ToString)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        If Not IsPostBack Then
            If Session("success") = "Y" Then
                Common.showMsg(Me.Page, "儲存成功！")
                Session("success") = ""
            End If
            empName.Text = uname '申請人
            GdvShow() '載入流程表
        End If
    End Sub

    '載入流程表
    Private Sub GdvShow()
        Try
            Dim dt As DataTable = Common.Qry_workflowName_isms(apprvid, dp)
            GridView.DataSource = dt
            GridView.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料

            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            '***意見
            Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text

            '***Start
            Select Case o_wf_order
                Case "0"
                    '人員
                    pan_user.Visible = True : ddl_user.Visible = False : lb_userid.Text = uid
                    '狀態
                    ddl_status.Visible = True : lb_status.Visible = False
                    ddl_status.DataSource = Common.Get_WFStatus(0)
                    ddl_status.DataValueField = "wfstatus_id" : ddl_status.DataTextField = "wfstatus_name" : ddl_status.DataBind()
                    '意見
                    txt_comment.Visible = True : lb_comment.Visible = False
                Case "1"
                    Dim dt_WorkFlow As DataTable = Common.Get_Wf_User1_isms(apprvid, o_wf_order, "")
                    ddl_user.Visible = True : pan_user.Visible = False
                    ddl_user.Items.Clear()
                    ddl_user.DataValueField = "wf_userid" : ddl_user.DataTextField = "user_name"
                    ddl_user.DataSource = dt_WorkFlow : ddl_user.DataBind()
            End Select


            If pan_user.Visible = True And lb_userid.Text <> "" Then
                lb_username.Text = ISMS.Common.notNull(Common.Get_User_Name(lb_userid.Text))
                If lb_username.Text = "" Then lb_username.Text = Common.Get_venderemp_Name(lb_userid.Text) '表示非員工，可能為廠商
            End If
        End If
    End Sub

    '送出審核
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            If (Not String.IsNullOrEmpty(startTime.Text)) And (Not String.IsNullOrEmpty(endTime.Text)) Then
                If DateTime.Parse(endTime.Text) < DateTime.Parse(startTime.Text) Or DateTime.Parse(applyDate.Text) > DateTime.Parse(startTime.Text) Then
                    Common.showMsg(Me.Page, "請符合結束時間>=開始日期>=申請日期")
                    Return
                End If
            End If
            If String.IsNullOrEmpty(applyDate.Text) Or String.IsNullOrEmpty(applyOrg.Text) Or String.IsNullOrEmpty(applyName.Text) _
                Or (String.IsNullOrEmpty(applyTel1.Text) And String.IsNullOrEmpty(applyTel2.Text)) Or String.IsNullOrEmpty(applyReason.Text) Or String.IsNullOrEmpty(F_Ip.Text) _
                Or String.IsNullOrEmpty(F_Port.Text) Or String.IsNullOrEmpty(T_Ip.Text) Or String.IsNullOrEmpty(T_Port.Text) Or String.IsNullOrEmpty(Radio_Connected.Text) Then
                Dim msg As String = ""
                If String.IsNullOrEmpty(applyDate.Text) Then
                    msg += "申請日期"
                End If
                If String.IsNullOrEmpty(applyOrg.Text) Then
                    If msg = "" Then
                        msg = "申請單位"
                    Else
                        msg += ", 申請單位"
                    End If
                End If
                If String.IsNullOrEmpty(applyName.Text) Then
                    If msg = "" Then
                        msg = "申請人員"
                    Else
                        msg += ", 申請人員"
                    End If
                End If
                If (String.IsNullOrEmpty(applyTel1.Text) And String.IsNullOrEmpty(applyTel2.Text)) Then
                    If msg = "" Then
                        msg = "連絡電話"
                    Else
                        msg += ", 連絡電話"
                    End If
                End If
                If String.IsNullOrEmpty(applyReason.Text) Then
                    If msg = "" Then
                        msg = "申請事由"
                    Else
                        msg += ", 申請事由"
                    End If
                End If
                If String.IsNullOrEmpty(F_Ip.Text) Then
                    If msg = "" Then
                        msg = "來源IP"
                    Else
                        msg += ", 來源IP"
                    End If
                End If
                If String.IsNullOrEmpty(F_Port.Text) Then
                    If msg = "" Then
                        msg = "來源Port"
                    Else
                        msg += ", 來源Port"
                    End If
                End If
                If String.IsNullOrEmpty(T_Ip.Text) Then
                    If msg = "" Then
                        msg = "目的IP"
                    Else
                        msg += ", 目的IP"
                    End If
                End If
                If String.IsNullOrEmpty(T_Port.Text) Then
                    If msg = "" Then
                        msg = "目的Port"
                    Else
                        msg += ", 目的Port"
                    End If
                End If
                If String.IsNullOrEmpty(Radio_Connected.Text) Then
                    If msg = "" Then
                        msg = "單/雙向連接"
                    Else
                        msg += ", 單/雙向連接"
                    End If
                End If
                Common.showMsg(Me.Page, "請確實填寫" + msg)
                Return
            End If

            If GridView.Rows.Count < 1 Then
                Common.showMsg(Me.Page, "請先至(系統管理->審核流程管理)設定關卡流程")
                Return
            End If

            Dim Now As String = Common.YearYYYY(applyDate.Text) '日期
            Dim applyDate_Chn As String = Common.Year(Now).Replace("/", "") '民國YYYMMDD
            Dim sql = "SELECT * FROM Firewall WHERE applyDate_Chn = '" + applyDate_Chn + "' ORDER BY recordId DESC"
            Dim data As DataTable = Common.Con(sql)
            Dim applyDate_Chn_SN = 1
            If Not (data.Rows.Count = 0) Then
                applyDate_Chn_SN = Integer.Parse(Right(data.Rows(0)("recordId"), 3)) + 1 'SN
            End If
            Dim recordid As String = "A4-" + applyDate_Chn + "-" + applyDate_Chn_SN.ToString.PadLeft(3, "0") '表單編號

            Dim startdate = ""
            Dim enddate = ""
            If Not String.IsNullOrEmpty(startTime.Text) Then
                If DateTime.Parse(applyDate.Text) > DateTime.Parse(startTime.Text) Then
                    Common.showMsg(Me.Page, "請符合結束時間>=開始日期>=申請日期")
                    Return
                End If
                startdate = "'" + Common.YearYYYY(startTime.Text) + "'"
            Else
                startdate = "null"
            End If
            If Not String.IsNullOrEmpty(endTime.Text) Then
                If DateTime.Parse(applyDate.Text) > DateTime.Parse(endTime.Text) Then
                    Common.showMsg(Me.Page, "請符合結束時間>=開始日期>=申請日期")
                    Return
                End If
                enddate = "'" + Common.YearYYYY(endTime.Text) + "'"
            Else
                enddate = "null"
            End If
            '處理上傳檔案
            If CType(Me.FindControl("File_applyFile"), FileUpload).HasFile Then
                '有檔案
                Dim oriFileName = CType(Me.FindControl("File_applyFile"), FileUpload)
                Dim UpFileName As String = Common.UpFile(File_Path, recordid, CType(Me.FindControl("File_applyFile"), FileUpload))
                '塞入主表資料
                SqlTxt = "insert into Firewall (recordId,applyDate,applyDate_Chn,applyDate_Chn_SN,empUid,empName,applyOrg,applyName,applyFile_ori_name,applyFile,applyTel1,applyTel2,startTime,endTime,applyReason,F_Ip,F_Port,T_Ip,T_Port,Connected,using,create_time,op_time) "
                SqlTxt += "VALUES('" + recordid + "','" + Now + "','" + applyDate_Chn + "','" + applyDate_Chn_SN.ToString + "','" + uid + "','" + uname + "','" + applyOrg.Text + "','" + applyName.Text + "','" + oriFileName.FileName + "','"
                SqlTxt += UpFileName + "','" + applyTel1.Text + "','" + applyTel2.Text + "'," + startdate + "," + enddate + ",'" + applyReason.Text + "','" + F_Ip.Text
                SqlTxt += "','" + F_Port.Text + "','" + T_Ip.Text + "','" + T_Port.Text + "','" + Radio_Connected.SelectedValue + "','Y',getdate(),getdate())"

            Else
                '沒有檔案
                '塞入主表資料
                SqlTxt = "insert into Firewall (recordId,applyDate,applyDate_Chn,applyDate_Chn_SN,empUid,empName,applyOrg,applyName,applyTel1,applyTel2,startTime,endTime,applyReason,F_Ip,F_Port,T_Ip,T_Port,Connected,using,create_time,op_time) "
                SqlTxt += "VALUES('" + recordid + "','" + Now + "','" + applyDate_Chn + "','" + applyDate_Chn_SN.ToString + "','" + uid + "','" + uname + "','" + applyOrg.Text + "','" + applyName.Text + "','"
                SqlTxt += applyTel1.Text + "','" + applyTel2.Text + "'," + startdate + "," + enddate + ",'" + applyReason.Text + "','" + F_Ip.Text
                SqlTxt += "','" + F_Port.Text + "','" + T_Ip.Text + "','" + T_Port.Text + "','" + Radio_Connected.SelectedValue + "','Y',getdate(),getdate())"

            End If


            '申請人
            SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype ,dp) values "
            SqlTxt += " ('" + apprvid + "','" + recordid + "','" + CType(GridView.Rows(0).FindControl("wf_order"), Label).Text + "','" + uid + "',"
            SqlTxt += "'" + CType(GridView.Rows(0).FindControl("ddl_status"), DropDownList).SelectedValue + "','" + CType(GridView.Rows(0).FindControl("txb_comment"), TextBox).Text + "',getdate() ,'0', 'vender','" + dp + "') "


            '下一位審核人
            If GridView.Rows.Count >= 2 Then
                SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype ,dp) values "
                SqlTxt += " ('" + apprvid + "','" + recordid + "','" + CType(GridView.Rows(1).FindControl("wf_order"), Label).Text + "','" + CType(GridView.Rows(1).FindControl("ddl_user"), DropDownList).SelectedValue + "',"
                SqlTxt += " '1', '',getdate() ,'1', 'land','" + dp + "') "
            End If

            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Session("success") = "Y"
            Response.Redirect("FireWall.aspx", True)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub
End Class
