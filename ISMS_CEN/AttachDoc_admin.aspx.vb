﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO
Partial Class AttachDoc_admin
    Inherits PageBase
    'Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim uid As String = User.Identity.Name '使用者名稱
    Dim uname As String = Common.Get_User_Name(uid) '使用者代號
    Dim apprvid As String
    Dim approvalid As String
    Dim dp As String = Get_DP()
    Dim File_Path As String = ConfigurationManager.AppSettings("AttachDoc_Path")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        approvalid = ConfigurationManager.AppSettings("flow_AttachDoc") '8
        apprvid = ConfigurationManager.AppSettings("flow_AttachDoc") '8
        Try
            If Not IsPostBack Then
                txb_sdate.Text = Common.Year(Now.AddDays(-30))
                txb_edate.Text = Common.Year(Now.AddDays(1))
                SqlDataSource() '載入管理者清單
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '載入管理者清單
    Protected Sub SqlDataSource()

        Dim sql As String = "SELECT * FROM AttachDoc where using='Y'"
        '排除已撤銷之申請單
        '排除停用帳號
        sql += " and empUid in (select user_id from ismsUser where using='Y') "
        If txb_username.Text <> "" Then
            sql += " and empName like'%" + Trim(txb_username.Text) + "%'"
        End If
        If txb_sdate.Text <> "" Then
            sql += " and applyDate >= '" + Common.YearYYYY(txb_sdate.Text) + "'"
        End If
        If txb_edate.Text <> "" Then
            sql += " and applyDate <= '" + Common.YearYYYY(txb_edate.Text) + "'"
        End If

        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = sql

        GridView1.DataSourceID = SqlDataSource1.ID

    End Sub

    '申請清單民國年處理
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '民國年轉換
                Dim lb_applydate As String = CType(e.Row.FindControl("lb_applydate"), Label).Text
                CType(e.Row.FindControl("lb_applydate"), Label).Text = Common.Year(Date.Parse(lb_applydate))
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '查詢
    Protected Sub but_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_search.Click
        Try
            SqlDataSource()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub


    '查看或撤銷
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
        Dim hidden_recordId As HiddenField = (TryCast(SelectedRow.FindControl("hidden_recordId"), HiddenField))
        Select Case e.CommandName

            Case "audit_btn"
                Try
                    GdvShow()
                    Dim sql = "  SELECT * FROM AttachDoc WHERE recordId = '" + hidden_recordId.Value + "'"
                    Dim data As DataTable = Common.Con(sql)
                    If data.Rows.Count > 0 Then
                        lb_recordid.Text = hidden_recordId.Value
                        GdvShow()
                        atdt_Name_DataSource()
                        applyDate.Text = Common.Year(data.Rows(0)("applyDate"))
                        recordId.Text = hidden_recordId.Value
                        empName.Text = data.Rows(0)("empName")
                        atdt_Name.SelectedValue = data.Rows(0)("inp_atdt_Id")
                        atdt_Name.Enabled = False
                        comment.Text = data.Rows(0)("comment")
                        comment.Enabled = False
                        images1_link.Text = data.Rows(0)("images1")
                    Else
                        Response.Redirect("AttachDoc_admin.aspx", True) '如果沒有這個recordId就重新導向管理者清單
                    End If
                    tb_result.Visible = True
                    tb_query.Visible = False
                    GridView1.Visible = False
                Catch ex As Exception
                    lb_msg.Text = ex.Message
                End Try
            Case "using_btn"
                Try
                    Dim sql As New StringBuilder
                    sql.Append("update AttachDoc set using='N' where recordId ='" + hidden_recordId.Value + "' ")
                    sql.Append("  update form_record set status = -1 where recordId = '" + hidden_recordId.Value + "' and approvalId = '" + approvalid + "' and status = 1")

                    Dim conn As SqlConnection = Nothing : Dim cmd As New SqlCommand(sql.ToString)
                    Try
                        conn = Common.ConnDBS : conn.Open() : cmd.Connection = conn
                        If cmd.ExecuteNonQuery() Then GridView1.DataBind() : lb_msg.Text = "撤銷成功！"
                    Catch ex As Exception
                        lb_msg.Text = "撤銷失敗！" + ex.Message
                    Finally
                        conn.Close() : conn.Close() : conn.Dispose() : cmd.Dispose()
                    End Try
                Catch ex As Exception
                    lb_msg.Text = ex.Message
                End Try


        End Select
    End Sub

    '關卡
    Private Sub GdvShow()
        Dim dt As DataTable = Common.Qry_workflowName_isms_Req(approvalid, lb_recordid.Text, apprvid, dp)
        GridView.DataSource = dt
        GridView.DataBind()

    End Sub

    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料

            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            Dim hid_status As HiddenField = CType(e.Row.FindControl("hid_status"), HiddenField)
            '***意見
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text
            '***審核人員***
            ' Dim lb_wf_inside_order As String = CType(e.Row.FindControl("lb_wf_inside_order"), Label).Text

            Dim ds As DataSet

            '***Start
            If o_wf_order = "0" And hid_status.Value <> "1" Then
                pan_user.Visible = True
                '將審核人員設為自己
                Dim sql = "    SELECT user_id, user_name FROM ismsUser WHERE user_id = '" + lb_username.Text + "'"
                Dim data As DataTable = Common.Con(sql)
                CType(e.Row.FindControl("lb_username"), Label).Text = data.Rows(0)("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = data.Rows(0)("user_id")
            ElseIf hid_status.Value = "1" Then '待審核
                ds = Common.Qry_form_record_wait(lb_recordid.Text)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
            ElseIf hid_status.Value = "2" Or hid_status.Value = "9" Then '審核已通過
                ds = Common.Qry_form_record_name(lb_recordid.Text, o_wf_order)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
            End If

            If hid_status.Value = "" Then
                lb_status.Text = ""
            Else
                CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(hid_status.Value))
            End If
        End If
    End Sub

    '表單項目資料來源
    Private Sub atdt_Name_DataSource()
        Try
            Dim sql As String = "SELECT * FROM AttachDocType WHERE atdt_Enabled = 1"
            sql += " ORDER BY atdt_time DESC "
            atdt_Name.Items.Clear()
            atdt_Name.DataTextField = "atdt_Name"
            atdt_Name.DataValueField = "atdt_Id"
            atdt_Name.DataSource = Common.Con(sql)
            atdt_Name.DataBind()
            atdt_Name.SelectedValue = atdt_Name.Items(0).Value '預設選項為第一個值
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '1_檔案下載_按鈕
    Protected Sub images1_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles images1_Btn.Click
        Try
            Common.DownFile(Me.Page, File_Path + images1_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_回列表_按鈕
    Protected Sub but_esc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_esc.Click
        Response.Redirect("AttachDoc_admin.aspx", True)
    End Sub
End Class
