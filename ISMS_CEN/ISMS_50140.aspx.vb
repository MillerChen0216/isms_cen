﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_50140
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand

    Protected Sub Show_Insert_degree_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert_degree.Click
        Show_TB_degree.Visible = False
        Show_TB_dept.Visible = False
        Ins_TB_degree.Visible = True
        Edit_TB_dp.Visible = False
        degree_Name_Ins.Text = ""
    End Sub

    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click
        If degree_Name_Edit.Text = "" Then
            Common.showMsg(Me.Page, "資料欄位不允許空白")
        Else
            SqlTxt = "UPDATE degree  SET  degree_name ='" + degree_Name_Edit.Text + "' where degree_id = '" + hidden_degreeid.Value + "' and dp='" + Hidden_DP.Value + "'"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", degree_Name_Edit.Text)

            GridView1.DataBind()
            Show_TB_degree.Visible = True
            Show_TB_dept.Visible = True
            Edit_TB_dp.Visible = True
            Edit_TB_degree.Visible = False
        End If
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
        SelectedRow.Cells(3).Visible = True
        hidden_degreeid.Value = SelectedRow.Cells(3).Text


        Select Case e.CommandName

            Case "Del_Btn"

                SqlTxt = "DELETE FROM degree where degree_id ='" + hidden_degreeid.Value + "' and  dp='" + Hidden_DP.Value + "'"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DEL", SelectedRow.Cells(0).Text)

                GridView1.DataBind()

            Case "Edit_Btn"

                Edit_TB_degree.Visible = True
                Show_TB_dept.Visible = False
                Show_TB_degree.Visible = False
                Edit_TB_dp.Visible = False
                degree_Name_Edit.Text = SelectedRow.Cells(0).Text


        End Select
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(3).Visible = False
    End Sub


    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim oDataRow As Data.DataRowView
        Dim sShowField As String
        Dim sMessage As String
        If e.Row.RowIndex <> -1 Then
            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = oDataRow.Item("degree_name").ToString()
            sMessage = String.Format("您確定要刪除 [{0}] 嗎?", sShowField)
            CType(e.Row.Cells(2).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"
        End If
    End Sub

    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        If degree_Name_Ins.Text = "" Then
            Common.showMsg(Me.Page, "職稱不允許空白")
        Else
            SqlTxt = "INSERT INTO degree (dp,[degree_name]) VALUES ('" + Hidden_DP.Value + "','" + degree_Name_Ins.Text + "') "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", degree_Name_Ins.Text)

            GridView1.DataBind()
            Show_TB_dept.Visible = True
            Show_TB_degree.Visible = True
            Edit_TB_dp.Visible = True
            Ins_TB_degree.Visible = False
        End If
    End Sub


    Protected Sub Show_Ins_dep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Ins_dep.Click
        Ins_TB_dept.Visible = True
        Show_TB_dept.Visible = False
        Show_TB_degree.Visible = False
        Edit_TB_dp.Visible = False
        Show_ddl()
    End Sub

    Protected Sub Show_ddl()

        'ddl_below_ins.DataSource = Common.Get_dept(Hidden_DP.Value)
        'ddl_below_ins.DataValueField = "dept_id"
        'ddl_below_ins.DataTextField = "dept_name"
        'ddl_below_ins.DataBind()

        'ddl_chief_ins.DataSource = Common.Get_User(Hidden_DP.Value)
        'ddl_chief_ins.DataValueField = "user_id"
        'ddl_chief_ins.DataTextField = "user_name"
        'ddl_chief_ins.DataBind()


    End Sub



    Protected Sub Ins_BtnDep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ins_BtnDep.Click
        If dept_name_ins.Text = "" Then
            Common.showMsg(Me.Page, "部門名稱不允許空白")
        Else


            SqlTxt = "INSERT INTO deptment  (dp,[dept_name]) VALUES ('" + Hidden_DP.Value + "','" + dept_name_ins.Text + "') "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", dept_name_ins.Text)

            GridView2.DataBind()
            Show_TB_dept.Visible = True
            Show_TB_degree.Visible = True
            Ins_TB_dept.Visible = False
            Edit_TB_dp.Visible = True
        End If
    End Sub

    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView2.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView2.Rows(Index)
        SelectedRow.Cells(5).Visible = True
        hidden_deptid.Value = SelectedRow.Cells(5).Text


        Select Case e.CommandName

            Case "Del_Btn"

                SqlTxt = "DELETE FROM deptment where dept_id ='" + hidden_deptid.Value + "' and dp='" + Hidden_DP.Value + "' "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DEL", SelectedRow.Cells(0).Text)

                GridView2.DataBind()

            Case "Edit_Btn"

                Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM deptment where dept_id ='" + SelectedRow.Cells(5).Text + "' and dp='" + Hidden_DP.Value + "'", Conn)
                Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
                mycmd.SelectCommand = selectCMD
                Dim myDs As DataSet = New DataSet()

                mycmd.Fill(myDs, "deptment")


                Edit_TB_dept.Visible = True
                Show_TB_degree.Visible = False
                Show_TB_dept.Visible = False
                Edit_TB_dp.Visible = False
                dept_name_Edit.Text = SelectedRow.Cells(0).Text

                'ddl_below_edit.DataSource = Common.Get_dept(Hidden_DP.Value)
                'ddl_below_edit.DataValueField = "dept_id"
                'ddl_below_edit.DataTextField = "dept_name"
                'ddl_below_edit.DataBind()

                'ddl_chief_edit.DataSource = Common.Get_User(Hidden_DP.Value)
                'ddl_chief_edit.DataValueField = "user_id"
                'ddl_chief_edit.DataTextField = "user_name"
                'ddl_chief_edit.DataBind()
                'ddl_chief_edit.SelectedValue = myDs.Tables(0).Rows(0).Item("chief")

                'ddl_below_edit.SelectedValue = myDs.Tables(0).Rows(0).Item("below")

        End Select
    End Sub

    Protected Sub GridView2_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowCreated
        e.Row.Cells(5).Visible = False
        e.Row.Cells(1).Visible = False
        e.Row.Cells(2).Visible = False
    End Sub

    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound
        Dim oDataRow As Data.DataRowView
        Dim sShowField As String
        Dim sMessage As String
        If e.Row.RowIndex <> -1 Then
            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = oDataRow.Item("dept_name").ToString()
            sMessage = String.Format("您確定要刪除 [{0}] 嗎?", sShowField)
            CType(e.Row.Cells(4).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"
            'e.Row.Cells(1).Text = Common.Get_dept_Name(e.Row.Cells(1).Text) '部門名稱
            'e.Row.Cells(2).Text = Common.Get_User_Name(e.Row.Cells(2).Text) '帳號名稱
        End If
    End Sub

    Protected Sub Edit_BtnDep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Edit_BtnDep.Click
        If dept_name_Edit.Text = "" Then
            Common.showMsg(Me.Page, "部門名稱不允許空白")
        Else

            SqlTxt = "UPDATE deptment  SET  dept_name ='" + dept_name_Edit.Text + "' where dept_id = '" + hidden_deptid.Value + "' and  dp='" + Hidden_DP.Value + "'"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", dept_name_Edit.Text)
            GridView2.DataBind()
            Show_TB_degree.Visible = True
            Show_TB_dept.Visible = True
            Edit_TB_dept.Visible = False
            Edit_TB_dp.Visible = True
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        SqlDataSource()
        If Not (Page.IsPostBack) Then
            SqlDataSource3()
        End If

    End Sub
    Protected Sub SqlDataSource()

        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource2.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = "SELECT * FROM degree where dp='" + Hidden_DP.Value + "'"
        SqlDataSource2.SelectCommand = "SELECT * FROM deptment where dp='" + Hidden_DP.Value + "'"

        GridView1.DataSourceID = SqlDataSource1.ID
        GridView2.DataSourceID = SqlDataSource2.ID

    End Sub
    Protected Sub SqlDataSource3()

        Dim selectCMD As SqlCommand = New SqlCommand("SELECT dp_id,dp_name,dp_mail FROM DP_DATA where dp_id='" + Hidden_DP.Value + "'", Conn)
        Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
        mycmd.SelectCommand = selectCMD
        Dim myDs As DataSet = New DataSet()

        mycmd.Fill(myDs, "DP_DATA")
        dp_name_Edit.Text = myDs.Tables(0).Rows(0).Item("dp_name").ToString
        dp_mail_Edit.Text = myDs.Tables(0).Rows(0).Item("dp_mail").ToString
        Conn.Close()
    End Sub

    Protected Sub Edit_BtnDP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Edit_BtnDP.Click
        SqlTxt = "UPDATE DP_Data  SET  dp_mail ='" + dp_mail_Edit.Text + "' where dp_id = '" + Hidden_DP.Value + "'"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", dp_mail_Edit.Text)

    End Sub
End Class
