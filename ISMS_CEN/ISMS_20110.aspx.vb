﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class ISMS_20110
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand

    Protected Sub SqlDataSource()
        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = "SELECT * FROM [asset]  ORDER BY asset_id"
        GridView1.DataSourceID = SqlDataSource1.ID
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        SqlDataSource()
    End Sub

    Protected Sub Show_Insert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert.Click
        Show_TB.Visible = False
        Ins_TB.Visible = True
    End Sub

    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        If asset_id_Ins.Text = "" Then
            Common.showMsg(Me.Page, "大分類代碼不允許空白")
        Else
            If asset_name_Ins.Text = "" Then
                Common.showMsg(Me.Page, "大分類名稱不允許空白")
            Else
                SqlTxt = "INSERT INTO [asset]  (asset_id,asset_name,asset_Ename,asset_desc) VALUES ('" + asset_id_Ins.Text + "' , '" + asset_name_Ins.Text + "' ,'" + asset_Ename_Ins.Text + "','" + asset_desc_Ins.Text + "' ) "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", asset_name_Ins.Text)

                GridView1.DataBind()
                asset_id_Ins.Text = ""
                asset_name_Ins.Text = ""
                Show_TB.Visible = True
                Ins_TB.Visible = False

            End If

        End If
    End Sub

    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click
        If asset_name_Edit.Text = "" Then
            Common.showMsg(Me.Page, "大分類名稱不允許空白")
        Else

            SqlTxt = "UPDATE [asset]  SET  asset_name ='" + asset_name_Edit.Text + "' ,asset_Ename='" + asset_ename_Edit.Text + "', asset_desc='" + asset_desc_Edit.Text + "' where asset_id ='" + Hidden_Assetid.Value + "' "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

            GridView1.DataBind()
            Show_TB.Visible = True
            Edit_TB.Visible = False
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", asset_name_Edit.Text)

        End If
    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)

        Hidden_Assetid.Value = SelectedRow.Cells(0).Text


        Select Case e.CommandName

            Case "Del_Btn"
                SqlTxt = "DELETE FROM [asset] where asset_id ='" + Hidden_Assetid.Value + "'"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DEL", SelectedRow.Cells(1).Text)
                GridView1.DataBind()


            Case "Edit_Btn"

                Edit_TB.Visible = True
                Show_TB.Visible = False

                asset_id_Edit.Text = SelectedRow.Cells(0).Text
                asset_name_Edit.Text = SelectedRow.Cells(1).Text
                asset_ename_Edit.Text = SelectedRow.Cells(2).Text
                asset_desc_Edit.Text = SelectedRow.Cells(3).Text.Replace("&nbsp;", "")


        End Select
    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim oDataRow As Data.DataRowView
        Dim sShowField As String
        Dim sMessage As String
        If e.Row.RowIndex <> -1 Then
            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = oDataRow.Item("asset_name").ToString()
            sMessage = String.Format("您確定要刪除 [{0}] 嗎?", sShowField)
            CType(e.Row.Cells(5).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"
        End If
    End Sub
End Class
