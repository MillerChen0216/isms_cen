﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_31130
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        Dim Ds1 As New DataSet

        If Not Page.IsPostBack Then
            ddl_Asset.DataSource = Common.Get_Asset()
            ddl_Asset.DataValueField = "asset_id"
            ddl_Asset.DataTextField = "asset_name"
            ddl_Asset.DataBind()

        End If

        Ds1 = Common.Get_riskEvaluate_AssetItem(Hidden_DP.Value, ddl_Asset.SelectedValue, "")
        If Ds1.Tables(0).Rows.Count = 0 Then
            Show_Insert.Visible = False
        Else
            Show_Insert.Visible = True
        End If
        SqlDataSource()
        Dim ds As New DataSet
        ds = Common.Get_Asset()
        If ds.Tables(0).Rows.Count = 0 Then
            Server.Transfer("ISMS_20110.aspx")
        End If

    End Sub

    Protected Sub SqlDataSource()
        SqlDataSource1.ConnectionString = Conn.ConnectionString
        SqlDataSource1.SelectCommand = "SELECT a.id,(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability) as assitScore,a.risk_type,a.assitem_id,b.assitem_name,a.weakness_id,d.weakness,e.threat,a.threat_id,a.riskp_score,a.impact_score FROM riskEvaluate as a,assetItem as b,weakness as d,threat as e WHERE b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id AND a.dp=b.dp AND b.dp='" + Hidden_DP.Value + "' AND b.asset_id=" + ddl_Asset.SelectedValue + " ORDER BY a.assitem_id"
        'Response.Write("SELECT a.id,(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability) as assitScore,a.risk_type,a.assitem_id,b.assitem_name,a.weakness_id,d.weakness,e.threat,a.threat_id,a.riskp_score,a.impact_score FROM riskEvaluate as a,assetItem as b,weakness as d,threat as e WHERE b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id AND b.asset_id=" + ddl_Asset.SelectedValue + " ORDER BY a.assitem_id")
        GridView1.DataSourceID = SqlDataSource1.ID
        Dim KeyNames() As String = {"assitem_id"}
        GridView1.DataKeyNames = KeyNames
    End Sub
    Protected Sub ShowInsddl()
        ddl_assitem_id_ins.DataSource = Common.Get_riskEvaluate_AssetItem(Hidden_DP.Value, ddl_Asset.SelectedValue, "")
        ddl_assitem_id_ins.DataValueField = "assitem_id"
        ddl_assitem_id_ins.DataTextField = "assitem_name"
        ddl_assitem_id_ins.DataBind()

        DDLRisk_typeIns.DataSource = Common.Get_Asset()
        DDLRisk_typeIns.DataValueField = "asset_id"
        DDLRisk_typeIns.DataTextField = "asset_name"
        DDLRisk_typeIns.DataBind()


        ddl_Cweakness_ins.DataSource = Common.Get_weakness(ddl_Asset.SelectedValue)
        ddl_Cweakness_ins.DataValueField = "weakness_id"
        ddl_Cweakness_ins.DataTextField = "weakness"
        ddl_Cweakness_ins.DataBind()

        'ddl_Iweakness_ins.DataSource = Common.Get_weakness(ddl_Asset.SelectedValue)
        'ddl_Iweakness_ins.DataValueField = "weakness_id"
        'ddl_Iweakness_ins.DataTextField = "weakness"
        'ddl_Iweakness_ins.DataBind()

        'ddl_Aweakness_ins.DataSource = Common.Get_weakness(ddl_Asset.SelectedValue)
        'ddl_Aweakness_ins.DataValueField = "weakness_id"
        'ddl_Aweakness_ins.DataTextField = "weakness"
        'ddl_Aweakness_ins.DataBind()

        ddl_Cthreat_ins.DataSource = Common.Get_threat(ddl_Asset.SelectedValue)
        ddl_Cthreat_ins.DataValueField = "threat_id"
        ddl_Cthreat_ins.DataTextField = "threat"
        ddl_Cthreat_ins.DataBind()

        'ddl_Ithreat_ins.DataSource = Common.Get_threat(ddl_Asset.SelectedValue)
        'ddl_Ithreat_ins.DataValueField = "threat_id"
        'ddl_Ithreat_ins.DataTextField = "threat"
        'ddl_Ithreat_ins.DataBind()

        'ddl_Athreat_ins.DataSource = Common.Get_threat(ddl_Asset.SelectedValue)
        'ddl_Athreat_ins.DataValueField = "threat_id"
        'ddl_Athreat_ins.DataTextField = "threat"
        'ddl_Athreat_ins.DataBind()

        ddl_Criskp_ins.DataSource = Common.Get_riskP()
        ddl_Criskp_ins.DataValueField = "riskP_score"
        ddl_Criskp_ins.DataTextField = "riskP_desc"
        ddl_Criskp_ins.DataBind()

        'ddl_Iriskp_ins.DataSource = Common.Get_riskP()
        'ddl_Iriskp_ins.DataValueField = "riskP_score"
        'ddl_Iriskp_ins.DataTextField = "riskP_desc"
        'ddl_Iriskp_ins.DataBind()

        'ddl_Ariskp_ins.DataSource = Common.Get_riskP()
        'ddl_Ariskp_ins.DataValueField = "riskP_score"
        'ddl_Ariskp_ins.DataTextField = "riskP_desc"
        'ddl_Ariskp_ins.DataBind()

        ddl_Cimpact_ins.DataSource = Common.Get_riskImpact()
        ddl_Cimpact_ins.DataValueField = "impact_score"
        ddl_Cimpact_ins.DataTextField = "impact_desc"
        ddl_Cimpact_ins.DataBind()

        'ddl_Iimpact_ins.DataSource = Common.Get_riskImpact()
        'ddl_Iimpact_ins.DataValueField = "impact_score"
        'ddl_Iimpact_ins.DataTextField = "impact_desc"
        'ddl_Iimpact_ins.DataBind()

        'ddl_Aimpact_ins.DataSource = Common.Get_riskImpact()
        'ddl_Aimpact_ins.DataValueField = "impact_score"
        'ddl_Aimpact_ins.DataTextField = "impact_desc"
        'ddl_Aimpact_ins.DataBind()

    End Sub
    Protected Sub ShowUpdddl()

        DDLRisk_typeUpd.DataSource = Common.Get_Asset
        DDLRisk_typeUpd.DataValueField = "asset_id"
        DDLRisk_typeUpd.DataTextField = "asset_name"
        DDLRisk_typeUpd.DataBind()


        ddl_weakness_upd.DataSource = Common.Get_weakness(ddl_Asset.SelectedValue)
        ddl_weakness_upd.DataValueField = "weakness_id"
        ddl_weakness_upd.DataTextField = "weakness"
        ddl_weakness_upd.DataBind()

        ddl_threat_upd.DataSource = Common.Get_threat(ddl_Asset.SelectedValue)
        ddl_threat_upd.DataValueField = "threat_id"
        ddl_threat_upd.DataTextField = "threat"
        ddl_threat_upd.DataBind()

        ddl_riskp_upd.DataSource = Common.Get_riskP()
        ddl_riskp_upd.DataValueField = "riskP_score"
        ddl_riskp_upd.DataTextField = "riskP_desc"
        ddl_riskp_upd.DataBind()

        ddl_impact_upd.DataSource = Common.Get_riskImpact()
        ddl_impact_upd.DataValueField = "impact_score"
        ddl_impact_upd.DataTextField = "impact_desc"
        ddl_impact_upd.DataBind()

    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)

        Select Case e.CommandName

            Case "Edit_Btn"



                Dim Ds, Ds1 As New DataSet
                Ds = Common.Get_riskEvaluate_AssetItem(Hidden_DP.Value, ddl_Asset.SelectedValue, SelectedRow.Cells(1).Text)
                assitem_value_updlb.Text = Ds.Tables(0).Rows(0).Item("Asset_Value")
                assitem_dept_updlb.Text = Common.Get_dept_Name(Ds.Tables(0).Rows(0).Item("dept_id"))
                Ds1 = Common.Get_AssetID(SelectedRow.Cells(0).Text)
                Edit_TB.Visible = True
                Show_TB.Visible = False
                ShowUpdddl()
                assitem_id_updlb.Text = SelectedRow.Cells(1).Text
                assit_name_updlb.Text = SelectedRow.Cells(1).Text

                assitem_name_upd.Text = SelectedRow.Cells(1).Text
                'Response.Write(SelectedRow.Cells(0).Text)

                DDLRisk_typeUpd.SelectedValue = Ds1.Tables(0).Rows(0).Item("asset_id")
                ddl_weakness_upd.SelectedValue = SelectedRow.Cells(10).Text
                ddl_threat_upd.SelectedValue = SelectedRow.Cells(11).Text
                ddl_riskp_upd.SelectedValue = SelectedRow.Cells(6).Text
                ddl_impact_upd.SelectedValue = SelectedRow.Cells(7).Text

                Hidden_assitem_id.Value = SelectedRow.Cells(12).Text

                'ShowUpdddl()

            Case "Del_Btn"
                SqlTxt = "DELETE FROM riskEvaluate WHERE id ='" + SelectedRow.Cells(12).Text + "'"

                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DEL", SelectedRow.Cells(0).Text + "-" + SelectedRow.Cells(1).Text)

                GridView1.DataBind()

        End Select

    End Sub

    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click

        SqlTxt = "UPDATE [riskEvaluate] SET [Risk_type]='" + DDLRisk_typeUpd.SelectedValue + "',[weakness_id] =" + ddl_weakness_upd.SelectedValue + ", [threat_id] =" + ddl_threat_upd.SelectedValue + ",riskp_score=" + ddl_riskp_upd.SelectedValue + ",impact_score=" + ddl_impact_upd.SelectedValue + " WHERE ID =" + Hidden_assitem_id.Value + ""
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", assitem_name_upd.Text + "-" + DDLRisk_typeUpd.SelectedItem.Text + "-" + ddl_weakness_upd.SelectedValue + "-" + ddl_threat_upd.SelectedValue + "-" + ddl_riskp_upd.SelectedValue + "-" + ddl_impact_upd.SelectedValue)

        GridView1.DataBind()
        Show_TB.Visible = True
        Edit_TB.Visible = False


    End Sub

    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        'Dim SInfotype As String
        Dim ds As New DataSet

        'ds = Common.Get_CheckriskEvaluatePK(ddl_assitem_id_ins.SelectedValue)
        'If ds.Tables(0).Rows.Count >= 1 Then
        '    Common.showMsg(Me.Page, "資產脆弱威脅分析資產名稱重複，請重新輸入!")

        'Else


        SqlTxt = "INSERT INTO riskEvaluate ([dp],[assitem_id],[risk_type],[weakness_id],[threat_id],[riskp_score],[impact_score]) VALUES ('" + Hidden_DP.Value + "','" + ddl_assitem_id_ins.SelectedValue + "','" + DDLRisk_typeIns.SelectedValue + "'," + ddl_Cweakness_ins.SelectedValue + "," + ddl_Cthreat_ins.SelectedValue + "," + ddl_Criskp_ins.SelectedValue + "," + ddl_Cimpact_ins.SelectedValue + ")"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", ddl_assitem_id_ins.SelectedItem.Text + "-" + DDLRisk_typeIns.SelectedItem.Text + "-" + ddl_Cweakness_ins.SelectedItem.Text + "-" + ddl_Cthreat_ins.SelectedItem.Text + "-" + ddl_Criskp_ins.SelectedItem.Text + "-" + ddl_Cimpact_ins.SelectedItem.Text)
        GridView1.DataBind()

        Show_TB.Visible = True
        Ins_TB.Visible = False
        'End If

    End Sub

    Protected Sub Show_Insert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert.Click
        Show_TB.Visible = False
        Ins_TB.Visible = True
        ShowInsddl()
        Dim Ds As New DataSet
        Ds = Common.Get_riskEvaluate_AssetItem(Hidden_DP.Value, ddl_Asset.SelectedValue, ddl_assitem_id_ins.SelectedValue)
        DDLRisk_typeIns.SelectedValue = ddl_Asset.SelectedValue
        assitem_id_lb.Text = ddl_assitem_id_ins.SelectedValue
        assitem_name_lb.Text = ddl_assitem_id_ins.SelectedItem.Text
        assitem_value_lb.Text = Ds.Tables(0).Rows(0).Item("Asset_Value")
        assitem_dept_lb.Text = Common.Get_dept_Name(Ds.Tables(0).Rows(0).Item("dept_id"))


    End Sub
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow OrElse e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(10).Visible = False
            e.Row.Cells(11).Visible = False
            e.Row.Cells(12).Visible = False

        End If

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        Dim oDataRow As Data.DataRowView
        Dim sShowField As String
        Dim sMessage As String
        Dim Ds As New DataSet
        If e.Row.RowIndex <> -1 Then

            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = oDataRow.Item("risk_type").ToString + "-" + oDataRow.Item("assitem_name").ToString + "-" + oDataRow.Item("threat").ToString + "-" + oDataRow.Item("weakness").ToString
            sMessage = String.Format("您確定要刪除 [{0}] 嗎?", sShowField)
            CType(e.Row.Cells(9).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"

            Ds = Common.Get_AssetName(e.Row.Cells(0).Text)
            e.Row.Cells(0).Text = Ds.Tables(0).Rows(0).Item("asset_name")
        End If
    End Sub

    Protected Sub ddl_assitem_id_ins_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_assitem_id_ins.SelectedIndexChanged
        assitem_id_lb.Text = "資產代碼：" + ddl_assitem_id_ins.SelectedValue
    End Sub


    Protected Sub DDLRisk_typeIns_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLRisk_typeIns.SelectedIndexChanged
        'ddl_Cthreat_ins.DataSource = Common.Get_threat(DDLRisk_typeIns.SelectedValue)
        'ddl_Cthreat_ins.DataValueField = "threat_id"
        'ddl_Cthreat_ins.DataTextField = "threat"
        'ddl_Cthreat_ins.DataBind()

        'ddl_Cweakness_ins.DataSource = Common.Get_weakness(DDLRisk_typeIns.SelectedValue)
        'ddl_Cweakness_ins.DataValueField = "weakness"
        'ddl_Cweakness_ins.DataValueField = "weakness_id"
        'ddl_Cweakness_ins.DataBind()
    End Sub

    Protected Sub DDLRisk_typeUpd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLRisk_typeUpd.SelectedIndexChanged

        'ddl_threat_upd.DataSource = Common.Get_threat(DDLRisk_typeUpd.SelectedValue)
        'ddl_threat_upd.DataValueField = "threat_id"
        'ddl_threat_upd.DataTextField = "threat"
        'ddl_threat_upd.DataBind()

        'ddl_weakness_upd.DataSource = Common.Get_weakness(DDLRisk_typeUpd.SelectedValue)
        'ddl_weakness_upd.DataValueField = "weakness"
        'ddl_weakness_upd.DataValueField = "weakness_id"
        'ddl_weakness_upd.DataBind()

    End Sub
End Class
