﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DiaryLog_list.aspx.vb" Inherits="DiaryLog_list" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>附件線上簽核</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
    <link href="CSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="JS/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="JS/bootstrap.min.js"></script>
    <script type="text/javascript" src="JS/jquery-ui.min.js"></script>
    <script type="text/javascript">
        /**
        * Created by EIJI on 2014/1/3.
        */

        (function () {
            var yearTextSelector = '.ui-datepicker-year';

            var dateNative = new Date(),
        dateTW = new Date(
            dateNative.getFullYear() - 1911,
            dateNative.getMonth(),
            dateNative.getDate()
        );


            function leftPad(val, length) {
                var str = '' + val;
                while (str.length < length) {
                    str = '0' + str;
                }
                return str;
            }

            // 應該有更好的做法
            var funcColle = {
                onSelect: {
                    basic: function (dateText, inst) {
                        /*
                        var yearNative = inst.selectedYear < 1911
                        ? inst.selectedYear + 1911 : inst.selectedYear;*/
                        dateNative = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);

                        // 年分小於100會被補成19**, 要做例外處理
                        var yearTW = inst.selectedYear > 1911
                    ? leftPad(inst.selectedYear - 1911, 4)
                    : inst.selectedYear;
                        var monthTW = leftPad(inst.selectedMonth + 1, 2);
                        var dayTW = leftPad(inst.selectedDay, 2);
                        console.log(monthTW);
                        dateTW = new Date(
                    yearTW + '-' +
                    monthTW + '-' +
                    dayTW + 'T00:00:00.000Z'
                );
                        console.log(dateTW);
                        return $.datepicker.formatDate(twSettings.dateFormat, dateTW);
                    }
                }
            };

            var twSettings = {
                closeText: '關閉',
                prevText: '上個月',
                nextText: '下個月',
                currentText: '今天',
                monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
            '七月', '八月', '九月', '十月', '十一月', '十二月'],
                monthNamesShort: ['一月', '二月', '三月', '四月', '五月', '六月',
            '七月', '八月', '九月', '十月', '十一月', '十二月'],
                dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
                dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
                dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
                weekHeader: '周',
                dateFormat: 'yy/mm/dd',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: true,
                yearSuffix: '年',

                onSelect: function (dateText, inst) {
                    $(this).val(funcColle.onSelect.basic(dateText, inst));
                    if (typeof funcColle.onSelect.newFunc === 'function') {
                        funcColle.onSelect.newFunc(dateText, inst);
                    }
                }
            };

            // 把yearText換成民國
            var replaceYearText = function () {
                var $yearText = $('.ui-datepicker-year');

                if (twSettings.changeYear !== true) {
                    $yearText.text('民國' + dateTW.getFullYear());
                } else {
                    // 下拉選單
                    if ($yearText.prev('span.datepickerTW-yearPrefix').length === 0) {
                        $yearText.before("<span class='datepickerTW-yearPrefix'>民國</span>");
                    }
                    $yearText.children().each(function () {
                        if (parseInt($(this).text()) > 1911) {
                            $(this).text(parseInt($(this).text()) - 1911);
                        }
                    });
                }
            };

            $.fn.datepickerTW = function (options) {

                // setting on init,
                if (typeof options === 'object') {
                    //onSelect例外處理, 避免覆蓋
                    if (typeof options.onSelect === 'function') {
                        funcColle.onSelect.newFunc = options.onSelect;
                        options.onSelect = twSettings.onSelect;
                    }
                    // year range正規化成西元, 小於1911的數字都會被當成民國年
                    if (options.yearRange) {
                        var temp = options.yearRange.split(':');
                        for (var i = 0; i < temp.length; i += 1) {
                            //民國前處理
                            if (parseInt(temp[i]) < 1) {
                                temp[i] = parseInt(temp[i]) + 1911;
                            } else {
                                temp[i] = parseInt(temp[i]) < 1911
                            ? parseInt(temp[i]) + 1911
                            : temp[i];
                            }
                        }
                        options.yearRange = temp[0] + ':' + temp[1];
                    }
                    // if input val not empty
                    if ($(this).val() !== '') {
                        options.defaultDate = $(this).val();
                    }
                }

                // setting after init
                if (arguments.length > 1) {
                    // 目前還沒想到正常的解法, 先用轉換成init setting obj的形式
                    if (arguments[0] === 'option') {
                        options = {};
                        options[arguments[1]] = arguments[2];
                    }
                }

                // override settings
                $.extend(twSettings, options);

                // init
                $(this).datepicker(twSettings);

                // beforeRender
                $(this).click(function () {
                    var isFirstTime = ($(this).val() === '');

                    // year range and default date

                    if ((twSettings.defaultDate || twSettings.yearRange) && isFirstTime) {

                        if (twSettings.defaultDate) {
                            $(this).datepicker('setDate', twSettings.defaultDate);
                        }

                        // 當有year range時, select初始化設成range的最末年
                        if (twSettings.yearRange) {
                            var $yearSelect = $('.ui-datepicker-year'),
                        nowYear = twSettings.defaultDate
                            ? $(this).datepicker('getDate').getFullYear()
                            : dateNative.getFullYear();

                            $yearSelect.children(':selected').removeAttr('selected');
                            if ($yearSelect.children('[value=' + nowYear + ']').length > 0) {
                                $yearSelect.children('[value=' + nowYear + ']').attr('selected', 'selected');
                            } else {
                                $yearSelect.children().last().attr('selected', 'selected');
                            }
                        }
                    } else {
                        $(this).datepicker('setDate', dateNative);
                    }

                    $(this).val($.datepicker.formatDate(twSettings.dateFormat, dateTW));

                    replaceYearText();

                    if (isFirstTime) {
                        $(this).val('');
                    }
                });

                // afterRender
                $(this).focus(function () {
                    replaceYearText();
                });

                return this;
            };

        })();

        $(function () {
            $('.datepickerTW').datepickerTW({
                changeYear: true,
                changeMonth: true,
                yearRange: ((new Date).getFullYear() - 1) + ":" + ((new Date).getFullYear() + 1),
                //defaultDate: '86-11-01',
                dateFormat: 'yy/mm/dd'
            });
            $('.datepicker').datepicker({
                changeYear: true,
                changeMonth: true,
                dateFormat: 'yy-mm-dd',
                yearRange: '1911:2018',
                defaultDate: '2016-11-01'
            });

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="True">
    </asp:ScriptManager>
    <table border="1" cellpadding="0" runat="server" id="Table1" cellspacing="0" class="tb_title_w_2" width="850px">
        <tr>
            <td>
                <img src="Images/exe.gif" />資訊管理工作日誌 | 個人申請清單 列表
                <asp:Label ID="lb_msg" runat="server" ForeColor="red" EnableViewState="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table id="tb_result" runat="server" border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2"
                    style="width: 100%" visible="false">
                    <tr>
                        <td colspan="4" style="text-align: right">
                            <asp:Button ID="but_update" runat="server" Text="修改儲存" CausesValidation="False" />
                            <asp:Button ID="but_print" OnClientClick="window.print() " runat="server" Text="列印"/>
                            <asp:Button ID="but_esc" runat="server" Text="回列表" CausesValidation="False" />
                            <asp:HiddenField ID="hidden_recordId" runat="server" Value='<%#Eval("hidden_recordId") %>' />
                        </td>
                    </tr>
                    <tr>
            <td class="tb_title_w_1">
                日 期*
            </td>
            <td class="tb_title_w_2" align="center">
                <asp:Label ID="applyDate" runat="server"></asp:Label>
            </td>
            <td class="tb_title_w_1">
                表單編號
            </td>
            <td class="tb_title_w_2">
                <asp:Label ID="recordId" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_1" width="150px" colspan="4">
                一般記事
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="時間">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Hr" Text='<%# eval("InTimeHr") %>'></asp:Label>:
                                            <asp:Label runat="server" ID="Min" Text='<%# eval("InTimeMin") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="辦理事項">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="ap_comment" CommandName="Btn_SYSIO" Text='<%# eval("applyComment") %>'
                                                CommandArgument='<%#Eval("recordId") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="單位名稱">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="ap_org" Text='<%# eval("applyOrg") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="人員名稱">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="ap_name" Text='<%# eval("applyName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="tb_title_w_1" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="false" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="時間">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="time_h" Text='<%# eval("time_h") %>'></asp:Label>:
                                <asp:Label runat="server" ID="time_m" Text='<%# eval("time_m") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="辦理事項">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="ap_comment" Text='<%# eval("ap_comment") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="單位名稱">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="ap_org" Text='<%# eval("ap_org") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="人員名稱">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="ap_name" Text='<%# eval("ap_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="tb_title_w_1" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_1" width="150px" colspan="1">
                <asp:Button runat="server" Text="各項作業檢查*" ID="Btn_exam" />
            </td>
            <td class="tb_title_w_2" width="150px" colspan="4">
                <asp:RadioButtonList ID="Radio_checkStatus" runat="server" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Text="均正常" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="有異常" Value="2"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_1" width="150px" colspan="4">
                異常記事(狀況描述與處理情形)
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_2" colspan="4">
                <asp:TextBox ID="comment" TextMode="MultiLine" runat="server" Width="100%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_1" width="150px" colspan="4">
                交代事項
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_2" colspan="4">
                <asp:TextBox ID="workitems" TextMode="MultiLine" runat="server" Width="100%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_1" width="150px" colspan="1">
                附件檔案
            </td>
            <td class="tb_title_w_2" colspan="3">
                <asp:Label ID="applyFile_link" runat="server" Visible="false" Text="0"></asp:Label>
                <asp:LinkButton ID="applyFile" runat="server" Text="檔案下載"></asp:LinkButton>
            </td>
        </tr>
                    <tr>
                        <td colspan="4">
                            <table border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2" style="width: 100%">
                                <tr>
                                    <td colspan="4" align="center">
                                        <%--<uc1:FlowControl ID="FlowControl1" runat="server"  />--%>
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="lb_recordid" runat="server" Visible="false" Text="0"></asp:Label>
                                                <asp:Label ID="lb_id" runat="server" Visible="false" Text="99"></asp:Label>
                                                <asp:Label ID="lb_name" runat="server" Visible="false" Text="99"></asp:Label>
                                                <asp:GridView ID="GridView" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                                    CssClass="tb_1" Width="100%">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="關卡" ItemStyle-CssClass="tb_w_1">
                                                            <ItemTemplate>
                                                                <asp:Label ID="wf_order" runat="server" Text='<%# eval("wf_order") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="審核身份" ItemStyle-CssClass="tb_w_1">
                                                            <ItemTemplate>
                                                                <asp:Label ID="chk_name" runat="server" Text='<%# eval("chk_name") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="審核人員" ItemStyle-CssClass="tb_w_1">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddl_user" runat="server" Visible="false">
                                                                </asp:DropDownList>
                                                                <asp:Panel ID="pan_user" runat="server" Visible="false">
                                                                    <asp:Label ID="lb_username" runat="server"></asp:Label>
                                                                    (<asp:Label ID="label_userid" runat="server"></asp:Label>)
                                                                </asp:Panel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="狀態" ItemStyle-CssClass="tb_w_1">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lb_status" runat="server"></asp:Label>
                                                                <asp:HiddenField ID="hid_status" runat="server" Value='<%#Eval("wf_status_id") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="意見" ItemStyle-CssClass="tb_w_1">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lb_comment" runat="server" Text='<%# Eval("wf_comment")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="tb_title_w_1" />
                                                </asp:GridView>
                                                <%--  <asp:SqlDataSource ID="WF_DS" runat="server"></asp:SqlDataSource>--%>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table id="tb_query" runat="server">
                    <tr>
                        <td class="tb_title_w_1">
                            申請日期起迄
                        </td>
                        <td>
                            <asp:TextBox ID="txb_sdate" class="datepickerTW" Style="text-align: left" Height="19px" runat="server"></asp:TextBox> ~
                            <asp:TextBox ID="txb_edate" class="datepickerTW" Style="text-align: left" Height="19px" runat="server"></asp:TextBox> 
                        </td>
                        <td>
                            <asp:Button ID="but_search" runat="server" Text="查詢" />
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
                <asp:GridView ID="GridView1" runat="server" CssClass="tb_1" AutoGenerateColumns="false"
                    Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="申請日期">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lb_applydate" Text='<%# eval("applyDate") %>'></asp:Label>
                                <asp:HiddenField ID="hidden_recordId" runat="server" Value='<%#Eval("recordId")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="表單編號">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="recordId" Text='<%# eval("recordId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="使用者姓名">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lb_userName" Text='<%# eval("empName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="管理">
                            <ItemTemplate>
                                <asp:Button ID="Button1" CommandName="audit_btn" runat="server" Text="查看" CommandArgument='<%#Eval("recordId") %>' />
                                <asp:Button ID="Button2" CommandName="using_btn" runat="server" Text="撤銷" CommandArgument='<%#Eval("recordId")%>'
                                    OnClientClick="if ( confirm ('確定撤銷嗎？')==false) return false;" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <EmptyDataTemplate>
                        …尚無資料…</EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <table class="tb_title_w_2" runat="server" id="Table2" border="1" cellpadding="0"
        cellspacing="0" width="700px" visible="false">
        <tr>
            <td>
                <img src="Images/exe.gif" />資訊管理工作日誌 - 各項作業檢查表
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="false" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="序">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="Num" Text='<%# eval("Num") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="項目">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="comment" Text='<%# eval("comment") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="結果">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="result" Text='<%# eval("result") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="tb_title_w_1" />
                    <EmptyDataTemplate>
                        …尚無資料…</EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button runat="server" ID="Btn_examBack" Text="確定並返回表單" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
