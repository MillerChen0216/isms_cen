﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_40150.aspx.vb" Inherits="ISMS_40150" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js" type="text/javascript"></script>
      <script type="text/javascript">
          $(function () {
              $('[id*=lb_Form]').multiselect({
                  includeSelectAllOption: true
              });
          });
    </script>
</head>
<body style="background-color: #ffffff">
    <form id="form1" runat="server">
    <div>
        <br />
                <asp:HiddenField ID="Hidden_DP" runat="server" />

        <table id="TB_Set_FormGroup" runat="server" cellpadding="0" cellspacing="1" class="tb_1" style="width:800px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="2">
                    <img alt="" src="Images/exe.gif" />
                    <asp:Label ID="lab_InsForm" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    文件/表單名稱</td>
                <td class="tb_title_w_1">
                    文件/表單</td>
                
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="txt_Ins_Form_Name" runat="server" Width="180px"></asp:TextBox>
                    <asp:Label ID="lab_FormName" runat="server" Width="120px"></asp:Label></td>
                <td class="tb_w_1">
                <asp:ListBox ID="lb_Form" runat="server" SelectionMode="Multiple"></asp:ListBox></td>
            </tr>
            <tr>
                <td class="tb_w_1" colspan="4">
                    <asp:Button ID="btn_Ins_FormGroup" runat="server" CssClass="button-small" Text="確定新增" />
                    <asp:Button ID="btn_GoBack" runat="server" CssClass="button-small" Text="取消" />
                </td>
            </tr>
        </table>
            
        <asp:SqlDataSource ID="sds_Form_Group" runat="server"></asp:SqlDataSource>
         <asp:HiddenField ID="hid_approvalid_glo" runat="server"  Visible="false"/>
        <table id="TB_Show_FormGroup" runat="server" cellpadding="3" cellspacing="0" class="tb_1"
            style="width: 800px" >
            <tr>
                <td class="tb_title_w_2" colspan="3">
                    <img src="Images/exe.GIF" />
                     文件/表單管理 
                    <asp:Button ID="Insert_Btn" runat="server" CssClass="button-small" Text="新增資料" />
                </td>

            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server"
                        AutoGenerateColumns="False" CellPadding="3" CssClass="tb_1" Width="800px">
                        <Columns>
                            <asp:BoundField DataField="Form_Name"  HeaderText="文件/表單名稱" >
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>   
                            
                            <asp:TemplateField HeaderText="文件/表單功能">
                                <ItemTemplate>
                                    <asp:Label ID="lab_Form_Fun" runat="server"></asp:Label>                 
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="管理">
                                <ItemTemplate>
                                    <asp:Button ID="btn_Set_FormFun" runat="server"  Text="編輯功能" OnClick="btn_Set_FormFun_Click"/>
                                    <asp:HiddenField ID="hid_approvalid" runat="server" Value='<%# Eval("approvalId")%>'  Visible="false"/>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            
                        </Columns>
                    </asp:GridView> </td>
            </tr>
        </table>
         <asp:SqlDataSource ID="sds_Form_Fun" runat="server"></asp:SqlDataSource>
        <table id="TB_Show_FormFun" runat="server" cellpadding="3" cellspacing="0" class="tb_1"
            style="width: 800px" visible="false" >
            <tr>
                <td class="tb_title_w_2" colspan="3">
                    <img src="Images/exe.GIF" />
                     編輯文件/表單功能 
                    <asp:Button ID="btn_Ins_FormFun" runat="server" CssClass="button-small" Text="新增功能" />
                </td>

            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView2" runat="server"
                        AutoGenerateColumns="False" CellPadding="3" CssClass="tb_1" Width="800px">
                        <Columns>
                            <asp:BoundField DataField="Value" HeaderText="文件/表單功能">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>  
                            <asp:TemplateField HeaderText="管理">
                                <ItemTemplate>
                                    <asp:Button ID ="btn_Del_FormFun" runat="server" Text="移除" OnClick="btn_Del_FormFun_Click" OnClientClick="return confirm('確定要移除此功能嗎？')" />
                                    <asp:HiddenField ID="hid_sURL" runat="server" Value='<%# Eval("sURL")%>'  Visible="false"/>
                                </ItemTemplate>
                                 <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>                
                        </Columns>
                    </asp:GridView> </td>
            </tr>
             <tr>
                <td class="tb_w_1" colspan="2">
                    <asp:Button ID="btn_GoBack2" runat="server" CssClass="button-small" Text="取消" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>