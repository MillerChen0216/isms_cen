﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SYSCode.aspx.vb" Inherits="SYSCode" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript">

</script>
<body>
    <form id="form1" runat="server">
    <div>
    <br />
    <table id="Table" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 500px" visible="true">
            <tr>
                <th colspan="2" class="tb_title_w_2" align="center">
                    <img src="Images/Icon1.gif" />
                    資訊系統代號維護
                    <asp:Label ID="lb_msg" runat="server" ForeColor="red" EnableViewState="false"></asp:Label>
                </th>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    操作
                </td>
                <td class="tb_w_1" align="center">
                    <asp:RadioButtonList ID="EditType" runat="server" RepeatDirection="Horizontal"
                        OnSelectedIndexChanged="EditType_selectedchange" AutoPostBack="True">
                        <asp:ListItem Text="新增" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="修改" Value="1"></asp:ListItem>
                        <asp:ListItem Text="刪除" Value="2"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr runat="server" id="otr">
                <td class="tb_title_w_1" width="150px">
                    系統名稱
                </td>
                <td class="tb_w_1" align="left">
                    <asp:DropDownList ID="Ddl_Name" runat="server" Width="200px" AutoPostBack="true" OnTextChanged="Ddl_Name_TextChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr runat="server" id="ntr1">
                <td class="tb_title_w_1" width="150px">
                    新系統代號*
                </td>
                <td class="tb_w_1" align="left">
                    <asp:TextBox ID="TextBox_Code" runat="server" Width="200px" onkeyup="value=value.replace(/[^a-zA-Z]/g,'')" MaxLength="3"></asp:TextBox>
                </td>
            </tr>
            <tr runat="server" id="ntr2">
                <td class="tb_title_w_1" width="150px">
                    新系統名稱*
                </td>
                <td class="tb_w_1" align="left">
                    <asp:TextBox ID="TextBox_Name" runat="server" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr runat="server" id="ntr3">
                <td class="tb_title_w_1" width="150px">
                    新備註
                </td>
                <td class="tb_w_1" align="left">
                    <asp:TextBox ID="TextBox_Remarks" runat="server" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="Btn_Save" runat="server" CssClass="button-small" Text="儲存" />
                    <asp:Button ID="Btn_Action" runat="server" CssClass="button-small" Text="確定" />
                    <asp:Button ID="Btn_Cancel" runat="server" CssClass="button-small" Text="取消" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>