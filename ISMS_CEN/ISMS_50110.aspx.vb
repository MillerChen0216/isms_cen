﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_50110
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand


    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)

        Select Case e.CommandName

            Case "Disable"

                Dim User_id As String = SelectedRow.Cells(0).Text
                Dim using_txt As String = SelectedRow.Cells(5).Text


                If using_txt = "Y" Then

                    SqlTxt = "UPDATE ismsUser  SET [using] = 'N' WHERE user_id = '" + User_id + "'"
                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                    Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "CLOSE", user_id_ins.Text + "使用者")

                Else
                    SqlTxt = "UPDATE ismsUser  SET [using] = 'Y' WHERE user_id = '" + User_id + "'"
                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                    Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "OPEN", user_id_ins.Text + "使用者")

                End If

                GridView1.DataBind()

            Case "Reset_Pwd"

                pwdtxt1.Text = ""
                pwdtxt2.Text = ""
                pwd_tb.Visible = True
                Edit_TB.Visible = False
                Show_TB.Visible = False
                useridtxt.Text = SelectedRow.Cells(0).Text


            Case "Edit_Btn"

                Dim selectCMD As SqlCommand = New SqlCommand("SELECT * FROM ismsUser where user_id ='" + SelectedRow.Cells(0).Text + "'", Conn)
                Dim mycmd As SqlDataAdapter = New SqlDataAdapter()
                mycmd.SelectCommand = selectCMD
                Dim myDs As DataSet = New DataSet()

                mycmd.Fill(myDs, "ismsUser")

                Edit_TB.Visible = True
                Show_TB.Visible = False

                user_name.Text = ""
                email.Text = ""
                user_id.Text = SelectedRow.Cells(0).Text
                user_name.Text = SelectedRow.Cells(1).Text
                email.Text = Strings.Replace(SelectedRow.Cells(6).Text, "&nbsp;", "")
                phone_edit.Text = Strings.Replace(SelectedRow.Cells(7).Text, "&nbsp;", "")
                ext_edit.Text = Strings.Replace(SelectedRow.Cells(8).Text, "&nbsp;", "")

                Dim degree_name, dept_name, Group_name As String
                degree_name = SelectedRow.Cells(2).Text
                ddl_degree.DataSource = Common.Get_degree(Get_DP())
                ddl_degree.DataValueField = "degree_id"
                ddl_degree.DataTextField = "degree_name"
                ddl_degree.DataBind()

                dept_name = SelectedRow.Cells(3).Text
                ddl_dept.DataSource = Common.Get_dept(Get_DP())
                ddl_dept.DataValueField = "dept_id"
                ddl_dept.DataTextField = "dept_name"
                ddl_dept.DataBind()

                Group_name = SelectedRow.Cells(4).Text
                ddl_group.DataSource = Common.Get_Group(Get_DP())
                ddl_group.DataValueField = "group_id"
                ddl_group.DataTextField = "group_name"
                ddl_group.DataBind()

                ddl_degree.SelectedValue = myDs.Tables(0).Rows(0).Item("degree_id").ToString
                ddl_dept.SelectedValue = myDs.Tables(0).Rows(0).Item("dept_id").ToString
                ddl_group.SelectedValue = myDs.Tables(0).Rows(0).Item("group_id").ToString



        End Select

    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(12).Visible = False
        'e.Row.Cells(9).Visible = False
    End Sub



    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim oDataRow As Data.DataRowView
        Dim sShowField As String
        Dim sMessage As String
        If e.Row.RowIndex <> -1 Then
            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = oDataRow.Item("user_id").ToString()
            If e.Row.Cells(5).Text = "Y" Then
                CType(e.Row.Cells(9).Controls(0), Button).Text = "停用"
                sMessage = String.Format("您確定要停用 [{0}] 嗎?", sShowField)
            Else
                CType(e.Row.Cells(9).Controls(0), Button).Text = "啟用"
                sMessage = String.Format("您確定要啟用 [{0}] 嗎?", sShowField)
            End If

            e.Row.Cells(2).Text = Common.Get_degree_Name(Strings.Replace(e.Row.Cells(2).Text, "&nbsp;", "")) '職稱名稱
            e.Row.Cells(3).Text = Common.Get_dept_Name(Strings.Replace(e.Row.Cells(3).Text, "&nbsp;", ""))  '單位名稱
            e.Row.Cells(4).Text = Common.Get_group_Name(Strings.Replace(e.Row.Cells(4).Text, "&nbsp;", "")) '群組名稱

            CType(e.Row.Cells(9).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"

            'sMessage1 = String.Format("您確定 [{0}] 要重設密碼嗎?", sShowField)
            'CType(e.Row.Cells(9).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage1 & "')==false) {return false;}"
            'Common.SendMail(e.Row.Cells(0).Text, "ResetPwd")
        End If
    End Sub


    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click



        SqlTxt = "UPDATE ismsUser  SET  user_name ='" + user_name.Text + "' , degree_id ='" + ddl_degree.SelectedValue + "', dept_id='" + ddl_dept.SelectedValue + "', group_id='" + ddl_group.SelectedValue + "', email='" + email.Text + "', phone='" + phone_edit.Text + "' ,ext ='" + ext_edit.Text + "' where user_id = '" + user_id.Text + "' "
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", user_id_ins.Text + "使用者")

        GridView1.DataBind()
        Show_TB.Visible = True
        Edit_TB.Visible = False

    End Sub

    Protected Sub Show_Insert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert.Click
        Show_TB.Visible = False
        Ins_TB.Visible = True

        'send mail sub
    End Sub

    Protected Sub Show_Insertddl()


        ddl_degree_ins.DataSource = Common.Get_degree(Hidden_DP.Value)
        ddl_degree_ins.DataValueField = "degree_id"
        ddl_degree_ins.DataTextField = "degree_name"
        ddl_degree_ins.DataBind()

        ddl_dept_ins.DataSource = Common.Get_dept(Hidden_DP.Value)
        ddl_dept_ins.DataValueField = "dept_id"
        ddl_dept_ins.DataTextField = "dept_name"
        ddl_dept_ins.DataBind()

        ddl_group_ins.DataSource = Common.Get_Group(Hidden_DP.Value)
        ddl_group_ins.DataValueField = "group_id"
        ddl_group_ins.DataTextField = "group_name"
        ddl_group_ins.DataBind()


    End Sub

    Protected Sub SqlDataSource()
        Dim sql As String
        sql = " SELECT a.pwd,a.user_id, a.user_name, a.using, a.email,a.phone,a.ext ,a.dept_id as dept_name, "
        sql += " a.degree_id as degree_name, a.group_id as group_name FROM ismsUser a ,ctlGroup b where "
        sql += " a.group_id = b.group_id and b.dp='" + Get_DP() + "'order by user_id "
        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = sql
        GridView1.DataSourceID = SqlDataSource1.ID
        Dim KeyNames() As String = {"user_id"}
        GridView1.DataKeyNames = KeyNames
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        If Not Page.IsPostBack Then
            Show_Insertddl()
        End If
        SqlDataSource()
    End Sub

    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        SqlTxt = "INSERT INTO ismsUser  ([user_id],[user_name],[dept_id],[degree_id],[using],[email],[phone],[ext],[group_id],[update_date],pwd,old_pwd) VALUES ('" + user_id_ins.Text + "' , '" + user_name_ins.Text + "' , '" + ddl_dept_ins.SelectedValue + "','" + ddl_degree_ins.SelectedValue + "','Y','" + email_ins.Text + "','" + phone_ins.Text + "','" + ext_ins.Text + "','" + ddl_group_ins.SelectedValue + "','" + DateTime.Now.ToString("yyyyMMdd") + "','" + Common.Encode(ins_pwdtxt.Text) + "','" + Common.Encode(ins_pwdtxt.Text) + "') "
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", user_id_ins.Text + "使用者")

        GridView1.DataBind()
        Show_TB.Visible = True
        Ins_TB.Visible = False
        user_id_ins.Text = "" : ins_pwdtxt.Text = "" : user_name_ins.Text = ""
        ddl_degree_ins.SelectedIndex = 0 : ddl_dept_ins.SelectedIndex = 0 : ddl_group_ins.SelectedIndex = 0
        email_ins.Text = "" : phone_ins.Text = "" : ext_ins.Text = ""

    End Sub

    Protected Sub user_id_ins_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles user_id_ins.TextChanged
        Dim ds As New DataSet
        ds = Common.Get_CheckUser(user_id_ins.Text)
        If ds.Tables(0).Rows.Count >= 1 Then
            Common.showMsg(Me.Page, "此帳號系統已經存在，請重新建立新帳號!")
            user_id_ins.Text = ""
        End If

    End Sub






    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        If pwdtxt1.Text = "" Or pwdtxt2.Text = "" Then
            Common.showMsg(Me.Page, "密碼欄位不得為空白，謝謝！")
            pwdtxt1.Focus()
        ElseIf pwdtxt1.Text <> pwdtxt2.Text Then
            Common.showMsg(Me.Page, "兩次密碼輸入不一樣，請檢查，謝謝！")
            pwdtxt1.Focus()
        Else
            SqlTxt = "UPDATE ismsUser  SET  pwd ='" + Common.Encode(pwdtxt2.Text) + "',update_date ='" + DateTime.Now.ToString("yyyyMMdd") + "' where user_id = '" + useridtxt.Text + "' "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", useridtxt.Text + "使用者密碼")
            Common.showMsg(Me.Page, "密碼重設成功！")

        End If
    End Sub
End Class
