﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO

Partial Class SYSIO_rec_list
    Inherits PageBase
    'Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim uid As String = User.Identity.Name '使用者名稱
    Dim uname As String = Common.Get_User_Name(uid) '使用者代號
    Dim apprvid As String
    Dim approvalid As String
    Dim dp As String = Me.Get_DP
    Dim File_Path As String = ConfigurationManager.AppSettings("SYSIO_Path")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        approvalid = ConfigurationManager.AppSettings("flow_SYSIO_rec") '11
        apprvid = ConfigurationManager.AppSettings("flow_SYSIO_rec") '11
        Try
            If Not IsPostBack Then
                txb_sdate.Text = Common.Year(Now.AddDays(-30))
                txb_edate.Text = Common.Year(Now.AddDays(1))
                SqlDataSource()
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '申請清單
    Protected Sub SqlDataSource()
        Dim sql As String = " SELECT K.recordId,K.SYSIO_recordId,J.applyDate,K.empName from SYSIO J, ( select *  from SYSIO_rec where empUid='" + uid + "'  and using='Y'  and empUid in (select user_id from ismsUser where using='Y') ) K where J.recordId = K.SYSIO_recordId " '排除已撤銷之申請單'排除停用帳號
        If txb_sdate.Text <> "" Then
            sql += " and applyDate >= '" + Common.YearYYYY(txb_sdate.Text) + "'"
        End If
        If txb_edate.Text <> "" Then
            sql += " and applyDate <= '" + Common.YearYYYY(txb_edate.Text) + "'"
        End If
        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = sql
        GridView1.DataSourceID = SqlDataSource1.ID
    End Sub

    '申請清單_RowDataBound
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '民國年轉換
                Dim Button2 As Button = DirectCast(e.Row.FindControl("Button2"), Button)
                Dim lb_applydate As String = CType(e.Row.FindControl("lb_applydate"), Label).Text
                Dim hidden_recordId As String = CType(e.Row.FindControl("hidden_recordId"), HiddenField).Value
                CType(e.Row.FindControl("lb_applydate"), Label).Text = Common.Year(Date.Parse(lb_applydate))
                Dim ds As DataSet = Common.Qry_Process(hidden_recordId)
                If ds.Tables(0).Rows.Count <= 0 Then
                    Button2.Visible = False '只有在第一關才可以撤銷
                End If
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '查詢按鈕
    Protected Sub but_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_search.Click
        SqlDataSource()
    End Sub

    '查看_1及撤銷按鈕
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim GETrecordId As String = e.CommandArgument
        hidden_recordId.Value = GETrecordId
        Select Case e.CommandName

            Case "audit_btn"
                '查看
                Try

                    lb_recordid.Text = hidden_recordId.Value '紀錄該表單編號
                    Dim sql = "  SELECT * FROM SYSIO_rec WHERE recordId = '" + lb_recordid.Text + "'"
                    Dim data As DataTable = Common.Con(sql)
                    If data.Rows.Count > 0 Then

                        '載入關卡表
                        GdvShow()

                        '載入資料
                        sql = "select * from SYSIO where recordId = '" + data.Rows(0)("SYSIO_recordId") + "'"
                        Dim data2 = Common.Con(sql)
                        If Not data2.Rows.Count = 0 Then
                            applyDate.Text = Common.Year(data2.Rows(0)("applyDate")) '申請日期
                            SYSIO_recordId.Text = data2.Rows(0)("recordId") '表單編號
                            chk_target_1.Checked = If(data2.Rows(0)("target_1") = "1", True, False) '維護標的_資訊系統
                            chk_target_2.Checked = If(data2.Rows(0)("target_2") = "1", True, False) '維護標的_資訊設備
                            targetName.Text = data2.Rows(0)("targetName") '標的名稱
                            applyOrg.Text = data2.Rows(0)("applyOrg") '申請單位
                            applyName.Text = data2.Rows(0)("applyName") '申請人員
                            chk_method_1.Checked = If(data2.Rows(0)("method_1") = "1", True, False) '維護方式_親至機房
                            chk_method_2.Checked = If(data2.Rows(0)("method_2") = "1", True, False) '維護方式_親至資訊(電腦)室
                            chk_method_3.Checked = If(data2.Rows(0)("method_3") = "1", True, False) '維護方式_Internet連線
                            chk_method_4.Checked = If(data2.Rows(0)("method_4") = "1", True, False) '維護方式_其他
                            methodOther.Text = data2.Rows(0)("methodOther") '維護方式_其他內容
                            chk_location_1.Checked = If(data2.Rows(0)("location_1") = "1", True, False) '存放位置_機房
                            chk_location_2.Checked = If(data2.Rows(0)("location_2") = "1", True, False) '存放位置_辦公(控制)室
                            chk_location_3.Checked = If(data2.Rows(0)("location_3") = "1", True, False) '存放位置_其他
                            locationOther.Text = data2.Rows(0)("locationOther") '存放位置_其他內容
                            InDate.Text = Common.Year(data2.Rows(0)("InDate")) '進出日期
                            InTime.Text = data2.Rows(0)("InTimeHr") + "：" + data2.Rows(0)("InTimeMin") '進出時間_進時'進出時間_進分
                            OutTime.Text = data2.Rows(0)("OutTimeHr") + "：" + data2.Rows(0)("OutTimeMin") '離開時間_出時'離開時間_出分
                            applyComment.Text = data2.Rows(0)("applyComment") '辦理事項
                            chk_IfChangeSoftware.Checked = If(data2.Rows(0)("IfChangeSoftware") = "1", True, False) '變更程式
                            chk_IfSYSPG.Checked = If(data2.Rows(0)("IfSYSPG") = "1", True, False) '軟體程式變更申請單
                            chk_IfFirewall.Checked = If(data2.Rows(0)("IfFirewall") = "1", True, False) '防火牆維護申請表
                            chk_IfTest.Checked = If(data2.Rows(0)("IfTest") = "1", True, False) '測試作業之程式
                            chk_IfItemIn.Checked = If(data2.Rows(0)("IfItemIn") = "1", True, False) '攜入
                            chk_IfItemOut.Checked = If(data2.Rows(0)("IfItemOut") = "1", True, False) '攜出
                            chk_IfItemLocation.Checked = If(data2.Rows(0)("IfItemLocation") = "1", True, False) '變更位置

                            '載入選擇
                            If chk_IfSYSPG.Checked = "1" Then
                                Dim sql_related_syspgrecordId = "select * from SYSIO_related where SYSIO_recordId = '" + lb_recordid.Text + "' and kind = 'PG'"
                                Dim data_related = Common.Con(sql_related_syspgrecordId)
                                If Not data_related.Rows.Count = 0 Then
                                    SYSPG_recordId2.Text = data_related.Rows(0)("related_recordId") 'SYSPG_recordId
                                End If
                            Else
                                SYSPG_recordId2.Text = ""
                            End If
                            If chk_IfFirewall.Checked = "1" Then
                                Dim sql_related_firewallrecordId = "select * from SYSIO_related where SYSIO_recordId = '" + lb_recordid.Text + "' and kind = 'FW'"
                                Dim data_related = Common.Con(sql_related_firewallrecordId)
                                If Not data_related.Rows.Count = 0 Then
                                    Firewall_recordId2.Text = data_related.Rows(0)("related_recordId") 'Firewall_recordId
                                End If
                            Else
                                Firewall_recordId2.Text = ""
                            End If

                            '檢測結果
                            Dim sql_exam = "  select * from SYSIO_exam where SYSIO_recordId = '" + lb_recordid.Text + "' "
                            Dim data_exam = Common.Con(sql_exam)
                            If data_exam.Rows.Count = 0 Then
                                GdvShow1()
                            Else
                                GridView2.DataSource = data_exam
                                GridView2.DataBind()
                                Session("newTable") = data_exam '紀錄
                            End If

                            '查看裡面是否有資料，有才秀出來
                            If IsDBNull(data2.Rows(0)("applyFile")) Then
                                applyFile.Visible = False
                            Else
                                applyFile.Visible = True
                                applyFile_link.Text = data2.Rows(0)("applyFile")
                            End If
                        End If

                        '主資料
                        action.Text = data.Rows(0)("action")
                        actionTime.Text = Common.Year(data.Rows(0)("actionTime"))

                        actionInTime.Text = data.Rows(0)("InTimeHr") + "：" + data.Rows(0)("InTimeMin") '進出時間_進時'進出時間_進分
                        actionOutTime.Text = data.Rows(0)("OutTimeHr") + "：" + data.Rows(0)("OutTimeMin") '離開時間_出時'離開時間_出分

                        '配對編號
                        Dim sql_related_syspgrecordId2 = "select * from SYSIO_related where SYSIO_recordId = '" + lb_recordid.Text + "' and kind = 'PG'"
                        Dim data_related_syspg = Common.Con(sql_related_syspgrecordId2)
                        If Not data_related_syspg.Rows.Count = 0 Then
                            SYSPG_recordId2.Text = data_related_syspg.Rows(0)("related_recordId") 'SYSPG_recordId
                        End If

                        Dim sql_related_firewallrecordId2 = "select * from SYSIO_related where SYSIO_recordId = '" + lb_recordid.Text + "' and kind = 'FW'"
                        Dim data_related_firewall = Common.Con(sql_related_firewallrecordId2)
                        If Not data_related_firewall.Rows.Count = 0 Then
                            Firewall_recordId2.Text = data_related_firewall.Rows(0)("related_recordId") 'Firewall_recordId
                        Else
                            Firewall_recordId2.Text = ""
                        End If

                        '查看裡面是否有資料，有才秀出來
                        If IsDBNull(data.Rows(0)("actionFile")) Then
                            actionFile.Visible = False
                        Else
                            actionFile.Visible = True
                            actionFile_link.Text = data.Rows(0)("actionFile")
                        End If


                        SheetEnabled(False)


                        but_update.Visible = False '20200804我的申請單不開放修改
                    Else
                        Response.Redirect("SYSIO_rec_list.aspx", True) '重新載入附件表單簽核_審核 因為此表單編號已經不存在
                    End If


                    '頁面調整
                    tb_result.Visible = True
                    tb_query.Visible = False
                    GridView1.Visible = False
                    GdvShow() '載入關卡表
                Catch ex As Exception
                    lb_msg.Text = ex.Message
                End Try
            Case "using_btn"
                '撤銷
                Dim sql As New StringBuilder
                sql.Append("update SYSIO_rec set using='N' where recordId ='" + hidden_recordId.Value + "' ")
                sql.Append("  update form_record set status = -1 where recordId = '" + hidden_recordId.Value + "' and approvalId = '" + approvalid + "' and status = 1")
                sql.Append(" delete SYSIO_exam where SYSIO_recordId = '" + hidden_recordId.Value + "'")
                sql.Append("  delete SYSIO_related where SYSIO_recordId = '" + hidden_recordId.Value + "'")

                Dim conn As SqlConnection = Nothing : Dim cmd As New SqlCommand(sql.ToString)
                Try
                    conn = Common.ConnDBS : conn.Open() : cmd.Connection = conn
                    If cmd.ExecuteNonQuery() Then GridView1.DataBind() : lb_msg.Text = "撤銷成功！"
                Catch ex As Exception
                    lb_msg.Text = "撤銷失敗！" + ex.Message
                Finally
                    conn.Close() : conn.Close() : conn.Dispose() : cmd.Dispose()
                End Try

        End Select

    End Sub

    '小工具1
    Private Sub SheetEnabled(ByVal use As Boolean)
        action.Enabled = use
        btn_SYSPG.Visible = use
        btn_Firewall.Visible = use
        actionTime.Enabled = use
    End Sub

    '載入SYSIO的檢測結果
    Private Sub GdvShow1()
        Try
            Dim newTable As New DataTable

            newTable.Columns.Add("equipment")
            newTable.Columns.Add("quantity")
            newTable.Columns.Add("result")

            GridView2.DataSource = newTable
            GridView2.DataBind()

        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '檢測結果的Enabled
    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                CType(e.Row.FindControl("equipment"), TextBox).Enabled = False
                CType(e.Row.FindControl("quantity"), TextBox).Enabled = False
                CType(e.Row.FindControl("result"), TextBox).Enabled = False
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_回列表_按鈕
    Protected Sub but_esc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_esc.Click
        Response.Redirect("SYSIO_rec_list.aspx", True)
    End Sub

    '載入關卡表
    Private Sub GdvShow()
        Dim dt As DataTable = Common.Qry_workflowName_isms_Req(approvalid, lb_recordid.Text, apprvid, dp)
        GridView.DataSource = dt
        GridView.DataBind()

    End Sub

    '檔案下載_按鈕
    Protected Sub applyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + applyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料

            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            Dim hid_status As HiddenField = CType(e.Row.FindControl("hid_status"), HiddenField)
            '***意見
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text
            '***審核人員***
            'Dim lb_wf_inside_order As String = CType(e.Row.FindControl("lb_wf_inside_order"), Label).Text

            Dim ds As DataSet

            '***Start
            If o_wf_order = "0" And hid_status.Value <> "1" Then
                pan_user.Visible = True
                '將審核人員設為自己
                Dim sql = "    SELECT user_id, user_name FROM ismsUser WHERE user_id = '" + uid + "'"
                Dim data As DataTable = Common.Con(sql)
                CType(e.Row.FindControl("lb_username"), Label).Text = data.Rows(0)("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = data.Rows(0)("user_id")
            ElseIf hid_status.Value = "1" Then '待審核
                ds = Common.Qry_form_record_wait(lb_recordid.Text)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
            ElseIf hid_status.Value = "2" Or hid_status.Value = "9" Then '審核已通過
                ds = Common.Qry_form_record_name(lb_recordid.Text, o_wf_order)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
            End If

            If hid_status.Value = "" Then
                lb_status.Text = ""
            Else
                CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(hid_status.Value))
            End If
        End If
    End Sub

End Class
