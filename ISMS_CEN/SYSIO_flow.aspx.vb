﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI

Partial Class SYSIO_flow
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlCmd As SqlCommand
    Dim uid As String = User.Identity.Name '使用者名稱
    Dim uname As String = Common.Get_User_Name(User.Identity.Name.ToString) '使用者代號
    Dim SqlTxt As String
    Dim approvalid As String
    Dim apprvid As String
    Dim dp As String = Me.Get_DP
    Dim File_Path As String = ConfigurationManager.AppSettings("SYSIO_Path")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        approvalid = ConfigurationManager.AppSettings("flow_SYSIO") '10
        apprvid = ConfigurationManager.AppSettings("flow_SYSIO") '10
        Gdv1Show() '待審核表
        If Not IsPostBack Then
            Time_DataSource() '進入時間的時間下拉資料
        End If
    End Sub

    '待審核表來源
    Protected Sub Gdv1Show()
        Try
            Dim sql = " select * from  form_record a join SYSIO b on  a.recordid=b.recordid where b.using = 'Y' and approvalid='" + approvalid + "' and a.status=1 and userid='" + uid + "' order by b.applyDate"
            Dim data As DataTable = Common.Con(sql)
            GridView1.DataSource = data
            GridView1.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '簽核過程及民國年轉換
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '簽核過程
                Dim recordid As String = CType(e.Row.FindControl("lb_rowid"), Label).Text
                CType(e.Row.FindControl("lb_flow"), Label).Text = Common.Str_form_record_isms(approvalid, recordid)
                '民國年轉換
                Dim lb_applydate As String = CType(e.Row.FindControl("lb_applydate"), Label).Text
                CType(e.Row.FindControl("lb_applydate"), Label).Text = Common.Year(Date.Parse(lb_applydate))
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '審核表畫面至審核畫面_1
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Try
            Dim Index As Integer = CType(e.CommandArgument, Int32)
            Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
            Dim lb_rowid As String = (TryCast(SelectedRow.FindControl("lb_rowid"), Label)).Text

            Select Case e.CommandName
                Case "audit_btn"
                    Dim sql = "  SELECT * FROM SYSIO WHERE recordId = '" + lb_rowid + "'"
                    Dim data As DataTable = Common.Con(sql)
                    If data.Rows.Count > 0 Then
                        '紀錄該表單編號
                        lb_recordid.Text = lb_rowid
                        '載入關卡表
                        GdvShow()

                        ''填入該lb_rowid資料
                        applyDate.Text = Common.Year(data.Rows(0)("applyDate")) '申請日期
                        recordId.Text = data.Rows(0)("recordId") '表單編號
                        chk_target_1.Checked = If(data.Rows(0)("target_1") = "1", True, False) '維護標的_資訊系統
                        chk_target_2.Checked = If(data.Rows(0)("target_2") = "1", True, False) '維護標的_資訊設備
                        targetName.Text = data.Rows(0)("targetName") '標的名稱
                        applyOrg.Text = data.Rows(0)("applyOrg") '申請單位
                        applyName.Text = data.Rows(0)("applyName") '申請人員
                        chk_method_1.Checked = If(data.Rows(0)("method_1") = "1", True, False) '維護方式_親至機房
                        chk_method_2.Checked = If(data.Rows(0)("method_2") = "1", True, False) '維護方式_親至資訊(電腦)室
                        chk_method_3.Checked = If(data.Rows(0)("method_3") = "1", True, False) '維護方式_Internet連線
                        chk_method_4.Checked = If(data.Rows(0)("method_4") = "1", True, False) '維護方式_其他
                        methodOther.Text = data.Rows(0)("methodOther") '維護方式_其他內容
                        chk_location_1.Checked = If(data.Rows(0)("location_1") = "1", True, False) '存放位置_機房
                        chk_location_2.Checked = If(data.Rows(0)("location_2") = "1", True, False) '存放位置_辦公(控制)室
                        chk_location_3.Checked = If(data.Rows(0)("location_3") = "1", True, False) '存放位置_其他
                        locationOther.Text = data.Rows(0)("locationOther") '存放位置_其他內容
                        InDate.Text = Common.Year(data.Rows(0)("InDate")) '進出日期
                        Ddl_InHour.Text = data.Rows(0)("InTimeHr") '進出時間_進時
                        Ddl_InMin.Text = data.Rows(0)("InTimeMin") '進出時間_進分
                        Ddl_OutHour.Text = data.Rows(0)("OutTimeHr") '離開時間_出時
                        Ddl_OutMin.Text = data.Rows(0)("OutTimeMin") '離開時間_出分
                        applyComment.Text = data.Rows(0)("applyComment") '辦理事項
                        chk_IfChangeSoftware.Checked = If(data.Rows(0)("IfChangeSoftware") = "1", True, False) '變更程式
                        chk_IfSYSPG.Checked = If(data.Rows(0)("IfSYSPG") = "1", True, False) '軟體程式變更申請單
                        chk_IfFirewall.Checked = If(data.Rows(0)("IfFirewall") = "1", True, False) '防火牆維護申請表
                        chk_IfTest.Checked = If(data.Rows(0)("IfTest") = "1", True, False) '測試作業之程式
                        chk_IfItemIn.Checked = If(data.Rows(0)("IfItemIn") = "1", True, False) '攜入
                        chk_IfItemOut.Checked = If(data.Rows(0)("IfItemOut") = "1", True, False) '攜出
                        chk_IfItemLocation.Checked = If(data.Rows(0)("IfItemLocation") = "1", True, False) '變更位置

                        '載入選擇
                        If chk_IfSYSPG.Checked = "1" Then
                            Dim sql_related_syspgrecordId = "select * from SYSIO_related where SYSIO_recordId = '" + recordId.Text + "' and kind = 'PG'"
                            Dim data_related = Common.Con(sql_related_syspgrecordId)
                            If Not data_related.Rows.Count = 0 Then
                                SYSPG_recordId.Text = data_related.Rows(0)("related_recordId") 'SYSPG_recordId
                            End If
                        Else
                            SYSPG_recordId.Text = ""
                        End If
                        If chk_IfFirewall.Checked = "1" Then
                            Dim sql_related_firewallrecordId = "select * from SYSIO_related where SYSIO_recordId = '" + recordId.Text + "' and kind = 'FW'"
                            Dim data_related = Common.Con(sql_related_firewallrecordId)
                            If Not data_related.Rows.Count = 0 Then
                                Firewall_recordId.Text = data_related.Rows(0)("related_recordId") 'Firewall_recordId
                            End If
                        Else
                            Firewall_recordId.Text = ""
                        End If

                        '查看裡面是否有資料，有才秀出來
                        If IsDBNull(data.Rows(0)("applyFile")) Then
                            applyFile.Visible = False
                        Else
                            applyFile.Visible = True
                            applyFile_link.Text = data.Rows(0)("applyFile")
                        End If

                        Dim workflowId As Integer = Common.Get_workflowId(approvalid, recordId.Text) '取得關卡

                        '如果是第0關
                        If workflowId = 0 Then
                            SheetEnabled(True)
                            File_applyFile.Visible = True
                        Else
                            SheetEnabled(False)
                            File_applyFile.Visible = False
                        End If

                        '檢測結果
                        Dim sql_exam = "  select * from SYSIO_exam where SYSIO_recordId = '" + lb_recordid.Text + "' "
                        Dim data_exam = Common.Con(sql_exam)
                        If data_exam.Rows.Count = 0 Then
                            If workflowId = 0 Then
                                GdvShow1()
                            End If
                        Else
                            GridView2.DataSource = data_exam
                            GridView2.DataBind()
                            Session("newTable") = data_exam '紀錄
                        End If

                        '查看裡面是否有資料，有才秀出來
                        If IsDBNull(data.Rows(0)("applyFile")) Then
                            applyFile.Visible = False
                        Else
                            applyFile.Visible = True
                            applyFile_link.Text = data.Rows(0)("applyFile")
                        End If

                        '將審核畫面弄出來
                        tb_result.Visible = True
                        tr_gridview.Visible = False
                        Insert_Btn.Visible = True
                        but_esc.Visible = True
                    Else
                        Response.Redirect("SYSIO_flow.aspx", True) '重新載入附件表單簽核_審核 因為此表單編號已經不存在
                    End If


            End Select
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If chk_method_1.Enabled Then
                    CType(e.Row.FindControl("equipment"), TextBox).Enabled = True
                    CType(e.Row.FindControl("quantity"), TextBox).Enabled = True
                    CType(e.Row.FindControl("result"), TextBox).Enabled = True
                Else
                    CType(e.Row.FindControl("equipment"), TextBox).Enabled = False
                    CType(e.Row.FindControl("quantity"), TextBox).Enabled = False
                    CType(e.Row.FindControl("result"), TextBox).Enabled = False
                End If
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '小工具1
    Private Sub SheetEnabled(ByVal use As Boolean)
        chk_target_1.Enabled = use '維護標的_資訊系統
        chk_target_2.Enabled = use '維護標的_資訊設備
        targetName.Enabled = use '標的名稱
        applyOrg.Enabled = use '申請單位
        applyName.Enabled = use '申請人員
        chk_method_1.Enabled = use '維護方式_親至機房
        chk_method_2.Enabled = use '維護方式_親至資訊(電腦)室
        chk_method_3.Enabled = use '維護方式_Internet連線
        chk_method_4.Enabled = use '維護方式_其他
        methodOther.Enabled = use '維護方式_其他內容
        chk_location_1.Enabled = use '存放位置_機房
        chk_location_2.Enabled = use '存放位置_辦公(控制)室
        chk_location_3.Enabled = use '存放位置_其他
        locationOther.Enabled = use '存放位置_其他內容
        InDate.Enabled = use '進出日期
        Ddl_InHour.Enabled = use '進出時間_進時
        Ddl_InMin.Enabled = use '進出時間_進分
        Ddl_OutHour.Enabled = use '離開時間_出時
        Ddl_OutMin.Enabled = use '離開時間_出分
        applyComment.Enabled = use '辦理事項
        chk_IfChangeSoftware.Enabled = use '變更程式
        chk_IfSYSPG.Enabled = use '軟體程式變更申請單
        chk_IfFirewall.Enabled = use '防火牆維護申請表
        chk_IfTest.Enabled = use '測試作業之程式
        chk_IfItemIn.Enabled = use '攜入
        chk_IfItemOut.Enabled = use '攜出
        chk_IfItemLocation.Enabled = use '變更位置
        btn_SYSPG.Visible = use
        btn_Firewall.Visible = use
        td_Addexam.Visible = use
    End Sub

    '載入SYSIO的檢測結果(初始只有三筆空資料)
    Private Sub GdvShow1()
        Try
            Dim newTable As New DataTable

            newTable.Columns.Add("equipment")
            newTable.Columns.Add("quantity")
            newTable.Columns.Add("result")

            newTable.Rows.Add("", "", "")
            newTable.Rows.Add("", "", "")
            newTable.Rows.Add("", "", "")

            GridView2.DataSource = newTable
            GridView2.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '按鈕_新增檢測結果
    Protected Sub Btn_Addexam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Addexam.Click
        Try

            Dim newTable As New DataTable
            newTable.Columns.Add("equipment")
            newTable.Columns.Add("quantity")
            newTable.Columns.Add("result")

            Dim count = GridView2.Rows.Count
            For Item As Integer = 0 To count - 1 Step 1
                Dim equipment = CType(GridView2.Rows(Item).Cells(0).FindControl("equipment"), TextBox).Text
                Dim quantity = CType(GridView2.Rows(Item).Cells(0).FindControl("quantity"), TextBox).Text
                Dim result = CType(GridView2.Rows(Item).Cells(0).FindControl("result"), TextBox).Text
                newTable.Rows.Add(equipment, quantity, result)
            Next

            newTable.Rows.Add("", "", "")
            GridView2.DataSource = newTable
            GridView2.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub


    '時間資料來源
    Private Sub Time_DataSource()
        Try
            '進入時間
            Ddl_InHour.Items.Clear()
            Ddl_InMin.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                Ddl_InHour.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                Ddl_InMin.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            '離開時間
            Ddl_OutHour.Items.Clear()
            Ddl_OutMin.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                Ddl_OutHour.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                Ddl_OutMin.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '按鈕_主頁至軟體程式變更申請單的"選擇"(1)
    Protected Sub btn_SYSPG_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_SYSPG.Click
        Try
            GdvTabel2_2(DateTime.Now.AddMonths(-1), DateTime.Now) '載入選擇表格的資料(SYSPG)
            Table1.Visible = False
            Table2_1.Visible = True
            Table2_2.Visible = True
            Table2_3.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_載入SYSPG
    Private Sub GdvTabel2_2(ByVal Startdate As Date, ByRef Enddate As Date)
        Try
            Dim sql As String = "select * from SYSPG where using = 'Y' and applyDate between '" + Startdate.ToString("yyyy/MM/dd") + "' and '" + Enddate.ToString("yyyy/MM/dd") + "'"
            sdate.Text = Common.Year(Startdate.ToString("yyyy/MM/dd"))
            edate.Text = Common.Year(Enddate.ToString("yyyy/MM/dd"))
            Dim data As DataTable = Common.Con(sql)
            For Each item In data.Select
                item("empName") = Common.Year(DateTime.Parse(item("applyDate")).ToString("yyyy/MM/dd"))
            Next
            GridView3.DataSource = data
            GridView3.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '按鈕_主頁至房活強維護申請表的"選擇"(2)
    Protected Sub btn_Firewall_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Firewall.Click
        Try
            GdvTabel2_3(DateTime.Now.AddMonths(-1), DateTime.Now) '載入選擇表格的資料(SYSPG)
            Table1.Visible = False
            Table2_1.Visible = True
            Table2_2.Visible = False
            Table2_3.Visible = True
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '2_載入Firewall
    Private Sub GdvTabel2_3(ByVal Startdate As Date, ByRef Enddate As Date)
        Try
            Dim sql As String = "select * from Firewall where using = 'Y' and applyDate between '" + Startdate.ToString("yyyy/MM/dd") + "' and '" + Enddate.ToString("yyyy/MM/dd") + "'"
            sdate.Text = Common.Year(Startdate.ToString("yyyy/MM/dd"))
            edate.Text = Common.Year(Enddate.ToString("yyyy/MM/dd"))
            Dim data As DataTable = Common.Con(sql)
            For Each item In data.Select
                item("empName") = Common.Year(DateTime.Parse(item("applyDate")).ToString("yyyy/MM/dd"))
            Next
            GridView4.DataSource = data
            GridView4.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '按鈕_搜尋表單編號日期區間
    Protected Sub Btn_applyDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_applyDate.Click
        Try
            If Table2_2.Visible Then
                GdvTabel2_2(DateTime.Parse(Common.YearYYYY(sdate.Text)), DateTime.Parse(Common.YearYYYY(edate.Text)))
            Else
                GdvTabel2_3(DateTime.Parse(Common.YearYYYY(sdate.Text)), DateTime.Parse(Common.YearYYYY(edate.Text)))
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_選取某表單編號_至主頁(SYSPG)
    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView3.RowCommand
        Try
            Dim GETrecordId As String = e.CommandArgument
            SYSPG_recordId.Text = GETrecordId
            chk_IfSYSPG.Checked = True
            chk_IfChangeSoftware.Checked = True

            Table1.Visible = True
            Table2_1.Visible = False
            Table2_2.Visible = False
            Table2_3.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '2_選取某表單編號_至主頁(Firewall)
    Protected Sub GridView3_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView4.RowCommand
        Try
            Dim GETrecordId As String = e.CommandArgument
            Firewall_recordId.Text = GETrecordId
            chk_IfFirewall.Checked = True
            chk_IfChangeSoftware.Checked = True

            Table1.Visible = True
            Table2_1.Visible = False
            Table2_2.Visible = False
            Table2_3.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '關卡表
    Private Sub GdvShow()
        Try
            Dim dt As DataTable = Common.Qry_workflowName_isms_Req(approvalid, lb_recordid.Text, apprvid, dp)
            GridView.DataSource = dt
            GridView.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub



    '關卡表_RowDataBound
    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '***已簽核資料

                '***人員
                Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
                Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
                Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
                Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
                '***狀態
                Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
                Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
                Dim hid_status As HiddenField = CType(e.Row.FindControl("hid_status"), HiddenField)
                '***意見
                Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
                Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
                '***關卡***
                Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text
                '***審核人員***
                'Dim lb_wf_inside_order As String = CType(e.Row.FindControl("lb_wf_inside_order"), Label).Text

                Dim ds As DataSet


                '***Start
                If o_wf_order = "0" And hid_status.Value <> "1" Then
                    pan_user.Visible = True
                    '將審核人員設為自己
                    Dim sql = "    SELECT user_id, user_name FROM ismsUser WHERE user_id = '" + uid + "'"
                    Dim data As DataTable = Common.Con(sql)
                    CType(e.Row.FindControl("lb_username"), Label).Text = data.Rows(0)("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = data.Rows(0)("user_id")
                ElseIf hid_status.Value = "1" Then '待審核
                    ds = Common.Qry_form_record_wait(lb_recordid.Text)
                    pan_user.Visible = True
                    CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
                    lb_count.Text = e.Row.RowIndex
                ElseIf hid_status.Value = "2" Or hid_status.Value = "9" Then '審核已通過
                    ds = Common.Qry_form_record_name(lb_recordid.Text, o_wf_order)
                    pan_user.Visible = True
                    CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
                ElseIf e.Row.RowIndex = CType(lb_count.Text, Integer) + 1 Then '待審核下一關卡
                    ddl_user.Visible = True
                    ds = Common.Get_Wf_User(dp, apprvid, o_wf_order, "")
                    ddl_user.DataSource = ds
                    ddl_user.DataTextField = "name_id"
                    ddl_user.DataValueField = "wf_userid"
                    ddl_user.DataBind()
                End If

                If hid_status.Value = "1" Then '待審核
                    ddl_status.Visible = True
                    lb_status.Visible = False
                    txt_comment.Visible = True
                    lb_comment.Visible = False
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(1)
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()

                ElseIf hid_status.Value = "" Then
                    ddl_status.Visible = False
                    lb_status.Visible = True
                    txt_comment.Visible = False
                    lb_comment.Visible = True
                Else
                    ddl_status.Visible = False
                    lb_status.Visible = True
                    txt_comment.Visible = False
                    lb_comment.Visible = True
                    CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(hid_status.Value))
                End If
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '檔案下載_按鈕
    Protected Sub applyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + applyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub


    '返回列表_按鈕
    Protected Sub but_esc_click()
        tr_gridview.Visible = True
        tb_result.Visible = False
        Insert_Btn.Visible = False
        but_esc.Visible = False
        Gdv1Show()
    End Sub

    '送出審核_按鈕
    Protected Sub Insert_Btn_click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim index As Integer = CType(lb_count.Text, Integer)
        Dim max_index As Integer = GridView.Rows.Count

        Dim wf_order As String = CType(GridView.Rows(index).FindControl("wf_order"), Label).Text '待審核的關卡
        Dim status As String = CType(GridView.Rows(index).FindControl("ddl_status"), DropDownList).SelectedValue '狀態
        Dim comment As String = CType(GridView.Rows(index).FindControl("txb_comment"), TextBox).Text
        Dim id As String = CType(GridView.Rows(index).FindControl("lb_id2"), Label).Text
        Dim stage As Integer = Common.Get_workflowId(approvalid, recordId.Text) '取得關卡

        If status = 2 Then '通過
            If index + 1 < GridView.Rows.Count Then '流程尚未到最後關卡

                If stage = 0 Then

                    '回到第0關
                    If Not String.IsNullOrEmpty(InDate.Text) Then
                        Dim inTime = DateTime.Parse(Common.YearYYYY(InDate.Text + " " + Ddl_InHour.SelectedValue + ":" + Ddl_InMin.SelectedValue))
                        Dim outTime = DateTime.Parse(Common.YearYYYY(InDate.Text + " " + Ddl_OutHour.SelectedValue + ":" + Ddl_OutMin.SelectedValue))
                        If inTime > outTime Then
                            Common.showMsg(Me.Page, "離開時間需大於進入時間")
                            Return
                        End If
                    End If

                    If (Not chk_target_1.Checked And Not chk_target_2.Checked) Or String.IsNullOrEmpty(targetName.Text) _
                     Or String.IsNullOrEmpty(applyOrg.Text) Or String.IsNullOrEmpty(applyName.Text) _
                     Or (Not chk_method_1.Checked And Not chk_method_2.Checked And Not chk_method_3.Checked And (Not chk_method_4.Checked Or String.IsNullOrEmpty(methodOther.Text))) _
                     Or (Not chk_location_1.Checked And Not chk_location_2.Checked And (Not chk_location_3.Checked Or String.IsNullOrEmpty(locationOther.Text))) _
                     Or String.IsNullOrEmpty(InDate.Text) Or String.IsNullOrEmpty(applyComment.Text) Then
                        Common.showMsg(Me.Page, "請確實填寫資訊")
                        Return
                    End If

                    '處理勾取資料
                    Dim target_1 = If(chk_target_1.Checked, "1", "0")
                    Dim target_2 = If(chk_target_2.Checked, "1", "0")

                    Dim method_1 = If(chk_method_1.Checked, "1", "0")
                    Dim method_2 = If(chk_method_2.Checked, "1", "0")
                    Dim method_3 = If(chk_method_3.Checked, "1", "0")
                    Dim method_4 = If(chk_method_4.Checked, "1", "0")
                    Dim method_other = If(chk_method_4.Checked, methodOther.Text, "")

                    Dim location_1 = If(chk_location_1.Checked, "1", "0")
                    Dim location_2 = If(chk_location_2.Checked, "1", "0")
                    Dim location_3 = If(chk_location_3.Checked, "1", "0")
                    Dim location_other = If(chk_location_3.Checked, locationOther.Text, "")

                    Dim IfChangeSoftware = If(chk_IfChangeSoftware.Checked, "1", "0")
                    Dim IfSYSPG = If(Not String.IsNullOrEmpty(SYSPG_recordId.Text) And chk_IfSYSPG.Checked, "1", "0")
                    Dim IfFirewall = If(Not String.IsNullOrEmpty(Firewall_recordId.Text) And chk_IfFirewall.Checked, "1", "0")
                    Dim IfTest = If(chk_IfTest.Checked, "1", "0")

                    Dim IfItemIn = If(chk_IfItemIn.Checked, "1", "0")
                    Dim IfItemOut = If(chk_IfItemOut.Checked, "1", "0")
                    Dim IfItemLocation = If(chk_IfItemLocation.Checked, "1", "0")
                    'insert into SYSIO_exam (recordId,SYSIO_recordId,equipment,quantity,result,create_time,op_time)values('','','','','','','')

                    Dim count = GridView2.Rows.Count
                    '先刪掉原本的
                    Dim sql_Forexam = " delete SYSIO_exam where SYSIO_recordId = '" + recordId.Text + "'"
                    Dim time = DateTime.Now
                    For Item As Integer = 0 To count - 1 Step 1
                        Dim equipment = CType(GridView2.Rows(Item).Cells(0).FindControl("equipment"), TextBox).Text
                        Dim quantity = CType(GridView2.Rows(Item).Cells(0).FindControl("quantity"), TextBox).Text
                        Dim result = CType(GridView2.Rows(Item).Cells(0).FindControl("result"), TextBox).Text
                        If Not String.IsNullOrEmpty(equipment) And Not String.IsNullOrEmpty(quantity) And Not String.IsNullOrEmpty(result) Then
                            sql_Forexam += " insert into SYSIO_exam (recordId,SYSIO_recordId,equipment,quantity,result,create_time,op_time)values('Sioexam" + time.AddMilliseconds(Item).ToString("yyyymmddHHmmssfff") + "','" + recordId.Text + "','" + equipment + "','" + quantity + "','" + result + "',getdate(),getdate())"
                        End If
                    Next
                    Dim SqlCmd = New SqlCommand(sql_Forexam, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()

                    '先刪除原本的
                    SqlTxt = "  delete SYSIO_related where SYSIO_recordId = '" + recordId.Text + "'"
                    '如果有選取軟體程式變更申請表
                    If IfSYSPG = "1" Then
                        SqlTxt += " insert into SYSIO_related (SYSIO_recordId,kind,related_recordId)values('" + recordId.Text + "','PG','" + SYSPG_recordId.Text + "')"
                    End If
                    '如果有選取防火牆維護申請表
                    If IfFirewall = "1" Then
                        SqlTxt += " insert into SYSIO_related (SYSIO_recordId,kind,related_recordId)values('" + recordId.Text + "','FW','" + Firewall_recordId.Text + "')"
                    End If

                    '處理上傳檔案
                    If CType(Me.FindControl("File_applyFile"), FileUpload).HasFile Then
                        '有檔案
                        Dim oriFileName = CType(Me.FindControl("File_applyFile"), FileUpload)
                        Dim UpFileName As String = Common.UpFile(File_Path, recordId.Text, CType(Me.FindControl("File_applyFile"), FileUpload))
                        '塞入主表資料
                        SqlTxt += " update SYSIO set target_1 = '" + target_1 + "',target_2 = '" + target_2 + "',targetName = '" + targetName.Text + "',applyOrg = '" + applyOrg.Text + "',applyName = '" + applyName.Text + "'," _
                            + "method_1 = '" + method_1 + "',method_2 = '" + method_2 + "',method_3 = '" + method_3 + "',method_4 = '" + method_4 + "',methodOther = '" + method_other + "'," _
                            + "location_1 = '" + location_1 + "',location_2 = '" + location_2 + "',location_3 = '" + location_3 + "',locationOther = '" + location_other + "',InDate = '" + Common.YearYYYY(InDate.Text) + "',InTimeHr = '" + Ddl_InHour.SelectedValue + "',InTimeMin = '" + Ddl_InMin.SelectedValue + "'," _
                            + "OutTimeHr = '" + Ddl_OutHour.SelectedValue + "',OutTimeMin = '" + Ddl_OutMin.SelectedValue + "',applyFile_ori_name = '" + oriFileName.FileName + "',applyFile = '" + UpFileName + "'," _
                            + "applyComment = '" + applyComment.Text + "',IfChangeSoftware = '" + IfChangeSoftware + "',IfSYSPG = '" + IfSYSPG + "',IfFirewall = '" + IfFirewall + "'," _
                            + "IfTest = '" + IfTest + "',IfItemIn = '" + IfItemIn + "',IfItemOut = '" + IfItemOut + "',IfItemLocation = '" + IfItemLocation + "',op_time = getdate() where recordId = '" + recordId.Text + "'"

                    Else
                        '沒有檔案
                        '塞入主表資料
                        SqlTxt += " update SYSIO set target_1 = '" + target_1 + "',target_2 = '" + target_2 + "',targetName = '" + targetName.Text + "',applyOrg = '" + applyOrg.Text + "',applyName = '" + applyName.Text + "'," _
                            + "method_1 = '" + method_1 + "',method_2 = '" + method_2 + "',method_3 = '" + method_3 + "',method_4 = '" + method_4 + "',methodOther = '" + method_other + "'," _
                            + "location_1 = '" + location_1 + "',location_2 = '" + location_2 + "',location_3 = '" + location_3 + "',locationOther = '" + location_other + "',InDate = '" + Common.YearYYYY(InDate.Text) + "',InTimeHr = '" + Ddl_InHour.SelectedValue + "',InTimeMin = '" + Ddl_InMin.SelectedValue + "'," _
                            + "OutTimeHr = '" + Ddl_OutHour.SelectedValue + "',OutTimeMin = '" + Ddl_OutMin.SelectedValue + "'," _
                            + "applyComment = '" + applyComment.Text + "',IfChangeSoftware = '" + IfChangeSoftware + "',IfSYSPG = '" + IfSYSPG + "',IfFirewall = '" + IfFirewall + "'," _
                            + "IfTest = '" + IfTest + "',IfItemIn = '" + IfItemIn + "',IfItemOut = '" + IfItemOut + "',IfItemLocation = '" + IfItemLocation + "',op_time = getdate() where recordId = '" + recordId.Text + "'"

                    End If

                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                End If


                Dim workflowid As String = CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) + 1
                Dim user_id = CType(GridView.Rows(index + 1).FindControl("ddl_user"), DropDownList).SelectedValue

                SqlTxt = " INSERT INTO form_record (approvalId,workflowId,userId,userType,status,comment,recordId,signDate,role,dp) VALUES "
                SqlTxt += " ('" + approvalid + "','" + workflowid + "','" + user_id + "','land','1','','" + lb_recordid.Text + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + workflowid + "','" + dp + "') "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            End If
            SqlTxt = " UPDATE form_record SET status='2' , signDate ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' , comment = '" + comment + "' WHERE status != 2 and status !=0 and approvalId='" + approvalid + "'and recordId ='" + lb_recordid.Text + "' and id ='" + id + "' "
        ElseIf status = 9 Then '退回
            If CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) > 0 Then '目前流程不是第一關卡
                Dim workflowid As String = CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) - 1
                'Dim user_id = CType(GridView.Rows(index - 1).FindControl("label_userid"), Label).Text
                Dim dt As DataTable = Common.Qry_form_record1(approvalid, lb_recordid.Text, workflowid)
                Dim user_id As String = dt.Rows(0).Item("userId").ToString

                SqlTxt = " INSERT INTO form_record (approvalId,workflowId,userId,userType,status,comment,recordId,signDate,role,dp) VALUES "
                SqlTxt += " ('" + approvalid + "','" + workflowid + "','" + user_id + "','land','1','','" + lb_recordid.Text + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + workflowid + "','" + dp + "') "

                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            Else
                Common.showMsg(Me.Page, "流程已到填表人，無法再退回！")
                Return
            End If

            SqlTxt = " UPDATE form_record SET status='9' , signDate ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' , comment = '" + comment + "' WHERE approvalId='" + approvalid + "'and recordId ='" + lb_recordid.Text + "' and id ='" + id + "' "

        End If
        Dim cmd As New SqlCommand(SqlTxt.ToString)

        Try
            Conn = Common.ConnDBS : Conn.Open() : cmd.Connection = Conn
            If cmd.ExecuteNonQuery() Then Common.showMsg(Me.Page, "審核完成！") : but_esc_click()
        Catch ex As Exception
            Common.showMsg(Me.Page, "審核失敗！")
        Finally
            Conn.Close() : Conn.Close() : Conn.Dispose() : cmd.Dispose()
        End Try
    End Sub

End Class
