﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class AchieveDoc
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim File_Path As String = ConfigurationManager.AppSettings("AchieveDoc_Path")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        If Not IsPostBack Then
            MainDataSource() '成果檔案管理主表
            MainEditTable.Visible = False : UploadTable.Visible = False : LibaryTable.Visible = False '其餘先隱藏
        End If
    End Sub

    '主頁Table來源
    Protected Sub MainDataSource()
        Try
            MainSource.SelectCommand = "SELECT ROW_NUMBER() OVER(ORDER BY A.acdm_Name) AS Num , A.acdm_Id,A.acdm_Name, A.acds_time, a.acds_images1 FROM ( SELECT  A.acdm_Id, A.acdm_Name, MAX(B.acds_time) AS acds_time, MAX(B.acds_images1) AS acds_images1 FROM AchieveDocMain A, AchieveDocSub B WHERE A.acdm_Id = acds_acdmId GROUP BY A.acdm_Id, A.acdm_Name ) A "
            MainSource.ConnectionString = Common.ConnDBS.ConnectionString
            MainGridView.DataSourceID = MainSource.ID
            Dim KeyNames() As String = {"Num"}
            MainGridView.DataKeyNames = KeyNames
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try
    End Sub

    '主頁Table處理民國年
    Protected Sub MainGridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles MainGridView.RowDataBound
        Try
            Dim oDataRow As Data.DataRowView
            Dim oriYear As String
            If e.Row.RowIndex <> -1 Then
                oDataRow = CType(e.Row.DataItem, Data.DataRowView)
                oriYear = oDataRow.Item("acds_time").ToString()
                e.Row.Cells(2).Text = Common.Year(oriYear)
            End If
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try
    End Sub

    '主頁_下載
    Protected Sub MainDownLoad(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim sql = "SELECT * FROM AchieveDocSub WHERE acds_images1 = '" + CType(sender, Button).CommandArgument.Replace(";", "") + "'"
            Dim data As DataTable = Common.Con(sql)

            Common.DownFileForOriFileName(Me.Page, File_Path + CType(sender, Button).CommandArgument.Replace(";", ""), data.Rows(0)("acds_images1_ori_name"))
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try
    End Sub

    '主頁面至文件名稱維護_按鈕_1
    Protected Sub Btn_FileEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_FileEdit.Click
        Try
           MainEditTable.Visible = True : MainTable.Visible = False
            MainEditType.SelectedValue = "0" : MainEditType_selectedchange(sender, e) '預設為新增
            FileName_DataSource() '文件名稱維護文件名稱的下拉Orderby創建時間desc
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try
    End Sub

    '1_RadioButton_SlectedChange
    Protected Sub MainEditType_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            MainEditText_FileName.Text = ""
            Select Case MainEditType.SelectedValue
                Case "0"
                    '新增
                    otr.Visible = False
                    ntr.Visible = True
                    MainEditText_FileName.Visible = True
                    MainEditBtn.Visible = False
                    MainEditBtn_Save.Visible = True
                Case "1"
                    '修改
                    ntr.Visible = True
                    otr.Visible = True
                    MainEditDdl_FileName.Visible = True
                    MainEditText_FileName.Visible = True
                    MainEditBtn.Visible = False
                    MainEditBtn_Save.Visible = True
                Case "2"
                    '刪除
                    ntr.Visible = False
                    otr.Visible = True
                    MainEditDdl_FileName.Visible = True
                    MainEditBtn.Visible = True '確定按鈕為True
                    MainEditBtn_Save.Visible = False
                Case Else
                    '沒這可能
            End Select
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try
    End Sub

    '1_儲存(包含新增跟更新文件名稱)
    Protected Sub MainEditBtn_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MainEditBtn_Save.Click
        Try
            Dim sql = "SELECT * FROM AchieveDocMain WHERE acdm_Name = '" + MainEditText_FileName.Text + "'"
            Dim data As DataTable = Common.Con(sql)
            If data.Rows.Count = 0 And Not (String.IsNullOrEmpty(MainEditText_FileName.Text)) Then '確認該名稱目前沒有被使用
                If (otr.Visible) Then
                    '更新
                    SqlTxt = "UPDATE AchieveDocMain set acdm_Name = '" + MainEditText_FileName.Text + "' where acdm_Id = '" + MainEditDdl_FileName.SelectedValue + "'"
                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                    Common.showMsg(Me.Page, "更新文件名稱!")
                Else
                    '新增
                    SqlTxt = "INSERT INTO AchieveDocMain (acdm_Id,acdm_Name,acdm_userId,acdm_time) values ('acdm" + Date.Now.ToString("yyyyMMddHHmmssfff") + "','" + MainEditText_FileName.Text + "','" + User.Identity.Name.ToString + "',GETDATE()  )"
                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                    Common.showMsg(Me.Page, "新增文件名稱!")
                End If
                MainEditText_FileName.Text = "" '清空文件名稱TextBox
                FileName_DataSource() '更新下拉
            Else
                Common.showMsg(Me.Page, "文件名稱不得重複或空白!")
            End If
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try
    End Sub

    '1_確定(包含刪除文件名稱)
    Protected Sub MainEditBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MainEditBtn.Click
        Try
            Dim sql = "SELECT * FROM AchieveDocSub WHERE acds_acdmId = '" + MainEditDdl_FileName.SelectedValue + "'"
            Dim data As DataTable = Common.Con(sql)
            If data.Rows.Count = 0 Then
                '刪除(檢查該文件名稱是否還有檔案)
                SqlTxt = " DELETE AchieveDocMain where acdm_Id = '" + MainEditDdl_FileName.SelectedValue + "'"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.showMsg(Me.Page, "刪除文件名稱!")
                MainEditText_FileName.Text = "" '清空文件名稱TextBox
                FileName_DataSource() '更新下拉
            Else
                Common.showMsg(Me.Page, "該文件名稱尚有檔案!")
            End If
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try
    End Sub

    '1_取消
    Protected Sub MainEditBtn_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MainEditBtn_Cancel.Click
        MainDataSource() '成果檔案管理主表
        MainTable.Visible = True : MainEditTable.Visible = False : UploadTable.Visible = False : LibaryTable.Visible = False '其餘先隱藏
    End Sub


    '1、2_文件名稱下拉來源
    Public Sub FileName_DataSource()
        Try
            Dim sql As String = "SELECT * FROM AchieveDocMain"
            sql += " order by acdm_time desc "
            MainEditDdl_FileName.Items.Clear()
            MainEditDdl_FileName.DataTextField = "acdm_Name"
            MainEditDdl_FileName.DataValueField = "acdm_Id"
            MainEditDdl_FileName.DataSource = Common.Con(sql)
            MainEditDdl_FileName.DataBind()
            UploadDdl_FileName.Items.Clear()
            UploadDdl_FileName.DataTextField = "acdm_Name"
            UploadDdl_FileName.DataValueField = "acdm_Id"
            UploadDdl_FileName.DataSource = Common.Con(sql)
            UploadDdl_FileName.DataBind()
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try
    End Sub



    '主頁_歷程_2
    Protected Sub goLibary(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            LibaryTable.Visible = True : MainTable.Visible = False
            LibaryDdl_Year_DataSource(CType(sender, Button).CommandArgument) '歷程下拉
            LibaryDataSource(CType(sender, Button).CommandArgument) '歷程表格資料
            Hidden_acds_acdmId.Value = CType(sender, Button).CommandArgument '紀錄該歷程的acdm_Id
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try
    End Sub

    '2_歷程Table來源
    Protected Sub LibaryDataSource(ByVal acdm_Id As String)
        LibarySource.SelectCommand = "SELECT ROW_NUMBER() OVER(ORDER BY acds_Id) AS Num,acds_Id, acds_userName, acds_year,acds_time , acds_comment, acds_images1 FROM AchieveDocSub WHERE acds_acdmId = '" + acdm_Id + "' AND acds_year = " + LibaryDdl_Year.SelectedValue
        LibarySource.ConnectionString = Common.ConnDBS.ConnectionString
        LibaryGridView.DataSourceID = LibarySource.ID
        Dim KeyNames() As String = {"Num"}
        LibaryGridView.DataKeyNames = KeyNames
    End Sub

    '2_歷程Table需隱藏欄位
    Protected Sub LibaryGridView_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles LibaryGridView.RowCreated
        'Response.Write(e.Row.Cells(2).Visible)
        e.Row.Cells(0).Visible = False
        e.Row.Cells(1).Visible = False
    End Sub

    '2_RadioButton_SelectedChange
    Protected Sub LibaryDdl_Year_selectedchange(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            LibaryDataSource(Hidden_acds_acdmId.Value)
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try
    End Sub

    '2_刪除的confirm_處理民國年
    Protected Sub LibaryGridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles LibaryGridView.RowDataBound
        Try
            Dim oDataRow As Data.DataRowView
            Dim oriYear As String
            If e.Row.RowIndex <> -1 Then
                CType(e.Row.Cells(7).Controls(0), Button).OnClientClick = "if (confirm('您確定要刪除嗎?')==false) {return false;}"
                oDataRow = CType(e.Row.DataItem, Data.DataRowView)
                oriYear = oDataRow.Item("acds_time").ToString()
                e.Row.Cells(4).Text = Common.Year(oriYear)
            End If
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try
    End Sub

    '2_歷程頁面表個內按鈕(包含下載、刪除)
    Protected Sub LibaryGridView_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles LibaryGridView.RowCommand
        Try
            Dim Index As Integer = CType(e.CommandArgument, Int32)
            Dim SelectedRow As GridViewRow = LibaryGridView.Rows(Index)

            Select Case e.CommandName
                Case "LibaryBtn_Delete"
                    '刪除
                    Dim acds_Id As String = SelectedRow.Cells(0).Text '抓取該acds_Id
                    SqlTxt = " DELETE AchieveDocSub where acds_Id = '" + acds_Id + "'"
                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                    Common.showMsg(Me.Page, "刪除成功!")

                    Dim sql = "SELECT ROW_NUMBER() OVER(ORDER BY acds_Id) AS Num,acds_Id, acds_userName, acds_year,acds_time , acds_comment, acds_images1 FROM AchieveDocSub WHERE acds_acdmId = '" + Hidden_acds_acdmId.Value + "'"
                    Dim data As DataTable = Common.Con(sql)
                    If data.Rows.Count = 0 Then
                        Response.Redirect("AchieveDoc.aspx", True) '因為沒有資料了所以返回主頁
                    Else
                        LibaryDdl_Year_DataSource(Hidden_acds_acdmId.Value) '歷程下拉
                        LibaryDataSource(Hidden_acds_acdmId.Value) '歷程表格資料
                    End If

                Case "LibaryBtn_DownLoad"
                    '下載
                    Dim acds_images1 As String = SelectedRow.Cells(1).Text '抓取該acds_images1
                    Common.DownFile(Me.Page, File_Path + acds_images1)
            End Select
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try
    End Sub

    '2_歷程年份下拉
    Public Sub LibaryDdl_Year_DataSource(ByVal acdm_Id As String)
        Dim sql As String = "SELECT DISTINCT acds_year FROM AchieveDocSub where acds_acdmId = '" + acdm_Id + "' order by acds_year desc"
        LibaryDdl_Year.Items.Clear()
        LibaryDdl_Year.DataTextField = "acds_year"
        LibaryDdl_Year.DataValueField = "acds_year"
        LibaryDdl_Year.DataSource = Common.Con(sql)
        LibaryDdl_Year.DataBind()
    End Sub

    '2_歷程至主頁_按鈕
    Protected Sub LibaryBtn_Back_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LibaryBtn_Back.Click
        Response.Redirect("AchieveDoc.aspx", True) '返回主頁
    End Sub

    '主頁面至檔案上傳_按鈕_3
    Protected Sub Btn_goUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_goUpload.Click
        UploadTable.Visible = True : MainTable.Visible = False
        FileName_DataSource() '檔案上傳文件名稱的下拉Orderby創建時間desc
        DateDay.Text = Common.Year(Date.Today)
        UserName.Text = Common.Get_User_Name(User.Identity.Name.ToString)
    End Sub

    '3_取消
    Protected Sub UploadBtn_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UploadBtn_Cancel.Click
        MainDataSource() '成果檔案管理主表
        MainTable.Visible = True : MainEditTable.Visible = False : UploadTable.Visible = False : LibaryTable.Visible = False '其餘先隱藏
    End Sub

    '3_儲存
    Protected Sub UploadBtn_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UploadBtn_Save.Click
        Try
            If CType(Me.FindControl("acds_images1"), FileUpload).HasFile Then
                Dim now As Date = Date.Now
                Dim year As String = Common.Y(now)
                Dim acds_Id As String = "acds" + now.ToString("yyyyMMddHHmmssfff")
                Dim oriFileName = CType(Me.FindControl("acds_images1"), FileUpload)
                Dim UpFileName As String = Common.UpFile(File_Path, acds_Id, CType(Me.FindControl("acds_images1"), FileUpload))
                SqlTxt = "INSERT INTO AchieveDocSub ([acds_Id],[acds_acdmId],[acds_year],[acds_userId],[acds_userName],[acds_time],[acds_comment],[acds_images1_ori_name],[acds_images1])VALUES('" + acds_Id + "','" + UploadDdl_FileName.SelectedValue + "','" + year + "','" + User.Identity.Name.ToString + "','" + UserName.Text + "','" + now.ToString("yyyy/MM/dd HH:mm:ss") + "','" + UploadText_FileComment.Text + "','" + oriFileName.FileName + "','" + UpFileName + "')"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.showMsg(Me.Page, "檔案上傳完成!")
                UploadText_FileComment.Text = ""
            End If
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try
    End Sub


    ''共用_連結資料庫抓表格
    'Public Function Con(ByVal sql As String) As DataTable
    '    Dim cmd As SqlCommand = New SqlCommand()
    '    Dim SqlconnStr As String = ConfigurationManager.ConnectionStrings("ISMS_DBConnectionString").ConnectionString
    '    cmd.Connection = New SqlConnection(SqlconnStr)
    '    cmd.CommandText = Sql
    '    Dim da As New SqlDataAdapter(cmd)
    '    Dim dt As New DataTable
    '    da.Fill(dt)
    '    Return dt
    'End Function

    ''西元轉民國
    'Public Function Year(ByVal Timer As Date) As String
    '    Dim dtNow As Date = CDate(Timer)
    '    Dim twC = New System.Globalization.TaiwanCalendar()
    '    Dim date1 As String
    '    date1 = Integer.Parse(twC.GetYear(dtNow)).ToString() + dtNow.ToString("/MM/dd")
    '    Return date1
    'End Function

    ''西元轉民國(只有年)
    'Public Function Y(ByVal Timer As Date) As String
    '    Dim dtNow As Date = CDate(Timer)
    '    Dim twC = New System.Globalization.TaiwanCalendar()
    '    Dim date1 As String
    '    date1 = Integer.Parse(twC.GetYear(dtNow)).ToString()
    '    Return date1
    'End Function

End Class