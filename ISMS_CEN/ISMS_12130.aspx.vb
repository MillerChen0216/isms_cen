﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class ISMS_12130
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim SqlDa As SqlDataAdapter
    Dim SqlDs As DataSet
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim i As Integer

    Protected Sub Disable_SqlDS()
        Disable_DS.ConnectionString = Common.ConnDBS.ConnectionString
        Disable_DS.SelectCommand = "SELECT * FROM docResoure where doc_status ='DEL_PUBLISH'   and dp='" + Hidden_DP.Value + "' order by issue_date"
        GridView1.DataSourceID = Disable_DS.ID
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP

        Disable_SqlDS()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)


        Select Case e.CommandName

            Case "Disable_CMD"

                SqlTxt = "UPDATE docResoure  SET Disable_docUser='" + User.Identity.Name.ToString + "',latest='NO',locked='0',doc_status = 'DEL',abort_date='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "' WHERE doc_id='" + SelectedRow.Cells(6).Text + "'"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()

                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", SelectedRow.Cells(6).Text, System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DOCDEL", SelectedRow.Cells(2).Text)
                Common.SendMail(Common.Get_Userid(SelectedRow.Cells(4).Text), "DocDisable", SelectedRow.Cells(1).Text, SelectedRow.Cells(2).Text, "")

        End Select
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(6).Visible = False
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        Dim sMessage, sShowField As String
        Dim oDataRow As Data.DataRowView
        If e.Row.RowIndex <> -1 Then
            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = e.Row.Cells(1).Text
            sMessage = String.Format("您確定要廢止此份 [{0}] 文件嗎?", sShowField)
            CType(e.Row.Cells(5).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"

            If e.Row.RowType <> DataControlRowType.Header Then
                e.Row.Cells(0).Text = Common.ISMS_DocNameChange(e.Row.Cells(0).Text)
                e.Row.Cells(4).Text = Common.Get_User_Name(e.Row.Cells(4).Text)
            End If
        End If

    End Sub

End Class
