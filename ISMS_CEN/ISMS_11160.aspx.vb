﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_11160
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim SqlDr As SqlDataReader
    Protected Sub Show_DDL_DocType()

        Dim ds As New DataSet
        ds = Common.Get_DocType

        DDL_Type.DataSource = Common.Get_DocType
        DDL_Type.DataTextField = "sValue"
        DDL_Type.DataValueField = "doc_type"
        DDL_Type.DataBind()
        DDL_Type.Items.Insert(ds.Tables(0).Rows.Count, New ListItem("人員", "UserType"))

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP

        If Not Page.IsPostBack Then
            Show_DDL_DocType()
        End If


    End Sub

    Protected Sub Doc_SqlDS()
        Doc_DS.ConnectionString = Common.ConnDBS.ConnectionString
        If DDL_Type.SelectedValue = "UserType" Then
            SqlTxt = "Select user_id from ismsuser WHERE left(user_id,2)='" + Hidden_DP.Value + "'"
            If Keyword_txt.Text <> "" Then
                SqlTxt = SqlTxt + " AND (user_id Like  '%" + Keyword_txt.Text + "%'  or user_name  Like '%" + Keyword_txt.Text + "%' )"
            End If
            Doc_DS.SelectCommand = SqlTxt
            GridView4.DataSourceID = Doc_DS.ID
        Else
            SqlTxt = "SELECT  distinct docResoure.doc_id, docResoure.doc_name, docResoure.doc_num, docResoure.doc_type, docResoure.doc_version FROM  appLog ,  docResoure  where appLog.target_id = CAST(docResoure.doc_id AS nvarchar(50)) and docResoure.doc_type='" + DDL_Type.SelectedValue + "'  and docResoure.issue_date is not null and latest='YES'   and docResoure.dp='" + Hidden_DP.Value + "'"
            'Response.Write("SELECT  distinct docResoure.doc_id, docResoure.doc_name, docResoure.doc_num, docResoure.doc_type, docResoure.doc_version FROM  appLog ,  docResoure  where appLog.target_id = docResoure.doc_id and doc_type='" + DDL_Type.SelectedValue + "'  and docResoure.issue_date is not null")
            If Keyword_txt.Text <> "" Then
                SqlTxt = SqlTxt + " and (doc_capsule Like  '%" + Keyword_txt.Text + "%'  or doc_num  Like '%" + Keyword_txt.Text + "%' or doc_version Like '%" + Keyword_txt.Text + "%')"
            End If
            Doc_DS.SelectCommand = SqlTxt
            GridView1.DataSourceID = Doc_DS.ID
        End If
    End Sub


    Protected Sub Result_DS1()
        SQLDS1.ConnectionString = Common.ConnDBS.ConnectionString
        SQLDS1.SelectCommand = "SELECT  distinct ismsuser.user_id FROM  appLog, ismsUser, docResoure where applog.user_id=ismsuser.user_id and applog.target_id='" + Hidden_DocId.Value + "'  and docResoure.doc_type='" + DDL_Type.SelectedValue + "' and docResoure.issue_date is not null and latest='YES' and docResoure.dp='" + Hidden_DP.Value + "'"
        'Response.Write("SELECT  distinct ismsuser.user_id FROM  appLog, ismsUser, docResoure where applog.user_id=ismsuser.user_id and applog.target_id='" + Hidden_DocId.Value + "'  and docResoure.doc_type='" + DDL_Type.SelectedValue + "' and docResoure.issue_date is not null and latest='YES'")
        GridView2.DataSourceID = SQLDS1.ID

        SQLDS2.ConnectionString = Common.ConnDBS.ConnectionString
        SQLDS2.SelectCommand = "SELECT  distinct ismsuser.user_id FROM  appLog, ismsUser, docResoure where applog.user_id <> ismsuser.user_id and applog.target_id='" + Hidden_DocId.Value + "' and  docResoure.doc_type='" + DDL_Type.SelectedValue + "' and docResoure.issue_date is not null and latest='YES' and docResoure.dp='" + Hidden_DP.Value + "'"
        'Response.Write("SELECT  distinct ismsuser.user_id FROM  appLog, ismsUser, docResoure where applog.user_id <> ismsuser.user_id and applog.target_id='" + Hidden_DocId.Value + "' and  docResoure.doc_type='" + DDL_Type.SelectedValue + "' and docResoure.issue_date is not null and latest='YES'")
        GridView3.DataSourceID = SQLDS2.ID

    End Sub

    Protected Sub QueryBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles QueryBtn.Click

        If DDL_Type.SelectedValue = "UserType" Then
            Doc_SqlDS()
            TB_User.Visible = True
            Result_TB1.Visible = False
            Result_TB2.Visible = False
            Doc_TB.Visible = False
        Else
            Doc_SqlDS()
            Doc_TB.Visible = True
            LB_List.Text = Common.ISMS_DocNameChange(DDL_Type.SelectedValue)
            Result_TB1.Visible = False
            Result_TB2.Visible = False
            TB_User.Visible = False
        End If


    End Sub

    Protected Sub UserDown_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        Hidden_DocId.Value = GridView1.Rows(Index).Cells(4).Text
        Label_txt.Text = Common.ISMS_DocNameChange(DDL_Type.SelectedValue)
        Result_TB1.Visible = True
        Result_DS1()

    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(4).Visible = False
    End Sub


    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound
        If e.Row.RowType <> DataControlRowType.Header Then
            e.Row.Cells(0).Text = Common.Get_User_Name(e.Row.Cells(0).Text)
        End If
    End Sub

    Protected Sub GridView3_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView3.RowDataBound
        If e.Row.RowType <> DataControlRowType.Header Then
            e.Row.Cells(0).Text = Common.Get_User_Name(e.Row.Cells(0).Text)
        End If
    End Sub

    Protected Sub User_DS()
        SQLDS3.ConnectionString = Common.ConnDBS.ConnectionString
        SQLDS3.SelectCommand = "Select * from docResoure where issue_date is not null and latest='YES' and docResoure.dp='" + Hidden_DP.Value + "'"
        GridView5.DataSourceID = SQLDS3.ID
    End Sub
    Protected Sub DocDown_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        Hidden_userId.Value = GridView4.Rows(Index).Cells(0).Text
        User_DS()
        Result_TB2.Visible = True
        user_lb.Text = Hidden_userId.Value
    End Sub

    Protected Sub GridView4_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView4.RowDataBound
        If e.Row.RowType <> DataControlRowType.Header Then
            e.Row.Cells(0).Text = Common.Get_User_Name(e.Row.Cells(0).Text)
        End If

    End Sub

    Protected Sub GridView5_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView5.RowCreated
        e.Row.Cells(5).Visible = False
    End Sub




    Protected Sub GridView5_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView5.RowDataBound

        If e.Row.RowType <> DataControlRowType.Header Then
            e.Row.Cells(0).Text = Common.ISMS_DocNameChange(e.Row.Cells(0).Text)
        End If

        If e.Row.RowIndex >= 0 Then
            Dim Ds As New DataSet

            Ds = Common.Check_DocDownLog(e.Row.Cells(5).Text, Common.Get_Userid(Hidden_userId.Value))

            If Ds.Tables(0).Rows.Count = 0 Then
                CType(e.Row.FindControl("DownRecordLB"), Label).ForeColor = Drawing.Color.Red
                CType(e.Row.FindControl("DownRecordLB"), Label).Text = "未下載"
            Else
                CType(e.Row.FindControl("DownRecordLB"), Label).Text = "已下載"
            End If

        End If

    End Sub
End Class
