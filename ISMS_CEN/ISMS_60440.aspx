﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_60440.aspx.vb" Inherits="ISMS_60440" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>地所系統使用者帳號申請表</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function toLocalSDate() {
            var syear = parseInt(document.getElementById("txb_sdate").value.substr(0, 4)) - 1911;
            document.getElementById("txb_sdate").value = syear + document.getElementById("txb_sdate").value.substr(4, 6);
        }
        function toLocalEDate() {
            var eyear = parseInt(document.getElementById("txb_edate").value.substr(0, 4)) - 1911;
            document.getElementById("txb_edate").value = eyear + document.getElementById("txb_edate").value.substr(4, 6);
        }
   </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
            EnableScriptLocalization="True">
        </asp:ScriptManager>
        <table border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2" width="850px">
            <tr>
                <td>
                    <img src="Images/exe.gif" />地所系統使用者帳號申請表 | 個人申請清單 列表
                    <asp:Label ID="lb_msg" runat="server" ForeColor="red" EnableViewState="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="tb_result" runat="server" border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2" style="width:100%" visible="false">
                        <tr>
                          <td colspan="4" style="text-align:right">
                                <asp:Button ID="but_update" runat="server" Text="修改儲存" CausesValidation="False"   />
                                <asp:Button ID="but_print" runat="server" Text="列印" CausesValidation="False"   />
                                <asp:Button ID="but_esc" runat="server" Text="回列表" CausesValidation="False" />
                                <asp:HiddenField ID="hidden_office_apply_id" runat="server"  />
                          </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="tb_title_w_1">桃園市<asp:Label ID="lblOffice" runat="server" Text=""></asp:Label>地政事務所資訊系統使用者帳號申請表</td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1" style="width: 110px;">單位</td>
                            <td><asp:DropDownList ID="ddl_deptment" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="tb_title_w_1" style="width: 110px;"> 申請原因</td>
                            <td>
                                <asp:RadioButtonList ID="rdo_apply_Reason" runat="server" RepeatColumns="3" RepeatLayout="Flow">
                                    <%--<asp:ListItem Value="01">年度檢討</asp:ListItem>--%>
                                    <asp:ListItem Value="02">職務異動</asp:ListItem>
                                    <asp:ListItem Value="05">其他</asp:ListItem>
                                    <asp:ListItem Value="04">離職</asp:ListItem>
                                    <asp:ListItem Value="03">新進人員</asp:ListItem>
                               </asp:RadioButtonList>
                                <asp:TextBox ID="txb_apply_Reason_other" Style="text-align: left" Height="19px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">使用者姓名</td>
                            <td><asp:TextBox ID="txb_user_name" runat="server" ></asp:TextBox></td>
                            <td class="tb_title_w_1"> 分機</td>
                            <td><asp:TextBox ID="txb_ext" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">職稱</td>
                            <td colspan="3">
                                <asp:DropDownList ID="ddl_degree" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">業務項目</td>
                            <td colspan="3"> <asp:TextBox ID="txb_item" runat="server" Text=""></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">申請日期</td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddl_apply_year" runat="server" Width="60px" AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddl_apply_day_selectedchange">
                                                    </asp:DropDownList>年
                                                    <asp:DropDownList ID="ddl_apply_month" runat="server" Width="50px" AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddl_apply_day_selectedchange">
                                                    </asp:DropDownList>月
                                                    <asp:DropDownList ID="ddl_apply_day" runat="server" Width="50px">
                                                    </asp:DropDownList>日
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="tb_title_w_1">啟用日期</td>
                            <td>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddl_enable_year" runat="server" Width="60px" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddl_open_day_selectedchange">
                                                </asp:DropDownList>年
                                                <asp:DropDownList ID="ddl_enable_month" runat="server" Width="50px" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddl_open_day_selectedchange">
                                                </asp:DropDownList>月
                                                <asp:DropDownList ID="ddl_enable_day" runat="server" Width="50px">
                                                </asp:DropDownList>日
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">使用期間</td>
                            <td colspan="3">
                                <asp:UpdatePanel ID="upd_time" runat="server">
                                    <ContentTemplate>
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddl_s_year" runat="server" Width="60px" AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddl_s_day_selectedchange">
                                                    </asp:DropDownList>年
                                                    <asp:DropDownList ID="ddl_s_month" runat="server" Width="50px" AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddl_s_day_selectedchange">
                                                    </asp:DropDownList>月
                                                    <asp:DropDownList ID="ddl_s_day" runat="server" Width="50px">
                                                    </asp:DropDownList>日 至<asp:DropDownList ID="ddl_e_year" runat="server" Width="60px"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddl_e_day_selectedchange">
                                                    </asp:DropDownList>年
                                                    <asp:DropDownList ID="ddl_e_month" runat="server" Width="50px" AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddl_e_day_selectedchange">
                                                    </asp:DropDownList>月
                                                    <asp:DropDownList ID="ddl_e_day" runat="server" Width="50px">
                                                    </asp:DropDownList>日
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">使用者代碼</td>
                            <td><asp:TextBox ID="txb_user_id" runat="server" Text="Wang"></asp:TextBox></td>
                            <td class="tb_title_w_1">使用者密碼</td>
                            <td>預設值</td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">保密切結</td>
                            <td colspan="3">本人___________於業務權限範圍內，將確實依本系統使用作業規定，查詢或列印相關資料，並遵守個人資料保護法、智慧財產權、著作權等相關規定，如有不當使用、資料外洩或違法者，應依法負民、刑事責任及行政責任。</td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">
                                註銷(停用)日期</td>
                            <td>
                                <asp:TextBox ID="txb_disable_date" Style="text-align: left" Height="19px" runat="server"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txb_disable_date" OnClientDateSelectionChanged="toLocalDate"
                                    Format="yyyy/MM/dd">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="tb_title_w_1">
                                註銷(停用)原因</td>
                            <td><asp:TextBox ID="txb_disable_reason" runat="server" Text=""></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="4" class="tb_title_w_1">填表說明</td>
                        </tr>
                        <tr>
                            <td colspan="4" style="height: 109px">
                                <asp:Label ID="note_Edit" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="tb_title_w_1">
                                附表：系統授權範圍</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2" style="width: 100%">
                                    <tr>
                                        <td class="tb_title_w_1" style="width: 30%;">
                                            授權範圍</td>
                                        <td class="tb_title_w_1" style="width: 40%;">
                                            權限</td>
                                        <td class="tb_title_w_1" style="width: 30%;">
                                            備註</td>
                                    </tr>
                                    <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_OnItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbl_sys_name" Enabled="false" Text='<%#Eval("sys_name") %>' runat="server"></asp:Label>
                                                    <asp:HiddenField ID="hidden_sysid" runat="server" Value='<%#Eval("sys_id") %>' />
                                                    <asp:HiddenField ID="hidden_idtname" runat="server" />
                                                </td>
                                                <td>
                                                    <!-- Repeated data -->
                                                    <asp:Repeater ID="ChildRepeater" runat="server">
                                                        <ItemTemplate>
                                                            <!-- Nested repeated data -->
                                                            <asp:CheckBox ID="chk_idt_name" runat="server" Text='<%#Eval("idt_name") %>' />
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="sys_rmk" Text='<%#Eval("sys_rmk") %>' runat="server" style="width:100%"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                       <tr>
          <td colspan="4" align="center" ><%--<uc1:FlowControl ID="FlowControl1" runat="server"  />--%>
                    <asp:UpdatePanel ID ="UpdatePanel3" runat="server" >
            <ContentTemplate> 
                <asp:Label ID="lb_recordid"  runat="server" Visible="false" Text="0"></asp:Label>
              <asp:Label ID="lb_id"  runat="server" Visible="false" Text="99"></asp:Label>
              <asp:Label ID="lb_name"  runat="server" Visible="false" Text="99"></asp:Label>
         <asp:GridView ID="GridView" runat="server" AutoGenerateColumns="False" CellPadding="3"    CssClass="tb_1" Width="100%"  >
                        <Columns>
                           <asp:TemplateField HeaderText="關卡" ItemStyle-CssClass="tb_w_1">
                                 <ItemTemplate>
                                    <asp:Label ID="wf_order" runat="server" Text='<%# eval("wf_order") %>'></asp:Label>
                                 </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField Headertext="審核身份" ItemStyle-CssClass="tb_w_1">
                                  <ItemTemplate>
                                    <asp:Label ID="chk_name" runat="server" Text='<%# eval("chk_name") %>' ></asp:Label>
                                 </ItemTemplate>
                           </asp:TemplateField>                        
                           <asp:TemplateField HeaderText="審核人員" ItemStyle-CssClass="tb_w_1">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddl_user" runat="server" Visible="false"  ></asp:DropDownList>
                                     
                                    <asp:Panel ID="pan_user" runat="server" Visible="false">
                                    <asp:Label ID="lb_username" runat="server"></asp:Label>
                                    (<asp:Label ID="label_userid" runat="server"></asp:Label>)
                                    </asp:Panel>                         
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="狀態" ItemStyle-CssClass="tb_w_1">
                               <ItemTemplate>
                                    <asp:Label ID="lb_status" runat="server" ></asp:Label>
                                    <asp:HiddenField ID="hid_status" runat="server" Value='<%#Eval("wf_status_id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="意見" ItemStyle-CssClass="tb_w_1">
                                <ItemTemplate>
                                    <asp:Label ID="lb_comment" runat="server" Text='<%# Eval("wf_comment")%>' ></asp:Label>                                        
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                       <HeaderStyle CssClass="tb_title_w_1" />
                    </asp:GridView> 
                  <%--  <asp:SqlDataSource ID="WF_DS" runat="server"></asp:SqlDataSource>--%>
                  </ContentTemplate>
          </asp:UpdatePanel>                                     
          </td>
         </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table id="tb_query" runat="server">
                        <tr>
                            <td class="tb_title_w_1">
                                申請日期起迄</td>
                            <td>
                                <asp:TextBox ID="txb_sdate" Style="text-align: left" Height="19px" runat="server"></asp:TextBox>~
                                <asp:TextBox ID="txb_edate" Style="text-align: left" Height="19px" runat="server"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txb_sdate" OnClientDateSelectionChanged="toLocalSDate"
                                    Format="yyyy/MM/dd">
                                </cc1:CalendarExtender>
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txb_edate" OnClientDateSelectionChanged="toLocalEDate"
                                    Format="yyyy/MM/dd">
                                </cc1:CalendarExtender>
                            </td>
                            <td>
                                <asp:Button ID="but_search" runat="server" Text="查詢" /></td>
                        </tr>
                    </table>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
                    <asp:GridView ID="GridView1" runat="server" CssClass="tb_1" AutoGenerateColumns="false"  Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="申請日期">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lb_applydate" Text='<%# eval("apply_date") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="使用者姓名">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lb_userName" Text='<%# eval("user_name") %>'></asp:Label>
                                    <asp:HiddenField ID="hidden_recordId" runat="server" Value='<%#Eval("recordId")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="申請原因">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lb_applyreason" Text='<%# eval("apply_reason") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="管理">
                                <ItemTemplate>
                                    <asp:Button ID="Button1" CommandName="audit_btn" runat="server" Text="查看" CommandArgument='<%#Eval("office_apply_id") %>'/>
                                    <asp:Button ID="Button2" CommandName="using_btn"  runat="server" Text="撤銷" CommandArgument='<%#Eval("office_apply_id") %>' OnClientClick="if ( confirm ('確定撤銷嗎？')==false) return false;" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="tb_title_w_1" />
                        <EmptyDataTemplate>
                            …尚無資料…</EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
