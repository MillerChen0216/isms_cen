﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FireWall.aspx.vb" Inherits="FireWall" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>附件表單維護</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
    <link href="CSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="JS/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="JS/bootstrap.min.js"></script>
    <script type="text/javascript" src="JS/jquery-ui.min.js"></script>
    <script type="text/javascript">
        /<label style="color:red;">*</label><label style="color:red;">*</label>
        <label style="color:red;">*</label> Created by EIJI on 2014/1/3.
        <label style="color:red;">*</label>/

        (function () {
            var yearTextSelector = '.ui-datepicker-year';

            var dateNative = new Date(),
        dateTW = new Date(
            dateNative.getFullYear() - 1911,
            dateNative.getMonth(),
            dateNative.getDate()
        );


            function leftPad(val, length) {
                var str = '' + val;
                while (str.length < length) {
                    str = '0' + str;
                }
                return str;
            }

            // 應該有更好的做法
            var funcColle = {
                onSelect: {
                    basic: function (dateText, inst) {
                        /<label style="color:red;">*</label>
                        var yearNative = inst.selectedYear < 1911
                        ? inst.selectedYear + 1911 : inst.selectedYear;<label style="color:red;">*</label>/
                        dateNative = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);

                        // 年分小於100會被補成19<label style="color:red;">*</label><label style="color:red;">*</label>, 要做例外處理
                        var yearTW = inst.selectedYear > 1911
                    ? leftPad(inst.selectedYear - 1911, 4)
                    : inst.selectedYear;
                        var monthTW = leftPad(inst.selectedMonth + 1, 2);
                        var dayTW = leftPad(inst.selectedDay, 2);
                        console.log(monthTW);
                        dateTW = new Date(
                    yearTW + '-' +
                    monthTW + '-' +
                    dayTW + 'T00:00:00.000Z'
                );
                        console.log(dateTW);
                        return $.datepicker.formatDate(twSettings.dateFormat, dateTW);
                    }
                }
            };

            var twSettings = {
                closeText: '關閉',
                prevText: '上個月',
                nextText: '下個月',
                currentText: '今天',
                monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
            '七月', '八月', '九月', '十月', '十一月', '十二月'],
                monthNamesShort: ['一月', '二月', '三月', '四月', '五月', '六月',
            '七月', '八月', '九月', '十月', '十一月', '十二月'],
                dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
                dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
                dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
                weekHeader: '周',
                dateFormat: 'yy/mm/dd',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: true,
                yearSuffix: '年',

                onSelect: function (dateText, inst) {
                    $(this).val(funcColle.onSelect.basic(dateText, inst));
                    if (typeof funcColle.onSelect.newFunc === 'function') {
                        funcColle.onSelect.newFunc(dateText, inst);
                    }
                }
            };

            // 把yearText換成民國
            var replaceYearText = function () {
                var $yearText = $('.ui-datepicker-year');

                if (twSettings.changeYear !== true) {
                    $yearText.text('民國' + dateTW.getFullYear());
                } else {
                    // 下拉選單
                    if ($yearText.prev('span.datepickerTW-yearPrefix').length === 0) {
                        $yearText.before("<span class='datepickerTW-yearPrefix'>民國</span>");
                    }
                    $yearText.children().each(function () {
                        if (parseInt($(this).text()) > 1911) {
                            $(this).text(parseInt($(this).text()) - 1911);
                        }
                    });
                }
            };

            $.fn.datepickerTW = function (options) {

                // setting on init,
                if (typeof options === 'object') {
                    //onSelect例外處理, 避免覆蓋
                    if (typeof options.onSelect === 'function') {
                        funcColle.onSelect.newFunc = options.onSelect;
                        options.onSelect = twSettings.onSelect;
                    }
                    // year range正規化成西元, 小於1911的數字都會被當成民國年
                    if (options.yearRange) {
                        var temp = options.yearRange.split(':');
                        for (var i = 0; i < temp.length; i += 1) {
                            //民國前處理
                            if (parseInt(temp[i]) < 1) {
                                temp[i] = parseInt(temp[i]) + 1911;
                            } else {
                                temp[i] = parseInt(temp[i]) < 1911
                            ? parseInt(temp[i]) + 1911
                            : temp[i];
                            }
                        }
                        options.yearRange = temp[0] + ':' + temp[1];
                    }
                    // if input val not empty
                    if ($(this).val() !== '') {
                        options.defaultDate = $(this).val();
                    }
                }

                // setting after init
                if (arguments.length > 1) {
                    // 目前還沒想到正常的解法, 先用轉換成init setting obj的形式
                    if (arguments[0] === 'option') {
                        options = {};
                        options[arguments[1]] = arguments[2];
                    }
                }

                // override settings
                $.extend(twSettings, options);

                // init
                $(this).datepicker(twSettings);

                // beforeRender
                $(this).click(function () {
                    var isFirstTime = ($(this).val() === '');

                    // year range and default date

                    if ((twSettings.defaultDate || twSettings.yearRange) && isFirstTime) {

                        if (twSettings.defaultDate) {
                            $(this).datepicker('setDate', twSettings.defaultDate);
                        }

                        // 當有year range時, select初始化設成range的最末年
                        if (twSettings.yearRange) {
                            var $yearSelect = $('.ui-datepicker-year'),
                        nowYear = twSettings.defaultDate
                            ? $(this).datepicker('getDate').getFullYear()
                            : dateNative.getFullYear();

                            $yearSelect.children(':selected').removeAttr('selected');
                            if ($yearSelect.children('[value=' + nowYear + ']').length > 0) {
                                $yearSelect.children('[value=' + nowYear + ']').attr('selected', 'selected');
                            } else {
                                $yearSelect.children().last().attr('selected', 'selected');
                            }
                        }
                    } else {
                        $(this).datepicker('setDate', dateNative);
                    }

                    $(this).val($.datepicker.formatDate(twSettings.dateFormat, dateTW));

                    replaceYearText();

                    if (isFirstTime) {
                        $(this).val('');
                    }
                });

                // afterRender
                $(this).focus(function () {
                    replaceYearText();
                });

                return this;
            };

        })();

        $(function () {
            $('.datepickerTW').datepickerTW({
                changeYear: true,
                changeMonth: true,
                yearRange: ((new Date).getFullYear() - 1) + ":" + ((new Date).getFullYear() + 1),
                //defaultDate: '86-11-01',
                dateFormat: 'yy/mm/dd'
            });

            $('.datepicker').datepicker({
                changeYear: true,
                changeMonth: true,
                dateFormat: 'yy-mm-dd',
                yearRange: '1911:2018',
                defaultDate: '2016-11-01'
            });

        });
    </script>
</head>
<body onload="immediately()">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="True">
    </asp:ScriptManager>
    <table border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2" width="700px">
        <tr>
            <td colspan="4">
                <img src="Images/exe.gif" />防火牆維護申請表 | 填表
                <asp:Label ID="lb_msg" runat="server" ForeColor="red" EnableViewState="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_1">
                申請日期<label style="color:red;">*</label>
            </td>
            <td class="tb_title_w_2" align="center">
                <asp:TextBox ID="applyDate" runat="server" class="datepickerTW"></asp:TextBox>
            </td>
            <td class="tb_title_w_1">
                表單編號
            </td>
            <td class="tb_title_w_2">
                <%--<asp:Label ID="recordId" runat="server" ></asp:Label>--%>
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_1" width="150px" colspan="1">
                申請代表人
            </td>
            <td class="tb_title_w_2" colspan="3">
                <asp:Label ID="empName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_1" width="150px">
                申請單位<label style="color:red;">*</label>
            </td>
            <td class="tb_title_w_2">
                <asp:TextBox ID="applyOrg" runat="server"></asp:TextBox>
            </td>
            <td class="tb_title_w_1" width="150px">
                申請人員<label style="color:red;">*</label>
            </td>
            <td class="tb_title_w_2">
                <asp:TextBox ID="applyName" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_1" width="150px" colspan="1">
                連絡電話<label style="color:red;">*</label>
            </td>
            <td class="tb_title_w_2" colspan="3">
                <asp:TextBox ID="applyTel1" runat="server"></asp:TextBox>
                <asp:TextBox ID="applyTel2" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_1" width="150px" colspan="1">
                預計開放時間
            </td>
            <td class="tb_title_w_2" colspan="3">
                <asp:TextBox ID="startTime" runat="server" class="datepickerTW"></asp:TextBox>
                至
                <asp:TextBox ID="endTime" runat="server" class="datepickerTW"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_1" width="150px" colspan="4">
                申請事由<label style="color:red;">*</label>
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_2" colspan="4">
                <asp:TextBox ID="applyReason" TextMode="MultiLine" runat="server" Width="100%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_1" width="150px">
                來源IP<label style="color:red;">*</label>
            </td>
            <td class="tb_title_w_2">
                <asp:TextBox ID="F_Ip" runat="server"></asp:TextBox>
            </td>
            <td class="tb_title_w_1" width="150px">
                來源Port<label style="color:red;">*</label>
            </td>
            <td class="tb_title_w_2">
                <asp:TextBox ID="F_Port" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_1" width="150px">
                目的IP<label style="color:red;">*</label>
            </td>
            <td class="tb_title_w_2">
                <asp:TextBox ID="T_Ip" runat="server"></asp:TextBox>
            </td>
            <td class="tb_title_w_1" width="150px">
                目的Port<label style="color:red;">*</label>
            </td>
            <td class="tb_title_w_2">
                <asp:TextBox ID="T_Port" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tb_title_w_1" width="150px">
                單/雙向連接<label style="color:red;">*</label>
            </td>
            <td class="tb_title_w_2">
                <asp:RadioButtonList ID="Radio_Connected" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Text="單向連接" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="雙向連接" Value="2"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td class="tb_title_w_1" width="150px">
                附件檔案
            </td>
            <td class="tb_title_w_2">
                <asp:FileUpload ID="File_applyFile" runat="server" Width="70%" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <%--<uc1:FlowControl ID="FlowControl1" runat="server"  />--%>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="GridView" runat="server" AutoGenerateColumns="False" CellPadding="3"
                            CssClass="tb_1" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="關卡" ItemStyle-CssClass="tb_w_1">
                                    <ItemTemplate>
                                        <asp:Label ID="wf_order" runat="server" Text='<%# eval("wf_order") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="審核身份" ItemStyle-CssClass="tb_w_1">
                                    <ItemTemplate>
                                        <asp:Label ID="chk_name" runat="server" Text='<%# eval("chk_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="審核人員" ItemStyle-CssClass="tb_w_1">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_user" runat="server" Visible="false">
                                        </asp:DropDownList>
                                        <asp:Label ID="lb_wf_inside_order" runat="server" Text='<%# eval("wf_inside_order") %>'
                                            Visible="false"></asp:Label>
                                        <asp:Panel ID="pan_user" runat="server" Visible="false">
                                            <asp:Label ID="lb_username" runat="server"></asp:Label>
                                            (<asp:Label ID="label_userid" runat="server"></asp:Label>)
                                        </asp:Panel>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="狀態" ItemStyle-CssClass="tb_w_1">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_status" runat="server" Visible="False">
                                        </asp:DropDownList>
                                        <asp:Label ID="lb_status" runat="server" Visible="False"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="意見" ItemStyle-CssClass="tb_w_1">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txb_comment" runat="server" Visible="False" Width="150px"></asp:TextBox>
                                        <asp:Label ID="lb_comment" runat="server" Visible="False"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="tb_title_w_1" />
                        </asp:GridView>
                        <%--  <asp:SqlDataSource ID="WF_DS" runat="server"></asp:SqlDataSource>--%>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:Button ID="Button1" runat="server" Text="送出審核" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
