﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AchieveDoc.aspx.vb" Inherits="AchieveDoc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>成果檔案管理</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript">

</script>
<body>
    <form id="form1" runat="server">
    <div>
        <br />
        <table id="MainTable" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 500px" visible="true">
            <tr>
                <td class="tb_title_w_2">
                    <img src="Images/Icon1.gif" />
                    成果檔案管理
                    <asp:Button ID="Btn_FileEdit" runat="server" CssClass="button-small" Text="文件名稱維護" />
                    <asp:Button ID="Btn_goUpload" runat="server" CssClass="button-small" Text="檔案上傳" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="MainGridView" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="500px">
                        <Columns>
                            <asp:BoundField DataField="Num" HeaderText="序">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="acdm_Name" HeaderText="文件名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="acds_time" HeaderText="更新日期">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            
                            <asp:TemplateField HeaderText="操作">
                            <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                                <ItemTemplate>
                                    <asp:Button ID="lkb_address" runat="server" Text="下載" OnClick ="MainDownLoad" CommandArgument='<%# eval("acds_images1") %>'></asp:Button>
                                    <asp:Button ID="MainBtn_goLibary" runat="server" Text="歷程" OnClick="goLibary" CommandArgument='<%# eval("acdm_Id") %>'  />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="MainSource" runat="server"></asp:SqlDataSource>
                </td>
            </tr>
        </table>
        <table id="MainEditTable" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 500px" visible="true">
            <tr>
                <th colspan="2" class="tb_title_w_2" align="center">
                    <img src="Images/Icon1.gif" />
                    文件名稱維護
                </th>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    操作
                </td>
                <td class="tb_w_1" align="center">
                    <asp:RadioButtonList ID="MainEditType" runat="server" RepeatDirection="Horizontal"
                        OnSelectedIndexChanged="MainEditType_selectedchange" AutoPostBack="True">
                        <asp:ListItem Text="新增" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="修改" Value="1"></asp:ListItem>
                        <asp:ListItem Text="刪除" Value="2"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr runat="server" id="otr">
                <td class="tb_title_w_1" width="150px">
                    文件名稱
                </td>
                <td class="tb_w_1" align="left">
                    <asp:DropDownList ID="MainEditDdl_FileName" runat="server" Width="200px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr runat="server" id="ntr">
                <td class="tb_title_w_1" width="150px">
                    新文件名稱*
                </td>
                <td class="tb_w_1" align="left">
                    <asp:TextBox ID="MainEditText_FileName" runat="server" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="MainEditBtn_Save" runat="server" CssClass="button-small" Text="儲存" />
                    <asp:Button ID="MainEditBtn" runat="server" CssClass="button-small" Text="確定" />
                    <asp:Button ID="MainEditBtn_Cancel" runat="server" CssClass="button-small" Text="取消" />
                </td>
            </tr>
        </table>
        <table id="UploadTable" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 500px" visible="true">
            <tr>
                <th colspan="2" class="tb_title_w_2" align="center">
                    <img src="Images/Icon1.gif" />
                    檔案上傳
                </th>
            </tr>
            <tr>
                <td class="tb_title_w_1" width="150px">
                    文件名稱*
                </td>
                <td class="tb_w_1" align="left">
                    <asp:DropDownList ID="UploadDdl_FileName" runat="server" Width="200px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    更新人員
                </td>
                <td class="tb_w_1" align="left">
                    <asp:Label ID="UserName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    更新日期
                </td>
                <td class="tb_w_1" align="left">
                    <asp:Label ID="DateDay" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    更新說明
                </td>
                <td class="tb_w_1" align="left">
                    <asp:TextBox ID="UploadText_FileComment" runat="server" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    附件檔案*
                </td>
                <td class="tb_w_1" align="left">
                    <asp:FileUpload ID="acds_images1" runat="server" Width="70%" /> <br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="UploadBtn_Save" runat="server" CssClass="button-small" Text="儲存" />
                    <asp:Button ID="UploadBtn_Cancel" runat="server" CssClass="button-small" Text="取消" />
                </td>
            </tr>
        </table>
        <table id="LibaryTable" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 500px" visible="true">
            <tr>
                <th class="tb_title_w_2">
                    <img src="Images/Icon1.gif" />
                    歷程-資產清冊
                    <asp:HiddenField ID="Hidden_acds_acdmId" runat="server" />
                </th>
                <th class="tb_title_w_1">
                <asp:Button ID="LibaryBtn_Back" runat="server" CssClass="button-small" Text="返回清單" />
                    年度
                    <asp:DropDownList ID="LibaryDdl_Year" runat="server" OnSelectedIndexChanged="LibaryDdl_Year_selectedchange"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </th>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="LibaryGridView" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="500px">
                        <Columns>
                            <asp:BoundField DataField="acds_Id" HeaderText="acds_Id">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="acds_images1" HeaderText="acds_images1" >
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Num" HeaderText="序">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="acds_userName" HeaderText="更新人員">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="acds_time" HeaderText="更新日期">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="acds_comment" HeaderText="更新說明">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                             <asp:ButtonField ButtonType="Button" CommandName="LibaryBtn_DownLoad" Text="下載">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:ButtonField>
                            <asp:ButtonField ButtonType="Button" CommandName="LibaryBtn_Delete" Text="刪除">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:ButtonField>
                           <%--<asp:TemplateField HeaderText="操作">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                                <ItemTemplate>
                                    <asp:Button ID="LibaryBtn_DownLoad" runat="server" CssClass="button-small" Text="下載" />
                                    <asp:Button ID="LibaryBtn_Delete" runat="server" CssClass="button-small" Text="刪除" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="LibarySource" runat="server"></asp:SqlDataSource>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
