﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_31110.aspx.vb" Inherits="ISMS_31110" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <div>
                <br />
                        <asp:HiddenField ID="Hidden_DP" runat="server" />

                <table id="Show_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 488px"
                    visible="true">
                    <tr>
                        <td class="tb_title_w_2" colspan="5" rowspan="1" style="width: 482px; height: 28px">
                            <img src="Images/exe.GIF" />
                            脆弱點敘述項目管理|列表
                            <asp:DropDownList ID="ddl_Asset" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:Button ID="Show_Insert" runat="server" CssClass="button-small" Text="新增資料" />&nbsp;</td>
                    </tr>
                    <tr>
                        <td rowspan="1" style="width: 501px">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
                                CellPadding="0" Width="500px">
                                <Columns>
                                    <asp:BoundField DataField="asset_id" HeaderText="資產類別">
                                        <HeaderStyle CssClass="tb_title_w_1" />
                                        <ItemStyle CssClass="tb_w_1" Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="weakness_id" HeaderText="項次">
                                        <HeaderStyle CssClass="tb_title_w_1" />
                                        <ItemStyle CssClass="tb_w_1" Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="weakness" HeaderText="脆弱點敘述">
                                        <HeaderStyle CssClass="tb_title_w_1" />
                                        <ItemStyle CssClass="tb_w_1" Width="65%" />
                                    </asp:BoundField>
                                    <asp:ButtonField ButtonType="Button" CommandName="Edit_Btn" Text="編輯">
                                        <HeaderStyle CssClass="tb_title_w_1" />
                                        <ItemStyle CssClass="tb_w_1" Width="10%" />
                                    </asp:ButtonField>
                                    <asp:ButtonField ButtonType="Button" CommandName="Del_Btn" Text="刪除">
                                        <HeaderStyle CssClass="tb_title_w_1" />
                                        <ItemStyle CssClass="tb_w_1" Width="10%" />
                                    </asp:ButtonField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <br />
                <table id="Edit_TB" runat="server" class="tb_1" style="width: 672px" visible="false" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="tb_title_w_2" colspan="6" >
                            <img src="Images/exe.GIF" />
                            脆弱點敘述項目管理&nbsp; | 更新資料 &nbsp;<asp:Button ID="Update_Btn" runat="server" CssClass="button-small"
                                Text="更新資料" /></td>
                    </tr>
                    <tr>
                        <td class="tb_title_w_1" >
                            資產類別</td>
                        <td class="tb_title_w_1" >
                            <span>脆弱點敘述</span></td>
                    </tr>
                    <tr >
                        <td class="tb_w_1" >
                            <asp:DropDownList ID="ddl_Asset_upd" runat="server">
                            </asp:DropDownList></td>
                        <td class="tb_w_1" >
                            <span>
                                <asp:TextBox ID="weakness_upd" runat="server" TextMode="MultiLine" Width="552px" MaxLength="512"></asp:TextBox></span></td>
                    </tr>
                </table>
            </div>
        </div>
        <table id="Ins_TB" runat="server" class="tb_1" style="width: 672px" visible="false" cellpadding="0" cellspacing="0">
            <tr>
                <td class="tb_title_w_2" colspan="6" >
                    <img src="Images/exe.GIF" />
                    脆弱點敘述項目管理&nbsp; | 新增資料 &nbsp;<asp:Button ID="Insert_Btn" runat="server" CssClass="button-small"
                        Text="新增資料" /></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" >
                    資產類別</td>
                <td class="tb_title_w_1" >
                    <span>脆弱點敘述</span></td>
            </tr>
            <tr >
                <td class="tb_w_1" >
                    <asp:DropDownList ID="ddl_Asset_ins" runat="server">
                    </asp:DropDownList></td>
                <td class="tb_w_1" >
                    <span>
                        <asp:TextBox ID="weakness_ins" runat="server" TextMode="MultiLine" Width="552px" MaxLength="512"></asp:TextBox></span></td>
            </tr>
        </table>
    
    </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <asp:HiddenField ID="Hidden_sn" runat="server" />
    </form>
</body>
</html>
