﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_10150.aspx.vb" Inherits="ISMS_10150" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <asp:HiddenField ID="Hidden_DP" runat="server" />
            <br />
            <table id="TB1" cellpadding="0" cellspacing="1" class="tb_1" style="width: 650px" runat="server">
                <tr>
                    <td class="tb_title_w_2" colspan="5">
                        <img src="Images/exe.gif" alt="" />
                        修訂紀錄表 | 列表&nbsp;</td>
                </tr>
                <tr>
                    <td class="tb_title_w_1">
                        文件類別</td>
                    <td class="tb_title_w_1">
                        文件名稱</td>
                </tr>
                <tr >
                    <td class="tb_w_1">
                        <asp:DropDownList ID="DDL_DocType" runat="server">
                        </asp:DropDownList></td>
                    <td class="tb_w_1">
                        <asp:TextBox ID="Doc_name_txt" runat="server"></asp:TextBox></td>
                </tr>
                <tr >
                    <td class="tb_w_1" colspan="3">
                        <asp:Button ID="Query_Btn" runat="server" CssClass="button-small" Text="查詢紀錄" /></td>
                </tr>
            </table>
            <br />
        </div>
    
    </div>
                 
                 
                 <asp:GridView ID="GridView1" runat="server" CellPadding="3"
                    CssClass="tb_1" Width="650px" AutoGenerateColumns="False" Visible="False">
                    <Columns>
                        <asp:BoundField DataField="doc_type" HeaderText="文件類別">
                            <HeaderStyle CssClass="tb_title_w_1" />
                            <ItemStyle CssClass="tb_w_1" />
                        </asp:BoundField>
                        <asp:BoundField DataField="doc_name" HeaderText="文件名稱">
                            <HeaderStyle CssClass="tb_title_w_1" />
                            <ItemStyle CssClass="tb_w_1" />
                        </asp:BoundField>
                        <asp:BoundField DataField="doc_num" HeaderText="文件編號">
                            <HeaderStyle CssClass="tb_title_w_1" />
                            <ItemStyle CssClass="tb_w_1" />
                        </asp:BoundField>
                        <asp:BoundField DataField="doc_version" HeaderText="文件版別">
                            <HeaderStyle CssClass="tb_title_w_1" />
                            <ItemStyle CssClass="tb_w_1" />
                        </asp:BoundField>
                        <asp:BoundField DataField="issue_date" HeaderText="日期">
                            <HeaderStyle CssClass="tb_title_w_1" />
                            <ItemStyle CssClass="tb_w_1" />
                        </asp:BoundField>
                        <asp:BoundField DataField="doc_upddesc" HeaderText="修訂內容" >
                            <HeaderStyle CssClass="tb_title_w_1" />
                            <ItemStyle CssClass="tb_w_1" />
                        </asp:BoundField>
                        <asp:BoundField DataField="creator" HeaderText="增修人" >
                         <HeaderStyle CssClass="tb_title_w_1" />
                          <ItemStyle CssClass="tb_w_1" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            <asp:SqlDataSource ID="Doc_DS" runat="server"></asp:SqlDataSource>
    </form>
</body>
</html>
