﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_31171.aspx.vb" Inherits="ISMS_31171" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <div>
                <div>
                    <br />
                            <asp:HiddenField ID="Hidden_DP" runat="server" />

                    <table id="Show_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 750px"
                        visible="true">
                        <tr>
                            <td class="tb_title_w_2" colspan="5" rowspan="1" >
                                <img src="Images/exe.GIF" />
                                威脅弱點評估表|列表
                                <asp:DropDownList ID="ddl_Asset" runat="server" AutoPostBack="True" >
                                    <asp:ListItem Value="0">全部</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddl_assgroup" runat="server" AutoPostBack="True" >
                                    <asp:ListItem Value="0" Selected="True">全部</asp:ListItem>
                                </asp:DropDownList>
                                &nbsp;<asp:Button ID="Rpt_Btn" runat="server" CssClass="normal_font" Text="報表查詢" /></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="height: 18px" >
                                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
                                    Width="800px">
                                    <LocalReport ReportPath="RPT\31171.rdlc">
                                        <DataSources>
                                            <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet2_DataTable1" />
                                        </DataSources>
                                    </LocalReport>
                                </rsweb:ReportViewer>
                                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
                                    TypeName="DataSet2TableAdapters.DataTable1TableAdapter">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="ddl_Asset" DefaultValue="0" Name="asset_id" PropertyName="SelectedValue" />
                                        <asp:ControlParameter ControlID="ddl_assgroup" DefaultValue="" Name="assgroup_id"
                                            PropertyName="SelectedValue" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                                &nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    
    </div>
        &nbsp;
    </form>
</body>
</html>
