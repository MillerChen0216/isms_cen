﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class ISMS_40130
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim i As Integer


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        If Page.IsPostBack = False Then
            SetFields()
        End If

    End Sub
    Protected Sub Flow_SqlDS()

        Flow_DS.ConnectionString = Common.ConnDBS.ConnectionString
        Flow_DS.SelectCommand = "SELECT * FROM docResoure where (doc_status='REV_NEW' or doc_status='REV_VERIFY' or doc_status ='NEW' or doc_status='VERIFY' or doc_status='DEL_NEW' or doc_status='DEL_VERIFY') AND dp='" + Hidden_DP.Value + "'"
        GridView2.DataSourceID = Flow_DS.ID

    End Sub


    Protected Sub SqlDS()

        Flow_Ds1.ConnectionString = Common.ConnDBS.ConnectionString
        'Flow_Ds1.SelectCommand = "SELECT * FROM riskEvaluateMaster  WHERE dp='" + Hidden_DP.Value + "' order by add_date desc"
        Flow_Ds1.SelectCommand = "SELECT a.dp,a.form_id,a.asset_id,a.riskEvtab_name,a.form_status,a.add_user,a.add_date,a.Issue_date,a.apprv_id,b.asset_name as asset_type FROM ThreatWeaknessEvaluateMaster a,asset b  WHERE a.asset_id = b.asset_id AND dp='" + Hidden_DP.Value + "' order by add_date desc"
        GridView1.DataSourceID = Flow_Ds1.ID
    End Sub




    Protected Sub GridView2_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowCreated
        e.Row.Cells(5).Visible = False
    End Sub

    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound


        Dim oDataRow As Data.DataRowView
        If e.Row.RowIndex <> -1 Then

            oDataRow = CType(e.Row.DataItem, Data.DataRowView)

            Dim Ds, Ds1, Ds2, Ds3 As New DataSet
            Ds = Common.Get_wfFormRecord(e.Row.Cells(0).Text, e.Row.Cells(5).Text, "")
            Ds1 = Common.Get_Wf_ApprvUser(Hidden_DP.Value, e.Row.Cells(0).Text, "")
            Ds2 = Common.Get_Doc_info(e.Row.Cells(5).Text)


            e.Row.Cells(0).Text = Common.ISMS_DocNameChange(e.Row.Cells(0).Text)




        End If
    End Sub



    Protected Sub GridView4_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView4.RowCreated
        e.Row.Cells(5).Visible = False
    End Sub

    Protected Sub GridView4_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView4.RowDataBound

        Dim ds, ds1, ds2, ds3, ds4, ds5, ds6 As New DataSet
        Dim doc_status, wf_status, wf_user, wf_count, wf_order, wf_user_order, creator, del_creator As String



        If e.Row.RowIndex <> -1 Then

            ds = Common.Get_wfFormRecord(Hidden_apprv_id.Value, Hidden_Docid.Value, "")
            ds1 = Common.Get_Doc_info(Hidden_Docid.Value)
            ds2 = Common.Get_Wf_ApprvUser(Hidden_DP.Value, Hidden_apprv_id.Value, e.Row.Cells(0).Text)
            ds3 = Common.Get_wfFormRecord(Hidden_apprv_id.Value, Hidden_Docid.Value, "ASC")
            ds5 = Common.Get_wfRecord(Hidden_apprv_id.Value, Hidden_Docid.Value, HiddneRecord_id.Value)
            ds6 = Common.Get_wfInside_Record(Hidden_apprv_id.Value, Hidden_Docid.Value, HiddneRecord_id.Value, e.Row.Cells(0).Text)

            If e.Row.RowIndex >= 1 Then

                ds4 = Common.Get_Wf_User(Hidden_DP.Value, Hidden_apprv_id.Value, e.Row.Cells(0).Text, e.Row.Cells(5).Text)
                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds4.Tables(0).Rows(0).Item("wf_userid"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds4.Tables(0).Rows(0).Item("wf_userid")

            ElseIf e.Row.RowIndex = 0 And (Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "NEW" Or Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "VERIFY" Or Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "REV_NEW" Or Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "REV_VERIFY") Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("creator"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("creator")

            ElseIf e.Row.RowIndex = 0 And Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "DEL_NEW" Or Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status")) = "DEL_VERIFY" Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("del_creator"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("del_creator")

            End If


            Try
                wf_status = Common.FixNull(ds.Tables(0).Rows(0).Item("wf_status_id"))
                wf_order = Common.FixNull(ds.Tables(0).Rows(0).Item("wf_order"))

            Catch ex As Exception


            End Try
            doc_status = Common.FixNull(ds1.Tables(0).Rows(0).Item("doc_status"))
            wf_user_order = Common.FixNull(ds2.Tables(0).Rows(0).Item("wf_order"))
            wf_user = Common.FixNull(CType(e.Row.FindControl("label_userid"), Label).Text)
            wf_count = Common.FixNull(ds.Tables(0).Rows.Count)
            creator = Common.FixNull(ds1.Tables(0).Rows(0).Item("creator"))
            del_creator = Common.FixNull(ds1.Tables(0).Rows(0).Item("del_creator"))
            Hidden_Status.Value = doc_status


            If wf_count = 0 And wf_user_order = 0 And (doc_status = "NEW" Or doc_status = "REV_NEW") Then

                CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True

            ElseIf wf_count >= 1 Then

                If wf_user_order = 0 And doc_status = "DEL_NEW" Then

                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True

                ElseIf (doc_status = "VERIFY" Or doc_status = "REV_VERIFY" Or doc_status = "DEL_VERIFY") And (ds.Tables(0).Rows(0).Item("wf_status_id") = "2" Or ds.Tables(0).Rows(0).Item("wf_status_id") = "0") And wf_count >= 1 And e.Row.RowIndex >= 1 And wf_user_order = ds.Tables(0).Rows(0).Item("wf_order") + 1 And wf_user = User.Identity.Name.ToString Then

                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True

                Else
                    CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = False

                End If

            Else
                CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = False


            End If


            '填上已簽核過的狀態與意見
            If ds6.Tables(0).Rows.Count >= 1 Then
                If ds6.Tables(0).Rows(0).Item("wf_inside_order") = e.Row.Cells(5).Text And e.Row.RowIndex >= 0 And e.Row.RowIndex <= ds5.Tables(0).Rows.Count Then
                    CType(e.Row.FindControl("lb_status"), Label).Visible = True
                    CType(e.Row.FindControl("comment_lb"), Label).Visible = True
                    CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(ds6.Tables(0).Rows(0).Item("wf_status_id")))
                    CType(e.Row.FindControl("comment_lb"), Label).Text = Common.FixNull(ds6.Tables(0).Rows(0).Item("wf_comment"))
                Else
                    CType(e.Row.FindControl("lb_status"), Label).Visible = False
                    CType(e.Row.FindControl("comment_lb"), Label).Visible = False
                End If

            End If


            '狀態選單
            If e.Row.RowIndex >= 1 Then
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(1)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
            Else
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(0)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
            End If



        End If



    End Sub


    Protected Sub WF_SqlDS()

        WF_DS.ConnectionString = Common.ConnDBS.ConnectionString
        WF_DS.SelectCommand = "SELECT * FROM workflowName where dp='" + Hidden_DP.Value + "' AND apprv_id='" + Hidden_apprv_id.Value + "' order by wf_order, wf_inside_order"
        GridView4.DataSourceID = WF_DS.ID

    End Sub


    Protected Sub flow_ddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles flow_ddl.SelectedIndexChanged
        If flow_ddl.SelectedValue = "0" Then
            Risk_tb1.Visible = False
            Risk_tb2.Visible = False
            Risk_tb3.Visible = False
            Doc_Tb1.Visible = False
            Doc_Tb2.Visible = False
            Doc_Tb3.Visible = False
        ElseIf flow_ddl.SelectedValue = "1" Then
            Flow_SqlDS()
            Risk_tb1.Visible = False
            Risk_tb2.Visible = False
            Risk_tb3.Visible = False
            Doc_Tb1.Visible = True
            Doc_Tb2.Visible = False
            Doc_Tb3.Visible = False
        ElseIf flow_ddl.SelectedValue = "2" Then
            SqlDS()
            Risk_tb1.Visible = True
            Risk_tb2.Visible = False
            Risk_tb3.Visible = False
            Doc_Tb1.Visible = False
            Doc_Tb2.Visible = False
            Doc_Tb3.Visible = False
        End If

    End Sub

    Protected Sub audit_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Doc_Tb1.Visible = False
        Doc_Tb2.Visible = True
        Doc_Tb3.Visible = True
        Dim Ds, Ds1, ds2 As New DataSet
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        Hidden_Docid.Value = GridView2.Rows(Index).Cells(5).Text
        Ds = Common.Get_Doc_info(Hidden_Docid.Value)

        docCat_LB.Text = Common.ISMS_DocNameChange(Ds.Tables(0).Rows(0).Item("doc_type"))

        docNum_LB.Text = Ds.Tables(0).Rows(0).Item("doc_num")
        docVer_LB.Text = Ds.Tables(0).Rows(0).Item("doc_version")
        docName_LB.Text = Ds.Tables(0).Rows(0).Item("doc_name")

        docCreator.Text = Ds.Tables(0).Rows(0).Item("creator")
        docCreatorName.Text = Common.Get_User_Name(Ds.Tables(0).Rows(0).Item("creator"))

        Hidden_apprv_id.Value = Ds.Tables(0).Rows(0).Item("apprv_id")

        Ds1 = Common.Get_wfFormRecord(Hidden_apprv_id.Value, Hidden_Docid.Value, "")
        If Ds1.Tables(0).Rows.Count = 0 Then
            HiddneRecord_id.Value = 1
        ElseIf Ds1.Tables(0).Rows.Count >= 1 And Ds1.Tables(0).Rows(0).Item("wf_status_id") = "9" Then
            HiddneRecord_id.Value = Ds1.Tables(0).Rows(0).Item("record_id") + 1
        ElseIf Ds1.Tables(0).Rows.Count >= 1 And Right(Ds.Tables(0).Rows(0).Item("doc_status"), 3) = "NEW" Then
            HiddneRecord_id.Value = Ds1.Tables(0).Rows(0).Item("record_id") + 1
        Else
            HiddneRecord_id.Value = Ds1.Tables(0).Rows(0).Item("record_id")
        End If


        If Ds.Tables(0).Rows(0).Item("doc_status") = "NEW" Or Ds.Tables(0).Rows(0).Item("doc_status") = "VERIFY" Or Ds.Tables(0).Rows(0).Item("doc_status") = "REJECT" Then
            lb_status.Text = "新版審核"
            docDsc_LB.Text = Ds.Tables(0).Rows(0).Item("doc_capsule")
        ElseIf Left(Ds.Tables(0).Rows(0).Item("doc_status"), 3) = "DEL" Then
            lb_status.Text = "廢止審核"
            docDsc_LB.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("doc_capsule"))
        ElseIf Left(Ds.Tables(0).Rows(0).Item("doc_status"), 3) = "REV" Then
            lb_status.Text = "改版審核"
            docDsc_LB.Text = Ds.Tables(0).Rows(0).Item("doc_upddesc")
        End If

        WF_SqlDS()



    End Sub


    Protected Sub GridView3_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView3.PreRender
        Dim i As Integer = 1
        Dim j As Integer = 0
        Dim k As Integer
        Dim AlternatingRowStyle_i As Integer = 1
        Dim mySingleRow As GridViewRow

        For Each mySingleRow In GridView3.Rows
            'For j = 0 To 13
            For j = 0 To 5
                GridView3.Rows(CInt(mySingleRow.RowIndex)).Cells(j).CssClass = "tb_w_1"
            Next
        Next

        'For Each mySingleRow In GridView3.Rows

        '    If CInt(mySingleRow.RowIndex) = 0 Then

        '        For k = 0 To 8
        '            mySingleRow.Cells(k).RowSpan = 1
        '        Next


        '    Else
        '        If mySingleRow.Cells(0).Text.Trim() = GridView3.Rows(CInt(mySingleRow.RowIndex) - i).Cells(0).Text.Trim() And mySingleRow.Cells(1).Text.Trim() = GridView3.Rows(CInt(mySingleRow.RowIndex) - i).Cells(1).Text.Trim() Then

        '            For k = 0 To 8
        '                GridView3.Rows(CInt(mySingleRow.RowIndex) - i).Cells(k).RowSpan += 1
        '            Next

        '            i = i + 1
        '            For k = 0 To 8
        '                mySingleRow.Cells(k).Visible = False
        '            Next


        '        Else
        '            For k = 0 To 8
        '                GridView3.Rows(CInt(mySingleRow.RowIndex)).Cells(k).RowSpan = 1
        '            Next
        '            i = 1
        '        End If
        '    End If
        'Next
    End Sub

    Protected Sub GridView3_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView3.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim tc As TableCellCollection = e.Row.Cells
            tc.Clear()

            tc.Add(New TableHeaderCell)
            tc(0).Text = "資產編號"
            tc.Add(New TableHeaderCell)
            tc(1).Text = "資產名稱"
            tc.Add(New TableHeaderCell)
            tc(2).Text = "威脅弱點值"
            tc.Add(New TableHeaderCell)
            tc(3).Text = "資產價值"
            tc.Add(New TableHeaderCell)
            tc(4).Text = "綜合風險值"
            tc.Add(New TableHeaderCell)
            tc(5).Text = "群組"
            tc.Add(New TableHeaderCell)
            'tc.Add(New TableHeaderCell)
            'tc(0).Text = "風險類別"
            'tc.Add(New TableHeaderCell)
            'tc(1).Text = "資產編號"
            'tc.Add(New TableHeaderCell)
            'tc(2).Text = "資產名稱"
            'tc.Add(New TableHeaderCell)
            'tc(3).Text = "使用者"
            'tc.Add(New TableHeaderCell)
            'tc(4).Text = "擁有者"
            'tc.Add(New TableHeaderCell)
            'tc(5).Text = "權責單位"
            'tc.Add(New TableHeaderCell)
            'tc(6).Text = "數量"
            'tc.Add(New TableHeaderCell)
            'tc(7).Text = "放置地點"
            'tc.Add(New TableHeaderCell)
            'tc(8).Text = "資產價值"
            'tc.Add(New TableHeaderCell)
            ''tc(9).Text = "資產安全等級"
            ''tc.Add(New TableHeaderCell)
            'tc(9).Text = "威脅"
            'tc.Add(New TableHeaderCell)
            'tc(10).Text = "弱點"
            'tc.Add(New TableHeaderCell)
            'tc(11).Text = "可能性"
            'tc.Add(New TableHeaderCell)
            'tc(12).Text = "衝擊性"
            'tc.Add(New TableHeaderCell)
            ''KM LAND
            'tc(13).Text = "威脅弱點值"
            'KM TAX
            'tc(13).Text = "綜合風險值"  


            'For i = 0 To 13
            For i = 0 To 5
                tc(i).CssClass = "tb_title_w_1"
            Next


        End If
    End Sub

    Protected Sub GridView3_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView3.RowDataBound
        'If e.Row.RowIndex <> -1 Then
        '    Dim Ds, Ds1 As New DataSet
        '    e.Row.Cells(3).Text = Common.Get_User_Name(e.Row.Cells(3).Text)
        '    e.Row.Cells(4).Text = Common.Get_User_Name(e.Row.Cells(4).Text)
        '    e.Row.Cells(5).Text = Common.Get_dept_Name(e.Row.Cells(5).Text)
        '    Ds = Common.Get_AssetName(e.Row.Cells(0).Text)
        '    e.Row.Cells(0).Text = Ds.Tables(0).Rows(0).Item("asset_name")

        'End If
    End Sub


    Protected Sub SetFields()

        Dim risk_type, dept_id, assitem_id, assitem_name, location, item_amount, user_id, holder, twScore, assgroup_name As New BoundField
        Dim assitSecure_lv, assitScore, weakness, threat, riskp_score, impact_score, totalrisk_score, totalrisk_lv As New BoundField

        'dept_id.DataField = "dept_id"
        assitem_id.DataField = "assitem_id"
        assitem_name.DataField = "assitem_name"
        'user_id.DataField = "user_id"
        'holder.DataField = "holder"
        'item_amount.DataField = "item_amount"
        'location.DataField = "location"
        assitScore.DataField = "assitScore"
        'assitSecure_lv.DataField = "assitSecure_lv"
        'weakness.DataField = "weakness"
        'threat.DataField = "threat"
        'riskp_score.DataField = "riskp_score"
        'impact_score.DataField = "impact_score"
        twScore.DataField = "twScore"
        totalrisk_score.DataField = "totalrisk_score"
        assgroup_name.DataField = "assgroup_name"
        'totalrisk_lv.DataField = "totalrisk_lv"
        'risk_type.DataField = "risk_type"

        'GridView3.Columns.Add(risk_type)
        GridView3.Columns.Add(assitem_id)
        GridView3.Columns.Add(assitem_name)
        'GridView3.Columns.Add(user_id)
        'GridView3.Columns.Add(holder)
        'GridView3.Columns.Add(dept_id)
        'GridView3.Columns.Add(item_amount)
        'GridView3.Columns.Add(location)
        GridView3.Columns.Add(assitScore)
        'GridView1.Columns.Add(assitSecure_lv)
        'GridView3.Columns.Add(threat)
        'GridView3.Columns.Add(weakness)
        'GridView3.Columns.Add(riskp_score)
        'GridView3.Columns.Add(impact_score)
        GridView3.Columns.Add(twScore)
        GridView3.Columns.Add(totalrisk_score)
        GridView3.Columns.Add(assgroup_name)
        'GridView1.Columns.Add(totalrisk_lv)
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(4).Visible = False
        e.Row.Cells(5).Visible = False
    End Sub

    Protected Sub GridView5_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView5.RowCreated
        e.Row.Cells(5).Visible = False

    End Sub

    Protected Sub GridView5_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView5.RowDataBound

        Dim ds, ds1, ds2, ds3, ds4, ds5, ds6 As New DataSet
        Dim form_status, wf_status, wf_user, wf_count, wf_order, wf_user_order As String



        If e.Row.RowIndex <> -1 Then

            ds = Common.Get_wfFormRecord(5, Hidden_Formid.Value, "")
            'ds1 = Common.Get_RiskForm_info(Hidden_Formid.Value)
            ds1 = Common.Get_ThreatWeaknessRiskForm_info(Hidden_Formid.Value)
            ds2 = Common.Get_Wf_ApprvUser(Hidden_DP.Value, 5, e.Row.Cells(0).Text)
            ds3 = Common.Get_wfFormRecord(5, Hidden_Formid.Value, "ASC")
            ds5 = Common.Get_wfRecord(5, Hidden_Formid.Value, 1)
            ds6 = Common.Get_wfInside_Record(5, Hidden_Formid.Value, 1, e.Row.Cells(0).Text)

            If e.Row.RowIndex >= 1 Then

                ds4 = Common.Get_Wf_User(Hidden_DP.Value, 5, e.Row.Cells(0).Text, e.Row.Cells(5).Text)
                CType(e.Row.FindControl("lb_username1"), Label).Text = Common.Get_User_Name(ds4.Tables(0).Rows(0).Item("wf_userid"))
                CType(e.Row.FindControl("label_userid1"), Label).Text = ds4.Tables(0).Rows(0).Item("wf_userid")

            ElseIf e.Row.RowIndex = 0 And Common.FixNull(ds1.Tables(0).Rows(0).Item("form_status")) = "RISK_VERIFY" Then

                CType(e.Row.FindControl("lb_username1"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("add_user"))
                CType(e.Row.FindControl("label_userid1"), Label).Text = ds1.Tables(0).Rows(0).Item("add_user")

            Else

                CType(e.Row.FindControl("lb_username1"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("add_user"))
                CType(e.Row.FindControl("label_userid1"), Label).Text = ds1.Tables(0).Rows(0).Item("add_user")


            End If


            Try
                wf_status = Common.FixNull(ds.Tables(0).Rows(0).Item("wf_status_id"))
                wf_order = Common.FixNull(ds.Tables(0).Rows(0).Item("wf_order"))

            Catch ex As Exception


            End Try
            form_status = Common.FixNull(ds1.Tables(0).Rows(0).Item("form_status"))
            wf_user_order = Common.FixNull(ds2.Tables(0).Rows(0).Item("wf_order"))
            wf_user = Common.FixNull(CType(e.Row.FindControl("label_userid1"), Label).Text)
            wf_count = Common.FixNull(ds.Tables(0).Rows.Count)

            If wf_count >= 1 Then

                If wf_count = 1 And wf_user_order = 1 And form_status = "RISK_VERIFY" And wf_user = LCase(LCase(User.Identity.Name.ToString)) Then

                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status1"), DropDownList).Visible = True

                ElseIf form_status = "RISK_VERIFY" And (ds.Tables(0).Rows(0).Item("wf_status_id") = "2" Or ds.Tables(0).Rows(0).Item("wf_status_id") = "0") And wf_count >= 1 And e.Row.RowIndex >= 1 And wf_user_order = ds.Tables(0).Rows(0).Item("wf_order") + 1 And wf_user = LCase(LCase(User.Identity.Name.ToString)) Then

                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                Else
                    CType(e.Row.FindControl("ddl_status1"), DropDownList).Visible = False

                End If

            Else
                CType(e.Row.FindControl("ddl_status1"), DropDownList).Visible = False


            End If

            '填上已簽核過的狀態與意見
            If ds6.Tables(0).Rows.Count >= 1 Then
                If ds6.Tables(0).Rows(0).Item("wf_inside_order") = e.Row.Cells(5).Text And e.Row.RowIndex >= 0 And e.Row.RowIndex <= ds5.Tables(0).Rows.Count Then
                    CType(e.Row.FindControl("lb_status1"), Label).Visible = True
                    CType(e.Row.FindControl("comment_lb1"), Label).Visible = True
                    CType(e.Row.FindControl("lb_status1"), Label).Text = Common.Get_WFStatusName(Common.FixNull(ds6.Tables(0).Rows(0).Item("wf_status_id")))
                    CType(e.Row.FindControl("comment_lb1"), Label).Text = Common.FixNull(ds6.Tables(0).Rows(0).Item("wf_comment"))
                Else
                    CType(e.Row.FindControl("lb_status1"), Label).Visible = False
                    CType(e.Row.FindControl("comment_lb1"), Label).Visible = False
                End If

            End If


            '狀態選單
            If e.Row.RowIndex >= 1 Then
                CType(e.Row.FindControl("ddl_status1"), DropDownList).DataSource = Common.Get_WFStatus(1)
                CType(e.Row.FindControl("ddl_status1"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status1"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status1"), DropDownList).DataBind()
            Else
                CType(e.Row.FindControl("ddl_status1"), DropDownList).DataSource = Common.Get_WFStatus(0)
                CType(e.Row.FindControl("ddl_status1"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status1"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status1"), DropDownList).DataBind()
            End If



        End If

    End Sub



    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound


        Dim oDataRow As Data.DataRowView
        If e.Row.RowIndex <> -1 Then

            oDataRow = CType(e.Row.DataItem, Data.DataRowView)

            Dim Ds, Ds1, Ds2, Ds3 As New DataSet
            Ds = Common.Get_wfFormRecord(5, e.Row.Cells(4).Text, "")
            Ds1 = Common.Get_Wf_ApprvUser(Hidden_DP.Value, 5, "")
            'Ds2 = Common.Get_RiskForm_info(e.Row.Cells(4).Text)
            Ds2 = Common.Get_ThreatWeaknessRiskForm_info(e.Row.Cells(4).Text)

            If Ds.Tables(0).Rows.Count = 0 And Ds2.Tables(0).Rows(0).Item("add_user") = User.Identity.Name.ToString Then
                CType(e.Row.FindControl("audit_btn1"), Button).Visible = True
            End If



            If Ds.Tables(0).Rows.Count >= 1 And Ds2.Tables(0).Rows(0).Item("form_status") = "RISK_VERIFY" Then
                Dim Login_id, Sign_id As String
                Login_id = LCase(User.Identity.Name.ToString)
                Ds3 = Common.Get_Wf_ApprvUser(Hidden_DP.Value, 5, Ds.Tables(0).Rows(0).Item("wf_order") + 1)
                For i = 0 To Ds3.Tables(0).Rows.Count - 1
                    Sign_id = Ds3.Tables(0).Rows(i).Item("wf_userid")
                    'If Login_id = Sign_id Then
                    CType(e.Row.FindControl("audit_btn1"), Button).Visible = True
                    'End If
                Next
            End If


        End If
    End Sub


    Protected Sub audit_btn1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Risk_tb3.Visible = True
        Risk_tb2.Visible = True
        Risk_tb1.Visible = False


        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex


        'Response.Write("SELECT *  FROM riskEvaluateForm where PID=" + GridView1.Rows(Index).Cells(4).Text + "")
        Risk_DS.ConnectionString = Conn.ConnectionString
        'Risk_DS.SelectCommand = "SELECT *  FROM riskEvaluateForm where PID=" + GridView1.Rows(Index).Cells(4).Text
        Risk_DS.SelectCommand = "SELECT *  FROM ThreatWeaknessEvaluateForm where PID=" + GridView1.Rows(Index).Cells(4).Text

        GridView3.DataSourceID = Risk_DS.ID

        WF_DS1.ConnectionString = Common.ConnDBS.ConnectionString
        WF_DS1.SelectCommand = "SELECT * FROM workflowName where apprv_id='5' AND dp='" + Hidden_DP.Value + "' order by wf_order, wf_inside_order"
        GridView5.DataSourceID = WF_DS1.ID

        'SetFields()
        lb_riskname.Text = GridView1.Rows(Index).Cells(1).Text
        Hidden_Formid.Value = GridView1.Rows(Index).Cells(4).Text


    End Sub
End Class
