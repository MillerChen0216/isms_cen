﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_30110.aspx.vb" Inherits="ISMS_30110" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
   
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <br />
                    <asp:HiddenField ID="Hidden_DP" runat="server" />

            <table id="Show_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 600px"
                visible="true">
                <tr>
                    <td class="tb_title_w_2" colspan="5" rowspan="1" style="width: 482px">
                        <img src="Images/exe.gif" />
                        風險評鑑評量標準表 | 列表 
                        <asp:DropDownList ID="ddl_Asset" runat="server" AutoPostBack="True" Width="82px">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddl_infoSecurityType" runat="server" AutoPostBack="True" AppendDataBoundItems="True">
                            <asp:ListItem Selected="True" Value="All">全部</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="Show_Insert" runat="server" CssClass="button-small" Text="新增" />&nbsp;</td>
                </tr>
                <tr>
                    <td rowspan="1">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
                            CellPadding="0" Width="600px">
                            <Columns>
                                <asp:TemplateField HeaderText="評量標準類別">
                                    <EditItemTemplate>
                                        &nbsp;
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        &nbsp;<asp:DropDownList ID="ddl_infotype" runat="server" SelectedValue='<%# Bind ("info_type") %>' Enabled="False">
                                            <asp:ListItem Value="C">機密性</asp:ListItem>
                                            <asp:ListItem Value="I">完整性</asp:ListItem>
                                            <asp:ListItem Value="A">可用性</asp:ListItem>
                                        </asp:DropDownList>&nbsp;
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="tb_title_w_1" />
                                    <ItemStyle CssClass="tb_w_1" Width="20%" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="asset_score" HeaderText="分數">
                                    <HeaderStyle CssClass="tb_title_w_1" />
                                    <ItemStyle CssClass="tb_w_1" Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="asset_desc" HeaderText="內容敍述">
                                    <HeaderStyle CssClass="tb_title_w_1" />
                                    <ItemStyle CssClass="tb_w_1" Width="50%" />
                                </asp:BoundField>
                                <asp:ButtonField ButtonType="Button" CommandName="Edit_Btn" Text="編輯">
                                    <HeaderStyle CssClass="tb_title_w_1" />
                                    <ItemStyle CssClass="tb_w_1" Width="10%" />
                                </asp:ButtonField>
                                <asp:ButtonField ButtonType="Button" CommandName="Del_Btn" Text="刪除">
                                    <HeaderStyle CssClass="tb_title_w_1" />
                                    <ItemStyle CssClass="tb_w_1" Width="10%" />
                                </asp:ButtonField>
                                <asp:BoundField DataField="SN" HeaderText="序號">
                                    <HeaderStyle CssClass="tb_title_w_1" />
                                    <ItemStyle CssClass="tb_w_1" />
                                </asp:BoundField>
                                <asp:BoundField DataField="asset_id" HeaderText="大分類代碼">
                                    <HeaderStyle CssClass="tb_title_w_1" />
                                    <ItemStyle CssClass="tb_w_1" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <br />
            <table id="Edit_TB" class="tb_1" style="width: 650px" runat="server" visible=false cellpadding="0" cellspacing="0">
                <tr>
                    <td class="tb_title_w_2" colspan="6" style="width: 670px"><img src="Images/exe.gif" />
                        風險評鑑評量標準表 | 更新資料 &nbsp;<asp:Button ID="Update_Btn" runat="server" CssClass="button-small"
                            Text="更新資料" /></td>
                </tr>
                <tr>
                    <td class="tb_title_w_1">資產類別*</td>
                    <td class="tb_title_w_1">評量標準類別*</td>
                    <td class="tb_title_w_1" >分數*</td>
                    <td class="tb_title_w_1" >內容敘述</td>
                </tr>
                <tr >
                    <td class="tb_w_1"><asp:DropDownList ID="ddl_asset_upd" runat="server" BackColor="WhiteSmoke" Enabled="False">
                    </asp:DropDownList></td>
                    <td class="tb_w_1" >
                       
                            <asp:DropDownList ID="ddl_infoSecurityType_upd" runat="server">
                            </asp:DropDownList></td>
                    <td class="tb_w_1" >
                       
                            <asp:TextBox ID="asset_score_upd" runat="server" MaxLength="2" Width="35px"></asp:TextBox></td>
                    <td class="tb_w_1">
                       
                            <asp:TextBox ID="asset_desc_upd" runat="server" TextMode="MultiLine"  MaxLength="256" Width="300px"></asp:TextBox></td>
                </tr>
            </table>
        </div>
    
    </div><table id="ins_TB" class="tb_1" style="width: 650px" runat="server" visible=false cellpadding="0" cellspacing="0">
        <tr>
            <td class="tb_title_w_2" colspan="6" style="height: 27px"><img src="Images/exe.gif" />
                風險評鑑評量標準表 | 新增資料 &nbsp;&nbsp;<asp:Button ID="Insert_Btn" runat="server" CssClass="button-small"
                            Text="新增資料" /></td>
        </tr>
        <tr>
            <td class="tb_title_w_1" >
                資產類別*</td>
            <td class="tb_title_w_1" >
                評量標準類別*</td>
            <td class="tb_title_w_1" >
                分數*</td>
            <td class="tb_title_w_1" >
                內容敘述</td>
        </tr>
        <tr >
            <td class="tb_w_1" >
                <asp:DropDownList ID="ddl_asset_ins" runat="server">
                </asp:DropDownList></td>
            <td class="tb_w_1" >
                
                    <asp:DropDownList ID="ddl_infoSecurityType_ins" runat="server">
                    </asp:DropDownList></td>
            <td class="tb_w_1" >
                
                    <asp:TextBox ID="asset_score_ins" runat="server" Width="35px" MaxLength="2"></asp:TextBox></td>
            <td class="tb_w_1" >
               
                    <asp:TextBox ID="asset_desc_ins" runat="server" TextMode="MultiLine" Width="300px" MaxLength="256"></asp:TextBox></td>
        </tr>
    </table>
        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="asset_score_ins"
            ErrorMessage="分數請輸入數值0到99" MaximumValue="99" MinimumValue="0" Display="None"></asp:RangeValidator>&nbsp;
        <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="asset_score_upd"
            ErrorMessage="分數請輸入數值0到99" MaximumValue="99" MinimumValue="0" Display="None"></asp:RangeValidator><br />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <asp:HiddenField ID="Hidden_SN" runat="server" />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RangeValidator1">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RangeValidator2">
        </cc1:ValidatorCalloutExtender>
    </form>
</body>
</html>
