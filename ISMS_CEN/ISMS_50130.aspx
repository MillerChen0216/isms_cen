﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_50130.aspx.vb" Inherits="ISMS_50130" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>

<script type="text/javascript">
function Check(parentChk,ChildId)
{
    var oElements = document.getElementsByTagName("INPUT");
    var bIsChecked = parentChk.checked;

    for(i=0; i<oElements.length;i++)
    {
        if( IsCheckBox(oElements[i]) && 
            IsMatch(oElements[i].id, ChildId))
        {
            oElements[i].checked = bIsChecked;
        }        
    }   
}

function IsMatch(id, ChildId)
{
    var sPattern ='^GridView1.*'+ChildId+'$';
    var oRegExp = new RegExp(sPattern);
    if(oRegExp.exec(id)) 
        return true;
    else 
        return false;
}

function IsCheckBox(chk)
{
    if(chk.type == 'checkbox') return true;
    else return false;
}
</script>



<body>
    <form id="form1" runat="server">
    <div>
        <br />
                            <asp:HiddenField ID="Hidden_DP" runat="server" />

        <table id="Show_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 500px"
            visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="2" >
                    <img src="Images/exe.gif" />   權限管理 </td>
                
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width:200px" > 群組名稱</td>
                <td class="tb_w_1">  <asp:DropDownList ID="ddl_group" runat="server" AutoPostBack="True">  </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="tb_title_w_1"> 子系統名稱</td>
                <td class="tb_w_1">  <asp:DropDownList ID="ddl_subsys" runat="server" AutoPostBack="True" AppendDataBoundItems="true"> 
                    <asp:ListItem Selected="True"  Text="全部" Value=""></asp:ListItem>
                                     </asp:DropDownList></td>
            </tr>
            <tr>
               <td class="tb_w_1"  colspan="2" ><asp:Button ID="SetPower_Btn" runat="server" CssClass="button-small" Text="設定權限" />  </td>
            </tr>
            <tr>
                <td  colspan="2" >
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="550px">
                        <Columns>
                            <asp:BoundField DataField="sText" HeaderText="子系統名稱" SortExpression="sText">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="sValue" HeaderText="程式名稱" SortExpression="sValue">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="appId" HeaderText="編號" SortExpression="appId">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        <asp:TemplateField HeaderText="全選/全不選">
                            <HeaderTemplate>
                                全選<input type="checkbox" id="chkAll" name="chkAll" onclick="Check(this,'chkSelect2')" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect2" runat="server"/>&nbsp;
                                <asp:HiddenField ID="Hidden_cb" runat="server" Value='<%# Eval("cb")%>'    />
                                <asp:HiddenField ID="Hidden_appId" runat="server" Value='<%# Eval("appId") %>' Visible="False" />
                            </ItemTemplate>
                            <HeaderStyle CssClass="tb_title_w_1" />
                            <ItemStyle CssClass="tb_w_1" />
                        </asp:TemplateField>                                                 
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="Power_DS" runat="server"></asp:SqlDataSource>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
