﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_20130
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP

        If Not Page.IsPostBack Then
            ddl_Asset.DataSource = Common.Get_Asset()
            ddl_Asset.DataValueField = "asset_id"
            ddl_Asset.DataTextField = "asset_name"
            ddl_Asset.DataBind()
            SqlDataSource()
        End If
        'SqlDataSource()

        Dim ds As New DataSet
        ds = Common.Get_AssetItem(Hidden_DP.Value)
        If ds.Tables(0).Rows.Count = 0 Then
            Server.Transfer("ISMS_20120.aspx")
        End If

    End Sub

    Protected Sub SqlDataSource()
        If ddl_AssetQryColumn.SelectedValue = "All" Then
            SqlTxt = "SELECT b.asset_name,(select d.dept_name from deptment as d where d.dept_id = a.dept_id and d.dp=a.dp) as dept_name,a.assitem_desc,b.asset_Ename,b.asset_name,a.assitem_id,a.assitem_name,a.item_amount,a.user_id as use_name,a.holder as holder_name,a.location,a.Confidentiality,a.Integrity,a.Availability,a.assitem_desc,(select e.assgroup_name from assetGroup as e where e.asset_id = a.asset_id and e.assgroup_id=a.assgroup_id) as assgroup_name FROM assetItem as a, asset as b WHERE a.dp='" + Hidden_DP.Value + "' and a.asset_id = b.asset_id AND a.evaluate ='Y' AND a.expire='N' AND (a.asset_id=" + ddl_Asset.SelectedValue + " OR " + ddl_Asset.SelectedValue + "=0) ORDER BY a.asset_id, a.assitem_id"
        ElseIf ddl_AssetQryColumn.SelectedValue = "holder" Then

            SqlTxt = "SELECT b.asset_name,(select d.dept_name from deptment as d where d.dept_id = a.dept_id and d.dp=a.dp) as dept_name,a.assitem_desc,b.asset_Ename,b.asset_name,a.assitem_id,a.assitem_name,a.item_amount,a.user_id as use_name,a.holder as holder_name,a.location,a.Confidentiality,a.Integrity,a.Availability,a.assitem_desc,(select e.assgroup_name from assetGroup as e where e.asset_id = a.asset_id and e.assgroup_id=a.assgroup_id) as assgroup_name FROM assetItem as a, asset as b WHERE a.dp='" + Hidden_DP.Value + "' and a.asset_id = b.asset_id AND a.evaluate ='Y' AND a.expire='N' AND (a.asset_id=" + ddl_Asset.SelectedValue + " OR " + ddl_Asset.SelectedValue + "=0) AND a.holder  LIKE '%" + t_querystring.Text + "%' ORDER BY a.asset_id, a.assitem_id"
        Else
            SqlTxt = "SELECT b.asset_name,(select d.dept_name from deptment as d where d.dept_id = a.dept_id and d.dp=a.dp) as dept_name,a.assitem_desc,b.asset_Ename,b.asset_name,a.assitem_id,a.assitem_name,a.item_amount,a.user_id as use_name,a.holder as holder_name,a.location,a.Confidentiality,a.Integrity,a.Availability,a.assitem_desc,e.assgroup_name FROM assetItem as a, asset as b ,assetGroup as e WHERE a.dp='" + Hidden_DP.Value + "' AND a.asset_id = b.asset_id  AND e.asset_id = a.asset_id AND e.assgroup_id=a.assgroup_id AND a.evaluate ='Y' AND a.expire='N' AND (a.asset_id=" + ddl_Asset.SelectedValue + " OR " + ddl_Asset.SelectedValue + "=0) AND " + ddl_AssetQryColumn.SelectedValue + " LIKE '%" + t_querystring.Text + "%' ORDER BY a.asset_id, a.assitem_id"

        End If
        SqlDataSource1.ConnectionString = Conn.ConnectionString
        SqlDataSource1.SelectCommand = SqlTxt
        'Response.Write(SqlTxt)
        GridView1.DataSourceID = SqlDataSource1.ID
        GridView1.DataBind()
        'Dim KeyNames() As String = {"assitem_id"}
        'GridView1.DataKeyNames = KeyNames
    End Sub

    Protected Sub btn_query_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_query.Click
        SqlDataSource()
    End Sub

    Protected Sub btn_Excel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Excel.Click
        Dim ads As New System.Web.UI.WebControls.SqlDataSource
        Dim ExcelSqlTxt As String
        Dim sw As New System.IO.StringWriter()
        Dim htw As New System.Web.UI.HtmlTextWriter(sw)
        Dim dg As New GridView()

        Response.Clear()
        '檔名支援中文
        Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5")
        '避免儲存中文內容有亂碼
        Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8")
        'excel檔名()
        Response.AddHeader("content-disposition", "attachment;filename=" + DateTime.Now.ToString("yyyyMMdd HH:mm") + "_" + ddl_Asset.SelectedItem.Text + "_資產項清冊.xls")
        Response.ContentType = "application/vnd.ms-excel"
        Response.Charset = ""
        If ddl_AssetQryColumn.SelectedValue = "All" Then
            ExcelSqlTxt = "SELECT a.assitem_id as 資產編號,b.asset_name as 資產類別,a.assitem_name as 資產名稱,a.assitem_desc as 資產說明,a.item_amount as 數量,(select d.dept_name from deptment as d where d.dept_id = a.dept_id and d.dp=a.dp) as 權責單位,a.Confidentiality as 機密性,a.Integrity as 完整性,a.Availability as 可用性,(a.Confidentiality+a.Integrity+a.Availability) as 資產價值,(select e.assgroup_name from assetGroup as e where e.asset_id = a.asset_id and e.assgroup_id=a.assgroup_id) as 資產群組 FROM assetItem as a, asset as b WHERE a.dp='" + Hidden_DP.Value + "' and a.asset_id = b.asset_id AND a.evaluate ='Y' AND a.expire='N' AND (a.asset_id=" + ddl_Asset.SelectedValue + " OR " + ddl_Asset.SelectedValue + "=0) ORDER BY a.asset_id, a.assitem_id"
        ElseIf ddl_AssetQryColumn.SelectedValue = "holder" Then
            ExcelSqlTxt = "SELECT a.assitem_id as 資產編號,b.asset_name as 資產類別,a.assitem_name as 資產名稱,a.assitem_desc as 資產說明,a.item_amount as 數量,(select d.dept_name from deptment as d where d.dept_id = a.dept_id and d.dp=a.dp) as 權責單位,a.Confidentiality as 機密性,a.Integrity as 完整性,a.Availability as 可用性,(a.Confidentiality+a.Integrity+a.Availability) as 資產價值,(select e.assgroup_name from assetGroup as e where e.asset_id = a.asset_id and e.assgroup_id=a.assgroup_id) as 資產群組 FROM assetItem as a, asset as b WHERE a.dp='" + Hidden_DP.Value + "' and a.asset_id = b.asset_id AND a.evaluate ='Y' AND a.expire='N' AND (a.asset_id=" + ddl_Asset.SelectedValue + " OR " + ddl_Asset.SelectedValue + "=0) AND a.holder LIKE '%" + t_querystring.Text + "%' ORDER BY a.asset_id, a.assitem_id"
        Else
            ExcelSqlTxt = "SELECT a.assitem_id as 資產編號,b.asset_name as 資產類別,a.assitem_name as 資產名稱,a.assitem_desc as 資產說明,a.item_amount as 數量,(select d.dept_name from deptment as d where d.dept_id = a.dept_id and d.dp=a.dp) as 權責單位,a.Confidentiality as 機密性,a.Integrity as 完整性,a.Availability as 可用性,(a.Confidentiality+a.Integrity+a.Availability) as 資產價值,(select e.assgroup_name from assetGroup as e where e.asset_id = a.asset_id and e.assgroup_id=a.assgroup_id) as 資產群組 FROM assetGroup as a, asset as b WHERE a.dp='" + Hidden_DP.Value + "' and a.asset_id = b.asset_id AND a.evaluate ='Y' AND a.expire='N' AND (a.asset_id=" + ddl_Asset.SelectedValue + " OR " + ddl_Asset.SelectedValue + "=0) AND " + ddl_AssetQryColumn.SelectedValue + " LIKE '%" + t_querystring.Text + "%' ORDER BY a.asset_id, a.assitem_id"

        End If

        ads.ConnectionString = Conn.ConnectionString
        ads.SelectCommand = ExcelSqlTxt
        dg.DataSource = ads.Select(DataSourceSelectArguments.Empty)
        dg.DataBind()
        dg.RenderControl(htw)

        Response.Write(sw.ToString())
        Response.End()
    End Sub

    Protected Sub ddl_AssetQryColumn_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_AssetQryColumn.SelectedIndexChanged
        t_querystring.Text = ""

    End Sub


 

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        If e.Row.RowIndex <> -1 Then
            e.Row.Cells(12).Text = Convert.ToDecimal(e.Row.Cells(9).Text) + Convert.ToDecimal(e.Row.Cells(10).Text) + Convert.ToDecimal(e.Row.Cells(11).Text)
        End If

    End Sub
End Class
