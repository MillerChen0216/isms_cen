﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_40130.aspx.vb" Inherits="ISMS_40130" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HiddenField ID="Hidden_DP" runat="server" />
        <table id="Table1" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 300px"
            visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    審核狀態查詢 |&nbsp;
                    <asp:DropDownList ID="flow_ddl" runat="server" AutoPostBack="True" CssClass="normal_font">
                        <asp:ListItem Selected="True" Value="0">請選擇審核作業</asp:ListItem>
                        <asp:ListItem Value="1">文件審核作業</asp:ListItem>
                        <asp:ListItem Value="2">風險評鑑審核</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
        </table>
        <br />
        <table id="Risk_tb1" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 750px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    風險評鑑審核作業 | 列表&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="750px">
                        <Columns>
                            <asp:BoundField DataField="asset_type" HeaderText="資產類別">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="riskEvtab_name" HeaderText="風險評鑑表">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="add_date" HeaderText="日期">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="審核">
                                <ItemTemplate>
                                   <asp:Button ID="audit_btn1" runat="server" OnClick="audit_btn1_Click" Text="瀏覽" />
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="form_id" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="審核狀態">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lb_wf" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <table id="Doc_Tb1" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 750px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    文件審核作業 | 列表&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="750px">
                        <Columns>
                            <asp:BoundField DataField="doc_type" HeaderText="文件類別">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_num" HeaderText="文件編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_name" HeaderText="文件名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_version" HeaderText="版次">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="管理">
                                <ItemTemplate>
                                    <asp:Button ID="audit_btn" runat="server" OnClick="audit_btn_Click" Text="瀏覽" />
                                    &nbsp;
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="doc_id" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <table id="Doc_Tb2" runat="server" cellpadding="1" cellspacing="1" class="tb_1" style="width: 550px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="2">
                    <img src="Images/exe.gif" />
                    文件資訊</td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    文件類別</td>
                <td class="tb_w_1">
                    <asp:Label ID="docCat_LB" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    文件編號</td>
                <td class="tb_w_1">
                    <asp:Label ID="docNum_LB" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    版次</td>
                <td class="tb_w_1">
                    <asp:Label ID="docVer_LB" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    文件名稱</td>
                <td class="tb_w_1">
                    <asp:Label ID="docName_LB" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    增修人</td>
                <td class="tb_w_1">
                    <asp:Label ID="docCreator" runat="server"></asp:Label>
                    /
                    <asp:Label ID="docCreatorName" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    內容簡述/修訂內容</td>
                <td class="tb_w_1" style="color: #345b7b">
                    <asp:Label ID="docDsc_LB" runat="server"></asp:Label></td>
            </tr>
        </table>
        <table id="Risk_tb2" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 770px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    風險評鑑填表 |
                    <asp:Label ID="lb_riskname" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="770px">
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <table id="Risk_tb3" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 550px" visible="false">
            <tr>
                <td class="tb_title_w_2">
                    <img src="Images/exe.gif" />
                    風險評鑑審核作業</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView5" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="550px">
                        <Columns>
                            <asp:BoundField DataField="wf_order" HeaderText="關卡">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="chk_name" HeaderText="審核身分">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="審核人員">
                                <EditItemTemplate>
                                    &nbsp;
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lb_username1" runat="server"></asp:Label>
                                    (<asp:Label ID="label_userid1" runat="server"></asp:Label>)
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="狀態">
                                <EditItemTemplate>
                                    &nbsp;
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddl_status1" runat="server" Visible="False">
                                    </asp:DropDownList>
                                    <asp:Label ID="lb_status1" runat="server" Visible="False"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="意見">
                                <EditItemTemplate>
                                    &nbsp;
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="comment_lb1" runat="server" Visible="False"></asp:Label>
                                    &nbsp;
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="wf_inside_order" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table id="Doc_Tb3" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 550px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    審核作業 -
                    <asp:Label ID="lb_status" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="550px">
                        <Columns>
                            <asp:BoundField DataField="wf_order" HeaderText="關卡">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="chk_name" HeaderText="審核身分">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="審核人員">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lb_username" runat="server"></asp:Label>
                                    (<asp:Label ID="label_userid" runat="server"></asp:Label>)
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="狀態">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddl_status" runat="server" Visible="False">
                                    </asp:DropDownList>
                                    <asp:Label ID="lb_status" runat="server" Visible="False"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="意見">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="comment_lb" runat="server" Visible="False"></asp:Label>
                                    &nbsp;
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="wf_inside_order" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <asp:SqlDataSource ID="Flow_DS" runat="server"></asp:SqlDataSource>
        <asp:HiddenField ID="Hidden_Docid" runat="server" Visible="False" />
        <asp:HiddenField ID="Hidden_apprv_id" runat="server" Visible="False" />
        <asp:HiddenField ID="HiddneRecord_id" runat="server" Visible="False" />
        <asp:HiddenField ID="Hidden_Status" runat="server" Visible="False" />
        <asp:SqlDataSource ID="WF_DS" runat="server"></asp:SqlDataSource>
        <asp:HiddenField ID="Hidden_Formid" runat="server" Visible="False" />
        <asp:SqlDataSource ID="Flow_Ds1" runat="server"></asp:SqlDataSource>
        <asp:SqlDataSource ID="Risk_DS" runat="server"></asp:SqlDataSource>
        <asp:SqlDataSource ID="WF_DS1" runat="server"></asp:SqlDataSource>
        <br />
    
    </div>
    </form>
</body>
</html>
