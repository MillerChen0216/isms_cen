﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO

Partial Class Netconnect_list
    Inherits PageBase
    'Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim uid As String = User.Identity.Name '使用者名稱
    Dim uname As String = Common.Get_User_Name(uid) '使用者代號
    Dim apprvid As String
    Dim approvalid As String
    Dim dp As String = Me.Get_DP
    Dim File_Path As String = ConfigurationManager.AppSettings("Netconnect_Path")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        approvalid = ConfigurationManager.AppSettings("flow_Netconnect") '15
        apprvid = ConfigurationManager.AppSettings("flow_Netconnect") '15
        Try
            If Not IsPostBack Then
                txb_sdate.Text = Common.Year(Now.AddDays(-30))
                txb_edate.Text = Common.Year(Now.AddDays(1))
                SqlDataSource()
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '申請清單
    Protected Sub SqlDataSource()
        Dim sql As String = "  SELECT * FROM Netconnect where empUid='" + uid + "'  and using='Y'  and empUid in (select user_id from ismsUser where using='Y') " '排除已撤銷之申請單'排除停用帳號
        If txb_sdate.Text <> "" Then
            sql += " and applyDate >= '" + Common.YearYYYY(txb_sdate.Text) + "'"
        End If
        If txb_edate.Text <> "" Then
            sql += " and applyDate <= '" + Common.YearYYYY(txb_edate.Text) + "'"
        End If
        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = sql
        GridView1.DataSourceID = SqlDataSource1.ID
    End Sub

    '申請清單_RowDataBound
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '民國年轉換
                Dim Button2 As Button = DirectCast(e.Row.FindControl("Button2"), Button)
                Dim lb_applydate As String = CType(e.Row.FindControl("lb_applydate"), Label).Text
                Dim hidden_recordId As String = CType(e.Row.FindControl("hidden_recordId"), HiddenField).Value
                CType(e.Row.FindControl("lb_applydate"), Label).Text = Common.Year(Date.Parse(lb_applydate))
                Dim ds As DataSet = Common.Qry_Process(hidden_recordId)
                If ds.Tables(0).Rows.Count <= 0 Then
                    Button2.Visible = False '只有在第一關才可以撤銷
                End If
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '查詢按鈕
    Protected Sub but_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_search.Click
        SqlDataSource()
    End Sub

    '查看_1及撤銷按鈕
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim GETrecordId As String = e.CommandArgument
        hidden_recordId.Value = GETrecordId
        Select Case e.CommandName

            Case "audit_btn"
                '查看
                Try

                    lb_recordid.Text = hidden_recordId.Value '紀錄該表單編號
                    Dim sql = "  SELECT * FROM Netconnect WHERE recordId = '" + lb_recordid.Text + "'"
                    Dim data As DataTable = Common.Con(sql)
                    If data.Rows.Count > 0 Then

                        '載入關卡表
                        GdvShow()
                        DefaultSource() '載入預設資料

                        '填入該lb_rowid資料

                        '處理checkbox
                        Dim s_ip_1 = If(data.Rows(0)("s_ip_1") = "1", True, False)
                        Dim s_ip_2 = If(data.Rows(0)("s_ip_2") = "1", True, False)


                        applyDate.Text = Common.Year(data.Rows(0)("applyDate")) '申請日期
                        recordId.Text = data.Rows(0)("recordId") '表單編號
                        empName.Text = data.Rows(0)("empName") '申請代表人
                        applyOrg.Text = data.Rows(0)("applyOrg") '申請單位
                        applyName.Text = data.Rows(0)("applyName") '申請人員
                        applyTel1.Text = data.Rows(0)("applyTel1") '連絡電話1
                        applyTel2.Text = data.Rows(0)("applyTel2") '連絡電話2
                        Netconnect_recordId.Text = data.Rows(0)("Netconnect_recordId") '原單號
                        Ddl_Syscd.SelectedValue = data.Rows(0)("syscd_Id") '系統名稱
                        STime.Text = Common.Year(data.Rows(0)("startTime_d")) + " " + data.Rows(0)("startTime_h") + ":" + data.Rows(0)("startTime_m") '連線時間(起)_日時分
                        ETime.Text = Common.Year(data.Rows(0)("endTime_d")) + " " + data.Rows(0)("endTime_h") + ":" + data.Rows(0)("endTime_m") '連線時間(迄)_日時分


                        sql = "  SELECT * FROM Netconnect WHERE recordId = '" + Netconnect_recordId.Text + "'"
                        Dim data2 As DataTable = Common.Con(sql)
                        If data2.Rows.Count > 0 Then
                            ori_applyReason.Text = data2.Rows(0)("applyReason") '原申請事由
                        End If


                        applyReason.Text = data.Rows(0)("applyReason") '先清空接下來使用者要填的申請事由
                        chk_s_ip_1.Checked = s_ip_1 '單一IP
                        s_ip_1_content.Text = data.Rows(0)("s_ip_1_content")
                        chk_s_ip_2.Checked = s_ip_2 '網段(區間)IP
                        s_ip_2_content.Text = data.Rows(0)("s_ip_2_content")
                        s_ip_Other.Text = data.Rows(0)("s_ip_Other") '協定/來源 Port
                        d_ip_content.Text = data.Rows(0)("d_ip_content") '目的IP
                        d_ip_other.Text = data.Rows(0)("d_ip_other") '協定/目的 Port
                        Netconnect_recordId.Text = data.Rows(0)("Netconnect_recordId") '申請事項的原表單編號
                        '申請事項
                        Select Case data.Rows(0)("applyItem")
                            Case "1"
                                applyItem.Text = "新增"
                                applyItemEnabled(False)
                                oriTb_1.ColSpan = 3
                            Case "2"
                                applyItem.Text = "異動"
                                applyItemEnabled(True)
                                oriTb_1.ColSpan = 1
                            Case "3"
                                applyItem.Text = "註銷"
                                applyItemEnabled(True)
                                oriTb_1.ColSpan = 1
                        End Select


                        '查看裡面是否有資料，有才秀出來
                        If IsDBNull(data.Rows(0)("applyFile")) Then
                            applyFile.Visible = False
                        Else
                            applyFile_link.Text = data.Rows(0)("applyFile")
                            applyFile.Visible = True
                        End If

                        Dim workflowId As Integer = Common.Get_workflowId(approvalid, recordId.Text) '取得關卡

                        SheetEnabled(False)
                        

                        '大於等於辦理人關卡(先預設第1關)
                        If workflowId >= 1 Then
                            '查看裡面是否有資料，有才秀出來
                            '附件檔案
                            If IsDBNull(data.Rows(0)("verifyFile")) Then
                                verifyFile.Visible = False
                            Else
                                verifyFile.Visible = True
                                verifyFile_link.Text = data.Rows(0)("verifyFile")
                            End If
                            '辦理日期
                            If Not IsDBNull(data.Rows(0)("verifyTime")) Then
                                verifyTime.Text = Common.Year(data.Rows(0)("verifyTime"))
                            Else
                                verifyTime.Text = ""
                            End If

                            verifyTb.Visible = True
                            '都不能修改
                            verifyTime.Enabled = False
                        Else
                            verifyTb.Visible = False
                        End If




                        but_update.Visible = False '20200804我的申請單不開放修改
                    Else
                        Response.Redirect("Netconnect_list.aspx", True) '重新載入附件表單簽核_審核 因為此表單編號已經不存在
                    End If


                    '頁面調整
                    tb_result.Visible = True
                    tb_query.Visible = False
                    GridView1.Visible = False
                    GdvShow() '載入關卡表
                Catch ex As Exception
                    lb_msg.Text = ex.Message
                End Try
            Case "using_btn"
                '撤銷
                Dim sql As New StringBuilder
                sql.Append("update Netconnect set using='N' where recordId ='" + hidden_recordId.Value + "' ")
                sql.Append("  update form_record set status = -1 where recordId = '" + hidden_recordId.Value + "' and approvalId = '" + approvalid + "' and status = 1")

                Dim conn As SqlConnection = Nothing : Dim cmd As New SqlCommand(sql.ToString)
                Try
                    conn = Common.ConnDBS : conn.Open() : cmd.Connection = conn
                    If cmd.ExecuteNonQuery() Then GridView1.DataBind() : lb_msg.Text = "撤銷成功！"
                Catch ex As Exception
                    lb_msg.Text = "撤銷失敗！" + ex.Message
                Finally
                    conn.Close() : conn.Close() : conn.Dispose() : cmd.Dispose()
                End Try

        End Select

    End Sub

    '小工具1
    Private Sub SheetEnabled(ByVal use As Boolean)
        applyOrg.Enabled = use '申請單位
        applyName.Enabled = use '申請人員
        applyTel1.Enabled = use '連絡電話1
        applyTel2.Enabled = use '連絡電話2
        Ddl_Syscd.Enabled = use '系統名稱
        applyReason.Enabled = use '申請事由
        chk_s_ip_1.Enabled = use '單一IP
        s_ip_1_content.Enabled = use
        chk_s_ip_2.Enabled = use '網段(區間)IP
        s_ip_2_content.Enabled = use
        s_ip_Other.Enabled = use '協定/來源 Port
        d_ip_content.Enabled = use '目的IP
        d_ip_other.Enabled = use '協定/目的 Port
    End Sub

    '表單的Enabled
    Private Sub applyItemEnabled(ByVal use As Boolean)
        Try
            '若選到新增不用選原單號
            oriTb_2.Visible = use
            oriTb_3.Visible = use
            ori_applyReasonTb_1.Visible = use
            ori_applyReasonTb_2.Visible = use

        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '預設資料來源
    Private Sub DefaultSource()
        Try
            '系統名稱
            Dim sql As String = "SELECT * FROM SysCode WHERE syscd_Enabled = 1"
            sql += " ORDER BY syscd_time DESC "
            Ddl_Syscd.Items.Clear()
            Ddl_Syscd.DataTextField = "syscd_Name"
            Ddl_Syscd.DataValueField = "syscd_Id"
            Ddl_Syscd.DataSource = Common.Con(sql)
            Ddl_Syscd.DataBind()
            Ddl_Syscd.SelectedValue = Ddl_Syscd.Items(0).Value '預設選項為第一個值

        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_回列表_按鈕
    Protected Sub but_esc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_esc.Click
        Response.Redirect("Netconnect_list.aspx", True)
    End Sub

    '載入關卡表
    Private Sub GdvShow()
        Dim dt As DataTable = Common.Qry_workflowName_isms_Req(approvalid, lb_recordid.Text, apprvid, dp)
        GridView.DataSource = dt
        GridView.DataBind()

    End Sub

    '檔案下載_按鈕
    Protected Sub verifyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles verifyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + verifyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '檔案下載_按鈕
    Protected Sub applyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + applyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料

            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            Dim hid_status As HiddenField = CType(e.Row.FindControl("hid_status"), HiddenField)
            '***意見
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text
            '***審核人員***
            'Dim lb_wf_inside_order As String = CType(e.Row.FindControl("lb_wf_inside_order"), Label).Text

            Dim ds As DataSet

            '***Start
            If o_wf_order = "0" And hid_status.Value <> "1" Then
                pan_user.Visible = True
                '將審核人員設為自己
                Dim sql = "    SELECT user_id, user_name FROM ismsUser WHERE user_id = '" + uid + "'"
                Dim data As DataTable = Common.Con(sql)
                CType(e.Row.FindControl("lb_username"), Label).Text = data.Rows(0)("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = data.Rows(0)("user_id")
            ElseIf hid_status.Value = "1" Then '待審核
                ds = Common.Qry_form_record_wait(lb_recordid.Text)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
            ElseIf hid_status.Value = "2" Or hid_status.Value = "9" Then '審核已通過
                ds = Common.Qry_form_record_name(lb_recordid.Text, o_wf_order)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
            End If

            If hid_status.Value = "" Then
                lb_status.Text = ""
            Else
                CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(hid_status.Value))
            End If
        End If
    End Sub

End Class
