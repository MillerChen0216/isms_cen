﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_30230.aspx.vb" Inherits="ISMS_30230" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <div>
                <div>
                        <asp:HiddenField ID="Hidden_DP" runat="server" />

                    <table id="Show_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 488px"
                        visible="true">
                        <tr>
                            <td class="tb_title_w_2" colspan="5" rowspan="1" style="width: 482px; height: 28px">
                                <img src="Images/Icon1.GIF" />資產脆弱威脅分析|列表
                                <asp:DropDownList ID="ddl_Asset" runat="server" AutoPostBack="True" Width="82px">
                                </asp:DropDownList>
                                <asp:Button ID="Show_Insert" runat="server" CssClass="button-small" Text="新增資料" />&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 501px">
                                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    CellPadding="0" Width="500px">
                                    <Columns>
                                        <asp:BoundField DataField="assitem_id" HeaderText="資產代碼">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="assitem_name" HeaderText="資產名稱">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="info_Fname" HeaderText="C.I.A">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="weakness" HeaderText="脆弱點敘述">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="threat" HeaderText="威脅敘述">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="riskp_score" HeaderText="風險機率值">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="impact_score" HeaderText="風險衝擊值">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:ButtonField ButtonType="Button" CommandName="Edit_Btn" Text="編輯">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" Width="10%" />
                                        </asp:ButtonField>
                                        <asp:ButtonField ButtonType="Button" CommandName="Del_Btn" Text="刪除">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" Width="10%" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="info_type" HeaderText="info_type">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="weakness_id" HeaderText="weakness_id">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="threat_id" HeaderText="threat_id">
                                            <HeaderStyle CssClass="tb_title_w_1" />
                                            <ItemStyle CssClass="tb_w_1" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table id="Edit_TB" runat="server" class="tb_1" style="width:628px" visible="false" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="tb_title_w_2" colspan="11" style="height: 27px">
                                <img src="Images/Icon1.GIF" />
                                資產脆弱威脅分析&nbsp; | 更新資料 &nbsp;<asp:Button ID="Update_Btn" runat="server" CssClass="button-small"
                                    Text="更新資料" /></td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1" style="width: 102px">
                                資產名稱</td>
                            <td class="tb_title_w_1" style="width: 57px">
                                C.I.A</td>
                            <td class="tb_title_w_1" style="width: 144px">
                                脆弱點敘述</td>
                            <td class="tb_title_w_1" style="width: 134px">
                                威脅敘述</td>
                            <td class="tb_title_w_1" style="width: 144px">
                                風險機率值</td>
                            <td class="tb_title_w_1" style="width: 144px">
                                風險衝擊值</td>
                        </tr>
                        <tr style="color: #656b76; background-color: #ffffff">
                            <td class="tb_w_1" style="width: 148px; height: 26px;">
                                <asp:TextBox ID="assitem_name_upd" runat="server" Width="145px"></asp:TextBox></td>
                            <td class="tb_w_1" style="width: 57px; height: 26px;">
                                <asp:TextBox ID="info_type_upd" runat="server" Width="55px"></asp:TextBox></td>
                            <td class="tb_w_1" style="width: 144px; height: 26px;">
                                <asp:DropDownList ID="ddl_weakness_upd" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="tb_w_1" style="width: 120px; height: 26px;">
                                &nbsp;<asp:DropDownList ID="ddl_threat_upd" runat="server" Width="112px">
                                </asp:DropDownList></td>
                            <td class="tb_w_1" style="width: 144px; height: 26px;">
                                <asp:DropDownList ID="ddl_riskp_upd" runat="server" Width="144px">
                                </asp:DropDownList></td>
                            <td class="tb_w_1" style="width: 144px; height: 26px;">
                                <asp:DropDownList ID="ddl_impact_upd" runat="server">
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                </div>
            </div>
            <table id="Ins_TB" runat="server" class="tb_1" style="width:100%x" visible="false" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="tb_title_w_2" colspan="11" style="height: 27px">
                        <img src="Images/Icon1.GIF" />
                        資產脆弱威脅分析&nbsp; | 新增資料 &nbsp;&nbsp;
                        <asp:Button ID="Insert_Btn" runat="server" CssClass="button-small"
                            Text="確定資料" /></td>
                </tr>
                <tr>
                    <td class="tb_title_w_1" style="width: 148px">
                        資產名稱</td>
                    <td class="tb_title_w_1" style="width: 57px">
                        C.I.A</td>
                    <td class="tb_title_w_1" style="width: 144px">
                        脆弱點敘述</td>
                    <td class="tb_title_w_1" style="width: 120px">
                        威脅敘述</td>
                    <td class="tb_title_w_1" style="width: 144px">
                        風險機率值</td>
                    <td class="tb_title_w_1" style="width: 144px">
                        風險衝擊值</td>
                </tr>
                <tr style="color: #656b76; background-color: #ffffff">
                    <td class="tb_w_1">
                        <asp:DropDownList ID="ddl_assitem_id_ins" runat="server">
                            <asp:ListItem Selected="True">IBM SERVER X225</asp:ListItem><asp:ListItem>HP Server</asp:ListItem>
                        </asp:DropDownList></td>
                    <td class="tb_w_1" style="width: 57px; height: 9px">
                        C(機密性)</td>
                    <td class="tb_w_1">
                        <asp:DropDownList ID="ddl_Cweakness_ins" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td class="tb_w_1">
                        &nbsp;<asp:DropDownList ID="ddl_Cthreat_ins" runat="server" Width="112px">
                        </asp:DropDownList></td>
                    <td class="tb_w_1">
                        <asp:DropDownList ID="ddl_Criskp_ins" runat="server">
                        </asp:DropDownList></td>
                    <td class="tb_w_1">
                        <asp:DropDownList ID="ddl_Cimpact_ins" runat="server">
                        </asp:DropDownList></td>
                </tr>
                <tr style="color: #656b76; background-color: #ffffff">
                    <td class="tb_w_1" style="width: 148px; height: 24px;">
                    </td>
                    <td class="tb_w_1" style="width: 57px; height: 24px;">
                        I(完整性)</td>
                    <td class="tb_w_1">
                        <asp:DropDownList ID="ddl_Iweakness_ins" runat="server">
                        </asp:DropDownList></td>
                    <td class="tb_w_1">
                        <asp:DropDownList ID="ddl_Ithreat_ins" runat="server" Width="112px">
                        </asp:DropDownList></td>
                    <td class="tb_w_1">
                        <asp:DropDownList ID="ddl_Iriskp_ins" runat="server">
                        </asp:DropDownList></td>
                    <td class="tb_w_1">
                        <asp:DropDownList ID="ddl_Iimpact_ins" runat="server">
                        </asp:DropDownList></td>
                </tr>
                <tr style="color: #656b76; background-color: #ffffff">
                    <td class="tb_w_1" style="width: 148px; height: 24px;">
                    </td>
                    <td class="tb_w_1" style="width: 57px; height: 24px;">
                        A(可用性)</td>
                    <td class="tb_w_1" style="height: 24px;">
                        <asp:DropDownList ID="ddl_Aweakness_ins" runat="server">
                        </asp:DropDownList></td>
                    <td class="tb_w_1" style="height: 24px;">
                        <asp:DropDownList ID="ddl_Athreat_ins" runat="server" Width="112px">
                        </asp:DropDownList></td>
                    <td class="tb_w_1">
                        <asp:DropDownList ID="ddl_Ariskp_ins" runat="server">
                        </asp:DropDownList></td>
                    <td class="tb_w_1">
                        <asp:DropDownList ID="ddl_Aimpact_ins" runat="server">
                        </asp:DropDownList></td>
                </tr>
            </table>
        </div>
    
    </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <asp:HiddenField ID="Hidden_assitem_id" runat="server" />
    </form>
</body>
</html>
