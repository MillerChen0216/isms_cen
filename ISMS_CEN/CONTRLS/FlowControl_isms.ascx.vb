﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports System.Collections.Generic
Partial Class FlowControl_isms
    Inherits System.Web.UI.UserControl
    Dim Common As New ISMS.Common
    Dim _apprvid As String = "0", _uid As String = "", _formid As String = "0", _createuid As String = "", _rowid As Integer = 0
    Dim _other As String = "", _msg As String = ""

    Dim _approvalid As String = ""
#Region "傳值"
    Public WriteOnly Property msg() As String
        Set(ByVal value As String)
            _msg = value
        End Set
    End Property
    Public WriteOnly Property other() As String
        Set(ByVal value As String)
            _other = value
        End Set
    End Property
    Public WriteOnly Property apprvid() As String
        Set(ByVal value As String)
            _apprvid = value
        End Set
    End Property
    Public WriteOnly Property approvalid() As String
        Set(ByVal value As String)
            _approvalid = value
        End Set
    End Property
    Public WriteOnly Property createuid() As String
        Set(ByVal value As String)
            _createuid = value
        End Set
    End Property
    Public WriteOnly Property uid() As String
        Set(ByVal value As String)
            _uid = value
        End Set
    End Property
    Public WriteOnly Property rowid() As String
        Set(ByVal value As String)
            _rowid = value
        End Set
    End Property
    Public WriteOnly Property formid() As String
        Set(ByVal value As String)
            _formid = value
        End Set
    End Property

    Public ReadOnly Property _verify() As Boolean
        Get
            Return lb_verify.Text
        End Get
    End Property


    Public ReadOnly Property _i() As Integer
        Get
            Return lb_i.Text
        End Get
    End Property
    Public ReadOnly Property _wf_order() As String
        Get
            Return CType(GridView.Rows(lb_i.Text).FindControl("wf_order"), Label).Text
        End Get
    End Property

    Public ReadOnly Property _userid() As String
        Get
            Dim ddl_user As DropDownList = CType(GridView.Rows(lb_i.Text).FindControl("ddl_user"), DropDownList)
            Dim o_userid As String = ""
            If ddl_user.Visible = True Then
                o_userid = Left(ddl_user.SelectedValue, ddl_user.SelectedValue.Length - 1)
            Else
                o_userid = CType(GridView.Rows(lb_i.Text).FindControl("label_userid"), Label).Text
            End If
            Return o_userid
        End Get
    End Property
    Public ReadOnly Property _status() As String
        Get
            Return CType(GridView.Rows(lb_i.Text).FindControl("ddl_status"), DropDownList).SelectedValue
        End Get
    End Property
    Public ReadOnly Property _comment() As String
        Get
            Return CType(GridView.Rows(lb_i.Text).FindControl("txb_comment"), TextBox).Text
        End Get
    End Property
    Public ReadOnly Property _wf_inside_order() As String
        Get

            Dim ddl_user As DropDownList = CType(GridView.Rows(lb_i.Text).FindControl("ddl_user"), DropDownList)
            Dim o_wf_inside_order As String = ""
            If ddl_user.Visible = True Then
                o_wf_inside_order = Right(ddl_user.SelectedValue, 1)
            Else
                o_wf_inside_order = CType(GridView.Rows(lb_i.Text).FindControl("lb_wf_inside_order"), Label).Text
            End If
            Return o_wf_inside_order
        End Get
    End Property

    Public ReadOnly Property _wf_order_next() As String
        Get
            Dim o_wf_order As String = ""
            Dim i As Integer = CInt(lb_i.Text) + 1
            If i < GridView.Rows.Count Then
                o_wf_order = CType(GridView.Rows(i).FindControl("wf_order"), Label).Text
            End If
            Return o_wf_order
        End Get
    End Property




    Public ReadOnly Property _userid_next() As String
        Get
            Dim o_userid As String = ""
            Dim i As Integer = CInt(lb_i.Text) + 1
            If i < GridView.Rows.Count Then
                Dim ddl_user As DropDownList = CType(GridView.Rows(i).FindControl("ddl_user"), DropDownList)
                If ddl_user.Visible = True Then
                    o_userid = Left(ddl_user.SelectedValue, ddl_user.SelectedValue.Length - 1)
                Else
                    o_userid = CType(GridView.Rows(i).FindControl("label_userid"), Label).Text
                End If
            End If
            Return o_userid
        End Get
    End Property

    Public ReadOnly Property _wf_inside_order_next() As String
        Get
            Dim o_wf_inside_order As String = ""
            Dim i As Integer = CInt(lb_i.Text) + 1
            If i < GridView.Rows.Count Then
                Dim ddl_user As DropDownList = CType(GridView.Rows(i).FindControl("ddl_user"), DropDownList)
                If ddl_user.Visible = True Then
                    o_wf_inside_order = Right(ddl_user.SelectedValue, 1)
                Else
                    o_wf_inside_order = CType(GridView.Rows(i).FindControl("lb_wf_inside_order"), Label).Text
                End If
            End If
            Return o_wf_inside_order
        End Get
    End Property


    Public ReadOnly Property _wf_order_pre() As String
        Get
            Dim o_wf_order As String = ""
            Dim dt As DataTable = ViewState("dt")
            If dt.Rows.Count > 0 Then
                o_wf_order = dt.Rows(0)("role")
            End If
            'Dim i As Integer = CInt(lb_i.Text) - 1
            'If i < GridView.Rows.Count Then
            '    o_wf_order = CType(GridView.Rows(i).FindControl("wf_order"), Label).Text
            'End If
            Return o_wf_order
        End Get
    End Property




    Public ReadOnly Property _userid_pre() As String
        Get
            Dim o_userid As String = ""
            Dim dt As DataTable = ViewState("dt")
            If dt.Rows.Count > 0 Then
                o_userid = dt.Rows(0)("userid")
            End If
            'Dim i As Integer = CInt(lb_i.Text) - 1
            'If i < GridView.Rows.Count Then
            '    Dim ddl_user As DropDownList = CType(GridView.Rows(i).FindControl("ddl_user"), DropDownList)
            '    If ddl_user.Visible = True Then
            '        o_userid = Left(ddl_user.SelectedValue, ddl_user.SelectedValue.Length - 1)
            '    Else
            '        o_userid = CType(GridView.Rows(i).FindControl("label_userid"), Label).Text
            '    End If
            'End If
            Return o_userid
        End Get
    End Property

    Public ReadOnly Property _wf_inside_order_pre() As String
        Get
            Dim o_wf_inside_order As String = ""
            Dim dt As DataTable = ViewState("dt")
            If dt.Rows.Count > 0 Then
                o_wf_inside_order = dt.Rows(0)("role")
            End If
            'Dim i As Integer = CInt(lb_i.Text) - 1
            'If i < GridView.Rows.Count Then
            '    Dim ddl_user As DropDownList = CType(GridView.Rows(i).FindControl("ddl_user"), DropDownList)
            '    If ddl_user.Visible = True Then
            '        o_wf_inside_order = Right(ddl_user.SelectedValue, 1)
            '    Else
            '        o_wf_inside_order = CType(GridView.Rows(i).FindControl("lb_wf_inside_order"), Label).Text
            '    End If
            'End If
            Return o_wf_inside_order
        End Get
    End Property
#End Region


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'ApprovalControl1.apprvid = _apprvid
        WF_SqlDS()

    End Sub


    Protected Sub WF_SqlDS() '簽核角色
        WF_DS.ConnectionString = Common.ConnDBS.ConnectionString
        If _rowid = 99 Then

            WF_DS.SelectCommand = "SELECT wf_order,chk_name,isnull(userid,'') wf_userid,isnull(status,'') wf_status_id,isnull(comment,'') wf_comment,case when status='1' then null else signdate end  signdate   FROM workflowName a left join form_record b on  approvalid='" & _approvalid & "' and recordid='" & _formid & "' and wf_order=role where apprv_id='" + _apprvid + "' order by isnull(b.id,999999999),wf_order"

            GridView1.DataSourceID = WF_DS.ID
        Else
            'WF_DS.SelectCommand = "SELECT id as chk_id,apprvid as apprv_id,wfOrder as wf_order,name as chk_name,  1 wf_inside_order FROM workflow_Name where apprvid='" + _apprvid + "' order by wforder "
            WF_DS.SelectCommand = "SELECT wf_order,chk_name,isnull(userid,'') wf_userid,isnull(status,'') wf_status_id,isnull(comment,'') wf_comment,b.id, case when status='1' then null else signdate end  signdate   FROM workflowName a join form_record b on  approvalid='" & _approvalid & "' and recordid='" & _formid & "' and wf_order=role where apprv_id='" + _apprvid + "'    union all SELECT wf_order,chk_name,'' wf_userid,''  wf_status_id,'' wf_comment ,9999999999,''  FROM workflow_Name where apprv_id='" + _apprvid + "'   and wf_order > (select role from form_record where  approvalid='" & _approvalid & "' and recordid='" & _formid & "' and status=1) order by id"


            GridView.DataSourceID = WF_DS.ID
        End If








    End Sub

    'Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        '***人員
    '        Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
    '        Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
    '        Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
    '        Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
    '        '***狀態
    '        Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
    '        Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
    '        '***意見
    '        Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
    '        Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
    '        '***關卡***
    '        Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text

    '        '***Start
    '        Dim dt_wfFormRecord As DataTable
    '        If _rowid = 0 And o_wf_order = "0" Then
    '            '人員
    '            pan_user.Visible = True : ddl_user.Visible = False : lb_userid.Text = _uid
    '            '狀態
    '            ddl_status.Visible = True : lb_status.Visible = False
    '            ddl_status.DataSource = Common.Get_WFStatus(0)
    '            ddl_status.DataValueField = "wfstatus_id" : ddl_status.DataTextField = "wfstatus_name" : ddl_status.DataBind()
    '            lb_i.Text = "0" : _rowid = 0
    '            '意見
    '            txt_comment.Visible = True : lb_comment.Visible = False
    '            lb_i.Text = e.Row.RowIndex : _rowid = e.Row.RowIndex
    '        ElseIf _rowid = 99 Then '僅唯讀
    '            '***已簽核資料
    '            ' dt_wfFormRecord = CType(Common.Get_wfFormRecord(_apprvid, _formid, ""), DataSet).Tables(0)
    '            dt_wfFormRecord = CType(Common.Get_form_record_isms(_approvalid, _formid, ""), DataSet).Tables(0)

    '            If dt_wfFormRecord.Rows.Count > 0 Then
    '                Dim dv As DataView = dt_wfFormRecord.DefaultView
    '                dv.RowFilter = "wf_order='" & o_wf_order & "'"
    '                If dv.Count > 0 Then '表示有簽核記錄    
    '                    With dv.ToTable.Rows(0)
    '                        '人員
    '                        pan_user.Visible = True : ddl_user.Visible = False
    '                        lb_userid.Text = Common.notNull(.Item("wf_userid"))

    '                        '狀態
    '                        lb_status.Visible = True : ddl_status.Visible = False
    '                        lb_status.Text = Common.Get_WFStatusName(Common.FixNull(.Item("wf_status_id")))
    '                        '意見
    '                        lb_comment.Visible = True : txt_comment.Visible = False
    '                        lb_comment.Text = Common.notNull(.Item("wf_comment"))
    '                    End With
    '                End If
    '            End If
    '        Else
    '            '***已簽核資料
    '            ' dt_wfFormRecord = CType(Common.Get_wfFormRecord(_apprvid, _formid, ""), DataSet).Tables(0)
    '            dt_wfFormRecord = CType(Common.Get_form_record_isms(_approvalid, _formid, ""), DataSet).Tables(0)
    '            If dt_wfFormRecord.Rows.Count > 0 Then
    '                Dim dv As DataView = dt_wfFormRecord.DefaultView
    '                dv.RowFilter = "wf_order='" & o_wf_order & "'"
    '                If dv.Count > 0 Then '表示有簽核記錄    
    '                    With dv.ToTable.Rows(0)
    '                        If .Item("wf_status_id") = "1" And .Item("wf_userid") = _uid Then '等待審核且簽核人員進行審核
    '                            '人員
    '                            pan_user.Visible = True : ddl_user.Visible = False
    '                            lb_userid.Text = .Item("wf_userid")
    '                            '狀態
    '                            ddl_status.Visible = True : lb_status.Visible = False
    '                            ddl_status.DataSource = Common.Get_WFStatus(1)
    '                            ddl_status.DataValueField = "wfstatus_id" : ddl_status.DataTextField = "wfstatus_name" : ddl_status.DataBind()
    '                            '意見
    '                            txt_comment.Visible = True : lb_comment.Visible = False

    '                            lb_i.Text = e.Row.RowIndex : _rowid = e.Row.RowIndex '用下呈現下一筆該簽核人員清單
    '                            If o_wf_order = "2" Then lb_msg.Text = _msg Else lb_msg.Text = "" 'for 測試室申請未三天前提出申請者，呈現告知訊息
    '                        Else '唯讀
    '                            '人員
    '                            pan_user.Visible = True : ddl_user.Visible = False
    '                            lb_userid.Text = Common.notNull(.Item("wf_userid"))

    '                            '狀態
    '                            lb_status.Visible = True : ddl_status.Visible = False
    '                            lb_status.Text = Common.Get_WFStatusName(Common.FixNull(.Item("wf_status_id")))
    '                            '意見
    '                            lb_comment.Visible = True : txt_comment.Visible = False
    '                            lb_comment.Text = Common.notNull(.Item("wf_comment"))
    '                        End If



    '                    End With
    '                Else
    '                    'ddl_user.Visible = False : pan_user.Visible = False
    '                End If
    '            End If






    '        End If








    '        If e.Row.RowIndex = CInt(lb_i.Text) + 1 Then '呈現下一列資料


    '            Dim dt_WorkFlow As DataTable = Common.Get_Wf_User1_isms(_apprvid, o_wf_order, "")
    '            If dt_WorkFlow.Rows.Count > 0 Then
    '                If dt_WorkFlow.Rows(0)("wf_userid") = "[self]" Then
    '                    '人員
    '                    pan_user.Visible = True : ddl_user.Visible = False
    '                    lb_userid.Text = _createuid


    '                Else
    '                    ddl_user.Visible = True : pan_user.Visible = False
    '                    ddl_user.Items.Clear()
    '                    ddl_user.DataValueField = "a" : ddl_user.DataTextField = "user_name"
    '                    ddl_user.DataSource = dt_WorkFlow : ddl_user.DataBind()


    '                End If
    '            End If

    '        End If



    '        If pan_user.Visible = True And lb_userid.Text <> "" Then
    '            lb_username.Text = ISMS.Common.notNull(Common.Get_User_Name(lb_userid.Text))
    '            If lb_username.Text = "" Then lb_username.Text = Common.Get_venderemp_Name(lb_userid.Text) '表示非員工，可能為廠商
    '        End If


    '    End If










    'End Sub

    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            '***意見
            Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text

            '***Start

            If lb_status.Text = "1" And lb_userid.Text = _uid Then

                '人員
                pan_user.Visible = True : ddl_user.Visible = False
                '狀態
                ddl_status.Visible = True : lb_status.Visible = False
                ddl_status.DataSource = Common.Get_WFStatus(1)
                ddl_status.DataValueField = "wfstatus_id" : ddl_status.DataTextField = "wfstatus_name" : ddl_status.DataBind()
                '意見
                txt_comment.Visible = True : lb_comment.Visible = False

                lb_i.Text = e.Row.RowIndex : _rowid = e.Row.RowIndex '用下呈現下一筆該簽核人員清單
                If o_wf_order = "2" Then lb_msg.Text = _msg Else lb_msg.Text = "" 'for 測試室申請未三天前提出申請者，呈現告知訊息

                Dim dt As DataTable = Common.Qry_form_record(_approvalid, _formid, o_wf_order)
                ViewState("dt") = dt
            Else
                If lb_status.Text <> "" Then lb_status.Text = Common.Get_WFStatusName(lb_status.Text)
            End If

            If e.Row.RowIndex = CInt(lb_i.Text) + 1 Then '呈現下一列資料
                Dim dt_WorkFlow As DataTable = Common.Get_Wf_User1_isms(_apprvid, o_wf_order, "")
                If dt_WorkFlow.Rows.Count > 0 Then
                    If dt_WorkFlow.Rows(0)("wf_userid") = "[self]" Then
                        '人員
                        pan_user.Visible = True : ddl_user.Visible = False
                        lb_userid.Text = _createuid


                    Else
                        ddl_user.Visible = True : pan_user.Visible = False
                        ddl_user.Items.Clear()
                        ddl_user.DataValueField = "a" : ddl_user.DataTextField = "user_name"
                        ddl_user.DataSource = dt_WorkFlow : ddl_user.DataBind()


                    End If
                End If

            End If

            lb_username.Text = ISMS.Common.notNull(Common.Get_User_Name(lb_userid.Text))
            If lb_username.Text = lb_userid.Text Then lb_username.Text = Common.Get_venderemp_Name(lb_userid.Text.Trim.ToUpper) '表示非員工，可能為廠商

            If pan_user.Visible = True And lb_userid.Text <> "" Then


            End If


        End If










    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***人員
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
           
        
            '***已簽核資料
            Try
                If lb_status.Text <> "" Then lb_status.Text = Common.Get_WFStatusName(lb_status.Text)
                If pan_user.Visible = True And lb_userid.Text <> "" Then
                    lb_username.Text = ISMS.Common.notNull(Common.Get_User_Name(lb_userid.Text))
                    If lb_username.Text = lb_userid.Text Then lb_username.Text = Common.Get_venderemp_Name(lb_userid.Text.Trim.ToUpper) '表示非員工，可能為廠商

                End If
            Catch ex As Exception

            End Try











        


        End If










    End Sub











    Protected Sub ddl_user_click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl_user As DropDownList = CType(sender, DropDownList)
        Dim oRow As GridViewRow = CType(sender, DropDownList).NamingContainer
        Dim Index As Integer = oRow.RowIndex
        With GridView.Rows(Index)
            CType(.FindControl("lb_wf_inside_order"), Label).Text = Right(ddl_user.SelectedValue, 1)
            CType(.FindControl("label_userid"), Label).Text = Left(ddl_user.SelectedValue, ddl_user.SelectedValue.Length - 1)
            CType(.FindControl("lb_username"), Label).Text = ddl_user.SelectedItem.Text

        End With

    End Sub

End Class
