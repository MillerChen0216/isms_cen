﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Partial Class CONTRLS_approvalControl
    Inherits System.Web.UI.UserControl
    Dim _apprvid As String = "0"
    Dim Common As New ISMS.Common
    Public WriteOnly Property apprvid() As String
        Set(ByVal value As String)
            _apprvid = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Label1.Text = Common.Get_approval(_apprvid)
    End Sub
End Class
