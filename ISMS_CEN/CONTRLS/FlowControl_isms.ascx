<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FlowControl_isms.ascx.vb" Inherits="FlowControl_isms" %>
<%@ Register Src="approvalControl.ascx" TagName="approvalControl" TagPrefix="uc1" %>
   

    <link href="../CSS/isms.css" rel="stylesheet" type="text/css" />

<table id="tb2" runat="server" cellpadding="0" cellspacing="0" class="tb_1"   visible="true" width="100%">
            <tr>
                <td class="tb_title_w_2">
                 
                    <asp:Label ID="lb_verify" runat="server" Text="0" Visible="false"></asp:Label>
                    <asp:Label ID="lb_i" runat="server" Text="99" Visible="false"></asp:Label>
                    <asp:Label ID="lb_msg" runat="server" ForeColor="red"></asp:Label>
                 </td>
            </tr>
            <tr>
                <td>
                    
                    <asp:GridView ID="GridView" runat="server" AutoGenerateColumns="False" CellPadding="3"    CssClass="tb_1" Width="100%"  >
                        <Columns>
                           <asp:TemplateField HeaderText="關卡" ItemStyle-CssClass="tb_w_1">
                                 <ItemTemplate>
                                    <asp:Label ID="wf_order" runat="server" Text='<%# eval("wf_order") %>'></asp:Label>
                                 </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField Headertext="審核身份" ItemStyle-CssClass="tb_w_1">
                                  <ItemTemplate>
                                    <asp:Label ID="chk_name" runat="server" Text='<%# eval("chk_name") %>' ></asp:Label>
                                 </ItemTemplate>
                           </asp:TemplateField>                        
                           <asp:TemplateField HeaderText="審核人員" ItemStyle-CssClass="tb_w_1">
                                <ItemTemplate>
                  
                                    <asp:DropDownList ID="ddl_user" runat="server" Visible="false"  OnSelectedIndexChanged="ddl_user_click"></asp:DropDownList>
                                  
                                     
                                    <asp:Panel ID="pan_user" runat="server" Visible='<%# iif(eval("wf_userid")="","false","true")  %>' >
                                    <asp:Label ID="lb_username" runat="server"></asp:Label>
                                    (<asp:Label ID="label_userid" runat="server" Text='<%# eval("wf_userid") %>' ></asp:Label>)
                                    </asp:Panel> 
                                                
                         
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="狀態" ItemStyle-CssClass="tb_w_1">
                               <ItemTemplate>
                                    <asp:DropDownList ID="ddl_status" runat="server" Visible="False"></asp:DropDownList>
                                    <asp:Label ID="lb_status" runat="server"  text='<%# eval("wf_status_id")  %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="意見" ItemStyle-CssClass="tb_w_1">
                                <ItemTemplate>
                                    <asp:TextBox ID="txb_comment" runat="server" Visible="False" Width="150px"></asp:TextBox>
                                    <asp:Label ID="lb_comment" runat="server"   Text='<%# eval("wf_comment")  %>'></asp:Label>
                                
                                  
                                </ItemTemplate>
                                       </asp:TemplateField>
                            <asp:TemplateField HeaderText="審核時間" ItemStyle-CssClass="tb_w_1">
                                <ItemTemplate>
                                    <asp:Label ID="lb_signdate" runat="server"   Text='<%# eval("signdate")  %>'></asp:Label>
                                
                                  
                                </ItemTemplate>
                            </asp:TemplateField>
                   
                              
                        </Columns>
                       <HeaderStyle CssClass="tb_title_w_1" />
             
                    </asp:GridView>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"    CssClass="tb_1" Width="100%"  >
                        <Columns>
                           <asp:TemplateField HeaderText="關卡" ItemStyle-CssClass="tb_w_1">
                                 <ItemTemplate>
                                    <asp:Label ID="wf_order" runat="server" Text='<%# eval("wf_order") %>'></asp:Label>
                                 </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField Headertext="審核身份" ItemStyle-CssClass="tb_w_1">
                                  <ItemTemplate>
                                    <asp:Label ID="chk_name" runat="server" Text='<%# eval("chk_name") %>' ></asp:Label>
                                 </ItemTemplate>
                           </asp:TemplateField>                        
                           <asp:TemplateField HeaderText="審核人員" ItemStyle-CssClass="tb_w_1">
                                <ItemTemplate>
                  
                                
                                     
                                    <asp:Panel ID="pan_user" runat="server" >
                                    <asp:Label ID="lb_username" runat="server" ></asp:Label>
                                    (<asp:Label ID="label_userid" runat="server" Text='<%# eval("wf_userid") %>' ></asp:Label>)
                                    </asp:Panel> 
                                                
                         
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="狀態" ItemStyle-CssClass="tb_w_1">
                               <ItemTemplate>
                                     <asp:Label ID="lb_status" runat="server" text='<%# eval("wf_status_id")  %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="意見" ItemStyle-CssClass="tb_w_1">
                                <ItemTemplate>
                                     <asp:Label ID="lb_comment" runat="server"  Text='<%# eval("wf_comment")  %>'></asp:Label>
                                
                                  
                                </ItemTemplate>
                            </asp:TemplateField>
                                               <asp:TemplateField HeaderText="審核時間" ItemStyle-CssClass="tb_w_1">
                                <ItemTemplate>
                                    <asp:Label ID="lb_signdate" runat="server"   Text='<%# eval("signdate")  %>'></asp:Label>
                                
                                  
                                </ItemTemplate>
                            </asp:TemplateField>
                              
                        </Columns>
                       <HeaderStyle CssClass="tb_title_w_1" />
             
                    </asp:GridView>
                    </td>
            </tr>
        </table>       
         <asp:SqlDataSource ID="WF_DS" runat="server"></asp:SqlDataSource>
