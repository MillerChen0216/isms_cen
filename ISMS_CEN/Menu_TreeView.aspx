﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Menu_TreeView.aspx.vb" Inherits="Menu_TreeView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>TreeView_Page</title>  
</head>
<body style="margin-left:0px ; margin-top:0px; overflow-x:hidden; width:300px ">
    <form id="form1" runat="server" >
    <div>
        <asp:TreeView ID="TreeView1" runat="server" ImageSet="XPFileExplorer" NodeIndent="15" ShowLines="True" LineImagesFolder="~/TreeLineImages" ExpandDepth="2">
            <ParentNodeStyle Font-Bold="False" />
            <HoverNodeStyle Font-Underline="True" ForeColor="#6666AA" />
            <SelectedNodeStyle BackColor="#B5B5B5" Font-Underline="True" HorizontalPadding="0px"
                VerticalPadding="0px"  />
            <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" HorizontalPadding="1px"
                NodeSpacing="0px" VerticalPadding="1px"  />
        </asp:TreeView>
    </div>
    </form>
</body>
</html>
