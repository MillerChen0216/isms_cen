﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_30150
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()

        SqlDataSource()

    End Sub
    Protected Sub SqlDataSource()
        SqlDataSource1.ConnectionString = Conn.ConnectionString
        SqlTxt = "SELECT totalrisk_lv,lv_scoreS,lv_scoreE,totalrisk_desc FROM totalRisklv Order by totalrisk_lv"
        SqlDataSource1.SelectCommand = SqlTxt
        GridView1.DataSourceID = SqlDataSource1.ID

    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)

        Select Case e.CommandName

            Case "Del_Btn"
                SqlTxt = "DELETE FROM [totalRisklv] where totalrisk_lv ='" + SelectedRow.Cells(0).Text + "' "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DEL", SelectedRow.Cells(0).Text + "-" + CType(SelectedRow.FindControl("lv_scoreS"), Label).Text + "~" + CType(SelectedRow.FindControl("lv_scoreE"), Label).Text)

                GridView1.DataBind()

            Case "Edit_Btn"

                Edit_TB.Visible = True
                Show_TB.Visible = False
                totalrisk_lv_upd.Text = SelectedRow.Cells(0).Text

                lv_scoreS_upd.Text = CType(Me.GridView1.Rows(Index).FindControl("lv_scoreS"), Label).Text
                lv_scoreE_upd.Text = CType(Me.GridView1.Rows(Index).FindControl("lv_scoreE"), Label).Text
                totalrisk_desc_upd.Text = SelectedRow.Cells(2).Text
        End Select
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        Dim oDataRow As Data.DataRowView
        Dim sShowField As String
        Dim sMessage As String
        If e.Row.RowIndex <> -1 Then

            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = oDataRow.Item("totalrisk_desc").ToString()
            sMessage = String.Format("您確定要刪除 [{0}] 嗎?", sShowField)
            CType(e.Row.Cells(4).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"
        End If
    End Sub

    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click
        If totalrisk_lv_upd.Text = "" Then
            Common.showMsg(Me.Page, "風險等級不允許空白")
        ElseIf lv_scoreS_upd.Text = "" Or lv_scoreE_upd.Text = "" Then
            Common.showMsg(Me.Page, "風險值不允許空白")
        Else

            SqlTxt = "UPDATE [totalRisklv]  SET  lv_scoreS =" + lv_scoreS_upd.Text + ",lv_scoreE=" + lv_scoreE_upd.Text + ",totalrisk_desc='" + totalrisk_desc_upd.Text + "' WHERE totalrisk_lv =" + totalrisk_lv_upd.Text
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", totalrisk_lv_upd.Text + "-" + lv_scoreS_upd.Text + "~" + lv_scoreE_upd.Text)

            GridView1.DataBind()
            Show_TB.Visible = True
            Edit_TB.Visible = False
        End If
    End Sub

    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        If totalrisk_lv_ins.Text = "" Then
            Common.showMsg(Me.Page, "風險等級不允許空白")
        ElseIf lv_scoreS_ins.Text = "" Or lv_scoreE_ins.Text = "" Then
            Common.showMsg(Me.Page, "風險值不允許空白")
        Else
            SqlTxt = "INSERT INTO [totalRisklv]  ([totalrisk_lv],[lv_scoreS],[lv_scoreE],[totalrisk_desc]) VALUES (" + totalrisk_lv_ins.Text + " , " + lv_scoreS_ins.Text + "," + lv_scoreE_ins.Text + ",'" + totalrisk_desc_ins.Text + "' ) "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", totalrisk_lv_ins.Text + "-" + lv_scoreS_ins.Text + "~" + lv_scoreE_ins.Text)

            GridView1.DataBind()

            Show_TB.Visible = True
            ins_TB.Visible = False

        End If
    End Sub

    Protected Sub Show_Insert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert.Click
        Show_TB.Visible = False
        ins_TB.Visible = True
    End Sub
End Class