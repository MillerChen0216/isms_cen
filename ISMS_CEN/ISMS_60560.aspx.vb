﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI

Partial Class ISMS_60560
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlCmd As SqlCommand
    Dim uid, uname, recordId As String
    Dim SqlTxt As String
    Dim approvalid As String
    Dim apprvid As String
    Dim apprvid2 As String
    Dim dp As String
    Dim dp2 As String = Get_DP()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uid = User.Identity.Name '使用者帳號 
        Dim nowpage() As String = Request.CurrentExecutionFilePath.Split("/")
        approvalid = Me.Get_approvalid(nowpage(nowpage.Length - 1), dp2)
        apprvid = Me.Get_apprvid(nowpage(nowpage.Length - 1), dp2)
        Gdv1Show()

    End Sub

    Protected Sub Gdv1Show()

        Dim dt As DataTable = Common.Qry_b_office_use_wfFormRecord(approvalid, uid)

        GridView1.DataSource = dt
        GridView1.DataBind()
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim recordid As String = CType(e.Row.FindControl("lb_rowid"), Label).Text
            CType(e.Row.FindControl("lb_flow"), Label).Text = Common.Str_form_record_isms(approvalid, recordid)
        End If
    End Sub
    Protected Sub Repeater1_OnItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
            'Reference the Repeater Item.
            Dim item As RepeaterItem = e.Item
            'Reference the Controls.
            Dim idtRepeater As Repeater = (TryCast(item.FindControl("ChildRepeater"), Repeater))
            Dim sysid As HiddenField = (TryCast(item.FindControl("hidden_sysid"), HiddenField))
            idtRepeater.DataSource = Common.Get_auth_sys_idt(sysid.Value)
            idtRepeater.DataBind()
        End If
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
        Dim lb_rowid As Label = (TryCast(SelectedRow.FindControl("lb_rowid"), Label))

        Select Case e.CommandName
            Case "audit_btn"

                Dim Ds, Ds1 As New DataSet

                Ds = Common.Get_b_office_apply_byrecordid(lb_rowid.Text)

                If Ds.Tables(0).Rows.Count > 0 Then
                    '1091119modify「申請原因」改取db<原固定寫為「年度檢討」>
                    Dim reason As String = Ds.Tables(0).Rows(0).Item("apply_reason").ToString.Trim
                    Dim reasonStr = ""

                    Select Case reason
                        Case "01"
                            reasonStr = "年度檢討"
                        Case "02"
                            reasonStr = "職務異動"
                        Case "04"
                            reasonStr = "離職"
                        Case "05"
                            reasonStr = "新進人員"
                        Case "03"
                            reasonStr = Ds.Tables(0).Rows(0).Item("apply_reason_other").ToString
                    End Select
                    lbl_reason.Text = reasonStr

                    lb_recordid.Text = Ds.Tables(0).Rows(0).Item("recordid").ToString
                    lb_id.Text = Ds.Tables(0).Rows(0).Item("user_id").ToString
                    lb_name.Text = Ds.Tables(0).Rows(0).Item("user_name").ToString
                    lbl_dept_name.Text = Ds.Tables(0).Rows(0).Item("dept_name").ToString
                    lbl_user_name.Text = Ds.Tables(0).Rows(0).Item("user_name").ToString
                    lbl_ext.Text = Ds.Tables(0).Rows(0).Item("ext").ToString
                    lbl_degree_name.Text = Ds.Tables(0).Rows(0).Item("degree_name").ToString
                    lbl_item.Text = Ds.Tables(0).Rows(0).Item("item")
                    Dim apply_date As String = Ds.Tables(0).Rows(0).Item("apply_date").ToString
                    If apply_date.Length = 7 Then
                        lbl_apply_date.Text = apply_date.Substring(0, 3) + "年" + apply_date.Substring(3, 2) + "月" + apply_date.Substring(5, 2) + "日"
                    Else
                        lbl_apply_date.Text = apply_date
                    End If

                    Dim enable_date As String = Ds.Tables(0).Rows(0).Item("enable_date").ToString
                    If enable_date.Length = 7 Then
                        lbl_enable_date.Text = enable_date.Substring(0, 3) + "年" + enable_date.Substring(3, 2) + "月" + enable_date.Substring(5, 2) + "日"
                    Else
                        lbl_enable_date.Text = enable_date
                    End If
                    Dim start_date As String = Ds.Tables(0).Rows(0).Item("start_date").ToString
                    If start_date.Length = 7 Then
                        lbl_start_date.Text = start_date.Substring(0, 3) + "年" + start_date.Substring(3, 2) + "月" + start_date.Substring(5, 2) + "日"
                    Else
                        lbl_start_date.Text = start_date
                    End If

                    Dim end_date As String = Ds.Tables(0).Rows(0).Item("end_date").ToString
                    If end_date.Length = 7 Then
                        lbl_end_date.Text = end_date.Substring(0, 3) + "年" + end_date.Substring(3, 2) + "月" + end_date.Substring(5, 2) + "日"
                    Else
                        lbl_end_date.Text = end_date
                    End If

                    Dim disable_date As String = Ds.Tables(0).Rows(0).Item("disable_date").ToString
                    If disable_date.Length = 7 Then
                        lbl_disable_date.Text = disable_date.Substring(0, 3) + "年" + disable_date.Substring(3, 2) + "月" + disable_date.Substring(5, 2) + "日"
                    Else
                        lbl_disable_date.Text = disable_date
                    End If
                    lbl_user_id.Text = Ds.Tables(0).Rows(0).Item("user_id").ToString
                    dp = Ds.Tables(0).Rows(0).Item("dp").ToString
                    apprvid2 = Common.Get_apprv_id(approvalid, dp)
                    lbl_disable_reason.Text = Ds.Tables(0).Rows(0).Item("disable_reason").ToString

                    bindRepeater()
                    bindNote()
                    For Each item As Control In Repeater1.Controls
                        Dim idt_repeater As Repeater = DirectCast(item.FindControl("ChildRepeater"), Repeater)
                        Dim hid_sys_id As HiddenField = DirectCast(item.FindControl("hidden_sysid"), HiddenField)
                        Dim hidden_idtname As HiddenField = DirectCast(item.FindControl("hidden_idtname"), HiddenField)
                        Dim sys_rmk As Label = DirectCast(item.FindControl("lbl_sys_rmk"), Label)
                        Dim list_idt As List(Of String) = New List(Of String)

                        Ds1 = Common.Get_b_office_apply_dtl_byUser(Ds.Tables(0).Rows(0).Item("b_office_apply_id").ToString, hid_sys_id.Value)
                        If Ds1.Tables(0).Rows.Count > 0 Then
                            sys_rmk.Text = Ds1.Tables(0).Rows(0).Item("rmk").ToString
                            For Each item2 As Control In idt_repeater.Controls
                                Dim chk_idt_name As CheckBox = DirectCast(item2.FindControl("chk_idt_name"), CheckBox)
                                Dim find_rows As DataRow() = Ds1.Tables(0).Select("idt_name like '%" + chk_idt_name.Text.Trim + "%'")
                                If find_rows.Length > 0 Then
                                    chk_idt_name.Checked = True
                                    list_idt.Add("," + chk_idt_name.Text)
                                End If
                            Next

                        End If
                        hidden_idtname.Value = String.Join("", list_idt.ToArray())
                    Next

                End If
                GdvShow()
                tb_result.Visible = True
                tr_gridview.Visible = False
                Insert_Btn.Visible = True
                but_esc.Visible = True
        End Select
    End Sub

    Private Sub GdvShow()
        Dim dt As DataTable = Common.Qry_workflowName_isms_Req(approvalid, lb_recordid.Text, apprvid2, dp)
        GridView.DataSource = dt
        GridView.DataBind()

    End Sub

    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料

            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            Dim hid_status As HiddenField = CType(e.Row.FindControl("hid_status"), HiddenField)
            '***意見
            Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text
            '***審核人員***
            'Dim lb_wf_inside_order As String = CType(e.Row.FindControl("lb_wf_inside_order"), Label).Text

            Dim ds As DataSet

            '***Start
            If o_wf_order = "0" And hid_status.Value <> "1" Then
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = lb_name.Text
                CType(e.Row.FindControl("label_userid"), Label).Text = lb_id.Text
            ElseIf hid_status.Value = "1" Then '待審核
                ds = Common.Qry_form_record_wait(lb_recordid.Text)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
                lb_count.Text = e.Row.RowIndex
            ElseIf hid_status.Value = "2" Or hid_status.Value = "9" Then '審核已通過
                ds = Common.Qry_form_record_name(lb_recordid.Text, o_wf_order)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
            ElseIf e.Row.RowIndex = CType(lb_count.Text, Integer) + 1 Then '待審核下一關卡
                ddl_user.Visible = True
                ds = Common.Get_Wf_User(dp, apprvid2, o_wf_order, "")
                ddl_user.DataSource = ds
                ddl_user.DataTextField = "name_id"
                ddl_user.DataValueField = "wf_userid"
                ddl_user.DataBind()
            End If

            If hid_status.Value = "1" Then '待審核
                ddl_status.Visible = True
                lb_status.Visible = False
                txt_comment.Visible = True
                lb_comment.Visible = False
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(1)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()

            ElseIf hid_status.Value = "" Then
                ddl_status.Visible = False
                lb_status.Visible = True
                txt_comment.Visible = False
                lb_comment.Visible = True
            Else
                ddl_status.Visible = False
                lb_status.Visible = True
                txt_comment.Visible = False
                lb_comment.Visible = True
                CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(hid_status.Value))
            End If
        End If
    End Sub

    Protected Sub bindRepeater()

        Repeater1.DataSource = Common.Get_auth_sys(approvalid, dp)
        Repeater1.DataBind()

    End Sub

    Protected Sub bindNote()
        Dim Ds As New DataSet
        Ds = Common.Get_auth_sys_note(approvalid, dp)
        If Ds.Tables(0).Rows.Count > 0 Then
            note_Edit.Text = Ds.Tables(0).Rows(0).Item("note")
        End If
    End Sub
    Protected Sub but_esc_click()
        tr_gridview.Visible = True
        tb_result.Visible = False
        Insert_Btn.Visible = False
        but_esc.Visible = False
        Gdv1Show()
    End Sub

    Protected Sub Insert_Btn_click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim index As Integer = CType(lb_count.Text, Integer)
        Dim max_index As Integer = GridView.Rows.Count

        Dim wf_order As String = CType(GridView.Rows(index).FindControl("wf_order"), Label).Text '待審核的關卡
        Dim status As String = CType(GridView.Rows(index).FindControl("ddl_status"), DropDownList).SelectedValue '狀態
        Dim comment As String = CType(GridView.Rows(index).FindControl("txb_comment"), TextBox).Text
        Dim id As String = CType(GridView.Rows(index).FindControl("lb_id2"), Label).Text

        If status = 2 Then '通過
            If index + 1 < GridView.Rows.Count Then '流程尚未到最後關卡
                Dim workflowid As String = CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) + 1
                Dim user_id = CType(GridView.Rows(index + 1).FindControl("ddl_user"), DropDownList).SelectedValue

                SqlTxt = " INSERT INTO form_record (approvalId,workflowId,userId,userType,status,comment,recordId,signDate,role,dp) VALUES "
                SqlTxt += " ('" + approvalid + "','" + workflowid + "','" + user_id + "','land','1','','" + lb_recordid.Text + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + workflowid + "','" + Common.Get_DP2(user_id) + "') "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            End If
            SqlTxt = " UPDATE form_record SET status='2' , signDate ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' , comment = '" + comment + "' WHERE status != 2 and status !=0 and approvalId='" + approvalid + "'and recordId ='" + lb_recordid.Text + "' and id ='" + id + "' "

        ElseIf status = 9 Then '退回
            If CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) > 0 Then '目前流程不是第一關卡
                Dim workflowid As String = CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) - 1
                'Dim user_id = CType(GridView.Rows(index - 1).FindControl("label_userid"), Label).Text
                Dim dt As DataTable = Common.Qry_form_record1(approvalid, lb_recordid.Text, workflowid)
                Dim user_id As String = dt.Rows(0).Item("userId").ToString

                SqlTxt = " INSERT INTO form_record (approvalId,workflowId,userId,userType,status,comment,recordId,signDate,role,dp) VALUES "
                SqlTxt += " ('" + approvalid + "','" + workflowid + "','" + user_id + "','land','1','','" + lb_recordid.Text + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + workflowid + "','" + Common.Get_DP2(user_id) + "') "

                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            Else
                Common.showMsg(Me.Page, "流程已到填表人，無法再退回！")
                Return
            End If

            SqlTxt = " UPDATE form_record SET status='9' , signDate ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' , comment = '" + comment + "' WHERE approvalId='" + approvalid + "'and recordId ='" + lb_recordid.Text + "' and id ='" + id + "' "

        End If
        Dim cmd As New SqlCommand(SqlTxt.ToString)

        Try
            Conn = Common.ConnDBS : Conn.Open() : cmd.Connection = Conn
            If cmd.ExecuteNonQuery() Then Common.showMsg(Me.Page, "審核完成！") : but_esc_click()
        Catch ex As Exception
            Common.showMsg(Me.Page, "審核失敗！")
        Finally
            Conn.Close() : Conn.Close() : Conn.Dispose() : cmd.Dispose()
        End Try
    End Sub
End Class