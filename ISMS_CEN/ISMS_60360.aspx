﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_60360.aspx.vb" Inherits="ISMS_60360" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>地政局系統使用者帳號申請表</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
            EnableScriptLocalization="True">
        </asp:ScriptManager>
        <table border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2" width="850px">
            <tr>
                <td>
                    <img src="Images/exe.gif" />地政局系統使用者帳號申請表 | 待審核
                    <asp:Label ID="lb_msg" runat="server" ForeColor="red" EnableViewState="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="tb_result" runat="server" border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2"
                        style="width: 100%" visible="false">
                        <tr>
                            <td colspan="4" class="tb_title_w_1">
                                桃園市政府地政局資訊系統使用者帳號申請表</td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1" style="width: 110px;">
                                單位</td>
                            <td>
                                <asp:Label ID="lbl_dept_name" runat="server" Text="桃園市政府地政局"></asp:Label></td>
                            <td class="tb_title_w_1" style="width: 110px;"> 申請原因</td>
                            <td><asp:Label ID="lbl_reason" runat="server" ></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">
                                使用者姓名</td>
                            <td><asp:Label ID="lbl_user_name" runat="server" ></asp:Label></td>
                            <td class="tb_title_w_1">
                                分機</td>
                            <td>
                                <asp:Label ID="lbl_ext" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">
                                職稱</td>
                            <td colspan="3">
                                <asp:Label ID="lbl_degree_name" runat="server" Text="承辦人員"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">
                                業務項目</td>
                            <td colspan="3">
                                <asp:Label ID="lbl_item" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">
                                申請日期</td>
                            <td>
                                <asp:Label ID="lbl_apply_date" runat="server" ></asp:Label></td>
                            <td class="tb_title_w_1">
                                啟用日期</td>
                            <td>
                                <asp:Label ID="lbl_enable_date" runat="server" ></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">
                                使用期間</td>
                            <td colspan="3">
                                <asp:Label ID="lbl_start_date" runat="server" ></asp:Label>至
                                <asp:Label ID="lbl_end_date" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">
                                使用者代碼</td>
                            <td>
                                <asp:Label ID="lbl_user_id" runat="server" Text=""></asp:Label></td>
                            <td class="tb_title_w_1">
                                使用者密碼</td>
                            <td> 預設值 </td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">
                                保密切結</td>
                            <td colspan="3">
                                本人___________於業務權限範圍內，將確實依本系統使用作業規定，查詢或列印相關資料，並遵守個人資料保護法、智慧財產權、著作權等相關規定，如有不當使用、資料外洩或違法者，應依法負民、刑事責任及行政責任。</td>
                        </tr>
                        <tr>
                            <td class="tb_title_w_1">
                                註銷(停用)日期</td>
                            <td>
                                <asp:Label ID="lbl_disable_date" runat="server" Text=""></asp:Label></td>
                            <td class="tb_title_w_1">
                                註銷(停用)原因</td>
                            <td>
                                <asp:Label ID="lbl_disable_reason" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="4" class="tb_title_w_1">
                                填表說明</td>
                        </tr>
                        <tr>
                            <td colspan="4" style="height: 109px">
                                <asp:Label ID="note_Edit" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="tb_title_w_1">
                                附表1：一般系統帳號申請表</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2" style="width: 100%">
                                    <tr>
                                        <td class="tb_title_w_1" style="width: 30%;">
                                            授權範圍</td>
                                        <td class="tb_title_w_1" style="width: 40%;">
                                            權限</td>
                                        <td class="tb_title_w_1" style="width: 30%;">
                                            備註</td>
                                    </tr>
                                    <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_OnItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbl_sys_name" Enabled="false" Text='<%#Eval("sys_name") %>' runat="server"></asp:Label>
                                                    <asp:HiddenField ID="hidden_sysid" runat="server" Value='<%#Eval("sys_id") %>' />
                                                    <asp:HiddenField ID="hidden_idtname" runat="server" />
                                                </td>
                                                <td>
                                                    <!-- Repeated data -->
                                                    <asp:Repeater ID="ChildRepeater" runat="server">
                                                        <ItemTemplate>
                                                            <!-- Nested repeated data -->
                                                            <asp:CheckBox ID="chk_idt_name" runat="server" Text='<%#Eval("idt_name") %>' Enabled="false" />
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbl_sys_rmk" Enabled="false" Text='<%#Eval("sys_rmk") %>' runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                                            <tr>
          <td colspan="4" align="center" ><%--<uc1:FlowControl ID="FlowControl1" runat="server"  />--%>
                    <asp:UpdatePanel ID ="UpdatePanel3" runat="server" >
            <ContentTemplate> 
                <asp:Label ID="lb_recordid"  runat="server" Visible="false" Text="0"></asp:Label>
              <asp:Label ID="lb_count"  runat="server" Visible="false" Text="999"></asp:Label>
              <asp:Label ID="lb_id"  runat="server" Visible="false" Text="99"></asp:Label>        
              <asp:Label ID="lb_name"  runat="server" Visible="false" Text="99"></asp:Label>
         <asp:GridView ID="GridView" runat="server" AutoGenerateColumns="False" CellPadding="3"    CssClass="tb_1" Width="100%"  >
                        <Columns>
                           <asp:TemplateField HeaderText="關卡" ItemStyle-CssClass="tb_w_1">
                                 <ItemTemplate>
                                    <asp:Label ID="wf_order" runat="server" Text='<%# eval("wf_order") %>'></asp:Label>
                                    <asp:Label ID="lb_id2"  runat="server" Visible="false" Text='<%# Eval("id")%>'></asp:Label>
                                 </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField Headertext="審核身份" ItemStyle-CssClass="tb_w_1">
                                  <ItemTemplate>
                                    <asp:Label ID="chk_name" runat="server" Text='<%# eval("chk_name") %>' ></asp:Label>
                                 </ItemTemplate>
                           </asp:TemplateField>                        
                           <asp:TemplateField HeaderText="審核人員" ItemStyle-CssClass="tb_w_1">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddl_user" runat="server" Visible="false"  ></asp:DropDownList>
                                                          
                                    <asp:Panel ID="pan_user" runat="server" Visible="false">
                                    <asp:Label ID="lb_username" runat="server"></asp:Label>
                                    (<asp:Label ID="label_userid" runat="server"></asp:Label>)
                                    </asp:Panel>                         
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="狀態" ItemStyle-CssClass="tb_w_1">
                               <ItemTemplate>
                                    <asp:DropDownList ID="ddl_status" runat="server"  Visible="False"></asp:DropDownList>
                                    <asp:Label ID="lb_status" runat="server"  Visible="False"></asp:Label>
                                   <asp:HiddenField ID="hid_status" runat="server" Value='<%#Eval("wf_status_id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="意見" ItemStyle-CssClass="tb_w_1">
                                <ItemTemplate>
                                    <asp:TextBox ID="txb_comment" runat="server" Text='<%# Eval("wf_comment")%>' Visible="False" Width="150px"></asp:TextBox>
                                    <asp:Label ID="lb_comment" runat="server" Text='<%# eval("wf_comment") %>' Visible="False"></asp:Label>                                 
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                       <HeaderStyle CssClass="tb_title_w_1" />
                    </asp:GridView> 
                  <%--  <asp:SqlDataSource ID="WF_DS" runat="server"></asp:SqlDataSource>--%>
                  </ContentTemplate>
          </asp:UpdatePanel>                   
                     <asp:Button ID="Insert_Btn" runat="server" Text="送出審核" OnClick="Insert_Btn_click" Visible="false"  />
                     <asp:Button id="but_esc" runat="server" Text="回列表"  OnClick="but_esc_click"  Visible="false"/>
          </td>
         </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr_gridview" runat="server">
               <td>
                   <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
                  <asp:GridView ID="GridView1" runat="server" CssClass="tb_1" AutoGenerateColumns="false" Width="850px">
             <Columns>
                <asp:TemplateField HeaderText="申請日期">
                <ItemTemplate>
                  <asp:Label runat="server" ID="lb_applydate" Text='<%# Format(Eval("signDate"), "yyyy/MM/dd")%>'></asp:Label> 
                  <asp:Label runat="server" ID="lb_rowid" Text='<%# eval("recordid") %>' Visible="false" ></asp:Label> 
                  <%--<asp:Label runat="server" ID="lb_douser" Text='<%# eval("douser") %>' Visible="false"></asp:Label>--%>
                </ItemTemplate>
              </asp:TemplateField>
              <%--<asp:TemplateField HeaderText="專案名稱"><ItemTemplate><asp:Label runat="server" ID="lb_assetMidName" Text='<%# eval("assetMidName") %>' ></asp:Label> </ItemTemplate></asp:TemplateField> --%>
                   <asp:TemplateField HeaderText="簽核過程">
                <ItemTemplate>
                   <asp:Label runat="server" ID="lb_flow"  ></asp:Label> 
                </ItemTemplate>
              </asp:TemplateField>                
                  <asp:ButtonField ButtonType="Button" CommandName="audit_btn" Text="審核" HeaderText="審核">
                        <ItemStyle CssClass="tb_w_1" />
                  </asp:ButtonField>
              </Columns>
                <HeaderStyle CssClass="tb_title_w_1" />
                <EmptyDataTemplate>…尚無資料…</EmptyDataTemplate>
        </asp:GridView>
               </td>
            </tr>
        </table>
    </form>
</body>
</html>