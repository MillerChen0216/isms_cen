﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class ISMS_11173
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim Ds As New DataSet


    Protected Sub Doc1_SqlDS()
        Doc1_DS.ConnectionString = Common.ConnDBS.ConnectionString
        If ddl_type1.SelectedIndex <> 0 And ddl_type2.SelectedIndex <> 0 Then
            Doc1_DS.SelectCommand = "SELECT * FROM docResoure where doc_type='13' and latest='YES'   and dp='" + Hidden_DP.Value + "' and doctype1='" + ddl_type1.SelectedValue + "' and doctype2='" + ddl_type2.SelectedValue + "'"

        Else
            Doc1_DS.SelectCommand = "SELECT * FROM docResoure where doc_type='13' and latest='YES'   and dp='" + Hidden_DP.Value + "'"

        End If
        GridView1.DataSourceID = Doc1_DS.ID
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP

        If Not Page.IsPostBack Then

            Common.ddl_DataRead("SELECT * FROM docType1 where dp='" + Hidden_DP.Value + "'  order by id", ddl_type1, 2, 1)
            ddl_type2.Enabled = False

        ElseIf ddl_type1.SelectedIndex = 0 Then
            ddl_type2.Enabled = False
        End If

        Doc1_SqlDS()
        LB_DocType.Text = Common.ISMS_DocNameChange("13")
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(5).Visible = False
        e.Row.Cells(3).Visible = False
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex

        Try
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", GridView1.Rows(Index).Cells(5).Text, System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "FILEDOWN", GridView1.Rows(Index).Cells(2).Text)
            Ds = Common.Get_Doc_info(GridView1.Rows(Index).Cells(5).Text)
            Response.ContentType = "application/octet-stream"
            Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5")
            Response.AddHeader("content-disposition", "attachment;filename=" + Common.Decode((Ds.Tables(0).Rows(0).Item("doc_filename"))) + "")
            Response.WriteFile(Common.Sys_Config("doc_Path") + "/" + ("" + (Ds.Tables(0).Rows(0).Item("doc_filename")) + ""))
            Response.End()
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try


    End Sub


    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowIndex <> -1 Then
            Dim ds As New DataSet
            ds = Common.Get_issueDoc_info(e.Row.Cells(5).Text)
            Dim wDate As Date
            wDate = ds.Tables(0).Rows(0).Item("issue_date")

            If wDate.AddDays(Common.Sys_Config("News_Day")) >= Now.Date Then
                CType(e.Row.FindControl("Image1"), Image).Visible = True
            Else
                CType(e.Row.FindControl("Image1"), Image).Visible = False
            End If
        End If

    End Sub


    Protected Sub ddl_type1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_type1.SelectedIndexChanged
        ddl_type2.Items.Clear()
        ddl_type2.Items.Add(New ListItem("請選擇子類別", 0))

        If ddl_type1.SelectedIndex = 0 Then
            ddl_type2.Enabled = False

        Else
            ddl_type2.Enabled = True
            Common.ddl_DataRead("SELECT   *  FROM   docType2  where pid=" + ddl_type1.SelectedValue + "", ddl_type2, 2, 1)

        End If
    End Sub


End Class
