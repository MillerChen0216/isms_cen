﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_31160.aspx.vb" Inherits="ISMS_31160" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <asp:HiddenField ID="Hidden_DP" runat="server" />
            &nbsp;</div>
        <span style="font-size: 12px; color: #2666a6; font-family: Arial; background-color: #b8e1f1">
        </span>
        <div>
            <table id="Main_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 750px"
                visible="true">
                <tr>
                    <td class="tb_title_w_2" colspan="6">
                        <img src="Images/exe.gif" />
                        風險評鑑審核作業 | 列表&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
                            CssClass="tb_1" Width="750px">
                            <Columns>
                                <asp:BoundField DataField="asset_type" HeaderText="資產類別">
                                    <HeaderStyle CssClass="tb_title_w_1" />
                                    <ItemStyle CssClass="tb_w_1" />
                                </asp:BoundField>
                                <asp:BoundField DataField="riskEvtab_name" HeaderText="風險評鑑表">
                                    <HeaderStyle CssClass="tb_title_w_1" />
                                    <ItemStyle CssClass="tb_w_1" />
                                </asp:BoundField>
                                <asp:BoundField DataField="issue_date" HeaderText="日期">
                                    <HeaderStyle CssClass="tb_title_w_1" />
                                    <ItemStyle CssClass="tb_w_1" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="瀏覽">
                                    <ItemTemplate>
                                        <asp:Button ID="audit_btn" runat="server" Text="瀏覽" OnClick="audit_btn_Click" />
                                        &nbsp;
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="tb_title_w_1" />
                                    <ItemStyle CssClass="tb_w_1" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="form_id" HeaderText="編號">
                                    <HeaderStyle CssClass="tb_title_w_1" />
                                    <ItemStyle CssClass="tb_w_1" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    
    </div>
        <asp:SqlDataSource ID="Flow_DS" runat="server"></asp:SqlDataSource>
        <asp:SqlDataSource ID="RiskForm_DS" runat="server"></asp:SqlDataSource>
        <asp:SqlDataSource ID="WF_DS" runat="server"></asp:SqlDataSource>
        <asp:HiddenField ID="Hidden_Formid" runat="server" Visible="False" />
        <table id="tb1" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 770px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    風險評鑑填表 |
                    <asp:Label ID="lb_riskname" runat="server"></asp:Label>&nbsp;<asp:Button ID="Output_XLS"
                        runat="server" Text="另存Excel檔案" /></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="770px">
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
