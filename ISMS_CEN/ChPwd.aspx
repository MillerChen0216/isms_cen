﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ChPwd.aspx.vb" Inherits="ChPwd" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <br />
        <table id="pwd_tb" runat="server" cellpadding="3" cellspacing="0" class="tb_1" style="width: 300px"
            visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="3">
                    <img src="Images/exe.GIF" />
                    修改密碼
                    <asp:Button ID="Button1" runat="server" CssClass="button-small" Text="確定修改" />
                </td>
            </tr>
            <tr>
                <td class="tb_title_w_1">
                    帳號</td>
                <td class="tb_title_w_1">
                    密碼</td>
                <td class="tb_title_w_1">
                    確認密碼</td>
            </tr>
            <tr>
                <td class="tb_w_1">
                    <asp:TextBox ID="useridtxt" runat="server" BackColor="WhiteSmoke" ReadOnly="True"
                        Width="70px"></asp:TextBox></td>
                <td class="tb_w_1">
                    <asp:TextBox ID="pwdtxt1" runat="server" TextMode="Password" Width="100px"></asp:TextBox>
                </td>
                <td class="tb_w_1">
                    <asp:TextBox ID="pwdtxt2" runat="server" TextMode="Password" Width="100px"></asp:TextBox></td>
            </tr>
        </table>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    
    </div>
        <asp:RegularExpressionValidator ID="RVL_PASSWORD1" runat="server" ControlToValidate="pwdtxt1"
            Display="None" ErrorMessage="密碼至少8碼含英數字" ValidationExpression="^.*(?=.{8,})(?=.*\d)(?=.*[a-zA-Z]).*$"></asp:RegularExpressionValidator>
        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="pwdtxt1"
            ControlToValidate="pwdtxt2" Display="None" ErrorMessage="密碼不同請再確認"></asp:CompareValidator>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" TargetControlID="RVL_PASSWORD1">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="server" TargetControlID="CompareValidator1">
        </cc1:ValidatorCalloutExtender>
    </form>
</body>
</html>
