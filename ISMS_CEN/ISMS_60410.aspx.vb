﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_60410
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim dp As String = Get_DP()
    Dim approvalid As String

    Protected Sub Show_Insert_auth_sys_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert_auth_sys.Click
        Show_auth_sys_idt.Visible = False
        Edit_auth_sys_idt.Visible = False
        Show_auth_sys.Visible = False
        Edit_auth_sys.Visible = False
        Ins_auth_sys.Visible = True
        sys_Name_Ins.Text = ""
    End Sub

    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click
        If sys_Name_Edit.Text = "" Then
            Common.showMsg(Me.Page, "授權欄位不允許空白")
        Else
            SqlTxt = "UPDATE auth_sys  SET  sys_name ='" + sys_Name_Edit.Text + "',sys_rmk ='" + sys_Rmk_Edit.Text + "' where sys_id = '" + hidden_sysid.Value + "'"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", sys_Name_Edit.Text)

            GridView1.DataBind()
            Show_auth_sys.Visible = True
            Edit_auth_sys.Visible = False
            Show_auth_sys_idt.Visible = False
            Edit_auth_sys_idt.Visible = False
        End If
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
        hidden_sysid.Value = DirectCast(SelectedRow.FindControl("hid_sys_id"), HiddenField).Value
        hidden_sysname.Value = DirectCast(SelectedRow.FindControl("lb_sys_name"), Label).Text


        Select Case e.CommandName

            Case "Del_Btn"

                SqlTxt = "DELETE FROM auth_sys where sys_id ='" + hidden_sysid.Value + "' "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DEL", hidden_sysname.Value)

                GridView1.DataBind()

            Case "Edit_Btn"

                Edit_auth_sys.Visible = True
                Show_auth_sys.Visible = False
                Show_auth_sys_idt.Visible = False
                Edit_auth_sys_idt.Visible = False
                sys_Name_Edit.Text = hidden_sysname.Value
                sys_Rmk_Edit.Text = DirectCast(SelectedRow.FindControl("hid_sys_rmk"), HiddenField).Value
            Case "ViewIdt_Btn"

                Edit_auth_sys.Visible = False
                Show_auth_sys.Visible = False
                Show_auth_sys_idt.Visible = True
                Edit_auth_sys_idt.Visible = False
                sys_Name_show.Text = hidden_sysname.Value

                bindGridView2()
        End Select
    End Sub


    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim oDataRow As Data.DataRowView
        Dim sShowField As String
        Dim sMessage As String
        If e.Row.RowIndex <> -1 Then
            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = oDataRow.Item("sys_name").ToString()
            sMessage = String.Format("您確定要刪除 [{0}] 嗎?", sShowField)
            'CType(e.Row.FindControl("Button2"), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"
            Dim radio_using As RadioButtonList = CType(e.Row.FindControl("rdo_able"), RadioButtonList)
            radio_using.SelectedValue = Common.notNull(CType(e.Row.FindControl("hid_using"), HiddenField).Value)
        End If
    End Sub

    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        If sys_Name_Ins.Text = "" Then
            Common.showMsg(Me.Page, "授權範圍不允許空白")
        Else
            SqlTxt = "INSERT INTO auth_sys ([auth_type],[sys_name],[sys_rmk],[dp],[using]) VALUES ('" + approvalid + "','" + sys_Name_Ins.Text + "','" + sys_Rmk_Ins.Text + "','" + dp + "','Y') "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", sys_Name_Ins.Text)

            GridView1.DataBind()
            Show_auth_sys.Visible = True
            Ins_auth_sys.Visible = False
            Show_auth_sys_idt.Visible = False
            Edit_auth_sys_idt.Visible = False

        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Dim nowpage() As String = Request.CurrentExecutionFilePath.Split("/")
        approvalid = Me.Get_approvalid(nowpage(nowpage.Length - 1), dp)
        SqlDataSource()
        If Not IsPostBack Then
            bindNote()
        End If
    End Sub
    Protected Sub SqlDataSource()

        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = "SELECT * FROM auth_sys where auth_type='" + approvalid + "' and dp = '" + dp + "'and sys_name like '%" + sys_Name_Qry.Text + "%'"

        GridView1.DataSourceID = SqlDataSource1.ID

    End Sub

    Protected Sub bindGridView2()


        SqlDataSource2.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource2.SelectCommand = "SELECT * FROM  auth_sys_idt where sys_id='" + hidden_sysid.Value + "'"

        GridView2.DataSourceID = SqlDataSource2.ID

        GridView2.DataBind()
    End Sub

    Protected Sub bindNote()
        Dim Ds As New DataSet
        Ds = Common.Get_auth_sys_note(approvalid, dp)
        If Ds.Tables(0).Rows.Count > 0 Then
            note_Edit.Text = Ds.Tables(0).Rows(0).Item("note").Replace("<br/>", System.Environment.NewLine)
        End If
    End Sub


    Protected Sub Show_Insert_auth_sys_idt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert_auth_sys_idt.Click
        Show_auth_sys_idt.Visible = False
        Edit_auth_sys_idt.Visible = False
        Show_auth_sys.Visible = False
        Edit_auth_sys.Visible = False
        Ins_auth_sys.Visible = False
        Ins_auth_sys_idt.Visible = True
        sys_Name.Text = hidden_sysname.Value '授權範圍
        idt_Name_Ins.Text = ""


    End Sub

    Protected Sub InsertIdt_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles InsertIdt_Btn.Click
        If idt_Name_Ins.Text = "" Then
            Common.showMsg(Me.Page, "權限不允許空白")
        Else
            SqlTxt = "INSERT INTO auth_sys_idt ([idt_name],[sys_id]) VALUES ('" + idt_Name_Ins.Text + "','" + hidden_sysid.Value + "') "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", idt_Name_Ins.Text)

            Show_auth_sys.Visible = False
            Ins_auth_sys.Visible = False
            Show_auth_sys_idt.Visible = True
            Edit_auth_sys_idt.Visible = False
            Ins_auth_sys_idt.Visible = False
            bindGridView2()
        End If
    End Sub

    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView2.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView2.Rows(Index)
        hidden_idtid.Value = DirectCast(SelectedRow.FindControl("hid_idt_id"), HiddenField).Value
        Select Case e.CommandName

            Case "Del_Btn"

                SqlTxt = "DELETE FROM dbo.auth_sys_idt where sys_id ='" + hidden_sysid.Value + "' and idt_id ='" + hidden_idtid.Value + "' "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DEL", DirectCast(SelectedRow.FindControl("lb_idt_name"), Label).Text)

                GridView2.DataBind()

            Case "Edit_Btn"

                Edit_auth_sys.Visible = False
                Show_auth_sys.Visible = False
                Show_auth_sys_idt.Visible = False
                Edit_auth_sys_idt.Visible = True
                sys_Name_view.Text = hidden_sysname.Value
                idt_Name_Edit.Text = DirectCast(SelectedRow.FindControl("lb_idt_name"), Label).Text


        End Select
    End Sub

    Protected Sub UpdateIdt_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpdateIdt_Btn.Click
        If idt_Name_Edit.Text = "" Then
            Common.showMsg(Me.Page, "權限不允許空白")
        Else
            SqlTxt = "UPDATE auth_sys_idt  SET  idt_name ='" + idt_Name_Edit.Text + "' where sys_id = '" + hidden_sysid.Value + "' and idt_id ='" + hidden_idtid.Value + "' "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", idt_Name_Edit.Text)

            GridView2.DataBind()
            Show_auth_sys.Visible = False
            Edit_auth_sys.Visible = False
            Show_auth_sys_idt.Visible = True
            Edit_auth_sys_idt.Visible = False
            bindGridView2()
        End If

    End Sub

    Protected Sub Query_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Query_Btn.Click
        GridView1.DataBind()
    End Sub

    Protected Sub EditNote_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles EditNote_Btn.Click
        Dim Ds As New DataSet
        Ds = Common.Get_auth_sys_note(approvalid, dp)
        If Ds.Tables(0).Rows.Count > 0 Then
            SqlTxt = "UPDATE auth_sys_note  SET  note ='" + note_Edit.Text.Replace(System.Environment.NewLine, "<br/>") + "' where auth_type = '" + approvalid + "' and dp = '" + dp + "'"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
        Else
            SqlTxt = "INSERT INTO auth_sys_note ([auth_type],[note],[dp]) VALUES ('" + approvalid + "','" + note_Edit.Text.Replace(System.Environment.NewLine, "<br/>") + "','" + dp + "') "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
        End If
        Common.showMsg(Me.Page, "填表說明儲存成功！")

    End Sub
    Protected Sub rdo_ableSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim rdo As RadioButtonList = CType(sender, RadioButtonList)
        Dim row As GridViewRow = CType(rdo.NamingContainer, GridViewRow)
        Dim rowindex As Integer = CType(row, GridViewRow).RowIndex
        Dim SelectedRow As GridViewRow = GridView1.Rows(rowindex)
        Dim sys_id As String = DirectCast(SelectedRow.FindControl("hid_sys_id"), HiddenField).Value
        Dim Ds As New DataSet
        Dim sql As String

        If rdo.SelectedValue = "Y" Then '啟用
            sql = " UPDATE auth_sys SET using = 'Y' WHERE sys_id ='" + sys_id + "' AND dp ='" + dp + "' AND auth_type ='" + approvalid + "' "
            Dim mycmd As SqlCommand = New SqlCommand(sql, Conn)
            Conn.Open()
            mycmd.ExecuteNonQuery()
            Conn.Close()
            Common.showMsg(Me.Page, "權限啟用成功!")
        Else '停用
            Ds = Common.Qry_Auth_using4(dp, sys_id)
            If Ds.Tables(0).Rows.Count > 0 Then
                Common.showMsg(Me.Page, "已授權此權限給使用者，所以無法停用此授權!")
                rdo.SelectedValue = "N"
            Else
                sql = " UPDATE auth_sys SET using = 'N' WHERE sys_id ='" + sys_id + "' AND dp ='" + dp + "' AND auth_type ='" + approvalid + "' "
                Dim mycmd As SqlCommand = New SqlCommand(sql, Conn)
                Conn.Open()
                mycmd.ExecuteNonQuery()
                Conn.Close()
                Common.showMsg(Me.Page, "權限停用成功!")
            End If

        End If

    End Sub
End Class
