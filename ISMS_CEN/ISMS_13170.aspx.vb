﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class ISMS_13170
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim oRow As GridViewRow
    Dim Index As Integer

    Protected Sub Doc1_SqlDS()
        Doc1_DS.ConnectionString = Common.ConnDBS.ConnectionString
        Doc1_DS.SelectCommand = "SELECT * FROM ClassTraining_list where dp='" + Hidden_DP.Value + "' and class_enable=1"
        GridView1.DataSourceID = Doc1_DS.ID
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        Doc1_SqlDS()
        LB_List.Text = Common.ISMS_DocNameChange("7")
        ddl_user_ins.DataSource = Common.Get_User(Hidden_DP.Value)
        ddl_user_ins.DataTextField = "user_name"
        ddl_user_ins.DataValueField = "user_id"
        ddl_user_ins.DataBind()

    End Sub


    Protected Sub Ins_DocBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ins_DocBtn.Click
        class_num_ins.Text = ""
        class_hour_ins.Text = ""
        class_name_ins.Text = ""
        class_date_ins.Text = ""
        Ins_TB.Visible = True
        Doc_TB.Visible = False
        LB_ADD.Text = Common.ISMS_DocNameChange("7")
    End Sub


    Protected Sub Upload(ByVal MyFileUpload As FileUpload)
        Dim path As String = HttpContext.Current.Request.MapPath(Common.Sys_Config("File_Path"))
        Dim DateTime_Str As String = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
        Dim DateTime_Str1 As String = DateTime.Now.ToString("yyyyMMddHHmmss")
        Dim Str_FileName As String = DateTime_Str1 + "_" + MyFileUpload.FileName
        Dim pid As Integer


        Dim Ds As New DataSet
        Ds = Common.Get_MaxTrainingid

        pid = Ds.Tables(0).Rows(0).Item("id")
        ' If (MyFileUpload.HasFile) = True Then
        Try

            '  pid = Ds.Tables(0).Rows(0).Item("board_id")
            SqlTxt = "INSERT INTO ClassTraining_files (pid,filename)  VALUES(  " + pid.ToString + " ,'" + Str_FileName + "')"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            MyFileUpload.SaveAs(path & Str_FileName)
        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try

        'End If
    End Sub






    Protected Sub Ins_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ins_Btn.Click

        'Dim DateTime_Str As String = Board_Date.Text.ToString("yyyy-MM-dd HH:mm:ss")

        SqlTxt = "INSERT INTO ClassTraining_list (dp,uid,uname,class_date,class_num,class_name,class_hour,class_enable)  VALUES " + _
        "('" + Hidden_DP.Value + "','" + ddl_user_ins.SelectedValue + "','" + Common.Get_User_Name(ddl_user_ins.SelectedValue) + "','" + class_date_ins.Text + "','" + class_num_ins.Text + "','" + class_name_ins.Text + "','" + class_hour_ins.Text + "',1)"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()


        If FileUpload1.HasFile = True Then
            Upload(FileUpload1)
        End If

      


        GridView1.DataBind()
        Doc_TB.Visible = True
        Ins_TB.Visible = False

        Dim Ds As New DataSet
        Ds = Common.Get_MaxTrainingid
        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", Ds.Tables(0).Rows(0).Item("id"), System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADDTRAINING", Ds.Tables(0).Rows(0).Item("class_name"))



    End Sub

    Protected Sub Disable_Doc_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex

        SqlTxt = "update  ClassTraining_list set class_enable=0   WHERE id='" + GridView1.Rows(Index).Cells(6).Text + "'"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()

        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", GridView1.Rows(Index).Cells(6).Text, System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DELTRAINING", GridView1.Rows(Index).Cells(3).Text)



    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(6).Visible = False
    End Sub
    Protected Sub Rev_Doc_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Label1.Text = Common.ISMS_DocNameChange("7")

        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex

        Edit_TB.Visible = True
        Ins_TB.Visible = False
        Doc_TB.Visible = False


        Dim ds, ds1 As New DataSet
        ds = Common.Get_Training_info(Me.GridView1.Rows(Index).Cells(6).Text)
        ddl_user_edit.DataSource = Common.Get_User(Hidden_DP.Value)
        ddl_user_edit.DataTextField = "user_name"
        ddl_user_edit.DataValueField = "user_id"
        ddl_user_edit.DataBind()
        ddl_user_edit.SelectedValue = ds.Tables(0).Rows(0).Item("uid").ToString

        class_num_edit.Text = ds.Tables(0).Rows(0).Item("class_num").ToString
        class_name_edit.Text = ds.Tables(0).Rows(0).Item("class_name").ToString
        class_hour_edit.Text = ds.Tables(0).Rows(0).Item("class_hour").ToString
        class_date_edit.Text = ds.Tables(0).Rows(0).Item("class_date").ToString


        Dim i As Integer

        ds1 = Common.Get_Training_files(Me.GridView1.Rows(Index).Cells(6).Text)

        If ds1.Tables(0).Rows.Count - 1 <> -1 Then
            For i = 0 To ds1.Tables(0).Rows.Count - 1
                If i = 0 Then
                    HyperLink2.Visible = True
                    HyperLink2.Text = ds1.Tables(0).Rows(i).Item("filename")
                    HyperLink2.NavigateUrl = Common.Sys_Config("File_Path") + ds1.Tables(0).Rows(i).Item("filename")
                End If
            Next
        End If

        Hidden_Docid.Value = GridView1.Rows(Index).Cells(6).Text
    End Sub



    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim sMessage, sShowField As String
        Dim oDataRow As Data.DataRowView
        If e.Row.RowIndex <> -1 Then
            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = e.Row.Cells(0).Text + "/" + e.Row.Cells(3).Text
            sMessage = String.Format("您確定要修改此筆 [{0}] 資料嗎?", sShowField)
            CType(e.Row.FindControl("Rev_Doc"), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"
            sMessage = String.Format("您確定要刪除此筆 [{0}] 資料嗎?", sShowField)
            CType(e.Row.FindControl("Disable_Doc"), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"

     

        End If
    End Sub




    Protected Sub EditBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles EditBtn.Click

        SqlTxt = "Update ClassTraining_list Set class_name='" + class_name_edit.Text + "', uid='" + ddl_user_edit.SelectedValue + "',uname='" + Common.Get_User_Name(ddl_user_edit.SelectedValue) + "',class_date='" + class_date_edit.Text + "',class_num='" + class_num_edit.Text + "',class_hour='" + class_hour_edit.Text + "' where id=" + Hidden_Docid.Value + ""
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()


        If FileUpload4.HasFile = True Then
            Upload_Edit(FileUpload4)
        End If



        GridView1.DataBind()
        Doc_TB.Visible = True
        Ins_TB.Visible = False
        Edit_TB.Visible = False

        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", Hidden_Docid.Value, System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDITTRAINING", class_name_edit.Text)


    End Sub







    Protected Sub Upload_Edit(ByVal EditFileUpload As FileUpload)
        Dim path As String = HttpContext.Current.Request.MapPath(Common.Sys_Config("File_Path"))
        Dim DateTime_Str As String = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
        Dim DateTime_Str1 As String = DateTime.Now.ToString("yyyyMMddHHmmss")
        Dim Str_FileName As String = DateTime_Str1 + "_" + EditFileUpload.FileName

        Dim Ds, Ds1 As New DataSet
        Ds1 = Common.Get_Training_files(Me.GridView1.Rows(Index).Cells(6).Text)
        Try

            SqlTxt = "Delete from ClassTraining_files  where pid= " + Hidden_Docid.Value + " "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()

            SqlTxt = "INSERT INTO ClassTraining_files (pid,filename)  VALUES(  " + Hidden_Docid.Value + " ,'" + Str_FileName + "')"
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            EditFileUpload.SaveAs(path & Str_FileName)

        Catch ex As Exception
            Common.showMsg(Me.Page, ex.Message)
        End Try


    End Sub



    Protected Sub View_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        HyperLink1.Text = ""

        Label2.Text = Common.ISMS_DocNameChange("7")


        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex

        Edit_TB.Visible = False
        Ins_TB.Visible = False
        Doc_TB.Visible = False
        info_tb.Visible = True

        Dim ds, ds1 As New DataSet
        ds = Common.Get_Training_info(Me.GridView1.Rows(Index).Cells(6).Text)
        lb_uid.Text = Common.Get_User_Name(ds.Tables(0).Rows(0).Item("uid").ToString)
        lb_class_date.Text = ds.Tables(0).Rows(0).Item("class_date").ToString
        lb_class_num.Text = ds.Tables(0).Rows(0).Item("class_num").ToString
        lb_class_name.Text = ds.Tables(0).Rows(0).Item("class_name").ToString
        lb_class_hour.Text = ds.Tables(0).Rows(0).Item("class_hour").ToString

        ds1 = Common.Get_Training_files(Me.GridView1.Rows(Index).Cells(6).Text)

        Dim i As Integer


        If ds1.Tables(0).Rows.Count - 1 <> -1 Then
            For i = 0 To ds1.Tables(0).Rows.Count - 1
                If i = 0 Then
                    HyperLink1.Visible = True
                    HyperLink1.Text = ds1.Tables(0).Rows(i).Item("filename")
                    HyperLink1.NavigateUrl = Common.Sys_Config("File_Path") + ds1.Tables(0).Rows(i).Item("filename")
                End If
            Next
        End If
    End Sub

    Protected Sub list_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles list_btn.Click
        Ins_TB.Visible = False
        Edit_TB.Visible = False
        info_tb.Visible = False
        Doc_TB.Visible = True
    End Sub

End Class
