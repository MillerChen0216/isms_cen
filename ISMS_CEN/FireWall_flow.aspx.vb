﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI
Partial Class FireWall_flow
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlCmd As SqlCommand
    Dim uid As String = User.Identity.Name '使用者名稱
    Dim uname As String = Common.Get_User_Name(User.Identity.Name.ToString) '使用者代號
    Dim SqlTxt As String
    Dim approvalid As String
    Dim apprvid As String
    Dim dp As String = Me.Get_DP
    Dim File_Path As String = ConfigurationManager.AppSettings("FireWall_Path")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        approvalid = ConfigurationManager.AppSettings("flow_FireWall") '16
        apprvid = ConfigurationManager.AppSettings("flow_FireWall") '16
        If Not IsPostBack Then
            verifyTime.Text = Common.Year(DateTime.Now)
        End If
        Gdv1Show() '待審核表
    End Sub

    '待審核表來源
    Protected Sub Gdv1Show()
        Try
            Dim sql = " select * from  form_record a join Firewall b on  a.recordid=b.recordid where b.using = 'Y' and approvalid='" + approvalid + "' and a.status=1 and userid='" + uid + "' order by b.applyDate"
            Dim data As DataTable = Common.Con(sql)
            GridView1.DataSource = data
            GridView1.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '簽核過程及民國年轉換
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '簽核過程
                Dim recordid As String = CType(e.Row.FindControl("lb_rowid"), Label).Text
                CType(e.Row.FindControl("lb_flow"), Label).Text = Common.Str_form_record_isms(approvalid, recordid)
                '民國年轉換
                Dim lb_applydate As String = CType(e.Row.FindControl("lb_applydate"), Label).Text
                CType(e.Row.FindControl("lb_applydate"), Label).Text = Common.Year(Date.Parse(lb_applydate))
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '審核表畫面至審核畫面_1
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Try
            Dim Index As Integer = CType(e.CommandArgument, Int32)
            Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
            Dim lb_rowid As String = (TryCast(SelectedRow.FindControl("lb_rowid"), Label)).Text

            Select Case e.CommandName
                Case "audit_btn"
                    Dim sql = "  SELECT * FROM Firewall WHERE recordId = '" + lb_rowid + "'"
                    Dim data As DataTable = Common.Con(sql)
                    If data.Rows.Count > 0 Then
                        '紀錄該表單編號
                        lb_recordid.Text = lb_rowid
                        '載入關卡表
                        GdvShow()
                        Dim stage As Integer = CType(lb_count.Text, Integer) '找關卡

                        ''填入該lb_rowid資料
                        applyDate.Text = Common.Year(data.Rows(0)("applyDate")) '申請日期
                        recordId.Text = data.Rows(0)("recordId") '表單編號
                        empName.Text = data.Rows(0)("empName") '申請代表人
                        applyOrg.Text = data.Rows(0)("applyOrg") '申請單位
                        applyName.Text = data.Rows(0)("applyName") '申請人員
                        applyTel1.Text = data.Rows(0)("applyTel1") '連絡電話1
                        applyTel2.Text = data.Rows(0)("applyTel2") '連絡電話2
                        If Not IsDBNull(data.Rows(0)("startTime")) Then
                            startTime.Text = Common.Year(data.Rows(0)("startTime")) '預計開放日期(起)
                        End If
                        If Not IsDBNull(data.Rows(0)("endTime")) Then
                            endTime.Text = Common.Year(data.Rows(0)("endTime")) '預計開放日期(迄)
                        End If
                        applyReason.Text = data.Rows(0)("applyReason") '申請事由
                        F_Ip.Text = data.Rows(0)("F_Ip") '來源IP
                        F_Port.Text = data.Rows(0)("F_Port") '來源Port
                        T_Ip.Text = data.Rows(0)("T_Ip") '目的IP
                        T_Port.Text = data.Rows(0)("T_Port") '目的Port
                        Radio_Connected.SelectedValue = data.Rows(0)("Connected") '單/雙向連接

                        '載入資訊維護暨攜出入申請單編號
                        SYSIO_recordId.Text = ""
                        Dim sql2 = "select * from SYSIO_related where related_recordId = '" + lb_recordid.Text + "'"
                        Dim data2 As DataTable = Common.Con(sql2)
                        If Not data2.Rows.Count = 0 Then
                            For Each item In data2.Select
                                If Not String.IsNullOrEmpty(item("original_SYSIO_recordId").ToString()) Then
                                    If Not SYSIO_recordId.Text.Contains(item("original_SYSIO_recordId").ToString()) Then
                                        SYSIO_recordId.Text += item("original_SYSIO_recordId") + ", "
                                    End If
                                Else
                                    SYSIO_recordId.Text += item("SYSIO_recordId") + ","
                                End If
                            Next
                        End If


                        Dim workflowId As Integer = Common.Get_workflowId(approvalid, recordId.Text) '取得關卡

                        If workflowId = 0 Then '(GridView.Rows.Count - stage) = 3
                            '第0關
                            TopSheetEnabled(True) '上方資料第0關可修改
                            DownSheetVisible(False) '下方資料第0關還看不到
                            '檔案處理
                            File_applyFile.Visible = True
                        Else
                            '其他關卡
                            TopSheetEnabled(False) '上方資料其他關卡不可修改
                            '檔案處理
                            File_applyFile.Visible = False
                        End If

                        If workflowId = 1 Then
                            '資訊人員關卡
                            '檔案處理
                            File_verifyFile.Visible = True
                            DownSheetVisible(True) '下方資料承辦人員看得到
                            DownSheetEnabled(True) '下方資料承辦人員可以用
                            '.Text = data.Rows(0)("") TODO 資訊維護暨攜出入申請單編號
                        Else
                            '其他關卡
                            '檔案處理
                            File_verifyFile.Visible = False
                            DownSheetEnabled(False) '下方資料其他關卡不能操作

                            If workflowId > 1 Then
                                '已經填完才可以秀出來給別人看
                                DownSheetVisible(True) '下方資料後面人員看得到
                                Radio_verifyRule.SelectedValue = data.Rows(0)("verifyRule") '防火牆規則
                                verifyComment.Text = data.Rows(0)("verifyComment") '審核說明
                                verifyTime.Text = Common.Year(data.Rows(0)("verifyTime")) '辦理日期
                            End If
                        End If

                        '查看裡面是否有資料，有才秀出來
                        If IsDBNull(data.Rows(0)("applyFile")) Then
                            applyFile.Visible = False
                        Else
                            applyFile.Visible = True
                            applyFile_link.Text = data.Rows(0)("applyFile")
                        End If

                        '查看裡面是否有資料，有才秀出來
                        If IsDBNull(data.Rows(0)("verifyFile")) Then
                            verifyFile.Visible = False
                        Else
                            verifyFile.Visible = True
                            verifyFile_link.Text = data.Rows(0)("verifyFile") '附件檔案(資訊單位填寫)
                        End If

                        '將審核畫面弄出來
                        tb_result.Visible = True
                        tr_gridview.Visible = False
                        Insert_Btn.Visible = True
                        but_esc.Visible = True
                    Else
                        Response.Redirect("FireWall_flow.aspx", True) '重新載入附件表單簽核_審核 因為此表單編號已經不存在
                    End If


            End Select
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '小工具1
    Private Sub TopSheetEnabled(ByVal use As Boolean)
        applyOrg.Enabled = use
        applyName.Enabled = use
        applyTel1.Enabled = use
        applyTel2.Enabled = use
        startTime.Enabled = use
        endTime.Enabled = use
        applyReason.Enabled = use
        F_Ip.Enabled = use
        F_Port.Enabled = use
        T_Ip.Enabled = use
        T_Port.Enabled = use
        Radio_Connected.Enabled = use
    End Sub

    '小工具2
    Private Sub DownSheetVisible(ByVal show As Boolean)
        tr1.Visible = show
        tr2.Visible = show
        tr3.Visible = show
        tr4.Visible = show
        tr5.Visible = show
    End Sub

    '小工具3
    Private Sub DownSheetEnabled(ByVal use As Boolean)
        Radio_verifyRule.Enabled = use
        verifyComment.Enabled = use
        verifyTime.Enabled = use
    End Sub

    '關卡表
    Private Sub GdvShow()
        Try
            Dim dt As DataTable = Common.Qry_workflowName_isms_Req(approvalid, lb_recordid.Text, apprvid, dp)
            GridView.DataSource = dt
            GridView.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '關卡表_RowDataBound
    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '***已簽核資料

                '***人員
                Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
                Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
                Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
                Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
                '***狀態
                Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
                Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
                Dim hid_status As HiddenField = CType(e.Row.FindControl("hid_status"), HiddenField)
                '***意見
                Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
                Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
                '***關卡***
                Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text
                '***審核人員***
                'Dim lb_wf_inside_order As String = CType(e.Row.FindControl("lb_wf_inside_order"), Label).Text

                Dim ds As DataSet


                '***Start
                If o_wf_order = "0" And hid_status.Value <> "1" Then
                    pan_user.Visible = True
                    '將審核人員設為自己
                    Dim sql = "    SELECT user_id, user_name FROM ismsUser WHERE user_id = '" + uid + "'"
                    Dim data As DataTable = Common.Con(sql)
                    CType(e.Row.FindControl("lb_username"), Label).Text = data.Rows(0)("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = data.Rows(0)("user_id")
                ElseIf hid_status.Value = "1" Then '待審核
                    ds = Common.Qry_form_record_wait(lb_recordid.Text)
                    pan_user.Visible = True
                    CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
                    lb_count.Text = e.Row.RowIndex
                ElseIf hid_status.Value = "2" Or hid_status.Value = "9" Then '審核已通過
                    ds = Common.Qry_form_record_name(lb_recordid.Text, o_wf_order)
                    pan_user.Visible = True
                    CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
                ElseIf e.Row.RowIndex = CType(lb_count.Text, Integer) + 1 Then '待審核下一關卡
                    ddl_user.Visible = True
                    ds = Common.Get_Wf_User(dp, apprvid, o_wf_order, "")
                    ddl_user.DataSource = ds
                    ddl_user.DataTextField = "name_id"
                    ddl_user.DataValueField = "wf_userid"
                    ddl_user.DataBind()
                End If

                If hid_status.Value = "1" Then '待審核
                    ddl_status.Visible = True
                    lb_status.Visible = False
                    txt_comment.Visible = True
                    lb_comment.Visible = False
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(1)
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()

                ElseIf hid_status.Value = "" Then
                    ddl_status.Visible = False
                    lb_status.Visible = True
                    txt_comment.Visible = False
                    lb_comment.Visible = True
                Else
                    ddl_status.Visible = False
                    lb_status.Visible = True
                    txt_comment.Visible = False
                    lb_comment.Visible = True
                    CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(hid_status.Value))
                End If
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '檔案下載_按鈕(上()
    Protected Sub applyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + applyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '檔案下載_按鈕(下)
    Protected Sub verifyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles verifyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + verifyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '返回列表_按鈕
    Protected Sub but_esc_click()
        tr_gridview.Visible = True
        tb_result.Visible = False
        Insert_Btn.Visible = False
        but_esc.Visible = False
        Gdv1Show()
    End Sub

    '送出審核_按鈕
    Protected Sub Insert_Btn_click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim index As Integer = CType(lb_count.Text, Integer)
        Dim max_index As Integer = GridView.Rows.Count

        Dim wf_order As String = CType(GridView.Rows(index).FindControl("wf_order"), Label).Text '待審核的關卡
        Dim status As String = CType(GridView.Rows(index).FindControl("ddl_status"), DropDownList).SelectedValue '狀態
        Dim comment As String = CType(GridView.Rows(index).FindControl("txb_comment"), TextBox).Text
        Dim id As String = CType(GridView.Rows(index).FindControl("lb_id2"), Label).Text
        Dim stage As Integer = Common.Get_workflowId(approvalid, recordId.Text) '取得關卡

        If status = 2 Then '通過
            If index + 1 < GridView.Rows.Count Then '流程尚未到最後關卡

                If stage = 0 Then

                    '回到第0關
                    If DateTime.Parse(applyDate.Text) > DateTime.Parse(startTime.Text) Or DateTime.Parse(endTime.Text) < DateTime.Parse(startTime.Text) Then
                        Common.showMsg(Me.Page, "請符合結束時間>=開始日期>=申請日期")
                        Return
                    End If
                    If String.IsNullOrEmpty(applyDate.Text) Or String.IsNullOrEmpty(applyOrg.Text) Or String.IsNullOrEmpty(applyName.Text) _
                        Or (String.IsNullOrEmpty(applyTel1.Text) And String.IsNullOrEmpty(applyTel2.Text)) Or String.IsNullOrEmpty(applyReason.Text) Or String.IsNullOrEmpty(F_Ip.Text) _
                        Or String.IsNullOrEmpty(F_Port.Text) Or String.IsNullOrEmpty(T_Ip.Text) Or String.IsNullOrEmpty(T_Port.Text) Or String.IsNullOrEmpty(Radio_Connected.Text) Then
                        Common.showMsg(Me.Page, "請確實填寫資訊")
                        Return
                    End If
                    Dim startdate = ""
                    Dim enddate = ""
                    If Not String.IsNullOrEmpty(startTime.Text) Then
                        startdate = "'" + Common.YearYYYY(startTime.Text) + "'"
                    Else
                        startdate = "null"
                    End If
                    If Not String.IsNullOrEmpty(endTime.Text) Then
                        enddate = "'" + Common.YearYYYY(endTime.Text) + "'"
                    Else
                        enddate = "null"
                    End If

                    Dim applyFile = "" '沒有檔案
                    If CType(Me.FindControl("File_applyFile"), FileUpload).HasFile Then
                        '有檔案
                        Dim oriFileName = CType(Me.FindControl("File_applyFile"), FileUpload)
                        Dim UpFileName As String = Common.UpFile(File_Path, recordId.Text, CType(Me.FindControl("File_applyFile"), FileUpload))
                        applyFile = ", applyFile_ori_name = '" + oriFileName.FileName + "', applyFile = '" + UpFileName + "'"
                    End If


                    SqlTxt = " update Firewall set " _
                    + "applyOrg = '" + applyOrg.Text + "', applyName = '" + applyName.Text + "', applyTel1= '" + applyTel1.Text + "', applyTel2 = '" + applyTel2.Text + "'," _
                    + " startTime = " + startdate + ", endTime = " + enddate + ", applyReason = '" + applyReason.Text + "'," _
                    + "F_Ip= '" + F_Ip.Text + "', F_Port = '" + F_Port.Text + "', T_Ip = '" + T_Ip.Text + "', T_Port = '" + T_Port.Text + "', Connected = '" + Radio_Connected.SelectedValue + "'" + applyFile + "," _
                    + " op_time = getdate() where recordId = '" + recordId.Text + "'"

                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                End If

                Dim workflowid As String = CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) + 1
                Dim user_id = CType(GridView.Rows(index + 1).FindControl("ddl_user"), DropDownList).SelectedValue

                SqlTxt = " INSERT INTO form_record (approvalId,workflowId,userId,userType,status,comment,recordId,signDate,role,dp) VALUES "
                SqlTxt += " ('" + approvalid + "','" + workflowid + "','" + user_id + "','land','1','','" + lb_recordid.Text + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + workflowid + "','" + dp + "') "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            End If
            '資訊人員填寫更新
            If stage = 1 Then
                If String.IsNullOrEmpty(verifyComment.Text) Or String.IsNullOrEmpty(verifyTime.Text) Then
                    Common.showMsg(Me.Page, "請確實填寫資訊")
                    Return
                End If
                Dim verifyFile = "" '沒有檔案
                If CType(Me.FindControl("File_verifyFile"), FileUpload).HasFile Then
                    '有檔案
                    Dim oriFileName = CType(Me.FindControl("File_verifyFile"), FileUpload)
                    Dim UpFileName As String = Common.UpFile(File_Path, recordId.Text, CType(Me.FindControl("File_verifyFile"), FileUpload))
                    verifyFile = " verifyFile_ori_name = '" + oriFileName.FileName + "', verifyFile = '" + UpFileName + "',"

                End If

                SqlTxt = " update Firewall set " _
                + " verifyRule = '" + Radio_verifyRule.SelectedValue + "', verifyComment = '" + verifyComment.Text + "', verifyTime = '" + Common.YearYYYY(verifyTime.Text) + "'," + verifyFile + " op_time = getdate() where recordId = '" + recordId.Text + "'"

                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            End If
            SqlTxt += " UPDATE form_record SET status='2' , signDate ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' , comment = '" + comment + "' WHERE status != 2 and status !=0 and approvalId='" + approvalid + "'and recordId ='" + lb_recordid.Text + "' and id ='" + id + "' "
        ElseIf status = 9 Then '退回
            If CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) > 0 Then '目前流程不是第一關卡
                Dim workflowid As String = CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) - 1
                'Dim user_id = CType(GridView.Rows(index - 1).FindControl("label_userid"), Label).Text
                Dim dt As DataTable = Common.Qry_form_record1(approvalid, lb_recordid.Text, workflowid)
                Dim user_id As String = dt.Rows(0).Item("userId").ToString

                SqlTxt = " INSERT INTO form_record (approvalId,workflowId,userId,userType,status,comment,recordId,signDate,role,dp) VALUES "
                SqlTxt += " ('" + approvalid + "','" + workflowid + "','" + user_id + "','land','1','','" + lb_recordid.Text + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + workflowid + "','" + dp + "') "

                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            Else
                Common.showMsg(Me.Page, "流程已到填表人，無法再退回！")
                Return
            End If

            SqlTxt = " UPDATE form_record SET status='9' , signDate ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' , comment = '" + comment + "' WHERE approvalId='" + approvalid + "'and recordId ='" + lb_recordid.Text + "' and id ='" + id + "' "

        End If
        Dim cmd As New SqlCommand(SqlTxt.ToString)

        Try
            Conn = Common.ConnDBS : Conn.Open() : cmd.Connection = Conn
            If cmd.ExecuteNonQuery() Then Common.showMsg(Me.Page, "審核完成！") : but_esc_click()
        Catch ex As Exception
            Common.showMsg(Me.Page, "審核失敗！")
        Finally
            Conn.Close() : Conn.Close() : Conn.Dispose() : cmd.Dispose()
        End Try
    End Sub
End Class
