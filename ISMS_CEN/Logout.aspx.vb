﻿
Partial Class Logout
    Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormsAuthentication.SignOut()
        Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "0", System.IO.Path.GetFileName(Request.PhysicalPath), "GENERAL", "LOGINOUT", "")
        Response.Redirect("Login.aspx")
        Response.Write("<script language='javascript'> { window.opener=null;window.close();}</script>")

    End Sub
End Class
