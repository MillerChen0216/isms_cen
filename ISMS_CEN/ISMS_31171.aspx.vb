﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_31171
    Inherits PageBase
    'Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP

        If Not Page.IsPostBack Then

            Common.ddl_DataRead("SELECT * FROM asset  order by asset_id", ddl_Asset, 1, 0)
            ddl_assgroup.Enabled = False

        ElseIf ddl_Asset.SelectedIndex = 0 Then
            ddl_assgroup.Enabled = False
        End If
    End Sub
    Protected Sub ddl_Asset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Asset.SelectedIndexChanged
        ddl_assgroup.Items.Clear()
        ddl_assgroup.Items.Add(New ListItem("全部", 0))

        If ddl_Asset.SelectedIndex = 0 Then
            ddl_assgroup.Enabled = False
        Else
            ddl_assgroup.Enabled = True
            Common.ddl_DataRead("SELECT   *  FROM   assetGroup  where asset_id=" + ddl_Asset.SelectedValue + "", ddl_assgroup, 2, 1)
        End If
    End Sub
    Protected Sub Rpt_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rpt_Btn.Click
        ReportViewer1.LocalReport.Refresh()
    End Sub
End Class
