﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_12110.aspx.vb" Inherits="ISMS_12110" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:HiddenField ID="Hidden_DP" runat="server" />
        <br />
        <table id="Main_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 750px"
            visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    文件審核管理 | 列表&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="750px">
                        <Columns>
                            <asp:BoundField DataField="doc_type" HeaderText="文件類別">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_num" HeaderText="文件編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_name" HeaderText="文件名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_version" HeaderText="版次">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="管理">
                                <ItemTemplate>
                                    <asp:Button ID="audit_btn" runat="server" Text="審核" OnClick="audit_btn_Click" />
                                    <asp:Button ID="Edit_Btn" runat="server" Text="修改" OnClick="Edit_Btn_Click" Visible="False" />
                                    <asp:Button ID="Del_Btn" runat="server" Text="刪除" OnClick="Del_Btn_Click" Visible="False" />
                                    <asp:Label ID="test" runat="server" ></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="doc_id" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
        <asp:SqlDataSource ID="Flow_DS" runat="server"></asp:SqlDataSource>
        <asp:HiddenField ID="Hidden_Docid" runat="server" Visible="False" /><asp:HiddenField ID="Hidden_apprv_id" runat="server" Visible="False" /><asp:HiddenField ID="HiddneRecord_id" runat="server" Visible="False" /><asp:HiddenField ID="Hidden_Status" runat="server" Visible="False" />
        <asp:HiddenField ID="Hidden_doc_dp" runat="server" Visible="False" />
        <asp:SqlDataSource ID="WF_DS" runat="server"></asp:SqlDataSource>
        <table class="tb_1" style="width: 550px" cellpadding="1" cellspacing="1" id="info_tb" runat="server" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="2">
                    <img src="Images/exe.gif" />
                    文件資訊</td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    文件類別</td>
                <td class="tb_w_1">
                    <asp:Label ID="docCat_LB" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    文件編號</td>
                <td class="tb_w_1">
                    <asp:Label ID="docNum_LB" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    版次</td>
                <td class="tb_w_1">
                    <asp:Label ID="docVer_LB" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    文件名稱</td>
                <td class="tb_w_1">
                    <asp:Label ID="docName_LB" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    增修人</td>
                <td class="tb_w_1">
                    <asp:Label ID="docCreator" runat="server"></asp:Label>
                    /
                    <asp:Label ID="docCreatorName" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" style="width: 135px">
                    內容簡述/修訂內容</td>
                <td class="tb_w_1" style="color: #345b7b">
                    <asp:Label ID="docDsc_LB" runat="server"></asp:Label></td>
            </tr>
            <tr style="color: #345b7b">
                <td class="tb_title_w_1" style="width: 135px">
                    檔案下載</td>
                <td class="tb_w_1">
                    <asp:Button ID="Filedown_Btn" runat="server" Text="文件下載" /></td>
            </tr>
        </table>
        <br />
        <table id="tb2" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 550px"
            visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    審核作業 -
                    <asp:Label ID="lb_status" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                     <asp:Label ID="lb_count"  runat="server" Visible="false" Text="999"></asp:Label>
                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="550px">
                        <Columns>
                            <asp:BoundField DataField="wf_order" HeaderText="關卡">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="chk_name" HeaderText="審核身分">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="審核人員">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddl_user" runat="server" Visible="false" Width="120px" ></asp:DropDownList>
                                      <asp:Panel ID="pan_user" runat="server" Visible="false">
                                    <asp:Label ID="lb_username" runat="server"></asp:Label>
                                    (<asp:Label ID="label_userid" runat="server"></asp:Label>)
                                     <asp:Label ID="lab_sn"  Text='<%# Eval("sn")%>' Visible="false" runat="server"></asp:Label>
                                     <asp:Label ID="lab_status_id"  Text='<%# Eval("wf_status_id")%>' Visible="false" runat="server"></asp:Label>
                                      </asp:Panel>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="狀態">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddl_status" runat="server" Visible="False">
                                    </asp:DropDownList>
                                    <asp:Label ID="lb_status" runat="server" Visible="False"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="意見" >
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="comment_lb" runat="server" Visible="False" ></asp:Label>
                                    <asp:TextBox ID="comment_txt" runat="server" Visible="False" Width="100px"></asp:TextBox>
                                    <asp:Button ID="verify_btn" runat="server" OnClick="verify_btn_Click" Text="審核" Visible="False" />
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="wf_inside_order" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    </td>
            </tr>
        </table>
        <br />
        <asp:Label ID="lb_wfData" runat="server" CssClass="normal_font" Visible="False" Width="550px"></asp:Label><br />
        <br />
        &nbsp;&nbsp;
        <br />
    </form>
</body>
</html>
