﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_40140.aspx.vb" Inherits="ISMS_40140" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>地政局系統使用者帳號申請表</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
            EnableScriptLocalization="True">
        </asp:ScriptManager>
        <table border="1" cellpadding="0" cellspacing="0" class="tb_title_w_2" width="850px">
            <tr>
                <td>
                    <img src="Images/exe.gif" />待審核清單 | 管理者 
                    <asp:Label ID="lb_msg" runat="server" ForeColor="red" EnableViewState="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                     <table id="Ins_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 850px"
            visible="false">
          
            <tr>
                <td class="tb_title_w_1">
                    等待審核人員</td>
                <td class="tb_title_w_1">
                    變更審核人員</td>
                <td class="tb_title_w_1">
                    全部使用者</td>
            </tr>
            <tr>
                <td class="tb_w_1" style="color: #656b76;width:180px">
                    <asp:Label ID="check_name" runat="server"></asp:Label>
                    <asp:HiddenField ID="hid_recordid" runat="server" />
                    <asp:HiddenField ID="hid_docid" runat="server" />
                </td>
                <td class="tb_w_1">
                    <asp:Label ID="lab_dp" runat="server"  Text ="所別 :"></asp:Label>
                    <asp:DropDownList ID="ddl_dp" runat="server"  OnSelectedIndexChanged="ddl_dp_selectedchange" AutoPostBack="true" ></asp:DropDownList>
                    <asp:Label ID="lab_deptment" runat="server" Text ="單位 :" ></asp:Label>
                    <asp:DropDownList ID="ddl_deptment" runat="server" OnSelectedIndexChanged="ddl_deptment_selectedchange" AutoPostBack="true" ></asp:DropDownList>
                    &nbsp;<asp:DropDownList ID="ddl_Upd_User" runat="server"></asp:DropDownList></td>
                <td class="tb_w_1" style="color: #656b76">
                    <asp:CheckBox ID="chk_distin" runat="server" AutoPostBack="true"></asp:CheckBox>
                </td>
            </tr>
            <tr>
                <td align="left" class="tb_w_1" colspan="3">
                    <asp:Button ID="Update_Btn" runat="server" Text="確定變更" />
                    <asp:Button ID="Cancel_Btn" runat="server" Text="取消變更" /></td>
            </tr>
        </table>
                </td>
            </tr>
            <tr id="tr_gridview" runat="server">
               <td>
                   <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
                   <asp:HiddenField ID="hid_type"  runat="server" ></asp:HiddenField>
                  <asp:GridView ID="GridView1" runat="server" CssClass="tb_1" AutoGenerateColumns="false" Width="850px">
             <Columns>
                <asp:TemplateField HeaderText="送達日期">
                <ItemTemplate>
                  <asp:Label runat="server" ID="lb_applydate" Text='<%# Format(Eval("signDate"), "yyyy/MM/dd")%>'></asp:Label> 
                  <asp:HiddenField runat="server" ID="hid_recordid_grd" Value='<%# eval("recordid") %>' />
                  <asp:HiddenField runat="server" ID="hid_docid_grd" Value='<%# Eval("doc_id")%>' />
                  <asp:Label runat="server" ID="lb_workflowid" Text='<%# Eval("workflowid")%>' Visible="false"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="表單名稱">
                  <ItemTemplate>
                      <asp:Label runat="server" ID="lb_FormName" Text='<%# Eval("type")%>' Width="190px" ></asp:Label>
                      <asp:HiddenField ID="hid_approvalid" runat="server" Value ='<%# Eval("approvalid")%>' />
                  </ItemTemplate>
               </asp:TemplateField> 
                   <asp:TemplateField HeaderText="簽核過程">
                <ItemTemplate>
                   <asp:Label runat="server" ID="lb_flow" width="490px" ></asp:Label> 
                   <asp:HiddenField runat="server" ID="hid_dp" />
                   <asp:HiddenField runat="server" ID="hid_apprvid" />
                   <asp:HiddenField runat="server" ID="hid_wf_order" />
                </ItemTemplate>
              </asp:TemplateField>                
                  <asp:ButtonField ButtonType="Button" CommandName="audit_btn" Text="流程移轉" HeaderText="流程移轉">
                        <ItemStyle CssClass="tb_w_1" />
                  </asp:ButtonField>
              </Columns>
                <HeaderStyle CssClass="tb_title_w_1" />
                <EmptyDataTemplate>…尚無資料…</EmptyDataTemplate>
        </asp:GridView>
               </td>
            </tr>
        </table>
    </form>
</body>
</html>

