﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO

Partial Class SYSPG_admin
    Inherits PageBase
    'Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim uid As String = User.Identity.Name '使用者名稱
    Dim uname As String = Common.Get_User_Name(uid) '使用者代號
    Dim apprvid As String
    Dim approvalid As String
    Dim dp As String = Get_DP()
    Dim File_Path As String = ConfigurationManager.AppSettings("SYSPG_Path")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        approvalid = ConfigurationManager.AppSettings("flow_SYSPG") '13
        apprvid = ConfigurationManager.AppSettings("flow_SYSPG") '13
        Try
            If Not IsPostBack Then
                txb_sdate.Text = Common.Year(Now.AddDays(-30))
                txb_edate.Text = Common.Year(Now.AddDays(1))
                SqlDataSource() '載入管理者清單
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '載入管理者清單
    Protected Sub SqlDataSource()

        Dim sql As String = "SELECT * FROM SYSPG where using='Y'"
        '排除已撤銷之申請單
        '排除停用帳號
        sql += " and empUid in (select user_id from ismsUser where using='Y') "
        If txb_username.Text <> "" Then
            sql += " and empName like'%" + Trim(txb_username.Text) + "%'"
        End If
        If txb_sdate.Text <> "" Then
            sql += " and applyDate >= '" + Common.YearYYYY(txb_sdate.Text) + "'"
        End If
        If txb_edate.Text <> "" Then
            sql += " and applyDate <= '" + Common.YearYYYY(txb_edate.Text) + "'"
        End If

        SqlDataSource1.ConnectionString = Common.ConnDBS.ConnectionString
        SqlDataSource1.SelectCommand = sql

        GridView1.DataSourceID = SqlDataSource1.ID

    End Sub

    '申請清單民國年處理
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '民國年轉換
                Dim lb_applydate As String = CType(e.Row.FindControl("lb_applydate"), Label).Text
                CType(e.Row.FindControl("lb_applydate"), Label).Text = Common.Year(Date.Parse(lb_applydate))
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '查詢
    Protected Sub but_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_search.Click
        Try
            SqlDataSource()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub


    '查看或撤銷
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
        Dim hidden_recordId As HiddenField = (TryCast(SelectedRow.FindControl("hidden_recordId"), HiddenField))
        Select Case e.CommandName

            Case "audit_btn"
                Try
                    lb_recordid.Text = hidden_recordId.Value '紀錄該表單編號
                    Dim sql = "  SELECT * FROM SYSPG WHERE recordId = '" + hidden_recordId.Value + "'"
                    Dim data As DataTable = Common.Con(sql)
                    If data.Rows.Count > 0 Then
                        '載入關卡表
                        GdvShow()
                        '填入該lb_rowid資料()
                        applyDate.Text = Common.Year(data.Rows(0)("applyDate")) '申請日期
                        recordId.Text = data.Rows(0)("recordId") '表單編號
                        syscd_Name.Text = data.Rows(0)("syscd_Name") '系統名稱
                        sysFunction.Text = data.Rows(0)("sysFunction") '功能名稱
                        applyOrg.Text = data.Rows(0)("applyOrg") '申請單位
                        applyName.Text = data.Rows(0)("applyName") '申請人員
                        Radio_applyReason.SelectedValue = data.Rows(0)("applyReason") '變更原因
                        applyAction.Text = data.Rows(0)("applyAction") '申請變更事項
                        applyPath.Text = data.Rows(0)("applyPath") '程式路徑(詳列變更之程式存放路徑)

                        If Not IsDBNull(data.Rows(0)("expectedTime")) Then
                            expectedTime.Text = Common.Year(data.Rows(0)("expectedTime")) + " " + DateTime.Parse(data.Rows(0)("expectedTime")).ToString("HH:mm") '預定完成時間
                        End If


                        '不開放編輯
                        SheetEnabled(False)


                        '查看裡面是否有資料，有才秀出來
                        If IsDBNull(data.Rows(0)("applyFile")) Then
                            applyFile.Visible = False
                        Else
                            applyFile.Visible = True
                            applyFile_link.Text = data.Rows(0)("applyFile")
                        End If
                        
                    Else
                        Response.Redirect("SYSPG_admin.aspx", True) '如果沒有這個recordId就重新導向管理者清單
                    End If
                    tb_result.Visible = True
                    tb_query.Visible = False
                    GridView1.Visible = False
                Catch ex As Exception
                    lb_msg.Text = ex.Message
                End Try
            Case "using_btn"
                Try
                    Dim checksql = "   select * from SYSPG_rec where SYSPG_recordId = '" + hidden_recordId.Value + "' and using = 'Y'"
                    Dim data = Common.Con(checksql)
                    If data.Rows.Count = 0 Then
                        Dim SYSIO_related_sql = "  select * from SYSIO_related where kind = 'PG' and SYSIO_recordId = '" + hidden_recordId.Value + "'"
                        Dim SYSIO_related_data = Common.Con(SYSIO_related_sql)
                        If SYSIO_related_data.Rows.Count > 0 Then
                            lb_msg.Text = "本申請表已使用於資訊維護暨攜出入申請單，無法撤銷"
                            Return
                        End If

                        Dim sql As New StringBuilder
                        sql.Append("update SYSPG set using='N' where recordId ='" + hidden_recordId.Value + "' ")
                        sql.Append("  update form_record set status = -1 where recordId = '" + hidden_recordId.Value + "' and approvalId = '" + approvalid + "' and status = 1")
                        

                        Dim conn As SqlConnection = Nothing : Dim cmd As New SqlCommand(sql.ToString)
                        Try
                            conn = Common.ConnDBS : conn.Open() : cmd.Connection = conn
                            If cmd.ExecuteNonQuery() Then GridView1.DataBind() : lb_msg.Text = "撤銷成功！"
                        Catch ex As Exception
                            lb_msg.Text = "撤銷失敗！" + ex.Message
                        Finally
                            conn.Close() : conn.Close() : conn.Dispose() : cmd.Dispose()
                        End Try
                    End If
                    lb_msg.Text = "不能撤銷，要先撤銷紀錄表"

                Catch ex As Exception
                    lb_msg.Text = ex.Message
                End Try


        End Select
    End Sub

    '小工具1
    Private Sub SheetEnabled(ByVal use As Boolean)
        sysFunction.Enabled = use
        applyOrg.Enabled = use
        applyName.Enabled = use
        Radio_applyReason.Enabled = use
        applyAction.Enabled = use
        applyPath.Enabled = use
    End Sub

    '關卡
    Private Sub GdvShow()
        Dim dt As DataTable = Common.Qry_workflowName_isms_Req(approvalid, lb_recordid.Text, apprvid, dp)
        GridView.DataSource = dt
        GridView.DataBind()

    End Sub

    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料

            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            Dim hid_status As HiddenField = CType(e.Row.FindControl("hid_status"), HiddenField)
            '***意見
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text
            '***審核人員***
            ' Dim lb_wf_inside_order As String = CType(e.Row.FindControl("lb_wf_inside_order"), Label).Text

            Dim ds As DataSet

            '***Start
            If o_wf_order = "0" And hid_status.Value <> "1" Then
                pan_user.Visible = True
                '將審核人員設為自己
                Dim sql = "    SELECT user_id, user_name FROM ismsUser WHERE user_id = '" + lb_username.Text + "'"
                Dim data As DataTable = Common.Con(sql)
                CType(e.Row.FindControl("lb_username"), Label).Text = data.Rows(0)("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = data.Rows(0)("user_id")
            ElseIf hid_status.Value = "1" Then '待審核
                ds = Common.Qry_form_record_wait(lb_recordid.Text)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
            ElseIf hid_status.Value = "2" Or hid_status.Value = "9" Then '審核已通過
                ds = Common.Qry_form_record_name(lb_recordid.Text, o_wf_order)
                pan_user.Visible = True
                CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
            End If

            If hid_status.Value = "" Then
                lb_status.Text = ""
            Else
                CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(hid_status.Value))
            End If
        End If
    End Sub

    '檔案下載_按鈕
    Protected Sub applyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + applyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '1_回列表_按鈕
    Protected Sub but_esc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_esc.Click
        Response.Redirect("SYSPG_admin.aspx", True)
    End Sub

End Class
