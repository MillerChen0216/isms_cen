﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_12120.aspx.vb" Inherits="ISMS_12120" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:HiddenField ID="Hidden_DP" runat="server" />

        <br />
        <table id="Main_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 750px"
            visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    文件發行管理 | 列表&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="750px">
                        <Columns>
                            <asp:BoundField DataField="doc_type" HeaderText="文件類別" >
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_num" HeaderText="文件編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_name" HeaderText="文件名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_version" HeaderText="版次">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="creator" HeaderText="增修人">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:ButtonField ButtonType="Button" CommandName="Publish_Cmd" HeaderText="發行管理"
                                Text="發行">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="doc_id" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
        <asp:SqlDataSource ID="Publish_DS" runat="server"></asp:SqlDataSource>
    </form>
</body>
</html>
