﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class ISMS_31182
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim i As Integer
    Public appid As String = "5"

    Protected Sub SqlDS()

        Flow_DS.ConnectionString = Common.ConnDBS.ConnectionString
        Flow_DS.SelectCommand = "SELECT * FROM ThreatWeaknessEvaluateMaster where apprv_id='" + appid + "' and dp='" + Hidden_DP.Value + "' and form_status = 'RISK_ISSUE'   order by add_date desc"
        GridView1.DataSourceID = Flow_DS.ID
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP

        Dim ds As New DataSet
        ds = Common.Get_Asset()
        If ds.Tables(0).Rows.Count = 0 Then
            Server.Transfer("ISMS_20110.aspx")
        End If
        SqlDS()
    End Sub


    Protected Sub audit_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        'Response.Write("SELECT *  FROM riskEvaluateForm where PID=" + GridView1.Rows(Index).Cells(4).Text + "")
        RiskForm_DS.ConnectionString = Conn.ConnectionString
        RiskForm_DS.SelectCommand = "SELECT *  FROM ThreatWeaknessEvaluateForm where PID=" + GridView1.Rows(Index).Cells(4).Text + ""
        GridView2.DataSourceID = RiskForm_DS.ID

        Show_TB.Visible = True
        Main_TB.Visible = False
        'lb_riskname.Text = GridView1.Rows(Index).Cells(1).Text
        Hidden_Formid.Value = GridView1.Rows(Index).Cells(4).Text
        Hidden_FormName.Value = GridView1.Rows(Index).Cells(1).Text


    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(4).Visible = False
        e.Row.Cells(5).Visible = False
    End Sub

    Protected Sub Output_XLS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Output_XLS.Click
        Dim sw As New System.IO.StringWriter()
        Dim htw As New System.Web.UI.HtmlTextWriter(sw)
        Response.Clear()
        '檔名支援中文
        Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5")
        '避免儲存中文內容有亂碼
        'Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8")
        Response.Write("<meta http-equiv=Content-Type content=text/html;charset=utf-8>")
        'excel檔名()
        Response.AddHeader("content-disposition", "attachment;filename=" + Hidden_FormName.Value + ".xls")
        Response.ContentType = "application/vnd.ms-excel"
        GridView2.RenderControl(htw)
        Response.Write(sw.ToString())
        Response.End()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        '處理'GridView' 的控制項 'GridView' 必須置於有 runat=server 的表單標記之中
    End Sub
End Class