﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_13150
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer
    Dim uid As String
    Dim uname As String
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim wf17 As String = ConfigurationManager.AppSettings("wf17")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        uid = User.Identity.Name.ToString
        SqlDS()
    End Sub
    Protected Sub SqlDS()
        Flow_DS.ConnectionString = Common.ConnDBS.ConnectionString
        Flow_DS.SelectCommand = "SELECT * FROM CurrentFormMaster where status ='FLOW' order by form_id desc"
        GridView1.DataSourceID = Flow_DS.ID
    End Sub
    Protected Sub WF_SqlDS()
        WF_DS.ConnectionString = Common.ConnDBS.ConnectionString
        WF_DS.SelectCommand = "SELECT * FROM workflowName where apprv_id='" + hidden_apprvid.Value + "' order by wf_order, wf_inside_order"
        GridView3.DataSourceID = WF_DS.ID
    End Sub


    Protected Sub audit_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Event_TB.Visible = True
        Main_TB.Visible = False
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        Dim Ds, Ds1 As New DataSet
        Ds = Common.Get_Current_info(GridView1.Rows(Index).Cells(3).Text)
        Ds1 = Common.Get_wfFormRecord(wf17, GridView1.Rows(Index).Cells(3).Text, "")

        hidden_apprvid.Value = Ds.Tables(0).Rows(0).Item("apprv_id")

        'Response.Write(Ds.Tables(0).Rows(0).Item("douser2"))

        If Ds1.Tables(0).Rows(0).Item("wf_order") = 1 And uid = Ds.Tables(0).Rows(0).Item("douser1") Then
            current_analyze.ReadOnly = False
            current_method_yy.ReadOnly = False
            current_method_mm.ReadOnly = False
            current_method_dd.ReadOnly = False
            current_method.ReadOnly = False
            save_btn.Visible = True
        ElseIf Ds1.Tables(0).Rows(0).Item("wf_order") = 3 And uid = Ds.Tables(0).Rows(0).Item("douser2") Then
            flow_status.Enabled = True
            flow_desc.ReadOnly = False
            flow_yy.ReadOnly = False
            flow_mm.ReadOnly = False
            flow_dd.ReadOnly = False
            close_status.Enabled = True
            close_desc.ReadOnly = False
            close_yy.ReadOnly = False
            close_mm.ReadOnly = False
            close_dd.ReadOnly = False
            save_btn.Visible = True


        End If

        event_num.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_num"))
        event_source.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("event_source"))
        current_desc.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("current_desc"))

        current_analyze.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("current_analyze"))
        current_method_yy.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("current_method_yy"))
        current_method_mm.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("current_method_mm"))
        current_method_dd.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("current_method_dd"))
        current_method.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("current_method"))
        'flow_status
        flow_desc.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("flow_desc"))
        flow_yy.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("flow_yy"))
        flow_mm.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("flow_mm"))
        flow_dd.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("flow_dd"))
        'close_status
        close_desc.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("close_desc"))
        close_yy.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("close_yy"))
        close_mm.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("close_mm"))
        close_dd.Text = Common.FixNull(Ds.Tables(0).Rows(0).Item("close_dd"))

        If Common.FixNull(Ds.Tables(0).Rows(0).Item("close_status")) = "True" And Common.FixNull(Ds.Tables(0).Rows(0).Item("close_status")) <> "" Then
            close_status.Checked = True
        ElseIf Common.FixNull(Ds.Tables(0).Rows(0).Item("close_status")) = "False" And Common.FixNull(Ds.Tables(0).Rows(0).Item("close_status")) <> "" Then
            close_status.Checked = False
        End If

        If Common.FixNull(Ds.Tables(0).Rows(0).Item("flow_status")) = "True" And Common.FixNull(Ds.Tables(0).Rows(0).Item("flow_status")) <> "" Then
            flow_status.Checked = True
        ElseIf Common.FixNull(Ds.Tables(0).Rows(0).Item("flow_status")) = "False" And Common.FixNull(Ds.Tables(0).Rows(0).Item("flow_status")) <> "" Then
            flow_status.Checked = False
        End If




        Hidden_Formid.Value = GridView1.Rows(Index).Cells(3).Text
        tb2.Visible = True
        WF_SqlDS()

    End Sub


    Protected Sub GridView3_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView3.RowCreated
        e.Row.Cells(5).Visible = False

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound


        Dim oDataRow As Data.DataRowView

        Dim appid As String = wf17

        If e.Row.RowIndex <> -1 Then

            oDataRow = CType(e.Row.DataItem, Data.DataRowView)

            Dim Ds, Ds1, Ds2, Ds3 As New DataSet
            Ds = Common.Get_wfFormRecord(appid, e.Row.Cells(3).Text, "")
            Ds1 = Common.Get_Wf_ApprvUser(hidden_dp.Value, appid, "")
            Ds2 = Common.Get_Current_info(e.Row.Cells(3).Text)

            If Ds.Tables(0).Rows(0).Item("wf_order") = 0 And Ds2.Tables(0).Rows(0).Item("dousermgr") = User.Identity.Name.ToString Then
                CType(e.Row.FindControl("audit_btn"), Button).Visible = True
            ElseIf Ds.Tables(0).Rows(0).Item("wf_order") = 1 And Ds2.Tables(0).Rows(0).Item("douser1") = User.Identity.Name.ToString Then
                CType(e.Row.FindControl("audit_btn"), Button).Visible = True
            ElseIf Ds.Tables(0).Rows(0).Item("wf_order") = 2 And Ds2.Tables(0).Rows(0).Item("dousermgr1") = User.Identity.Name.ToString Then
                CType(e.Row.FindControl("audit_btn"), Button).Visible = True
            ElseIf Ds.Tables(0).Rows(0).Item("wf_order") = 3 And Ds2.Tables(0).Rows(0).Item("douser2") = User.Identity.Name.ToString Then
                CType(e.Row.FindControl("audit_btn"), Button).Visible = True
            ElseIf Ds.Tables(0).Rows(0).Item("wf_order") = 4 And Ds2.Tables(0).Rows(0).Item("dousermgr2") = User.Identity.Name.ToString Then
                CType(e.Row.FindControl("audit_btn"), Button).Visible = True
            End If



            'If Ds.Tables(0).Rows.Count >= 1 And Ds2.Tables(0).Rows(0).Item("status") = "FLOW" Then
            '    Dim Login_id, Sign_id As String
            '    Login_id = User.Identity.Name.ToString
            '    Ds3 = Common.Get_Wf_ApprvUser(hidden_dp.Value, appid, Ds.Tables(0).Rows(0).Item("wf_order") + 1)
            '    For i = 0 To Ds3.Tables(0).Rows.Count - 1
            '        Sign_id = Ds3.Tables(0).Rows(i).Item("wf_userid")
            '        If Login_id = Sign_id Then
            '            CType(e.Row.FindControl("audit_btn"), Button).Visible = True
            '        End If
            '    Next
            'End If



            'Ds4 = Common.Get_wfRecord(5, e.Row.Cells(4).Text, "")
            'For i = 0 To Ds4.Tables(0).Rows.Count - 1
            '    If i = Ds4.Tables(0).Rows.Count - 1 Then
            '        CType(e.Row.FindControl("lb_wf"), Label).Text = CType(e.Row.FindControl("lb_wf"), Label).Text + Common.Get_User_Name(Ds4.Tables(0).Rows(i).Item("wf_userid")) + "(" + Common.Get_WFStatusName(Ds4.Tables(0).Rows(i).Item("wf_status_id")) + ") =>" + Ds4.Tables(0).Rows(i).Item("wf_userid")
            '    Else
            '        CType(e.Row.FindControl("lb_wf"), Label).Text = CType(e.Row.FindControl("lb_wf"), Label).Text + Common.Get_User_Name(Ds4.Tables(0).Rows(i).Item("wf_userid")) + "(" + Common.Get_WFStatusName(Ds4.Tables(0).Rows(i).Item("wf_status_id")) + ") =>"
            '    End If
            'Next



        End If
    End Sub
    Protected Sub GridView3_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView3.RowDataBound

        Dim ds, ds1, ds2, ds3, ds4, ds5, ds6 As New DataSet
        Dim form_status, wf_status, wf_user, wf_count, wf_order, wf_user_order As String



        If e.Row.RowIndex <> -1 Then

            ds = Common.Get_wfFormRecord(hidden_apprvid.Value, Hidden_Formid.Value, "")
            ds1 = Common.Get_Current_info(Hidden_Formid.Value)
            ds2 = Common.Get_Wf_ApprvUser(hidden_dp.Value, hidden_apprvid.Value, e.Row.Cells(0).Text)
            ds3 = Common.Get_wfFormRecord(hidden_apprvid.Value, Hidden_Formid.Value, "ASC")
            ds5 = Common.Get_wfRecord(hidden_apprvid.Value, Hidden_Formid.Value, 1)
            ds6 = Common.Get_wfInside_Record(hidden_apprvid.Value, Hidden_Formid.Value, 1, e.Row.Cells(0).Text)

            'If e.Row.RowIndex >= 1 Then

            'ds4 = Common.Get_Wf_User(hidden_apprvid.Value, e.Row.Cells(0).Text, e.Row.Cells(5).Text)
            'CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds4.Tables(0).Rows(0).Item("wf_userid"))
            'CType(e.Row.FindControl("label_userid"), Label).Text = ds4.Tables(0).Rows(0).Item("wf_userid")

            'ElseIf e.Row.RowIndex = 1 And Common.FixNull(ds1.Tables(0).Rows(0).Item("status")) = "FLOW Then" Then

            '    ds4 = Common.Get_Wf_User(hidden_apprvid.Value, e.Row.Cells(0).Text, e.Row.Cells(5).Text)
            '    CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("douser"))
            '    CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("douser")

            If e.Row.RowIndex = 0 And Common.FixNull(ds1.Tables(0).Rows(0).Item("status")) = "FLOW" Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("douser"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("douser")


            ElseIf e.Row.RowIndex = 1 And Common.FixNull(ds1.Tables(0).Rows(0).Item("status")) = "FLOW" Then

                'ds4 = Common.Get_Wf_User(Hidden_DP.Value, hidden_apprvid.Value, e.Row.Cells(0).Text, e.Row.Cells(5).Text)
                'CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds4.Tables(0).Rows(0).Item("wf_userid"))
                'CType(e.Row.FindControl("label_userid"), Label).Text = ds4.Tables(0).Rows(0).Item("wf_userid")
                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("dousermgr"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("dousermgr")

            ElseIf e.Row.RowIndex = 2 And Common.FixNull(ds1.Tables(0).Rows(0).Item("status")) = "FLOW" Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("douser1"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("douser1")

            ElseIf e.Row.RowIndex = 3 And Common.FixNull(ds1.Tables(0).Rows(0).Item("status")) = "FLOW" Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("dousermgr1"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("dousermgr1")

            ElseIf e.Row.RowIndex = 4 And Common.FixNull(ds1.Tables(0).Rows(0).Item("status")) = "FLOW" Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("douser2"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("douser2")

            ElseIf e.Row.RowIndex = 5 And Common.FixNull(ds1.Tables(0).Rows(0).Item("status")) = "FLOW" Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("dousermgr2"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("dousermgr2")

                'ElseIf e.Row.RowIndex = 1 And Common.FixNull(ds1.Tables(0).Rows(0).Item("status")) = "FLOW" Then

                '    CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("douser"))
                '    CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("douser")



            End If


            Try
                wf_status = Common.FixNull(ds.Tables(0).Rows(0).Item("wf_status_id"))
                wf_order = Common.FixNull(ds.Tables(0).Rows(0).Item("wf_order"))
            Catch ex As Exception
            End Try


            form_status = Common.FixNull(ds1.Tables(0).Rows(0).Item("status"))
            wf_user_order = Common.FixNull(ds2.Tables(0).Rows(0).Item("wf_order"))
            wf_user = Common.FixNull(CType(e.Row.FindControl("label_userid"), Label).Text)
            wf_count = Common.FixNull(ds.Tables(0).Rows.Count)

            'If wf_count >= 1 Then

            If wf_order = 0 And e.Row.RowIndex = 1 And form_status = "FLOW" And ds1.Tables(0).Rows(0).Item("dousermgr") = uid Then

                CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("verify_btn"), Button).Visible = True

            ElseIf wf_order = 1 And e.Row.RowIndex = 2 And form_status = "FLOW" And ds1.Tables(0).Rows(0).Item("douser1") = uid Then

                CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("verify_btn"), Button).Visible = True

            ElseIf wf_order = 2 And e.Row.RowIndex = 3 And form_status = "FLOW" And ds1.Tables(0).Rows(0).Item("dousermgr1") = uid Then

                CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("verify_btn"), Button).Visible = True

            ElseIf wf_order = 3 And e.Row.RowIndex = 4 And form_status = "FLOW" And ds1.Tables(0).Rows(0).Item("douser2") = uid Then

                CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("verify_btn"), Button).Visible = True

            ElseIf wf_order = 4 And e.Row.RowIndex = 5 And form_status = "FLOW" And ds1.Tables(0).Rows(0).Item("dousermgr2") = uid Then

                CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("verify_btn"), Button).Visible = True


                'ElseIf form_status = "FLOW" And (ds.Tables(0).Rows(0).Item("wf_status_id") = "2" Or ds.Tables(0).Rows(0).Item("wf_status_id") = "0") And wf_count >= 1 And e.Row.RowIndex >= 1 And wf_user_order = ds.Tables(0).Rows(0).Item("wf_order") + 1 And wf_user = uid Then

                '    CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = True
                '    CType(e.Row.FindControl("comment_txt"), TextBox).Visible = True
                '    CType(e.Row.FindControl("verify_btn"), Button).Visible = True
            Else
                CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = False
                CType(e.Row.FindControl("comment_txt"), TextBox).Visible = False
                CType(e.Row.FindControl("verify_btn"), Button).Visible = False

            End If

            'Else
            '    CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = False
            '    CType(e.Row.FindControl("comment_txt"), TextBox).Visible = False
            '    CType(e.Row.FindControl("verify_btn"), Button).Visible = False


            ' End If

            '填上已簽核過的狀態與意見
            If ds6.Tables(0).Rows.Count >= 1 Then
                If ds6.Tables(0).Rows(0).Item("wf_inside_order") = e.Row.Cells(5).Text And e.Row.RowIndex >= 0 And e.Row.RowIndex <= ds5.Tables(0).Rows.Count Then
                    CType(e.Row.FindControl("lb_status"), Label).Visible = True
                    CType(e.Row.FindControl("comment_lb"), Label).Visible = True
                    CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(ds6.Tables(0).Rows(0).Item("wf_status_id")))
                    CType(e.Row.FindControl("comment_lb"), Label).Text = Common.FixNull(ds6.Tables(0).Rows(0).Item("wf_comment"))
                Else
                    CType(e.Row.FindControl("lb_status"), Label).Visible = False
                    CType(e.Row.FindControl("comment_lb"), Label).Visible = False
                End If

            End If


            '狀態選單
            If e.Row.RowIndex >= 1 Then
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(1)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
            Else
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(0)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
            End If



        End If
    End Sub



    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(3).Visible = False
        e.Row.Cells(4).Visible = False
    End Sub

    Protected Sub verify_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex


        Dim ds, ds1, ds2, ds3, Ds4, ds5 As New DataSet
        Dim form_status As String

        ds = Common.Get_Wf_ApprvUser(Hidden_DP.Value, hidden_apprvid.Value, "")
        ds5 = Common.Get_wfFormRecord(hidden_apprvid.Value, Hidden_Formid.Value, "")

        Ds4 = Common.Get_WfUser(hidden_apprvid.Value, "")

        SqlTxt = "INSERT INTO wfFormRecord " + _
         "(apprv_id,doc_id,wf_order,wf_userid,wf_status_id,wf_comment,signdate,record_id,wf_inside_order,doc_status) VALUES" + _
        "('" + hidden_apprvid.Value + "','" + Hidden_Formid.Value + "','" + oRow.Cells(0).Text + "','" + CType(GridView3.Rows(Index).FindControl("label_userid"), Label).Text + "','" + CType(GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue + "'," + _
        " '" + CType(GridView3.Rows(Index).FindControl("comment_txt"), TextBox).Text + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','1','" + oRow.Cells(5).Text + "','EVENTFORM')"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()


        ds1 = Common.Get_Current_info(Hidden_Formid.Value)
        ds2 = Common.Get_WorkFlow_MaxOrderId(hidden_apprvid.Value, Hidden_DP.Value)
        form_status = ds1.Tables(0).Rows(0).Item("status")


        If form_status = "FLOW" Then


            If CType(Me.GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue = "2" And ds5.Tables(0).Rows(0).Item("wf_order") = 4 Then

                SqlTxt = "UPDATE CurrentFormMaster  SET status = 'ISSUE' WHERE form_id='" + Hidden_Formid.Value + "' "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()

                ' Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", Hidden_Formid.Value, System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "RISKPUBLISH", lb_riskname.Text)

                'Mail通知()
                'ds3 = Common.Get_Wf_ApprvUser(5, oRow.Cells(0).Text + 1)
                'ds3 = Common.Get_User
                'Common.SendMail(ds1.Tables(0).Rows(i).Item("creator"), "Workflow_OK", docNum_LB.Text, docName_LB.Text)
                'For i = 0 To ds3.Tables(0).Rows.Count - 1
                'Common.SendMail(ds1.Tables(0).Rows(0).Item("add_user"), "Workflow_RISK_OK", "", lb_riskname.Text)
                'Next


                'ElseIf CType(Me.GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue = "2" And ds.Tables(0).Rows.Count - 1 > Index Then

                'SqlTxt = "UPDATE riskEvaluateMaster  SET Issue_date='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',form_status = 'RISK_ISSUE' WHERE form_id='" + Hidden_Formid.Value + "' "

                'Mail通知()
                'ds3 = Common.Get_Wf_ApprvUser(5, oRow.Cells(0).Text + 1)
                'ds3 = Common.Get_User
                'Common.SendMail(ds1.Tables(0).Rows(i).Item("creator"), "Workflow_OK", docNum_LB.Text, docName_LB.Text)
                'For i = 0 To ds3.Tables(0).Rows.Count - 1
                'Common.SendMail(ds3.Tables(0).Rows(0).Item("wf_userid"), "Workflow_RISK1", "", lb_riskname.Text)
                'Next



            ElseIf CType(Me.GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue = "9" Then

                SqlTxt = "UPDATE CurrentFormMaster  SET status = 'REJECT' WHERE form_id='" + Hidden_Formid.Value + "' "
                'Mail通知
                'Common.SendMail(ds1.Tables(0).Rows(0).Item("add_user"), "Workflow_RISK_KO", "", lb_riskname.Text)
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()

            End If


        End If
        tb2.Visible = False
        Main_TB.Visible = True
        Event_TB.Visible = False



    End Sub


    Protected Sub save_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles save_btn.Click
        SqlTxt = "UPDATE CurrentFormMaster  SET current_analyze='" + current_analyze.Text + "',current_method = '" + current_method.Text + "', current_method_yy='" + current_method_yy.Text + "', current_method_mm='" + current_method_mm.Text + "',current_method_dd='" + current_method_dd.Text + "',flow_status='" + flow_status.Checked.ToString + "',flow_desc='" + flow_desc.Text + "',flow_yy='" + flow_yy.Text + "',flow_mm='" + flow_mm.Text + "',flow_dd='" + flow_dd.Text + "',close_status='" + close_status.Checked.ToString + "',close_desc='" + close_desc.Text + "',close_yy='" + close_yy.Text + "',close_mm='" + close_mm.Text + "',close_dd='" + close_dd.Text + "' WHERE form_id='" + Hidden_Formid.Value + "' "
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        Common.showMsg(Me.Page, "資料儲存成功，但尚未送審!!")
    End Sub
End Class
