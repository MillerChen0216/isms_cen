﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_31170
    Inherits PageBase
    'Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP

        If Not Page.IsPostBack Then

            Common.ddl_DataRead("SELECT * FROM asset  order by asset_id", ddl_Asset, 1, 0)
            ddl_assgroup.Enabled = False

        ElseIf ddl_Asset.SelectedIndex = 0 Then
            ddl_assgroup.Enabled = False
        End If


        If ddl_Asset.SelectedIndex = 0 Or ddl_assgroup.SelectedIndex = 0 Then
            Show_Insert.Visible = False
        Else
            Show_Insert.Visible = True
        End If

        Dim ds As New DataSet
        ds = Common.Get_Asset()
        If ds.Tables(0).Rows.Count = 0 Then
            Server.Transfer("ISMS_20110.aspx")
        End If


        SqlDataSource()

    End Sub

    Protected Sub SqlDataSource()
        SqlDataSource1.ConnectionString = Conn.ConnectionString
        SqlDataSource1.SelectCommand = "SELECT a.id,a.risktype_id,c.risk_type,a.assgroup_id,b.assgroup_name,a.weakness_id,d.weakness,a.threat_id,e.threat,a.riskp_score,a.impact_score ,a.riskp_score " + Common.Sys_Config("Risk_operation") + " a.impact_score as tw_score FROM ThreatWeaknessEvaluate as a,assetGroup as b,risk_type as c,weakness as d,threat as e WHERE a.dp='" + Hidden_DP.Value + "' and a.assgroup_id=b.assgroup_id AND a.risktype_id=c.risk_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND  a.asset_id=" + ddl_Asset.SelectedValue + "  AND a.assgroup_id='" + ddl_assgroup.SelectedValue + "'"
        'Response.Write("SELECT a.id,a.risktype_id,b.risk_type,a.weakness_id,d.weakness,a.threat_id,e.threat,a.riskp_score,a.impact_score ,a.riskp_score " + Common.Sys_Config("Risk_operation") + " a.impact_score as tw_score FROM ThreatWeaknessEvaluate as a,risk_type as b,weakness as d,threat as e WHERE a.risktype_id=b.risk_id and a.asset_id = d.asset_id AND a.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND  a.asset_id=" + ddl_Asset.SelectedValue + "  AND a.risktype_id=" + ddl_risktype.SelectedValue + "")
        GridView1.DataSourceID = SqlDataSource1.ID
        Dim KeyNames() As String = {"ID"}
        GridView1.DataKeyNames = KeyNames
    End Sub
    Protected Sub ShowInsddl()
        ddl_risktype_ins.DataSource = Common.Get_assetidWithRiskType(ddl_Asset.SelectedValue)
        ddl_risktype_ins.DataValueField = "risk_id"
        ddl_risktype_ins.DataTextField = "risk_type"
        ddl_risktype_ins.DataBind()

        ddl_Cweakness_ins.DataSource = Common.Get_weakness(ddl_risktype_ins.SelectedValue)
        ddl_Cweakness_ins.DataValueField = "weakness_id"
        ddl_Cweakness_ins.DataTextField = "weakness"
        ddl_Cweakness_ins.DataBind()

        ddl_Cthreat_ins.DataSource = Common.Get_threat(ddl_risktype_ins.SelectedValue)
        ddl_Cthreat_ins.DataValueField = "threat_id"
        ddl_Cthreat_ins.DataTextField = "threat"
        ddl_Cthreat_ins.DataBind()

        ddl_Criskp_ins.DataSource = Common.Get_riskP()
        ddl_Criskp_ins.DataValueField = "riskP_score"
        ddl_Criskp_ins.DataTextField = "riskP_desc"
        ddl_Criskp_ins.DataBind()

        ddl_Cimpact_ins.DataSource = Common.Get_riskImpact()
        ddl_Cimpact_ins.DataValueField = "impact_score"
        ddl_Cimpact_ins.DataTextField = "impact_desc"
        ddl_Cimpact_ins.DataBind()

    End Sub
    Protected Sub ShowUpdddl()
        ddl_risktype_upd.DataSource = Common.Get_assetidWithRiskType(ddl_Asset.SelectedValue)
        ddl_risktype_upd.DataValueField = "risk_id"
        ddl_risktype_upd.DataTextField = "risk_type"
        ddl_risktype_upd.DataBind()

        ddl_weakness_upd.DataSource = Common.Get_weakness(ddl_Asset.SelectedValue)
        ddl_weakness_upd.DataValueField = "weakness_id"
        ddl_weakness_upd.DataTextField = "weakness"
        ddl_weakness_upd.DataBind()

        ddl_threat_upd.DataSource = Common.Get_threat(ddl_Asset.SelectedValue)
        ddl_threat_upd.DataValueField = "threat_id"
        ddl_threat_upd.DataTextField = "threat"
        ddl_threat_upd.DataBind()

        ddl_riskp_upd.DataSource = Common.Get_riskP()
        ddl_riskp_upd.DataValueField = "riskP_score"
        ddl_riskp_upd.DataTextField = "riskP_desc"
        ddl_riskp_upd.DataBind()

        ddl_impact_upd.DataSource = Common.Get_riskImpact()
        ddl_impact_upd.DataValueField = "impact_score"
        ddl_impact_upd.DataTextField = "impact_desc"
        ddl_impact_upd.DataBind()

    End Sub

    Protected Sub GridView1_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.PreRender
        Dim i As Integer = 1
        Dim j As Integer = 0
        Dim k As Integer
        Dim mySingleRow As GridViewRow

        For Each mySingleRow In GridView1.Rows

            If CInt(mySingleRow.RowIndex) = 0 Then

                For k = 0 To 1
                    mySingleRow.Cells(k).RowSpan = 1
                Next


            Else
                If mySingleRow.Cells(0).Text.Trim() = GridView1.Rows(CInt(mySingleRow.RowIndex) - i).Cells(0).Text.Trim() And mySingleRow.Cells(1).Text.Trim() = GridView1.Rows(CInt(mySingleRow.RowIndex) - i).Cells(1).Text.Trim() Then

                    For k = 0 To 1
                        GridView1.Rows(CInt(mySingleRow.RowIndex) - i).Cells(k).RowSpan += 1
                    Next

                    i = i + 1
                    For k = 0 To 1
                        mySingleRow.Cells(k).Visible = False
                    Next


                Else
                    For k = 0 To 1
                        GridView1.Rows(CInt(mySingleRow.RowIndex)).Cells(k).RowSpan = 1
                    Next
                    i = 1
                End If
            End If
        Next


    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)

        Select Case e.CommandName

            Case "Edit_Btn"

                Hidden_ID.Value = SelectedRow.Cells(10).Text
                asset_ed_lb.Text = ddl_Asset.SelectedItem.Text
                assgroup_ed_lb.Text = ddl_assgroup.SelectedItem.Text
                Edit_TB.Visible = True
                Show_TB.Visible = False
                ShowUpdddl()
                ddl_risktype_upd.SelectedValue = SelectedRow.Cells(12).Text
                ddl_weakness_upd.SelectedValue = SelectedRow.Cells(8).Text
                ddl_threat_upd.SelectedValue = SelectedRow.Cells(9).Text
                ddl_riskp_upd.SelectedValue = SelectedRow.Cells(3).Text
                ddl_impact_upd.SelectedValue = SelectedRow.Cells(4).Text

            Case "Del_Btn"
                SqlTxt = "DELETE FROM ThreatWeaknessEvaluate WHERE id =" + SelectedRow.Cells(10).Text + ""

                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                'Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DEL", SelectedRow.Cells(0).Text + "-" + SelectedRow.Cells(1).Text)

                GridView1.DataBind()

        End Select

    End Sub

    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click

        SqlTxt = "UPDATE  ThreatWeaknessEvaluate SET weakness_id =" + ddl_weakness_upd.SelectedValue + ", threat_id =" + ddl_threat_upd.SelectedValue + ",riskp_score=" + ddl_riskp_upd.SelectedValue + ",impact_score=" + ddl_impact_upd.SelectedValue + " WHERE ID =" + Hidden_ID.Value + ""
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        'Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", assitem_name_upd.Text + "-" + DDLRisk_typeUpd.SelectedItem.Text + "-" + ddl_weakness_upd.SelectedValue + "-" + ddl_threat_upd.SelectedValue + "-" + ddl_riskp_upd.SelectedValue + "-" + ddl_impact_upd.SelectedValue)

        GridView1.DataBind()
        Show_TB.Visible = True
        Edit_TB.Visible = False


    End Sub

    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        Dim ds As New DataSet



        SqlTxt = "INSERT INTO ThreatWeaknessEvaluate (dp,asset_id,risktype_id,assgroup_id,weakness_id,threat_id,riskp_score,impact_score) VALUES ('" + Hidden_DP.Value + "','" + ddl_Asset.SelectedValue + "','" + ddl_risktype_ins.SelectedValue + "','" + ddl_assgroup.SelectedValue + "'," + ddl_Cweakness_ins.SelectedValue + "," + ddl_Cthreat_ins.SelectedValue + "," + ddl_Criskp_ins.SelectedValue + "," + ddl_Cimpact_ins.SelectedValue + ")"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()
        'Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", ddl_assitem_id_ins.SelectedItem.Text + "-" + DDLRisk_typeIns.SelectedItem.Text + "-" + ddl_Cweakness_ins.SelectedItem.Text + "-" + ddl_Cthreat_ins.SelectedItem.Text + "-" + ddl_Criskp_ins.SelectedItem.Text + "-" + ddl_Cimpact_ins.SelectedItem.Text)
        GridView1.DataBind()

        Show_TB.Visible = True
        Ins_TB.Visible = False

    End Sub

    Protected Sub Show_Insert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert.Click

        Show_TB.Visible = False
        Ins_TB.Visible = True
        ShowInsddl()
        asset_ins_lb.Text = ddl_Asset.SelectedItem.Text
        assgroup_ins_lb.Text = ddl_assgroup.SelectedItem.Text


    End Sub
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow OrElse e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(8).Visible = False
            e.Row.Cells(9).Visible = False
            e.Row.Cells(10).Visible = False
            e.Row.Cells(11).Visible = False
            e.Row.Cells(12).Visible = False

        End If

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        Dim oDataRow As Data.DataRowView
        Dim sShowField As String
        Dim sMessage As String
        Dim Ds As New DataSet
        If e.Row.RowIndex <> -1 Then

            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = oDataRow.Item("assgroup_name").ToString + "-" + oDataRow.Item("threat").ToString + "-" + oDataRow.Item("weakness").ToString
            sMessage = String.Format("您確定要刪除 [{0}] 嗎?", sShowField)
            CType(e.Row.Cells(7).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"

        End If
    End Sub


    Protected Sub ddl_Asset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Asset.SelectedIndexChanged
        ddl_assgroup.Items.Clear()
        ddl_assgroup.Items.Add(New ListItem("請選擇資產群組", 0))

        If ddl_Asset.SelectedIndex = 0 Then
            ddl_assgroup.Enabled = False
            Show_Insert.Visible = False

        Else
            ddl_assgroup.Enabled = True
            Show_Insert.Visible = False
            Common.ddl_DataRead("SELECT   *  FROM   assetGroup  where asset_id=" + ddl_Asset.SelectedValue + "", ddl_assgroup, 2, 1)

        End If
    End Sub


    Protected Sub ddl_risktype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_assgroup.SelectedIndexChanged
        If ddl_assgroup.SelectedIndex = 0 Then
            Show_Insert.Visible = False
        Else
            Show_Insert.Visible = True
        End If

    End Sub

    Protected Sub ddl_risktype_ins_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_risktype_ins.SelectedIndexChanged
        ddl_Cweakness_ins.DataSource = Common.Get_weakness(ddl_risktype_ins.SelectedValue)
        ddl_Cweakness_ins.DataValueField = "weakness_id"
        ddl_Cweakness_ins.DataTextField = "weakness"
        ddl_Cweakness_ins.DataBind()

        ddl_Cthreat_ins.DataSource = Common.Get_threat(ddl_risktype_ins.SelectedValue)
        ddl_Cthreat_ins.DataValueField = "threat_id"
        ddl_Cthreat_ins.DataTextField = "threat"
        ddl_Cthreat_ins.DataBind()
    End Sub
End Class
