﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.UI.UserControl
Imports System.Web.UI

Partial Class Netconnect_flow
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlCmd As SqlCommand
    Dim uid As String = User.Identity.Name '使用者名稱
    Dim uname As String = Common.Get_User_Name(User.Identity.Name.ToString) '使用者代號
    Dim SqlTxt As String
    Dim approvalid As String
    Dim apprvid As String
    Dim dp As String = Me.Get_DP
    Dim File_Path As String = ConfigurationManager.AppSettings("Netconnect_Path")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        approvalid = ConfigurationManager.AppSettings("flow_Netconnect") '15
        apprvid = ConfigurationManager.AppSettings("flow_Netconnect") '15
        Gdv1Show() '待審核表
    End Sub

    '待審核表來源
    Protected Sub Gdv1Show()
        Try
            Dim sql = " select * from  form_record a join Netconnect b on  a.recordid=b.recordid where b.using = 'Y' and approvalid='" + approvalid + "' and a.status=1 and userid='" + uid + "' order by b.applyDate"
            Dim data As DataTable = Common.Con(sql)
            GridView1.DataSource = data
            GridView1.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '簽核過程及民國年轉換
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '簽核過程
                Dim recordid As String = CType(e.Row.FindControl("lb_rowid"), Label).Text
                CType(e.Row.FindControl("lb_flow"), Label).Text = Common.Str_form_record_isms(approvalid, recordid)
                '民國年轉換
                Dim lb_applydate As String = CType(e.Row.FindControl("lb_applydate"), Label).Text
                CType(e.Row.FindControl("lb_applydate"), Label).Text = Common.Year(Date.Parse(lb_applydate))
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub


    '審核表畫面至審核畫面_1
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Try
            Dim Index As Integer = CType(e.CommandArgument, Int32)
            Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
            Dim lb_rowid As String = (TryCast(SelectedRow.FindControl("lb_rowid"), Label)).Text

            Select Case e.CommandName
                Case "audit_btn"
                    Dim sql = "  SELECT * FROM Netconnect WHERE recordId = '" + lb_rowid + "'"
                    Dim data As DataTable = Common.Con(sql)
                    If data.Rows.Count > 0 Then
                        '紀錄該表單編號
                        lb_recordid.Text = lb_rowid
                        '載入關卡表
                        GdvShow()


                        DefaultSource() '載入預設資料

                        '填入該lb_rowid資料

                        '處理checkbox
                        Dim s_ip_1 = If(data.Rows(0)("s_ip_1") = "1", True, False)
                        Dim s_ip_2 = If(data.Rows(0)("s_ip_2") = "1", True, False)


                        applyDate.Text = Common.Year(data.Rows(0)("applyDate")) '申請日期
                        recordId.Text = data.Rows(0)("recordId") '表單編號
                        empName.Text = data.Rows(0)("empName") '申請代表人
                        applyOrg.Text = data.Rows(0)("applyOrg") '申請單位
                        applyName.Text = data.Rows(0)("applyName") '申請人員
                        applyTel1.Text = data.Rows(0)("applyTel1") '連絡電話1
                        applyTel2.Text = data.Rows(0)("applyTel2") '連絡電話2
                        Netconnect_recordId.Text = data.Rows(0)("Netconnect_recordId") '原單號
                        Ddl_Syscd.SelectedValue = data.Rows(0)("syscd_Id") '系統名稱
                        startTime_d.Text = Common.Year(data.Rows(0)("startTime_d")) '連線時間(起)_日
                        Ddl_StartHour.SelectedValue = data.Rows(0)("startTime_h") '連線時間(起)_時
                        Ddl_StartMin.SelectedValue = data.Rows(0)("startTime_m") '連線時間(起)_分
                        endTime_d.Text = Common.Year(data.Rows(0)("endTime_d")) '連線時間(迄)_日
                        Ddl_EndHour.Text = data.Rows(0)("endTime_h") '連線時間(迄)_時
                        Ddl_EndMin.Text = data.Rows(0)("endTime_m") '連線時間(迄)_分


                        sql = "  SELECT * FROM Netconnect WHERE recordId = '" + Netconnect_recordId.Text + "'"
                        Dim data2 As DataTable = Common.Con(sql)
                        If data2.Rows.Count > 0 Then
                            ori_applyReason.Text = data2.Rows(0)("applyReason") '原申請事由
                        End If


                        applyReason.Text = data.Rows(0)("applyReason") '先清空接下來使用者要填的申請事由
                        chk_s_ip_1.Checked = s_ip_1 '單一IP
                        s_ip_1_content.Text = data.Rows(0)("s_ip_1_content")
                        chk_s_ip_2.Checked = s_ip_2 '網段(區間)IP
                        s_ip_2_content.Text = data.Rows(0)("s_ip_2_content")
                        s_ip_Other.Text = data.Rows(0)("s_ip_Other") '協定/來源 Port
                        d_ip_content.Text = data.Rows(0)("d_ip_content") '目的IP
                        d_ip_other.Text = data.Rows(0)("d_ip_other") '協定/目的 Port
                        Netconnect_recordId.Text = data.Rows(0)("Netconnect_recordId") '申請事項的原表單編號
                        '申請事項
                        Select Case data.Rows(0)("applyItem")
                            Case "1"
                                applyItem.Text = "新增"
                                applyItemEnabled(False)
                                oriTb_1.ColSpan = 3
                            Case "2"
                                applyItem.Text = "異動"
                                applyItemEnabled(True)
                                oriTb_1.ColSpan = 1
                            Case "3"
                                applyItem.Text = "註銷"
                                applyItemEnabled(True)
                                oriTb_1.ColSpan = 1
                        End Select


                        '查看裡面是否有資料，有才秀出來
                        If IsDBNull(data.Rows(0)("applyFile")) Then
                            applyFile.Visible = False
                        Else
                            applyFile_link.Text = data.Rows(0)("applyFile")
                            applyFile.Visible = True
                        End If

                        Dim workflowId As Integer = Common.Get_workflowId(approvalid, recordId.Text) '取得關卡

                        If workflowId = 0 Then
                            If Not applyItem.Text = "註銷" Then
                                File_applyFile.Visible = True
                                SheetEnabled(True)
                            Else
                                SheetEnabled(False)
                                applyReason.Enabled = True '申請事由
                            End If
                        Else
                            SheetEnabled(False)
                            File_applyFile.Visible = False
                        End If

                        '大於等於辦理人關卡(先預設第1關)
                        If workflowId >= 1 Then
                            '查看裡面是否有資料，有才秀出來
                            '附件檔案
                            If IsDBNull(data.Rows(0)("verifyFile")) Then
                                verifyFile.Visible = False
                            Else
                                verifyFile.Visible = True
                                verifyFile_link.Text = data.Rows(0)("verifyFile")
                            End If
                            '辦理日期
                            If Not IsDBNull(data.Rows(0)("verifyTime")) Then
                                verifyTime.Text = Common.Year(data.Rows(0)("verifyTime"))
                            Else
                                verifyTime.Text = Common.Year(DateTime.Now)
                            End If

                            verifyTb.Visible = True
                            '如果再辦理人關卡就開放編輯
                            If workflowId = 1 Then
                                verifyTime.Enabled = True
                                File_verifyFile.Visible = True
                            Else
                                verifyTime.Enabled = False
                                File_verifyFile.Visible = False
                            End If
                        Else
                            verifyTb.Visible = False
                        End If


                        '將審核畫面弄出來
                        tb_result.Visible = True
                        tr_gridview.Visible = False
                        Insert_Btn.Visible = True
                        but_esc.Visible = True
                    Else
                        Response.Redirect("Netconnect_flow.aspx", True) '重新載入附件表單簽核_審核 因為此表單編號已經不存在
                    End If


            End Select
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub

    '表單的Enabled
    Private Sub applyItemEnabled(ByVal use As Boolean)
        Try
            '若選到新增不用選原單號
            oriTb_2.Visible = use
            oriTb_3.Visible = use
            ori_applyReasonTb_1.Visible = use
            ori_applyReasonTb_2.Visible = use

        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '預設資料來源
    Private Sub DefaultSource()
        Try
            '時:分時間下拉
            Ddl_StartHour.Items.Clear()
            Ddl_StartMin.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                Ddl_StartHour.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                Ddl_StartMin.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next

            Ddl_EndHour.Items.Clear()
            Ddl_EndMin.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                Ddl_EndHour.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                Ddl_EndMin.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next


            '系統名稱
            Dim sql As String = "SELECT * FROM SysCode WHERE syscd_Enabled = 1"
            sql += " ORDER BY syscd_time DESC "
            Ddl_Syscd.Items.Clear()
            Ddl_Syscd.DataTextField = "syscd_Name"
            Ddl_Syscd.DataValueField = "syscd_Id"
            Ddl_Syscd.DataSource = Common.Con(sql)
            Ddl_Syscd.DataBind()
            Ddl_Syscd.SelectedValue = Ddl_Syscd.Items(0).Value '預設選項為第一個值

        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '小工具1
    Private Sub SheetEnabled(ByVal use As Boolean)
        applyOrg.Enabled = use '申請單位
        applyName.Enabled = use '申請人員
        applyTel1.Enabled = use '連絡電話1
        applyTel2.Enabled = use '連絡電話2
        Ddl_Syscd.Enabled = use '系統名稱
        startTime_d.Enabled = use '連線時間(起)_日
        Ddl_StartHour.Enabled = use '連線時間(起)_時
        Ddl_StartMin.Enabled = use '連線時間(起)_分
        endTime_d.Enabled = use '連線時間(迄)_日
        Ddl_EndHour.Enabled = use '連線時間(迄)_時
        Ddl_EndMin.Enabled = use '連線時間(迄)_分
        applyReason.Enabled = use '申請事由
        chk_s_ip_1.Enabled = use '單一IP
        s_ip_1_content.Enabled = use
        chk_s_ip_2.Enabled = use '網段(區間)IP
        s_ip_2_content.Enabled = use
        s_ip_Other.Enabled = use '協定/來源 Port
        d_ip_content.Enabled = use '目的IP
        d_ip_other.Enabled = use '協定/目的 Port
    End Sub

    '關卡表
    Private Sub GdvShow()
        Try
            Dim dt As DataTable = Common.Qry_workflowName_isms_Req(approvalid, lb_recordid.Text, apprvid, dp)
            GridView.DataSource = dt
            GridView.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try

    End Sub



    '關卡表_RowDataBound
    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '***已簽核資料

                '***人員
                Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
                Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
                Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
                Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
                '***狀態
                Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
                Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
                Dim hid_status As HiddenField = CType(e.Row.FindControl("hid_status"), HiddenField)
                '***意見
                Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
                Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
                '***關卡***
                Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text
                '***審核人員***
                'Dim lb_wf_inside_order As String = CType(e.Row.FindControl("lb_wf_inside_order"), Label).Text

                Dim ds As DataSet


                '***Start
                If o_wf_order = "0" And hid_status.Value <> "1" Then
                    pan_user.Visible = True
                    '將審核人員設為自己
                    Dim sql = "    SELECT user_id, user_name FROM ismsUser WHERE user_id = '" + uid + "'"
                    Dim data As DataTable = Common.Con(sql)
                    CType(e.Row.FindControl("lb_username"), Label).Text = data.Rows(0)("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = data.Rows(0)("user_id")
                ElseIf hid_status.Value = "1" Then '待審核
                    ds = Common.Qry_form_record_wait(lb_recordid.Text)
                    pan_user.Visible = True
                    CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
                    lb_count.Text = e.Row.RowIndex
                ElseIf hid_status.Value = "2" Or hid_status.Value = "9" Then '審核已通過
                    ds = Common.Qry_form_record_name(lb_recordid.Text, o_wf_order)
                    pan_user.Visible = True
                    CType(e.Row.FindControl("lb_username"), Label).Text = ds.Tables(0).Rows(0).Item("user_name")
                    CType(e.Row.FindControl("label_userid"), Label).Text = ds.Tables(0).Rows(0).Item("user_id")
                ElseIf e.Row.RowIndex = CType(lb_count.Text, Integer) + 1 Then '待審核下一關卡
                    ddl_user.Visible = True
                    ds = Common.Get_Wf_User(dp, apprvid, o_wf_order, "")
                    ddl_user.DataSource = ds
                    ddl_user.DataTextField = "name_id"
                    ddl_user.DataValueField = "wf_userid"
                    ddl_user.DataBind()
                End If

                If hid_status.Value = "1" Then '待審核
                    ddl_status.Visible = True
                    lb_status.Visible = False
                    txt_comment.Visible = True
                    lb_comment.Visible = False
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(1)
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                    CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()

                ElseIf hid_status.Value = "" Then
                    ddl_status.Visible = False
                    lb_status.Visible = True
                    txt_comment.Visible = False
                    lb_comment.Visible = True
                Else
                    ddl_status.Visible = False
                    lb_status.Visible = True
                    txt_comment.Visible = False
                    lb_comment.Visible = True
                    CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(hid_status.Value))
                End If
            End If
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '檔案下載_按鈕
    Protected Sub applyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + applyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '檔案下載_按鈕(辦理人員)
    Protected Sub verifyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles verifyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + verifyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub


    '返回列表_按鈕
    Protected Sub but_esc_click()
        tr_gridview.Visible = True
        tb_result.Visible = False
        Insert_Btn.Visible = False
        but_esc.Visible = False
        Gdv1Show()
    End Sub

    '送出審核_按鈕
    Protected Sub Insert_Btn_click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim index As Integer = CType(lb_count.Text, Integer)
        Dim max_index As Integer = GridView.Rows.Count

        Dim wf_order As String = CType(GridView.Rows(index).FindControl("wf_order"), Label).Text '待審核的關卡
        Dim status As String = CType(GridView.Rows(index).FindControl("ddl_status"), DropDownList).SelectedValue '狀態
        Dim comment As String = CType(GridView.Rows(index).FindControl("txb_comment"), TextBox).Text
        Dim id As String = CType(GridView.Rows(index).FindControl("lb_id2"), Label).Text
        Dim stage As Integer = Common.Get_workflowId(approvalid, recordId.Text) '取得關卡

        If status = 2 Then '通過
            If index + 1 < GridView.Rows.Count Then '流程尚未到最後關卡

                If stage = 0 Then
                    '回到第0關
                    If String.IsNullOrEmpty(applyOrg.Text) Or String.IsNullOrEmpty(applyName.Text) Or (String.IsNullOrEmpty(applyTel1.Text) And String.IsNullOrEmpty(applyTel2.Text)) _
                            Or String.IsNullOrEmpty(applyReason.Text) _
                            Or String.IsNullOrEmpty(s_ip_Other.Text) Or String.IsNullOrEmpty(d_ip_content.Text) Or String.IsNullOrEmpty(d_ip_other.Text) _
                            Or ((Not chk_s_ip_1.Checked Or String.IsNullOrEmpty(s_ip_1_content.Text)) And (Not chk_s_ip_2.Checked Or String.IsNullOrEmpty(s_ip_2_content.Text))) Then
                        Common.showMsg(Me.Page, "請確實填寫資訊")
                        Return
                    End If

                    '處理資訊系統軟體代號
                    Dim sql2 = "select * from SysCode where syscd_Id = '" + Ddl_Syscd.SelectedValue + "'"
                    Dim data2 As DataTable = Common.Con(sql2)
                    Dim syscd_Code = data2.Rows(0)("syscd_Code")

                    '處理checkbox
                    Dim s_ip_1 = If(chk_s_ip_1.Checked, "1", "0")
                    Dim s_ip_2 = If(chk_s_ip_2.Checked, "1", "0")

                    If Not chk_s_ip_1.Checked Then
                        s_ip_1_content.Text = ""
                    End If

                    If Not chk_s_ip_2.Checked Then
                        s_ip_2_content.Text = ""
                    End If


                    '處理上傳檔案
                    If CType(Me.FindControl("File_applyFile"), FileUpload).HasFile Then
                        '有檔案
                        Dim oriFileName = CType(Me.FindControl("File_applyFile"), FileUpload)
                        Dim UpFileName As String = Common.UpFile(File_Path, recordId.Text, CType(Me.FindControl("File_applyFile"), FileUpload))
                        '塞入主表資料
                        SqlTxt = " update Netconnect set applyOrg = '" + applyOrg.Text + "',applyName = '" + applyName.Text + "',applyTel1 = '" + applyTel1.Text + "',applyTel2 = '" + applyTel2.Text + "'," _
                            + "Netconnect_recordId = '" + Netconnect_recordId.Text + "',syscd_Id = '" + Ddl_Syscd.SelectedValue + "',syscd_Code = '" + syscd_Code + "',syscd_Name = '" + Ddl_Syscd.SelectedItem.Text + "'," _
                            + "startTime_d = '" + Common.YearYYYY(startTime_d.Text) + "',startTime_h = '" + Ddl_StartHour.SelectedValue + "',startTime_m = '" + Ddl_StartMin.SelectedValue + "'," _
                            + "endTime_d = '" + Common.YearYYYY(endTime_d.Text) + "',endTime_h = '" + Ddl_EndHour.SelectedValue + "',endTime_m = '" + Ddl_EndMin.SelectedValue + "',applyReason = '" + applyReason.Text + "'," _
                            + "s_ip_1 = '" + s_ip_1 + "',s_ip_1_content = '" + s_ip_1_content.Text + "',s_ip_2 = '" + s_ip_2 + "',s_ip_2_content = '" + s_ip_2_content.Text + "',s_ip_Other = '" + s_ip_Other.Text + "'," _
                            + "d_ip_content = '" + d_ip_content.Text + "',d_ip_other = '" + d_ip_other.Text + "',applyFile_ori_name = '" + oriFileName.FileName + "',applyFile = '" + UpFileName + "',op_time = getdate() where recordId = '" + recordId.Text + "'"

                    Else
                        '沒有檔案
                        '塞入主表資料
                        SqlTxt = " update Netconnect set applyOrg = '" + applyOrg.Text + "',applyName = '" + applyName.Text + "',applyTel1 = '" + applyTel1.Text + "',applyTel2 = '" + applyTel2.Text + "'," _
                            + "Netconnect_recordId = '" + Netconnect_recordId.Text + "',syscd_Id = '" + Ddl_Syscd.SelectedValue + "',syscd_Code = '" + syscd_Code + "',syscd_Name = '" + Ddl_Syscd.SelectedItem.Text + "'," _
                            + "startTime_d = '" + Common.YearYYYY(startTime_d.Text) + "',startTime_h = '" + Ddl_StartHour.SelectedValue + "',startTime_m = '" + Ddl_StartMin.SelectedValue + "'," _
                            + "endTime_d = '" + Common.YearYYYY(endTime_d.Text) + "',endTime_h = '" + Ddl_EndHour.SelectedValue + "',endTime_m = '" + Ddl_EndMin.SelectedValue + "',applyReason = '" + applyReason.Text + "'," _
                            + "s_ip_1 = '" + s_ip_1 + "',s_ip_1_content = '" + s_ip_1_content.Text + "',s_ip_2 = '" + s_ip_2 + "',s_ip_2_content = '" + s_ip_2_content.Text + "',s_ip_Other = '" + s_ip_Other.Text + "'," _
                            + "d_ip_content = '" + d_ip_content.Text + "',d_ip_other = '" + d_ip_other.Text + "',op_time = getdate() where recordId = '" + recordId.Text + "'"

                    End If

                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                End If

                If stage = 1 Then
                    '來到第一關(辦理人員)
                    If String.IsNullOrEmpty(verifyTime.Text) Then
                        Common.showMsg(Me.Page, "請確實填寫資訊")
                        Return
                    End If

                    '處理上傳檔案
                    If CType(Me.FindControl("File_verifyFile"), FileUpload).HasFile Then
                        '有檔案
                        Dim oriFileName = CType(Me.FindControl("File_verifyFile"), FileUpload)
                        Dim UpFileName As String = Common.UpFile(File_Path, recordId.Text, CType(Me.FindControl("File_verifyFile"), FileUpload))
                        '塞入主表資料
                        SqlTxt = " update Netconnect set verifyTime = '" + Common.YearYYYY(verifyTime.Text) + "', verifyFile_ori_name = '" + oriFileName.FileName + "', verifyFile = '" + UpFileName + "', op_time = getdate() where recordId = '" + recordId.Text + "'"

                    Else
                        '沒有檔案
                        '塞入主表資料
                        SqlTxt = " update Netconnect set verifyTime = '" + Common.YearYYYY(verifyTime.Text) + "', op_time = getdate() where recordId = '" + recordId.Text + "'"
                    End If

                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()
                End If

                Dim workflowid As String = CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) + 1
                Dim user_id = CType(GridView.Rows(index + 1).FindControl("ddl_user"), DropDownList).SelectedValue

                SqlTxt = " INSERT INTO form_record (approvalId,workflowId,userId,userType,status,comment,recordId,signDate,role,dp) VALUES "
                SqlTxt += " ('" + approvalid + "','" + workflowid + "','" + user_id + "','land','1','','" + lb_recordid.Text + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + workflowid + "','" + dp + "') "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            End If
            SqlTxt = " UPDATE form_record SET status='2' , signDate ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' , comment = '" + comment + "' WHERE status != 2 and status !=0 and approvalId='" + approvalid + "'and recordId ='" + lb_recordid.Text + "' and id ='" + id + "' "
        ElseIf status = 9 Then '退回
            If CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) > 0 Then '目前流程不是第一關卡
                Dim workflowid As String = CType(CType(GridView.Rows(lb_count.Text).FindControl("wf_order"), Label).Text, Integer) - 1
                'Dim user_id = CType(GridView.Rows(index - 1).FindControl("label_userid"), Label).Text
                Dim dt As DataTable = Common.Qry_form_record1(approvalid, lb_recordid.Text, workflowid)
                Dim user_id As String = dt.Rows(0).Item("userId").ToString

                SqlTxt = " INSERT INTO form_record (approvalId,workflowId,userId,userType,status,comment,recordId,signDate,role,dp) VALUES "
                SqlTxt += " ('" + approvalid + "','" + workflowid + "','" + user_id + "','land','1','','" + lb_recordid.Text + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + workflowid + "','" + dp + "') "

                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
            Else
                Common.showMsg(Me.Page, "流程已到填表人，無法再退回！")
                Return
            End If

            SqlTxt = " UPDATE form_record SET status='9' , signDate ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' , comment = '" + comment + "' WHERE approvalId='" + approvalid + "'and recordId ='" + lb_recordid.Text + "' and id ='" + id + "' "

        End If
        Dim cmd As New SqlCommand(SqlTxt.ToString)

        Try
            Conn = Common.ConnDBS : Conn.Open() : cmd.Connection = Conn
            If cmd.ExecuteNonQuery() Then Common.showMsg(Me.Page, "審核完成！") : but_esc_click()
        Catch ex As Exception
            Common.showMsg(Me.Page, "審核失敗！")
        Finally
            Conn.Close() : Conn.Close() : Conn.Dispose() : cmd.Dispose()
        End Try
    End Sub
End Class
