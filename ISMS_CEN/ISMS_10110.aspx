﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_10110.aspx.vb" Inherits="ISMS_10110" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Assembly Name="System.Web.Extensions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   <title>未命名頁面</title> 
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:HiddenField ID="Hidden_DP" runat="server" />
        <br />
        <table id="Doc_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 750px" visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    <asp:Label ID="LB_List" runat="server"></asp:Label>
                    | 列表&nbsp;
                    <asp:Button ID="Ins_DocBtn" runat="server" Text="新增文件" /></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        CssClass="tb_1" Width="750px">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/news_icon.gif" />
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="doc_num" HeaderText="文件編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_name" HeaderText="文件名稱">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_version" HeaderText="版次">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="doc_capsule" HeaderText="文件簡述">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                            <asp:BoundField DataField="issue_date" HeaderText="發行日期" DataFormatString="{0:d}">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>                               
                            <asp:TemplateField HeaderText="管理" ShowHeader="False">
                                <ItemTemplate>
                                    &nbsp;<asp:Button ID="Rev_Doc" runat="server" CommandName="Rev_Doc" Text="改版" OnClick="Rev_Doc_Click" />&nbsp;<asp:Button
                                        ID="Disable_Doc" runat="server" CommandName="Disable_Doc" Text="廢止" OnClick="Disable_Doc_Click" />
                                </ItemTemplate>
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="doc_id" HeaderText="編號">
                                <HeaderStyle CssClass="tb_title_w_1" />
                                <ItemStyle CssClass="tb_w_1" />
                            </asp:BoundField>
                         
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
        <asp:SqlDataSource ID="Doc1_DS" runat="server"></asp:SqlDataSource>
        &nbsp;
      <table id="Ins_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 750px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6" style="height: 27px">
                    <img src="Images/exe.gif" />
                    <asp:Label ID="LB_ADD" runat="server"></asp:Label>
                    | 新增資料</td>
            </tr>
            <tr>
                <td class="tb_title_w_1" >
                    申請日期</td>
                <td class="tb_w_1_left" >
                    <asp:Label ID="Author_date" runat="server" Width="84px"></asp:Label></td>       
            </tr>
            <tr>
                  <td class="tb_title_w_1" >
                    申請單位</td>
                 <td class="tb_w_1_left" >
                    <asp:Label ID="dept" runat="server" Width="113px" ></asp:Label></td>           
            </tr>
            <tr>
                 <td class="tb_title_w_1">
                    申請人</td>
                <td class="tb_w_1_left" >
                    <asp:Label ID="Author" runat="server" Width="111px"></asp:Label></td>        
            </tr>
            <tr>
                <td class="tb_title_w_1" >
                    文件編號</td>
                <td class="tb_w_1_left" >
                    <asp:TextBox ID="Doc_num" runat="server" Width="85px" style="TEXT-TRANSFORM: uppercase"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="DocNum_RF" runat="server" ControlToValidate="Doc_num"
                        Display="Dynamic" ErrorMessage="文件代碼不得空白!">*</asp:RequiredFieldValidator>
                    <asp:Label ID="Label1" runat="server" Width="198px">(IS-文件歸屬-文件階層-流水號)</asp:Label></td>
             </tr> 
            <tr> 
                            <td class="tb_title_w_1">
                    文件名稱</td> 
                                    <td class="tb_w_1_left" >
                    <asp:TextBox ID="Doc_name" runat="server" Width="394px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="DocName_RF" runat="server" ControlToValidate="Doc_name"
                        Display="Dynamic" ErrorMessage="文件名稱不得空白!">*</asp:RequiredFieldValidator></td>
             </tr> 
            <tr>
                 <td class="tb_title_w_1" >
                    文件類別</td>
                 <td class="tb_w_1_left" >
                    <asp:Label ID="doctype" runat="server" Width="106px" >第一階文件：政策</asp:Label></td>           
            </tr> 
            <tr>
                 <td class="tb_title_w_1" >
                    機密等級</td>
                 <td class="tb_w_1_left" >
                    <asp:RadioButtonList ID="rb_doc_secret" runat="server" RepeatDirection="Horizontal">
                         <asp:ListItem Selected="True" Value="P">P(公開)</asp:ListItem>
                         <asp:ListItem Value="I">I(內部)</asp:ListItem>
                         <asp:ListItem Value="R">R(限閱)</asp:ListItem>
                     </asp:RadioButtonList></td>           
            </tr> 
                        <tr>
                 <td class="tb_title_w_1" >
                    申請用途</td>
                 <td class="tb_w_1_left" >
                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" Enabled="False">
                         <asp:ListItem Selected="True" Value="1">制定</asp:ListItem>
                         <asp:ListItem Value="2">修訂</asp:ListItem>
                         <asp:ListItem Value="3">廢止</asp:ListItem>
                         <asp:ListItem Value="4">銷毀</asp:ListItem>
                     </asp:RadioButtonList></td>          
            </tr> 
            <tr>
                  <td class="tb_title_w_1">
                    版次</td>
                                    <td class="tb_w_1_left" >
                                        <asp:TextBox ID="Doc_Ver" runat="server" Text="1.0" Width="40px"></asp:TextBox></td>
            </tr>
                    <tr>
                   
                <td class="tb_title_w_1" style="height: 80px">
                    異動說明</td>
                <td class="tb_w_1_left" style="height: 80px" >
                    <asp:TextBox ID="Doc_desc" runat="server" TextMode="MultiLine" Width="580px" Height="66px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="DocDes_RF" runat="server" ControlToValidate="Doc_desc"
                        Display="Dynamic" ErrorMessage="異動說明不得空白!">*</asp:RequiredFieldValidator></td>
            </tr>
         
          <tr>
              <td class="tb_title_w_1" colspan="2" >
                  文件上傳</td>
          </tr>
          <tr>
              <td  colspan="2">
                  <asp:FileUpload ID="Doc_Upload" runat="server" Width="400px" />
                  <asp:RequiredFieldValidator ID="DocFile_RF" runat="server" ControlToValidate="Doc_Upload"
                      Display="Dynamic" ErrorMessage="上傳檔案不得空白!">*</asp:RequiredFieldValidator></td>
          </tr>
          <tr>
              <td align="left" class="tb_w_1" colspan="2">
                  <asp:Button ID="Ins_Btn" runat="server" Text="確定新增" />&nbsp;
              </td>
          </tr>
      </table>
        <cc1:ValidatorCalloutExtender ID="DocNum_VLD" runat="server" TargetControlID="DocNum_RF">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="DocName_VLD" runat="server" TargetControlID="DocName_RF">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="DocDes_VLD" runat="server" TargetControlID="DocDes_RF">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="DocFile_VLD" runat="server" TargetControlID="DocFile_RF">
        </cc1:ValidatorCalloutExtender>
        <table id="ChRev_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1"
            style="width: 750px" visible="false">
            <tr>
                <td class="tb_title_w_2" colspan="6" style="height: 27px">
                    <img src="Images/exe.gif" />
                    <asp:Label ID="LB_REV" runat="server"></asp:Label>
                    | 改版作業</td>
            </tr>
             <tr>
                 <td class="tb_title_w_1">
                    申請日期</td>
              <td class="tb_w_1_left" >
                    <asp:Label ID="Rev_AuthorDate" runat="server"></asp:Label></td>   
            </tr>
           <tr>
                  <td class="tb_title_w_1" >
                    申請單位</td>
                 <td class="tb_w_1_left" >
                    <asp:Label ID="Rev_dept" runat="server" Width="113px" ></asp:Label></td>           
            </tr>
            <tr>
                    <td class="tb_title_w_1">
                    申請人</td>
                   <td class="tb_w_1_left" >
                    <asp:Label ID="Rec_AuthorTxt" runat="server"></asp:Label></td>
            </tr>
            <tr>
                   <td class="tb_title_w_1" >
                    文件編號</td>
                      <td class="tb_w_1_left" >
                    <asp:TextBox ID="Doc_num_rev" runat="server" Width="85px" ReadOnly="True" BackColor="WhiteSmoke"></asp:TextBox></td>
            </tr>
            <tr>
                            <td class="tb_title_w_1">
                    文件名稱</td>
                                    <td class="tb_w_1_left" style="color: #656b76" >
                    <asp:TextBox ID="doc_name_rev" runat="server" ReadOnly="True" BackColor="WhiteSmoke"></asp:TextBox>
                    </td>
             </tr>
            <tr>
                 <td class="tb_title_w_1" >
                    文件類別</td>
                 <td class="tb_w_1_left" >
                    <asp:Label ID="Label2" runat="server" Width="106px" >第一階文件：政策</asp:Label></td>           
            </tr> 

          <tr>
                 <td class="tb_title_w_1" >
                    機密等級</td>
                 <td class="tb_w_1_left" >
                    <asp:RadioButtonList ID="rb_revdoc_secret" runat="server" RepeatDirection="Horizontal">
                         <asp:ListItem Selected="True" Value="P">公開</asp:ListItem>
                         <asp:ListItem Value="I">內部</asp:ListItem>
                         <asp:ListItem Value="R">限閱</asp:ListItem>
                     </asp:RadioButtonList></td>           
            </tr> 
               <tr>
                 <td class="tb_title_w_1" >
                    申請用途</td>
                 <td class="tb_w_1_left" >
                                        <asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal" Enabled="False">
                         <asp:ListItem Value="1">制定</asp:ListItem>
                         <asp:ListItem Value="2" Selected="True">修訂</asp:ListItem>
                         <asp:ListItem Value="3">廢止</asp:ListItem>
                         <asp:ListItem Value="4">銷毀</asp:ListItem>
                     </asp:RadioButtonList></td>          
            </tr> 
            <tr>
                            <td class="tb_title_w_1">
                    版次</td>
                                    <td class="tb_w_1_left" >
                                       <asp:TextBox ID="Rev_Num" runat="server" Width="40px"></asp:TextBox></td>
            </tr>
<tr>
                <td class="tb_title_w_1">
                    修訂內容</td>

             <td class="tb_w_1_left" >
                    <asp:TextBox ID="Doc_desc_rev" runat="server" TextMode="MultiLine" Height="66px" Width="580px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RevDocDes_RFV" runat="server" ControlToValidate="Doc_desc_rev" ErrorMessage="內容簡述不得空白!" SetFocusOnError="True">*</asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td class="tb_title_w_1" colspan="2" >
                    文件上傳</td>
            </tr>
            <tr>
                <td  colspan="2">
                    <asp:FileUpload ID="Doc_Upload_rev" runat="server" Width="400px" />
                    <asp:RequiredFieldValidator ID="REVDocFile_RFV" runat="server" ControlToValidate="Doc_Upload_rev" ErrorMessage="上傳檔案不得空白!" SetFocusOnError="True" TabIndex="1">*</asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td align="left" class="tb_w_1" colspan="2">
                    <asp:Button ID="Rev_OKBtn" runat="server" Text="確定改版" />
                    &nbsp;
                </td>
            </tr>
        </table>
        <cc1:ValidatorCalloutExtender ID="REVDocDes_VLD" runat="server" TargetControlID="RevDocDes_RFV" Enabled="True">
        </cc1:ValidatorCalloutExtender>
        <cc1:ValidatorCalloutExtender ID="RevDocFile_VLD" runat="server" TargetControlID="REVDocFile_RFV" Enabled="True">
        </cc1:ValidatorCalloutExtender>
        <asp:HiddenField ID="Hidden_Docid" runat="server" Visible="False" />
        <asp:HiddenField ID="Hidden_capsule" runat="server" Visible="False" />
        &nbsp;&nbsp;<br />
        <br />
        <br />
        &nbsp;<br />
        &nbsp;
    </form>
</body>
</html>
