﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class ISMS_31150
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim i As Integer

    Protected Sub SqlDS()

        Flow_DS.ConnectionString = Common.ConnDBS.ConnectionString
        Flow_DS.SelectCommand = "SELECT * FROM riskEvaluateMaster  WHERE dp='" + Hidden_DP.Value + "' AND  Issue_date is NULL order by add_date desc"
        GridView1.DataSourceID = Flow_DS.ID
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        SqlDS()
    End Sub

 




    Protected Sub audit_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        'Response.Write("SELECT *  FROM riskEvaluateForm where PID=" + GridView1.Rows(Index).Cells(4).Text + "")
        RiskForm_DS.ConnectionString = Conn.ConnectionString
        RiskForm_DS.SelectCommand = "SELECT *  FROM riskEvaluateForm where PID=" + GridView1.Rows(Index).Cells(4).Text
        GridView2.DataSourceID = RiskForm_DS.ID

        WF_DS.ConnectionString = Common.ConnDBS.ConnectionString
        WF_DS.SelectCommand = "SELECT * FROM workflowName where dp='" + Hidden_DP.Value + "' and apprv_id='5' order by wf_order, wf_inside_order"
        GridView3.DataSourceID = WF_DS.ID

        SetFields()
        tb1.Visible = True
        tb2.Visible = True
        Main_TB.Visible = False
        lb_riskname.Text = GridView1.Rows(Index).Cells(1).Text
        Hidden_Formid.Value = GridView1.Rows(Index).Cells(4).Text


    End Sub

    Protected Sub GridView2_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView2.PreRender
        Dim i As Integer = 1
        Dim j As Integer = 0
        Dim k As Integer
        Dim AlternatingRowStyle_i As Integer = 1
        Dim mySingleRow As GridViewRow

        For Each mySingleRow In GridView2.Rows
            For j = 0 To 13
                GridView2.Rows(CInt(mySingleRow.RowIndex)).Cells(j).CssClass = "tb_w_1"
            Next
        Next

        For Each mySingleRow In GridView2.Rows

            If CInt(mySingleRow.RowIndex) = 0 Then

                For k = 0 To 8
                    mySingleRow.Cells(k).RowSpan = 1
                Next


            Else
                If mySingleRow.Cells(0).Text.Trim() = GridView2.Rows(CInt(mySingleRow.RowIndex) - i).Cells(0).Text.Trim() And mySingleRow.Cells(1).Text.Trim() = GridView2.Rows(CInt(mySingleRow.RowIndex) - i).Cells(1).Text.Trim() Then

                    For k = 0 To 8
                        GridView2.Rows(CInt(mySingleRow.RowIndex) - i).Cells(k).RowSpan += 1
                    Next

                    i = i + 1
                    For k = 0 To 8
                        mySingleRow.Cells(k).Visible = False
                    Next


                Else
                    For k = 0 To 8
                        GridView2.Rows(CInt(mySingleRow.RowIndex)).Cells(k).RowSpan = 1
                    Next
                    i = 1
                End If
            End If
        Next

    End Sub

    Protected Sub GridView2_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim tc As TableCellCollection = e.Row.Cells
            tc.Clear()

            tc.Add(New TableHeaderCell)
            tc(0).Text = "風險類別"
            tc.Add(New TableHeaderCell)
            tc(1).Text = "資產編號"
            tc.Add(New TableHeaderCell)
            tc(2).Text = "資產名稱"
            tc.Add(New TableHeaderCell)
            tc(3).Text = "使用者"
            tc.Add(New TableHeaderCell)
            tc(4).Text = "擁有者"
            tc.Add(New TableHeaderCell)
            tc(5).Text = "權責單位"
            tc.Add(New TableHeaderCell)
            tc(6).Text = "數量"
            tc.Add(New TableHeaderCell)
            tc(7).Text = "放置地點"
            tc.Add(New TableHeaderCell)
            tc(8).Text = "資產價值"
            tc.Add(New TableHeaderCell)
            'tc(9).Text = "資產安全等級"
            'tc.Add(New TableHeaderCell)
            tc(9).Text = "威脅"
            tc.Add(New TableHeaderCell)
            tc(10).Text = "弱點"
            tc.Add(New TableHeaderCell)
            tc(11).Text = "可能性"
            tc.Add(New TableHeaderCell)
            tc(12).Text = "衝擊性"
            tc.Add(New TableHeaderCell)
            'KM LAND
            tc(13).Text = "威脅弱點值"
            'KM TAX
            'tc(13).Text = "綜合風險值"  


            For i = 0 To 13
                tc(i).CssClass = "tb_title_w_1"
            Next


        End If
    End Sub

    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound
        If e.Row.RowIndex <> -1 Then
            Dim Ds, Ds1 As New DataSet
            e.Row.Cells(3).Text = Common.Get_User_Name(e.Row.Cells(3).Text)
            e.Row.Cells(4).Text = Common.Get_User_Name(e.Row.Cells(4).Text)
            e.Row.Cells(5).Text = Common.Get_dept_Name(e.Row.Cells(5).Text)
            Ds = Common.Get_AssetName(e.Row.Cells(0).Text)
            e.Row.Cells(0).Text = Ds.Tables(0).Rows(0).Item("asset_name")

        End If
    End Sub


    Protected Sub SetFields()

        Dim risk_type, dept_id, assitem_id, assitem_name, location, item_amount, user_id, holder As New BoundField
        Dim assitSecure_lv, assitScore, weakness, threat, riskp_score, impact_score, totalrisk_score, totalrisk_lv As New BoundField

        dept_id.DataField = "dept_id"
        assitem_id.DataField = "assitem_id"
        assitem_name.DataField = "assitem_name"
        user_id.DataField = "user_id"
        holder.DataField = "holder"
        item_amount.DataField = "item_amount"
        location.DataField = "location"
        assitScore.DataField = "assitScore"
        assitSecure_lv.DataField = "assitSecure_lv"
        weakness.DataField = "weakness"
        threat.DataField = "threat"
        riskp_score.DataField = "riskp_score"
        impact_score.DataField = "impact_score"
        totalrisk_score.DataField = "totalrisk_score"
        totalrisk_lv.DataField = "totalrisk_lv"
        risk_type.DataField = "risk_type"




        GridView2.Columns.Add(risk_type)
        GridView2.Columns.Add(assitem_id)
        GridView2.Columns.Add(assitem_name)
        GridView2.Columns.Add(user_id)
        GridView2.Columns.Add(holder)
        GridView2.Columns.Add(dept_id)
        GridView2.Columns.Add(item_amount)
        GridView2.Columns.Add(location)
        GridView2.Columns.Add(assitScore)
        'GridView1.Columns.Add(assitSecure_lv)
        GridView2.Columns.Add(threat)
        GridView2.Columns.Add(weakness)
        GridView2.Columns.Add(riskp_score)
        GridView2.Columns.Add(impact_score)
        GridView2.Columns.Add(totalrisk_score)
        'GridView1.Columns.Add(totalrisk_lv)

    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(4).Visible = False
        e.Row.Cells(5).Visible = False
    End Sub

    Protected Sub GridView3_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView3.RowCreated
        e.Row.Cells(5).Visible = False

    End Sub

    Protected Sub GridView3_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView3.RowDataBound

        Dim ds, ds1, ds2, ds3, ds4, ds5, ds6 As New DataSet
        Dim form_status, wf_status, wf_user, wf_count, wf_order, wf_user_order As String



        If e.Row.RowIndex <> -1 Then

            ds = Common.Get_wfFormRecord(5, Hidden_Formid.Value, "")
            ds1 = Common.Get_RiskForm_info(Hidden_Formid.Value)
            ds2 = Common.Get_Wf_ApprvUser(Hidden_DP.Value, 5, e.Row.Cells(0).Text)
            ds3 = Common.Get_wfFormRecord(5, Hidden_Formid.Value, "ASC")
            ds5 = Common.Get_wfRecord(5, Hidden_Formid.Value, 1)
            ds6 = Common.Get_wfInside_Record(5, Hidden_Formid.Value, 1, e.Row.Cells(0).Text)

            If e.Row.RowIndex >= 1 Then

                ds4 = Common.Get_Wf_User(Hidden_DP.Value, 5, e.Row.Cells(0).Text, e.Row.Cells(5).Text)
                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds4.Tables(0).Rows(0).Item("wf_userid"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds4.Tables(0).Rows(0).Item("wf_userid")

            ElseIf e.Row.RowIndex = 0 And Common.FixNull(ds1.Tables(0).Rows(0).Item("form_status")) = "RISK_VERIFY" Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("add_user"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("add_user")


            End If


            Try
                wf_status = Common.FixNull(ds.Tables(0).Rows(0).Item("wf_status_id"))
                wf_order = Common.FixNull(ds.Tables(0).Rows(0).Item("wf_order"))

            Catch ex As Exception


            End Try
            form_status = Common.FixNull(ds1.Tables(0).Rows(0).Item("form_status"))
            wf_user_order = Common.FixNull(ds2.Tables(0).Rows(0).Item("wf_order"))
            wf_user = Common.FixNull(CType(e.Row.FindControl("label_userid"), Label).Text)
            wf_count = Common.FixNull(ds.Tables(0).Rows.Count)

            If wf_count >= 1 Then

                If wf_count = 1 And wf_user_order = 1 And form_status = "RISK_VERIFY" And wf_user = User.Identity.Name.ToString Then

                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("verify_btn"), Button).Visible = True

                ElseIf form_status = "RISK_VERIFY" And (ds.Tables(0).Rows(0).Item("wf_status_id") = "2" Or ds.Tables(0).Rows(0).Item("wf_status_id") = "0") And wf_count >= 1 And e.Row.RowIndex >= 1 And wf_user_order = ds.Tables(0).Rows(0).Item("wf_order") + 1 And wf_user = User.Identity.Name.ToString Then

                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("verify_btn"), Button).Visible = True
                Else
                    CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = False
                    CType(e.Row.FindControl("comment_txt"), TextBox).Visible = False
                    CType(e.Row.FindControl("verify_btn"), Button).Visible = False

                End If

            Else
                CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = False
                CType(e.Row.FindControl("comment_txt"), TextBox).Visible = False
                CType(e.Row.FindControl("verify_btn"), Button).Visible = False


            End If

            '填上已簽核過的狀態與意見
            If ds6.Tables(0).Rows.Count >= 1 Then
                If ds6.Tables(0).Rows(0).Item("wf_inside_order") = e.Row.Cells(5).Text And e.Row.RowIndex >= 0 And e.Row.RowIndex <= ds5.Tables(0).Rows.Count Then
                    CType(e.Row.FindControl("lb_status"), Label).Visible = True
                    CType(e.Row.FindControl("comment_lb"), Label).Visible = True
                    CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(ds6.Tables(0).Rows(0).Item("wf_status_id")))
                    CType(e.Row.FindControl("comment_lb"), Label).Text = Common.FixNull(ds6.Tables(0).Rows(0).Item("wf_comment"))
                Else
                    CType(e.Row.FindControl("lb_status"), Label).Visible = False
                    CType(e.Row.FindControl("comment_lb"), Label).Visible = False
                End If

            End If


                '狀態選單
            If e.Row.RowIndex >= 1 Then
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(1)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
            Else
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(0)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
            End If



            End If

    End Sub

    Protected Sub verify_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex

        Dim ds, ds1, ds2, ds3 As New DataSet
        Dim form_status As String

        ds = Common.Get_Wf_ApprvUser(Hidden_DP.Value, 5, "")

        SqlTxt = "INSERT INTO wfFormRecord " + _
         "(apprv_id,doc_id,wf_order,wf_userid,wf_status_id,wf_comment,signdate,record_id,wf_inside_order,doc_status) VALUES" + _
        "('5','" + Hidden_Formid.Value + "','" + oRow.Cells(0).Text + "','" + CType(GridView3.Rows(Index).FindControl("label_userid"), Label).Text + "','" + CType(GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue + "'," + _
        " '" + CType(GridView3.Rows(Index).FindControl("comment_txt"), TextBox).Text + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','1','" + oRow.Cells(5).Text + "','RISK')"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()


        ds1 = Common.Get_RiskForm_info(Hidden_Formid.Value)
        ds2 = Common.Get_WorkFlow_MaxOrderId(5, Hidden_DP.Value)
        form_status = ds1.Tables(0).Rows(0).Item("form_status")


        If form_status = "RISK_VERIFY" Then


            If CType(Me.GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue = "2" And oRow.Cells(0).Text = ds2.Tables(0).Rows(0).Item("wf_order") Then

                SqlTxt = "UPDATE riskEvaluateMaster  SET Issue_date='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',form_status = 'RISK_ISSUE' WHERE form_id='" + Hidden_Formid.Value + "' "

                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", Hidden_Formid.Value, System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "RISKPUBLISH", lb_riskname.Text)

                'Mail通知()
                'ds3 = Common.Get_Wf_ApprvUser(5, oRow.Cells(0).Text + 1)
                'ds3 = Common.Get_User
                'Common.SendMail(ds1.Tables(0).Rows(i).Item("creator"), "Workflow_OK", docNum_LB.Text, docName_LB.Text)
                'For i = 0 To ds3.Tables(0).Rows.Count - 1
                Common.SendMail(ds1.Tables(0).Rows(0).Item("add_user"), "Workflow_RISK_OK", "", lb_riskname.Text, "")
                'Next


            ElseIf CType(Me.GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue = "2" And ds.Tables(0).Rows.Count - 1 > Index Then

                'SqlTxt = "UPDATE riskEvaluateMaster  SET Issue_date='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',form_status = 'RISK_ISSUE' WHERE form_id='" + Hidden_Formid.Value + "' "

                'Mail通知()
                ds3 = Common.Get_Wf_ApprvUser(Hidden_DP.Value, 5, oRow.Cells(0).Text + 1)
                'ds3 = Common.Get_User
                'Common.SendMail(ds1.Tables(0).Rows(i).Item("creator"), "Workflow_OK", docNum_LB.Text, docName_LB.Text)
                For i = 0 To ds3.Tables(0).Rows.Count - 1
                    Common.SendMail(ds3.Tables(0).Rows(0).Item("wf_userid"), "Workflow_RISK1", "", lb_riskname.Text, "")
                Next



            ElseIf CType(Me.GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue = "9" Then

                'SqlTxt = "UPDATE docResoure  SET doc_status = 'ISSUE',locked='0' WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                SqlTxt = "UPDATE riskEvaluateMaster  SET form_status = 'RISK_REJECT' WHERE form_id='" + Hidden_Formid.Value + "' "
                'Mail通知
                Common.SendMail(ds1.Tables(0).Rows(0).Item("add_user"), "Workflow_RISK_KO", "", lb_riskname.Text, "")

            End If


        End If


        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()

        tb1.Visible = False
        tb2.Visible = False
        Main_TB.Visible = True
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound


        Dim oDataRow As Data.DataRowView
        If e.Row.RowIndex <> -1 Then

            oDataRow = CType(e.Row.DataItem, Data.DataRowView)

            Dim Ds, Ds1, Ds2, Ds3 As New DataSet
            Ds = Common.Get_wfFormRecord(5, e.Row.Cells(4).Text, "")
            Ds1 = Common.Get_Wf_ApprvUser(Hidden_DP.Value, 5, "")
            Ds2 = Common.Get_RiskForm_info(e.Row.Cells(4).Text)

            If Ds.Tables(0).Rows.Count = 0 And Ds2.Tables(0).Rows(0).Item("add_user") = User.Identity.Name.ToString Then
                CType(e.Row.FindControl("audit_btn"), Button).Visible = True
            End If



            If Ds.Tables(0).Rows.Count >= 1 And Ds2.Tables(0).Rows(0).Item("form_status") = "RISK_VERIFY" Then
                Dim Login_id, Sign_id As String
                Login_id = User.Identity.Name.ToString
                Ds3 = Common.Get_Wf_ApprvUser(Hidden_DP.Value, 5, Ds.Tables(0).Rows(0).Item("wf_order") + 1)
                For i = 0 To Ds3.Tables(0).Rows.Count - 1
                    Sign_id = Ds3.Tables(0).Rows(i).Item("wf_userid")
                    If Login_id = Sign_id Then
                        CType(e.Row.FindControl("audit_btn"), Button).Visible = True
                    End If
                Next
            End If



            'Ds4 = Common.Get_wfRecord(5, e.Row.Cells(4).Text, "")
            'For i = 0 To Ds4.Tables(0).Rows.Count - 1
            '    If i = Ds4.Tables(0).Rows.Count - 1 Then
            '        CType(e.Row.FindControl("lb_wf"), Label).Text = CType(e.Row.FindControl("lb_wf"), Label).Text + Common.Get_User_Name(Ds4.Tables(0).Rows(i).Item("wf_userid")) + "(" + Common.Get_WFStatusName(Ds4.Tables(0).Rows(i).Item("wf_status_id")) + ") =>" + Ds4.Tables(0).Rows(i).Item("wf_userid")
            '    Else
            '        CType(e.Row.FindControl("lb_wf"), Label).Text = CType(e.Row.FindControl("lb_wf"), Label).Text + Common.Get_User_Name(Ds4.Tables(0).Rows(i).Item("wf_userid")) + "(" + Common.Get_WFStatusName(Ds4.Tables(0).Rows(i).Item("wf_status_id")) + ") =>"
            '    End If
            'Next



        End If
    End Sub
End Class
