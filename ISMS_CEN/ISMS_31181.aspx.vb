﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class ISMS_31181
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim oRow As GridViewRow
    Dim Index As Integer
    Dim i As Integer
    Public appid As String = "5"

    Protected Sub SqlDS()

        Flow_DS.ConnectionString = Common.ConnDBS.ConnectionString
        Flow_DS.SelectCommand = "SELECT * FROM ThreatWeaknessEvaluateMaster where apprv_id=" + appid + " and dp='" + Hidden_DP.Value + "'  order by add_date desc"
        GridView1.DataSourceID = Flow_DS.ID
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP

        Dim ds As New DataSet
        ds = Common.Get_Asset()
        If ds.Tables(0).Rows.Count = 0 Then
            Server.Transfer("ISMS_20110.aspx")
        End If

        SqlDS()
    End Sub






    Protected Sub audit_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex
        'Response.Write("SELECT *  FROM riskEvaluateForm where PID=" + GridView1.Rows(Index).Cells(4).Text + "")
        RiskForm_DS.ConnectionString = Conn.ConnectionString
        RiskForm_DS.SelectCommand = "SELECT *  FROM ThreatWeaknessEvaluateForm where PID=" + GridView1.Rows(Index).Cells(4).Text + ""
        GridView2.DataSourceID = RiskForm_DS.ID

        WF_DS.ConnectionString = Common.ConnDBS.ConnectionString
        WF_DS.SelectCommand = "SELECT * FROM workflowName where apprv_id='" + appid + "' and dp='" + Hidden_DP.Value + "' order by wf_order, wf_inside_order"
        GridView3.DataSourceID = WF_DS.ID

        Show_TB.Visible = True
        tb2.Visible = True
        Main_TB.Visible = False
        'lb_riskname.Text = GridView1.Rows(Index).Cells(1).Text
        Hidden_Formid.Value = GridView1.Rows(Index).Cells(4).Text


    End Sub


    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(4).Visible = False
        e.Row.Cells(5).Visible = False
    End Sub

    Protected Sub GridView3_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView3.RowCreated
        e.Row.Cells(5).Visible = False

    End Sub

    Protected Sub GridView3_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView3.RowDataBound

        Dim ds, ds1, ds2, ds3, ds4, ds5, ds6 As New DataSet
        Dim form_status, wf_status, wf_user, wf_count, wf_order, wf_user_order As String



        If e.Row.RowIndex <> -1 Then

            ds = Common.Get_wfFormRecord(appid, Hidden_Formid.Value, "")
            ds1 = Common.Get_ThreatWeaknessRiskForm_info(Hidden_Formid.Value)
            ds2 = Common.Get_Wf_ApprvUser(Hidden_DP.Value, appid, e.Row.Cells(0).Text)
            ds3 = Common.Get_wfFormRecord(appid, Hidden_Formid.Value, "ASC")
            ds5 = Common.Get_wfRecord(appid, Hidden_Formid.Value, 1)
            ds6 = Common.Get_wfInside_Record(appid, Hidden_Formid.Value, 1, e.Row.Cells(0).Text)

            If e.Row.RowIndex >= 1 Then

                ds4 = Common.Get_Wf_User(Hidden_DP.Value, appid, e.Row.Cells(0).Text, e.Row.Cells(5).Text)
                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds4.Tables(0).Rows(0).Item("wf_userid"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds4.Tables(0).Rows(0).Item("wf_userid")

                ' ElseIf e.Row.RowIndex = 0 And Common.FixNull(ds1.Tables(0).Rows(0).Item("form_status")) = "RISK_VERIFY" Then
            ElseIf e.Row.RowIndex = 0 Then

                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(ds1.Tables(0).Rows(0).Item("add_user"))
                CType(e.Row.FindControl("label_userid"), Label).Text = ds1.Tables(0).Rows(0).Item("add_user")


            End If


            Try
                wf_status = Common.FixNull(ds.Tables(0).Rows(0).Item("wf_status_id"))
                wf_order = Common.FixNull(ds.Tables(0).Rows(0).Item("wf_order"))

            Catch ex As Exception


            End Try
            form_status = Common.FixNull(ds1.Tables(0).Rows(0).Item("form_status"))
            wf_user_order = Common.FixNull(ds2.Tables(0).Rows(0).Item("wf_order"))
            wf_user = Common.FixNull(CType(e.Row.FindControl("label_userid"), Label).Text)
            wf_count = Common.FixNull(ds.Tables(0).Rows.Count)

            If wf_count >= 1 Then

                If wf_count = 1 And wf_user_order = 1 And form_status = "RISK_VERIFY" And wf_user = User.Identity.Name.ToString Then

                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("verify_btn"), Button).Visible = True

                ElseIf form_status = "RISK_VERIFY" And (ds.Tables(0).Rows(0).Item("wf_status_id") = "2" Or ds.Tables(0).Rows(0).Item("wf_status_id") = "0") And wf_count >= 1 And e.Row.RowIndex >= 1 And wf_user_order = ds.Tables(0).Rows(0).Item("wf_order") + 1 And wf_user = User.Identity.Name.ToString Then

                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                    CType(e.Row.Cells(e.Row.RowIndex).FindControl("verify_btn"), Button).Visible = True
                Else
                    CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = False
                    CType(e.Row.FindControl("comment_txt"), TextBox).Visible = False
                    CType(e.Row.FindControl("verify_btn"), Button).Visible = False

                End If

            Else
                CType(e.Row.FindControl("ddl_status"), DropDownList).Visible = False
                CType(e.Row.FindControl("comment_txt"), TextBox).Visible = False
                CType(e.Row.FindControl("verify_btn"), Button).Visible = False


            End If

            '填上已簽核過的狀態與意見
            If ds6.Tables(0).Rows.Count >= 1 Then
                If ds6.Tables(0).Rows(0).Item("wf_inside_order") = e.Row.Cells(5).Text And e.Row.RowIndex >= 0 And e.Row.RowIndex <= ds5.Tables(0).Rows.Count Then
                    CType(e.Row.FindControl("lb_status"), Label).Visible = True
                    CType(e.Row.FindControl("comment_lb"), Label).Visible = True
                    CType(e.Row.FindControl("lb_status"), Label).Text = Common.Get_WFStatusName(Common.FixNull(ds6.Tables(0).Rows(0).Item("wf_status_id")))
                    CType(e.Row.FindControl("comment_lb"), Label).Text = Common.FixNull(ds6.Tables(0).Rows(0).Item("wf_comment"))
                Else
                    CType(e.Row.FindControl("lb_status"), Label).Visible = False
                    CType(e.Row.FindControl("comment_lb"), Label).Visible = False
                End If

            End If


            '狀態選單
            If e.Row.RowIndex >= 1 Then
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(1)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
            Else
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(0)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
            End If



        End If

    End Sub

    Protected Sub verify_btn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oRow = CType(sender, Button).NamingContainer
        Index = oRow.RowIndex

        Dim ds, ds1, ds2, ds3 As New DataSet
        Dim form_status As String

        ds = Common.Get_Wf_ApprvUser(Hidden_DP.Value, appid, "")

        SqlTxt = "INSERT INTO wfFormRecord " + _
         "(apprv_id,doc_id,wf_order,wf_userid,wf_status_id,wf_comment,signdate,record_id,wf_inside_order,doc_status) VALUES" + _
        "('" + appid + "','" + Hidden_Formid.Value + "','" + oRow.Cells(0).Text + "','" + CType(GridView3.Rows(Index).FindControl("label_userid"), Label).Text + "','" + CType(GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue + "'," + _
        " '" + CType(GridView3.Rows(Index).FindControl("comment_txt"), TextBox).Text + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','1','" + oRow.Cells(5).Text + "','RISK')"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()


        ds1 = Common.Get_ThreatWeaknessRiskForm_info(Hidden_Formid.Value)
        ds2 = Common.Get_WorkFlow_MaxOrderId(appid, Hidden_DP.Value)
        form_status = ds1.Tables(0).Rows(0).Item("form_status")


        If form_status = "RISK_VERIFY" Then


            If CType(Me.GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue = "2" And oRow.Cells(0).Text = ds2.Tables(0).Rows(0).Item("wf_order") Then

                SqlTxt = "UPDATE ThreatWeaknessEvaluateMaster  SET Issue_date='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',form_status = 'RISK_ISSUE' WHERE form_id='" + Hidden_Formid.Value + "' "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()

                'Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", Hidden_Formid.Value, System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "RISKPUBLISH", lb_riskname.Text)

                'Mail通知()
                'ds3 = Common.Get_Wf_ApprvUser(5, oRow.Cells(0).Text + 1)
                'ds3 = Common.Get_User
                'Common.SendMail(ds1.Tables(0).Rows(i).Item("creator"), "Workflow_OK", docNum_LB.Text, docName_LB.Text)
                'For i = 0 To ds3.Tables(0).Rows.Count - 1
                'Common.SendMail(ds1.Tables(0).Rows(0).Item("add_user"), "Workflow_RISK_OK", "", lb_riskname.Text)
                'Next


            ElseIf CType(Me.GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue = "2" And ds.Tables(0).Rows.Count - 1 > Index Then

                'SqlTxt = "UPDATE ThreatWeaknessEvaluateMaster  SET Issue_date='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',form_status = 'RISK_ISSUE' WHERE form_id='" + Hidden_Formid.Value + "' "
                'SqlCmd = New SqlCommand(SqlTxt, Conn)
                'Conn.Open()
                'SqlCmd.ExecuteNonQuery()
                'Conn.Close()

                'SqlTxt = "UPDATE riskEvaluateMaster  SET Issue_date='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',form_status = 'RISK_ISSUE' WHERE form_id='" + Hidden_Formid.Value + "' "

                'Mail通知()
                'ds3 = Common.Get_Wf_ApprvUser(5, oRow.Cells(0).Text + 1)
                'ds3 = Common.Get_User
                'Common.SendMail(ds1.Tables(0).Rows(i).Item("creator"), "Workflow_OK", docNum_LB.Text, docName_LB.Text)
                'For i = 0 To ds3.Tables(0).Rows.Count - 1
                'Common.SendMail(ds3.Tables(0).Rows(0).Item("wf_userid"), "Workflow_RISK1", "", lb_riskname.Text)
                'Next



            ElseIf CType(Me.GridView3.Rows(Index).FindControl("ddl_status"), DropDownList).SelectedValue = "9" Then

                'SqlTxt = "UPDATE docResoure  SET doc_status = 'ISSUE',locked='0' WHERE doc_id='" + Hidden_Docid.Value + "' and apprv_id='" + Hidden_apprv_id.Value + "'"
                SqlTxt = "UPDATE ThreatWeaknessEvaluateMaster  SET form_status = 'RISK_REJECT' WHERE form_id='" + Hidden_Formid.Value + "' "
                'Mail通知
                'Common.SendMail(ds1.Tables(0).Rows(0).Item("add_user"), "Workflow_RISK_KO", "", lb_riskname.Text)
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()



            End If


        End If



        Show_TB.Visible = False
        tb2.Visible = False
        Main_TB.Visible = True
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound


        Dim oDataRow As Data.DataRowView
        If e.Row.RowIndex <> -1 Then

            oDataRow = CType(e.Row.DataItem, Data.DataRowView)

            Dim Ds, Ds1, Ds2, Ds3 As New DataSet
            Ds = Common.Get_wfFormRecord(appid, e.Row.Cells(4).Text, "")
            Ds1 = Common.Get_Wf_ApprvUser(Hidden_DP.Value, appid, "")
            Ds2 = Common.Get_ThreatWeaknessRiskForm_info(e.Row.Cells(4).Text)

            If Ds.Tables(0).Rows.Count = 0 And Ds2.Tables(0).Rows(0).Item("add_user") = User.Identity.Name.ToString Then
                CType(e.Row.FindControl("audit_btn"), Button).Visible = True
            End If



            If Ds.Tables(0).Rows.Count >= 1 And Ds2.Tables(0).Rows(0).Item("form_status") = "RISK_VERIFY" Then
                Dim Login_id, Sign_id As String
                Login_id = User.Identity.Name.ToString
                Ds3 = Common.Get_Wf_ApprvUser(Hidden_DP.Value, appid, Ds.Tables(0).Rows(0).Item("wf_order") + 1)
                For i = 0 To Ds3.Tables(0).Rows.Count - 1
                    Sign_id = Ds3.Tables(0).Rows(i).Item("wf_userid")
                    If Login_id = Sign_id Then
                        CType(e.Row.FindControl("audit_btn"), Button).Visible = True
                    End If
                Next
            End If


            Dim ds4 As New DataSet
            ds4 = Common.Get_AssetName(e.Row.Cells(0).Text)
            e.Row.Cells(0).Text = ds4.Tables(0).Rows(0).Item("asset_name")


            'End Select
            'Ds4 = Common.Get_wfRecord(5, e.Row.Cells(4).Text, "")
            'For i = 0 To Ds4.Tables(0).Rows.Count - 1
            '    If i = Ds4.Tables(0).Rows.Count - 1 Then
            '        CType(e.Row.FindControl("lb_wf"), Label).Text = CType(e.Row.FindControl("lb_wf"), Label).Text + Common.Get_User_Name(Ds4.Tables(0).Rows(i).Item("wf_userid")) + "(" + Common.Get_WFStatusName(Ds4.Tables(0).Rows(i).Item("wf_status_id")) + ") =>" + Ds4.Tables(0).Rows(i).Item("wf_userid")
            '    Else
            '        CType(e.Row.FindControl("lb_wf"), Label).Text = CType(e.Row.FindControl("lb_wf"), Label).Text + Common.Get_User_Name(Ds4.Tables(0).Rows(i).Item("wf_userid")) + "(" + Common.Get_WFStatusName(Ds4.Tables(0).Rows(i).Item("wf_status_id")) + ") =>"
            '    End If
            'Next



        End If
    End Sub
End Class
