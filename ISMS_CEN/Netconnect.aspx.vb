﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class Netconnect
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim File_Path As String = ConfigurationManager.AppSettings("Netconnect_Path")
    Dim dp As String = Me.Get_DP
    Dim apprvid As String = ConfigurationManager.AppSettings("flow_Netconnect") '15
    Dim uid As String = User.Identity.Name
    Dim uname As String = Common.Get_User_Name(User.Identity.Name.ToString)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        If Not IsPostBack Then
            If Session("success") = "Y" Then
                Common.showMsg(Me.Page, "儲存成功！")
                Session("success") = ""
            End If
            GdvShow() '載入流程表
            DefaultSource()
        End If
    End Sub

    '載入流程表
    Private Sub GdvShow()
        Try
            Dim dt As DataTable = Common.Qry_workflowName_isms(apprvid, dp)
            GridView.DataSource = dt
            GridView.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '預設資料來源
    Private Sub DefaultSource()
        Try
            '時:分時間下拉
            Ddl_StartHour.Items.Clear()
            Ddl_StartMin.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                Ddl_StartHour.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                Ddl_StartMin.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next

            Ddl_EndHour.Items.Clear()
            Ddl_EndMin.Items.Clear()
            For I As Integer = 23 To 0 Step -1
                Ddl_EndHour.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next
            For I As Integer = 59 To 0 Step -1
                Ddl_EndMin.Items.Insert(0, New ListItem(I.ToString.PadLeft(2, "0"), I.ToString.PadLeft(2, "0")))
            Next


            '申請代表人
            empName.Text = uname


            '系統名稱
            Dim sql As String = "SELECT * FROM SysCode WHERE syscd_Enabled = 1"
            sql += " ORDER BY syscd_time DESC "
            Ddl_Syscd.Items.Clear()
            Ddl_Syscd.DataTextField = "syscd_Name"
            Ddl_Syscd.DataValueField = "syscd_Id"
            Ddl_Syscd.DataSource = Common.Con(sql)
            Ddl_Syscd.DataBind()
            Ddl_Syscd.SelectedValue = Ddl_Syscd.Items(0).Value '預設選項為第一個值


            '原單號日期區間
            sdate.Text = Common.Year(DateTime.Now.AddMonths(-1))
            edate.Text = Common.Year(DateTime.Now)


            '抓Session("NetconnectType")
            Select Case Session("NetconnectType")
                Case "新增"
                    sheetEnabled(False)
                    Radio_applyItem.SelectedValue = "1"
                    oriTb_1.ColSpan = 3
                Case "異動"
                    Radio_applyItem.SelectedValue = "2"
                    sheetEnabled(True) '*
                    oriTb_1.ColSpan = 1 '*
                    Netconnect_recordId.Text = "" '*
                Case "註銷"
                    Radio_applyItem.SelectedValue = "3"
                    sheetEnabled(True) '*
                    oriTb_1.ColSpan = 1 '*
                    Netconnect_recordId.Text = "" '*

                    File_applyFile.Visible = False
                    sheetEnabled() '將表格內資料Enabled
            End Select
            Session("NetconnectType") = ""



        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '表單的Enabled
    Private Sub sheetEnabled(ByVal use As Boolean)
        Try
            '若選到新增不用選原單號
            oriTb_2.Visible = use
            oriTb_3.Visible = use
            ori_applyReasonTb_1.Visible = use
            ori_applyReasonTb_2.Visible = use

        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '表單的Enabled = false
    Private Sub sheetEnabled()
        Try
            applyOrg.Enabled = False '申請單位
            applyName.Enabled = False '申請人員
            applyTel1.Enabled = False '連絡電話1
            applyTel2.Enabled = False '連絡電話2
            Ddl_Syscd.Enabled = False '系統名稱
            startTime_d.Enabled = False '連線時間(起)_日
            Ddl_StartHour.Enabled = False '連線時間(起)_時
            Ddl_StartMin.Enabled = False '連線時間(起)_分
            endTime_d.Enabled = False '連線時間(迄)_日
            Ddl_EndHour.Enabled = False '連線時間(迄)_時
            Ddl_EndMin.Enabled = False '連線時間(迄)_分
            chk_s_ip_1.Enabled = False '單一IP
            s_ip_1_content.Enabled = False
            chk_s_ip_2.Enabled = False '網段(區間)IP
            s_ip_2_content.Enabled = False
            s_ip_Other.Enabled = False '協定/來源 Port
            d_ip_content.Enabled = False '目的IP
            d_ip_other.Enabled = False '協定/目的 Port
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '"選擇"_按鈕
    Protected Sub Btn_recordId_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_recordId.Click
        Try
            Table1.Visible = False
            Table2_1.Visible = True
            Table2_2.Visible = True

            Btn_applyDate_Action()

        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '點擊選擇後的表格資料來源
    Private Sub Btn_applyDate_Action()
        Try
            Dim sql = "  select recordId, applyDate, applyOrg, applyName  from Netconnect  where recordId not in ( select a.recordId from Netconnect a ,form_record b where b.status=1 and a.recordId = b.recordId) and using='Y'"
            If sdate.Text <> "" Then
                sql += " and applyDate >= '" + Common.YearYYYY(sdate.Text) + "'"
            End If
            If edate.Text <> "" Then
                sql += " and applyDate <= '" + Common.YearYYYY(edate.Text) + "'"
            End If
            Dim data = Common.Con(sql)
            GridView1.DataSource = data
            GridView1.DataBind()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '"查詢"_按鈕
    Protected Sub Btn_applyDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_applyDate.Click
        Try
            Btn_applyDate_Action()
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '選取某表單編號_至主頁
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Try
            Dim GETrecordId As String = e.CommandArgument

            Dim sql = "select * from Netconnect where recordId = '" + GETrecordId + "'"
            Dim data = Common.Con(sql)

            '處理checkbox
            Dim s_ip_1 = If(data.Rows(0)("s_ip_1") = "1", True, False)
            Dim s_ip_2 = If(data.Rows(0)("s_ip_2") = "1", True, False)

            applyOrg.Text = data.Rows(0)("applyOrg") '申請單位
            applyName.Text = data.Rows(0)("applyName") '申請人員
            applyTel1.Text = data.Rows(0)("applyTel1") '連絡電話1
            applyTel2.Text = data.Rows(0)("applyTel2") '連絡電話2
            Netconnect_recordId.Text = data.Rows(0)("recordId") '原單號
            Ddl_Syscd.SelectedValue = data.Rows(0)("syscd_Id") '系統名稱
            startTime_d.Text = Common.Year(data.Rows(0)("startTime_d")) '連線時間(起)_日
            Ddl_StartHour.SelectedValue = data.Rows(0)("startTime_h") '連線時間(起)_時
            Ddl_StartMin.SelectedValue = data.Rows(0)("startTime_m") '連線時間(起)_分
            endTime_d.Text = Common.Year(data.Rows(0)("endTime_d")) '連線時間(迄)_日
            Ddl_EndHour.Text = data.Rows(0)("endTime_h") '連線時間(迄)_時
            Ddl_EndMin.Text = data.Rows(0)("endTime_m") '連線時間(迄)_分
            ori_applyReason.Text = data.Rows(0)("applyReason") '原申請事由
            applyReason.Text = "" '先清空接下來使用者要填的申請事由
            chk_s_ip_1.Checked = s_ip_1 '單一IP
            s_ip_1_content.Text = data.Rows(0)("s_ip_1_content")
            chk_s_ip_2.Checked = s_ip_2 '網段(區間)IP
            s_ip_2_content.Text = data.Rows(0)("s_ip_2_content")
            s_ip_Other.Text = data.Rows(0)("s_ip_Other") '協定/來源 Port
            d_ip_content.Text = data.Rows(0)("d_ip_content") '目的IP
            d_ip_other.Text = data.Rows(0)("d_ip_other") '協定/目的 Port


            '檔案先依照申請事項作改變
            If Radio_applyItem.SelectedValue = "1" Then
                '新增
                applyFile.Visible = False
            End If
            If Radio_applyItem.SelectedValue = "2" Then
                '異動
                applyFile.Visible = True
            End If
            If Radio_applyItem.SelectedValue = "3" Then
                '註銷
                applyFile.Visible = True
            End If

            '查看裡面是否有資料，有才秀出來
            If IsDBNull(data.Rows(0)("applyFile")) Then
                applyFile.Visible = False
                applyFile_link.Text = ""
                ori_applyFile_link.Text = ""
            Else
                applyFile_link.Text = data.Rows(0)("applyFile")
                ori_applyFile_link.Text = data.Rows(0)("applyFile_ori_name")
            End If

            Table1.Visible = True
            Table2_1.Visible = False
            Table2_2.Visible = False
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    '檔案下載_按鈕
    Protected Sub applyFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applyFile.Click
        Try
            Common.DownFile(Me.Page, File_Path + applyFile_link.Text)
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

    Protected Sub Radio_applyItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        '檔案先依照申請事項作改變
        If Radio_applyItem.SelectedValue = "1" Then
            '新增
            Session("NetconnectType") = "新增"
        End If
        If Radio_applyItem.SelectedValue = "2" Then
            '異動
            Session("NetconnectType") = "異動"
        End If
        If Radio_applyItem.SelectedValue = "3" Then
            '註銷
            Session("NetconnectType") = "註銷"
        End If

        Response.Redirect("Netconnect.aspx", True)


    End Sub


    Protected Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '***已簽核資料

            '***人員
            Dim ddl_user As DropDownList = CType(e.Row.FindControl("ddl_user"), DropDownList)
            Dim pan_user As Panel = CType(e.Row.FindControl("pan_user"), Panel)
            Dim lb_username As Label = CType(e.Row.FindControl("lb_username"), Label)
            Dim lb_userid As Label = CType(e.Row.FindControl("label_userid"), Label)
            '***狀態
            Dim ddl_status As DropDownList = CType(e.Row.FindControl("ddl_status"), DropDownList)
            Dim lb_status As Label = CType(e.Row.FindControl("lb_status"), Label)
            '***意見
            Dim txt_comment As TextBox = CType(e.Row.FindControl("txb_comment"), TextBox)
            Dim lb_comment As Label = CType(e.Row.FindControl("lb_comment"), Label)
            '***關卡***
            Dim o_wf_order As String = CType(e.Row.FindControl("wf_order"), Label).Text

            '***Start
            Select Case o_wf_order
                Case "0"
                    '人員
                    pan_user.Visible = True : ddl_user.Visible = False : lb_userid.Text = uid
                    '狀態
                    ddl_status.Visible = True : lb_status.Visible = False
                    ddl_status.DataSource = Common.Get_WFStatus(0)
                    ddl_status.DataValueField = "wfstatus_id" : ddl_status.DataTextField = "wfstatus_name" : ddl_status.DataBind()
                    '意見
                    txt_comment.Visible = True : lb_comment.Visible = False
                Case "1"
                    Dim dt_WorkFlow As DataTable = Common.Get_Wf_User1_isms(apprvid, o_wf_order, "")
                    ddl_user.Visible = True : pan_user.Visible = False
                    ddl_user.Items.Clear()
                    ddl_user.DataValueField = "wf_userid" : ddl_user.DataTextField = "user_name"
                    ddl_user.DataSource = dt_WorkFlow : ddl_user.DataBind()
            End Select


            If pan_user.Visible = True And lb_userid.Text <> "" Then
                lb_username.Text = ISMS.Common.notNull(Common.Get_User_Name(lb_userid.Text))
                If lb_username.Text = "" Then lb_username.Text = Common.Get_venderemp_Name(lb_userid.Text) '表示非員工，可能為廠商
            End If
        End If
    End Sub

    '送出審核
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            'String.IsNullOrEmpty()
            If Not String.IsNullOrEmpty(applyDate.Text) And Not String.IsNullOrEmpty(startTime_d.Text) And Not String.IsNullOrEmpty(endTime_d.Text) Then
                Dim Apply_Date = DateTime.Parse(Common.YearYYYY(applyDate.Text))
                Dim inTime = DateTime.Parse(Common.YearYYYY(startTime_d.Text + " " + Ddl_StartHour.SelectedValue + ":" + Ddl_StartMin.SelectedValue))
                Dim outTime = DateTime.Parse(Common.YearYYYY(endTime_d.Text + " " + Ddl_EndHour.SelectedValue + ":" + Ddl_EndMin.SelectedValue))
                If Apply_Date > inTime Or inTime > outTime Then
                    Common.showMsg(Me.Page, "請符合申請時間<=連線時間(起)<=連線時間(迄)")
                    Return
                End If
            End If

            If String.IsNullOrEmpty(applyDate.Text) Or String.IsNullOrEmpty(applyOrg.Text) Or String.IsNullOrEmpty(applyName.Text) Or (String.IsNullOrEmpty(applyTel1.Text) And String.IsNullOrEmpty(applyTel2.Text)) _
                Or ((Radio_applyItem.SelectedValue = "2" Or Radio_applyItem.SelectedValue = "3") And String.IsNullOrEmpty(Netconnect_recordId.Text)) Or String.IsNullOrEmpty(applyReason.Text) _
                Or String.IsNullOrEmpty(s_ip_Other.Text) Or String.IsNullOrEmpty(d_ip_content.Text) Or String.IsNullOrEmpty(d_ip_other.Text) _
                Or ((Not chk_s_ip_1.Checked Or String.IsNullOrEmpty(s_ip_1_content.Text)) And (Not chk_s_ip_2.Checked Or String.IsNullOrEmpty(s_ip_2_content.Text))) Then
                Common.showMsg(Me.Page, "請確實填寫資訊")
                Return
            End If


            If GridView.Rows.Count < 1 Then
                Common.showMsg(Me.Page, "請先至(系統管理->審核流程管理)設定關卡流程")
                Return
            End If

            Dim Now As String = Common.YearYYYY(applyDate.Text) '日期
            Dim applyDate_Chn As String = Common.Year(Now).Replace("/", "") '民國YYYMMDD
            Dim sql = "SELECT * FROM Netconnect WHERE applyDate_Chn = '" + applyDate_Chn + "' ORDER BY recordId DESC"
            Dim data As DataTable = Common.Con(sql)
            Dim applyDate_Chn_SN = 1
            If Not (data.Rows.Count = 0) Then
                applyDate_Chn_SN = Integer.Parse(Right(data.Rows(0)("recordId"), 3)) + 1 'SN
            End If

            Dim recordid As String = "A3-" + applyDate_Chn + "-" + applyDate_Chn_SN.ToString.PadLeft(3, "0") '表單編號

            '處理資訊系統軟體代號
            Dim sql2 = "select * from SysCode where syscd_Id = '" + Ddl_Syscd.SelectedValue + "'"
            Dim data2 As DataTable = Common.Con(sql2)
            Dim syscd_Code = data2.Rows(0)("syscd_Code")

            '處理checkbox
            Dim s_ip_1 = If(chk_s_ip_1.Checked, "1", "0")
            Dim s_ip_2 = If(chk_s_ip_2.Checked, "1", "0")

            If Not chk_s_ip_1.Checked Then
                s_ip_1_content.Text = ""
            End If

            If Not chk_s_ip_2.Checked Then
                s_ip_2_content.Text = ""
            End If


            '處理上傳檔案
            If CType(Me.FindControl("File_applyFile"), FileUpload).HasFile Then
                '有檔案
                Dim oriFileName = CType(Me.FindControl("File_applyFile"), FileUpload)
                Dim UpFileName As String = Common.UpFile(File_Path, recordid, CType(Me.FindControl("File_applyFile"), FileUpload))
                '塞入主表資料
                SqlTxt = "  insert into Netconnect ( recordId, applyDate, applyDate_Chn, applyDate_Chn_SN, empUid, empName," _
                    + "applyOrg, applyName, applyTel1, applyTel2, applyItem, Netconnect_recordId," _
                    + "syscd_Id, syscd_Code, syscd_Name," _
                    + "startTime_d, startTime_h, startTime_m, endTime_d, endTime_h, endTime_m," _
                    + "applyReason, s_ip_1, s_ip_1_content, s_ip_2, s_ip_2_content, s_ip_Other," _
                    + "d_ip_content, d_ip_other, applyFile_ori_name, applyFile, using, create_time, op_time ) values (" _
                    + "'" + recordid + "','" + Now + "','" + applyDate_Chn + "','" + applyDate_Chn_SN.ToString() + "','" + uid + "','" + uname + "'," _
                    + "'" + applyOrg.Text + "','" + applyName.Text + "','" + applyTel1.Text + "','" + applyTel2.Text + "','" + Radio_applyItem.SelectedValue + "','" + Netconnect_recordId.Text + "'," _
                    + "'" + Ddl_Syscd.SelectedValue + "','" + syscd_Code + "','" + Ddl_Syscd.SelectedItem.Text + "'," _
                    + "'" + Common.YearYYYY(startTime_d.Text) + "','" + Ddl_StartHour.SelectedValue + "','" + Ddl_StartMin.SelectedValue + "'," _
                    + "'" + Common.YearYYYY(endTime_d.Text) + "','" + Ddl_EndHour.SelectedValue + "','" + Ddl_EndMin.SelectedValue + "'," _
                    + "'" + applyReason.Text + "','" + s_ip_1 + "','" + s_ip_1_content.Text + "','" + s_ip_2 + "','" + s_ip_2_content.Text + "','" + s_ip_Other.Text + "'," _
                    + "'" + d_ip_content.Text + "','" + d_ip_other.Text + "','" + oriFileName.FileName + "','" + UpFileName + "','Y',getdate(),getdate())"

            Else
                '假如是異動或註銷要確認原文件有沒有檔案
                If (Radio_applyItem.SelectedValue = "2" Or Radio_applyItem.SelectedValue = "3") And Not String.IsNullOrEmpty(applyFile_link.Text) Then
                    '原文件有檔案
                    SqlTxt = "  insert into Netconnect ( recordId, applyDate, applyDate_Chn, applyDate_Chn_SN, empUid, empName," _
                    + "applyOrg, applyName, applyTel1, applyTel2, applyItem, Netconnect_recordId," _
                    + "syscd_Id, syscd_Code, syscd_Name," _
                    + "startTime_d, startTime_h, startTime_m, endTime_d, endTime_h, endTime_m," _
                    + "applyReason, s_ip_1, s_ip_1_content, s_ip_2, s_ip_2_content, s_ip_Other," _
                    + "d_ip_content, d_ip_other, applyFile_ori_name, applyFile, using, create_time, op_time ) values (" _
                    + "'" + recordid + "','" + Now + "','" + applyDate_Chn + "','" + applyDate_Chn_SN.ToString() + "','" + uid + "','" + uname + "'," _
                    + "'" + applyOrg.Text + "','" + applyName.Text + "','" + applyTel1.Text + "','" + applyTel2.Text + "','" + Radio_applyItem.SelectedValue + "','" + Netconnect_recordId.Text + "'," _
                    + "'" + Ddl_Syscd.SelectedValue + "','" + syscd_Code + "','" + Ddl_Syscd.SelectedItem.Text + "'," _
                    + "'" + Common.YearYYYY(startTime_d.Text) + "','" + Ddl_StartHour.SelectedValue + "','" + Ddl_StartMin.SelectedValue + "'," _
                    + "'" + Common.YearYYYY(endTime_d.Text) + "','" + Ddl_EndHour.SelectedValue + "','" + Ddl_EndMin.SelectedValue + "'," _
                    + "'" + applyReason.Text + "','" + s_ip_1 + "','" + s_ip_1_content.Text + "','" + s_ip_2 + "','" + s_ip_2_content.Text + "','" + s_ip_Other.Text + "'," _
                    + "'" + d_ip_content.Text + "','" + d_ip_other.Text + "','" + ori_applyFile_link.Text + "','" + applyFile_link.Text + "','Y',getdate(),getdate())"
                Else
                    '沒有檔案
                    '塞入主表資料
                    SqlTxt = "  insert into Netconnect ( recordId, applyDate, applyDate_Chn, applyDate_Chn_SN, empUid, empName," _
                        + "applyOrg, applyName, applyTel1, applyTel2, applyItem, Netconnect_recordId," _
                        + "syscd_Id, syscd_Code, syscd_Name," _
                        + "startTime_d, startTime_h, startTime_m, endTime_d, endTime_h, endTime_m," _
                        + "applyReason, s_ip_1, s_ip_1_content, s_ip_2, s_ip_2_content, s_ip_Other," _
                        + "d_ip_content, d_ip_other, using, create_time, op_time ) values (" _
                        + "'" + recordid + "','" + Now + "','" + applyDate_Chn + "','" + applyDate_Chn_SN.ToString() + "','" + uid + "','" + uname + "'," _
                        + "'" + applyOrg.Text + "','" + applyName.Text + "','" + applyTel1.Text + "','" + applyTel2.Text + "','" + Radio_applyItem.SelectedValue + "','" + Netconnect_recordId.Text + "'," _
                        + "'" + Ddl_Syscd.SelectedValue + "','" + syscd_Code + "','" + Ddl_Syscd.SelectedItem.Text + "'," _
                        + "'" + Common.YearYYYY(startTime_d.Text) + "','" + Ddl_StartHour.SelectedValue + "','" + Ddl_StartMin.SelectedValue + "'," _
                        + "'" + Common.YearYYYY(endTime_d.Text) + "','" + Ddl_EndHour.SelectedValue + "','" + Ddl_EndMin.SelectedValue + "'," _
                        + "'" + applyReason.Text + "','" + s_ip_1 + "','" + s_ip_1_content.Text + "','" + s_ip_2 + "','" + s_ip_2_content.Text + "','" + s_ip_Other.Text + "'," _
                        + "'" + d_ip_content.Text + "','" + d_ip_other.Text + "','Y',getdate(),getdate())"
                End If
            End If


            '申請人
            SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype ,dp) values "
            SqlTxt += " ('" + apprvid + "','" + recordid + "','" + CType(GridView.Rows(0).FindControl("wf_order"), Label).Text + "','" + uid + "',"
            SqlTxt += "'" + CType(GridView.Rows(0).FindControl("ddl_status"), DropDownList).SelectedValue + "','" + CType(GridView.Rows(0).FindControl("txb_comment"), TextBox).Text + "',getdate() ,'0', 'vender','" + dp + "') "


            '下一位審核人
            If GridView.Rows.Count >= 2 Then
                SqlTxt += " insert into form_record (approvalid , recordid , workflowid , userid , status , comment , signdate , role , usertype ,dp) values "
                SqlTxt += " ('" + apprvid + "','" + recordid + "','" + CType(GridView.Rows(1).FindControl("wf_order"), Label).Text + "','" + CType(GridView.Rows(1).FindControl("ddl_user"), DropDownList).SelectedValue + "',"
                SqlTxt += " '1', '',getdate() ,'1', 'land','" + dp + "') "
            End If

            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Session("success") = "Y"
            Response.Redirect("Netconnect.aspx", True)
            Common.showMsg(Me.Page, "儲存成功！")
        Catch ex As Exception
            lb_msg.Text = ex.Message
        End Try
    End Sub

End Class
