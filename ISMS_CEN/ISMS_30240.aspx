﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ISMS_30240.aspx.vb" Inherits="ISMS_30240" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
    <link href="CSS/isms.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <br />
                <asp:HiddenField ID="Hidden_DP" runat="server" />

        <table id="Doc_TB" runat="server" cellpadding="0" cellspacing="0" class="tb_1" style="width: 770px"
            visible="true">
            <tr>
                <td class="tb_title_w_2" colspan="6">
                    <img src="Images/exe.gif" />
                    風險評鑑填表 &nbsp;
                    <asp:DropDownList ID="ddl_Asset" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:Button ID="Output_XLS" runat="server" Text="另存Excel檔案"/></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3" CssClass="tb_1" Width="770px">
                       
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <asp:SqlDataSource ID="RiskForm_DS" runat="server"></asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>
