﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class ISMS_12120
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand


    Protected Sub publish_SqlDS()
        Publish_DS.ConnectionString = Common.ConnDBS.ConnectionString
        Publish_DS.SelectCommand = "SELECT * FROM docResoure where ( doc_status ='PUBLISH' or doc_status='REV_PUBLISH'  ) and dp='" + Hidden_DP.Value + "' order by create_date"
        GridView1.DataSourceID = Publish_DS.ID
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP

        publish_SqlDS()


    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)
        Dim Ds As New DataSet
        Ds = Common.Get_OldDoc_Rev(SelectedRow.Cells(1).Text, SelectedRow.Cells(3).Text)

        Select Case e.CommandName

            Case "Publish_Cmd"

                '發行新版文件
                SqlTxt = "UPDATE docResoure SET Publisher='" + User.Identity.Name.ToString + "',latest='YES',locked='0',doc_status = 'ISSUE',issue_date='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "' WHERE doc_id='" + SelectedRow.Cells(6).Text + "'"
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()

                If Ds.Tables(0).Rows.Count >= 1 Then
                    '將舊版文件更新OLD資訊
                    SqlTxt = "UPDATE docResoure SET latest='NO',doc_status = 'OLD',locked='0' WHERE doc_id='" + Ds.Tables(0).Rows(0).Item("doc_id").ToString + "'"
                    SqlCmd = New SqlCommand(SqlTxt, Conn)
                    Conn.Open()
                    SqlCmd.ExecuteNonQuery()
                    Conn.Close()

                End If
                If Ds.Tables(0).Rows.Count >= 1 Then
                    Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", SelectedRow.Cells(6).Text, System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "REVDOCPUBLISH", SelectedRow.Cells(2).Text)
                Else
                    Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", SelectedRow.Cells(6).Text, System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "NEWDOCPUBLISH", SelectedRow.Cells(2).Text)

                End If
                'Common.SendMail(Common.Get_Userid(SelectedRow.Cells(4).Text), "DocPublish", SelectedRow.Cells(1).Text, SelectedRow.Cells(2).Text, "")
                'Common.SendMail(Hidden_DP.Value, "DocPublishGroup", SelectedRow.Cells(1).Text, SelectedRow.Cells(2).Text, "")

        End Select
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        e.Row.Cells(6).Visible = False
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType <> DataControlRowType.Header Then
            e.Row.Cells(0).Text = Common.ISMS_DocNameChange(e.Row.Cells(0).Text)
            e.Row.Cells(4).Text = Common.Get_User_Name(e.Row.Cells(4).Text)
        End If
        Dim sMessage, sShowField As String
        Dim oDataRow As Data.DataRowView
        If e.Row.RowIndex <> -1 Then
            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = e.Row.Cells(1).Text
            sMessage = String.Format("您確定要發行此份 [{0}] 文件嗎?", sShowField)
            CType(e.Row.Cells(5).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"
        End If

    End Sub
End Class
