﻿Imports System.Security
Partial Class Top_Page
    Inherits PageBase
    Dim Common As New ISMS.Common


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Login_Usr.Text = Common.Get_DP_Name(User.Identity.Name.ToString) + " 登入者：" + Common.Get_User_Name(User.Identity.Name.ToString) + "(" + User.Identity.Name.ToString + ")"
        'Me.Login_Usr.Text = "登入者：" + System.Security.Principal.WindowsIdentity.GetCurrent.Name
        Me.Link_Label.NavigateUrl = "Logout.aspx"
        Me.Link_Label.Text = "【登出系統】"
        Me.Link_Label.Target = "_top"
        Me.PwdLink.Visible = True
        Me.PwdLink.NavigateUrl = "ChPwd.aspx"
        Me.PwdLink.Text = "【密碼修改】"
        Me.PwdLink.Target = "Main"
    End Sub
End Class
