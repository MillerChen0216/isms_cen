﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Partial Class ISMS_30110
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        If Not Page.IsPostBack Then
            Showddl()

        End If

        SqlDataSource()

        Dim ds As New DataSet
        ds = Common.Get_Asset()
        If ds.Tables(0).Rows.Count = 0 Then
            Server.Transfer("ISMS_20110.aspx")
        End If



    End Sub
    Protected Sub SqlDataSource()
        SqlDataSource1.ConnectionString = Conn.ConnectionString
        SqlTxt = "SELECT a.sn,a.asset_id,a.info_type,a.asset_score,a.asset_desc FROM assetValueList as a,infoSecurityType as b WHERE a.info_type = b.info_type AND a.asset_id=" + ddl_Asset.SelectedValue + " AND (a.info_type = '" + ddl_infoSecurityType.SelectedValue + "' OR '" + ddl_infoSecurityType.SelectedValue + "'='All') ORDER BY b.info_order,a.asset_score"
        SqlDataSource1.SelectCommand = SqlTxt

        GridView1.DataSourceID = SqlDataSource1.ID
        'Dim KeyNames() As String = {"sn"}
        'GridView1.DataKeyNames = KeyNames
    End Sub
    Protected Sub Showddl()
        ddl_Asset.DataSource = Common.Get_Asset()
        ddl_Asset.DataValueField = "asset_id"
        ddl_Asset.DataTextField = "asset_name"
        ddl_Asset.DataBind()
        ddl_asset_upd.DataSource = Common.Get_Asset()
        ddl_asset_upd.DataValueField = "asset_id"
        ddl_asset_upd.DataTextField = "asset_name"
        ddl_asset_upd.DataBind()
        ddl_asset_ins.DataSource = Common.Get_Asset()
        ddl_asset_ins.DataValueField = "asset_id"
        ddl_asset_ins.DataTextField = "asset_name"
        ddl_asset_ins.DataBind()
        ddl_infoSecurityType.DataSource = Common.Get_infoSecurityType()
        ddl_infoSecurityType.DataValueField = "info_type"
        ddl_infoSecurityType.DataTextField = "info_Fname"
        ddl_infoSecurityType.DataBind()
        ddl_infoSecurityType_upd.DataSource = Common.Get_infoSecurityType()
        ddl_infoSecurityType_upd.DataValueField = "info_type"
        ddl_infoSecurityType_upd.DataTextField = "info_Fname"
        ddl_infoSecurityType_upd.DataBind()
        ddl_infoSecurityType_ins.DataSource = Common.Get_infoSecurityType()
        ddl_infoSecurityType_ins.DataValueField = "info_type"
        ddl_infoSecurityType_ins.DataTextField = "info_Fname"
        ddl_infoSecurityType_ins.DataBind()
    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim Index As Integer = CType(e.CommandArgument, Int32)
        Dim SelectedRow As GridViewRow = GridView1.Rows(Index)

        'SelectedRow.Cells(5).Visible = False


        Select Case e.CommandName

            Case "Del_Btn"
                SqlTxt = "DELETE FROM [assetValueList] where sn ='" + SelectedRow.Cells(5).Text + "' "
                SqlCmd = New SqlCommand(SqlTxt, Conn)
                Conn.Open()
                SqlCmd.ExecuteNonQuery()
                Conn.Close()
                Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "DEL", CType(SelectedRow.FindControl("ddl_infotype"), DropDownList).SelectedItem.Text + "-" + SelectedRow.Cells(1).Text + "-" + SelectedRow.Cells(2).Text)

                GridView1.DataBind()

            Case "Edit_Btn"
                ' Response.Write(SelectedRow.Cells(5).Visible)
                '  SelectedRow.Cells(5).Visible = True

                Edit_TB.Visible = True
                Show_TB.Visible = False
                ddl_asset_upd.SelectedValue = SelectedRow.Cells(6).Text
                ddl_infoSecurityType_upd.SelectedValue = CType(Me.GridView1.Rows(Index).FindControl("ddl_infotype"), DropDownList).SelectedValue
                asset_score_upd.Text = SelectedRow.Cells(1).Text
                asset_desc_upd.Text = SelectedRow.Cells(2).Text
                Hidden_SN.Value = SelectedRow.Cells(5).Text


        End Select
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated

        e.Row.Cells(5).Visible = False
        e.Row.Cells(6).Visible = False
        'Me.GridView1.Columns.Item(5).Visible = False

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        Dim oDataRow As Data.DataRowView
        Dim sShowField As String
        Dim sMessage As String
        If e.Row.RowIndex <> -1 Then

            oDataRow = CType(e.Row.DataItem, Data.DataRowView)
            sShowField = oDataRow.Item("asset_desc").ToString()
            sMessage = String.Format("您確定要刪除 [{0}] 嗎?", sShowField)
            CType(e.Row.Cells(4).Controls(0), Button).OnClientClick = "if (confirm('" & sMessage & "')==false) {return false;}"
        End If
    End Sub

    Protected Sub Update_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update_Btn.Click
        If asset_score_upd.Text = "" Then
            Common.showMsg(Me.Page, "分數不允許空白")
        Else

            SqlTxt = "UPDATE [assetValueList]  SET  asset_id =" + ddl_asset_upd.SelectedValue + ",info_type='" + ddl_infoSecurityType_upd.SelectedValue + "',asset_score=" + asset_score_upd.Text + ",asset_desc='" + asset_desc_upd.Text + "' WHERE sn =" + Hidden_SN.Value
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "EDIT", ddl_infoSecurityType_upd.SelectedItem.Text + "-" + asset_score_upd.Text + "-" + asset_desc_upd.Text)

            GridView1.DataBind()
            Show_TB.Visible = True
            Edit_TB.Visible = False
        End If
    End Sub

    Protected Sub Insert_Btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insert_Btn.Click
        If asset_score_ins.Text = "" Then
            Common.showMsg(Me.Page, "分數不允許空白")
        Else
            SqlTxt = "INSERT INTO [assetValueList]  ([asset_id],[info_type],[asset_score],[asset_desc]) VALUES (" + ddl_asset_ins.SelectedValue + " , '" + ddl_infoSecurityType_ins.SelectedValue + "'," + asset_score_ins.Text + ",'" + asset_desc_ins.Text + "' ) "
            SqlCmd = New SqlCommand(SqlTxt, Conn)
            Conn.Open()
            SqlCmd.ExecuteNonQuery()
            Conn.Close()
            Common.Ins_Log(User.Identity.Name.ToString, "EMPLOYEE", "", System.IO.Path.GetFileName(Request.PhysicalPath), "APPLICATION", "ADD", ddl_infoSecurityType_ins.SelectedItem.Text + "-" + asset_score_ins.Text + "-" + asset_desc_ins.Text)

            GridView1.DataBind()



            Show_TB.Visible = True
            ins_TB.Visible = False

            asset_score_ins.Text = "0"
            asset_desc_ins.Text = ""

        End If
    End Sub

    Protected Sub Show_Insert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Show_Insert.Click
        Show_TB.Visible = False
        ins_TB.Visible = True
    End Sub

   
End Class
