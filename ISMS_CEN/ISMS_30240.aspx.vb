﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports System.Collections.Generic
Partial Class ISMS_30240
    Inherits System.Web.UI.Page
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Not Page.IsPostBack Then
            ddl_Asset.DataSource = Common.Get_Asset(Hidden_DP.Value)
            ddl_Asset.DataValueField = "asset_id"
            ddl_Asset.DataTextField = "asset_name"
            ddl_Asset.DataBind()
            SetFields()
        End If
        SqlDataSource()
    End Sub


    Protected Sub SqlDataSource()
        RiskForm_DS.ConnectionString = Conn.ConnectionString
        RiskForm_DS.SelectCommand = "SELECT b.Confidentiality,b.Integrity,b.Availability,b.location,b.item_amount,b.user_id,b.holder,a.assitem_id,b.assitem_name,a.info_type,c.info_type+'('+c.info_Cname+')' as info_Fname,a.weakness_id,d.weakness,e.threat,a.threat_id,a.riskp_score,a.impact_score FROM riskEvaluate as a,assetItem as b,infoSecurityType as c,weakness as d,threat as e WHERE b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id AND a.info_type = c.info_type AND b.asset_id=" + ddl_Asset.SelectedValue + " ORDER BY a.assitem_id,c.info_order"
        'Response.Write("SELECT b.Confidentiality,b.Integrity,b.Availability,b.location,b.item_amount,b.user_id,b.holder,a.assitem_id,b.assitem_name,a.info_type,c.info_type+'('+c.info_Cname+')' as info_Fname,a.weakness_id,d.weakness,e.threat,a.threat_id,a.riskp_score,a.impact_score FROM riskEvaluate as a,assetItem as b,infoSecurityType as c,weakness as d,threat as e WHERE b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id AND a.info_type = c.info_type AND b.asset_id=" + ddl_Asset.SelectedValue + " ORDER BY a.assitem_id,c.info_order")
        GridView1.DataSourceID = RiskForm_DS.ID
        'Dim KeyNames() As String = {"assitem_id"}
        'GridView1.DataKeyNames = KeyNames
    End Sub

    Public Shared Sub GroupRows(ByVal GridView1 As GridView, ByVal cellNum As Integer, ByVal cellNum2 As Integer)
        Dim i As Integer = 0, rowSpanNum As Integer = 1
        While i < GridView1.Rows.Count - 1
            Dim gvr As GridViewRow = GridView1.Rows(i)
            i += 1
            While i < GridView1.Rows.Count
                Dim gvrNext As GridViewRow = GridView1.Rows(i)
                If gvr.Cells(cellNum).Text + gvr.Cells(cellNum2).Text = gvrNext.Cells(cellNum).Text + gvrNext.Cells(cellNum2).Text Then
                    gvrNext.Cells(cellNum).Visible = False
                    rowSpanNum += 1
                Else
                    gvr.Cells(cellNum).RowSpan = rowSpanNum
                    rowSpanNum = 1
                    Exit While
                End If

                If i = GridView1.Rows.Count - 1 Then
                    gvr.Cells(cellNum).RowSpan = rowSpanNum
                End If
                i += 1
            End While
        End While
    End Sub

   


    Protected Sub GridView1_PreRender1(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.PreRender
        GroupRows(Me.GridView1, 1, 1)
    End Sub


    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim tc As TableCellCollection = e.Row.Cells
            tc.Clear()

            tc.Add(New TableHeaderCell)
            tc(0).Text = "資產編號"
            tc.Add(New TableHeaderCell)
            tc(1).Text = "資產名稱"
            tc.Add(New TableHeaderCell)
            tc(2).Text = "使用人"
            tc.Add(New TableHeaderCell)
            tc(3).Text = "保管人"
            'tc.Add(New TableHeaderCell)
            'tc(4).Text = "擁有者"
            tc.Add(New TableHeaderCell)
            tc(4).Text = "數量"
            tc.Add(New TableHeaderCell)
            tc(5).Text = "放置地點"
            tc.Add(New TableHeaderCell)
            tc(6).Text = "機密性"
            tc.Add(New TableHeaderCell)
            tc(7).Text = "完整性"
            tc.Add(New TableHeaderCell)
            tc(8).Text = "可用性"
            tc.Add(New TableHeaderCell)
            tc(9).Text = "資產價值"
            tc.Add(New TableHeaderCell)
            tc(10).Text = "資產安全等級"
            tc.Add(New TableHeaderCell)
            tc(11).Text = "脆弱點敘述"
            tc.Add(New TableHeaderCell)
            tc(12).Text = "威脅述敘"
            tc.Add(New TableHeaderCell)
            tc(13).Text = "風險機率值"
            tc.Add(New TableHeaderCell)
            tc(14).Text = "風險衝擊值"
            tc.Add(New TableHeaderCell)
            tc(15).Text = "總風險值"
            tc.Add(New TableHeaderCell)
            tc(16).Text = "風險等級"



        End If
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        '    Dim drv As DataRow
        '    drv = CType(e.Row.DataItem, DataRow)
        '    If e.Row.RowType = DataControlRowType.DataRow Then


        '        'e.Row.Cells(0).RowSpan = 3
        '        e.Row.DataBind()


        '    End If

        If e.Row.RowIndex <> -1 Then
            Dim assitScroe As Integer
            Dim secure_lv, serure_desc As String
            Dim Ds As New DataSet
            assitScroe = Int(e.Row.Cells(8).Text) + Int(e.Row.Cells(7).Text) + Int(e.Row.Cells(6).Text)
            e.Row.Cells(9).Text = assitScroe.ToString
            Ds = Common.Get_Secure_lv(e.Row.Cells(9).Text)
            secure_lv = Ds.Tables(0).Rows(0).Item("secure_lv")
            serure_desc = Ds.Tables(0).Rows(0).Item("serure_desc")

            e.Row.Cells(10).Text = secure_lv + "(" + serure_desc + ")"
            'e.Row.Cells(0).RowSpan = 3
            'e.Row.Cells(1).RowSpan = 3
            'e.Row.Cells(2).RowSpan = 3
            'e.Row.Cells(3).RowSpan = 3
            'e.Row.Cells(4).RowSpan = 3
            'e.Row.Cells(5).RowSpan = 3
            'e.Row.Cells(6).RowSpan = 3
            'e.Row.Cells(7).RowSpan = 3
            'e.Row.Cells(8).RowSpan = 3
            'e.Row.Cells(9).RowSpan = 3
            'e.Row.Cells(10).RowSpan = 3
            'e.Row.Cells(11).RowSpan = 0
            'e.Row.Cells(12).RowSpan = 0
            'e.Row.Cells(13).RowSpan = 0
            'e.Row.Cells(14).RowSpan = 0
            'e.Row.Cells(15).RowSpan = 0

        End If


    End Sub

    'Public Shared Sub GroupRows(ByVal GridView1 As GridView, ByVal cellNum As Integer)
    '    Dim i As Integer = 0, rowSpanNum As Integer = 1
    '    While i < GridView1.Rows.Count - 1
    '        Dim gvr As GridViewRow = GridView1.Rows(i)
    '        i += 1
    '        While i < GridView1.Rows.Count
    '            Dim gvrNext As GridViewRow = GridView1.Rows(i)
    '            If gvr.Cells(cellNum).Text = gvrNext.Cells(cellNum).Text Then
    '                gvrNext.Cells(cellNum).Visible = False
    '                rowSpanNum += 1
    '            Else
    '                gvr.Cells(cellNum).RowSpan = rowSpanNum
    '                rowSpanNum = 1
    '                Exit While
    '            End If
    '            If i = GridView1.Rows.Count - 1 Then
    '                gvr.Cells(cellNum).RowSpan = rowSpanNum
    '            End If
    '            i += 1
    '        End While
    '    End While
    'End Sub







    Protected Sub SetFields()

        Dim assitem_id, Confidentiality, Integrity, Availability, location, item_amount, user_id, holder, assitem_name, info_type As New BoundField
        Dim assitSecure_lv, assitScore, info_Fname, weakness_id, threat, threat_id, riskp_score, impact_score As New BoundField

        assitem_id.DataField = "assitem_id"
        Confidentiality.DataField = "Confidentiality"
        Integrity.DataField = "Integrity"
        Availability.DataField = "Availability"
        location.DataField = "location"
        item_amount.DataField = "item_amount"
        user_id.DataField = "user_id"

        holder.DataField = "holder"
        assitem_name.DataField = "assitem_name"
        info_type.DataField = "info_type"
        info_Fname.DataField = "info_Fname"
        weakness_id.DataField = "weakness_id"
        threat.DataField = "threat"
        threat_id.DataField = "threat_id"
        riskp_score.DataField = "riskp_score"
        assitSecure_lv.DataField = ""
        assitScore.DataField = ""


        GridView1.Columns.Add(assitem_id)
        GridView1.Columns.Add(assitem_name)
        GridView1.Columns.Add(user_id)
        GridView1.Columns.Add(holder)
        GridView1.Columns.Add(item_amount)
        GridView1.Columns.Add(location)
        GridView1.Columns.Add(Confidentiality)
        GridView1.Columns.Add(Integrity)
        GridView1.Columns.Add(Availability)
        GridView1.Columns.Add(assitScore)
        GridView1.Columns.Add(assitSecure_lv)
        'GridView1.Columns.Add(assitem_id)
        'GridView1.Columns.Add(assitem_id)
        'GridView1.Columns.Add(assitem_id)
        'GridView1.Columns.Add(assitem_id)
        'GridView1.Columns.Add(assitem_id)


        'GridView1.Columns.Add(user_id)
        'GridView1.Columns.Add(user_id)
        'GridView1.Columns.Add(user_id)
        'GridView1.Columns.Add(user_id)
        'GridView1.Columns.Add(user_id)
        'GridView1.Columns.Add(user_id)
        'GridView1.Columns.Add(user_id)
        'GridView1.Columns.Add(user_id)


    End Sub
End Class
