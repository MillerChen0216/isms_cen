﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports System.Collections.Generic
Partial Class ISMS_31180
    Inherits PageBase
    Dim Common As New ISMS.Common
    Dim Conn As SqlConnection = Common.ConnDBS
    Dim SqlTxt As String
    Dim SqlCmd As SqlCommand
    Dim SqlDr As SqlDataReader
    Dim i As Integer
    Dim oRow As GridViewRow
    Dim Index As Integer
    Public appid As String = "5"

    Protected Sub SqlDataSource()
        SqlDS1.ConnectionString = Conn.ConnectionString
        'SqlDS1.SelectCommand = "SELECT distinct a.assitem_id,a.assitem_name,a.Confidentiality,a.Integrity,a.Availability,(a.Confidentiality" + Common.Sys_Config("Math_operation") + "a.Integrity" + Common.Sys_Config("Math_operation") + "a.Availability) as asset_score,(SELECT max(riskp_score" + Common.Sys_Config("Risk_operation") + "impact_score) as tw_score  FROM ThreatWeaknessEvaluate group by asset_id)as tw_score,((a.Confidentiality" + Common.Sys_Config("Math_operation") + "a.Integrity" + Common.Sys_Config("Math_operation") + "a.Availability)" + Common.Sys_Config("Risk_operation") + "((SELECT max(riskp_score" + Common.Sys_Config("Risk_operation") + "impact_score) as tw_score  FROM ThreatWeaknessEvaluate group by asset_id)) ) as total_score from assetItem as a,ThreatWeaknessEvaluate as b WHERE a.asset_id=b.asset_id and (a.Confidentiality >=" + Common.Risk_Config("C") + " or a.Integrity>=" + Common.Risk_Config("I") + " or a.Availability >=" + Common.Risk_Config("A") + ") and  (a.Confidentiality+a.Integrity+a.Availability) >=" + Common.Risk_Config("assetValue") + "  and a.asset_id=" + ddl_Asset.SelectedValue + " ORDER BY a.assitem_id"
        '港務v1
        'Dim Sqlstr = "select  distinct (a.Confidentiality" + Common.Sys_Config("Math_operation") + "a.Integrity" + Common.Sys_Config("Math_operation") + "a.Availability) as asset_score, a.assitem_id,a.assitem_name,a.assgroup_id,d.assgroup_name,b.riskp_score*b.impact_score as twscore,b.riskp_score" + Common.Sys_Config("Risk_operation") + "b.impact_score" + Common.Sys_Config("Risk_operation") + "(a.Confidentiality" + Common.Sys_Config("Math_operation") + "a.Integrity" + Common.Sys_Config("Math_operation") + "a.Availability)as totalscore,a.Confidentiality,a.Integrity,a.Availability " + " from assetItem a,ThreatWeaknessEvaluate b,View_ThreatWeaknessEvaluateMAX as c , assetGroup as d" + " where a.dp='" + Hidden_DP.Value + "' and a.evaluate='Y' and a.dp=b.dp and a.dp=c.dp and a.asset_id = b.asset_id and b.asset_id = c.asset_id and a.assgroup_id=d.assgroup_id  and a.assgroup_id=b.assgroup_id  and a.assgroup_id= c.assgroup_id " + " and b.riskp_score*b.impact_score = c.subscore and (a.Confidentiality >= (select risk_cfvalue from riskConfig where risk_cftype='C') " + " or a.Integrity >=(select risk_cfvalue from riskConfig where risk_cftype='I') or a.Availability >=(select risk_cfvalue from riskConfig where risk_cftype='A') " + " or (a.Confidentiality" + Common.Sys_Config("Math_operation") + "a.Integrity" + Common.Sys_Config("Math_operation") + "a.Availability) >= (select risk_cfvalue from riskConfig where risk_cftype='assetValue')) and b.asset_id=" + ddl_Asset.SelectedValue + " order by a.assitem_id,a.assgroup_id"
        '港務v2
        Dim Sqlstr = "select distinct (a.Confidentiality+a.Integrity+a.Availability) as asset_score, a.assitem_id,a.assitem_name,a.assgroup_id,d.assgroup_name,b.subscore as twscore,b.subscore*(a.Confidentiality+a.Integrity+a.Availability)as totalscore,a.Confidentiality,a.Integrity,a.Availability from assetItem a,View_ThreatWeaknessEvaluateMAX as b , assetGroup as d" + " where a.dp='" + Hidden_DP.Value + "' and a.evaluate='Y' and a.dp=b.dp and a.asset_id = b.asset_id and a.assgroup_id=b.assgroup_id and a.assgroup_id=d.assgroup_id " + " and (a.Confidentiality >= (select risk_cfvalue from riskConfig where risk_cftype='C') " + " or a.Integrity >=(select risk_cfvalue from riskConfig where risk_cftype='I') or a.Availability >=(select risk_cfvalue from riskConfig where risk_cftype='A') " + " or (a.Confidentiality" + Common.Sys_Config("Math_operation") + "a.Integrity" + Common.Sys_Config("Math_operation") + "a.Availability) >= (select risk_cfvalue from riskConfig where risk_cftype='assetValue')) and b.asset_id=" + ddl_Asset.SelectedValue + " and a.assitem_id not in (select assitem_id from riskEvaluate) union select distinct (a.Confidentiality+a.Integrity+a.Availability) as asset_score, a.assitem_id,a.assitem_name,a.assgroup_id,d.assgroup_name,b.riskp_score*b.impact_score as twscore,b.riskp_score*b.impact_score*(a.Confidentiality+a.Integrity+a.Availability)as totalscore,a.Confidentiality,a.Integrity,a.Availability" + " from assetItem a,riskEvaluate b,assetGroup as d " + " where a.dp='" + Hidden_DP.Value + "' and a.evaluate='Y' and a.dp=b.dp and a.assitem_id = b.assitem_id and a.assgroup_id=d.assgroup_id " + " and (a.Confidentiality >= (select risk_cfvalue from riskConfig where risk_cftype='C') " + " or a.Integrity >=(select risk_cfvalue from riskConfig where risk_cftype='I') or a.Availability >=(select risk_cfvalue from riskConfig where risk_cftype='A') " + " or (a.Confidentiality" + Common.Sys_Config("Math_operation") + "a.Integrity" + Common.Sys_Config("Math_operation") + "a.Availability) >= (select risk_cfvalue from riskConfig where risk_cftype='assetValue')) and a.asset_id=" + ddl_Asset.SelectedValue + " order by a.assgroup_id,a.assitem_id"
        SqlDS1.SelectCommand = Sqlstr
        'Response.Write(Sqlstr)
        '"and (a.Confidentiality" + Common.Sys_Config("Math_operation") + "a.Integrity" + Common.Sys_Config("Math_operation") + "a.Availability) >= (select risk_cfvalue from riskConfig where risk_cftype='assetValue') and b.riskp_score" + Common.Sys_Config("Risk_operation") + "b.impact_score" + Common.Sys_Config("Risk_operation") + "(a.Confidentiality" + Common.Sys_Config("Math_operation") + "a.Integrity" + Common.Sys_Config("Math_operation") + "a.Availability) >= (select risk_cfvalue from riskConfig where risk_cftype='acceptRisk') " + _
        'RiskForm_DS.SelectCommand = "SELECT a.risk_type,a.assitem_id ,b.assitem_name ,b.user_id ,b.holder ,b.dept_id,b.item_amount ,b.location ,b.Confidentiality ,b.Integrity ,b.Availability ,(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability) as assitScore,(Select secure_lv from secureLevel where lv_scoreS <=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)  and  lv_scoreE >=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)) as assitScore_lv ,(Select serure_desc from secureLevel where lv_scoreS <=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)  and  lv_scoreE >=(b.Confidentiality " + Common.Sys_Config("Math_operation") + " b.Integrity " + Common.Sys_Config("Math_operation") + " b.Availability)) as assitSecure_lv,d.weakness,e.threat,a.riskp_score,a.impact_score ,(a.riskp_score " + Common.Sys_Config("RISK_operation") + " a.impact_score " + Common.Sys_Config("RISK_operation") + " (b.Confidentiality + b.Integrity + b.Availability))  as totalrisk_score,(Select totalrisk_desc from totalRisklv where lv_scoreS <=(a.riskp_score " + Common.Sys_Config("Risk_operation") + " a.impact_score" + Common.Sys_Config("Risk_operation") + " (Select secure_lv from secureLevel where lv_scoreS <=(b.Confidentiality + b.Integrity + b.Availability) and lv_scoreE >=(b.Confidentiality + b.Integrity + b.Availability)) )  and  lv_scoreE >=(a.riskp_score " + Common.Sys_Config("Risk_operation") + " a.impact_score " + Common.Sys_Config("Risk_operation") + " (Select secure_lv from secureLevel where lv_scoreS <=(b.Confidentiality + b.Integrity + b.Availability) and lv_scoreE >=(b.Confidentiality + b.Integrity + b.Availability)) )) as totalrisk_lv FROM riskEvaluate as a,assetItem as b,weakness as d,threat as e WHERE b.asset_id = d.asset_id AND b.asset_id = e.asset_id AND a.weakness_id = d.weakness_id AND a.threat_id = e.threat_id AND a.assitem_id=b.assitem_id AND b.asset_id=" + ddl_Asset.SelectedValue + " ORDER BY a.assitem_id"
        'Response.Write("SELECT distinct a.assitem_id,a.assitem_name,a.Confidentiality,a.Integrity,a.Availability,(a.Confidentiality" + Common.Sys_Config("Math_operation") + "a.Integrity" + Common.Sys_Config("Math_operation") + "a.Availability) as asset_score,(SELECT max(riskp_score" + Common.Sys_Config("Risk_operation") + "impact_score) as tw_score  FROM ThreatWeaknessEvaluate group by asset_id)as tw_score,((a.Confidentiality" + Common.Sys_Config("Math_operation") + "a.Integrity" + Common.Sys_Config("Math_operation") + "a.Availability)" + Common.Sys_Config("Risk_operation") + "((SELECT max(riskp_score" + Common.Sys_Config("Risk_operation") + "impact_score) as tw_score  FROM ThreatWeaknessEvaluate group by asset_id)) ) as total_score from assetItem as a,ThreatWeaknessEvaluate as b WHERE (a.Confidentiality >=(select risk_cfvalue from riskConfig where risk_cftype='C') or a.Integrity >=(select risk_cfvalue from riskConfig where risk_cftype='I') or a.Availability>=(select risk_cfvalue from riskConfig where risk_cftype='A')) and ((a.Confidentiality+a.Integrity+a.Availability) >=(select risk_cfvalue from riskConfig where risk_cftype='assetValue')) and  a.asset_id=b.asset_id and  a.asset_id=" + ddl_Asset.SelectedValue + " ORDER BY a.assitem_id")
        GridView1.DataSourceID = SqlDS1.ID
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Check_users()
        Hidden_DP.Value = Me.Get_DP
        If Not Page.IsPostBack Then
            ddl_Asset.DataSource = Common.Get_Asset()
            ddl_Asset.DataValueField = "asset_id"
            ddl_Asset.DataTextField = "asset_name"
            ddl_Asset.DataBind()
        End If
        Dim ds As New DataSet
        ds = Common.Get_Asset()
        If ds.Tables(0).Rows.Count = 0 Then
            Server.Transfer("ISMS_20110.aspx")
        End If

        SqlDataSource()
        GridView1.DataBind()

        If GridView1.Rows.Count <= 0 Then
            tb2.Visible = False
            Output_XLS.Visible = False
        Else
            tb2.Visible = True
            Output_XLS.Visible = True
            WF_SqlDS()
        End If

    End Sub

    Protected Sub GridView1_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.PreRender
        Dim i As Integer = 1
        Dim j As Integer = 0
        Dim k As Integer
        Dim mySingleRow As GridViewRow

        For Each mySingleRow In GridView1.Rows

            If CInt(mySingleRow.RowIndex) = 0 Then

                For k = 0 To 2
                    mySingleRow.Cells(k).RowSpan = 1
                Next


            Else
                If mySingleRow.Cells(0).Text.Trim() = GridView1.Rows(CInt(mySingleRow.RowIndex) - i).Cells(0).Text.Trim() And mySingleRow.Cells(2).Text.Trim() = GridView1.Rows(CInt(mySingleRow.RowIndex) - i).Cells(2).Text.Trim() Then

                    For k = 0 To 2
                        GridView1.Rows(CInt(mySingleRow.RowIndex) - i).Cells(k).RowSpan += 1
                    Next

                    i = i + 1
                    For k = 0 To 2
                        mySingleRow.Cells(k).Visible = False
                    Next


                Else
                    For k = 0 To 2
                        GridView1.Rows(CInt(mySingleRow.RowIndex)).Cells(k).RowSpan = 1
                    Next
                    i = 1
                End If
            End If
        Next


    End Sub


    Protected Sub WF_SqlDS()

        WF_DS.ConnectionString = Common.ConnDBS.ConnectionString
        WF_DS.SelectCommand = "SELECT * FROM workflowName where apprv_id=" + appid + " and dp='" + Hidden_DP.Value + "' order by wf_order, wf_inside_order"
        GridView2.DataSourceID = WF_DS.ID

    End Sub

    Protected Sub GridView2_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowCreated
        e.Row.Cells(5).Visible = False

    End Sub

    Protected Sub Send_WF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Send_WF.Click

        Dim Ds As New DataSet



        Dim DateTime_Str As String = DateTime.Now.ToString("yyyyMMddHHmmss")
        Dim DateTime_Str1 As String = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")

        SqlTxt = " INSERT INTO ThreatWeaknessEvaluateMaster" + _
        "(dp,asset_id,riskEvtab_name,form_status,add_user,add_date,apprv_id)" + _
        "VALUES('" + Hidden_DP.Value + "','" + ddl_Asset.SelectedValue + "','" + DateTime_Str + "_風險評估彙整表','RISK_VERIFY','" + User.Identity.Name.ToString + "','" + DateTime_Str1 + "'," + appid + ") "
        'Response.Write(SqlTxt)
        'Response.End()

        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()

        Ds = Common.Get_MaxThreatWeaknessEvaluateFormid(appid)
        Dim Max_id As String = Ds.Tables(0).Rows(0).Item(0)

        SqlTxt = "INSERT INTO ThreatWeaknessEvaluateForm (PID,assitScore,assitem_id,assitem_name,assgroup_id,assgroup_name,twScore,totalrisk_score,confidentiality,Integrity,Availability)" + _
        " select distinct '" + Max_id + "',(a.Confidentiality+a.Integrity+a.Availability) as asset_score, a.assitem_id,a.assitem_name,a.assgroup_id,d.assgroup_name,b.subscore as twscore,b.subscore*(a.Confidentiality+a.Integrity+a.Availability)as totalscore,a.Confidentiality,a.Integrity,a.Availability from assetItem a,View_ThreatWeaknessEvaluateMAX as b , assetGroup as d" + " where a.dp='" + Hidden_DP.Value + "' and a.evaluate='Y' and a.dp=b.dp and a.asset_id = b.asset_id and a.assgroup_id=b.assgroup_id and a.assgroup_id=d.assgroup_id " + " and (a.Confidentiality >= (select risk_cfvalue from riskConfig where risk_cftype='C') " + " or a.Integrity >=(select risk_cfvalue from riskConfig where risk_cftype='I') or a.Availability >=(select risk_cfvalue from riskConfig where risk_cftype='A') " + " or (a.Confidentiality" + Common.Sys_Config("Math_operation") + "a.Integrity" + Common.Sys_Config("Math_operation") + "a.Availability) >= (select risk_cfvalue from riskConfig where risk_cftype='assetValue')) and b.asset_id=" + ddl_Asset.SelectedValue + " and a.assitem_id not in (select assitem_id from riskEvaluate) union select distinct'" + Max_id + "', (a.Confidentiality+a.Integrity+a.Availability) as asset_score, a.assitem_id,a.assitem_name,a.assgroup_id,d.assgroup_name,b.riskp_score*b.impact_score as twscore,b.riskp_score*b.impact_score*(a.Confidentiality+a.Integrity+a.Availability)as totalscore,a.Confidentiality,a.Integrity,a.Availability" + " from assetItem a,riskEvaluate b,assetGroup as d " + " where a.dp='" + Hidden_DP.Value + "' and a.evaluate='Y' and a.dp=b.dp and a.assitem_id = b.assitem_id and a.assgroup_id=d.assgroup_id " + " and (a.Confidentiality >= (select risk_cfvalue from riskConfig where risk_cftype='C') " + " or a.Integrity >=(select risk_cfvalue from riskConfig where risk_cftype='I') or a.Availability >=(select risk_cfvalue from riskConfig where risk_cftype='A') " + " or (a.Confidentiality" + Common.Sys_Config("Math_operation") + "a.Integrity" + Common.Sys_Config("Math_operation") + "a.Availability) >= (select risk_cfvalue from riskConfig where risk_cftype='assetValue')) and a.asset_id=" + ddl_Asset.SelectedValue + " order by a.assgroup_id,a.assitem_id"

        'Response.Write(SqlTxt)
        'Response.End()
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()

        SqlTxt = "INSERT INTO wfFormRecord " + _
             "(apprv_id,doc_id,wf_order,wf_userid,wf_status_id,wf_comment,signdate,record_id,wf_inside_order,doc_status) VALUES" + _
            "('" + appid + "','" + Max_id + "','" + GridView2.Rows(0).Cells(0).Text + "','" + CType(GridView2.Rows(0).FindControl("label_userid"), Label).Text + "','" + CType(GridView2.Rows(0).FindControl("ddl_status"), DropDownList).SelectedValue + "'," + _
            " '" + CType(GridView2.Rows(0).FindControl("comment_txt"), TextBox).Text + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','1','" + GridView2.Rows(0).Cells(5).Text + "','RISK')"
        SqlCmd = New SqlCommand(SqlTxt, Conn)
        Conn.Open()
        SqlCmd.ExecuteNonQuery()
        Conn.Close()

        Common.showMsg(Me.Page, "風險評估彙整表已經送出審核，謝謝!")

    End Sub

    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound

        Dim Ds As New DataSet

        If e.Row.RowIndex <> -1 Then

            If e.Row.RowIndex = 0 Then
                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(User.Identity.Name.ToString)
                CType(e.Row.FindControl("label_userid"), Label).Text = User.Identity.Name.ToString
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("ddl_status"), DropDownList).Visible = True
                CType(e.Row.Cells(e.Row.RowIndex).FindControl("comment_txt"), TextBox).Visible = True
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataSource = Common.Get_WFStatus(0)
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataValueField = "wfstatus_id"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataTextField = "wfstatus_name"
                CType(e.Row.FindControl("ddl_status"), DropDownList).DataBind()
            ElseIf e.Row.RowIndex >= 1 Then
                Ds = Common.Get_Wf_User(Hidden_DP.Value, appid, e.Row.Cells(0).Text, e.Row.Cells(5).Text)
                CType(e.Row.FindControl("lb_username"), Label).Text = Common.Get_User_Name(Ds.Tables(0).Rows(0).Item("wf_userid"))
                CType(e.Row.FindControl("label_userid"), Label).Text = Ds.Tables(0).Rows(0).Item("wf_userid")

            End If

        End If
    End Sub



    Protected Sub Output_XLS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Output_XLS.Click
        Dim sw As New System.IO.StringWriter()
        Dim htw As New System.Web.UI.HtmlTextWriter(sw)
        Response.Clear()
        '檔名支援中文
        Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5")
        '避免儲存中文內容有亂碼
        'Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8")
        Response.Write("<meta http-equiv=Content-Type content=text/html;charset=utf-8>")
        'excel檔名()
        Response.AddHeader("content-disposition", "attachment;filename=" + DateTime.Now.ToString("yyyyMMdd HH:mm") + "_ISMS" + ddl_Asset.SelectedItem.Text + "風險評鑑表.xls")
        Response.ContentType = "application/vnd.ms-excel"
        GridView1.RenderControl(htw)
        Response.Write(sw.ToString())
        Response.End()

    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        '處理'GridView' 的控制項 'GridView' 必須置於有 runat=server 的表單標記之中
    End Sub
End Class
